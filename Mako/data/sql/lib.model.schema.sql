
-----------------------------------------------------------------------------
-- beca
-----------------------------------------------------------------------------

DROP TABLE "beca" CASCADE;


CREATE TABLE "beca"
(
	"id" INT8  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"descuento" DECIMAL(10,6) default 0,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "beca_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "beca" IS '';


COMMENT ON COLUMN "beca"."id" IS 'Identificador de la beca';

COMMENT ON COLUMN "beca"."nombre" IS 'Nombre de la beca';

COMMENT ON COLUMN "beca"."activo" IS 'Bandera de vigencia';

COMMENT ON COLUMN "beca"."descuento" IS 'Descuento que otorga la beca';

SET search_path TO public;
-----------------------------------------------------------------------------
-- asignacion_beca
-----------------------------------------------------------------------------

DROP TABLE "asignacion_beca" CASCADE;


CREATE TABLE "asignacion_beca"
(
	"id" INT8  NOT NULL,
	"beca_id" INT8,
	"centro_id" INTEGER,
	"socio_id" INT8,
	"registro_operador_id" INTEGER,
	"fecha_solicitud" TIMESTAMP,
	"otorga_operador_id" INTEGER,
	"fecha_otorgado" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "uk_asignacion_beca" UNIQUE ("socio_id","beca_id")
);

COMMENT ON TABLE "asignacion_beca" IS '';


COMMENT ON COLUMN "asignacion_beca"."id" IS 'Identificador de la asignacion';

COMMENT ON COLUMN "asignacion_beca"."beca_id" IS 'Identificador de la beca';

COMMENT ON COLUMN "asignacion_beca"."centro_id" IS 'Identificador del centro donde se realiza la operacion';

COMMENT ON COLUMN "asignacion_beca"."socio_id" IS 'Identificador del socio a quien se le asigna beca';

COMMENT ON COLUMN "asignacion_beca"."registro_operador_id" IS 'Identificador del usuario quien registra la solicitud';

COMMENT ON COLUMN "asignacion_beca"."fecha_solicitud" IS 'Identificador de la fecha en que se realiza la solicitud';

COMMENT ON COLUMN "asignacion_beca"."otorga_operador_id" IS 'Identificador del usuario quien otorgó la beca';

COMMENT ON COLUMN "asignacion_beca"."fecha_otorgado" IS 'fecha en la que se otorga la beca';

SET search_path TO public;
-----------------------------------------------------------------------------
-- sincronia
-----------------------------------------------------------------------------

DROP TABLE "sincronia" CASCADE;


CREATE TABLE "sincronia"
(
	"id" INT8  NOT NULL,
	"pk_async" INT8,
	"clase" TEXT  NOT NULL,
	"operacion" VARCHAR(255)  NOT NULL,
	"tipo" VARCHAR(255)  NOT NULL,
	"detalle" TEXT  NOT NULL,
	"completo" BOOLEAN default 'f',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "sincronia" IS '';


COMMENT ON COLUMN "sincronia"."id" IS 'Identificador de registro de sincronia';

COMMENT ON COLUMN "sincronia"."pk_async" IS 'Identificador de la pk del registro principal a sincronizar';

COMMENT ON COLUMN "sincronia"."clase" IS 'La clase principal sobre la que se realizó la operación.';

COMMENT ON COLUMN "sincronia"."operacion" IS 'Nombre de la operación hecha pej. usuario.agregado, usuario.actualizado';

COMMENT ON COLUMN "sincronia"."tipo" IS 'Nombre del tipo de sincronia: broadcast, dc_a_centro, centro_a_dc';

COMMENT ON COLUMN "sincronia"."detalle" IS 'Detalle de la operación en formato YAML';

COMMENT ON COLUMN "sincronia"."completo" IS 'Bandera que indica si se ha completa esta tarea de sincronía.';

SET search_path TO public;
-----------------------------------------------------------------------------
-- sincronia_centro
-----------------------------------------------------------------------------

DROP TABLE "sincronia_centro" CASCADE;


CREATE TABLE "sincronia_centro"
(
	"id" INT8  NOT NULL,
	"sincronia_id" INT8,
	"centro_id" INTEGER,
	"sincronizado" BOOLEAN default 'f',
	"ultimo_mensaje" TEXT,
	"intento" INTEGER default 1,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "sincronia_centro" IS '';


COMMENT ON COLUMN "sincronia_centro"."id" IS 'Identificador de registro a sincronizar';

COMMENT ON COLUMN "sincronia_centro"."sincronia_id" IS 'Identificador del centro a sincronizar';

COMMENT ON COLUMN "sincronia_centro"."centro_id" IS 'Identificador del centro a sincronizar';

COMMENT ON COLUMN "sincronia_centro"."sincronizado" IS 'Bandera que indica si se ha completa esta tarea de sincronía en el centro.';

COMMENT ON COLUMN "sincronia_centro"."ultimo_mensaje" IS 'Ultimo Mensaje de error reportado';

COMMENT ON COLUMN "sincronia_centro"."intento" IS 'Número de intento';

SET search_path TO public;
-----------------------------------------------------------------------------
-- alumnos
-----------------------------------------------------------------------------

DROP TABLE "alumnos" CASCADE;


CREATE TABLE "alumnos"
(
	"matricula" INT8  NOT NULL,
	"socio_id" INT8  NOT NULL,
	PRIMARY KEY ("matricula"),
	CONSTRAINT "alumnos_U_1" UNIQUE ("matricula")
);

COMMENT ON TABLE "alumnos" IS '';


COMMENT ON COLUMN "alumnos"."matricula" IS 'Identificador del alumno';

COMMENT ON COLUMN "alumnos"."socio_id" IS 'Identificador del socio';

SET search_path TO public;
-----------------------------------------------------------------------------
-- aula
-----------------------------------------------------------------------------

DROP TABLE "aula" CASCADE;


CREATE TABLE "aula"
(
	"id" INT8  NOT NULL,
	"centro_id" INTEGER  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"descripcion" TEXT  NOT NULL,
	"capacidad" INTEGER  NOT NULL,
	"disponible" BOOLEAN default 't',
	"activo" BOOLEAN default 't',
	"operador_id" INTEGER,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "aula" IS '';


COMMENT ON COLUMN "aula"."id" IS 'Identificador del aula';

COMMENT ON COLUMN "aula"."centro_id" IS 'Identificador del centro';

COMMENT ON COLUMN "aula"."nombre" IS 'Nombre del aula';

COMMENT ON COLUMN "aula"."descripcion" IS 'Descripcion del aula';

COMMENT ON COLUMN "aula"."capacidad" IS 'Capadidad de alumnos que tiene el aula';

COMMENT ON COLUMN "aula"."disponible" IS 'Flag para saber si el aula esta disponible u ocupada';

COMMENT ON COLUMN "aula"."operador_id" IS 'Identificador del usuario que capturó el registro';

SET search_path TO public;
-----------------------------------------------------------------------------
-- calificacion_parcial
-----------------------------------------------------------------------------

DROP TABLE "calificacion_parcial" CASCADE;


CREATE TABLE "calificacion_parcial"
(
	"id" INT8  NOT NULL,
	"curso_id" INT8  NOT NULL,
	"evaluacion_id" INT8,
	"alumno_id" INT8  NOT NULL,
	"calificacion" NUMERIC(10,5)  NOT NULL,
	"operador_id" INTEGER,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "calificacion_parcial" IS '';


COMMENT ON COLUMN "calificacion_parcial"."id" IS 'Identificador de la calificacion';

COMMENT ON COLUMN "calificacion_parcial"."curso_id" IS 'Identificador del grupo';

COMMENT ON COLUMN "calificacion_parcial"."evaluacion_id" IS 'Identificador de la evaluacion parcial';

COMMENT ON COLUMN "calificacion_parcial"."alumno_id" IS 'Identificador del alumno';

COMMENT ON COLUMN "calificacion_parcial"."calificacion" IS 'calificacion de la evaluacion';

COMMENT ON COLUMN "calificacion_parcial"."operador_id" IS 'Identificador del usuario que capturó el registro';

SET search_path TO public;
-----------------------------------------------------------------------------
-- calificacion_final
-----------------------------------------------------------------------------

DROP TABLE "calificacion_final" CASCADE;


CREATE TABLE "calificacion_final"
(
	"id" INT8  NOT NULL,
	"curso_id" INT8  NOT NULL,
	"evaluacion_id" INT8,
	"alumno_id" INT8  NOT NULL,
	"calificacion" NUMERIC(10,5)  NOT NULL,
	"operador_id" INTEGER,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "calificacion_final" IS '';


COMMENT ON COLUMN "calificacion_final"."id" IS 'Identificador de la calificacion';

COMMENT ON COLUMN "calificacion_final"."curso_id" IS 'Identificador del grupo';

COMMENT ON COLUMN "calificacion_final"."evaluacion_id" IS 'Identificador de la evaluacion final';

COMMENT ON COLUMN "calificacion_final"."alumno_id" IS 'Identificador del alumno';

COMMENT ON COLUMN "calificacion_final"."calificacion" IS 'calificacion de la evaluacion';

COMMENT ON COLUMN "calificacion_final"."operador_id" IS 'Identificador del usuario que capturó el registro';

SET search_path TO public;
-----------------------------------------------------------------------------
-- categoria_curso
-----------------------------------------------------------------------------

DROP TABLE "categoria_curso" CASCADE;


CREATE TABLE "categoria_curso"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"siglas" VARCHAR(2)  NOT NULL,
	"descripcion" TEXT  NOT NULL,
	"padre" INTEGER,
	"profundidad" INTEGER default 1 NOT NULL,
	"path" VARCHAR(255),
	"operador_id" INTEGER,
	"activa" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "nombre_categoria_ukey" UNIQUE ("nombre")
);

COMMENT ON TABLE "categoria_curso" IS '';


COMMENT ON COLUMN "categoria_curso"."nombre" IS 'Nombre de la categoria';

COMMENT ON COLUMN "categoria_curso"."siglas" IS 'Siglas de la categoria';

COMMENT ON COLUMN "categoria_curso"."descripcion" IS 'Descripcion de la categoria';

COMMENT ON COLUMN "categoria_curso"."padre" IS 'Padre de la subcategoria';

COMMENT ON COLUMN "categoria_curso"."profundidad" IS 'Profundidad de la categoria';

COMMENT ON COLUMN "categoria_curso"."path" IS 'Ruta de los padres';

COMMENT ON COLUMN "categoria_curso"."operador_id" IS 'Identificador del usuario que capturó el registro';

SET search_path TO public;
-----------------------------------------------------------------------------
-- curso
-----------------------------------------------------------------------------

DROP TABLE "curso" CASCADE;


CREATE TABLE "curso"
(
	"id" INT8  NOT NULL,
	"categoria_curso_id" INTEGER  NOT NULL,
	"clave" VARCHAR(20)  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"descripcion" TEXT  NOT NULL,
	"perfil_id" INTEGER  NOT NULL,
	"duracion" INTEGER  NOT NULL,
	"seriado" BOOLEAN default 'f',
	"ultima_verificacion" TIMESTAMP,
	"asociado_moodle" BOOLEAN default 'f',
	"registrado_moodle" BOOLEAN default 'f',
	"asociado_rs" BOOLEAN default 'f',
	"registrado_rs" BOOLEAN default 'f',
	"restricciones_socio" BOOLEAN default 'f',
	"restricciones_horario" BOOLEAN default 'f',
	"calificacion_minima" INTEGER,
	"publicado" BOOLEAN default 'f' NOT NULL,
	"operador_id" INTEGER,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "clave_curso_ukey" UNIQUE ("clave"),
	CONSTRAINT "nombre_curso_ukey" UNIQUE ("nombre")
);

COMMENT ON TABLE "curso" IS '';


COMMENT ON COLUMN "curso"."id" IS 'Identificador del curso';

COMMENT ON COLUMN "curso"."categoria_curso_id" IS 'Categoria del curso';

COMMENT ON COLUMN "curso"."clave" IS 'Clave del curso';

COMMENT ON COLUMN "curso"."nombre" IS 'Nombre del curso';

COMMENT ON COLUMN "curso"."descripcion" IS 'Descripcion del curso';

COMMENT ON COLUMN "curso"."perfil_id" IS 'Perfil de l facilitador';

COMMENT ON COLUMN "curso"."duracion" IS 'Duracion en horas del curso';

COMMENT ON COLUMN "curso"."seriado" IS 'Flag para saber si es seriado o no el curso';

COMMENT ON COLUMN "curso"."ultima_verificacion" IS 'Fecha de la ultima verificacion';

COMMENT ON COLUMN "curso"."asociado_moodle" IS 'Flag para saber si esta asociado a moodle';

COMMENT ON COLUMN "curso"."registrado_moodle" IS 'Flag para saber si se registro correctamente a moodle';

COMMENT ON COLUMN "curso"."asociado_rs" IS 'Flag para saber si esta asociado a rs';

COMMENT ON COLUMN "curso"."registrado_rs" IS 'Flag para saber si se registro correctamente a rs';

COMMENT ON COLUMN "curso"."restricciones_socio" IS 'Flag para saber si tiene restricciones de socio';

COMMENT ON COLUMN "curso"."restricciones_horario" IS 'Flag para saber si tiene restricciones de horario';

COMMENT ON COLUMN "curso"."calificacion_minima" IS 'Calificacion minima en curso anterior de serie o examen de diagnóstico';

COMMENT ON COLUMN "curso"."publicado" IS 'Flag para saber si publicado el curso';

COMMENT ON COLUMN "curso"."operador_id" IS 'Identificador del usuario que capturó el registro';

SET search_path TO public;
-----------------------------------------------------------------------------
-- curso_centro
-----------------------------------------------------------------------------

DROP TABLE "curso_centro" CASCADE;


CREATE TABLE "curso_centro"
(
	"curso_id" INT8  NOT NULL,
	"centro_id" INTEGER  NOT NULL,
	PRIMARY KEY ("curso_id","centro_id")
);

COMMENT ON TABLE "curso_centro" IS '';


COMMENT ON COLUMN "curso_centro"."curso_id" IS 'Identificador del curso';

COMMENT ON COLUMN "curso_centro"."centro_id" IS 'Identificador del centro';

SET search_path TO public;
-----------------------------------------------------------------------------
-- cursos_finalizados
-----------------------------------------------------------------------------

DROP TABLE "cursos_finalizados" CASCADE;


CREATE TABLE "cursos_finalizados"
(
	"id" INT8  NOT NULL,
	"curso_id" INT8  NOT NULL,
	"alumno_id" INT8  NOT NULL,
	"grupo_id" INT8  NOT NULL,
	"calificacion_id" INT8  NOT NULL,
	"recursado" BOOLEAN default 'f' NOT NULL,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "cursos_finalizados" IS '';


COMMENT ON COLUMN "cursos_finalizados"."id" IS 'Identificador de la calificacion';

COMMENT ON COLUMN "cursos_finalizados"."curso_id" IS 'Identificador del grupo';

COMMENT ON COLUMN "cursos_finalizados"."alumno_id" IS 'Identificador del alumno';

COMMENT ON COLUMN "cursos_finalizados"."grupo_id" IS 'Identificador del curso';

COMMENT ON COLUMN "cursos_finalizados"."calificacion_id" IS 'Identificador de la calificacion final';

SET search_path TO public;
-----------------------------------------------------------------------------
-- dias_no_laborables
-----------------------------------------------------------------------------

DROP TABLE "dias_no_laborables" CASCADE;


CREATE TABLE "dias_no_laborables"
(
	"id" INT8  NOT NULL,
	"dia" TIMESTAMP  NOT NULL,
	"descripcion" TEXT,
	"operador_id" INTEGER,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "dias_no_laborables" IS '';


COMMENT ON COLUMN "dias_no_laborables"."id" IS 'Identificador del dia no laborable';

COMMENT ON COLUMN "dias_no_laborables"."dia" IS 'Fecha no laborable';

COMMENT ON COLUMN "dias_no_laborables"."descripcion" IS 'Descripcion de porque no es dia laborable';

COMMENT ON COLUMN "dias_no_laborables"."operador_id" IS 'Identificador del usuario que capturó el registro';

SET search_path TO public;
-----------------------------------------------------------------------------
-- evaluacion_parcial
-----------------------------------------------------------------------------

DROP TABLE "evaluacion_parcial" CASCADE;


CREATE TABLE "evaluacion_parcial"
(
	"id" INT8  NOT NULL,
	"curso_id" INT8  NOT NULL,
	"tema_id" INT8  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"descripcion" TEXT  NOT NULL,
	"calificacion_minima" NUMERIC(4,2),
	"calificacion_maxima" NUMERIC(4,2),
	"porcentaje" INTEGER  NOT NULL,
	"activo" BOOLEAN default 't',
	"operador_id" INTEGER,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "evaluacion_parcial" IS '';


COMMENT ON COLUMN "evaluacion_parcial"."id" IS 'Identificador de la calificacion';

COMMENT ON COLUMN "evaluacion_parcial"."curso_id" IS 'Identificador del grupo';

COMMENT ON COLUMN "evaluacion_parcial"."tema_id" IS 'Identificador del tema';

COMMENT ON COLUMN "evaluacion_parcial"."nombre" IS 'nobre de la evaluacion';

COMMENT ON COLUMN "evaluacion_parcial"."descripcion" IS 'Descripcion de la evaluacion';

COMMENT ON COLUMN "evaluacion_parcial"."calificacion_minima" IS 'Calificacion minima para la evaluacion';

COMMENT ON COLUMN "evaluacion_parcial"."calificacion_maxima" IS 'Calificacion maxima para la evaluacion';

COMMENT ON COLUMN "evaluacion_parcial"."porcentaje" IS 'Porcentaje del total de la evaluacion';

COMMENT ON COLUMN "evaluacion_parcial"."operador_id" IS 'Identificador del usuario que capturó el registro';

SET search_path TO public;
-----------------------------------------------------------------------------
-- evaluacion_final
-----------------------------------------------------------------------------

DROP TABLE "evaluacion_final" CASCADE;


CREATE TABLE "evaluacion_final"
(
	"id" INT8  NOT NULL,
	"curso_id" INT8  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"descripcion" TEXT  NOT NULL,
	"calificacion_minima" NUMERIC(4,2),
	"calificacion_maxima" NUMERIC(4,2),
	"activo" BOOLEAN default 't',
	"operador_id" INTEGER,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "evaluacion_final" IS '';


COMMENT ON COLUMN "evaluacion_final"."id" IS 'Identificador de la calificacion';

COMMENT ON COLUMN "evaluacion_final"."curso_id" IS 'Identificador del grupo';

COMMENT ON COLUMN "evaluacion_final"."nombre" IS 'nobre de la evaluacion';

COMMENT ON COLUMN "evaluacion_final"."descripcion" IS 'Descripcion de la evaluacion';

COMMENT ON COLUMN "evaluacion_final"."calificacion_minima" IS 'Calificacion minima para la evaluacion';

COMMENT ON COLUMN "evaluacion_final"."calificacion_maxima" IS 'Calificacion maxima para la evaluacion';

COMMENT ON COLUMN "evaluacion_final"."operador_id" IS 'Identificador del usuario que capturó el registro';

SET search_path TO public;
-----------------------------------------------------------------------------
-- fechas_horario
-----------------------------------------------------------------------------

DROP TABLE "fechas_horario" CASCADE;


CREATE TABLE "fechas_horario"
(
	"id" INT8  NOT NULL,
	"horario_id" INT8,
	"fecha" TIMESTAMP  NOT NULL,
	"hora_inicio" TIME  NOT NULL,
	"hora_fin" TIME  NOT NULL,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "fechas_horario" IS '';


COMMENT ON COLUMN "fechas_horario"."id" IS 'Identificador de la fecha de un horario';

COMMENT ON COLUMN "fechas_horario"."horario_id" IS 'Identificador del horario';

COMMENT ON COLUMN "fechas_horario"."fecha" IS 'Dia en el que se debe dar una sesion';

COMMENT ON COLUMN "fechas_horario"."hora_inicio" IS 'Hora de comienzo de la sesion';

COMMENT ON COLUMN "fechas_horario"."hora_fin" IS 'Hora en la que finaliza de la sesion';

SET search_path TO public;
-----------------------------------------------------------------------------
-- grupo
-----------------------------------------------------------------------------

DROP TABLE "grupo" CASCADE;


CREATE TABLE "grupo"
(
	"id" INT8  NOT NULL,
	"centro_id" INTEGER  NOT NULL,
	"curso_id" INT8  NOT NULL,
	"aula_id" INT8  NOT NULL,
	"horario_id" INT8  NOT NULL,
	"facilitador_id" INTEGER,
	"clave" VARCHAR(28)  NOT NULL,
	"cupo_total" INTEGER default 0 NOT NULL,
	"cupo_actual" INTEGER default 0 NOT NULL,
	"cupo_alcanzado" BOOLEAN default 'f' NOT NULL,
	"registrado_moodle" BOOLEAN default 'f' NOT NULL,
	"registrado_rs" BOOLEAN default 'f' NOT NULL,
	"duracion" INTEGER  NOT NULL,
	"duracion_perdida" INTEGER default 0 NOT NULL,
	"operador_id" INTEGER,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "grupo" IS '';


COMMENT ON COLUMN "grupo"."id" IS 'Identificador del grupo';

COMMENT ON COLUMN "grupo"."centro_id" IS 'Identificador del centro';

COMMENT ON COLUMN "grupo"."curso_id" IS 'Identificador del curso';

COMMENT ON COLUMN "grupo"."aula_id" IS 'Identificador del aula';

COMMENT ON COLUMN "grupo"."horario_id" IS 'Identificador del horario';

COMMENT ON COLUMN "grupo"."facilitador_id" IS 'Facilitador asignado para el curso';

COMMENT ON COLUMN "grupo"."clave" IS 'Calve del grupo';

COMMENT ON COLUMN "grupo"."cupo_total" IS 'Cupo total del grupo';

COMMENT ON COLUMN "grupo"."cupo_actual" IS 'Cupo actual del grupo';

COMMENT ON COLUMN "grupo"."cupo_alcanzado" IS 'Flag para saber si se alcanzo el cupo del curso';

COMMENT ON COLUMN "grupo"."registrado_moodle" IS 'Flag para saber si se registro correctamente en moodle';

COMMENT ON COLUMN "grupo"."registrado_rs" IS 'Flag para saber si se registro correctamente en RS';

COMMENT ON COLUMN "grupo"."duracion" IS 'Duracion en horas del curso';

COMMENT ON COLUMN "grupo"."duracion_perdida" IS 'Duracion en horas perdidas del curso';

COMMENT ON COLUMN "grupo"."operador_id" IS 'Identificador del usuario que capturó el registro';

SET search_path TO public;
-----------------------------------------------------------------------------
-- grupo_alumnos
-----------------------------------------------------------------------------

DROP TABLE "grupo_alumnos" CASCADE;


CREATE TABLE "grupo_alumnos"
(
	"id" INT8  NOT NULL,
	"grupo_id" INT8  NOT NULL,
	"alumno_id" INT8  NOT NULL,
	"preinscrito" BOOLEAN default 't',
	"fecha_preinscrito" TIMESTAMP  NOT NULL,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "grupo_alumnos" IS '';


COMMENT ON COLUMN "grupo_alumnos"."preinscrito" IS 'Flag para saber si se alcanzo el cupo del curso';

COMMENT ON COLUMN "grupo_alumnos"."fecha_preinscrito" IS 'Fecha de preinscripcion';

SET search_path TO public;
-----------------------------------------------------------------------------
-- horario
-----------------------------------------------------------------------------

DROP TABLE "horario" CASCADE;


CREATE TABLE "horario"
(
	"id" INT8  NOT NULL,
	"centro_id" INTEGER  NOT NULL,
	"curso_id" INT8  NOT NULL,
	"aula_id" INT8  NOT NULL,
	"fecha_inicio" TIMESTAMP  NOT NULL,
	"fecha_fin" TIMESTAMP  NOT NULL,
	"lunes" BOOLEAN default 'f' NOT NULL,
	"hi_lu" TIME,
	"hf_lu" TIME,
	"martes" BOOLEAN default 'f' NOT NULL,
	"hi_ma" TIME,
	"hf_ma" TIME,
	"miercoles" BOOLEAN default 'f' NOT NULL,
	"hi_mi" TIME,
	"hf_mi" TIME,
	"jueves" BOOLEAN default 'f' NOT NULL,
	"hi_ju" TIME,
	"hf_ju" TIME,
	"viernes" BOOLEAN default 'f' NOT NULL,
	"hi_vi" TIME,
	"hf_vi" TIME,
	"sabado" BOOLEAN default 'f' NOT NULL,
	"hi_sa" TIME,
	"hf_sa" TIME,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "horario" IS '';


COMMENT ON COLUMN "horario"."id" IS 'Identificador del horario';

COMMENT ON COLUMN "horario"."centro_id" IS 'Identificador del centro';

COMMENT ON COLUMN "horario"."curso_id" IS 'Identificador del curso';

COMMENT ON COLUMN "horario"."aula_id" IS 'Identificador del aula';

COMMENT ON COLUMN "horario"."fecha_inicio" IS 'Fecha de inicio del curso';

COMMENT ON COLUMN "horario"."fecha_fin" IS 'Fecha de fin del curso';

COMMENT ON COLUMN "horario"."lunes" IS 'Flag para saber si el curos se imparte los lunes';

COMMENT ON COLUMN "horario"."hi_lu" IS 'Hora de de inicio lunes';

COMMENT ON COLUMN "horario"."hf_lu" IS 'Hora de de fin lunes';

COMMENT ON COLUMN "horario"."martes" IS 'Flag para saber si el curos se imparte los martes';

COMMENT ON COLUMN "horario"."hi_ma" IS 'Hora de de inicio martes';

COMMENT ON COLUMN "horario"."hf_ma" IS 'Hora de de fin martes';

COMMENT ON COLUMN "horario"."miercoles" IS 'Flag para saber si el curos se imparte los miercoles';

COMMENT ON COLUMN "horario"."hi_mi" IS 'Hora de de inicio miercoles';

COMMENT ON COLUMN "horario"."hf_mi" IS 'Hora de de fin miercoles';

COMMENT ON COLUMN "horario"."jueves" IS 'Flag para saber si el curos se imparte los jueves';

COMMENT ON COLUMN "horario"."hi_ju" IS 'Hora de de inicio jueves';

COMMENT ON COLUMN "horario"."hf_ju" IS 'Hora de de fin jueves';

COMMENT ON COLUMN "horario"."viernes" IS 'Flag para saber si el curos se imparte los viernes';

COMMENT ON COLUMN "horario"."hi_vi" IS 'Hora de de inicio viernes';

COMMENT ON COLUMN "horario"."hf_vi" IS 'Hora de de fin viernes';

COMMENT ON COLUMN "horario"."sabado" IS 'Flag para saber si el curos se imparte los sabados';

COMMENT ON COLUMN "horario"."hi_sa" IS 'Hora de de inicio sabado';

COMMENT ON COLUMN "horario"."hf_sa" IS 'Hora de de fin sabado';

SET search_path TO public;
-----------------------------------------------------------------------------
-- lista_asistencia
-----------------------------------------------------------------------------

DROP TABLE "lista_asistencia" CASCADE;


CREATE TABLE "lista_asistencia"
(
	"id" bigserial  NOT NULL,
	"grupo_id" INTEGER  NOT NULL,
	"curso_id" INT8  NOT NULL,
	"alumno_id" INT8  NOT NULL,
	"fecha_id" INT8  NOT NULL,
	"asistencia" BOOLEAN default 'f' NOT NULL,
	"operador_id" INTEGER,
	"fecha_tomada" TIMESTAMP,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "lista_asistencia" IS '';


COMMENT ON COLUMN "lista_asistencia"."operador_id" IS 'Identificador del usuario que capturó el registro';

COMMENT ON COLUMN "lista_asistencia"."fecha_tomada" IS 'Primer fecha en que se tomo la asistencia';

SET search_path TO public;
-----------------------------------------------------------------------------
-- perfil_facilitador
-----------------------------------------------------------------------------

DROP TABLE "perfil_facilitador" CASCADE;


CREATE TABLE "perfil_facilitador"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"operador_id" INTEGER,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "nombre_ukey" UNIQUE ("nombre")
);

COMMENT ON TABLE "perfil_facilitador" IS '';


COMMENT ON COLUMN "perfil_facilitador"."nombre" IS 'Nombre del perfil';

COMMENT ON COLUMN "perfil_facilitador"."operador_id" IS 'Identificador del usuario que capturó el registro';

SET search_path TO public;
-----------------------------------------------------------------------------
-- restricciones_horario
-----------------------------------------------------------------------------

DROP TABLE "restricciones_horario" CASCADE;


CREATE TABLE "restricciones_horario"
(
	"id" INT8  NOT NULL,
	"curso_id" INT8  NOT NULL,
	"hora_inicio" TIME,
	"hora_fin" TIME,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "restricciones_horario" IS '';


COMMENT ON COLUMN "restricciones_horario"."id" IS 'Identificador de la restriccion';

COMMENT ON COLUMN "restricciones_horario"."curso_id" IS 'Identificador del curso';

COMMENT ON COLUMN "restricciones_horario"."hora_inicio" IS 'Hora minima de inicio';

COMMENT ON COLUMN "restricciones_horario"."hora_fin" IS 'Hora maxima de fin';

SET search_path TO public;
-----------------------------------------------------------------------------
-- restricciones_socio
-----------------------------------------------------------------------------

DROP TABLE "restricciones_socio" CASCADE;


CREATE TABLE "restricciones_socio"
(
	"id" INT8  NOT NULL,
	"curso_id" INT8  NOT NULL,
	"edad_minima" INTEGER,
	"edad_maxima" INTEGER,
	"sexo" CHAR(1),
	"ocupacion_id" INTEGER,
	"estudia" BOOLEAN,
	"nivel_estudio_id" INTEGER,
	"habilidad_informatica_id" INTEGER,
	"dominio_ingles_id" INTEGER,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "restricciones_socio" IS '';


COMMENT ON COLUMN "restricciones_socio"."id" IS 'Identificador del grupo';

COMMENT ON COLUMN "restricciones_socio"."curso_id" IS 'Identificador del curso';

COMMENT ON COLUMN "restricciones_socio"."edad_minima" IS 'Edad minima permitida';

COMMENT ON COLUMN "restricciones_socio"."edad_maxima" IS 'Edad maxima permitida';

COMMENT ON COLUMN "restricciones_socio"."sexo" IS 'Sexo del socio';

COMMENT ON COLUMN "restricciones_socio"."ocupacion_id" IS 'Identificador de laa ocupadion';

COMMENT ON COLUMN "restricciones_socio"."estudia" IS 'Flag para determinar que es necesario que estudi el socio';

COMMENT ON COLUMN "restricciones_socio"."nivel_estudio_id" IS 'Identificador del ultimo nivel de estudios';

COMMENT ON COLUMN "restricciones_socio"."habilidad_informatica_id" IS 'Identificador de la habilidad informatica';

COMMENT ON COLUMN "restricciones_socio"."dominio_ingles_id" IS 'Identificador del dominio de ingles';

SET search_path TO public;
-----------------------------------------------------------------------------
-- ruta_aprendizaje
-----------------------------------------------------------------------------

DROP TABLE "ruta_aprendizaje" CASCADE;


CREATE TABLE "ruta_aprendizaje"
(
	"curso_id" INT8  NOT NULL,
	"padre_id" INT8  NOT NULL,
	"id" serial  NOT NULL,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "ruta_aprendizaje" IS '';


COMMENT ON COLUMN "ruta_aprendizaje"."curso_id" IS 'Identificador del curso';

COMMENT ON COLUMN "ruta_aprendizaje"."padre_id" IS 'Identificador del padre del curso';

SET search_path TO public;
-----------------------------------------------------------------------------
-- tema
-----------------------------------------------------------------------------

DROP TABLE "tema" CASCADE;


CREATE TABLE "tema"
(
	"id" INT8  NOT NULL,
	"curso_id" INT8  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"numero" INTEGER,
	"descripcion" TEXT  NOT NULL,
	"activo" BOOLEAN default 't',
	"operador_id" INTEGER,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "tema" IS '';


COMMENT ON COLUMN "tema"."id" IS 'Identificador del tema';

COMMENT ON COLUMN "tema"."operador_id" IS 'Identificador del usuario que capturó el registro';

SET search_path TO public;
-----------------------------------------------------------------------------
-- usuario_perfil
-----------------------------------------------------------------------------

DROP TABLE "usuario_perfil" CASCADE;


CREATE TABLE "usuario_perfil"
(
	"usuario_id" INTEGER  NOT NULL,
	"perfil_id" INTEGER  NOT NULL,
	PRIMARY KEY ("usuario_id","perfil_id")
);

COMMENT ON TABLE "usuario_perfil" IS '';


COMMENT ON COLUMN "usuario_perfil"."usuario_id" IS 'Identificador del Facilitador';

COMMENT ON COLUMN "usuario_perfil"."perfil_id" IS 'Identificador del perfil';

SET search_path TO public;
-----------------------------------------------------------------------------
-- vacaciones
-----------------------------------------------------------------------------

DROP TABLE "vacaciones" CASCADE;


CREATE TABLE "vacaciones"
(
	"id" INT8  NOT NULL,
	"usuario_id" INTEGER,
	"fecha_inicio" DATE  NOT NULL,
	"fecha_fin" DATE  NOT NULL,
	"operador_id" INTEGER,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "vacaciones" IS '';


COMMENT ON COLUMN "vacaciones"."id" IS 'Identificador de las vacaciones';

COMMENT ON COLUMN "vacaciones"."usuario_id" IS 'Identificador del usuario que toma las vacaciones';

COMMENT ON COLUMN "vacaciones"."fecha_inicio" IS 'Fecha de inicio del curso';

COMMENT ON COLUMN "vacaciones"."fecha_fin" IS 'Fecha de fin del curso';

COMMENT ON COLUMN "vacaciones"."operador_id" IS 'Identificador del usuario que capturó el registro';

SET search_path TO public;
-----------------------------------------------------------------------------
-- categoria_metas_fijas
-----------------------------------------------------------------------------

DROP TABLE "categoria_metas_fijas" CASCADE;


CREATE TABLE "categoria_metas_fijas"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "categoria_metas_fijas_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "categoria_metas_fijas" IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- metas
-----------------------------------------------------------------------------

DROP TABLE "metas" CASCADE;


CREATE TABLE "metas"
(
	"id" INT8  NOT NULL,
	"centro_id" INTEGER,
	"categoria_fija_id" INTEGER,
	"categoria_producto_id" INT8,
	"categoria_beca_id" INTEGER,
	"fecha_inicio" DATE  NOT NULL,
	"fecha_fin" DATE,
	"semana" INTEGER  NOT NULL,
	"anio" INTEGER  NOT NULL,
	"meta" NUMERIC(12)  NOT NULL,
	"entero" BOOLEAN default 't',
	"usuario_id" INTEGER,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "uk_metas_fijas" UNIQUE ("centro_id","categoria_fija_id","semana","anio"),
	CONSTRAINT "uk_metas_productos" UNIQUE ("centro_id","categoria_producto_id","semana","anio"),
	CONSTRAINT "uk_metas_becas" UNIQUE ("centro_id","categoria_beca_id","semana","anio")
);

COMMENT ON TABLE "metas" IS '';


COMMENT ON COLUMN "metas"."id" IS 'Identificador de meta';

COMMENT ON COLUMN "metas"."centro_id" IS 'Id de Centro.';

COMMENT ON COLUMN "metas"."categoria_fija_id" IS 'Categoria de meta Fija (Ej. Meta de socios, meta de inscritos, etc).';

COMMENT ON COLUMN "metas"."categoria_producto_id" IS 'Categoria de meta de producto(Ej. Meta de gansitos, meta de Cursos de ingles, etc).';

COMMENT ON COLUMN "metas"."categoria_beca_id" IS 'Categoria de meta de Beca(Ej. Expedición RIA etc).';

COMMENT ON COLUMN "metas"."fecha_inicio" IS 'Fecha de inicio en que aplica la meta';

COMMENT ON COLUMN "metas"."fecha_fin" IS 'Fecha de fin en que aplica la meta';

COMMENT ON COLUMN "metas"."semana" IS 'Semana del año en que aplica la meta';

COMMENT ON COLUMN "metas"."anio" IS 'Año en que aplica la meta';

COMMENT ON COLUMN "metas"."meta" IS 'Cantidad (numerica o monetaria)';

COMMENT ON COLUMN "metas"."entero" IS 'Indica si la meta es entero o moneda (1 = entero)';

COMMENT ON COLUMN "metas"."usuario_id" IS 'Identificador del usuario que capturó el registro';

SET search_path TO public;
-----------------------------------------------------------------------------
-- relfixes
-----------------------------------------------------------------------------

DROP TABLE "relfixes" CASCADE;


CREATE TABLE "relfixes"
(
	"socio_id" INT8  NOT NULL,
	"centro_id" INTEGER,
	"usuario" VARCHAR(255)  NOT NULL,
	"fecha_alta" TIMESTAMP,
	"uidnumber" INTEGER,
	"uidnumber_new" INTEGER,
	PRIMARY KEY ("socio_id"),
	CONSTRAINT "relfixes_U_1" UNIQUE ("usuario")
);

COMMENT ON TABLE "relfixes" IS '';


COMMENT ON COLUMN "relfixes"."socio_id" IS 'Identificador del socio';

COMMENT ON COLUMN "relfixes"."centro_id" IS 'Identificador del centro donde se realiza la operacion';

COMMENT ON COLUMN "relfixes"."usuario" IS 'Nombre de usuario del socio';

COMMENT ON COLUMN "relfixes"."fecha_alta" IS 'Identificador de la fecha en que se dio de alta el socio';

COMMENT ON COLUMN "relfixes"."uidnumber" IS 'uidNumber actual';

COMMENT ON COLUMN "relfixes"."uidnumber_new" IS 'uidNumber nuevo (corregido)';

SET search_path TO public;
-----------------------------------------------------------------------------
-- inventario_operacion
-----------------------------------------------------------------------------

DROP TABLE "inventario_operacion" CASCADE;


CREATE TABLE "inventario_operacion"
(
	"id" serial  NOT NULL,
	"operacion" VARCHAR(30),
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "inventario_operacion" IS '';


COMMENT ON COLUMN "inventario_operacion"."operacion" IS 'Identificador de operacion';

SET search_path TO public;
-----------------------------------------------------------------------------
-- inventario
-----------------------------------------------------------------------------

DROP TABLE "inventario" CASCADE;


CREATE TABLE "inventario"
(
	"id" serial  NOT NULL,
	"operacion_id" INTEGER,
	"producto_id" INTEGER  NOT NULL,
	"cantidad" INT8  NOT NULL,
	"centro_id" INTEGER  NOT NULL,
	"motivo" TEXT,
	"created_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "inventario" IS '';


COMMENT ON COLUMN "inventario"."operacion_id" IS 'Identificador de operacion';

COMMENT ON COLUMN "inventario"."producto_id" IS 'Identificador de producto';

COMMENT ON COLUMN "inventario"."cantidad" IS 'Cantidad (entrada o salida)';

COMMENT ON COLUMN "inventario"."centro_id" IS 'Identificador de centro';

COMMENT ON COLUMN "inventario"."motivo" IS 'Motivo salida';

SET search_path TO public;
-----------------------------------------------------------------------------
-- inventario_conteo
-----------------------------------------------------------------------------

DROP TABLE "inventario_conteo" CASCADE;


CREATE TABLE "inventario_conteo"
(
	"id" serial  NOT NULL,
	"corte_id" INTEGER,
	"operacion_id" INTEGER,
	"producto_id" INTEGER  NOT NULL,
	"cantidad_final" INT8  NOT NULL,
	"conteo1" INT8  NOT NULL,
	"conteo2" INT8  NOT NULL,
	"centro_id" INTEGER  NOT NULL,
	"created_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "inventario_conteo" IS '';


COMMENT ON COLUMN "inventario_conteo"."corte_id" IS 'Identificador de corte';

COMMENT ON COLUMN "inventario_conteo"."operacion_id" IS 'Identificador de operacion';

COMMENT ON COLUMN "inventario_conteo"."producto_id" IS 'Identificador de producto';

COMMENT ON COLUMN "inventario_conteo"."cantidad_final" IS 'Cantidad inicial';

COMMENT ON COLUMN "inventario_conteo"."conteo1" IS 'Conteo fisico 1';

COMMENT ON COLUMN "inventario_conteo"."conteo2" IS 'Conteo fisico 2';

COMMENT ON COLUMN "inventario_conteo"."centro_id" IS 'Identificador de centro';

SET search_path TO public;
-----------------------------------------------------------------------------
-- inventario_corte
-----------------------------------------------------------------------------

DROP TABLE "inventario_corte" CASCADE;


CREATE TABLE "inventario_corte"
(
	"id" serial  NOT NULL,
	"operacion_id" INTEGER,
	"estado" BOOLEAN default 't',
	"centro_id" INTEGER  NOT NULL,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "inventario_corte" IS '';


COMMENT ON COLUMN "inventario_corte"."operacion_id" IS 'Identificador de operacion';

COMMENT ON COLUMN "inventario_corte"."estado" IS 'Estado 1: abierto, Estado 0: cerrado';

COMMENT ON COLUMN "inventario_corte"."centro_id" IS 'Identificador de centro';

SET search_path TO public;
-----------------------------------------------------------------------------
-- tipo_sesion
-----------------------------------------------------------------------------

DROP TABLE "tipo_sesion" CASCADE;


CREATE TABLE "tipo_sesion"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(90)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "tipo_sesion" IS '';


COMMENT ON COLUMN "tipo_sesion"."nombre" IS 'Nombre del tipo de sesión, DEFAULT, SIN CONSUMO DE SALDO, MULTISESION, MULTISESION SIN CONSUMO DE SALDO.';

SET search_path TO public;
-----------------------------------------------------------------------------
-- sesion
-----------------------------------------------------------------------------

DROP TABLE "sesion" CASCADE;


CREATE TABLE "sesion"
(
	"id" INT8  NOT NULL,
	"centro_id" INTEGER,
	"computadora_id" INT8,
	"ip" VARCHAR(15)  NOT NULL,
	"socio_id" INT8,
	"usuario" VARCHAR(90)  NOT NULL,
	"fecha_login" TIMESTAMP  NOT NULL,
	"tipo_id" INTEGER,
	"saldo" INTEGER default 0,
	PRIMARY KEY ("id"),
	CONSTRAINT "uk_sesion" UNIQUE ("centro_id","computadora_id")
);

COMMENT ON TABLE "sesion" IS '';


COMMENT ON COLUMN "sesion"."id" IS 'Identificador de sesión';

COMMENT ON COLUMN "sesion"."centro_id" IS 'Identificador de centro';

COMMENT ON COLUMN "sesion"."computadora_id" IS 'Identificador de PC';

COMMENT ON COLUMN "sesion"."ip" IS 'IP de la PC';

COMMENT ON COLUMN "sesion"."socio_id" IS 'Identificador del socio que hace la compra';

COMMENT ON COLUMN "sesion"."usuario" IS 'Nombre de usuario usado en el login del sistema';

COMMENT ON COLUMN "sesion"."fecha_login" IS 'Timestamp del evento login del sistema';

COMMENT ON COLUMN "sesion"."tipo_id" IS 'Identificador del tipo de sesión que se esta ejecutando.';

COMMENT ON COLUMN "sesion"."saldo" IS 'Saldo de tiempo en segundos del usuario cuando entró a la sesión';

SET search_path TO public;
-----------------------------------------------------------------------------
-- sesion_historico
-----------------------------------------------------------------------------

DROP TABLE "sesion_historico" CASCADE;


CREATE TABLE "sesion_historico"
(
	"id" INT8  NOT NULL,
	"centro_id" INTEGER,
	"computadora_id" INT8,
	"ip" VARCHAR(15)  NOT NULL,
	"socio_id" INT8,
	"usuario" VARCHAR(90)  NOT NULL,
	"fecha_login" TIMESTAMP  NOT NULL,
	"fecha_logout" TIMESTAMP  NOT NULL,
	"saldo_inicial" INTEGER default 0,
	"saldo_final" INTEGER default 0,
	"tipo_id" INTEGER,
	"ocupados" INTEGER default 0,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "sesion_historico" IS '';


COMMENT ON COLUMN "sesion_historico"."id" IS 'Identificador de sesión';

COMMENT ON COLUMN "sesion_historico"."centro_id" IS 'Identificador de centro';

COMMENT ON COLUMN "sesion_historico"."computadora_id" IS 'Identificador de PC';

COMMENT ON COLUMN "sesion_historico"."ip" IS 'IP de la PC';

COMMENT ON COLUMN "sesion_historico"."socio_id" IS 'Identificador del socio que hace la compra';

COMMENT ON COLUMN "sesion_historico"."usuario" IS 'Nombre de usuario usado en el login del sistema';

COMMENT ON COLUMN "sesion_historico"."fecha_login" IS 'Timestamp del evento login del sistema';

COMMENT ON COLUMN "sesion_historico"."fecha_logout" IS 'Timestamp del evento logout del sistema';

COMMENT ON COLUMN "sesion_historico"."saldo_inicial" IS 'Saldo de tiempo en segundos del usuario cuando entró a la sesión';

COMMENT ON COLUMN "sesion_historico"."saldo_final" IS 'Saldo de tiempo en segundos del usuario cuando salió de la sesión';

COMMENT ON COLUMN "sesion_historico"."tipo_id" IS 'Identificador del tipo de sesión que se esta ejecutando.';

COMMENT ON COLUMN "sesion_historico"."ocupados" IS 'Saldo de tiempo en segundos que ocupó el socio';

SET search_path TO public;
-----------------------------------------------------------------------------
-- sesion_operador
-----------------------------------------------------------------------------

DROP TABLE "sesion_operador" CASCADE;


CREATE TABLE "sesion_operador"
(
	"id" INT8  NOT NULL,
	"centro_id" INTEGER,
	"ip" VARCHAR(15)  NOT NULL,
	"usuario_id" INTEGER,
	"usuario" VARCHAR(90)  NOT NULL,
	"fecha_login" TIMESTAMP  NOT NULL,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "sesion_operador" IS '';


COMMENT ON COLUMN "sesion_operador"."id" IS 'Identificador de sesión';

COMMENT ON COLUMN "sesion_operador"."centro_id" IS 'Identificador de centro';

COMMENT ON COLUMN "sesion_operador"."ip" IS 'IP de la PC';

COMMENT ON COLUMN "sesion_operador"."usuario_id" IS 'Identificador del operador';

COMMENT ON COLUMN "sesion_operador"."usuario" IS 'Nombre de usuario usado en el login del sistema';

COMMENT ON COLUMN "sesion_operador"."fecha_login" IS 'Timestamp del evento login del sistema';

SET search_path TO public;
-----------------------------------------------------------------------------
-- sesion_operador_historico
-----------------------------------------------------------------------------

DROP TABLE "sesion_operador_historico" CASCADE;


CREATE TABLE "sesion_operador_historico"
(
	"id" INT8  NOT NULL,
	"centro_id" INTEGER,
	"ip" VARCHAR(15)  NOT NULL,
	"usuario_id" INTEGER,
	"usuario" VARCHAR(90)  NOT NULL,
	"fecha_login" TIMESTAMP  NOT NULL,
	"fecha_logout" TIMESTAMP  NOT NULL,
	"transcurridos" INTEGER default 0,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "sesion_operador_historico" IS '';


COMMENT ON COLUMN "sesion_operador_historico"."id" IS 'Identificador de sesión';

COMMENT ON COLUMN "sesion_operador_historico"."centro_id" IS 'Identificador de centro';

COMMENT ON COLUMN "sesion_operador_historico"."ip" IS 'IP de la PC';

COMMENT ON COLUMN "sesion_operador_historico"."usuario_id" IS 'Identificador del operador';

COMMENT ON COLUMN "sesion_operador_historico"."usuario" IS 'Nombre de usuario usado en el login del sistema';

COMMENT ON COLUMN "sesion_operador_historico"."fecha_login" IS 'Timestamp del evento login del sistema';

COMMENT ON COLUMN "sesion_operador_historico"."fecha_logout" IS 'Timestamp del evento logout del sistema';

COMMENT ON COLUMN "sesion_operador_historico"."transcurridos" IS 'Tiempo en segundos que ocupó el usuario';

SET search_path TO public;
-----------------------------------------------------------------------------
-- sesion_mac
-----------------------------------------------------------------------------

DROP TABLE "sesion_mac" CASCADE;


CREATE TABLE "sesion_mac"
(
	"ip" VARCHAR(17)  NOT NULL,
	"ip_host" VARCHAR(15),
	"ip_virtual" VARCHAR(15)  NOT NULL,
	PRIMARY KEY ("ip"),
	CONSTRAINT "sesion_mac_U_1" UNIQUE ("ip_host"),
	CONSTRAINT "sesion_mac_U_2" UNIQUE ("ip_virtual")
);

COMMENT ON TABLE "sesion_mac" IS '';


COMMENT ON COLUMN "sesion_mac"."ip" IS 'mac la PC';

COMMENT ON COLUMN "sesion_mac"."ip_host" IS 'IP de la PC real';

COMMENT ON COLUMN "sesion_mac"."ip_virtual" IS 'IP de la PC virtual';

SET search_path TO public;
-----------------------------------------------------------------------------
-- categoria_reporte
-----------------------------------------------------------------------------

DROP TABLE "categoria_reporte" CASCADE;


CREATE TABLE "categoria_reporte"
(
	"id" INT8  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "categoria_reporte_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "categoria_reporte" IS '';


COMMENT ON COLUMN "categoria_reporte"."id" IS 'Identificador de categoria de reporte';

COMMENT ON COLUMN "categoria_reporte"."nombre" IS 'Nombre de la beca';

COMMENT ON COLUMN "categoria_reporte"."activo" IS 'Bandera de vigencia';

SET search_path TO public;
-----------------------------------------------------------------------------
-- categoria_reporte_producto
-----------------------------------------------------------------------------

DROP TABLE "categoria_reporte_producto" CASCADE;


CREATE TABLE "categoria_reporte_producto"
(
	"categoria_id" INT8  NOT NULL,
	"producto_id" INTEGER  NOT NULL,
	PRIMARY KEY ("categoria_id","producto_id")
);

COMMENT ON TABLE "categoria_reporte_producto" IS '';


COMMENT ON COLUMN "categoria_reporte_producto"."categoria_id" IS 'Identificador de la categoria';

COMMENT ON COLUMN "categoria_reporte_producto"."producto_id" IS 'Identificador del producto que entra en la categoria';

SET search_path TO public;
-----------------------------------------------------------------------------
-- seccion
-----------------------------------------------------------------------------

DROP TABLE "seccion" CASCADE;


CREATE TABLE "seccion"
(
	"id" INT8  NOT NULL,
	"centro_id" INTEGER,
	"nombre" VARCHAR(255)  NOT NULL,
	"en_dashboard" BOOLEAN default 't',
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "seccion" IS '';


COMMENT ON COLUMN "seccion"."id" IS 'Identificador de sección';

COMMENT ON COLUMN "seccion"."centro_id" IS 'Identificador de centro';

COMMENT ON COLUMN "seccion"."en_dashboard" IS 'Bandera que indica si aparecerá en dashboard esta sección';

SET search_path TO public;
-----------------------------------------------------------------------------
-- tipo_pc
-----------------------------------------------------------------------------

DROP TABLE "tipo_pc" CASCADE;


CREATE TABLE "tipo_pc"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "tipo_pc_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "tipo_pc" IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- computadora
-----------------------------------------------------------------------------

DROP TABLE "computadora" CASCADE;


CREATE TABLE "computadora"
(
	"id" INT8  NOT NULL,
	"centro_id" INTEGER,
	"seccion_id" INT8,
	"ip" VARCHAR(15)  NOT NULL,
	"hostname" VARCHAR(90),
	"mac_adress" VARCHAR(17),
	"alias" VARCHAR(150),
	"tipo_id" INTEGER,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "computadora" IS '';


COMMENT ON COLUMN "computadora"."id" IS 'Identificador de PC';

COMMENT ON COLUMN "computadora"."centro_id" IS 'Identificador de centro';

COMMENT ON COLUMN "computadora"."seccion_id" IS 'Identificador de seccion';

COMMENT ON COLUMN "computadora"."ip" IS 'IP de la PC';

COMMENT ON COLUMN "computadora"."hostname" IS 'Hostname de la PC, usado para obligar a un usuario acceder solo desde este hostname.';

COMMENT ON COLUMN "computadora"."mac_adress" IS 'Mac de la PC';

COMMENT ON COLUMN "computadora"."alias" IS 'Alias de la pc en el dashboard';

COMMENT ON COLUMN "computadora"."tipo_id" IS 'Identificador del tipo de pc, recepción, operación, apartado, etc.';

SET search_path TO public;
-----------------------------------------------------------------------------
-- canal_contacto
-----------------------------------------------------------------------------

DROP TABLE "canal_contacto" CASCADE;


CREATE TABLE "canal_contacto"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "canal_contacto_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "canal_contacto" IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- recomienda
-----------------------------------------------------------------------------

DROP TABLE "recomienda" CASCADE;


CREATE TABLE "recomienda"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "recomienda_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "recomienda" IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- discapacidad
-----------------------------------------------------------------------------

DROP TABLE "discapacidad" CASCADE;


CREATE TABLE "discapacidad"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "discapacidad_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "discapacidad" IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- habilidad_informatica
-----------------------------------------------------------------------------

DROP TABLE "habilidad_informatica" CASCADE;


CREATE TABLE "habilidad_informatica"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "habilidad_informatica_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "habilidad_informatica" IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- dominio_ingles
-----------------------------------------------------------------------------

DROP TABLE "dominio_ingles" CASCADE;


CREATE TABLE "dominio_ingles"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "dominio_ingles_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "dominio_ingles" IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- nivel_estudio
-----------------------------------------------------------------------------

DROP TABLE "nivel_estudio" CASCADE;


CREATE TABLE "nivel_estudio"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "nivel_estudio_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "nivel_estudio" IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- profesion
-----------------------------------------------------------------------------

DROP TABLE "profesion" CASCADE;


CREATE TABLE "profesion"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "profesion_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "profesion" IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- estudio
-----------------------------------------------------------------------------

DROP TABLE "estudio" CASCADE;


CREATE TABLE "estudio"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "estudio_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "estudio" IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- tipo_escuela
-----------------------------------------------------------------------------

DROP TABLE "tipo_escuela" CASCADE;


CREATE TABLE "tipo_escuela"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "tipo_escuela_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "tipo_escuela" IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- tipo_estudio
-----------------------------------------------------------------------------

DROP TABLE "tipo_estudio" CASCADE;


CREATE TABLE "tipo_estudio"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "tipo_estudio_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "tipo_estudio" IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- grado_estudio
-----------------------------------------------------------------------------

DROP TABLE "grado_estudio" CASCADE;


CREATE TABLE "grado_estudio"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "grado_estudio_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "grado_estudio" IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- tipo_organizacion
-----------------------------------------------------------------------------

DROP TABLE "tipo_organizacion" CASCADE;


CREATE TABLE "tipo_organizacion"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "tipo_organizacion_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "tipo_organizacion" IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- sector
-----------------------------------------------------------------------------

DROP TABLE "sector" CASCADE;


CREATE TABLE "sector"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "sector_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "sector" IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- posicion
-----------------------------------------------------------------------------

DROP TABLE "posicion" CASCADE;


CREATE TABLE "posicion"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "posicion_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "posicion" IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- ocupacion
-----------------------------------------------------------------------------

DROP TABLE "ocupacion" CASCADE;


CREATE TABLE "ocupacion"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "ocupacion_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "ocupacion" IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- entidad_federativa
-----------------------------------------------------------------------------

DROP TABLE "entidad_federativa" CASCADE;


CREATE TABLE "entidad_federativa"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "entidad_federativa_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "entidad_federativa" IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- delegacion
-----------------------------------------------------------------------------

DROP TABLE "delegacion" CASCADE;


CREATE TABLE "delegacion"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "delegacion_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "delegacion" IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- parentesco
-----------------------------------------------------------------------------

DROP TABLE "parentesco" CASCADE;


CREATE TABLE "parentesco"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "parentesco_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "parentesco" IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- estado_civil
-----------------------------------------------------------------------------

DROP TABLE "estado_civil" CASCADE;


CREATE TABLE "estado_civil"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "estado_civil_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "estado_civil" IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- servicio_salud
-----------------------------------------------------------------------------

DROP TABLE "servicio_salud" CASCADE;


CREATE TABLE "servicio_salud"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "servicio_salud_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "servicio_salud" IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- vivienda_tipo
-----------------------------------------------------------------------------

DROP TABLE "vivienda_tipo" CASCADE;


CREATE TABLE "vivienda_tipo"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "vivienda_tipo_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "vivienda_tipo" IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- vivienda_es
-----------------------------------------------------------------------------

DROP TABLE "vivienda_es" CASCADE;


CREATE TABLE "vivienda_es"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "vivienda_es_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "vivienda_es" IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- material_piso
-----------------------------------------------------------------------------

DROP TABLE "material_piso" CASCADE;


CREATE TABLE "material_piso"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "material_piso_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "material_piso" IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- material_techo
-----------------------------------------------------------------------------

DROP TABLE "material_techo" CASCADE;


CREATE TABLE "material_techo"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "material_techo_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "material_techo" IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- uso_automovil
-----------------------------------------------------------------------------

DROP TABLE "uso_automovil" CASCADE;


CREATE TABLE "uso_automovil"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "uso_automovil_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "uso_automovil" IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- tipo_empresa
-----------------------------------------------------------------------------

DROP TABLE "tipo_empresa" CASCADE;


CREATE TABLE "tipo_empresa"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "tipo_empresa_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "tipo_empresa" IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- empresa
-----------------------------------------------------------------------------

DROP TABLE "empresa" CASCADE;


CREATE TABLE "empresa"
(
	"id" INT8  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"razon_social" VARCHAR(120),
	"tipo_id" INTEGER,
	"id_estado" INTEGER,
	"estado" VARCHAR(30),
	"ciudad" VARCHAR(60),
	"id_del" INTEGER,
	"municipio" VARCHAR(60),
	"id_municipio" INTEGER,
	"colonia" VARCHAR(60),
	"id_asentamiento" VARCHAR(60),
	"cp" CHAR(5),
	"calle" VARCHAR(60),
	"num_ext" VARCHAR(30),
	"num_int" VARCHAR(30),
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "empresa_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "empresa" IS '';


COMMENT ON COLUMN "empresa"."id" IS 'Identificador de empresa';

COMMENT ON COLUMN "empresa"."tipo_id" IS 'Identificador de tipo de empresa';

COMMENT ON COLUMN "empresa"."id_estado" IS 'Identificador de estado';

SET search_path TO public;
-----------------------------------------------------------------------------
-- centro
-----------------------------------------------------------------------------

DROP TABLE "centro" CASCADE;


CREATE TABLE "centro"
(
	"id" INTEGER  NOT NULL,
	"ip" VARCHAR(15)  NOT NULL,
	"hostname" TEXT  NOT NULL,
	"nombre" TEXT,
	"alias" VARCHAR(50),
	"razon_social" VARCHAR(254),
	"estado" VARCHAR(30),
	"municipio" VARCHAR(60),
	"colonia" VARCHAR(60),
	"cp" VARCHAR(5),
	"calle" VARCHAR(60),
	"num_ext" VARCHAR(30),
	"num_int" VARCHAR(30),
	"lat" VARCHAR(30),
	"long" VARCHAR(30),
	"dirgmaps" TEXT,
	"iva" DECIMAL(10,3) default 16,
	"rfc" VARCHAR(20),
	"desde" DATE,
	"hora_apertura" TIME,
	"hora_cierre" TIME,
	"pcs_usuario" INTEGER,
	"pcs_operacion" INTEGER,
	"key_gmaps" TEXT,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "centro" IS '';


COMMENT ON COLUMN "centro"."id" IS 'Identificador de centro';

COMMENT ON COLUMN "centro"."ip" IS 'Ip del servidor del centro';

COMMENT ON COLUMN "centro"."hostname" IS 'FQDN';

COMMENT ON COLUMN "centro"."alias" IS 'Alias del nombre del centro';

COMMENT ON COLUMN "centro"."razon_social" IS 'Razon social con q opera el centro.';

COMMENT ON COLUMN "centro"."dirgmaps" IS 'Dirección detectada por gmaps.';

COMMENT ON COLUMN "centro"."iva" IS 'IVA aplicable a la venta en este centro.';

COMMENT ON COLUMN "centro"."rfc" IS 'RFC bajo el que opera el centro';

COMMENT ON COLUMN "centro"."desde" IS 'Fecha de inicio de operacion';

COMMENT ON COLUMN "centro"."hora_apertura" IS 'Hora en q abre el centro';

COMMENT ON COLUMN "centro"."hora_cierre" IS 'Hora en q cierra el centro';

COMMENT ON COLUMN "centro"."pcs_usuario" IS 'Numero de pcs de usuario con las q cuenta el centro.';

COMMENT ON COLUMN "centro"."pcs_operacion" IS 'Pcs con las que cuenta el centro para operacion';

COMMENT ON COLUMN "centro"."key_gmaps" IS 'llave del api para usar gmaps en este centro.';

SET search_path TO public;
-----------------------------------------------------------------------------
-- rol
-----------------------------------------------------------------------------

DROP TABLE "rol" CASCADE;


CREATE TABLE "rol"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "rol_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "rol" IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- usuario
-----------------------------------------------------------------------------

DROP TABLE "usuario" CASCADE;


CREATE TABLE "usuario"
(
	"id" INTEGER  NOT NULL,
	"rol_id" INTEGER,
	"usuario" VARCHAR(90)  NOT NULL,
	"clave" VARCHAR(30)  NOT NULL,
	"nombre" VARCHAR(30)  NOT NULL,
	"apepat" VARCHAR(60)  NOT NULL,
	"apemat" VARCHAR(60),
	"nombre_completo" VARCHAR(200)  NOT NULL,
	"email" VARCHAR(60),
	"operador_id" INTEGER,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "usuario_U_1" UNIQUE ("usuario")
);

COMMENT ON TABLE "usuario" IS '';


COMMENT ON COLUMN "usuario"."id" IS 'Identificador de usuario';

COMMENT ON COLUMN "usuario"."rol_id" IS 'Identificador de rol';

COMMENT ON COLUMN "usuario"."usuario" IS 'Nombre de usuario usado en el login del sistema';

COMMENT ON COLUMN "usuario"."clave" IS 'Clave usada para autenticar usuario';

COMMENT ON COLUMN "usuario"."nombre" IS 'Nombre de la persona que se va a afiliar';

COMMENT ON COLUMN "usuario"."apepat" IS 'Apellido paterno de la persona';

COMMENT ON COLUMN "usuario"."apemat" IS 'Apellido materno de la persona';

COMMENT ON COLUMN "usuario"."nombre_completo" IS 'Nombre completo (nombre, apepat, apemat). Para busquedas.';

COMMENT ON COLUMN "usuario"."operador_id" IS 'Identificador del usuario que capturó el registro';

SET search_path TO public;
-----------------------------------------------------------------------------
-- usuario_centro
-----------------------------------------------------------------------------

DROP TABLE "usuario_centro" CASCADE;


CREATE TABLE "usuario_centro"
(
	"usuario_id" INTEGER  NOT NULL,
	"centro_id" INTEGER  NOT NULL,
	PRIMARY KEY ("usuario_id","centro_id")
);

COMMENT ON TABLE "usuario_centro" IS '';


COMMENT ON COLUMN "usuario_centro"."usuario_id" IS 'Identificador de usuario';

COMMENT ON COLUMN "usuario_centro"."centro_id" IS 'Identificador de centro';

SET search_path TO public;
-----------------------------------------------------------------------------
-- socio
-----------------------------------------------------------------------------

DROP TABLE "socio" CASCADE;


CREATE TABLE "socio"
(
	"id" INT8  NOT NULL,
	"centro_id" INTEGER,
	"rol_id" INTEGER,
	"usuario" VARCHAR(90)  NOT NULL,
	"clave" VARCHAR(30)  NOT NULL,
	"folio" VARCHAR(50)  NOT NULL,
	"nombre" VARCHAR(30)  NOT NULL,
	"apepat" VARCHAR(60)  NOT NULL,
	"apemat" VARCHAR(60),
	"nombre_completo" VARCHAR(200),
	"fecha_nac" DATE  NOT NULL,
	"sexo" CHAR(1)  NOT NULL,
	"foto" VARCHAR(50),
	"huella" TEXT,
	"email" VARCHAR(60),
	"tel" VARCHAR(60),
	"celular" VARCHAR(60),
	"id_estado" INTEGER,
	"estado" VARCHAR(30),
	"ciudad" VARCHAR(60),
	"id_del" INTEGER,
	"municipio" VARCHAR(60),
	"id_municipio" INTEGER,
	"colonia" VARCHAR(60),
	"id_asentamiento" VARCHAR(60),
	"cp" CHAR(5),
	"calle" VARCHAR(60),
	"num_ext" VARCHAR(30),
	"num_int" VARCHAR(30),
	"lat" VARCHAR(30),
	"long" VARCHAR(30),
	"dirgmaps" TEXT,
	"razon_social" VARCHAR(120),
	"rfc" VARCHAR(13),
	"telefono_fiscal" VARCHAR(60),
	"calle_fiscal" TEXT,
	"colonia_fiscal" VARCHAR(60),
	"cp_fiscal" VARCHAR(5),
	"ciudad_fiscal" VARCHAR(120),
	"estado_fiscal" VARCHAR(60),
	"municipio_fiscal" VARCHAR(150),
	"num_ext_fiscal" VARCHAR(50),
	"num_int_fiscal" VARCHAR(50),
	"pais_fiscal" VARCHAR(100) default 'MEXICO',
	"empresa" INTEGER,
	"canal_contacto_id" INTEGER,
	"liga" VARCHAR(120),
	"recomienda_id" INTEGER,
	"otros_contacto" VARCHAR(200),
	"socio_ref_id" INT8,
	"promotor_ref_id" INTEGER,
	"discapacidad_id" INTEGER,
	"habilidad_informatica_id" INTEGER,
	"dominio_ingles_id" INTEGER,
	"estudia" BOOLEAN default 't',
	"nivel_id" INTEGER,
	"profesion_actual_id" INTEGER,
	"estudio_actual_id" INTEGER,
	"otro_estudio_actual" VARCHAR(200),
	"tipo_escuela_id" INTEGER,
	"beca" BOOLEAN default 'f',
	"tipo_estudio_id" INTEGER,
	"grado_id" INTEGER,
	"profesion_ultimo_id" INTEGER,
	"estudio_ultimo_id" INTEGER,
	"otro_estudio_ultimo" VARCHAR(200),
	"trabaja" BOOLEAN default 'f',
	"organizacion_id" INTEGER,
	"sector_id" INTEGER,
	"posicion_id" INTEGER,
	"ocupacion_id" INTEGER,
	"parentesco_id" INTEGER,
	"er_nombre" VARCHAR(30),
	"er_apepat" VARCHAR(30),
	"er_apemat" VARCHAR(30),
	"er_fecha_nac" DATE,
	"er_sexo" CHAR(1),
	"er_email" VARCHAR(60),
	"er_tel" VARCHAR(60),
	"er_celular" VARCHAR(60),
	"observaciones" TEXT,
	"saldo" INTEGER default 0,
	"vigente" BOOLEAN default 't',
	"operador_id" INTEGER,
	"tipo_sesion_id" INTEGER default 1,
	"activo" BOOLEAN default 't',
	"ldap_uid" INTEGER default 0,
	"benef_oportunidades" BOOLEAN default 'f',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	"curp" VARCHAR(18),
	"estado_civil_id" INTEGER,
	"personas_vivienda" INTEGER default 0,
	"servicio_salud_id" INTEGER,
	"vivienda_tipo_id" INTEGER,
	"vivienda_es_id" INTEGER,
	"material_techo_id" INTEGER,
	"material_piso_id" INTEGER,
	"habitaciones" INTEGER default 0,
	"banos" INTEGER default 0,
	"focos" INTEGER default 0,
	"anios_residencia" INTEGER default 0,
	"meses_residencia" INTEGER default 0,
	"energia_electrica" BOOLEAN default 'f',
	"agua_entubada" BOOLEAN default 'f',
	"drenaje" BOOLEAN default 'f',
	"linea_telefonica" BOOLEAN default 'f',
	"television_paga" BOOLEAN default 'f',
	"internet" BOOLEAN default 'f',
	"lavadora" BOOLEAN default 'f',
	"secadora" BOOLEAN default 'f',
	"television" BOOLEAN default 'f',
	"calentador_agua" BOOLEAN default 'f',
	"horno_microondas" BOOLEAN default 'f',
	"tostador" BOOLEAN default 'f',
	"reproductor_video" BOOLEAN default 'f',
	"computadora" BOOLEAN default 'f',
	"telefono_celular" BOOLEAN default 'f',
	"reproductor_audio" BOOLEAN default 'f',
	"fregadero" BOOLEAN default 'f',
	"estufa" BOOLEAN default 'f',
	"refrigerador" BOOLEAN default 'f',
	"licuadora" BOOLEAN default 'f',
	"automoviles" INTEGER default 0,
	"automovil_propio" BOOLEAN default 'f',
	"uso_automovil_id" INTEGER,
	"socio_madre_id" INT8,
	"socio_madre_vive" BOOLEAN default 't',
	"socio_padre_id" INT8,
	"socio_padre_vive" BOOLEAN default 't',
	PRIMARY KEY ("id"),
	CONSTRAINT "socio_U_1" UNIQUE ("usuario")
);

COMMENT ON TABLE "socio" IS '';


COMMENT ON COLUMN "socio"."id" IS 'Identificador de usuario';

COMMENT ON COLUMN "socio"."centro_id" IS 'Identificador de usuario';

COMMENT ON COLUMN "socio"."rol_id" IS 'Identificador de rol , default 7 para socio';

COMMENT ON COLUMN "socio"."usuario" IS 'Nombre de usuario usado en el login del sistema';

COMMENT ON COLUMN "socio"."clave" IS 'Clave usada para autenticar usuario';

COMMENT ON COLUMN "socio"."folio" IS 'Numero de folio de la credencial pre-impresa';

COMMENT ON COLUMN "socio"."nombre" IS 'Nombre de la persona que se va a afiliar';

COMMENT ON COLUMN "socio"."apepat" IS 'Apellido paterno de la persona';

COMMENT ON COLUMN "socio"."apemat" IS 'Apellido materno de la persona';

COMMENT ON COLUMN "socio"."nombre_completo" IS 'Nombre completo (nombre, apepat, apemat). Para busquedas.';

COMMENT ON COLUMN "socio"."fecha_nac" IS 'Fecha nacimiento';

COMMENT ON COLUMN "socio"."sexo" IS 'Sexo de la persona';

COMMENT ON COLUMN "socio"."huella" IS 'Huella del indice del usuario';

COMMENT ON COLUMN "socio"."id_estado" IS 'Identificador de estado';

COMMENT ON COLUMN "socio"."dirgmaps" IS 'Dirección detectada por gmaps.';

COMMENT ON COLUMN "socio"."municipio_fiscal" IS 'Municipio fiscal';

COMMENT ON COLUMN "socio"."num_ext_fiscal" IS 'Numero exterior direccion fiscal';

COMMENT ON COLUMN "socio"."num_int_fiscal" IS 'Numero interior de direccion fiscal';

COMMENT ON COLUMN "socio"."pais_fiscal" IS 'Pais de la direccion fiscal';

COMMENT ON COLUMN "socio"."er_nombre" IS 'Nombre del contacto en caso de emergencia';

COMMENT ON COLUMN "socio"."er_apepat" IS 'Apellido paterno del contacto en caso de emergencia';

COMMENT ON COLUMN "socio"."er_apemat" IS 'Apellido materno del contacto en caso de emergencia';

COMMENT ON COLUMN "socio"."er_fecha_nac" IS 'Fecha nacimiento del contacto en caso de emergencia';

COMMENT ON COLUMN "socio"."er_sexo" IS 'Sexo del contacto en caso de emergencia';

COMMENT ON COLUMN "socio"."er_email" IS 'Email del contacto en caso de emergencia';

COMMENT ON COLUMN "socio"."er_tel" IS 'Teléfono del contacto en caso de emergencia';

COMMENT ON COLUMN "socio"."er_celular" IS 'Celular del contacto en caso de emergencia';

COMMENT ON COLUMN "socio"."observaciones" IS 'Campo de observaciones';

COMMENT ON COLUMN "socio"."saldo" IS 'Saldo de tiempo en segundos del usuario';

COMMENT ON COLUMN "socio"."vigente" IS 'Bandera de vigencia del usuario';

COMMENT ON COLUMN "socio"."operador_id" IS 'Identificador del usuario que capturó el registro';

COMMENT ON COLUMN "socio"."tipo_sesion_id" IS 'Identificador del tipo de sesión que se creará cuando se loguee el usuario.';

COMMENT ON COLUMN "socio"."ldap_uid" IS 'El uid que asigna el LDAP';

COMMENT ON COLUMN "socio"."benef_oportunidades" IS 'Bandera que indica si es beneficiario o no del programa oportunidades del gob.';

COMMENT ON COLUMN "socio"."estado_civil_id" IS 'Estado civil.';

COMMENT ON COLUMN "socio"."personas_vivienda" IS 'Número de personas con las que comparte vivienda.';

COMMENT ON COLUMN "socio"."servicio_salud_id" IS 'Servicio de salud de la que es beneficiario.';

COMMENT ON COLUMN "socio"."vivienda_tipo_id" IS 'Tipo de vivienda.';

COMMENT ON COLUMN "socio"."vivienda_es_id" IS 'La vivienda que habita es.';

COMMENT ON COLUMN "socio"."material_techo_id" IS 'Material del techo de la vivienda.';

COMMENT ON COLUMN "socio"."material_piso_id" IS 'Material del piso de la vivienda.';

COMMENT ON COLUMN "socio"."habitaciones" IS 'Número de habitaciones.';

COMMENT ON COLUMN "socio"."banos" IS 'Número de baños.';

COMMENT ON COLUMN "socio"."focos" IS 'Número de focos.';

COMMENT ON COLUMN "socio"."anios_residencia" IS 'Años de residir en el domicilio.';

COMMENT ON COLUMN "socio"."meses_residencia" IS 'Meses de residir en el domicilio.';

COMMENT ON COLUMN "socio"."energia_electrica" IS 'Servicios con que cuenta la vivienda. Energía electrica.';

COMMENT ON COLUMN "socio"."agua_entubada" IS 'Servicios con que cuenta la vivienda. Agua entubada.';

COMMENT ON COLUMN "socio"."drenaje" IS 'Servicios con que cuenta la vivienda. Drenaje.';

COMMENT ON COLUMN "socio"."linea_telefonica" IS 'Servicios con que cuenta la vivienda. Linea Telefonica.';

COMMENT ON COLUMN "socio"."television_paga" IS 'Servicios con que cuenta la vivienda. Televisión de paga.';

COMMENT ON COLUMN "socio"."internet" IS 'Servicios con que cuenta la vivienda. Internet.';

COMMENT ON COLUMN "socio"."lavadora" IS 'Enseres. Lavadora.';

COMMENT ON COLUMN "socio"."secadora" IS 'Enseres. Secadora.';

COMMENT ON COLUMN "socio"."television" IS 'Enseres. Televisión.';

COMMENT ON COLUMN "socio"."calentador_agua" IS 'Enseres. Calentador de agua.';

COMMENT ON COLUMN "socio"."horno_microondas" IS 'Enseres. Horno Microondas.';

COMMENT ON COLUMN "socio"."tostador" IS 'Enseres. Tostador de pan.';

COMMENT ON COLUMN "socio"."reproductor_video" IS 'Enseres. Reproductor de video.';

COMMENT ON COLUMN "socio"."computadora" IS 'Enseres. Computadora.';

COMMENT ON COLUMN "socio"."telefono_celular" IS 'Enseres. Teléfono celular.';

COMMENT ON COLUMN "socio"."reproductor_audio" IS 'Enseres. Reproductor de audio.';

COMMENT ON COLUMN "socio"."fregadero" IS 'Enseres. Fregadero con agua.';

COMMENT ON COLUMN "socio"."estufa" IS 'Enseres. Estufa con horno.';

COMMENT ON COLUMN "socio"."refrigerador" IS 'Enseres. Refrigerador.';

COMMENT ON COLUMN "socio"."licuadora" IS 'Enseres. Licuadora.';

COMMENT ON COLUMN "socio"."automoviles" IS 'Cuántos automóviles tienen en casa.';

COMMENT ON COLUMN "socio"."automovil_propio" IS '¿Tiene automóvil propio?.';

COMMENT ON COLUMN "socio"."uso_automovil_id" IS 'Uso que le da al automóvil.';

COMMENT ON COLUMN "socio"."socio_madre_vive" IS 'Vive (s/n).';

COMMENT ON COLUMN "socio"."socio_padre_vive" IS 'Vive (s/n).';

SET search_path TO public;
-----------------------------------------------------------------------------
-- aceptacion_terminos
-----------------------------------------------------------------------------

DROP TABLE "aceptacion_terminos" CASCADE;


CREATE TABLE "aceptacion_terminos"
(
	"socio_id" INT8  NOT NULL,
	"ip" VARCHAR(15)  NOT NULL,
	"created_at" TIMESTAMP,
	PRIMARY KEY ("socio_id")
);

COMMENT ON TABLE "aceptacion_terminos" IS '';


COMMENT ON COLUMN "aceptacion_terminos"."ip" IS 'Ip de la pc cliente donde se firma el socio.';

SET search_path TO public;
-----------------------------------------------------------------------------
-- categoria_producto
-----------------------------------------------------------------------------

DROP TABLE "categoria_producto" CASCADE;


CREATE TABLE "categoria_producto"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "categoria_producto_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "categoria_producto" IS '';


COMMENT ON COLUMN "categoria_producto"."nombre" IS 'Nombre de la categoría';

COMMENT ON COLUMN "categoria_producto"."activo" IS 'Bandera de vigencia';

SET search_path TO public;
-----------------------------------------------------------------------------
-- tipo_producto
-----------------------------------------------------------------------------

DROP TABLE "tipo_producto" CASCADE;


CREATE TABLE "tipo_producto"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "tipo_producto_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "tipo_producto" IS '';


COMMENT ON COLUMN "tipo_producto"."nombre" IS 'Nombre del tipo de producto';

COMMENT ON COLUMN "tipo_producto"."activo" IS 'Bandera de vigencia';

SET search_path TO public;
-----------------------------------------------------------------------------
-- unidad_producto
-----------------------------------------------------------------------------

DROP TABLE "unidad_producto" CASCADE;


CREATE TABLE "unidad_producto"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "unidad_producto_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "unidad_producto" IS '';


COMMENT ON COLUMN "unidad_producto"."nombre" IS 'Nombre de la unidad en la que está dado el producto';

COMMENT ON COLUMN "unidad_producto"."activo" IS 'Bandera de vigencia';

SET search_path TO public;
-----------------------------------------------------------------------------
-- producto
-----------------------------------------------------------------------------

DROP TABLE "producto" CASCADE;


CREATE TABLE "producto"
(
	"id" INTEGER  NOT NULL,
	"categoria_id" INTEGER  NOT NULL,
	"tipo_id" INTEGER  NOT NULL,
	"unidad_id" INTEGER  NOT NULL,
	"codigo" VARCHAR(250),
	"nombre" VARCHAR(250),
	"descripcion" TEXT,
	"precio_lista" DECIMAL(10,2) default 0,
	"tiempo" INTEGER,
	"venta_unitaria" BOOLEAN default 'f',
	"en_pv" BOOLEAN default 't',
	"es_subproducto" BOOLEAN default 'f',
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "producto" IS '';


COMMENT ON COLUMN "producto"."id" IS 'Identificador de producto';

COMMENT ON COLUMN "producto"."categoria_id" IS 'Identificador de categoría de producto';

COMMENT ON COLUMN "producto"."tipo_id" IS 'Identificador de tipo de prodcuto (servicio o prod, etc)';

COMMENT ON COLUMN "producto"."unidad_id" IS 'Identificador de unidad de producto';

COMMENT ON COLUMN "producto"."codigo" IS 'Código con que se reconoce al producto, ya sea EAN o cualquier otro';

COMMENT ON COLUMN "producto"."nombre" IS 'Nombre del producto';

COMMENT ON COLUMN "producto"."descripcion" IS 'Descripción del producto (podría incluir HTML)';

COMMENT ON COLUMN "producto"."precio_lista" IS 'Precio de lista del producto';

COMMENT ON COLUMN "producto"."tiempo" IS 'Tiempo que incluye el producto';

COMMENT ON COLUMN "producto"."venta_unitaria" IS 'Bandera que indica si sólo se puede vender una unidad por vez por usuario. (en una sola compra)';

COMMENT ON COLUMN "producto"."en_pv" IS 'Bandera que indica si el producto aparecerá en el listado de productos de punto de venta.';

COMMENT ON COLUMN "producto"."es_subproducto" IS 'Bandera que indica si el producto es subproducto de otro, hace que no salga como editable en el admin y depende del padre.';

COMMENT ON COLUMN "producto"."activo" IS 'Bandera de vigencia del producto';

SET search_path TO public;
-----------------------------------------------------------------------------
-- paquete
-----------------------------------------------------------------------------

DROP TABLE "paquete" CASCADE;


CREATE TABLE "paquete"
(
	"paquete_id" INTEGER  NOT NULL,
	"producto_id" INTEGER  NOT NULL,
	"cantidad" INTEGER default 1,
	"precio_paquete" DECIMAL(10,2) default 0,
	PRIMARY KEY ("paquete_id","producto_id")
);

COMMENT ON TABLE "paquete" IS '';


COMMENT ON COLUMN "paquete"."paquete_id" IS 'Identificador del producto principal que define el paquete';

COMMENT ON COLUMN "paquete"."producto_id" IS 'Identificador del producto que compone al paquete';

COMMENT ON COLUMN "paquete"."cantidad" IS 'Cantidad del producto que se agrega en el paquete';

COMMENT ON COLUMN "paquete"."precio_paquete" IS 'Precio que se asigna dentro de este paquete al producto relacionado';

SET search_path TO public;
-----------------------------------------------------------------------------
-- stock
-----------------------------------------------------------------------------

DROP TABLE "stock" CASCADE;


CREATE TABLE "stock"
(
	"centro_id" INTEGER  NOT NULL,
	"producto_id" INTEGER  NOT NULL,
	"existencia" INTEGER default 0,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("centro_id","producto_id")
);

COMMENT ON TABLE "stock" IS '';


COMMENT ON COLUMN "stock"."centro_id" IS 'Identificador de centro';

COMMENT ON COLUMN "stock"."producto_id" IS 'Identificador del producto que compone al paquete';

COMMENT ON COLUMN "stock"."existencia" IS 'Cantidad de producto actualmente';

COMMENT ON COLUMN "stock"."activo" IS 'Bandera de vigencia del producto';

SET search_path TO public;
-----------------------------------------------------------------------------
-- factura
-----------------------------------------------------------------------------

DROP TABLE "factura" CASCADE;


CREATE TABLE "factura"
(
	"id" INT8  NOT NULL,
	"centro_id" INTEGER,
	"folio_fiscal_id" INT8,
	"folio" INTEGER,
	"importe" DECIMAL(10,2) default 0,
	"subtotal" DECIMAL(10,2) default 0,
	"total" DECIMAL(10,2) default 0,
	"retencion" DECIMAL(10,2) default 0,
	"cadena_original" TEXT,
	"sello" TEXT,
	"fecha_factura" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "factura" IS '';


COMMENT ON COLUMN "factura"."id" IS 'Identificador de factura';

COMMENT ON COLUMN "factura"."centro_id" IS 'Identificador del centro donde se realiza la venta';

COMMENT ON COLUMN "factura"."folio_fiscal_id" IS 'Identificador de los folios fiscales con los que se genero la presente factura';

COMMENT ON COLUMN "factura"."folio" IS 'Folio de la factura';

COMMENT ON COLUMN "factura"."importe" IS 'Importe de la factura';

COMMENT ON COLUMN "factura"."subtotal" IS 'Subtotal de la factura';

COMMENT ON COLUMN "factura"."total" IS 'Total factura.';

COMMENT ON COLUMN "factura"."retencion" IS 'Monto retenido (IVA)';

COMMENT ON COLUMN "factura"."cadena_original" IS 'Cadena original de la factura';

COMMENT ON COLUMN "factura"."sello" IS 'Sello de la factura';

COMMENT ON COLUMN "factura"."fecha_factura" IS 'Fecha que contiene la factura';

SET search_path TO public;
-----------------------------------------------------------------------------
-- corte
-----------------------------------------------------------------------------

DROP TABLE "corte" CASCADE;


CREATE TABLE "corte"
(
	"id" INT8  NOT NULL,
	"centro_id" INTEGER,
	"folio_venta_inicial" INTEGER,
	"folio_venta_final" INTEGER,
	"fondo_inicial" DECIMAL(10,2) default 0,
	"fondo_caja" DECIMAL(10,2) default 0,
	"operador_id" INTEGER,
	"total_ventas" DECIMAL(10,2) default 0,
	"total_salidas" DECIMAL(10,2) default 0,
	"total_cancelaciones" DECIMAL(10,2) default 0,
	"total_vpg" DECIMAL(10,2) default 0,
	"total_facturas" DECIMAL(10,2) default 0,
	"total_conciliaciones" DECIMAL(10,2) default 0,
	"factura_id" INT8,
	"ip_caja" VARCHAR(15),
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "corte" IS '';


COMMENT ON COLUMN "corte"."id" IS 'Identificador de corte';

COMMENT ON COLUMN "corte"."centro_id" IS 'Identificador del centro donde se realiza la venta';

COMMENT ON COLUMN "corte"."folio_venta_inicial" IS 'Folio de la primera venta no cancelada en el corte';

COMMENT ON COLUMN "corte"."folio_venta_final" IS 'Folio de la ultima venta no cancelada en el corte';

COMMENT ON COLUMN "corte"."fondo_inicial" IS 'Fondo inicial en caja';

COMMENT ON COLUMN "corte"."fondo_caja" IS 'Fondo final en caja';

COMMENT ON COLUMN "corte"."operador_id" IS 'Identificador del usuario que realiza el corte';

COMMENT ON COLUMN "corte"."total_ventas" IS 'Total de venta del día';

COMMENT ON COLUMN "corte"."total_salidas" IS 'Total de salidas de dinero';

COMMENT ON COLUMN "corte"."total_cancelaciones" IS 'Total de cancelaciones en el día';

COMMENT ON COLUMN "corte"."total_vpg" IS 'Total de venta al público general';

COMMENT ON COLUMN "corte"."total_facturas" IS 'Total de venta al público general';

COMMENT ON COLUMN "corte"."total_conciliaciones" IS 'Total de conciliaciones';

COMMENT ON COLUMN "corte"."factura_id" IS 'Identificador de factura de venta al publico en general';

COMMENT ON COLUMN "corte"."ip_caja" IS 'IP de la máquina donde se realiza el corte de caja';

SET search_path TO public;
-----------------------------------------------------------------------------
-- estatus_orden
-----------------------------------------------------------------------------

DROP TABLE "estatus_orden" CASCADE;


CREATE TABLE "estatus_orden"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "estatus_orden_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "estatus_orden" IS '';


COMMENT ON COLUMN "estatus_orden"."nombre" IS 'Nombre del estatus de la orden, activo, cancelado, etc';

COMMENT ON COLUMN "estatus_orden"."activo" IS 'Bandera de vigencia';

SET search_path TO public;
-----------------------------------------------------------------------------
-- tipo_orden
-----------------------------------------------------------------------------

DROP TABLE "tipo_orden" CASCADE;


CREATE TABLE "tipo_orden"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "tipo_orden_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "tipo_orden" IS '';


COMMENT ON COLUMN "tipo_orden"."nombre" IS 'Nombre de la orden: Orden de Venta, Salida, Cancelación, etc';

COMMENT ON COLUMN "tipo_orden"."activo" IS 'Bandera de vigencia';

SET search_path TO public;
-----------------------------------------------------------------------------
-- forma_pago
-----------------------------------------------------------------------------

DROP TABLE "forma_pago" CASCADE;


CREATE TABLE "forma_pago"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "forma_pago_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "forma_pago" IS '';


COMMENT ON COLUMN "forma_pago"."nombre" IS 'Nombre de la forma de pago';

COMMENT ON COLUMN "forma_pago"."activo" IS 'Bandera de vigencia';

SET search_path TO public;
-----------------------------------------------------------------------------
-- orden
-----------------------------------------------------------------------------

DROP TABLE "orden" CASCADE;


CREATE TABLE "orden"
(
	"id" INT8  NOT NULL,
	"centro_id" INTEGER,
	"socio_id" INT8,
	"folio" INTEGER,
	"total" DECIMAL(10,2) default 0,
	"forma_pago_id" INTEGER default 1,
	"operador_id" INTEGER,
	"tipo_id" INTEGER,
	"ip_caja" VARCHAR(15),
	"es_factura" BOOLEAN default 'f',
	"razon_social" VARCHAR(256),
	"rfc" VARCHAR(13),
	"calle_fiscal" VARCHAR(256),
	"colonia_fiscal" VARCHAR(256),
	"cp_fiscal" VARCHAR(5),
	"estado_fiscal" VARCHAR(256),
	"municipio_fiscal" VARCHAR(256),
	"num_ext_fiscal" VARCHAR(60),
	"num_int_fiscal" VARCHAR(60),
	"pais_fiscal" VARCHAR(60) default 'MEXICO',
	"estatus_id" INTEGER default 1,
	"corte_id" INT8,
	"factura_id" INT8,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "orden" IS '';


COMMENT ON COLUMN "orden"."id" IS 'Identificador de orden';

COMMENT ON COLUMN "orden"."centro_id" IS 'Identificador del centro donde se realiza la venta';

COMMENT ON COLUMN "orden"."socio_id" IS 'Identificador del socio que hace la compra';

COMMENT ON COLUMN "orden"."folio" IS 'Folio consecutivo de venta';

COMMENT ON COLUMN "orden"."total" IS 'Total de la venta';

COMMENT ON COLUMN "orden"."forma_pago_id" IS 'Identificador de la forma en que se pagó la orden. (Efectivo, credito, etc)';

COMMENT ON COLUMN "orden"."operador_id" IS 'Identificador del usuario que realiza la orden';

COMMENT ON COLUMN "orden"."tipo_id" IS 'Identificador del tipo de orden';

COMMENT ON COLUMN "orden"."ip_caja" IS 'IP de la máquina donde se realiza la venta';

COMMENT ON COLUMN "orden"."es_factura" IS 'Bandera que indica si se expidió factura o ticket';

COMMENT ON COLUMN "orden"."razon_social" IS 'Nombre o razón social de la persona física o moral';

COMMENT ON COLUMN "orden"."rfc" IS 'RFC';

COMMENT ON COLUMN "orden"."calle_fiscal" IS 'Calle del domicilio fiscal';

COMMENT ON COLUMN "orden"."colonia_fiscal" IS 'Colonia del domicilio fiscal';

COMMENT ON COLUMN "orden"."cp_fiscal" IS 'Código postal del domicilio fiscal';

COMMENT ON COLUMN "orden"."estado_fiscal" IS 'Entidad federativa del domicilio fiscal';

COMMENT ON COLUMN "orden"."municipio_fiscal" IS 'Municipio o delegación del domicilio fiscal';

COMMENT ON COLUMN "orden"."num_ext_fiscal" IS 'Número exterior del domicilio fiscal';

COMMENT ON COLUMN "orden"."num_int_fiscal" IS 'Número interior del domicilio fiscal';

COMMENT ON COLUMN "orden"."pais_fiscal" IS 'País del domicilio fiscal';

COMMENT ON COLUMN "orden"."estatus_id" IS 'Identificador del estatus de la orden';

COMMENT ON COLUMN "orden"."corte_id" IS 'Bandera que indica si se expidió factura o ticket';

COMMENT ON COLUMN "orden"."factura_id" IS 'Identificador de factura';

SET search_path TO public;
-----------------------------------------------------------------------------
-- promocion
-----------------------------------------------------------------------------

DROP TABLE "promocion" CASCADE;


CREATE TABLE "promocion"
(
	"id" INT8  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"descripcion" TEXT  NOT NULL,
	"vigente_de" TIMESTAMP,
	"vigente_a" TIMESTAMP,
	"activo" BOOLEAN default 'f',
	"descuento" DECIMAL(10,6) default 0,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "promocion_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "promocion" IS '';


COMMENT ON COLUMN "promocion"."id" IS 'Identificador de la promoción';

COMMENT ON COLUMN "promocion"."nombre" IS 'Nombre de la promoción';

COMMENT ON COLUMN "promocion"."descripcion" IS 'descripción y detalles de la promoción';

COMMENT ON COLUMN "promocion"."vigente_de" IS 'Fecha en que entra en vigencia la promoción';

COMMENT ON COLUMN "promocion"."vigente_a" IS 'Fecha en que termina la vigencia de la promoción';

COMMENT ON COLUMN "promocion"."activo" IS 'Bandera de vigencia';

COMMENT ON COLUMN "promocion"."descuento" IS 'Descuento de la promoción';

SET search_path TO public;
-----------------------------------------------------------------------------
-- promocion_producto
-----------------------------------------------------------------------------

DROP TABLE "promocion_producto" CASCADE;


CREATE TABLE "promocion_producto"
(
	"promocion_id" INT8  NOT NULL,
	"producto_id" INTEGER  NOT NULL,
	PRIMARY KEY ("promocion_id","producto_id")
);

COMMENT ON TABLE "promocion_producto" IS '';


COMMENT ON COLUMN "promocion_producto"."promocion_id" IS 'Identificador de la promoción';

COMMENT ON COLUMN "promocion_producto"."producto_id" IS 'Identificador del producto que entra en la promoción';

SET search_path TO public;
-----------------------------------------------------------------------------
-- promocion_categoria
-----------------------------------------------------------------------------

DROP TABLE "promocion_categoria" CASCADE;


CREATE TABLE "promocion_categoria"
(
	"promocion_id" INT8  NOT NULL,
	"categoria_id" INTEGER  NOT NULL,
	PRIMARY KEY ("promocion_id","categoria_id")
);

COMMENT ON TABLE "promocion_categoria" IS '';


COMMENT ON COLUMN "promocion_categoria"."promocion_id" IS 'Identificador de la promoción';

COMMENT ON COLUMN "promocion_categoria"."categoria_id" IS 'Identificador de la categoría de productos que entran en la promoción';

SET search_path TO public;
-----------------------------------------------------------------------------
-- promocion_reglaci
-----------------------------------------------------------------------------

DROP TABLE "promocion_reglaci" CASCADE;


CREATE TABLE "promocion_reglaci"
(
	"id" INT8  NOT NULL,
	"promocion_id" INT8,
	"sexo" VARCHAR(1),
	"edad_de" INTEGER,
	"edad_a" INTEGER,
	"nivel_estudio" INTEGER,
	"recomendados" INTEGER,
	"centro_alta_id" INTEGER,
	"centro_venta_id" INTEGER,
	"municipio_centro" VARCHAR(256),
	"empresa_id" INT8,
	"discapacidad_id" INTEGER,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "promocion_reglaci" IS '';


COMMENT ON COLUMN "promocion_reglaci"."id" IS 'Identificador de la promoción';

COMMENT ON COLUMN "promocion_reglaci"."promocion_id" IS 'Identificador de la promoción';

COMMENT ON COLUMN "promocion_reglaci"."sexo" IS 'Sexo al que está dirigida la regla de descuento';

COMMENT ON COLUMN "promocion_reglaci"."edad_de" IS 'Inicio del rango de edad al que está dirigida la regla de descuento';

COMMENT ON COLUMN "promocion_reglaci"."edad_a" IS 'Fin del rango de edad al que está dirigida la regla de descuento';

COMMENT ON COLUMN "promocion_reglaci"."recomendados" IS 'Número de recomendados en su red.';

COMMENT ON COLUMN "promocion_reglaci"."centro_alta_id" IS 'Identificador del centro donde se dio de alta';

COMMENT ON COLUMN "promocion_reglaci"."centro_venta_id" IS 'Identificador del centro donde se realiza la venta';

COMMENT ON COLUMN "promocion_reglaci"."municipio_centro" IS 'Municipio del centro donde se realiza la venta';

COMMENT ON COLUMN "promocion_reglaci"."empresa_id" IS 'Identificador de la empresa referente';

COMMENT ON COLUMN "promocion_reglaci"."discapacidad_id" IS 'Identificador de salud del socio';

SET search_path TO public;
-----------------------------------------------------------------------------
-- promocion_regla
-----------------------------------------------------------------------------

DROP TABLE "promocion_regla" CASCADE;


CREATE TABLE "promocion_regla"
(
	"id" INT8  NOT NULL,
	"promocion_id" INT8,
	"sexo" VARCHAR(1),
	"edad_de" INTEGER,
	"edad_a" INTEGER,
	"nivel_estudio" INTEGER,
	"recomendados_de" INTEGER,
	"recomendados_a" INTEGER,
	"centro_alta_id" INTEGER,
	"centro_venta_id" INTEGER,
	"municipio" VARCHAR(256),
	"colonia" VARCHAR(256),
	"cp" VARCHAR(256),
	"discapacidad_id" INTEGER,
	"habilidad_informatica_id" INTEGER,
	"dominio_ingles_id" INTEGER,
	"profesion_id" INTEGER,
	"ocupacion_id" INTEGER,
	"posicion_id" INTEGER,
	"estudia" BOOLEAN,
	"trabaja" BOOLEAN,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "promocion_regla" IS '';


COMMENT ON COLUMN "promocion_regla"."id" IS 'Identificador de la promoción';

COMMENT ON COLUMN "promocion_regla"."promocion_id" IS 'Identificador de la promoción';

COMMENT ON COLUMN "promocion_regla"."sexo" IS 'Sexo al que está dirigida la regla de descuento';

COMMENT ON COLUMN "promocion_regla"."edad_de" IS 'Inicio del rango de edad al que está dirigida la regla de descuento';

COMMENT ON COLUMN "promocion_regla"."edad_a" IS 'Fin del rango de edad al que está dirigida la regla de descuento';

COMMENT ON COLUMN "promocion_regla"."recomendados_de" IS 'Número de recomendados en su red desde n socios.';

COMMENT ON COLUMN "promocion_regla"."recomendados_a" IS 'Número de recomendados en su red hasta n socios.';

COMMENT ON COLUMN "promocion_regla"."centro_alta_id" IS 'Identificador del centro donde se dio de alta';

COMMENT ON COLUMN "promocion_regla"."centro_venta_id" IS 'Identificador del centro donde se realiza la venta';

COMMENT ON COLUMN "promocion_regla"."municipio" IS 'Municipio domicilio del socio';

COMMENT ON COLUMN "promocion_regla"."colonia" IS 'Colonia municipio del socio';

COMMENT ON COLUMN "promocion_regla"."cp" IS 'CP domicilio del socio';

COMMENT ON COLUMN "promocion_regla"."discapacidad_id" IS 'Identificador de salud del socio';

COMMENT ON COLUMN "promocion_regla"."habilidad_informatica_id" IS 'Identificador de habilidad informática del socio';

COMMENT ON COLUMN "promocion_regla"."dominio_ingles_id" IS 'Identificador del dominio del ingles que tiene el socio';

COMMENT ON COLUMN "promocion_regla"."profesion_id" IS 'Identificador de la profesión del socio';

COMMENT ON COLUMN "promocion_regla"."ocupacion_id" IS 'Identificador de la ocupación del socio';

COMMENT ON COLUMN "promocion_regla"."posicion_id" IS 'Identificador de la posición laboral del socio';

COMMENT ON COLUMN "promocion_regla"."estudia" IS 'Bandera que indica si el socio estudia';

COMMENT ON COLUMN "promocion_regla"."trabaja" IS 'Bandera que indica si el socio trabaja';

SET search_path TO public;
-----------------------------------------------------------------------------
-- promocion_cupon
-----------------------------------------------------------------------------

DROP TABLE "promocion_cupon" CASCADE;


CREATE TABLE "promocion_cupon"
(
	"id" INT8  NOT NULL,
	"centro_id" INTEGER,
	"nombre" VARCHAR(256)  NOT NULL,
	"descripcion" TEXT  NOT NULL,
	"vigente_de" TIMESTAMP,
	"vigente_a" TIMESTAMP,
	"descuento" DECIMAL(10,6) default 0,
	"cantidad_cupones" INTEGER default 0,
	"creado_por" INTEGER,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "promocion_cupon" IS '';


COMMENT ON COLUMN "promocion_cupon"."id" IS 'Identificador de la promoción';

COMMENT ON COLUMN "promocion_cupon"."centro_id" IS 'Identificador del centro donde se realiza el cupón';

COMMENT ON COLUMN "promocion_cupon"."nombre" IS 'Nombre de la promoción';

COMMENT ON COLUMN "promocion_cupon"."descripcion" IS 'descripción y detalles de la promoción';

COMMENT ON COLUMN "promocion_cupon"."vigente_de" IS 'Fecha en que entra en vigencia la promoción';

COMMENT ON COLUMN "promocion_cupon"."vigente_a" IS 'Fecha en que termina la vigencia de la promoción';

COMMENT ON COLUMN "promocion_cupon"."descuento" IS 'Descuento de la promoción';

COMMENT ON COLUMN "promocion_cupon"."cantidad_cupones" IS 'Cantidad de cupones a generar en la promoción';

COMMENT ON COLUMN "promocion_cupon"."creado_por" IS 'Identificador del usuario que crea la promoción de cupones';

COMMENT ON COLUMN "promocion_cupon"."activo" IS 'Bandera de vigencia';

SET search_path TO public;
-----------------------------------------------------------------------------
-- cupon
-----------------------------------------------------------------------------

DROP TABLE "cupon" CASCADE;


CREATE TABLE "cupon"
(
	"id" INT8  NOT NULL,
	"promocion_cupon_id" INT8,
	"centro_id" INTEGER,
	"codigo" VARCHAR(16),
	"validador" INTEGER,
	"otorgado_a" INT8,
	"otorgado_por" INTEGER,
	"fecha_otorgado" TIMESTAMP,
	"razon_otorga" TEXT,
	"utilizado" BOOLEAN default 'f',
	"fecha_utilizado" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "cupon_U_1" UNIQUE ("codigo")
);

COMMENT ON TABLE "cupon" IS '';


COMMENT ON COLUMN "cupon"."id" IS 'Identificador de la promoción';

COMMENT ON COLUMN "cupon"."promocion_cupon_id" IS 'Identificador de la promoción';

COMMENT ON COLUMN "cupon"."centro_id" IS 'Identificador del centro donde se realiza el cupón';

COMMENT ON COLUMN "cupon"."codigo" IS 'Código con que se reconoce al producto, ya sea EAN o cualquier otro';

COMMENT ON COLUMN "cupon"."validador" IS 'Se usa como validador acompañado del código si es que se otorga impreso.';

COMMENT ON COLUMN "cupon"."otorgado_a" IS 'Identificador del socio a quien se le otorga el cupón';

COMMENT ON COLUMN "cupon"."otorgado_por" IS 'Identificador del usuario quien otorgó el cupón';

COMMENT ON COLUMN "cupon"."fecha_otorgado" IS 'Identificador del usuario quien otorgó el cupón';

COMMENT ON COLUMN "cupon"."razon_otorga" IS 'Razón por la que se otorga el cupón';

COMMENT ON COLUMN "cupon"."utilizado" IS 'Indica si ya se utilizo o no';

COMMENT ON COLUMN "cupon"."fecha_utilizado" IS 'Identificador del usuario quien otorgó el cupón';

SET search_path TO public;
-----------------------------------------------------------------------------
-- promocion_cupon_producto
-----------------------------------------------------------------------------

DROP TABLE "promocion_cupon_producto" CASCADE;


CREATE TABLE "promocion_cupon_producto"
(
	"promocion_id" INT8  NOT NULL,
	"producto_id" INTEGER  NOT NULL,
	PRIMARY KEY ("promocion_id","producto_id")
);

COMMENT ON TABLE "promocion_cupon_producto" IS '';


COMMENT ON COLUMN "promocion_cupon_producto"."promocion_id" IS 'Identificador de la promoción de cupones';

COMMENT ON COLUMN "promocion_cupon_producto"."producto_id" IS 'Identificador del producto que entra en la promoción';

SET search_path TO public;
-----------------------------------------------------------------------------
-- detalle_orden
-----------------------------------------------------------------------------

DROP TABLE "detalle_orden" CASCADE;


CREATE TABLE "detalle_orden"
(
	"id" INT8  NOT NULL,
	"orden_id" INT8,
	"centro_id" INTEGER,
	"socio_id" INT8,
	"producto_id" INTEGER,
	"precio_lista" DECIMAL(10,2) default 0,
	"tiempo" INTEGER default 0,
	"cantidad" DECIMAL(10,2) default 0,
	"descuento" DECIMAL(10,6) default 0,
	"subtotal" DECIMAL(10,2) default 0,
	"cupon_id" INT8,
	"promocion_id" INT8,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "detalle_orden" IS '';


COMMENT ON COLUMN "detalle_orden"."id" IS 'Identificador de detalle de orden';

COMMENT ON COLUMN "detalle_orden"."orden_id" IS 'Identificador de detalle de orden';

COMMENT ON COLUMN "detalle_orden"."centro_id" IS 'Identificador del centro donde se realiza la venta';

COMMENT ON COLUMN "detalle_orden"."socio_id" IS 'Identificador del socio que hace la compra';

COMMENT ON COLUMN "detalle_orden"."producto_id" IS 'Identificador del producto que compone esta compra';

COMMENT ON COLUMN "detalle_orden"."precio_lista" IS 'Precio de lista del producto en el momento de la venta';

COMMENT ON COLUMN "detalle_orden"."tiempo" IS 'Tiempo que incluye el producto en el momento de la venta';

COMMENT ON COLUMN "detalle_orden"."cantidad" IS 'Cantidad de producto a vender';

COMMENT ON COLUMN "detalle_orden"."descuento" IS 'Descuento realizado en el momento de la venta';

COMMENT ON COLUMN "detalle_orden"."subtotal" IS 'Subtotal';

COMMENT ON COLUMN "detalle_orden"."cupon_id" IS 'Identificador del cupon que se utilizó';

COMMENT ON COLUMN "detalle_orden"."promocion_id" IS 'Identificador de la promoción por la que se realizó descuento';

SET search_path TO public;
-----------------------------------------------------------------------------
-- folio_venta
-----------------------------------------------------------------------------

DROP TABLE "folio_venta" CASCADE;


CREATE TABLE "folio_venta"
(
	"centro_id" INTEGER  NOT NULL,
	"folio" INTEGER default 1,
	PRIMARY KEY ("centro_id")
);

COMMENT ON TABLE "folio_venta" IS '';


COMMENT ON COLUMN "folio_venta"."centro_id" IS 'Identificador del centro donde se realiza la venta';

COMMENT ON COLUMN "folio_venta"."folio" IS 'último Folio consecutivo de venta en el centro';

SET search_path TO public;
-----------------------------------------------------------------------------
-- folio_salida
-----------------------------------------------------------------------------

DROP TABLE "folio_salida" CASCADE;


CREATE TABLE "folio_salida"
(
	"centro_id" INTEGER  NOT NULL,
	"folio" INTEGER default 1,
	PRIMARY KEY ("centro_id")
);

COMMENT ON TABLE "folio_salida" IS '';


COMMENT ON COLUMN "folio_salida"."centro_id" IS 'Identificador del centro donde se realiza la salida de dinero';

COMMENT ON COLUMN "folio_salida"."folio" IS 'último Folio consecutivo de salida en el centro';

SET search_path TO public;
-----------------------------------------------------------------------------
-- folio_conciliacion
-----------------------------------------------------------------------------

DROP TABLE "folio_conciliacion" CASCADE;


CREATE TABLE "folio_conciliacion"
(
	"centro_id" INTEGER  NOT NULL,
	"folio" INTEGER default 1,
	PRIMARY KEY ("centro_id")
);

COMMENT ON TABLE "folio_conciliacion" IS '';


COMMENT ON COLUMN "folio_conciliacion"."centro_id" IS 'Identificador del centro donde se realiza la conciliación administrativa';

COMMENT ON COLUMN "folio_conciliacion"."folio" IS 'último Folio consecutivo de conciliación en el centro';

SET search_path TO public;
-----------------------------------------------------------------------------
-- folio_fiscal
-----------------------------------------------------------------------------

DROP TABLE "folio_fiscal" CASCADE;


CREATE TABLE "folio_fiscal"
(
	"id" INT8  NOT NULL,
	"centro_id" INTEGER,
	"serie" VARCHAR(30),
	"no_aprobacion" INTEGER,
	"ano_aprobacion" INTEGER,
	"serie_certificado" VARCHAR(30),
	"folio_inicial" INTEGER default 1,
	"folio_final" INTEGER,
	"folio_actual" INTEGER,
	"activo" BOOLEAN default 't',
	"num_certificado" VARCHAR(60),
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "folio_fiscal" IS '';


COMMENT ON COLUMN "folio_fiscal"."id" IS 'Identificador de folios';

COMMENT ON COLUMN "folio_fiscal"."centro_id" IS 'Identificador del centro al que pertenece el certificado';

COMMENT ON COLUMN "folio_fiscal"."serie" IS 'Número exterior del domicilio fiscal';

COMMENT ON COLUMN "folio_fiscal"."no_aprobacion" IS 'Número de aprobación';

COMMENT ON COLUMN "folio_fiscal"."ano_aprobacion" IS 'Año de aprobación';

COMMENT ON COLUMN "folio_fiscal"."serie_certificado" IS 'Serie del certificado';

COMMENT ON COLUMN "folio_fiscal"."folio_inicial" IS 'Primer folio del rango';

COMMENT ON COLUMN "folio_fiscal"."folio_final" IS 'Ultimo folio del rango';

COMMENT ON COLUMN "folio_fiscal"."folio_actual" IS 'folio actual del rango';

COMMENT ON COLUMN "folio_fiscal"."activo" IS 'Bandera de vigencia';

COMMENT ON COLUMN "folio_fiscal"."num_certificado" IS 'Nombre del archivo del certificado';

SET search_path TO public;
-----------------------------------------------------------------------------
-- tipo_salida
-----------------------------------------------------------------------------

DROP TABLE "tipo_salida" CASCADE;


CREATE TABLE "tipo_salida"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "tipo_salida_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "tipo_salida" IS '';


COMMENT ON COLUMN "tipo_salida"."nombre" IS 'Nombre del tipo de salida';

COMMENT ON COLUMN "tipo_salida"."activo" IS 'Bandera de vigencia';

SET search_path TO public;
-----------------------------------------------------------------------------
-- salida
-----------------------------------------------------------------------------

DROP TABLE "salida" CASCADE;


CREATE TABLE "salida"
(
	"id" INT8  NOT NULL,
	"centro_id" INTEGER,
	"operador_id" INTEGER,
	"monto" DECIMAL(10,2) default 0,
	"tipo_id" INTEGER  NOT NULL,
	"folio_docto_aval" VARCHAR(256),
	"folio" INTEGER,
	"ip_caja" VARCHAR(15),
	"observaciones" TEXT,
	"orden_id" INT8,
	"created_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "salida" IS '';


COMMENT ON COLUMN "salida"."id" IS 'Identificador de salida de dinero';

COMMENT ON COLUMN "salida"."centro_id" IS 'Identificador del centro donde se realiza la salida';

COMMENT ON COLUMN "salida"."operador_id" IS 'Identificador del usuario que realiza la salida';

COMMENT ON COLUMN "salida"."monto" IS 'Monto de la salida';

COMMENT ON COLUMN "salida"."tipo_id" IS 'Identificador del tipo de salida';

COMMENT ON COLUMN "salida"."folio_docto_aval" IS 'Folio del documento q avala la salida de existir';

COMMENT ON COLUMN "salida"."folio" IS 'Folio del la salida';

COMMENT ON COLUMN "salida"."ip_caja" IS 'IP de la máquina donde se realiza la salida';

COMMENT ON COLUMN "salida"."observaciones" IS 'Observaciones a la salida de caja';

COMMENT ON COLUMN "salida"."orden_id" IS 'Identificador de la orden de a';

SET search_path TO public;
-----------------------------------------------------------------------------
-- conciliacion
-----------------------------------------------------------------------------

DROP TABLE "conciliacion" CASCADE;


CREATE TABLE "conciliacion"
(
	"id" INT8  NOT NULL,
	"centro_id" INTEGER,
	"operador_id" INTEGER,
	"monto" DECIMAL(10,2) default 0,
	"folio_docto_aval" VARCHAR(256),
	"folio" INTEGER,
	"ip_caja" VARCHAR(15),
	"observaciones" TEXT,
	"orden_id" INT8,
	"created_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "conciliacion" IS '';


COMMENT ON COLUMN "conciliacion"."id" IS 'Identificador de salida de dinero';

COMMENT ON COLUMN "conciliacion"."centro_id" IS 'Identificador del centro donde se realiza la salida';

COMMENT ON COLUMN "conciliacion"."operador_id" IS 'Identificador del usuario que realiza la salida';

COMMENT ON COLUMN "conciliacion"."monto" IS 'Monto de la salida';

COMMENT ON COLUMN "conciliacion"."folio_docto_aval" IS 'Folio del documento q avala la salida de existir';

COMMENT ON COLUMN "conciliacion"."folio" IS 'Folio del la salida';

COMMENT ON COLUMN "conciliacion"."ip_caja" IS 'IP de la máquina donde se intervendrá para conciliar';

COMMENT ON COLUMN "conciliacion"."observaciones" IS 'Observaciones a la conciliación administrativa';

COMMENT ON COLUMN "conciliacion"."orden_id" IS 'Identificador de la orden de conciliación administrativa';

SET search_path TO public;
-----------------------------------------------------------------------------
-- cancelacion
-----------------------------------------------------------------------------

DROP TABLE "cancelacion" CASCADE;


CREATE TABLE "cancelacion"
(
	"id" INT8  NOT NULL,
	"centro_id" INTEGER,
	"operador_id" INTEGER,
	"orden_id" INT8,
	"orden_cancelacion_id" INT8,
	"observaciones" TEXT,
	"created_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "cancelacion" IS '';


COMMENT ON COLUMN "cancelacion"."id" IS 'Identificador de salida de dinero';

COMMENT ON COLUMN "cancelacion"."centro_id" IS 'Identificador del centro donde se realiza la salida';

COMMENT ON COLUMN "cancelacion"."operador_id" IS 'Identificador del cajero q realiza la cancelación';

COMMENT ON COLUMN "cancelacion"."orden_id" IS 'Identificador de la orden a cancelar';

COMMENT ON COLUMN "cancelacion"."orden_cancelacion_id" IS 'Identificador de la orden de cancelación';

COMMENT ON COLUMN "cancelacion"."observaciones" IS 'Observaciones a la cancelación';

SET search_path TO public;
-----------------------------------------------------------------------------
-- tipo_cobro
-----------------------------------------------------------------------------

DROP TABLE "tipo_cobro" CASCADE;


CREATE TABLE "tipo_cobro"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"cantidad" INTEGER default 0,
	"temporalidad" VARCHAR(30) default 'semana',
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "tipo_cobro_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "tipo_cobro" IS '';


COMMENT ON COLUMN "tipo_cobro"."nombre" IS 'Nombre del tipo de cobro, diario, semanal, quincenal, mensual, bimestral';

COMMENT ON COLUMN "tipo_cobro"."cantidad" IS 'Cantidad de temporalidades que cubren este tipo de cobro';

COMMENT ON COLUMN "tipo_cobro"."temporalidad" IS 'Temporalidad con que se realizarán los cobros';

COMMENT ON COLUMN "tipo_cobro"."activo" IS 'Bandera de vigencia';

SET search_path TO public;
-----------------------------------------------------------------------------
-- modalidad_cobro_producto
-----------------------------------------------------------------------------

DROP TABLE "modalidad_cobro_producto" CASCADE;


CREATE TABLE "modalidad_cobro_producto"
(
	"producto_id" INTEGER  NOT NULL,
	"cobro_id" INTEGER  NOT NULL,
	"numero_pagos" INTEGER default 0,
	PRIMARY KEY ("producto_id","cobro_id")
);

COMMENT ON TABLE "modalidad_cobro_producto" IS '';


COMMENT ON COLUMN "modalidad_cobro_producto"."producto_id" IS 'Identificador del producto';

COMMENT ON COLUMN "modalidad_cobro_producto"."cobro_id" IS 'Identificador de tipo de cobro que se puede aplicar a este producto';

COMMENT ON COLUMN "modalidad_cobro_producto"."numero_pagos" IS 'Cantidad de pagos para saldar el total del producto';

SET search_path TO public;
-----------------------------------------------------------------------------
-- subproducto_cobranza
-----------------------------------------------------------------------------

DROP TABLE "subproducto_cobranza" CASCADE;


CREATE TABLE "subproducto_cobranza"
(
	"producto_id" INTEGER  NOT NULL,
	"sub_producto_de" INTEGER,
	"cobro_id" INTEGER  NOT NULL,
	"numero_pago" INTEGER default 0,
	PRIMARY KEY ("producto_id","cobro_id")
);

COMMENT ON TABLE "subproducto_cobranza" IS '';


COMMENT ON COLUMN "subproducto_cobranza"."producto_id" IS 'Identificador del producto que se utiliza para las parcialidades';

COMMENT ON COLUMN "subproducto_cobranza"."sub_producto_de" IS 'Identificador del producto padre';

COMMENT ON COLUMN "subproducto_cobranza"."cobro_id" IS 'Identificador de tipo de cobro que representa este subproducto';

COMMENT ON COLUMN "subproducto_cobranza"."numero_pago" IS 'Número de pago';

SET search_path TO public;
-----------------------------------------------------------------------------
-- socio_pagos
-----------------------------------------------------------------------------

DROP TABLE "socio_pagos" CASCADE;


CREATE TABLE "socio_pagos"
(
	"id" INT8  NOT NULL,
	"centro_id" INTEGER,
	"socio_id" INT8,
	"tipo_cobro_id" INTEGER,
	"producto_id" INTEGER,
	"sub_producto_de" INTEGER,
	"numero_pago" INTEGER default 0,
	"detalle_orden_id" INT8,
	"fecha_a_pagar" TIMESTAMP,
	"fecha_pagado" TIMESTAMP,
	"pagado" BOOLEAN default 'f',
	"acuerdo_pago" BOOLEAN default 'f',
	"observaciones" TEXT,
	"cancelado" BOOLEAN default 'f',
	"orden_base_id" INT8,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "socio_pagos" IS '';


COMMENT ON COLUMN "socio_pagos"."id" IS 'Identificador de pago';

COMMENT ON COLUMN "socio_pagos"."centro_id" IS 'Identificador del centro donde existe el pago';

COMMENT ON COLUMN "socio_pagos"."socio_id" IS 'Identificador del socio que adquiere en modo de pagos un producto';

COMMENT ON COLUMN "socio_pagos"."tipo_cobro_id" IS 'Identificador de tipo de cobro';

COMMENT ON COLUMN "socio_pagos"."producto_id" IS 'Identificador de tipo de cobro';

COMMENT ON COLUMN "socio_pagos"."sub_producto_de" IS 'Identificador del producto padre';

COMMENT ON COLUMN "socio_pagos"."numero_pago" IS 'Número de pago';

COMMENT ON COLUMN "socio_pagos"."detalle_orden_id" IS 'Identificador de detalle de la orden cuando se ha realizado el pago pago';

COMMENT ON COLUMN "socio_pagos"."fecha_a_pagar" IS 'Marca de fecha en que tiene q pagar';

COMMENT ON COLUMN "socio_pagos"."fecha_pagado" IS 'Marca de fecha en la que pagó';

COMMENT ON COLUMN "socio_pagos"."pagado" IS 'Bandera de pagado';

COMMENT ON COLUMN "socio_pagos"."acuerdo_pago" IS 'Bandera de pagado';

COMMENT ON COLUMN "socio_pagos"."observaciones" IS 'Observaciones al pago';

COMMENT ON COLUMN "socio_pagos"."cancelado" IS 'Bandera de cancelacion';

COMMENT ON COLUMN "socio_pagos"."orden_base_id" IS 'Identificador de la primera orden de venta que relaciona los registros de pagos';

SET search_path TO public;
-----------------------------------------------------------------------------
-- producto_curso
-----------------------------------------------------------------------------

DROP TABLE "producto_curso" CASCADE;


CREATE TABLE "producto_curso"
(
	"producto_id" INTEGER  NOT NULL,
	"curso_id" INT8  NOT NULL,
	PRIMARY KEY ("producto_id","curso_id")
);

COMMENT ON TABLE "producto_curso" IS '';


COMMENT ON COLUMN "producto_curso"."producto_id" IS 'Identificador de producto';

COMMENT ON COLUMN "producto_curso"."curso_id" IS 'Identificador de curso';

SET search_path TO public;
-----------------------------------------------------------------------------
-- alumno_pagos
-----------------------------------------------------------------------------

DROP TABLE "alumno_pagos" CASCADE;


CREATE TABLE "alumno_pagos"
(
	"id" INT8  NOT NULL,
	"centro_id" INTEGER,
	"socio_id" INT8,
	"curso_id" INT8,
	"grupo_id" INT8,
	"tipo_cobro_id" INTEGER,
	"pagado" BOOLEAN default 'f',
	"numero_pago" INTEGER default 0,
	"cancelado" BOOLEAN default 'f',
	"producto_id" INTEGER,
	"sub_producto_de" INTEGER,
	"orden_base_id" INT8,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "alumno_pagos" IS '';


COMMENT ON COLUMN "alumno_pagos"."id" IS 'Identificador de pago';

COMMENT ON COLUMN "alumno_pagos"."centro_id" IS 'Identificador del centro donde se realiza la salida';

COMMENT ON COLUMN "alumno_pagos"."socio_id" IS 'Identificador del socio que adquiere en modo de pagos un producto';

COMMENT ON COLUMN "alumno_pagos"."curso_id" IS 'Identificador de producto';

COMMENT ON COLUMN "alumno_pagos"."grupo_id" IS 'Identificador de grupo';

COMMENT ON COLUMN "alumno_pagos"."tipo_cobro_id" IS 'Identificador de tipo de cobro';

COMMENT ON COLUMN "alumno_pagos"."pagado" IS 'Bandera de pagado';

COMMENT ON COLUMN "alumno_pagos"."numero_pago" IS 'Número de pago';

COMMENT ON COLUMN "alumno_pagos"."cancelado" IS 'Bandera de cancelacion';

COMMENT ON COLUMN "alumno_pagos"."producto_id" IS 'Identificador de producto correspondiente al curso';

COMMENT ON COLUMN "alumno_pagos"."sub_producto_de" IS 'Identificador del producto padre';

COMMENT ON COLUMN "alumno_pagos"."orden_base_id" IS 'Identificador de la ultima orden de venta que relaciona los cursos pagados';

SET search_path TO public;
ALTER TABLE "asignacion_beca" ADD CONSTRAINT "asignacion_beca_FK_1" FOREIGN KEY ("beca_id") REFERENCES "beca" ("id");

ALTER TABLE "asignacion_beca" ADD CONSTRAINT "asignacion_beca_FK_2" FOREIGN KEY ("centro_id") REFERENCES "centro" ("id");

ALTER TABLE "asignacion_beca" ADD CONSTRAINT "asignacion_beca_FK_3" FOREIGN KEY ("socio_id") REFERENCES "socio" ("id");

ALTER TABLE "asignacion_beca" ADD CONSTRAINT "asignacion_beca_FK_4" FOREIGN KEY ("registro_operador_id") REFERENCES "usuario" ("id");

ALTER TABLE "asignacion_beca" ADD CONSTRAINT "asignacion_beca_FK_5" FOREIGN KEY ("otorga_operador_id") REFERENCES "usuario" ("id");

ALTER TABLE "sincronia_centro" ADD CONSTRAINT "sincronia_centro_FK_1" FOREIGN KEY ("sincronia_id") REFERENCES "sincronia" ("id");

ALTER TABLE "sincronia_centro" ADD CONSTRAINT "sincronia_centro_FK_2" FOREIGN KEY ("centro_id") REFERENCES "centro" ("id");

ALTER TABLE "alumnos" ADD CONSTRAINT "alumnos_FK_1" FOREIGN KEY ("socio_id") REFERENCES "socio" ("id");

ALTER TABLE "aula" ADD CONSTRAINT "aula_FK_1" FOREIGN KEY ("centro_id") REFERENCES "centro" ("id") ON DELETE CASCADE;

ALTER TABLE "aula" ADD CONSTRAINT "aula_FK_2" FOREIGN KEY ("operador_id") REFERENCES "usuario" ("id") ON DELETE SET NULL;

ALTER TABLE "calificacion_parcial" ADD CONSTRAINT "calificacion_parcial_FK_1" FOREIGN KEY ("curso_id") REFERENCES "curso" ("id") ON DELETE RESTRICT;

ALTER TABLE "calificacion_parcial" ADD CONSTRAINT "calificacion_parcial_FK_2" FOREIGN KEY ("evaluacion_id") REFERENCES "evaluacion_parcial" ("id");

ALTER TABLE "calificacion_parcial" ADD CONSTRAINT "calificacion_parcial_FK_3" FOREIGN KEY ("alumno_id") REFERENCES "alumnos" ("matricula");

ALTER TABLE "calificacion_parcial" ADD CONSTRAINT "calificacion_parcial_FK_4" FOREIGN KEY ("operador_id") REFERENCES "usuario" ("id") ON DELETE SET NULL;

ALTER TABLE "calificacion_final" ADD CONSTRAINT "calificacion_final_FK_1" FOREIGN KEY ("curso_id") REFERENCES "curso" ("id") ON DELETE RESTRICT;

ALTER TABLE "calificacion_final" ADD CONSTRAINT "calificacion_final_FK_2" FOREIGN KEY ("evaluacion_id") REFERENCES "evaluacion_final" ("id");

ALTER TABLE "calificacion_final" ADD CONSTRAINT "calificacion_final_FK_3" FOREIGN KEY ("alumno_id") REFERENCES "alumnos" ("matricula");

ALTER TABLE "calificacion_final" ADD CONSTRAINT "calificacion_final_FK_4" FOREIGN KEY ("operador_id") REFERENCES "usuario" ("id") ON DELETE SET NULL;

ALTER TABLE "categoria_curso" ADD CONSTRAINT "categoria_curso_FK_1" FOREIGN KEY ("padre") REFERENCES "categoria_curso" ("id");

ALTER TABLE "categoria_curso" ADD CONSTRAINT "categoria_curso_FK_2" FOREIGN KEY ("operador_id") REFERENCES "usuario" ("id") ON DELETE SET NULL;

ALTER TABLE "curso" ADD CONSTRAINT "curso_FK_1" FOREIGN KEY ("categoria_curso_id") REFERENCES "categoria_curso" ("id") ON DELETE RESTRICT;

ALTER TABLE "curso" ADD CONSTRAINT "curso_FK_2" FOREIGN KEY ("perfil_id") REFERENCES "perfil_facilitador" ("id") ON DELETE RESTRICT;

ALTER TABLE "curso" ADD CONSTRAINT "curso_FK_3" FOREIGN KEY ("operador_id") REFERENCES "usuario" ("id") ON DELETE SET NULL;

ALTER TABLE "curso_centro" ADD CONSTRAINT "curso_centro_FK_1" FOREIGN KEY ("curso_id") REFERENCES "curso" ("id") ON DELETE CASCADE;

ALTER TABLE "curso_centro" ADD CONSTRAINT "curso_centro_FK_2" FOREIGN KEY ("centro_id") REFERENCES "centro" ("id") ON DELETE CASCADE;

ALTER TABLE "cursos_finalizados" ADD CONSTRAINT "cursos_finalizados_FK_1" FOREIGN KEY ("curso_id") REFERENCES "curso" ("id") ON DELETE RESTRICT;

ALTER TABLE "cursos_finalizados" ADD CONSTRAINT "cursos_finalizados_FK_2" FOREIGN KEY ("alumno_id") REFERENCES "alumnos" ("matricula");

ALTER TABLE "cursos_finalizados" ADD CONSTRAINT "cursos_finalizados_FK_3" FOREIGN KEY ("grupo_id") REFERENCES "grupo" ("id");

ALTER TABLE "cursos_finalizados" ADD CONSTRAINT "cursos_finalizados_FK_4" FOREIGN KEY ("calificacion_id") REFERENCES "calificacion_final" ("id") ON DELETE RESTRICT;

ALTER TABLE "dias_no_laborables" ADD CONSTRAINT "dias_no_laborables_FK_1" FOREIGN KEY ("operador_id") REFERENCES "usuario" ("id") ON DELETE SET NULL;

ALTER TABLE "evaluacion_parcial" ADD CONSTRAINT "evaluacion_parcial_FK_1" FOREIGN KEY ("curso_id") REFERENCES "curso" ("id") ON DELETE CASCADE;

ALTER TABLE "evaluacion_parcial" ADD CONSTRAINT "evaluacion_parcial_FK_2" FOREIGN KEY ("tema_id") REFERENCES "tema" ("id") ON DELETE CASCADE;

ALTER TABLE "evaluacion_parcial" ADD CONSTRAINT "evaluacion_parcial_FK_3" FOREIGN KEY ("operador_id") REFERENCES "usuario" ("id") ON DELETE SET NULL;

ALTER TABLE "evaluacion_final" ADD CONSTRAINT "evaluacion_final_FK_1" FOREIGN KEY ("curso_id") REFERENCES "curso" ("id") ON DELETE CASCADE;

ALTER TABLE "evaluacion_final" ADD CONSTRAINT "evaluacion_final_FK_2" FOREIGN KEY ("operador_id") REFERENCES "usuario" ("id") ON DELETE SET NULL;

ALTER TABLE "fechas_horario" ADD CONSTRAINT "fechas_horario_FK_1" FOREIGN KEY ("horario_id") REFERENCES "horario" ("id") ON DELETE CASCADE;

ALTER TABLE "grupo" ADD CONSTRAINT "grupo_FK_1" FOREIGN KEY ("centro_id") REFERENCES "centro" ("id") ON DELETE CASCADE;

ALTER TABLE "grupo" ADD CONSTRAINT "grupo_FK_2" FOREIGN KEY ("curso_id") REFERENCES "curso" ("id") ON DELETE CASCADE;

ALTER TABLE "grupo" ADD CONSTRAINT "grupo_FK_3" FOREIGN KEY ("aula_id") REFERENCES "aula" ("id") ON DELETE RESTRICT;

ALTER TABLE "grupo" ADD CONSTRAINT "grupo_FK_4" FOREIGN KEY ("horario_id") REFERENCES "horario" ("id") ON DELETE CASCADE;

ALTER TABLE "grupo" ADD CONSTRAINT "grupo_FK_5" FOREIGN KEY ("facilitador_id") REFERENCES "usuario" ("id") ON DELETE RESTRICT;

ALTER TABLE "grupo" ADD CONSTRAINT "grupo_FK_6" FOREIGN KEY ("operador_id") REFERENCES "usuario" ("id") ON DELETE SET NULL;

ALTER TABLE "grupo_alumnos" ADD CONSTRAINT "grupo_alumnos_FK_1" FOREIGN KEY ("grupo_id") REFERENCES "grupo" ("id");

ALTER TABLE "grupo_alumnos" ADD CONSTRAINT "grupo_alumnos_FK_2" FOREIGN KEY ("alumno_id") REFERENCES "alumnos" ("matricula");

ALTER TABLE "horario" ADD CONSTRAINT "horario_FK_1" FOREIGN KEY ("centro_id") REFERENCES "centro" ("id") ON DELETE CASCADE;

ALTER TABLE "horario" ADD CONSTRAINT "horario_FK_2" FOREIGN KEY ("curso_id") REFERENCES "curso" ("id") ON DELETE CASCADE;

ALTER TABLE "horario" ADD CONSTRAINT "horario_FK_3" FOREIGN KEY ("aula_id") REFERENCES "aula" ("id") ON DELETE RESTRICT;

ALTER TABLE "lista_asistencia" ADD CONSTRAINT "lista_asistencia_FK_1" FOREIGN KEY ("grupo_id") REFERENCES "grupo" ("id");

ALTER TABLE "lista_asistencia" ADD CONSTRAINT "lista_asistencia_FK_2" FOREIGN KEY ("curso_id") REFERENCES "curso" ("id");

ALTER TABLE "lista_asistencia" ADD CONSTRAINT "lista_asistencia_FK_3" FOREIGN KEY ("alumno_id") REFERENCES "alumnos" ("matricula");

ALTER TABLE "lista_asistencia" ADD CONSTRAINT "lista_asistencia_FK_4" FOREIGN KEY ("fecha_id") REFERENCES "fechas_horario" ("id");

ALTER TABLE "lista_asistencia" ADD CONSTRAINT "lista_asistencia_FK_5" FOREIGN KEY ("operador_id") REFERENCES "usuario" ("id") ON DELETE SET NULL;

ALTER TABLE "perfil_facilitador" ADD CONSTRAINT "perfil_facilitador_FK_1" FOREIGN KEY ("operador_id") REFERENCES "usuario" ("id") ON DELETE SET NULL;

ALTER TABLE "restricciones_horario" ADD CONSTRAINT "restricciones_horario_FK_1" FOREIGN KEY ("curso_id") REFERENCES "curso" ("id") ON DELETE CASCADE;

ALTER TABLE "restricciones_socio" ADD CONSTRAINT "restricciones_socio_FK_1" FOREIGN KEY ("curso_id") REFERENCES "curso" ("id") ON DELETE CASCADE;

ALTER TABLE "restricciones_socio" ADD CONSTRAINT "restricciones_socio_FK_2" FOREIGN KEY ("ocupacion_id") REFERENCES "ocupacion" ("id");

ALTER TABLE "restricciones_socio" ADD CONSTRAINT "restricciones_socio_FK_3" FOREIGN KEY ("nivel_estudio_id") REFERENCES "nivel_estudio" ("id");

ALTER TABLE "restricciones_socio" ADD CONSTRAINT "restricciones_socio_FK_4" FOREIGN KEY ("habilidad_informatica_id") REFERENCES "habilidad_informatica" ("id");

ALTER TABLE "restricciones_socio" ADD CONSTRAINT "restricciones_socio_FK_5" FOREIGN KEY ("dominio_ingles_id") REFERENCES "dominio_ingles" ("id");

ALTER TABLE "ruta_aprendizaje" ADD CONSTRAINT "ruta_aprendizaje_FK_1" FOREIGN KEY ("curso_id") REFERENCES "curso" ("id") ON DELETE CASCADE;

ALTER TABLE "ruta_aprendizaje" ADD CONSTRAINT "ruta_aprendizaje_FK_2" FOREIGN KEY ("padre_id") REFERENCES "curso" ("id") ON DELETE CASCADE;

ALTER TABLE "tema" ADD CONSTRAINT "tema_FK_1" FOREIGN KEY ("curso_id") REFERENCES "curso" ("id");

ALTER TABLE "tema" ADD CONSTRAINT "tema_FK_2" FOREIGN KEY ("operador_id") REFERENCES "usuario" ("id") ON DELETE SET NULL;

ALTER TABLE "usuario_perfil" ADD CONSTRAINT "usuario_perfil_FK_1" FOREIGN KEY ("usuario_id") REFERENCES "usuario" ("id") ON DELETE CASCADE;

ALTER TABLE "usuario_perfil" ADD CONSTRAINT "usuario_perfil_FK_2" FOREIGN KEY ("perfil_id") REFERENCES "perfil_facilitador" ("id") ON DELETE CASCADE;

ALTER TABLE "vacaciones" ADD CONSTRAINT "vacaciones_FK_1" FOREIGN KEY ("usuario_id") REFERENCES "usuario" ("id");

ALTER TABLE "vacaciones" ADD CONSTRAINT "vacaciones_FK_2" FOREIGN KEY ("operador_id") REFERENCES "usuario" ("id") ON DELETE SET NULL;

ALTER TABLE "metas" ADD CONSTRAINT "metas_FK_1" FOREIGN KEY ("centro_id") REFERENCES "centro" ("id");

ALTER TABLE "metas" ADD CONSTRAINT "metas_FK_2" FOREIGN KEY ("categoria_fija_id") REFERENCES "categoria_metas_fijas" ("id");

ALTER TABLE "metas" ADD CONSTRAINT "metas_FK_3" FOREIGN KEY ("categoria_producto_id") REFERENCES "categoria_reporte" ("id");

ALTER TABLE "metas" ADD CONSTRAINT "metas_FK_4" FOREIGN KEY ("categoria_beca_id") REFERENCES "beca" ("id");

ALTER TABLE "metas" ADD CONSTRAINT "metas_FK_5" FOREIGN KEY ("usuario_id") REFERENCES "usuario" ("id");

ALTER TABLE "relfixes" ADD CONSTRAINT "relfixes_FK_1" FOREIGN KEY ("centro_id") REFERENCES "centro" ("id");

ALTER TABLE "relfixes" ADD CONSTRAINT "relfixes_FK_2" FOREIGN KEY ("uidnumber") REFERENCES "centro" ("id");

ALTER TABLE "relfixes" ADD CONSTRAINT "relfixes_FK_3" FOREIGN KEY ("uidnumber_new") REFERENCES "centro" ("id");

ALTER TABLE "inventario" ADD CONSTRAINT "inventario_FK_1" FOREIGN KEY ("operacion_id") REFERENCES "inventario_operacion" ("id");

ALTER TABLE "inventario" ADD CONSTRAINT "inventario_FK_2" FOREIGN KEY ("producto_id") REFERENCES "producto" ("id");

ALTER TABLE "inventario" ADD CONSTRAINT "inventario_FK_3" FOREIGN KEY ("centro_id") REFERENCES "centro" ("id");

ALTER TABLE "inventario_conteo" ADD CONSTRAINT "inventario_conteo_FK_1" FOREIGN KEY ("corte_id") REFERENCES "inventario_corte" ("id");

ALTER TABLE "inventario_conteo" ADD CONSTRAINT "inventario_conteo_FK_2" FOREIGN KEY ("operacion_id") REFERENCES "inventario_operacion" ("id");

ALTER TABLE "inventario_conteo" ADD CONSTRAINT "inventario_conteo_FK_3" FOREIGN KEY ("producto_id") REFERENCES "producto" ("id");

ALTER TABLE "inventario_conteo" ADD CONSTRAINT "inventario_conteo_FK_4" FOREIGN KEY ("centro_id") REFERENCES "centro" ("id");

ALTER TABLE "inventario_corte" ADD CONSTRAINT "inventario_corte_FK_1" FOREIGN KEY ("operacion_id") REFERENCES "inventario_operacion" ("id");

ALTER TABLE "inventario_corte" ADD CONSTRAINT "inventario_corte_FK_2" FOREIGN KEY ("centro_id") REFERENCES "centro" ("id");

ALTER TABLE "sesion" ADD CONSTRAINT "sesion_FK_1" FOREIGN KEY ("centro_id") REFERENCES "centro" ("id");

ALTER TABLE "sesion" ADD CONSTRAINT "sesion_FK_2" FOREIGN KEY ("computadora_id") REFERENCES "computadora" ("id");

ALTER TABLE "sesion" ADD CONSTRAINT "sesion_FK_3" FOREIGN KEY ("socio_id") REFERENCES "socio" ("id");

ALTER TABLE "sesion" ADD CONSTRAINT "sesion_FK_4" FOREIGN KEY ("tipo_id") REFERENCES "tipo_sesion" ("id");

ALTER TABLE "sesion_historico" ADD CONSTRAINT "sesion_historico_FK_1" FOREIGN KEY ("centro_id") REFERENCES "centro" ("id");

ALTER TABLE "sesion_historico" ADD CONSTRAINT "sesion_historico_FK_2" FOREIGN KEY ("computadora_id") REFERENCES "computadora" ("id");

ALTER TABLE "sesion_historico" ADD CONSTRAINT "sesion_historico_FK_3" FOREIGN KEY ("socio_id") REFERENCES "socio" ("id");

ALTER TABLE "sesion_historico" ADD CONSTRAINT "sesion_historico_FK_4" FOREIGN KEY ("tipo_id") REFERENCES "tipo_sesion" ("id");

ALTER TABLE "sesion_operador" ADD CONSTRAINT "sesion_operador_FK_1" FOREIGN KEY ("centro_id") REFERENCES "centro" ("id");

ALTER TABLE "sesion_operador" ADD CONSTRAINT "sesion_operador_FK_2" FOREIGN KEY ("usuario_id") REFERENCES "usuario" ("id");

ALTER TABLE "sesion_operador_historico" ADD CONSTRAINT "sesion_operador_historico_FK_1" FOREIGN KEY ("centro_id") REFERENCES "centro" ("id");

ALTER TABLE "sesion_operador_historico" ADD CONSTRAINT "sesion_operador_historico_FK_2" FOREIGN KEY ("usuario_id") REFERENCES "usuario" ("id");

ALTER TABLE "categoria_reporte_producto" ADD CONSTRAINT "categoria_reporte_producto_FK_1" FOREIGN KEY ("categoria_id") REFERENCES "categoria_reporte" ("id");

ALTER TABLE "categoria_reporte_producto" ADD CONSTRAINT "categoria_reporte_producto_FK_2" FOREIGN KEY ("producto_id") REFERENCES "producto" ("id");

ALTER TABLE "seccion" ADD CONSTRAINT "seccion_FK_1" FOREIGN KEY ("centro_id") REFERENCES "centro" ("id");

ALTER TABLE "computadora" ADD CONSTRAINT "computadora_FK_1" FOREIGN KEY ("centro_id") REFERENCES "centro" ("id");

ALTER TABLE "computadora" ADD CONSTRAINT "computadora_FK_2" FOREIGN KEY ("seccion_id") REFERENCES "seccion" ("id");

ALTER TABLE "computadora" ADD CONSTRAINT "computadora_FK_3" FOREIGN KEY ("tipo_id") REFERENCES "tipo_pc" ("id");

ALTER TABLE "empresa" ADD CONSTRAINT "empresa_FK_1" FOREIGN KEY ("tipo_id") REFERENCES "tipo_empresa" ("id");

ALTER TABLE "empresa" ADD CONSTRAINT "empresa_FK_2" FOREIGN KEY ("id_estado") REFERENCES "entidad_federativa" ("id");

ALTER TABLE "usuario" ADD CONSTRAINT "usuario_FK_1" FOREIGN KEY ("rol_id") REFERENCES "rol" ("id");

ALTER TABLE "usuario" ADD CONSTRAINT "usuario_FK_2" FOREIGN KEY ("operador_id") REFERENCES "usuario" ("id");

ALTER TABLE "usuario_centro" ADD CONSTRAINT "usuario_centro_FK_1" FOREIGN KEY ("usuario_id") REFERENCES "usuario" ("id");

ALTER TABLE "usuario_centro" ADD CONSTRAINT "usuario_centro_FK_2" FOREIGN KEY ("centro_id") REFERENCES "centro" ("id");

ALTER TABLE "socio" ADD CONSTRAINT "socio_FK_1" FOREIGN KEY ("centro_id") REFERENCES "centro" ("id");

ALTER TABLE "socio" ADD CONSTRAINT "socio_FK_2" FOREIGN KEY ("rol_id") REFERENCES "rol" ("id");

ALTER TABLE "socio" ADD CONSTRAINT "socio_FK_3" FOREIGN KEY ("id_estado") REFERENCES "entidad_federativa" ("id");

ALTER TABLE "socio" ADD CONSTRAINT "socio_FK_4" FOREIGN KEY ("empresa") REFERENCES "empresa" ("id");

ALTER TABLE "socio" ADD CONSTRAINT "socio_FK_5" FOREIGN KEY ("canal_contacto_id") REFERENCES "canal_contacto" ("id");

ALTER TABLE "socio" ADD CONSTRAINT "socio_FK_6" FOREIGN KEY ("recomienda_id") REFERENCES "recomienda" ("id");

ALTER TABLE "socio" ADD CONSTRAINT "socio_FK_7" FOREIGN KEY ("socio_ref_id") REFERENCES "socio" ("id");

ALTER TABLE "socio" ADD CONSTRAINT "socio_FK_8" FOREIGN KEY ("promotor_ref_id") REFERENCES "usuario" ("id");

ALTER TABLE "socio" ADD CONSTRAINT "socio_FK_9" FOREIGN KEY ("discapacidad_id") REFERENCES "discapacidad" ("id");

ALTER TABLE "socio" ADD CONSTRAINT "socio_FK_10" FOREIGN KEY ("habilidad_informatica_id") REFERENCES "habilidad_informatica" ("id");

ALTER TABLE "socio" ADD CONSTRAINT "socio_FK_11" FOREIGN KEY ("dominio_ingles_id") REFERENCES "dominio_ingles" ("id");

ALTER TABLE "socio" ADD CONSTRAINT "socio_FK_12" FOREIGN KEY ("nivel_id") REFERENCES "nivel_estudio" ("id");

ALTER TABLE "socio" ADD CONSTRAINT "socio_FK_13" FOREIGN KEY ("profesion_actual_id") REFERENCES "profesion" ("id");

ALTER TABLE "socio" ADD CONSTRAINT "socio_FK_14" FOREIGN KEY ("estudio_actual_id") REFERENCES "estudio" ("id");

ALTER TABLE "socio" ADD CONSTRAINT "socio_FK_15" FOREIGN KEY ("tipo_escuela_id") REFERENCES "tipo_escuela" ("id");

ALTER TABLE "socio" ADD CONSTRAINT "socio_FK_16" FOREIGN KEY ("tipo_estudio_id") REFERENCES "tipo_estudio" ("id");

ALTER TABLE "socio" ADD CONSTRAINT "socio_FK_17" FOREIGN KEY ("grado_id") REFERENCES "grado_estudio" ("id");

ALTER TABLE "socio" ADD CONSTRAINT "socio_FK_18" FOREIGN KEY ("profesion_ultimo_id") REFERENCES "profesion" ("id");

ALTER TABLE "socio" ADD CONSTRAINT "socio_FK_19" FOREIGN KEY ("estudio_ultimo_id") REFERENCES "estudio" ("id");

ALTER TABLE "socio" ADD CONSTRAINT "socio_FK_20" FOREIGN KEY ("organizacion_id") REFERENCES "tipo_organizacion" ("id");

ALTER TABLE "socio" ADD CONSTRAINT "socio_FK_21" FOREIGN KEY ("sector_id") REFERENCES "sector" ("id");

ALTER TABLE "socio" ADD CONSTRAINT "socio_FK_22" FOREIGN KEY ("posicion_id") REFERENCES "posicion" ("id");

ALTER TABLE "socio" ADD CONSTRAINT "socio_FK_23" FOREIGN KEY ("ocupacion_id") REFERENCES "ocupacion" ("id");

ALTER TABLE "socio" ADD CONSTRAINT "socio_FK_24" FOREIGN KEY ("parentesco_id") REFERENCES "parentesco" ("id");

ALTER TABLE "socio" ADD CONSTRAINT "socio_FK_25" FOREIGN KEY ("operador_id") REFERENCES "usuario" ("id");

ALTER TABLE "socio" ADD CONSTRAINT "socio_FK_26" FOREIGN KEY ("tipo_sesion_id") REFERENCES "tipo_sesion" ("id");

ALTER TABLE "socio" ADD CONSTRAINT "socio_FK_27" FOREIGN KEY ("estado_civil_id") REFERENCES "estado_civil" ("id");

ALTER TABLE "socio" ADD CONSTRAINT "socio_FK_28" FOREIGN KEY ("servicio_salud_id") REFERENCES "servicio_salud" ("id");

ALTER TABLE "socio" ADD CONSTRAINT "socio_FK_29" FOREIGN KEY ("vivienda_tipo_id") REFERENCES "vivienda_tipo" ("id");

ALTER TABLE "socio" ADD CONSTRAINT "socio_FK_30" FOREIGN KEY ("vivienda_es_id") REFERENCES "vivienda_es" ("id");

ALTER TABLE "socio" ADD CONSTRAINT "socio_FK_31" FOREIGN KEY ("material_techo_id") REFERENCES "material_techo" ("id");

ALTER TABLE "socio" ADD CONSTRAINT "socio_FK_32" FOREIGN KEY ("material_piso_id") REFERENCES "material_piso" ("id");

ALTER TABLE "socio" ADD CONSTRAINT "socio_FK_33" FOREIGN KEY ("uso_automovil_id") REFERENCES "uso_automovil" ("id");

ALTER TABLE "socio" ADD CONSTRAINT "socio_FK_34" FOREIGN KEY ("socio_madre_id") REFERENCES "socio" ("id");

ALTER TABLE "socio" ADD CONSTRAINT "socio_FK_35" FOREIGN KEY ("socio_padre_id") REFERENCES "socio" ("id");

ALTER TABLE "aceptacion_terminos" ADD CONSTRAINT "aceptacion_terminos_FK_1" FOREIGN KEY ("socio_id") REFERENCES "socio" ("id");

ALTER TABLE "producto" ADD CONSTRAINT "producto_FK_1" FOREIGN KEY ("categoria_id") REFERENCES "categoria_producto" ("id");

ALTER TABLE "producto" ADD CONSTRAINT "producto_FK_2" FOREIGN KEY ("tipo_id") REFERENCES "tipo_producto" ("id");

ALTER TABLE "producto" ADD CONSTRAINT "producto_FK_3" FOREIGN KEY ("unidad_id") REFERENCES "unidad_producto" ("id");

ALTER TABLE "paquete" ADD CONSTRAINT "paquete_FK_1" FOREIGN KEY ("paquete_id") REFERENCES "producto" ("id");

ALTER TABLE "paquete" ADD CONSTRAINT "paquete_FK_2" FOREIGN KEY ("producto_id") REFERENCES "producto" ("id");

ALTER TABLE "stock" ADD CONSTRAINT "stock_FK_1" FOREIGN KEY ("centro_id") REFERENCES "centro" ("id");

ALTER TABLE "stock" ADD CONSTRAINT "stock_FK_2" FOREIGN KEY ("producto_id") REFERENCES "producto" ("id");

ALTER TABLE "factura" ADD CONSTRAINT "factura_FK_1" FOREIGN KEY ("centro_id") REFERENCES "centro" ("id");

ALTER TABLE "factura" ADD CONSTRAINT "factura_FK_2" FOREIGN KEY ("folio_fiscal_id") REFERENCES "folio_fiscal" ("id");

ALTER TABLE "corte" ADD CONSTRAINT "corte_FK_1" FOREIGN KEY ("centro_id") REFERENCES "centro" ("id");

ALTER TABLE "corte" ADD CONSTRAINT "corte_FK_2" FOREIGN KEY ("operador_id") REFERENCES "usuario" ("id");

ALTER TABLE "corte" ADD CONSTRAINT "corte_FK_3" FOREIGN KEY ("factura_id") REFERENCES "factura" ("id");

ALTER TABLE "orden" ADD CONSTRAINT "orden_FK_1" FOREIGN KEY ("centro_id") REFERENCES "centro" ("id");

ALTER TABLE "orden" ADD CONSTRAINT "orden_FK_2" FOREIGN KEY ("socio_id") REFERENCES "socio" ("id");

ALTER TABLE "orden" ADD CONSTRAINT "orden_FK_3" FOREIGN KEY ("forma_pago_id") REFERENCES "forma_pago" ("id");

ALTER TABLE "orden" ADD CONSTRAINT "orden_FK_4" FOREIGN KEY ("operador_id") REFERENCES "usuario" ("id");

ALTER TABLE "orden" ADD CONSTRAINT "orden_FK_5" FOREIGN KEY ("tipo_id") REFERENCES "tipo_orden" ("id");

ALTER TABLE "orden" ADD CONSTRAINT "orden_FK_6" FOREIGN KEY ("estatus_id") REFERENCES "estatus_orden" ("id");

ALTER TABLE "orden" ADD CONSTRAINT "orden_FK_7" FOREIGN KEY ("corte_id") REFERENCES "corte" ("id");

ALTER TABLE "orden" ADD CONSTRAINT "orden_FK_8" FOREIGN KEY ("factura_id") REFERENCES "factura" ("id");

ALTER TABLE "promocion_producto" ADD CONSTRAINT "promocion_producto_FK_1" FOREIGN KEY ("promocion_id") REFERENCES "promocion" ("id");

ALTER TABLE "promocion_producto" ADD CONSTRAINT "promocion_producto_FK_2" FOREIGN KEY ("producto_id") REFERENCES "producto" ("id");

ALTER TABLE "promocion_categoria" ADD CONSTRAINT "promocion_categoria_FK_1" FOREIGN KEY ("promocion_id") REFERENCES "promocion" ("id");

ALTER TABLE "promocion_categoria" ADD CONSTRAINT "promocion_categoria_FK_2" FOREIGN KEY ("categoria_id") REFERENCES "categoria_producto" ("id");

ALTER TABLE "promocion_reglaci" ADD CONSTRAINT "promocion_reglaci_FK_1" FOREIGN KEY ("promocion_id") REFERENCES "promocion" ("id");

ALTER TABLE "promocion_reglaci" ADD CONSTRAINT "promocion_reglaci_FK_2" FOREIGN KEY ("nivel_estudio") REFERENCES "nivel_estudio" ("id");

ALTER TABLE "promocion_reglaci" ADD CONSTRAINT "promocion_reglaci_FK_3" FOREIGN KEY ("centro_alta_id") REFERENCES "centro" ("id");

ALTER TABLE "promocion_reglaci" ADD CONSTRAINT "promocion_reglaci_FK_4" FOREIGN KEY ("centro_venta_id") REFERENCES "centro" ("id");

ALTER TABLE "promocion_reglaci" ADD CONSTRAINT "promocion_reglaci_FK_5" FOREIGN KEY ("empresa_id") REFERENCES "empresa" ("id");

ALTER TABLE "promocion_reglaci" ADD CONSTRAINT "promocion_reglaci_FK_6" FOREIGN KEY ("discapacidad_id") REFERENCES "discapacidad" ("id");

ALTER TABLE "promocion_regla" ADD CONSTRAINT "promocion_regla_FK_1" FOREIGN KEY ("promocion_id") REFERENCES "promocion" ("id");

ALTER TABLE "promocion_regla" ADD CONSTRAINT "promocion_regla_FK_2" FOREIGN KEY ("nivel_estudio") REFERENCES "nivel_estudio" ("id");

ALTER TABLE "promocion_regla" ADD CONSTRAINT "promocion_regla_FK_3" FOREIGN KEY ("centro_alta_id") REFERENCES "centro" ("id");

ALTER TABLE "promocion_regla" ADD CONSTRAINT "promocion_regla_FK_4" FOREIGN KEY ("centro_venta_id") REFERENCES "centro" ("id");

ALTER TABLE "promocion_regla" ADD CONSTRAINT "promocion_regla_FK_5" FOREIGN KEY ("discapacidad_id") REFERENCES "discapacidad" ("id");

ALTER TABLE "promocion_regla" ADD CONSTRAINT "promocion_regla_FK_6" FOREIGN KEY ("habilidad_informatica_id") REFERENCES "habilidad_informatica" ("id");

ALTER TABLE "promocion_regla" ADD CONSTRAINT "promocion_regla_FK_7" FOREIGN KEY ("dominio_ingles_id") REFERENCES "dominio_ingles" ("id");

ALTER TABLE "promocion_regla" ADD CONSTRAINT "promocion_regla_FK_8" FOREIGN KEY ("profesion_id") REFERENCES "profesion" ("id");

ALTER TABLE "promocion_regla" ADD CONSTRAINT "promocion_regla_FK_9" FOREIGN KEY ("ocupacion_id") REFERENCES "ocupacion" ("id");

ALTER TABLE "promocion_regla" ADD CONSTRAINT "promocion_regla_FK_10" FOREIGN KEY ("posicion_id") REFERENCES "posicion" ("id");

ALTER TABLE "promocion_cupon" ADD CONSTRAINT "promocion_cupon_FK_1" FOREIGN KEY ("centro_id") REFERENCES "centro" ("id");

ALTER TABLE "promocion_cupon" ADD CONSTRAINT "promocion_cupon_FK_2" FOREIGN KEY ("creado_por") REFERENCES "usuario" ("id");

ALTER TABLE "cupon" ADD CONSTRAINT "cupon_FK_1" FOREIGN KEY ("promocion_cupon_id") REFERENCES "promocion_cupon" ("id");

ALTER TABLE "cupon" ADD CONSTRAINT "cupon_FK_2" FOREIGN KEY ("centro_id") REFERENCES "centro" ("id");

ALTER TABLE "cupon" ADD CONSTRAINT "cupon_FK_3" FOREIGN KEY ("otorgado_a") REFERENCES "socio" ("id");

ALTER TABLE "cupon" ADD CONSTRAINT "cupon_FK_4" FOREIGN KEY ("otorgado_por") REFERENCES "usuario" ("id");

ALTER TABLE "promocion_cupon_producto" ADD CONSTRAINT "promocion_cupon_producto_FK_1" FOREIGN KEY ("promocion_id") REFERENCES "promocion_cupon" ("id");

ALTER TABLE "promocion_cupon_producto" ADD CONSTRAINT "promocion_cupon_producto_FK_2" FOREIGN KEY ("producto_id") REFERENCES "producto" ("id");

ALTER TABLE "detalle_orden" ADD CONSTRAINT "detalle_orden_FK_1" FOREIGN KEY ("orden_id") REFERENCES "orden" ("id");

ALTER TABLE "detalle_orden" ADD CONSTRAINT "detalle_orden_FK_2" FOREIGN KEY ("centro_id") REFERENCES "centro" ("id");

ALTER TABLE "detalle_orden" ADD CONSTRAINT "detalle_orden_FK_3" FOREIGN KEY ("socio_id") REFERENCES "socio" ("id");

ALTER TABLE "detalle_orden" ADD CONSTRAINT "detalle_orden_FK_4" FOREIGN KEY ("producto_id") REFERENCES "producto" ("id");

ALTER TABLE "detalle_orden" ADD CONSTRAINT "detalle_orden_FK_5" FOREIGN KEY ("cupon_id") REFERENCES "cupon" ("id");

ALTER TABLE "detalle_orden" ADD CONSTRAINT "detalle_orden_FK_6" FOREIGN KEY ("promocion_id") REFERENCES "promocion" ("id");

ALTER TABLE "folio_venta" ADD CONSTRAINT "folio_venta_FK_1" FOREIGN KEY ("centro_id") REFERENCES "centro" ("id");

ALTER TABLE "folio_salida" ADD CONSTRAINT "folio_salida_FK_1" FOREIGN KEY ("centro_id") REFERENCES "centro" ("id");

ALTER TABLE "folio_conciliacion" ADD CONSTRAINT "folio_conciliacion_FK_1" FOREIGN KEY ("centro_id") REFERENCES "centro" ("id");

ALTER TABLE "folio_fiscal" ADD CONSTRAINT "folio_fiscal_FK_1" FOREIGN KEY ("centro_id") REFERENCES "centro" ("id");

ALTER TABLE "salida" ADD CONSTRAINT "salida_FK_1" FOREIGN KEY ("centro_id") REFERENCES "centro" ("id");

ALTER TABLE "salida" ADD CONSTRAINT "salida_FK_2" FOREIGN KEY ("operador_id") REFERENCES "usuario" ("id");

ALTER TABLE "salida" ADD CONSTRAINT "salida_FK_3" FOREIGN KEY ("tipo_id") REFERENCES "tipo_salida" ("id");

ALTER TABLE "salida" ADD CONSTRAINT "salida_FK_4" FOREIGN KEY ("orden_id") REFERENCES "orden" ("id");

ALTER TABLE "conciliacion" ADD CONSTRAINT "conciliacion_FK_1" FOREIGN KEY ("centro_id") REFERENCES "centro" ("id");

ALTER TABLE "conciliacion" ADD CONSTRAINT "conciliacion_FK_2" FOREIGN KEY ("operador_id") REFERENCES "usuario" ("id");

ALTER TABLE "conciliacion" ADD CONSTRAINT "conciliacion_FK_3" FOREIGN KEY ("orden_id") REFERENCES "orden" ("id");

ALTER TABLE "cancelacion" ADD CONSTRAINT "cancelacion_FK_1" FOREIGN KEY ("centro_id") REFERENCES "centro" ("id");

ALTER TABLE "cancelacion" ADD CONSTRAINT "cancelacion_FK_2" FOREIGN KEY ("operador_id") REFERENCES "usuario" ("id");

ALTER TABLE "cancelacion" ADD CONSTRAINT "cancelacion_FK_3" FOREIGN KEY ("orden_id") REFERENCES "orden" ("id");

ALTER TABLE "cancelacion" ADD CONSTRAINT "cancelacion_FK_4" FOREIGN KEY ("orden_cancelacion_id") REFERENCES "orden" ("id");

ALTER TABLE "modalidad_cobro_producto" ADD CONSTRAINT "modalidad_cobro_producto_FK_1" FOREIGN KEY ("producto_id") REFERENCES "producto" ("id");

ALTER TABLE "modalidad_cobro_producto" ADD CONSTRAINT "modalidad_cobro_producto_FK_2" FOREIGN KEY ("cobro_id") REFERENCES "tipo_cobro" ("id");

ALTER TABLE "subproducto_cobranza" ADD CONSTRAINT "subproducto_cobranza_FK_1" FOREIGN KEY ("producto_id") REFERENCES "producto" ("id") ON DELETE CASCADE;

ALTER TABLE "subproducto_cobranza" ADD CONSTRAINT "subproducto_cobranza_FK_2" FOREIGN KEY ("sub_producto_de") REFERENCES "producto" ("id");

ALTER TABLE "subproducto_cobranza" ADD CONSTRAINT "subproducto_cobranza_FK_3" FOREIGN KEY ("cobro_id") REFERENCES "tipo_cobro" ("id");

ALTER TABLE "socio_pagos" ADD CONSTRAINT "socio_pagos_FK_1" FOREIGN KEY ("centro_id") REFERENCES "centro" ("id");

ALTER TABLE "socio_pagos" ADD CONSTRAINT "socio_pagos_FK_2" FOREIGN KEY ("socio_id") REFERENCES "socio" ("id");

ALTER TABLE "socio_pagos" ADD CONSTRAINT "socio_pagos_FK_3" FOREIGN KEY ("tipo_cobro_id") REFERENCES "tipo_cobro" ("id");

ALTER TABLE "socio_pagos" ADD CONSTRAINT "socio_pagos_FK_4" FOREIGN KEY ("producto_id") REFERENCES "producto" ("id");

ALTER TABLE "socio_pagos" ADD CONSTRAINT "socio_pagos_FK_5" FOREIGN KEY ("sub_producto_de") REFERENCES "producto" ("id");

ALTER TABLE "socio_pagos" ADD CONSTRAINT "socio_pagos_FK_6" FOREIGN KEY ("detalle_orden_id") REFERENCES "detalle_orden" ("id");

ALTER TABLE "socio_pagos" ADD CONSTRAINT "socio_pagos_FK_7" FOREIGN KEY ("orden_base_id") REFERENCES "orden" ("id");

ALTER TABLE "producto_curso" ADD CONSTRAINT "producto_curso_FK_1" FOREIGN KEY ("producto_id") REFERENCES "producto" ("id");

ALTER TABLE "producto_curso" ADD CONSTRAINT "producto_curso_FK_2" FOREIGN KEY ("curso_id") REFERENCES "curso" ("id");

ALTER TABLE "alumno_pagos" ADD CONSTRAINT "alumno_pagos_FK_1" FOREIGN KEY ("centro_id") REFERENCES "centro" ("id");

ALTER TABLE "alumno_pagos" ADD CONSTRAINT "alumno_pagos_FK_2" FOREIGN KEY ("socio_id") REFERENCES "socio" ("id");

ALTER TABLE "alumno_pagos" ADD CONSTRAINT "alumno_pagos_FK_3" FOREIGN KEY ("curso_id") REFERENCES "curso" ("id");

ALTER TABLE "alumno_pagos" ADD CONSTRAINT "alumno_pagos_FK_4" FOREIGN KEY ("grupo_id") REFERENCES "grupo" ("id") ON DELETE CASCADE;

ALTER TABLE "alumno_pagos" ADD CONSTRAINT "alumno_pagos_FK_5" FOREIGN KEY ("tipo_cobro_id") REFERENCES "tipo_cobro" ("id");

ALTER TABLE "alumno_pagos" ADD CONSTRAINT "alumno_pagos_FK_6" FOREIGN KEY ("producto_id") REFERENCES "producto" ("id");

ALTER TABLE "alumno_pagos" ADD CONSTRAINT "alumno_pagos_FK_7" FOREIGN KEY ("sub_producto_de") REFERENCES "producto" ("id");

ALTER TABLE "alumno_pagos" ADD CONSTRAINT "alumno_pagos_FK_8" FOREIGN KEY ("orden_base_id") REFERENCES "orden" ("id");
