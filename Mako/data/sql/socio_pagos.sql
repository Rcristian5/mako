--Cambios a la tabla de socio_pagos para poder manejar la cobranza fija al momento de realizar el primer pago, independientemente de los cambios del catalogo de producto
--Asi mismo con estos cambios es posible predecir sin realizar operaciones por el perfil de descuento del socio, los montos de pago semanales a futuro, independientemente de los cambios en catalogo de descuentos, perfil de socio 
--o cambios en el catalogo de promociones y descuentos.

ALTER TABLE socio_pagos ADD COLUMN precio_lista numeric(10,2);
COMMENT ON COLUMN socio_pagos.precio_lista IS 'Precio de lista del producto en el momento de la venta';

ALTER TABLE socio_pagos ADD COLUMN cantidad numeric(10,2);
COMMENT ON COLUMN socio_pagos.cantidad IS 'Cantidad de producto a vender';

ALTER TABLE socio_pagos ADD COLUMN descuento numeric(10,6);
COMMENT ON COLUMN socio_pagos.descuento IS 'Descuento realizado en el momento de la venta';

ALTER TABLE socio_pagos ADD COLUMN subtotal numeric(10,2);
COMMENT ON COLUMN socio_pagos.subtotal IS 'Subtotal';

ALTER TABLE socio_pagos ADD COLUMN tiempo integer;
COMMENT ON COLUMN socio_pagos.tiempo IS 'Tiempo que incluye el producto en el momento de la venta';

ALTER TABLE socio_pagos ADD COLUMN promocion_id bigint;
COMMENT ON COLUMN socio_pagos.promocion_id IS 'Identificador de la promoción por la que se realizó descuento';

ALTER TABLE socio_pagos
  ADD CONSTRAINT "socio_pagos_FK_8" FOREIGN KEY (promocion_id)
      REFERENCES promocion (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

