-----------------------------------------------------------------------------
-- reporte_activos
-----------------------------------------------------------------------------

DROP TABLE "reporte_activos" CASCADE;


CREATE TABLE "reporte_activos"
(
	"centro_id" INTEGER  NOT NULL,
	"fecha" DATE  NOT NULL,
	"socio_id" INT8  NOT NULL,
	"usuario" VARCHAR(90)  NOT NULL,
	"folio" VARCHAR(50)  NOT NULL,
	"nombre" VARCHAR(30)  NOT NULL,
	"apepat" VARCHAR(60)  NOT NULL,
	"apemat" VARCHAR(60),
	"nombre_completo" VARCHAR(200),
	"fecha_nac" DATE  NOT NULL,
	"sexo" CHAR(1)  NOT NULL,
	"email" VARCHAR(60),
	"tel" VARCHAR(60),
	"celular" VARCHAR(60),
	"estado" VARCHAR(30),
	"ciudad" VARCHAR(60),
	"municipio" VARCHAR(60),
	"colonia" VARCHAR(60),
	"cp" CHAR(5),
	"calle" VARCHAR(60),
	"num_ext" VARCHAR(30),
	"num_int" VARCHAR(30),
	"lat" VARCHAR(30),
	"long" VARCHAR(30),
	PRIMARY KEY ("centro_id","fecha","socio_id")
);

COMMENT ON TABLE "reporte_activos" IS '';


COMMENT ON COLUMN "reporte_activos"."centro_id" IS 'Id de Centro.';

COMMENT ON COLUMN "reporte_activos"."fecha" IS 'Fecha y hora a la que corresponde el registro';

COMMENT ON COLUMN "reporte_activos"."socio_id" IS 'Identificador de socio activo para la fecha dada';

COMMENT ON COLUMN "reporte_activos"."usuario" IS 'Nombre de usuario usado en el login del sistema';

COMMENT ON COLUMN "reporte_activos"."folio" IS 'Numero de folio de la credencial pre-impresa';

COMMENT ON COLUMN "reporte_activos"."nombre" IS 'Nombre de la persona ';

COMMENT ON COLUMN "reporte_activos"."apepat" IS 'Apellido paterno de la persona';

COMMENT ON COLUMN "reporte_activos"."apemat" IS 'Apellido materno de la persona';

COMMENT ON COLUMN "reporte_activos"."nombre_completo" IS 'Nombre completo (nombre, apepat, apemat). Para busquedas.';

COMMENT ON COLUMN "reporte_activos"."fecha_nac" IS 'Fecha nacimiento';

COMMENT ON COLUMN "reporte_activos"."sexo" IS 'Sexo de la persona';

