DROP TABLE "zona" CASCADE;


CREATE TABLE "zona"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "zona_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "zona" IS 'Tabla de zonas ';

INSERT INTO zona (nombre) VALUES ('Zona 1');
INSERT INTO zona (nombre) VALUES ('Zona 2');
INSERT INTO zona (nombre) VALUES ('Zona 3');
INSERT INTO zona (nombre) VALUES ('Zona 4');
INSERT INTO zona (nombre) VALUES ('Zona 5');



