-- Table: reporte_asistencias

DROP TABLE reporte_asistencias CASCADE;

CREATE TABLE reporte_asistencias
(
  id INT8  NOT NULL,  -- Identificador del reporte
  centro_id integer NOT NULL, -- Identificador del centro
  curso_id bigint NOT NULL, -- Identificador del curso
  fecha date NOT NULL, -- Fecha en la que finalizo el curso
  socios_registrados integer NOT NULL, -- Cantidad de soscios registrados para el curso
  socios_primera_sesion integer NOT NULL, -- Cantidad de soscios que asistio a la primera sesion
  socios_ultima_sesion integer NOT NULL, -- Cantidad de soscios que asistio a la ultima sesion
  socios_penultima_sesion integer NOT NULL, -- Cantidad de soscios que asistio a la penultima sesion
  socios_imprmieron_certificado integer NOT NULL, -- Cantidad de soscios que asistio que imprimio certificado
  socios_presentaron_examen integer NOT NULL, -- Cantidad de soscios que asistio que presento examen
  CONSTRAINT "reporte_asistencias_pkey" PRIMARY KEY (id),
  CONSTRAINT "reporte_asistencias_FK_1" FOREIGN KEY (centro_id)
      REFERENCES centro (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT "reporte_asistencias_FK_2" FOREIGN KEY (curso_id)
      REFERENCES curso (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT uk_reporte_asistencias UNIQUE (centro_id, curso_id, fecha)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE reporte_asistencias OWNER TO postgres;
COMMENT ON COLUMN reporte_asistencias.id IS 'Identificador del reporte';
COMMENT ON COLUMN reporte_asistencias.centro_id IS 'Identificador del centro';
COMMENT ON COLUMN reporte_asistencias.curso_id IS 'Identificador del curso';
COMMENT ON COLUMN reporte_asistencias.fecha IS 'Fecha en la que finalizo el curso';
COMMENT ON COLUMN reporte_asistencias.socios_registrados IS 'Cantidad de soscios registrados para el curso';
COMMENT ON COLUMN reporte_asistencias.socios_primera_sesion IS 'Cantidad de soscios que asistio a la primera sesion';
COMMENT ON COLUMN reporte_asistencias.socios_ultima_sesion IS 'Cantidad de soscios que asistio a la ultima sesion';
COMMENT ON COLUMN reporte_asistencias.socios_penultima_sesion IS 'Cantidad de soscios que asistio a la penultima sesion';
COMMENT ON COLUMN reporte_asistencias.socios_imprmieron_certificado IS 'Cantidad de soscios que asistio que imprimio certificado';
COMMENT ON COLUMN reporte_asistencias.socios_presentaron_examen IS 'Cantidad de soscios que asistio que presento examen';

