-----------------------------------------------------------------------------
-- reporte_metas
-----------------------------------------------------------------------------

DROP TABLE "reporte_metas" CASCADE;


CREATE TABLE "reporte_metas"
(
	"id" INT8  NOT NULL,
	"centro_id" INTEGER  NOT NULL,
	"categoria_fija_id" INTEGER,
	"categoria_producto_id" INT8,
	"categoria_beca_id" INT8,
	"cantidad" NUMERIC(12,2)  NOT NULL,
	"fecha" DATE  NOT NULL,
	"semana" INTEGER  NOT NULL,
	"anio" INTEGER  NOT NULL,
	"entero" BOOLEAN default 't',
	"meta_diaria" NUMERIC(12,2)  NOT NULL,
	"activo" BOOLEAN default 't',
	"sync" BOOLEAN default 'f',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id")
);

COMMENT ON TABLE "reporte_metas" IS '';


COMMENT ON COLUMN "reporte_metas"."id" IS 'Identificador de meta';

COMMENT ON COLUMN "reporte_metas"."centro_id" IS 'Id de Centro.';

COMMENT ON COLUMN "reporte_metas"."categoria_fija_id" IS 'Categoria de meta Fija (Ej. Meta de socios, meta de inscritos, etc).';

COMMENT ON COLUMN "reporte_metas"."categoria_producto_id" IS 'Categoria de meta de producto(Ej. Meta de gansitos, meta de Cursos de ingles, etc).';

COMMENT ON COLUMN "reporte_metas"."categoria_beca_id" IS 'Categoria de meta de Beca(Ej. Expedición RIA etc).';

COMMENT ON COLUMN "reporte_metas"."cantidad" IS 'Cantidad (numerica o monetaria)';

COMMENT ON COLUMN "reporte_metas"."fecha" IS 'Fecha a la que corresponde el registro';

COMMENT ON COLUMN "reporte_metas"."semana" IS 'Semana del año en que aplica la meta';

COMMENT ON COLUMN "reporte_metas"."anio" IS 'Año en que aplica la meta';

COMMENT ON COLUMN "reporte_metas"."entero" IS 'Indica si la meta es entero o moneda (1 = entero)';

COMMENT ON COLUMN "reporte_metas"."meta_diaria" IS 'Meta diaria (numerica o monetaria)';

