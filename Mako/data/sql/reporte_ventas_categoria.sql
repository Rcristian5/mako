--Tabla para reportar las ventas diarias de centro, es parte de los reportes de indicadores.

DROP TABLE "reporte_ventas_categorias" CASCADE;

CREATE TABLE "reporte_ventas_categorias"
(
	"id" INT8  NOT NULL,
	"centro_id" INTEGER  NOT NULL,
	"fecha" DATE  NOT NULL,
	"semana" INTEGER  NOT NULL,
	"anio" INTEGER  NOT NULL,
	"monto" NUMERIC(12,2) default 0 NOT NULL,
	"cantidad" INTEGER  NOT NULL,
	"tiempo" INTEGER,
	"categoria_id" INTEGER  NOT NULL,
	"categoria" VARCHAR(255)  NOT NULL,
	"pcs" INTEGER  NOT NULL,
	"cursos" INTEGER  NOT NULL,
	"aulas" INTEGER  NOT NULL,
	PRIMARY KEY ("id"),
	CONSTRAINT "uk_reporte_ventas_categorias" UNIQUE ("centro_id","categoria_id","fecha","monto")
);

COMMENT ON TABLE "reporte_ventas_categorias" IS '';

COMMENT ON COLUMN "reporte_ventas_categorias"."id" IS 'Identificador';

COMMENT ON COLUMN "reporte_ventas_categorias"."centro_id" IS 'Id de Centro.';

COMMENT ON COLUMN "reporte_ventas_categorias"."fecha" IS 'Fecha a la que corresponde el registro';

COMMENT ON COLUMN "reporte_ventas_categorias"."semana" IS 'Semana del año en que aplica la meta';

COMMENT ON COLUMN "reporte_ventas_categorias"."anio" IS 'Año en que aplica la meta';

COMMENT ON COLUMN "reporte_ventas_categorias"."monto" IS 'Monto de la venta';

COMMENT ON COLUMN "reporte_ventas_categorias"."cantidad" IS 'Cantidad vendida';

COMMENT ON COLUMN "reporte_ventas_categorias"."tiempo" IS 'Tiempo que se ha vendido';

COMMENT ON COLUMN "reporte_ventas_categorias"."categoria_id" IS 'Identificador de categoría de producto';

COMMENT ON COLUMN "reporte_ventas_categorias"."categoria" IS 'Nombre de la categoría';

COMMENT ON COLUMN "reporte_ventas_categorias"."pcs" IS 'Cantidad de PCs de usuario en el centro a la fecha de registro. (Usado en indicadores)';

COMMENT ON COLUMN "reporte_ventas_categorias"."cursos" IS 'Cantidad de cursos activos a la fecha de registro (Usado en indicadores)';

ALTER TABLE "reporte_ventas_categorias" ADD CONSTRAINT "reporte_ventas_categorias_FK_1" FOREIGN KEY ("centro_id") REFERENCES "centro" ("id");

ALTER TABLE "reporte_ventas_categorias" ADD CONSTRAINT "reporte_ventas_categorias_FK_2" FOREIGN KEY ("categoria_id") REFERENCES "categoria_producto" ("id");

