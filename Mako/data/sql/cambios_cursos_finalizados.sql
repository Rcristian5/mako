--Registros para estatus_curso_finalizado
insert into estatus_curso_finalizado values (4, 'NO REALIZO EXAMEN FINAL', true, '2011-11-21 00:00:00', '2011-11-21 00:00:00');
insert into estatus_curso_finalizado values (5, 'NO HAY EVALUACION FINAL', true, '2011-11-21 00:00:00', '2011-11-21 00:00:00');
insert into estatus_curso_finalizado values (6, 'NO SE ENCONTRO EL REGISTRO', true, '2011-11-21 00:00:00', '2011-11-21 00:00:00');

--Campo para la tabla cursos_finalizados
ALTER TABLE cursos_finalizados ADD COLUMN promedio_final numeric(5,2);