DROP TABLE "dias_operacion" CASCADE;


CREATE TABLE "dias_operacion"
(
	"id" INT8  NOT NULL,
	"centro_id" INTEGER  NOT NULL,
	"fecha" DATE  NOT NULL,
	"opera" BOOLEAN default 'f',
	PRIMARY KEY ("id"),
	CONSTRAINT "uk_dias_operacion" UNIQUE ("centro_id","fecha")
);

COMMENT ON TABLE "dias_operacion" IS '';


COMMENT ON COLUMN "dias_operacion"."id" IS 'Identificador';

COMMENT ON COLUMN "dias_operacion"."centro_id" IS 'Id de Centro.';

COMMENT ON COLUMN "dias_operacion"."fecha" IS 'Fecha a la que corresponde el registro';

COMMENT ON COLUMN "dias_operacion"."opera" IS 'Bandera que indica si el centro opero la fecha o no';

