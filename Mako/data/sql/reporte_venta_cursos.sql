DROP TABLE "reporte_venta_cursos" CASCADE;


CREATE TABLE "reporte_venta_cursos"
(
	"centro_id" INTEGER  NOT NULL,
	"fecha" DATE  NOT NULL,
	"detalle_orden_id" INT8  NOT NULL,
	"categoria_id" INTEGER  NOT NULL,
	"curso_id" INT8,
	"socio_id" INT8,
	"monto" NUMERIC(12,2) default 0 NOT NULL,
	"cantidad" INTEGER  NOT NULL,
	"siglas" VARCHAR(60),
	"categoria_nombre" VARCHAR(120),
	PRIMARY KEY ("centro_id","fecha","detalle_orden_id")
);

COMMENT ON TABLE "reporte_venta_cursos" IS '';


COMMENT ON COLUMN "reporte_venta_cursos"."centro_id" IS 'Id de Centro.';

COMMENT ON COLUMN "reporte_venta_cursos"."fecha" IS 'Fecha a la que corresponde el registro';

COMMENT ON COLUMN "reporte_venta_cursos"."detalle_orden_id" IS 'Identificador de detalle de la orden';

COMMENT ON COLUMN "reporte_venta_cursos"."categoria_id" IS 'Identificador de categoría de curso';

COMMENT ON COLUMN "reporte_venta_cursos"."curso_id" IS 'Identificador de socio activo para la fecha dada';

COMMENT ON COLUMN "reporte_venta_cursos"."socio_id" IS 'Identificador de socio activo para la fecha dada';

COMMENT ON COLUMN "reporte_venta_cursos"."monto" IS 'Monto de la venta';

COMMENT ON COLUMN "reporte_venta_cursos"."cantidad" IS 'Cantidad vendida, (para tomar en cuenta cancelaciones)';

