--Cambio al tipo de dato de grupo_id de la tabla lista_asistencia 

ALTER TABLE lista_asistencia ALTER COLUMN grupo_id TYPE bigint;
ALTER TABLE lista_asistencia ALTER COLUMN id DROP DEFAULT;
