
-----------------------------------------------------------------------------
-- categoria_metas_fijas
-----------------------------------------------------------------------------

DROP TABLE "categoria_metas_fijas" CASCADE;


CREATE TABLE "categoria_metas_fijas"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "categoria_metas_fijas_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "categoria_metas_fijas" IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- metas
-----------------------------------------------------------------------------

DROP TABLE "metas" CASCADE;


CREATE TABLE "metas"
(
	"id" INT8  NOT NULL,
	"centro_id" INTEGER NOT NULL,
	"categoria_fija_id" INTEGER,
	"categoria_producto_id" bigint,
	"categoria_beca_id" bigint,
	"fecha_inicio" DATE  NOT NULL,
	"fecha_fin" DATE,
	"semana" INTEGER  NOT NULL,
	"anio" INTEGER  NOT NULL,
	"meta" NUMERIC(12)  NOT NULL,
	"entero" BOOLEAN default 't',
	"usuario_id" INTEGER,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "uk_metas" UNIQUE ("centro_id","categoria_fija_id","categoria_producto_id","categoria_beca_id","fecha_inicio", "entero")
);

COMMENT ON TABLE "metas" IS '';


COMMENT ON COLUMN "metas"."id" IS 'Identificador de meta';

COMMENT ON COLUMN "metas"."centro_id" IS 'Id de Centro.';

COMMENT ON COLUMN "metas"."categoria_fija_id" IS 'Categoria de meta Fija (Ej. Meta de socios, meta de inscritos, etc).';

COMMENT ON COLUMN "metas"."categoria_producto_id" IS 'Categoria de meta de producto(Ej. Meta de gansitos, meta de Cursos de ingles, etc).';

COMMENT ON COLUMN "metas"."categoria_beca_id" IS 'Categoria de meta de Beca(Ej. Expedición RIA etc).';

COMMENT ON COLUMN "metas"."fecha_inicio" IS 'Fecha de inicio en que aplica la meta';

COMMENT ON COLUMN "metas"."fecha_fin" IS 'Fecha de fin en que aplica la meta';

COMMENT ON COLUMN "metas"."semana" IS 'Semana del año en que aplica la meta';

COMMENT ON COLUMN "metas"."anio" IS 'Año en que aplica la meta';

COMMENT ON COLUMN "metas"."meta" IS 'Cantidad (numerica o monetaria)';

COMMENT ON COLUMN "metas"."entero" IS 'Indica si la meta es entero o moneda (1 = entero)';

COMMENT ON COLUMN "metas"."usuario_id" IS 'Identificador del usuario que capturó el registro';

SET search_path TO public;


ALTER TABLE "metas" ADD CONSTRAINT "metas_FK_1" FOREIGN KEY ("centro_id") REFERENCES "centro" ("id");

ALTER TABLE "metas" ADD CONSTRAINT "metas_FK_2" FOREIGN KEY ("categoria_fija_id") REFERENCES "categoria_metas_fijas" ("id");

ALTER TABLE "metas" ADD CONSTRAINT "metas_FK_3" FOREIGN KEY ("categoria_producto_id") REFERENCES "categoria_reporte" ("id");

ALTER TABLE "metas" ADD CONSTRAINT "metas_FK_4" FOREIGN KEY ("categoria_beca_id") REFERENCES "beca" ("id");

ALTER TABLE "metas" ADD CONSTRAINT "metas_FK_5" FOREIGN KEY ("usuario_id") REFERENCES "usuario" ("id");



--
-- Data for Name: categoria_metas; Type: TABLE DATA; Schema: public; Owner: facturando
--

COPY categoria_metas_fijas (id, nombre, activo, created_at, updated_at) FROM stdin;
1	Socios	t	2010-12-07 14:19:28.96843	\N
2	Inscritos	t	2010-12-07 14:19:36.328655	\N
3	Cobranza	t	2010-12-07 14:20:28.299155	\N
\.




COPY metas (id,centro_id,categoria_fija_id,entero,fecha_inicio,semana,anio,meta)  FROM stdin;
1	1	1	1	'2010-01-01'	1	2010	65
2	2	1	1	'2010-01-01'	1	2010	60
3	3	1	1	'2010-01-01'	1	2010	60
4	4	1	1	'2010-01-01'	1	2010	60
5	5	1	1	'2010-01-01'	1	2010	60
6	6	1	1	'2010-01-01'	1	2010	60
7	7	1	1	'2010-01-01'	1	2010	60
8	8	1	1	'2010-01-01'	1	2010	50
9	9	1	1	'2010-01-01'	1	2010	0
10	10	1	1	'2010-01-01'	1	2010	0
11	11	1	1	'2010-01-01'	1	2010	120
12	12	1	1	'2010-01-01'	1	2010	90
13	13	1	1	'2010-01-01'	1	2010	0
14	14	1	1	'2010-01-01'	1	2010	120
15	15	1	1	'2010-01-01'	1	2010	120
16	16	1	1	'2010-01-01'	1	2010	120
17	17	1	1	'2010-01-01'	1	2010	0
18	18	1	1	'2010-01-01'	1	2010	90
19	19	1	1	'2010-01-01'	1	2010	60
20	20	1	1	'2010-01-01'	1	2010	120
21	21	1	1	'2010-01-01'	1	2010	60
22	22	1	1	'2010-01-01'	1	2010	60
23	23	1	1	'2010-01-01'	1	2010	60
24	24	1	1	'2010-01-01'	1	2010	60
25	25	1	1	'2010-01-01'	1	2010	60
26	26	1	1	'2010-01-01'	1	2010	60
27	27	1	1	'2010-01-01'	1	2010	60
28	28	1	1	'2010-01-01'	1	2010	60
29	29	1	1	'2010-01-01'	1	2010	60
30	30	1	1	'2010-01-01'	1	2010	60
31	31	1	1	'2010-01-01'	1	2010	60
32	32	1	1	'2010-01-01'	1	2010	60
33	1	2	1	'2010-01-01'	1	2010	70
34	2	2	1	'2010-01-01'	1	2010	60
35	3	2	1	'2010-01-01'	1	2010	50
36	4	2	1	'2010-01-01'	1	2010	55
37	5	2	1	'2010-01-01'	1	2010	38
38	6	2	1	'2010-01-01'	1	2010	44
39	7	2	1	'2010-01-01'	1	2010	60
40	8	2	1	'2010-01-01'	1	2010	36
41	9	2	1	'2010-01-01'	1	2010	0
42	10	2	1	'2010-01-01'	1	2010	0
43	11	2	1	'2010-01-01'	1	2010	0
44	12	2	1	'2010-01-01'	1	2010	0
45	13	2	1	'2010-01-01'	1	2010	0
46	14	2	1	'2010-01-01'	1	2010	0
47	15	2	1	'2010-01-01'	1	2010	0
48	16	2	1	'2010-01-01'	1	2010	0
49	17	2	1	'2010-01-01'	1	2010	0
50	18	2	1	'2010-01-01'	1	2010	12
51	19	2	1	'2010-01-01'	1	2010	0
52	20	2	1	'2010-01-01'	1	2010	0
53	21	2	1	'2010-01-01'	1	2010	0
54	22	2	1	'2010-01-01'	1	2010	0
55	23	2	1	'2010-01-01'	1	2010	0
56	24	2	1	'2010-01-01'	1	2010	0
57	25	2	1	'2010-01-01'	1	2010	0
58	26	2	1	'2010-01-01'	1	2010	0
59	27	2	1	'2010-01-01'	1	2010	0
60	28	2	1	'2010-01-01'	1	2010	0
61	29	2	1	'2010-01-01'	1	2010	0
62	30	2	1	'2010-01-01'	1	2010	0
63	31	2	1	'2010-01-01'	1	2010	0
64	32	2	1	'2010-01-01'	1	2010	0
65	1	3	0	'2010-01-01'	1	2010	8000
66	2	3	0	'2010-01-01'	1	2010	5500
67	3	3	0	'2010-01-01'	1	2010	5500
68	4	3	0	'2010-01-01'	1	2010	7000
69	5	3	0	'2010-01-01'	1	2010	4300
70	6	3	0	'2010-01-01'	1	2010	4500
71	7	3	0	'2010-01-01'	1	2010	6000
72	8	3	0	'2010-01-01'	1	2010	5600
73	9	3	0	'2010-01-01'	1	2010	0
74	10	3	0	'2010-01-01'	1	2010	0
75	11	3	0	'2010-01-01'	1	2010	1200
76	12	3	0	'2010-01-01'	1	2010	1200
77	13	3	0	'2010-01-01'	1	2010	0
78	14	3	0	'2010-01-01'	1	2010	1200
79	15	3	0	'2010-01-01'	1	2010	1200
80	16	3	0	'2010-01-01'	1	2010	1200
81	17	3	0	'2010-01-01'	1	2010	0
82	18	3	0	'2010-01-01'	1	2010	900
83	19	3	0	'2010-01-01'	1	2010	0
84	20	3	0	'2010-01-01'	1	2010	1200
85	21	3	0	'2010-01-01'	1	2010	0
86	22	3	0	'2010-01-01'	1	2010	0
87	23	3	0	'2010-01-01'	1	2010	0
88	24	3	0	'2010-01-01'	1	2010	0
89	25	3	0	'2010-01-01'	1	2010	0
90	26	3	0	'2010-01-01'	1	2010	0
91	27	3	0	'2010-01-01'	1	2010	0
92	28	3	0	'2010-01-01'	1	2010	0
93	29	3	0	'2010-01-01'	1	2010	0
94	30	3	0	'2010-01-01'	1	2010	0
95	31	3	0	'2010-01-01'	1	2010	0
96	32	3	0	'2010-01-01'	1	2010	0
\.


--
-- PostgreSQL database dump complete
--


