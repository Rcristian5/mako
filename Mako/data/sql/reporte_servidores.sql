-----------------------------------------------------------------------------
-- reporte_servidores  En esta tabla se guardan los servidores que no pudieron consolidados en los indicadores.
-----------------------------------------------------------------------------

DROP TABLE "reporte_servidores" CASCADE;


CREATE TABLE "reporte_servidores"
(
	"centro_id" INTEGER  NOT NULL,
	"ultima_fecha" DATE  NOT NULL,
	"created_at" TIMESTAMP,
	PRIMARY KEY ("centro_id")
);

COMMENT ON TABLE "reporte_servidores" IS '';


COMMENT ON COLUMN "reporte_servidores"."centro_id" IS 'Id de Centro que no respondio.';

COMMENT ON COLUMN "reporte_servidores"."ultima_fecha" IS 'Ultima fecha sincronizado';

