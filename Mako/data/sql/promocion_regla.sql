ALTER TABLE promocion_regla ADD COLUMN beca_id bigint;
ALTER TABLE promocion_regla ADD CONSTRAINT "promocion_regla_FK11" FOREIGN KEY (beca_id) REFERENCES beca (id)
   ON UPDATE NO ACTION ON DELETE NO ACTION;

