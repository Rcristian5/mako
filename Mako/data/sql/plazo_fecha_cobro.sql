-- Table: plazo_fecha_cobro

DROP TABLE plazo_fecha_cobro;

CREATE TABLE plazo_fecha_cobro
(
  grupo_id bigint NOT NULL, -- Identificador del grupo
  fecha_fin date NOT NULL, -- Fecha de fin del curso
  operador_id integer, -- Identificador del usuario que capturó el registro
  activo boolean DEFAULT true, -- Indica si el plazo para el cobro se encuentra activo o no
  created_at timestamp without time zone,
  updated_at timestamp without time zone,
  CONSTRAINT plazo_fecha_cobro_pkey PRIMARY KEY (grupo_id),
  CONSTRAINT "plazo_fecha_cobro_FK_1" FOREIGN KEY (grupo_id)
      REFERENCES grupo (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT "plazo_fecha_cobro_FK_2" FOREIGN KEY (operador_id)
      REFERENCES usuario (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE SET NULL
)
WITH (
  OIDS=FALSE
);
ALTER TABLE plazo_fecha_cobro OWNER TO postgres;
COMMENT ON COLUMN plazo_fecha_cobro.grupo_id IS 'Identificador del grupo';
COMMENT ON COLUMN plazo_fecha_cobro.fecha_fin IS 'Fecha de fin del curso';
COMMENT ON COLUMN plazo_fecha_cobro.operador_id IS 'Identificador del usuario que capturó el registro';
COMMENT ON COLUMN plazo_fecha_cobro.activo IS 'Indica si el plazo para el cobro se encuentra activo o no';

