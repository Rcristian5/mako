--Modificaciones a tabla de cursos finalizados e inclusión de catálogo de estatus de curso finalizado


DROP TABLE "estatus_curso_finalizado" CASCADE;


CREATE TABLE "estatus_curso_finalizado"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "estatus_curso_finalizado_U_1" UNIQUE ("nombre")
);

COPY estatus_curso_finalizado (id, nombre, activo, created_at, updated_at) FROM stdin;
1	APROBADO	t	2011-01-19 12:00:00	2011-01-19 12:00:00
2	REPROBADO	t	2011-01-19 12:00:00	2011-01-19 12:00:00
3	DESERTADO	t	2011-01-19 12:00:00	2011-01-19 12:00:00
\.


ALTER TABLE cursos_finalizados ADD COLUMN estatus_curso_finalizado_id integer;
COMMENT ON COLUMN cursos_finalizados.estatus_curso_finalizado_id IS 'Identificador del estatus con el que se finalizo el curso';

ALTER TABLE cursos_finalizados
  ADD CONSTRAINT "cursos_finalizados_FK_5" FOREIGN KEY (estatus_curso_finalizado_id)
      REFERENCES estatus_curso_finalizado (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE RESTRICT;

