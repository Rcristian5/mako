--Agrega la columna zona_id a la tabla centro
ALTER TABLE centro ADD COLUMN zona_id integer;
ALTER TABLE centro ALTER COLUMN zona_id SET STORAGE PLAIN;
COMMENT ON COLUMN centro.zona_id IS 'Identificador de zona a la que pertenece el centro';

ALTER TABLE centro
  ADD CONSTRAINT "centro_FK_1" FOREIGN KEY (zona_id)
      REFERENCES centro (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

UPDATE centro SET zona_id = 1 WHERE id IN (9,10,11,12,13,14,15,16,17,19,20);
UPDATE centro SET zona_id = 2 WHERE id IN (3,4,5,6,18,21,27,23,29,36,22);
UPDATE centro SET zona_id = 3 WHERE id IN (7,8,26,30,42,24,25,31,26,32,28);
UPDATE centro SET zona_id = 4 WHERE id IN (1,2,33,41,40,35);
UPDATE centro SET zona_id = 5 WHERE id IN (37,38,34,39);
