-----------------------------------------------------------------------------
-- reporte_categoria_cursos
-----------------------------------------------------------------------------

DROP TABLE "reporte_categoria_cursos" CASCADE;


CREATE TABLE "reporte_categoria_cursos"
(
	"centro_id" INTEGER  NOT NULL,
	"fecha" DATE  NOT NULL,
	"semana" INTEGER  NOT NULL,
	"anio" INTEGER  NOT NULL,
	"monto" NUMERIC(12,2) default 0 NOT NULL,
	"cantidad" INTEGER  NOT NULL,
	"categoria_id" INTEGER  NOT NULL,
	"siglas" VARCHAR(60),
	"categoria_nombre" VARCHAR(120),
	PRIMARY KEY ("centro_id","fecha","categoria_id")
);

COMMENT ON TABLE "reporte_categoria_cursos" IS '';


COMMENT ON COLUMN "reporte_categoria_cursos"."centro_id" IS 'Id de Centro.';

COMMENT ON COLUMN "reporte_categoria_cursos"."fecha" IS 'Fecha a la que corresponde el registro';

COMMENT ON COLUMN "reporte_categoria_cursos"."semana" IS 'Semana del año en que se realizo la venta';

COMMENT ON COLUMN "reporte_categoria_cursos"."anio" IS 'Año en que se realizo la venta';

COMMENT ON COLUMN "reporte_categoria_cursos"."monto" IS 'Monto de la venta';

COMMENT ON COLUMN "reporte_categoria_cursos"."cantidad" IS 'Cantidad vendida';

COMMENT ON COLUMN "reporte_categoria_cursos"."categoria_id" IS 'Identificador de categoría de curso';

