-----------------------------------------------------------------------------
-- reporte_indicadores_dia
-----------------------------------------------------------------------------

DROP TABLE "reporte_indicadores_dia" CASCADE;


CREATE TABLE "reporte_indicadores_dia"
(
	"id" INT8  NOT NULL,
	"centro_id" INTEGER  NOT NULL,
	"fecha" DATE  NOT NULL,
	"semana" INTEGER  NOT NULL,
	"anio" INTEGER  NOT NULL,
	"registros_nuevos" INTEGER  NOT NULL,
	"socios_registrados" INTEGER  NOT NULL,
	"socios_activos" INTEGER  NOT NULL,
	"socios_activos_nuevos" INTEGER  NOT NULL,
	"cobranza_educacion" DECIMAL(10,2)  NOT NULL,
	"cobranza_otros" DECIMAL(10,2)  NOT NULL,
	"cobranza_internet" DECIMAL(10,2)  NOT NULL,
	"cobranza_total" DECIMAL(10,2)  NOT NULL,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id","centro_id")
);

COMMENT ON TABLE "reporte_indicadores_dia" IS '';
COMMENT ON COLUMN "reporte_indicadores_dia"."id" IS 'Identificador de meta';
COMMENT ON COLUMN "reporte_indicadores_dia"."centro_id" IS 'Id de Centro.';
COMMENT ON COLUMN "reporte_indicadores_dia"."fecha" IS 'Fecha a la que corresponde el registro';
COMMENT ON COLUMN "reporte_indicadores_dia"."semana" IS 'Semana del año en que aplica la meta';
COMMENT ON COLUMN "reporte_indicadores_dia"."anio" IS 'Año en que aplica la meta';
COMMENT ON COLUMN "reporte_indicadores_dia"."registros_nuevos" IS 'Cantidad de registros nuevos';
COMMENT ON COLUMN "reporte_indicadores_dia"."socios_registrados" IS 'Cantidad de socios a la fecha';
COMMENT ON COLUMN "reporte_indicadores_dia"."socios_activos" IS 'Cantidad de socios activos en el centro';
COMMENT ON COLUMN "reporte_indicadores_dia"."socios_activos_nuevos" IS 'Cantidad de socios activos nuevos en el centro';
COMMENT ON COLUMN "reporte_indicadores_dia"."cobranza_educacion" IS 'Cobranza de los productos relacionados con curso';
COMMENT ON COLUMN "reporte_indicadores_dia"."cobranza_otros" IS 'Cobranza de otros productos que no son internet o curso';
COMMENT ON COLUMN "reporte_indicadores_dia"."cobranza_internet" IS 'Cobranza de internet';
COMMENT ON COLUMN "reporte_indicadores_dia"."cobranza_total" IS 'Cobranza de todos los productos vendidos';
SET search_path TO public;
-----------------------------------------------------------------------------
-- reporte_indicadores_dia_socios_activos
-----------------------------------------------------------------------------
DROP TABLE "reporte_indicadores_dia_socios_activos" CASCADE;
CREATE TABLE "reporte_indicadores_dia_socios_activos"
(
	"id" INT8  NOT NULL,
	"reporte_indicador_dia_id" INT8  NOT NULL,
	"centro_id" INTEGER  NOT NULL,
	"socio_id" INT8  NOT NULL,
	"usuario" VARCHAR(90)  NOT NULL,
	"nombre_completo" VARCHAR(200)  NOT NULL,
	"edad" INTEGER  NOT NULL,
	"ocupacion_id" INTEGER,
	"sexo" CHAR(1)  NOT NULL,
	PRIMARY KEY ("id","reporte_indicador_dia_id","centro_id")
);
COMMENT ON TABLE "reporte_indicadores_dia_socios_activos" IS '';
COMMENT ON COLUMN "reporte_indicadores_dia_socios_activos"."id" IS 'Identificador de meta';
COMMENT ON COLUMN "reporte_indicadores_dia_socios_activos"."reporte_indicador_dia_id" IS 'Id del reporte indicador.';
COMMENT ON COLUMN "reporte_indicadores_dia_socios_activos"."centro_id" IS 'Id de Centro.';
COMMENT ON COLUMN "reporte_indicadores_dia_socios_activos"."socio_id" IS 'Id del socio.';
COMMENT ON COLUMN "reporte_indicadores_dia_socios_activos"."usuario" IS 'Nombre de usuario usado en el login del sistema';
COMMENT ON COLUMN "reporte_indicadores_dia_socios_activos"."nombre_completo" IS 'Nombre completo (nombre, apepat, apemat). Para busquedas.';
COMMENT ON COLUMN "reporte_indicadores_dia_socios_activos"."edad" IS 'Edad del Socio';
COMMENT ON COLUMN "reporte_indicadores_dia_socios_activos"."sexo" IS 'Sexo de la persona';
SET search_path TO public;
-----------------------------------------------------------------------------
-- reporte_indicadores_dia_socios_activos_nuevos
-----------------------------------------------------------------------------
DROP TABLE "reporte_indicadores_dia_socios_activos_nuevos" CASCADE;
CREATE TABLE "reporte_indicadores_dia_socios_activos_nuevos"
(
	"id" INT8  NOT NULL,
	"reporte_indicador_dia_id" INT8  NOT NULL,
	"centro_id" INTEGER  NOT NULL,
	"socio_id" INT8  NOT NULL,
	"usuario" VARCHAR(90)  NOT NULL,
	"nombre_completo" VARCHAR(200)  NOT NULL,
	"edad" INTEGER  NOT NULL,
	"ocupacion_id" INTEGER,
	"sexo" CHAR(1)  NOT NULL,
	PRIMARY KEY ("id","reporte_indicador_dia_id","centro_id")
);
COMMENT ON TABLE "reporte_indicadores_dia_socios_activos_nuevos" IS '';
COMMENT ON COLUMN "reporte_indicadores_dia_socios_activos_nuevos"."id" IS 'Identificador de meta';
COMMENT ON COLUMN "reporte_indicadores_dia_socios_activos_nuevos"."reporte_indicador_dia_id" IS 'Id del reporte indicador.';
COMMENT ON COLUMN "reporte_indicadores_dia_socios_activos_nuevos"."centro_id" IS 'Id de Centro.';
COMMENT ON COLUMN "reporte_indicadores_dia_socios_activos_nuevos"."socio_id" IS 'Id del socio.';
COMMENT ON COLUMN "reporte_indicadores_dia_socios_activos_nuevos"."usuario" IS 'Nombre de usuario usado en el login del sistema';
COMMENT ON COLUMN "reporte_indicadores_dia_socios_activos_nuevos"."nombre_completo" IS 'Nombre completo (nombre, apepat, apemat). Para busquedas.';
COMMENT ON COLUMN "reporte_indicadores_dia_socios_activos_nuevos"."edad" IS 'Edad del Socio';
COMMENT ON COLUMN "reporte_indicadores_dia_socios_activos_nuevos"."sexo" IS 'Sexo de la persona';
SET search_path TO public;
-----------------------------------------------------------------------------
-- reporte_indicadores_dia_grupo
-----------------------------------------------------------------------------
DROP TABLE "reporte_indicadores_dia_grupo" CASCADE;
CREATE TABLE "reporte_indicadores_dia_grupo"
(
	"id" INT8  NOT NULL,
	"centro_id" INTEGER  NOT NULL,
	"grupo_id" INT8  NOT NULL,
	"curso_id" INT8  NOT NULL,
	"facilitador_id" INTEGER  NOT NULL,
	"categoria_curso_id" INTEGER  NOT NULL,
	"aula_id" INT8  NOT NULL,
	"fecha" DATE  NOT NULL,
	"semana" INTEGER  NOT NULL,
	"anio" INTEGER  NOT NULL,
	"socios_preinscritos" INTEGER  NOT NULL,
	"socios_inscritos" INTEGER  NOT NULL,
	"socios_inscritos_nuevos" INTEGER  NOT NULL,
	"socios_inscritos_recurrentes" INTEGER  NOT NULL,
	"alumnos_activos" INTEGER  NOT NULL,
	"alumnos_actividad_programada" INTEGER  NOT NULL,
	"alumnos_graduados" INTEGER  NOT NULL,
        "alumnos_reprobados" INTEGER  NOT NULL,
	"graduacion_esperada" INTEGER  NOT NULL,
	"alumnos_desertores" INTEGER  NOT NULL,
	"pagos_programados" INTEGER  NOT NULL,
	"pagos_cobrados" INTEGER  NOT NULL,
	"cobranza_programada" DECIMAL(10,2)  NOT NULL,
	"cobranza_cobrada" DECIMAL(10,2)  NOT NULL,
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id","centro_id")
);
COMMENT ON TABLE "reporte_indicadores_dia_grupo" IS '';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo"."id" IS 'Identificador de meta';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo"."centro_id" IS 'Id de Centro.';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo"."grupo_id" IS 'Id del grupo.';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo"."curso_id" IS 'Id del curso.';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo"."facilitador_id" IS 'Id del facilitador.';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo"."categoria_curso_id" IS 'Id de la categoria del curso.';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo"."aula_id" IS 'Id de la categoria del curso.';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo"."fecha" IS 'Fecha a la que corresponde el registro';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo"."semana" IS 'Semana del año en que aplica la meta';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo"."anio" IS 'Año en que aplica la meta';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo"."socios_preinscritos" IS 'Cantidad de socios preincritos en el centro';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo"."socios_inscritos" IS 'Cantidad de socios inscritos en el centro';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo"."socios_inscritos_nuevos" IS 'Cantidad de socios inscritos nuevos en el centro';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo"."socios_inscritos_recurrentes" IS 'Cantidad de socios inscritos recurrentes en el centro';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo"."alumnos_activos" IS 'Cantidad de alumnos activos';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo"."alumnos_graduados" IS 'Cantidad de alumnos graduados';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo"."alumnos_reprobados" IS 'Cantidad de alumnos reprobados';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo"."graduacion_esperada" IS 'Cantidad de graduados que se espera para el mes';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo"."alumnos_desertores" IS 'Cantidad de alumnos desertores';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo"."pagos_programados" IS 'Numero de pagos programados';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo"."pagos_cobrados" IS 'Numero de pagos cobrados';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo"."cobranza_programada" IS 'Cobranza de pagos programados';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo"."cobranza_cobrada" IS 'Cobranza de pagos cobrados';
SET search_path TO public;
-----------------------------------------------------------------------------
-- reporte_indicadores_dia_grupo_socios_inscritos
-----------------------------------------------------------------------------
DROP TABLE "reporte_indicadores_dia_grupo_socios_inscritos" CASCADE;
CREATE TABLE "reporte_indicadores_dia_grupo_socios_inscritos"
(
	"id" INT8  NOT NULL,
	"reporte_indicador_dia_grupo_id" INT8  NOT NULL,
	"centro_id" INTEGER  NOT NULL,
	"socio_id" INT8  NOT NULL,
	"usuario" VARCHAR(90)  NOT NULL,
	"nombre_completo" VARCHAR(200)  NOT NULL,
	"edad" INTEGER  NOT NULL,
	"ocupacion_id" INTEGER,
	"sexo" CHAR(1)  NOT NULL,
	PRIMARY KEY ("id","reporte_indicador_dia_grupo_id","centro_id")
);
COMMENT ON TABLE "reporte_indicadores_dia_grupo_socios_inscritos" IS '';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo_socios_inscritos"."id" IS 'Identificador de meta';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo_socios_inscritos"."reporte_indicador_dia_grupo_id" IS 'Id del reporte indicador por grupo.';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo_socios_inscritos"."centro_id" IS 'Id de Centro.';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo_socios_inscritos"."socio_id" IS 'Id del socio.';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo_socios_inscritos"."usuario" IS 'Nombre de usuario usado en el login del sistema';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo_socios_inscritos"."nombre_completo" IS 'Nombre completo (nombre, apepat, apemat). Para busquedas.';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo_socios_inscritos"."edad" IS 'Edad del Socio';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo_socios_inscritos"."sexo" IS 'Sexo de la persona';
SET search_path TO public;
-----------------------------------------------------------------------------
-- reporte_indicadores_dia_grupo_socios_preinscritos
-----------------------------------------------------------------------------
DROP TABLE "reporte_indicadores_dia_grupo_socios_preinscritos" CASCADE;
CREATE TABLE "reporte_indicadores_dia_grupo_socios_preinscritos"
(
	"id" INT8  NOT NULL,
	"reporte_indicador_dia_grupo_id" INT8  NOT NULL,
	"centro_id" INTEGER  NOT NULL,
	"socio_id" INT8  NOT NULL,
	"usuario" VARCHAR(90)  NOT NULL,
	"nombre_completo" VARCHAR(200)  NOT NULL,
	"edad" INTEGER  NOT NULL,
	"ocupacion_id" INTEGER,
	"sexo" CHAR(1)  NOT NULL,
	PRIMARY KEY ("id","reporte_indicador_dia_grupo_id","centro_id")
);
COMMENT ON TABLE "reporte_indicadores_dia_grupo_socios_preinscritos" IS '';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo_socios_preinscritos"."id" IS 'Identificador de meta';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo_socios_preinscritos"."reporte_indicador_dia_grupo_id" IS 'Id del reporte indicador por grupo.';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo_socios_preinscritos"."centro_id" IS 'Id de Centro.';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo_socios_preinscritos"."socio_id" IS 'Id del socio.';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo_socios_preinscritos"."usuario" IS 'Nombre de usuario usado en el login del sistema';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo_socios_preinscritos"."nombre_completo" IS 'Nombre completo (nombre, apepat, apemat). Para busquedas.';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo_socios_preinscritos"."edad" IS 'Edad del Socio';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo_socios_preinscritos"."sexo" IS 'Sexo de la persona';
SET search_path TO public;
-----------------------------------------------------------------------------
-- reporte_indicadores_dia_grupo_alumnos_activos
-----------------------------------------------------------------------------
DROP TABLE "reporte_indicadores_dia_grupo_alumnos_activos" CASCADE;
CREATE TABLE "reporte_indicadores_dia_grupo_alumnos_activos"
(
	"id" INT8  NOT NULL,
	"reporte_indicador_dia_grupo_id" INT8  NOT NULL,
	"centro_id" INTEGER  NOT NULL,
	"socio_id" INT8  NOT NULL,
	"usuario" VARCHAR(90)  NOT NULL,
	"nombre_completo" VARCHAR(200)  NOT NULL,
	"edad" INTEGER  NOT NULL,
	"ocupacion_id" INTEGER,
	"sexo" CHAR(1)  NOT NULL,
	PRIMARY KEY ("id","reporte_indicador_dia_grupo_id","centro_id")
);
COMMENT ON TABLE "reporte_indicadores_dia_grupo_alumnos_activos" IS '';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo_alumnos_activos"."id" IS 'Identificador de meta';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo_alumnos_activos"."reporte_indicador_dia_grupo_id" IS 'Id del reporte indicador por grupo.';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo_alumnos_activos"."centro_id" IS 'Id de Centro.';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo_alumnos_activos"."socio_id" IS 'Id del socio.';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo_alumnos_activos"."usuario" IS 'Nombre de usuario usado en el login del sistema';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo_alumnos_activos"."nombre_completo" IS 'Nombre completo (nombre, apepat, apemat). Para busquedas.';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo_alumnos_activos"."edad" IS 'Edad del Socio';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo_alumnos_activos"."sexo" IS 'Sexo de la persona';
SET search_path TO public;

-----------------------------------------------------------------------------
-- reporte_indicadores_dia_grupo_alumnos_actividad_programada
-----------------------------------------------------------------------------
DROP TABLE "reporte_indicadores_dia_grupo_alumnos_actividad_programada" CASCADE;
CREATE TABLE "reporte_indicadores_dia_grupo_alumnos_actividad_programada"
(
	"id" INT8  NOT NULL,
	"reporte_indicador_dia_grupo_id" INT8  NOT NULL,
	"centro_id" INTEGER  NOT NULL,
	"socio_id" INT8  NOT NULL,
	"usuario" VARCHAR(90)  NOT NULL,
	"nombre_completo" VARCHAR(200)  NOT NULL,
	"edad" INTEGER  NOT NULL,
	"ocupacion_id" INTEGER,
	"sexo" CHAR(1)  NOT NULL,
	PRIMARY KEY ("id","reporte_indicador_dia_grupo_id","centro_id")
);
COMMENT ON TABLE "reporte_indicadores_dia_grupo_alumnos_actividad_programada" IS '';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo_alumnos_actividad_programada"."id" IS 'Identificador de meta';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo_alumnos_actividad_programada"."reporte_indicador_dia_grupo_id" IS 'Id del reporte indicador por grupo.';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo_alumnos_actividad_programada"."centro_id" IS 'Id de Centro.';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo_alumnos_actividad_programada"."socio_id" IS 'Id del socio.';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo_alumnos_actividad_programada"."usuario" IS 'Nombre de usuario usado en el login del sistema';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo_alumnos_actividad_programada"."nombre_completo" IS 'Nombre completo (nombre, apepat, apemat). Para busquedas.';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo_alumnos_actividad_programada"."edad" IS 'Edad del Socio';
COMMENT ON COLUMN "reporte_indicadores_dia_grupo_alumnos_actividad_programada"."sexo" IS 'Sexo de la persona';
SET search_path TO public;

ALTER TABLE "reporte_indicadores_dia" ADD CONSTRAINT "reporte_indicadores_dia_FK_1" FOREIGN KEY ("centro_id") REFERENCES "centro" ("id");
ALTER TABLE "reporte_indicadores_dia_socios_activos" ADD CONSTRAINT "reporte_indicadores_dia_soc_FK_1" FOREIGN KEY ("ocupacion_id") REFERENCES "ocupacion" ("id");
ALTER TABLE "reporte_indicadores_dia_socios_activos" ADD CONSTRAINT "reporte_indicadores_dia_socios_activos_FK_1" FOREIGN KEY ("reporte_indicador_dia_id","centro_id") REFERENCES "reporte_indicadores_dia" ("id","centro_id");
ALTER TABLE "reporte_indicadores_dia_socios_activos_nuevos" ADD CONSTRAINT "reporte_indicadores_dia_soc_FK_1" FOREIGN KEY ("ocupacion_id") REFERENCES "ocupacion" ("id");
ALTER TABLE "reporte_indicadores_dia_socios_activos_nuevos" ADD CONSTRAINT "reporte_indicadores_dia_socios_activos_nuevos_FK_1" FOREIGN KEY ("reporte_indicador_dia_id","centro_id") REFERENCES "reporte_indicadores_dia" ("id","centro_id");
ALTER TABLE "reporte_indicadores_dia_grupo" ADD CONSTRAINT "reporte_indicadores_dia_gru_FK_1" FOREIGN KEY ("centro_id") REFERENCES "centro" ("id");
ALTER TABLE "reporte_indicadores_dia_grupo" ADD CONSTRAINT "reporte_indicadores_dia_gru_FK_2" FOREIGN KEY ("grupo_id") REFERENCES "grupo" ("id");
ALTER TABLE "reporte_indicadores_dia_grupo" ADD CONSTRAINT "reporte_indicadores_dia_gru_FK_3" FOREIGN KEY ("curso_id") REFERENCES "curso" ("id");
ALTER TABLE "reporte_indicadores_dia_grupo" ADD CONSTRAINT "reporte_indicadores_dia_gru_FK_4" FOREIGN KEY ("facilitador_id") REFERENCES "usuario" ("id");
ALTER TABLE "reporte_indicadores_dia_grupo" ADD CONSTRAINT "reporte_indicadores_dia_gru_FK_5" FOREIGN KEY ("categoria_curso_id") REFERENCES "categoria_curso" ("id");
ALTER TABLE "reporte_indicadores_dia_grupo" ADD CONSTRAINT "reporte_indicadores_dia_gru_FK_6" FOREIGN KEY ("aula_id") REFERENCES "aula" ("id");
ALTER TABLE "reporte_indicadores_dia_grupo_socios_inscritos" ADD CONSTRAINT "reporte_indicadores_dia_gru_FK_1" FOREIGN KEY ("ocupacion_id") REFERENCES "ocupacion" ("id");
ALTER TABLE "reporte_indicadores_dia_grupo_socios_inscritos" ADD CONSTRAINT "reporte_indicadores_dia_grupo_ins_FK_1" FOREIGN KEY ("reporte_indicador_dia_grupo_id","centro_id") REFERENCES "reporte_indicadores_dia_grupo" ("id","centro_id");
ALTER TABLE "reporte_indicadores_dia_grupo_socios_preinscritos" ADD CONSTRAINT "reporte_indicadores_dia_grupo_pre_FK_1" FOREIGN KEY ("ocupacion_id") REFERENCES "ocupacion" ("id");
ALTER TABLE "reporte_indicadores_dia_grupo_socios_preinscritos" ADD CONSTRAINT "reporte_indicadores_dia_ins_FK_1" FOREIGN KEY ("reporte_indicador_dia_grupo_id","centro_id") REFERENCES "reporte_indicadores_dia_grupo" ("id","centro_id");
ALTER TABLE "reporte_indicadores_dia_grupo_alumnos_activos" ADD CONSTRAINT "reporte_indicadores_dia_gru_FK_1" FOREIGN KEY ("ocupacion_id") REFERENCES "ocupacion" ("id");
ALTER TABLE "reporte_indicadores_dia_grupo_alumnos_activos" ADD CONSTRAINT "reporte_indicadores_dia_grupo_act_FK_1" FOREIGN KEY ("reporte_indicador_dia_grupo_id","centro_id") REFERENCES "reporte_indicadores_dia_grupo" ("id","centro_id");
ALTER TABLE "reporte_indicadores_dia_grupo_alumnos_actividad_programada" ADD CONSTRAINT "reporte_indicadores_dia_gru_FK_1" FOREIGN KEY ("ocupacion_id") REFERENCES "ocupacion" ("id");
ALTER TABLE "reporte_indicadores_dia_grupo_alumnos_actividad_programada" ADD CONSTRAINT "reporte_indicadores_dia_grupo_ins_ac_FK_1" FOREIGN KEY ("reporte_indicador_dia_grupo_id","centro_id") REFERENCES "reporte_indicadores_dia_grupo" ("id","centro_id");

