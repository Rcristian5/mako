-----------------------------------------------------------------------------
-- estado_civil
-----------------------------------------------------------------------------

--DROP TABLE "estado_civil" CASCADE;


CREATE TABLE "estado_civil"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "estado_civil_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "estado_civil" IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- servicio_salud
-----------------------------------------------------------------------------

--DROP TABLE "servicio_salud" CASCADE;


CREATE TABLE "servicio_salud"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "servicio_salud_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "servicio_salud" IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- vivienda_tipo
-----------------------------------------------------------------------------

--DROP TABLE "vivienda_tipo" CASCADE;


CREATE TABLE "vivienda_tipo"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "vivienda_tipo_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "vivienda_tipo" IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- vivienda_es
-----------------------------------------------------------------------------

--DROP TABLE "vivienda_es" CASCADE;


CREATE TABLE "vivienda_es"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "vivienda_es_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "vivienda_es" IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- material_piso
-----------------------------------------------------------------------------

--DROP TABLE "material_piso" CASCADE;


CREATE TABLE "material_piso"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "material_piso_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "material_piso" IS '';


SET search_path TO public;
-----------------------------------------------------------------------------
-- material_techo
-----------------------------------------------------------------------------

--DROP TABLE "material_techo" CASCADE;


CREATE TABLE "material_techo"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "material_techo_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "material_techo" IS '';


SET search_path TO public;

-----------------------------------------------------------------------------
-- uso_automovil
-----------------------------------------------------------------------------

--DROP TABLE "uso_automovil" CASCADE;


CREATE TABLE "uso_automovil"
(
	"id" serial  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "uso_automovil_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "uso_automovil" IS '';


SET search_path TO public;

-----------------------------------------------------------------------------
-- socio
-----------------------------------------------------------------------------

ALTER TABLE "socio"

	ADD COLUMN "estado_civil_id" INTEGER,
	ADD COLUMN "personas_vivienda" INTEGER default 0,
	ADD COLUMN "servicio_salud_id" INTEGER,
	ADD COLUMN "vivienda_tipo_id" INTEGER,
	ADD COLUMN "vivienda_es_id" INTEGER,
	ADD COLUMN "material_techo_id" INTEGER,
	ADD COLUMN "material_piso_id" INTEGER,
	ADD COLUMN "habitaciones" INTEGER default 0,
	ADD COLUMN "banos" INTEGER default 0,
	ADD COLUMN "focos" INTEGER default 0,
	ADD COLUMN "anios_residencia" INTEGER default 0,
	ADD COLUMN "meses_residencia" INTEGER default 0,
	ADD COLUMN "energia_electrica" BOOLEAN default 'f',
	ADD COLUMN "agua_entubada" BOOLEAN default 'f',
	ADD COLUMN "drenaje" BOOLEAN default 'f',
	ADD COLUMN "linea_telefonica" BOOLEAN default 'f',
	ADD COLUMN "television_paga" BOOLEAN default 'f',
	ADD COLUMN "internet" BOOLEAN default 'f',
	ADD COLUMN "lavadora" BOOLEAN default 'f',
	ADD COLUMN "secadora" BOOLEAN default 'f',
	ADD COLUMN "television" BOOLEAN default 'f',
	ADD COLUMN "calentador_agua" BOOLEAN default 'f',
	ADD COLUMN "horno_microondas" BOOLEAN default 'f',
	ADD COLUMN "tostador" BOOLEAN default 'f',
	ADD COLUMN "reproductor_video" BOOLEAN default 'f',
	ADD COLUMN "computadora" BOOLEAN default 'f',
	ADD COLUMN "telefono_celular" BOOLEAN default 'f',
	ADD COLUMN "reproductor_audio" BOOLEAN default 'f',
	ADD COLUMN "fregadero" BOOLEAN default 'f',
	ADD COLUMN "estufa" BOOLEAN default 'f',
	ADD COLUMN "refrigerador" BOOLEAN default 'f',
	ADD COLUMN "licuadora" BOOLEAN default 'f',
	ADD COLUMN "automoviles" INTEGER default 0,
	ADD COLUMN "automovil_propio" BOOLEAN default 'f',
	ADD COLUMN "uso_automovil_id" INTEGER,
	ADD COLUMN "socio_madre_id" INT8,
	ADD COLUMN "socio_madre_vive" BOOLEAN default 't',
	ADD COLUMN "socio_padre_id" INT8,
	ADD COLUMN "socio_padre_vive" BOOLEAN default 't'
;

COMMENT ON COLUMN "socio"."estado_civil_id" IS 'Estado civil.';

COMMENT ON COLUMN "socio"."personas_vivienda" IS 'Número de personas con las que comparte vivienda.';

COMMENT ON COLUMN "socio"."servicio_salud_id" IS 'Servicio de salud de la que es beneficiario.';

COMMENT ON COLUMN "socio"."vivienda_tipo_id" IS 'Tipo de vivienda.';

COMMENT ON COLUMN "socio"."vivienda_es_id" IS 'La vivienda que habita es.';

COMMENT ON COLUMN "socio"."material_techo_id" IS 'Material del techo de la vivienda.';

COMMENT ON COLUMN "socio"."material_piso_id" IS 'Material del piso de la vivienda.';

COMMENT ON COLUMN "socio"."habitaciones" IS 'Número de habitaciones.';

COMMENT ON COLUMN "socio"."banos" IS 'Número de baños.';

COMMENT ON COLUMN "socio"."focos" IS 'Número de focos.';

COMMENT ON COLUMN "socio"."anios_residencia" IS 'Años de residir en el domicilio.';

COMMENT ON COLUMN "socio"."meses_residencia" IS 'Meses de residir en el domicilio.';

COMMENT ON COLUMN "socio"."energia_electrica" IS 'Servicios con que cuenta la vivienda. Energía electrica.';

COMMENT ON COLUMN "socio"."agua_entubada" IS 'Servicios con que cuenta la vivienda. Agua entubada.';

COMMENT ON COLUMN "socio"."drenaje" IS 'Servicios con que cuenta la vivienda. Drenaje.';

COMMENT ON COLUMN "socio"."linea_telefonica" IS 'Servicios con que cuenta la vivienda. Linea Telefonica.';

COMMENT ON COLUMN "socio"."television_paga" IS 'Servicios con que cuenta la vivienda. Televisión de paga.';

COMMENT ON COLUMN "socio"."internet" IS 'Servicios con que cuenta la vivienda. Internet.';

COMMENT ON COLUMN "socio"."lavadora" IS 'Enseres. Lavadora.';

COMMENT ON COLUMN "socio"."secadora" IS 'Enseres. Secadora.';

COMMENT ON COLUMN "socio"."television" IS 'Enseres. Televisión.';

COMMENT ON COLUMN "socio"."calentador_agua" IS 'Enseres. Calentador de agua.';

COMMENT ON COLUMN "socio"."horno_microondas" IS 'Enseres. Horno Microondas.';

COMMENT ON COLUMN "socio"."tostador" IS 'Enseres. Tostador de pan.';

COMMENT ON COLUMN "socio"."reproductor_video" IS 'Enseres. Reproductor de video.';

COMMENT ON COLUMN "socio"."computadora" IS 'Enseres. Computadora.';

COMMENT ON COLUMN "socio"."telefono_celular" IS 'Enseres. Teléfono celular.';

COMMENT ON COLUMN "socio"."reproductor_audio" IS 'Enseres. Reproductor de audio.';

COMMENT ON COLUMN "socio"."fregadero" IS 'Enseres. Fregadero con agua.';

COMMENT ON COLUMN "socio"."estufa" IS 'Enseres. Estufa con horno.';

COMMENT ON COLUMN "socio"."refrigerador" IS 'Enseres. Refrigerador.';

COMMENT ON COLUMN "socio"."licuadora" IS 'Enseres. Licuadora.';

COMMENT ON COLUMN "socio"."automoviles" IS 'Cuántos automóviles tienen en casa.';

COMMENT ON COLUMN "socio"."automovil_propio" IS '¿Tiene automóvil propio?.';

COMMENT ON COLUMN "socio"."uso_automovil_id" IS 'Uso que le da al automóvil.';

COMMENT ON COLUMN "socio"."socio_madre_vive" IS 'Vive (s/n).';

COMMENT ON COLUMN "socio"."socio_padre_vive" IS 'Vive (s/n).';



ALTER TABLE "socio" ADD CONSTRAINT "socio_FK_27" FOREIGN KEY ("estado_civil_id") REFERENCES "estado_civil" ("id");

ALTER TABLE "socio" ADD CONSTRAINT "socio_FK_28" FOREIGN KEY ("servicio_salud_id") REFERENCES "servicio_salud" ("id");

ALTER TABLE "socio" ADD CONSTRAINT "socio_FK_29" FOREIGN KEY ("vivienda_tipo_id") REFERENCES "vivienda_tipo" ("id");

ALTER TABLE "socio" ADD CONSTRAINT "socio_FK_30" FOREIGN KEY ("vivienda_es_id") REFERENCES "vivienda_es" ("id");

ALTER TABLE "socio" ADD CONSTRAINT "socio_FK_31" FOREIGN KEY ("material_techo_id") REFERENCES "material_techo" ("id");

ALTER TABLE "socio" ADD CONSTRAINT "socio_FK_32" FOREIGN KEY ("material_piso_id") REFERENCES "material_piso" ("id");

ALTER TABLE "socio" ADD CONSTRAINT "socio_FK_33" FOREIGN KEY ("uso_automovil_id") REFERENCES "uso_automovil" ("id");

ALTER TABLE "socio" ADD CONSTRAINT "socio_FK_34" FOREIGN KEY ("socio_madre_id") REFERENCES "socio" ("id");

ALTER TABLE "socio" ADD CONSTRAINT "socio_FK_35" FOREIGN KEY ("socio_padre_id") REFERENCES "socio" ("id");

--
-- Datos de catalogos
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

SELECT pg_catalog.setval('estado_civil_id_seq', 6, true);

COPY estado_civil (id, nombre, activo, created_at, updated_at) FROM stdin;
1	1. Soltero	t	\N	\N
2	2. Casado	t	\N	\N
3	3. Unión libre	t	\N	\N
4	4. Divorciado	t	\N	\N
5	5. Viudo 	t	\N	\N
6	6. Sociedad de convivencia	t	\N	\N
\.


SELECT pg_catalog.setval('servicio_salud_id_seq', 6, true);

COPY servicio_salud (id, nombre, activo, created_at, updated_at) FROM stdin;
1	1. Ninguno	t	\N	\N
2	2. IMSS	t	\N	\N
3	3. ISSSTE	t	\N	\N
4	4. ISSEMyM	t	\N	\N
5	5. Seguro Popular	t	\N	\N
6	6. Otro	t	\N	\N
\.


SELECT pg_catalog.setval('vivienda_tipo_id_seq', 6, true);

COPY vivienda_tipo (id, nombre, activo, created_at, updated_at) FROM stdin;
1	1. Departamento 	t	\N	\N
2	2. Casa independiente	t	\N	\N
3	3. Cuarto de Vecindad	t	\N	\N
4	4. Pensión 	t	\N	\N
5	5. Asistencia Social	t	\N	\N
6	6. Otro	t	\N	\N
\.

SELECT pg_catalog.setval('vivienda_es_id_seq', 5, true);

COPY vivienda_es (id, nombre, activo, created_at, updated_at) FROM stdin;
1	1. Rentada	t	\N	\N
2	2. Familiar	t	\N	\N
3	3. Prestada	t	\N	\N
4	4. Propia	t	\N	\N
5	5. Otros	t	\N	\N
\.

SELECT pg_catalog.setval('material_piso_id_seq', 5, true);

COPY material_piso (id, nombre, activo, created_at, updated_at) FROM stdin;
1	1. Tierra	t	\N	\N
2	2. Cemento	t	\N	\N
3	3. Mosaico / Loseta	t	\N	\N
4	4. Madera	t	\N	\N
5	5. Otro	t	\N	\N
\.

SELECT pg_catalog.setval('material_techo_id_seq', 4, true);

COPY material_techo (id, nombre, activo, created_at, updated_at) FROM stdin;
1	1. Lámina	t	\N	\N
2	2. Concreto	t	\N	\N
3	3. Madera	t	\N	\N
4	4. Otro	t	\N	\N
\.

SELECT pg_catalog.setval('uso_automovil_id_seq', 2, true);

COPY uso_automovil (id, nombre, activo, created_at, updated_at) FROM stdin;
1	1. Familiar	t	\N	\N
2	2. Negocio	t	\N	\N
\.

