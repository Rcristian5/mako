--Cambios a la tabla de cursos, se agrego el campo nombre_corto para mostrar en moodle

ALTER TABLE curso ADD COLUMN nombre_corto VARCHAR(100) NULL;
COMMENT ON COLUMN curso.nombre_corto IS 'Nombre corto del curso';

ALTER TABLE curso
  ADD CONSTRAINT "nombre_corto_ukey" UNIQUE ("nombre_corto");
