-----------------------------------------------------------------------------
-- estatus_socios Tabla de datos para reportes
-----------------------------------------------------------------------------

DROP TABLE "estatus_socios" CASCADE;


CREATE TABLE "estatus_socios"
(
	"centro_id" INTEGER  NOT NULL,
	"fecha" DATE  NOT NULL,
	"nuevos" INTEGER default 0 NOT NULL,
	"activos" INTEGER default 0 NOT NULL,
	PRIMARY KEY ("centro_id","fecha")
);

COMMENT ON TABLE "estatus_socios" IS '';


COMMENT ON COLUMN "estatus_socios"."centro_id" IS 'Id de Centro.';

COMMENT ON COLUMN "estatus_socios"."fecha" IS 'Fecha y hora a la que corresponde el registro';

COMMENT ON COLUMN "estatus_socios"."nuevos" IS 'Nuevos registrados en el centro para esta fecha';

COMMENT ON COLUMN "estatus_socios"."activos" IS 'Socios activos para la fecha';

