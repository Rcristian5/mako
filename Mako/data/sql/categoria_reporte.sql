-----------------------------------------------------------------------------
-- categoria_reporte
-----------------------------------------------------------------------------

DROP TABLE "categoria_reporte" CASCADE;


CREATE TABLE "categoria_reporte"
(
	"id" INT8  NOT NULL,
	"nombre" VARCHAR(255)  NOT NULL,
	"activo" BOOLEAN default 't',
	"created_at" TIMESTAMP,
	"updated_at" TIMESTAMP,
	PRIMARY KEY ("id"),
	CONSTRAINT "categoria_reporte_U_1" UNIQUE ("nombre")
);

COMMENT ON TABLE "categoria_reporte" IS '';


COMMENT ON COLUMN "categoria_reporte"."nombre" IS 'Nombre de la categoria';

COMMENT ON COLUMN "categoria_reporte"."activo" IS 'Bandera de vigencia';

SET search_path TO public;
-----------------------------------------------------------------------------
-- categoria_reporte_producto
-----------------------------------------------------------------------------

DROP TABLE "categoria_reporte_producto" CASCADE;


CREATE TABLE "categoria_reporte_producto"
(
	"categoria_id" INT8  NOT NULL,
	"producto_id" INTEGER  NOT NULL,
	PRIMARY KEY ("categoria_id","producto_id")
);

COMMENT ON TABLE "categoria_reporte_producto" IS '';


COMMENT ON COLUMN "categoria_reporte_producto"."categoria_id" IS 'Identificador de la categoria';

COMMENT ON COLUMN "categoria_reporte_producto"."producto_id" IS 'Identificador del producto que entra en la categoria';

