-----------------------------------------------------------------------------
-- ocupacion_horas
-----------------------------------------------------------------------------

DROP TABLE "ocupacion_horas" CASCADE;


CREATE TABLE "ocupacion_horas"
(
	"id" INT8  NOT NULL,
	"centro_id" INTEGER  NOT NULL,
	"fecha_hora" TIMESTAMP  NOT NULL,
	"fecha" DATE  NOT NULL,
	"hora" TIME  NOT NULL,
	"consumo" NUMERIC(12,2) default 0,
	"sesiones" INTEGER default 0,
	"capacidad_hora" INT8 default 0,
	PRIMARY KEY ("id"),
	CONSTRAINT "uk_consumo_horas" UNIQUE ("centro_id","fecha_hora")
);

COMMENT ON TABLE "ocupacion_horas" IS '';
COMMENT ON COLUMN "ocupacion_horas"."id" IS 'Identificador';

COMMENT ON COLUMN "ocupacion_horas"."centro_id" IS 'Id de Centro.';

COMMENT ON COLUMN "ocupacion_horas"."fecha_hora" IS 'Fecha y hora a la que corresponde el registro';

COMMENT ON COLUMN "ocupacion_horas"."fecha" IS 'Fecha a la que corresponde el registro';

COMMENT ON COLUMN "ocupacion_horas"."hora" IS 'Hora a la que corresponde el registro';

COMMENT ON COLUMN "ocupacion_horas"."consumo" IS 'Minutos consumidos en la hora en ese centro';

COMMENT ON COLUMN "ocupacion_horas"."sesiones" IS 'Cantidad de sesiones presentes en esa hora en el centro';

COMMENT ON COLUMN "ocupacion_horas"."capacidad_hora" IS 'Capacidad en minutos del centro, por cada hora (PCs*60 segs)';

ALTER TABLE "ocupacion_horas" ADD CONSTRAINT "ocupacion_horas_FK_1" FOREIGN KEY ("centro_id") REFERENCES "centro" ("id");


