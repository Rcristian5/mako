<?php
$ips_err = "";

$dbh = new PDO('pgsql:host=10.250.0.38;port=5432;dbname=mako', 'postgres', null);
$sth = $dbh->prepare("
			SELECT id, ip, alias, nombre 
			FROM centro 
			WHERE 
				id <> 9999 
				AND ip NOT IN ('10.1.160.4','10.1.140.4','10.1.181.4','10.1.150.4')
			");
$sth->execute();

$centros = $sth->fetchAll(PDO::FETCH_ASSOC);
$centros = array(array("id"=>2, "ip"=>"10.1.1.4",  'alias'=>'tolu2', 'nombre' => 'Toluca 2'));
//print_r($centros);
$sth = null; $dbh = null;

$db_name = "mako";
foreach ($centros as $centro)
{
	//print("\n\t\tRESPALDANDO TABLAS");
	//exec("pg_dump -U postgres $db_name -t factura -t tr_corte_factura -t tr_factura_orden -t sincronia_timbrado > ~/DUMP_" . $centro['alias'] . "_" . date("Y-M-d") . ".sql");
	//print("\n\t\tTermino respaldo...!!!");
	$dbh = new PDO(sprintf("pgsql:host=%s;port=5432;dbname=%s", $centro['ip'], $db_name), 'postgres', null);

	/*$sth = $dbh->prepare("delete from sincronia_timbrado where id in (select id from sincronia_timbrado where (SPLIT_PART(RIGHT(factura,LENGTH(factura)-(select length(rfc) from centro limit 1) - (select length(serie)::INTEGER from folio_fiscal)),'.',1)::INTEGER) IN (SELECT folio FROM factura f INNER JOIN corte c ON f.id = c.factura_id)); ");
	$sth->execute();
	$sth = $dbh->prepare("delete from sincronia_timbrado where id in (select id from sincronia_timbrado where (SPLIT_PART(RIGHT(factura,LENGTH(factura)-(select length(rfc) from centro limit 1) - (select length(serie)::INTEGER from folio_fiscal)),'.',1)::INTEGER) NOT IN (SELECT f.folio FROM factura f INNER JOIN orden c ON f.id = c.factura_id));");
	$sth->execute();

	$sth = $dbh->prepare("delete from sincronia_sap where operacion = 'Pagos';");
	$sth->execute();
	$sth = $dbh->prepare("delete from sincronia_sap where operacion = 'Facturas';");
	$sth->execute();
	$sth = $dbh->prepare("update sincronia_timbrado set operacion = 'publishWS', cadena_cfdi=null");
	$sth->execute();

	$sth = $dbh->prepare("SELECT factura_id FROM orden o WHERE created_at::DATE >= '2016-06-01' AND o.es_factura;");
	$sth->execute();
	while ($fact = $sth->fetch(PDO::FETCH_ASSOC)) {
		if($fact['factura_id'])
			$id_facturas[] = $fact['factura_id'];
	}
	try {
		$sth = $dbh->prepare("DELETE FROM tr_factura_orden WHERE factura_id NOT IN (" . implode(",",$id_facturas) .");");
		$sth->execute();
	}catch(Exception $e) {
		print_r($e);
	}

	$sth = $dbh->prepare(" DELETE FROM tr_corte_factura");
	$sth->execute();

	$sth = $dbh->prepare("UPDATE corte SET factura_id = Null WHERE created_at::DATE >= '2016-06-01' ");
	$sth->execute();

	$qry = "DELETE FROM factura WHERE id NOT IN (" . implode(",", $id_facturas) . ") AND fecha_factura::DATE >= '2016-06-01'";
	var_dump($qry);
	try {
		$sth = $dbh->prepare($qry);
		$sth->execute();
	}catch(Exception $e)
	{
		print_r($e);
	}*/

	$sth = $dbh->prepare("SELECT MAX(folio) as folio FROM factura WHERE fecha_factura < '2016-06-01';");
	$sth->execute();
	$ult_folio = $sth->fetchAll(PDO::FETCH_ASSOC);
	//print("ULT_FOLIO => " . print_r($ult_folio[0],true));

	/*$sth = $dbh->prepare("SELECT f.folio FROM factura f INNER JOIN orden ord ON (ord.factura_id = f.id) AND f.fecha_factura::DATE >= '2016-06-01' ");
	$sth->execute();
	$folios_vta = $sth->fetchAll(PDO::FETCH_ASSOC);
	print("FOLIOS_VTA => " . print_r($folios_vta,true));*/

	$sth=$dbh->prepare("
SELECT
	d.fecha_corte
	, d.hora_corte
	, d.corte_id
	, SUM(factura_vta_tasa0)::INTEGER factura_vta_tasa0
	, SUM(factura_vta_tasa16)::INTEGER factura_vta_tasa16
	, SUM(factura_corte_tasa0)::INTEGER factura_corte_tasa0
	, SUM(factura_corte_tasa16)::INTEGER factura_corte_tasa16
FROM
(
	SELECT 
		CAST(c.created_at AS DATE) fecha_corte
		, CAST(c.created_at AS TIME) hora_corte
		, c.id corte_id
		, o.id orden_id
		, (CASE WHEN o.es_factura AND TRIM(UPPER(cp.nombre)) = 'LIBROS' THEN 1 ELSE 0 END) factura_vta_tasa0
		,(CASE WHEN o.es_factura AND TRIM(UPPER(cp.nombre)) <> 'LIBROS' THEN 1 ELSE 0 END) factura_vta_tasa16
		, (CASE WHEN NOT o.es_factura AND TRIM(UPPER(cp.nombre)) = 'LIBROS' THEN 1 ELSE 0 END) factura_corte_tasa0
		, (CASE WHEN NOT o.es_factura AND TRIM(UPPER(cp.nombre)) <> 'LIBROS' THEN 1 ELSE 0 END) factura_corte_tasa16
	FROM corte c
	INNER JOIN orden o ON (o.corte_id = c.id)
	INNER JOIN detalle_orden deto ON (deto.orden_id = o.id)
	INNER JOIN producto p ON (p.id = deto.producto_id)
	INNER JOIN categoria_producto cp oN (cp.id = p.categoria_id)
	WHERE c.created_at::DATE >= '2016-06-01'
	GROUP BY
		CAST(c.created_at AS DATE)
		, CAST(c.created_at AS TIME)
		, c.id, o.id, o.es_factura, cp.nombre
)d
GROUP BY 
	d.fecha_corte
	,d.hora_corte
	,d.corte_id;
		");
	$sth->execute();
	$cortes = $sth->fetchAll(PDO::FETCH_ASSOC);
	//print("CORTES => " . print_r($cortes,true));
	$folios_cortes = array();
	$folio_consec = intval($ult_folio[0]['folio']);
	$cortes_con_folios = array();
	foreach ($cortes as $corte) {
		$folio_tasa16 = 0;
		$folio_tasa0 = 0;
		if(intval($corte['factura_vta_tasa16'])){
			$folio_consec = $folio_consec + intval($corte['factura_vta_tasa16']);
			//print ("\n\t\tEncontro Ventas Tasa16");
			//print ("\n\t\t\t\tFOLIO_CONSEC => " . $folio_consec);
		}
		if(intval($corte['factura_vta_tasa0'])) {
			$folio_consec = $folio_consec + intval($corte['factura_vta_tasa0']);
			//print ("\n\t\tEncontro Ventas Tasa0");
			//print ("\n\t\t\t\tFOLIO_CONSEC => " . $folio_consec);
		}
		$folio_consec = $folio_consec + 1;
		$folio_tasa16 = $folio_consec;
		//print ("\n\t\t\t\tFOLIO_TASA16 => " . $folio_tasa16);
		if(intval($corte['factura_corte_tasa0'])) {
			$folio_consec = $folio_consec + 1;
			$folio_tasa0 = $folio_consec;
			//print ("\n\t\t\t\tFOLIO_TASA0 => " . $folio_tasa0);
		}
		$corte_final = new stdClass();
		$corte_final->fecha        = $corte['fecha_corte'];
		$corte_final->hora         = $corte['hora_corte'];
		$corte_final->folio_tasa16 = $folio_tasa16;
		//if($folio_tasa0)
			$corte_final->folio_tasa0  = $folio_tasa0;
		$cortes_con_folios[] = $corte_final;
	}

	//print("CORTE_CON_FOLIOS => " . print_r($cortes_con_folios,true));
	foreach ($cortes_con_folios as $corte_final) {
		$cadena=sprintf("php ./symfony mako:regenera-comprobantes-corte --fecha=%s --hora=%s --folio=%s %s"
				, $corte_final->fecha
				, $corte_final->hora
				, $corte_final->folio_tasa16
				, ($corte_final->folio_tasa0>0)? "--folio-tasa0={$corte_final->folio_tasa0}":"");
		/*$cadena= sprintf("ssh root@%s 'php -r \"echo getEnv(\"MAKO_RIA_ENV\");\"'",$centro['ip']);
		$cadena=sprintf("ssh root@%s 'php /var/www/Mako/symfony mako:regenera-comprobantes-corte --fecha=%s --hora=%s --folio=%s %s'"
				, $centro['ip']
				, $corte_final->fecha
				, $corte_final->hora
				, $corte_final->folio_tasa16
				, ($corte_final->folio_tasa0>0)? "--folio-tasa0={$corte_final->folio_tasa0}":"");*/
		exec($cadena);
		print "\n" . $cadena . "\n";
	}

	//$sth = $dbh->prepare(sprintf("UPDATE folio_fiscal SET folio_actual = %d WHERE activo",$folio_consec+1));
	//$sth->execute();
}
$sth = null; $dbh = null;

?>
