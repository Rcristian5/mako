<?php

require_once dirname(__FILE__).'/../lib/vendor/symfony/lib/autoload/sfCoreAutoload.class.php';
sfCoreAutoload::register();

class ProjectConfiguration extends sfProjectConfiguration
{
    public function setup()
    {
        $this->enablePlugins('sfPropelPlugin');
        $this->enablePlugins('sfFormExtraPlugin');
        $this->enablePlugins('ckWebServicePlugin');
        $this->enablePlugins('sfDependentSelectPlugin');
        $this->enablePlugins('sfWebBrowserPlugin');
        $this->enablePlugins('sfTCPDFPlugin');
    }
}
