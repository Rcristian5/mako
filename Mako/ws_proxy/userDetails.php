<?php
/**
 * 
 * 
 * @package	MoodleWS
 * @copyright	(c) P.Pollet 2007 under GPL
 */
class userDetails {
	/** 
	* @var  string
	*/
	public $idnumber;
	/** 
	* @var  string
	*/
	public $username;
	/** 
	* @var  string
	*/
	public $email;
	/** 
	* @var  string
	*/
	public $firstname;
	/** 
	* @var  string
	*/
	public $lastname;
	/* constructor */
	 public function userDetails() {
		 $this->idnumber='';
		 $this->username='';
		 $this->email='';
		 $this->firstname='';
		 $this->lastname='';
	}
}

?>
