<?php
/**
 * 
 * 
 * @package	MoodleWS
 * @copyright	(c) P.Pollet 2007 under GPL
 */
class pendingEnrolment {
	/** 
	* @var  string
	*/
	public $userid;
	/** 
	* @var  string
	*/
	public $firstname;
	/** 
	* @var  string
	*/
	public $lastname;
	/** 
	* @var  string
	*/
	public $username;
	/** 
	* @var  string
	*/
	public $email;
	/** 
	* @var  string
	*/
	public $courseid;
	/** 
	* @var  string
	*/
	public $coursename;
	/** 
	* @var  integer
	*/
	public $enrolmenttime;
	/** 
	* @var  string
	*/
	public $action;
	/* constructor */
	 public function pendingEnrolment() {
		 $this->userid='';
		 $this->firstname='';
		 $this->lastname='';
		 $this->username='';
		 $this->email='';
		 $this->courseid='';
		 $this->coursename='';
		 $this->enrolmenttime=0;
		 $this->action='';
	}
}

?>
