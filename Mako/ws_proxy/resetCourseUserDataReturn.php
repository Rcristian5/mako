<?php
/**
 * 
 * 
 * @package	MoodleWS
 * @copyright	(c) P.Pollet 2007 under GPL
 */
class resetCourseUserDataReturn {
	/** 
	* @var  boolean
	*/
	public $status;
	/** 
	* @var  string
	*/
	public $info;
	/* constructor */
	 public function resetCourseUserDataReturn() {
		 $this->status=false;
		 $this->info='';
	}
}

?>
