<?php
/**
 * 
 * 
 * @package	MoodleWS
 * @copyright	(c) P.Pollet 2007 under GPL
 */
class preenrolStudentsReturn {
	/** 
	* @var  boolean
	*/
	public $status;
	/** 
	* @var  string
	*/
	public $error;
	/* constructor */
	 public function preenrolStudentsReturn() {
		 $this->status=false;
		 $this->error='';
	}
}

?>
