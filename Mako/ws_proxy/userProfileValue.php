<?php
/**
 * 
 * 
 * @package	MoodleWS
 * @copyright	(c) P.Pollet 2007 under GPL
 */
class userProfileValue {
	/** 
	* @var  string
	*/
	public $fieldname;
	/** 
	* @var  string
	*/
	public $fieldvalue;
	/* constructor */
	 public function userProfileValue() {
		 $this->fieldname='';
		 $this->fieldvalue='';
	}
}

?>
