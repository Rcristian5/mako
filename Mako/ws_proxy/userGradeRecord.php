<?php
/**
 * 
 * 
 * @package	MoodleWS
 * @copyright	(c) P.Pollet 2007 under GPL
 */
class userGradeRecord {
	/** 
	* @var  integer
	*/
	public $id;
	/** 
	* @var  string
	*/
	public $idnumber;
	/** 
	* @var  string
	*/
	public $fullname;
	/** 
	* @var  float
	*/
	public $coursegrade;
	/** 
	* @var  string
	*/
	public $courseletter;
	/** 
	* @var  integer
	*/
	public $activitiescompleted;
	/** 
	* @var  activityGradeRecords
	*/
	public $activities;
	/* constructor */
	 public function userGradeRecord() {
		 $this->id=0;
		 $this->idnumber='';
		 $this->fullname='';
		 $this->coursegrade=0.0;
		 $this->courseletter='';
		 $this->activitiescompleted=0;
		 $this->activities=new activityGradeRecords();
	}
}

?>
