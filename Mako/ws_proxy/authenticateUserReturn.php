<?php
/**
 * 
 * 
 * @package	MoodleWS
 * @copyright	(c) P.Pollet 2007 under GPL
 */
class authenticateUserReturn {
	/** 
	* @var  string
	*/
	public $status;
	/** 
	* @var  string
	*/
	public $info;
	/* constructor */
	 public function authenticateUserReturn() {
		 $this->status='';
		 $this->info='';
	}
}

?>
