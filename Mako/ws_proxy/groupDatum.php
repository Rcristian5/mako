<?php
/**
 * 
 * 
 * @package	MoodleWS
 * @copyright	(c) P.Pollet 2007 under GPL
 */
class groupDatum {
	/** 
	* @var  string
	*/
	public $action;
	/** 
	* @var  integer
	*/
	public $id;
	/**
	 * @var string
	 */
	public $name;
	/**
	 * @var string
	 */
	public $description;
	/**
	 * @var string
	 */
	public $enrolmentkey;
	/**
	 * @var integer
	 */
	public $hidepicture;
	/**
	 * @var integer
	 */
	public $courseid;
	
	/* constructor */
	 public function groupDatum() {
		 $this->action='';
		 $this->id=0;
		 $this->name='';
		 $this->description='';
		 $this->enrolmentkey='';
		 $this->hidepicture=0;
		 $this->courseid=0;
	}
}

?>
