<?php
/**
 * 
 * 
 * @package	MoodleWS
 * @copyright	(c) P.Pollet 2007 under GPL
 */
class userLogin {
	/** 
	* @var  string
	*/
	public $username;
	/** 
	* @var  integer
	*/
	public $userid;
	/** 
	* @var  string
	*/
	public $useridnumber;
	/** 
	* @var  string
	*/
	public $coursename;
	/** 
	* @var  integer
	*/
	public $courseid;
	/** 
	* @var  string
	*/
	public $courseidnumber;
	/** 
	* @var  courseLogins
	*/
	public $logins;
	/* constructor */
	 public function userLogin() {
		 $this->username='';
		 $this->userid=0;
		 $this->useridnumber='';
		 $this->coursename='';
		 $this->courseid=0;
		 $this->courseidnumber='';
		 $this->logins=new courseLogins();
	}
}

?>
