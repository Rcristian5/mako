<?php
/**
 * 
 * 
 * @package	MoodleWS
 * @copyright	(c) P.Pollet 2007 under GPL
 */
class activityGradeRecord {
	/** 
	* @var  string
	*/
	public $module;
	/** 
	* @var  integer
	*/
	public $instance;
	/** 
	* @var  string
	*/
	public $name;
	/** 
	* @var  float
	*/
	public $grademin;
	/** 
	* @var  float
	*/
	public $grademax;
	/** 
	* @var  float
	*/
	public $gradepass;
	/** 
	* @var  float
	*/
	public $grade;
	/** 
	* @var  string
	*/
	public $letter;
	/** 
	* @var  string
	*/
	public $strgrade;
	/** 
	* @var  string
	*/
	public $strlonggrade;
	/** 
	* @var  string
	*/
	public $strfeedback;
	/* constructor */
	 public function activityGradeRecord() {
		 $this->module='';
		 $this->instance=0;
		 $this->name='';
		 $this->grademin=0.0;
		 $this->grademax=0.0;
		 $this->gradepass=0.0;
		 $this->grade=0.0;
		 $this->letter='';
		 $this->strgrade='';
		 $this->strlonggrade='';
		 $this->strfeedback='';
	}
}

?>
