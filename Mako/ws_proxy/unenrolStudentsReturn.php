<?php
/**
 * 
 * 
 * @package	MoodleWS
 * @copyright	(c) P.Pollet 2007 under GPL
 */
class unenrolStudentsReturn {
	/** 
	* @var  string
	*/
	public $error;
	/** 
	* @var  studentRecords
	*/
	public $students;
	/* constructor */
	 public function unenrolStudentsReturn() {
		 $this->error='';
		 $this->students=new studentRecords();
	}
}

?>
