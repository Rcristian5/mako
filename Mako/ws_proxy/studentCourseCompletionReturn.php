<?php
/**
 * 
 * 
 * @package	MoodleWS
 * @copyright	(c) P.Pollet 2007 under GPL
 */
class studentCourseCompletionReturn {
	/** 
	* @var  integer
	*/
	public $activitytotal;
	/** 
	* @var  integer
	*/
	public $activitycomplete;
	/* constructor */
	 public function studentCourseCompletionReturn() {
		 $this->activitytotal=0;
		 $this->activitycomplete=0;
	}
}

?>
