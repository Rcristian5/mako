<?php
/**
 * 
 * 
 * @package	MoodleWS
 * @copyright	(c) P.Pollet 2007 under GPL
 */
class courseGradeRecord {
	/** 
	* @var  string
	*/
	public $error;
	/** 
	* @var  integer
	*/
	public $id;
	/** 
	* @var  string
	*/
	public $idnumber;
	/** 
	* @var  string
	*/
	public $shortname;
	/** 
	* @var  string
	*/
	public $longname;
	/** 
	* @var  string
	*/
	public $activitiestotal;
	/** 
	* @var  userGradeRecords
	*/
	public $users;
	/* constructor */
	 public function courseGradeRecord() {
		 $this->error='';
		 $this->id=0;
		 $this->idnumber='';
		 $this->shortname='';
		 $this->longname='';
		 $this->activitiestotal='';
		 $this->users=new userGradeRecords();
	}
}

?>
