<?php
/**
 * Plantilla del menú del administrador
 */
?>
<div class="clearfix menubar">
	<div class="menu" style="float: left;"></div>
	<div style="float: right;">
		<?php echo $usuario->getUsuario() ?>
		|
		<?php echo $usuario->getNombreCompleto()?>
		| <a href="<?php echo url_for('sesiones/index') ?>">Home</a>
	</div>
</div>
