<?php
/**
 * Plantilla del menú 
 */
$Menu = $sf_user->getAttribute('menuData');
?>
<div class="clearfix menubar">
    <div class="menu" style="float: left;">


        <?php if ($Menu['administrador_escolar']) { ?>
            <a href="<?php echo url_for('categoria_curso/index') ?>">Categorías</a>
            | <a href="<?php echo url_for('curso/index') ?>">Cursos</a>
            | <a href="<?php echo url_for('modelador/index') ?>">Modelador</a>
            | <a href="<?php echo url_for('calendarizacion/index') ?>">Calendarizaci&oacute;n</a>
            | <a href="<?php echo url_for('cancelacion_grupo/index') ?>">Cancelaci&oacute;n de grupo</a>
            | <a href="<?php echo url_for('admin_aulas/index') ?>">Aulas</a>
            | <a href="<?php echo url_for('dias_laborables/index') ?>">Días laborables</a>
            | <a href="<?php echo url_for('verificacion_curso/index') ?>">Verificaci&oacute;n de cursos</a>
            | <a href="<?php echo url_for('admin_vacaciones/index') ?>">Vacaciones</a>
            <?php } ?>



        <?php if ($Menu['administrador_operaciones']) { ?>
            <a href="<?php echo url_for('admin_productos/index') ?>">Administrar productos</a> 
            | <a href="<?php echo url_for('admin_descuentos/index') ?>">Administrar descuentos</a>
        <?php } ?>



        <?php if ($Menu['administrador']) { ?>

        <?php } ?>



        <?php if ($Menu['caja']) { ?>
            <a href="<?php echo url_for('pos/new') ?>">Punto de venta</a> 
            | <a href="<?php echo url_for('corte_caja/resumenHoy') ?>">Movimientos del d&iacute;a</a>
            | <a href="<?php echo url_for('salida_caja/guiAuth') ?>">Salida de caja</a>
            | <a href="<?php echo url_for('corte_caja/guiAuth') ?>">Corte de caja</a>
            | <a href="<?php echo url_for('control/listas') ?>">Programación y estado de grupos</a>
        <?php } ?>



        <?php if ($Menu['facilitador']) { ?>

        <?php } ?>



        <?php if ($Menu['promotor']) { ?>

        <?php } ?>



        <?php if ($Menu['registro']) { ?>
            <a href="<?php echo url_for('registro/new') ?>">Registro de socios</a>
            | <a href="<?php echo url_for('registro/index') ?>">Búsqueda de socios</a>
        <?php } ?>



        <?php if ($Menu['soporte_tecnico']) { ?>

        <?php } ?>



        <?php if ($Menu['super_usuario']) { ?>

        <?php } ?>


    </div>
    <div style="float: right;">

        <?php echo $usuario->getUsuario() ?> 

        | <?php echo $usuario->getNombreCompleto() ?>

        | <a href="<?php echo url_for('sesiones/index') ?>">Home</a>

        <?php if ($sf_user->getAttribute('auth_externo')): ?>

            |&nbsp;<a href="<?php echo url_for('sesiones/logoutExt') ?>" onclick="return confirm('¿Desea salir del sistema?');">Salir</a>

        <?php endif; ?>
    </div>
</div>
