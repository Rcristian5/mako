<?php ?>
<div class="clearfix menubar">
	<div class="menu" style="float: left;">
		<a href="<?php echo url_for('admin_productos/index') ?>">Administrar
			productos</a> | <a
			href="<?php echo url_for('admin_descuentos/index') ?>">Administrar
			descuentos</a>

	</div>
	<div style="float: right;">
		<?php echo $usuario->getUsuario() ?>
		|
		<?php echo $usuario->getNombreCompleto()?>
		| <a href="<?php echo url_for('sesiones/index') ?>">Home</a>
		<?php if ($sf_user->getAttribute('auth_externo')):?>
		|&nbsp;<a href="<?php echo url_for('sesiones/logoutExt') ?>"
			onclick="return confirm('¿Desea salir del sistema?');">Salir</a>
		<?php endif;?>
	</div>
</div>
