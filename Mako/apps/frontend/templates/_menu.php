<?php
/**
 * Holder de menú
 *
 */
//TODO: sacar este codigo de aqui.

if ($sf_user->isAuthenticated()) {
    echo include_partial('global/' . $sf_user->getAttribute('menu'), array('usuario' => $sf_user->getAttribute('usuario')));
} elseif (!$sf_user->isAuthenticated() && $sf_user->getAttribute('auth_externo')) {
    //

} else {
    if (!$sf_user->hasFlash('notice')) {
        echo "<script>location.href='" . url_for('') . "';</script>";
        exit;
    }
}
?>

<?php
if ($sf_user->getFlash('notice')):
?>
<div class="ts ui-state-highlight ui-corner-all sombra-delgada" style="margin: 10px;">
    <span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-info"></span>
    <h1>
        <?php
            echo $sf_user->getFlash('notice');
            $sf_user->setFlash('notice', null);
        ?>
    </h1>
</div>
<?php
endif;

if ($sf_user->getAttribute('auth_externo') && !$sf_user->isAuthenticated()):
?>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <form action="<?php
        echo url_for('sesiones/authExterna') ?>"
        method="post">
        <div style="width: 100%" align="center">
            <div class="ui-widget ui-widget-content ui-corner-all sombra"
                style="width: 400px; height: 150px;">
                <div class="ui-widget ui-widget-header ui-corner-top tssss"
                    style="padding: 5px;">Proporcione sus datos para ingresar al sistema</div>
                <div align="center" style="margin-top: 20px">
                    Usuario: <input type="text" name="usuario" id="usuario" size="30"
                        maxlength="30" requerido="1" nouppercase="1" /> <br /> Clave: <input
                        type="password" name="clave" id="clave" size="30" maxlength="30"
                        requerido="1" nouppercase="1" /> <br /> <input type="submit"
                        name="Enviar" value="Enviar" id="enviar"
                        onclick="return requeridos();" />
                </div>
            </div>
        </div>
    </form>
<?php
    exit;
endif;
?>
