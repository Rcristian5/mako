<?php
/**
 * Plantilla del menú del cajero
 */
?>
<div class="clearfix menubar">
	<div class="menu" style="float: left;">
		<a href="<?php echo url_for('pos/new') ?>">Punto de venta</a> | <a
			href="<?php echo url_for('salida_caja/guiAuth') ?>">Salida de caja</a>
		| <a href="<?php echo url_for('corte_caja/guiAuth') ?>">Corte de caja</a>
		| <a href="<?php echo url_for('cancelacion_venta/guiAuth') ?>">Cancelación</a>
	</div>
	<div style="float: right;">
		<?php echo $usuario->getUsuario() ?>
		|
		<?php echo $usuario->getNombreCompleto()?>
		| <a href="<?php echo url_for('sesiones/index') ?>">Home</a>
	</div>
</div>
