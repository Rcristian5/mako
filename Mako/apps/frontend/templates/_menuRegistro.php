<?php
/**
 * Plantilla del menú del registro
 */
?>
<div class="clearfix menubar">
	<div class="menu" style="float: left;">
		<a href="<?php echo url_for('registro/new') ?>">Registro de socios</a>
		| <a href="<?php echo url_for('registro/index') ?>">Búsqueda de socios</a>
	</div>
	<div style="float: right;">
		<?php echo $usuario->getUsuario() ?>
		|
		<?php echo $usuario->getNombreCompleto()?>
		| <a href="<?php echo url_for('sesiones/index') ?>">Home</a>
	</div>
</div>
