<?php ?>

<div class="clearfix menubar">
	<div class="menu" style="float: left;">
		<a href="<?php echo url_for('categoria_curso/index') ?>">Categorías</a>
		| <a href="<?php echo url_for('curso/index') ?>">Cursos</a> | <a
			href="<?php echo url_for('modelador/index') ?>">Modelador</a> | <a
			href="<?php echo url_for('calendarizacion/index') ?>">Calendarizaci&oacute;n</a>
		| <a href="<?php echo url_for('cancelacion_grupo/index') ?>">Cancelaci&oacute;n
			de grupo</a> | <a href="<?php echo url_for('admin_aulas/index') ?>">Aulas</a>
		| <a href="<?php echo url_for('dias_laborables/index') ?>">Días
			laborables</a> | <a
			href="<?php echo url_for('verificacion_curso/index') ?>">Verificaci&oacute;n
			de cursos</a> | <a
			href="<?php echo url_for('admin_vacaciones/index') ?>">Vacaciones</a>

	</div>
	<div style="float: right;">
		<?php echo $usuario->getUsuario() ?>
		|
		<?php echo $usuario->getNombreCompleto()?>
		| <a href="<?php echo url_for('sesiones/index') ?>">Home</a>
		<?php if ($sf_user->getAttribute('auth_externo')):?>
		|&nbsp;<a href="<?php echo url_for('sesiones/logoutExt') ?>"
			onclick="return confirm('¿Desea salir del sistema?');">Salir</a>
		<?php endif;?>

	</div>
</div>
