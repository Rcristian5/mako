<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <?php include_http_metas() ?>
        <?php include_metas() ?>
        <?php include_title() ?>
        <link rel="shortcut icon" href="/favicon.ico" />
        <?php include_stylesheets() ?>
        <?php include_javascripts() ?>
    </head>
    <body>
        <table width="1080" height="98%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td height="20" class="menu" align="right">
                    <a href="http://www.ria.org.mx/" target="_blank">RIA</a> |
                    <a href="http://www.proacceso.org.mx/" target="_blank">ECO</a> |
                    <a href="http://www.ria.org.mx/msjs/terminos.html" target="_blank">Privacidad</a>
                </td>
            </tr>
            <tr>
                <td height="100" align="center" valign="middle">
                    <a href="<?php echo url_for('dashsocios/index') ?>">
                        <img src="/imgs/dashboard/logo.jpg" alt="" width="225" height="99" border="0" />
                    </a>
                </td>
            </tr>
            <tr>
                <td valign="top"><?php echo $sf_content ?>
                </td>
            </tr>
            <tr>
                <td height="20" class="menu" align="center">
                    2009 Derechos Reservados     Fundacion ProAcceso ECO A.C. - Gobierno del Estado de M&eacute;xico - Red de Innovaci&oacute;n y Aprendizaje
                </td>
            </tr>
        </table>
    </body>
</html>
