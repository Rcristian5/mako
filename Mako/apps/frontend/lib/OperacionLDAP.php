<?php
/**
 * Clase que encapsula las operaciones con LDAP
 */

class OperacionLDAP
{

	static public function registrarSocio(sfEvent $ev)
	{
		$socio = $ev['socio'];
		sfProjectConfiguration::getActive()->loadHelpers('Ldap');
		$uid = agregarSocioLdap(
				$socio->getUsuario(),
				$socio->getNombre(),
				trim($socio->getApepat() ." ". $socio->getApemat()),
				$socio->getClave(),
				$socio->getEmail(),
				$socio->getEstado(),
				$socio->getDirgmaps(),
				$socio->getTel(),
				$socio->getCelular(),
				$socio->getCentro()->getAlias(),
				$socio->getCentroId(),
				$socio->getId()
		);
		if($uid == false)
		{
			error_log("Error[OperacionLdap.registrarSocio]: Ocurrión un error al registrar al socio ".$socio);
		}
		else
		{
			$socio->setLdapUid($uid);
			$socio->save();
			error_log("Se acaba de registrar: ".$socio);
		}
	}

	static public function actualizarSocio(sfEvent $ev)
	{
		$socio = $ev['socio'];
		$modificaUsername = $ev['modificaUsername'];
		$usuarioAnterior = $ev['usuario_anterior'];

		sfProjectConfiguration::getActive()->loadHelpers('Ldap');
		modificarSocioLDAP(
				$socio->getUsuario(),
				$socio->getNombre(),
				trim($socio->getApepat() ." ". $socio->getApemat()),
				$socio->getClave(),
				$socio->getEmail(),
				$socio->getEstado(),
				$socio->getDirgmaps(),
				$socio->getTel(),
				$socio->getCelular(),
				$modificaUsername,
				$usuarioAnterior,
				$socio->getCentroId(),
				$socio->getId()
		);

		error_log("Se acaba de actualizar: ".$socio);
	}

	static public function registrarUsuario(sfEvent $ev)
	{
		$centro_id= sfConfig::get('app_centro_actual_id');
		$centro = CentroPeer::retrieveByPK($centro_id);

		$usuario = $ev->getSubject();
		sfProjectConfiguration::getActive()->loadHelpers('Ldap');
		agregarUsuarioLDAP(
				$usuario->getUsuario(),
				$usuario->getNombre(),
				trim($usuario->getApepat()." ".$usuario->getApemat()),
				$usuario->getClave(),
				$usuario->getEmail(),
				$centro->getAlias()
		);
		error_log("Se acaba de registrar: ".$usuario);
	}

	static public function actualizarUsuario(sfEvent $ev)
	{
		$usuario = $ev->getSubject();

		$modificaUsername = $ev['modificaUsername'];
		$usuarioAnterior = $ev['usuario_anterior'];

		sfProjectConfiguration::getActive()->loadHelpers('Ldap');
		modificarUsuarioLDAP(
				$usuario->getUsuario(),
				$usuario->getNombre(),
				trim($usuario->getApepat()." ".$usuario->getApemat()),
				$usuario->getClave(),
				$usuario->getEmail(),
				$modificaUsername,
				$usuarioAnterior
		);

		error_log("Se acaba de actualizar: ".$usuario);
	}

	/**
	 * Registra al socio en el curso dado
	 * @param sfEvent $ev
	 * @return void
	 */
	static public function inscribirAlumno(sfEvent $ev)
	{
		$curso = $ev['curso'];
		$socio = $ev['socio'];
		$grupo = $ev['grupo'];
		sfProjectConfiguration::getActive()->loadHelpers('Ldap');

		$clave = $curso->getClave();
		$usuario = $socio->getUsuario();
		$email = $socio->getEmail();
		asociaSocioCursoLDAP($usuario,$email,$clave);
	}

	/**
	 * Registra el nuevo curso en el servidor LDAP
	 * @param sfEvent $ev
	 * @return void
	 */
	static public function registrarCurso(sfEvent $ev)
	{
		$curso = $ev['curso'];
		sfProjectConfiguration::getActive()->loadHelpers('Ldap');

		agregarCurso($curso->getClave(),$curso->getNombre());
		error_log("Se acaba de registrar: ".$curso);
	}

	/**
	 * Modifica el nombre o la clave del curso en el serv ldap
	 * @param sfEvent $ev
	 * @return void
	 */
	static public function modificarCurso(sfEvent $ev)
	{
		$curso = $ev['curso'];
		//$anterior = $ev['clave_anterior'];
		error_log("Se escucho el evento de mod curso para LDAP: ".$curso->getClave());
		sfProjectConfiguration::getActive()->loadHelpers('Ldap');

		if(!isset($anterior))
		{
			$anterior = $curso->getClave();
		}
		modificarCurso($anterior,$curso->getClave(),$curso->getNombre());
		error_log("Se acaba de actualizar: ".$curso);
	}


	/**
	 * Cacha el evento usuario.login
	 * @param $usuario
	 * @param $clave
	 */
	static public function modificarClaveSocioBind(sfEvent $ev)
	{
		$socio = $ev['socio'];
		$usuario = $socio->getUsuario();
		$clave = $socio->getClave();
		self::modificarClaveSocio($usuario, $clave);
	}

	/**
	 * Modifica la clave del socio
	 * @param $usuario
	 * @param $clave
	 */
	static public function modificarClaveSocio($usuario,$clave)
	{
		sfProjectConfiguration::getActive()->loadHelpers('Ldap');
		modificarClaveSocioLDAP($usuario,$clave);
	}

	/**
	 * Cacha el evento usuario.login
	 * @param $usuario
	 * @param $clave
	 */
	static public function usuarioLoginBind(sfEvent $ev)
	{
		$sesion = $ev['sesion'];
	}

	/**
	 * Cacha el evento usuario.logout
	 * @param sfEvent $ev
	 */
	static public function usuarioLogoutBind(sfEvent $ev)
	{
		$sesion = $ev['sesion_historico'];
	}

	/**
	 * Cacha el evento socio.login
	 * @param sfEvent $ev
	 */
	static public function socioLoginBind(sfEvent $ev)
	{
		$sesion = $ev['sesion'];
		self::socioLogin($sesion);
	}

	/**
	 * Cacha el evento socio.logout
	 * @param sfEvent $ev
	 */
	static public function socioLogoutBind(sfEvent $ev)
	{
		$sesion = $ev['sesion_historico'];
		self::socioLogout($sesion);
	}

	/**
	 * Ejecuta en el ldap el cambio de host para obligar a que solo esa pc pueda estar en sesion
	 * @param Sesion $sesion
	 */
	static public function usuarioLogin(SesionOperador $sesion)
	{
		$sesion = $ev['sesion'];
		self::usuarioLogin($sesion);
	}

	/**
	 * Ejecuta en el ldap el cambio de host para obligar a que pueda entrar el usuario en cualquier pc
	 * @param Sesion $sesion
	 */
	static public function usuarioLogout(SesionOperadorHistorico $sesion)
	{
		$sesion = $ev['sesion_historico'];
		self::usuarioLogout($sesion);

	}

	/**
	 * Ejecuta en el ldap el cambio de host para obligar a que el socio solo entre en esa pc
	 * si es que no esta marcado como multisesion.
	 * @param Sesion $sesion
	 */
	static public function socioLogin(Sesion $sesion)
	{
		sfProjectConfiguration::getActive()->loadHelpers('Ldap');

		$tipo_id = $sesion->getTipoId();

		$host = $sesion->getComputadora()->getHostname();
		$usuario = $sesion->getUsuario();
		$ip = $sesion->getIp();

		//Vemos que tipo de sesión se trata y actuamos en consecuencia.
		switch ($tipo_id)
		{
			case sfConfig::get('app_sesion_default_id'):
				error_log("Se ha detectado login desde $ip -> $hostname con sesion default");
				modificarHostPermitido($usuario, $host);
				break;

			case sfConfig::get('app_sesion_sin_consumo_id'):
				modificarHostPermitido($usuario, $host);
				error_log("Se ha detectado login desde $ip -> $hostname con sesion sin consumo de tiempo");

				break;

			case sfConfig::get('app_sesion_multisesion_id'):
				error_log("Se ha detectado login desde $ip -> $hostname con multisesion");
				modificarHostPermitido($usuario, '*');
				break;

			case sfConfig::get('app_sesion_multisesion_sin_consumo_id'):
				error_log("Se ha detectado login desde $ip -> $hostname con multisesion sin consumo de tiempo");
				modificarHostPermitido($usuario, '*');
				break;

		}
	}

	/**
	 * Restaura el valor de host * en las sesiones que tienen tipo default y sesion sin consumo
	 * @param SesionHistorico $sesion
	 */
	static public function socioLogout(SesionHistorico $sesion)
	{
		sfProjectConfiguration::getActive()->loadHelpers('Ldap');
		error_log("Entra a socioLogout");
		$tipo_id = $sesion->getTipoId();
		$usuario = $sesion->getUsuario();
		error_log("Sesion tipo: $tipo_id");
		//Vemos que tipo de sesión se trata y actuamos en consecuencia.
		switch ($tipo_id)
		{
			case sfConfig::get('app_sesion_default_id'):
				modificarHostPermitido($usuario, '*');
				break;

			case sfConfig::get('app_sesion_sin_consumo_id'):
				modificarHostPermitido($usuario, '*');
				break;

		}

	}

	/**
	 * Verifica la existencia de un usuario en el ldap
	 *
	 * @param string $usuario username a verificar en el ldap
	 */
	static public function existeUsuario($usuario)
	{
		sfProjectConfiguration::getActive()->loadHelpers('Ldap');
		error_log("Verificando si existe el usuario $usuario en LDAP: ");
		return verificaUsuarioLDAP($usuario);
	}

}