<?php 
/**
 * Se encarga de mandar a imprimir tickets y facturas tanto de ventas como de cortes
 * a la impresora del punto de venta.
 * 
 */

define("DEBUG_LIB_IMPRESORASTICKETS", True);
define("DEBUG_LIB_IMPRESORASTICKETS_LOG", "/var/log/mako/facturacion.log");

class ImpresoraTickets {
	/**
	 * Genera e imprime el ticket de venta
	 * @param sfEvent $ev
	 * @return void
	 */
	static public function ticketVenta(sfEvent $ev)
	{
		error_log('ticketVenta');
        $tasa0 = isset($ev['tasa0']) ? true : false;
        $context = sfContext::getInstance();
		ImpresoraTickets::generaTicketVenta($context, $ev['orden'], $tasa0);
	}
	/**
	 * Genera e imprime el ticket de venta
	 * @param sfEvent $ev
	 * @return void
	 */
	static public function generaTicketVenta($context, $orden, $tasa0 = true)
	{
		error_log('generaTicketVenta');
		//Si se trata de una factura no imprimirmos el ticket.
		if($orden->getEsFactura())
		{
			return;
		}

		$ctx = $context;
		$ctx->getConfiguration()->loadHelpers('Partial');
                $ctx->getConfiguration()->loadHelpers('Facturas');

		$tasa_impuesto = intval($orden->getCentro()->getIva());

		//Si la tasa del iva del producto vendido es cero:
		if($tasa0) $tasa_impuesto = 0;

		list($iva,$importe,$total,$totalPalabras) = desglosa_iva( $orden->getTotal(), $tasa_impuesto);

		list($cadp,$caf_prods) = cadenaProductosTicket($orden, $tasa0);

		$parsTicket = array(
			'orden'=>$orden,
			'iva' => $iva,
			'importe'=>$importe,
			'total'=>$total,
			'totalPalabras'=>$totalPalabras,
			'cadp'=>$cadp,
			'caf_prods'=>$caf_prods
		);

		/*
        * Reingenieria  Mako. SIN - ID RQ
        * Responsables:  (BP) Bet Gader Porcayo Juárez, (LGL) Luis Gonzales Linares
        * Fecha:        21-01-2015
        * Descripción:  Se añade opcion para configuración de Nuevas Impresoras
        */
        $Prefijo = @sfConfig::get('app_prefijo');
		switch ($Prefijo) {
			case 'oki':
				$ticket = get_partial('pos/ticketVenta',$parsTicket);
				break;
			case 'Ch':
			case 'TMT20II':
				/*
				* En este caso se incluye el tipo Ch TMT20II
				*/
				$ticket = get_partial('pos/ticket'.$Prefijo.'Venta',$parsTicket);
				break;
			default:
				/*
				* En caso de no estar configurado el parámetro app_prefijo, o que no es alguno 
				* de los anteriores se configura con respecto a OKI, 
				*/
				$ticket = get_partial('pos/ticketVenta',$parsTicket);
				error_log('ERROR GENERICO!!! No se ha configurado el parámetro PREFIJO o el valor NO EXISTE en la configuración. Fn=ticketVenta');
				break;
		}



		$ip = Comun::ipReal();
		$id = $orden->getId();
		$nomtick=PATH_TICKETS."/ticket_venta".$id. ".txt";
		$fp = fopen($nomtick, "w+");
		fwrite($fp,$ticket);
		fclose($fp);
        sleep(2);
		switch ($Prefijo) {
			case 'TMT20II':
                /*
                * Para la apertura de cajo con las impresoras TMT20II
                */
                exec("echo -en '\033p011' | lp -h " . $ip ." -d okipos -o raw");
				break;
            default:
               /*
                * Para la apertura de cajo con cualquier otro tipo de  impresoras 
                */
                exec("echo p011 | lp -h " . $ip ." -d okipos -o raw");
                break;
		}
        sleep(2);
		error_log("lp -h ".$ip." -d okipos $nomtick");
		$res = exec("lp -h ".$ip." -d okipos $nomtick");
	}

	/**
	 * Genera e imprime la factura de venta.
	 * @param sfEvent $ev
	 * @return void
	 */
	static public function facturaVenta(sfEvent $ev)
	{
		if(DEBUG_LIB_IMPRESORASTICKETS)
			error_log("\n==============      " . date("Ymd H:i:s") . "  [LIB::ImpresoraTickets] Ingresando a facturaVta()     ====================",3,DEBUG_LIB_IMPRESORASTICKETS_LOG);
		$orden = $ev['orden'];
		$tasa0 = isset($ev['tasa0']) ? true : false;

		//Obtenemos la tasa del iva para esta venta
		if($tasa0) $tasa = 0;
		else $tasa = intval($orden->getCentro()->getIva()); 

		//Si NO se trata de una factura no imprimimos.
		if(!$orden->getEsFactura())
		{
			if(DEBUG_LIB_IMPRESORASTICKETS)
				error_log("\n" . date("Ymd H:i:s") . "  [LIB::ImpresoraTickets] La orden " . $orden->getId() . " => No es una factura.",3,DEBUG_LIB_IMPRESORASTICKETS_LOG);
			return;
		}

		error_log("Es factura y de tasa0 ".$tasa0);
		if(DEBUG_LIB_IMPRESORASTICKETS)
			error_log("\n" . date("Ymd H:i:s") . "  [LIB::ImpresoraTickets]Generando factura tasa => " . $tasa ,3,DEBUG_LIB_IMPRESORASTICKETS_LOG);
		$ip = Comun::ipReal();
		$id = Comun::generaId();

		$ctx = sfContext::getInstance();
		$ctx->getConfiguration()->loadHelpers('Partial');
		$ctx->getConfiguration()->loadHelpers('Facturas');

		list($confFactura,$conceptos,$cadenaconceptos,$totalPalabras,$ffis,$caf_prods) = FacturaPeer::facturaVenta($orden, $tasa0);

		$df = generaFactura($confFactura,$conceptos);
		$cadena = $df['cadena_original'];
		$sello = $df['sello_digital'];

		$total = $confFactura['total'];
		$subTotal = $confFactura['subTotal'];
		$importe = $subTotal;
		$iva = $confFactura['Impuestos']['Traslados']['Traslado']['importe'];

		$f = new Factura();
		$f->setFolioFiscal($ffis);
		$f->setCentroId(sfConfig::get('app_centro_actual_id'));
		$f->setFechaFactura($orden->getCreatedAt());
		$f->setFolio($ffis->getFolioActual());
		$f->setCadenaOriginal($cadena);
		$f->setSello($sello);
		$f->setTotal($total);
		$f->setImporte($importe);
		$f->setSubtotal($subTotal);
		$f->setRetencion($iva);
		$f->save();
		if(DEBUG_LIB_IMPRESORASTICKETS)
			error_log("\n" . date("Ymd H:i:s") . "  [LIB::ImpresoraTickets]Se guardo TUPLA en TBL_FACTURA => " . $f->getId() ,3,DEBUG_LIB_IMPRESORASTICKETS_LOG);

		$orden->setFactura($f);
		$orden->save();
		if(DEBUG_LIB_IMPRESORASTICKETS)
			error_log("\n" . date("Ymd H:i:s") . "  [LIB::ImpresoraTickets]Se guardo TUPLA en TBL_ORDEN => " . $orden->getId() ,3,DEBUG_LIB_IMPRESORASTICKETS_LOG);

		$fo = new TrFacturaOrden();
		$fo->setFacturaId($f->getId());
		$fo->setOrdenId($orden->getId());
		$fo->setCentroId(sfConfig::get('app_centro_actual_id'));
		$fo->save();
		if(DEBUG_LIB_IMPRESORASTICKETS)
			error_log("\n" . date("Ymd H:i:s") . "  [LIB::ImpresoraTickets]Registro en TBL_FACTURAORDEN (factura_id, orden_id) => (" . $fo->getFacturaId() . " , " . $orden->getId() . ")" ,3,DEBUG_LIB_IMPRESORASTICKETS_LOG);

		$varsPartial = array(
			'orden'=>$orden,
			'importe'=>$importe,
			'subTotal'=>$subTotal,
			'total'=>$total,
			'iva'=>$iva,
			'cadena'=>$cadena,
			'sello'=>$sello,
			'folio' => $ffis->getFolioActual(),
			'cadenaconceptos'=>$cadenaconceptos,
			'ffis'=>$ffis,
			'totalPalabras'=>$totalPalabras,
			'caf_prods' => $caf_prods,
			'tasa' => $tasa
		);


		/*
        * Reingenieria  Mako. SIN - ID RQ
        * Responsables:  (BP) Bet Gader Porcayo Juárez, (LGL) Luis Gonzalez Linares
        * Fecha:        21-01-2015
        * Descripción:  Se añade opcion para configuración de Nuevas Impresoras
        */
        $Prefijo = @sfConfig::get('app_prefijo');
		switch ($Prefijo) {
			case 'oki':
				$factura_corte = get_partial('pos/facturaVenta',$varsPartial);
				break;
			case 'Ch':
			case 'TMT20II':
				/*
				* En este caso se incluye el tipo Ch TMT20II
				*/
				$factura_corte = get_partial('pos/factura'.$Prefijo.'Venta',$varsPartial);
				break;
			default:
				/*
				* En caso de no estar configurado el parámetro app_prefijo, o que no es alguno 
				* de los anteriores se configura con respecto a OKI, 
				*/
				$factura_corte = get_partial('pos/facturaVenta',$varsPartial);
				error_log('ERROR GENERICO!!! No se ha configurado el parámetro PREFIJO o el valor NO EXISTE en la configuración. Fn=facturaVenta');
				break;
		}

		//Aumentamos el folio actual fiscal
		$ffis->setFolioActual($ffis->getFolioActual()+1);
		$ffis->save();
		if(DEBUG_LIB_IMPRESORASTICKETS)
			error_log("\n" . date("Ymd H:i:s") . "  [LIB::ImpresoraTickets] Actualizando Folio Fiscal (anterior => nuevo)     (" . $f->getFolio() . " => " . $ffis->getFolioActual() .")" ,3,DEBUG_LIB_IMPRESORASTICKETS_LOG);

		//Imprimirmos tickets resumen
		$nomtick= PATH_FACTURAS."/".$confFactura['Emisor']['rfc'].$confFactura['serie'].$confFactura['folio'].'.txt';
		error_log('Guardando ticket en: '.$nomtick);
		if(DEBUG_LIB_IMPRESORASTICKETS)
			error_log("\n" . date("Ymd H:i:s") . "  [LIB::ImpresoraTickets] Guardando archivo físico => " . $nomtick ,3,DEBUG_LIB_IMPRESORASTICKETS_LOG);
		$fp = fopen($nomtick, "w+");
		fwrite($fp,$factura_corte);
		fclose($fp);
                switch ($Prefijo) {
			case 'TMT20II':
				/*
				* Para la apertura de cajo con las impresoras TMT20II
				*/
                exec("echo -en '\033p011' | lp -h " . $ip ." -d okipos -o raw");
				break;
            default:
               /*
                * Para la apertura de cajo con cualquier otro tipo de  impresoras 
                */
                exec("echo p011 | lp -h " . $ip ." -d okipos -o raw");
                break;
		}
        sleep(2);
        error_log("lp -h ".$ip." -d okipos $nomtick");
		$res = exec("lp -h ".$ip." -d okipos $nomtick");
	}

	/**
	 * Genera e imprime la factura de corte.
	 * @param sfEvent $ev
	 * @return void
	 */
	static public function facturaCorte(sfEvent $ev)
	{
		if(DEBUG_LIB_IMPRESORASTICKETS)
			error_log("\n==================     " . date("Ymd H:i:s") . "  [LIB::ImpresoraTickets] Ingresando a FacturaCorte()     ==================",3,DEBUG_LIB_IMPRESORASTICKETS_LOG);

		$corte = $ev['corte'];
		$ordenes = $ev['ordenes'];

		//Vemos si hay alguna venta publico general tasa iva
		$ft = CortePeer::foliosTasa($corte);
		//Si no lo hay entonces terminamos sin mas el proceso
		if(!count($ft['ordenes_tasa'])) {
			if(DEBUG_LIB_IMPRESORASTICKETS)
				error_log("\n" . date("Ymd H:i:s") . "  [LIB::ImpresoraTickets] No existen ordenes tasa16 para generar Factura Tasa16 ",3,DEBUG_LIB_IMPRESORASTICKETS_LOG);
			return;
		}

        // Reingenieria Mako C. ID 13. Responsable: FE. Fecha: 10-03-2014. Descripción del cambio: No se genera factura si es negativo el iva
		if($corte->getTotalVpg()<=0) {
			if(DEBUG_LIB_IMPRESORASTICKETS)
				error_log("\n" . date("Ymd H:i:s") . "  [LIB::ImpresoraTickets] IVA NEGATIVO:  " . $corte->getTotalVpg() . " => Por lo tanto no se genera Factura. ",3,DEBUG_LIB_IMPRESORASTICKETS_LOG);
			return;
		}
		// // Fecha: 10-03-2014. Fin

		$ip = Comun::ipReal();
		$id = $corte->getId();

		$ctx = sfContext::getInstance();
		$ctx->getConfiguration()->loadHelpers('Partial');
		$ctx->getConfiguration()->loadHelpers('Facturas');

		list($confFactura,$conceptos,$ffis) = FacturaPeer::facturaCorte($corte);

		$df = generaFactura($confFactura,$conceptos);
		$cadena = $df['cadena_original'];
		$sello = $df['sello_digital'];

		$total = $confFactura['total'];
		$subTotal = $confFactura['subTotal'];
		$importe = $subTotal; 
		$iva = $confFactura['Impuestos']['Traslados']['Traslado']['importe'];

		$f = new Factura();
		$f->setCentroId(sfConfig::get('app_centro_actual_id'));
		$f->setFolioFiscal($ffis);
		$f->setFechaFactura($corte->getCreatedAt());
		$f->setFolio($ffis->getFolioActual());
		$f->setCadenaOriginal($cadena);
		$f->setSello($sello);
		$f->setTotal($total);
		$f->setImporte($importe);
		$f->setSubtotal($subTotal);
		$f->setRetencion($iva);
		$f->save();
		if(DEBUG_LIB_IMPRESORASTICKETS)
			error_log("\n" . date("Ymd H:i:s") . "  [LIB::ImpresoraTickets]Se guardo TUPLA en TBL_FACTURA => " . $f->getId() ,3,DEBUG_LIB_IMPRESORASTICKETS_LOG);

		$corte->setFactura($f);
		$corte->save();

		if(DEBUG_LIB_IMPRESORASTICKETS)
			error_log("\n" . date("Ymd H:i:s") . "  [LIB::ImpresoraTickets]Se guardo TUPLA en TBL_CORTE => " . $corte->getId() ,3,DEBUG_LIB_IMPRESORASTICKETS_LOG);

		// Generamos registros en FACTURA_ORDEN
		foreach ($ft['ordenes_tasa'] as $orden_id)
		{
			$fo = new TrFacturaOrden();
			$fo->setFacturaId($f->getId());
			$fo->setOrdenId($orden_id);
			$fo->setCentroId(sfConfig::get('app_centro_actual_id'));
			$fo->save();
			if(DEBUG_LIB_IMPRESORASTICKETS)
				error_log("\n" . date("Ymd H:i:s") . "  [LIB::ImpresoraTickets]Registro en TBL_FACTURAORDEN (factura_id, orden_id) => (" . $fo->getFacturaId() . " , " . $orden_id . ")" ,3,DEBUG_LIB_IMPRESORASTICKETS_LOG);
		}

		// Generamos tupla en CORTE_FACTURA para su manipulación
		$corte_fact = new TrCorteFactura();
		$corte_fact->setCorteId($corte->getId());
		$corte_fact->setFacturaId($f->getId());
		$corte_fact->setTipoFacturaId(sfConfig::get('app_tipo_factura_tasa16'));
		$corte_fact->setOperacionFacturaId(sfConfig::get('app_operacion_factura_corte'));
		$corte_fact->setCentroId(sfConfig::get('app_centro_actual_id'));
		$corte_fact->save();

		if(DEBUG_LIB_IMPRESORASTICKETS)
			error_log("\n" . date("Ymd H:i:s") . "  [LIB::ImpresoraTickets]Registro en TBL_CORTEFACTURA (corte_id, factura_id) => (" . $corte_fact->getCorteId() . " , " . $corte_fact->getFacturaId() . ")" ,3,DEBUG_LIB_IMPRESORASTICKETS_LOG);

		$descripcion = isset($conceptos[0]) ? $conceptos[0]['descripcion'] : ''; 

		$varsPartial = array(
			'corte'=>$corte,
			'importe'=>$importe,
			'subTotal'=>$subTotal,
			'total'=>$total,
			'iva'=>$iva,
			'cadena'=>$cadena,
			'sello'=>$sello,
			'ffis'=>$ffis,
			'conceptos' => $descripcion,
			'tasa' => intval($corte->getCentro()->getIva())
		);

		/*
        * Reingenieria  Mako. SIN - ID RQ
        * Responsables:  (BP) Bet Gader Porcayo Juárez, (LGL) Luis Gonzalez Linares
        * Fecha:        21-01-2015
        * Descripción:  Se añade opcion para configuración de Nuevas Impresoras
        */
        $Prefijo = @sfConfig::get('app_prefijo');
		switch ($Prefijo) {
			case 'oki':
				$factura_corte = get_partial('corte_caja/facturaCorte',$varsPartial);
				break;
			case 'Ch':
			case 'TMT20II':
				/*
				* En este caso se incluye el tipo Ch TMT20II
				*/
				$factura_corte = get_partial('corte_caja/factura'.$Prefijo.'Corte',$varsPartial);
				break;
			default:
				/*
				* En caso de no estar configurado el parámetro app_prefijo, o que no es alguno 
				* de los anteriores se configura con respecto a OKI, 
				*/
				$factura_corte = get_partial('corte_caja/facturaCorte',$varsPartial);
				error_log('ERROR GENERICO!!! No se ha configurado el parámetro PREFIJO o el valor NO EXISTE en la configuración. Fn=facturaCorte');
				break;
		}

		//Aumentamos el folio actual fiscal
		$ffis->setFolioActual($ffis->getFolioActual()+1);
		$ffis->save();
		if(DEBUG_LIB_IMPRESORASTICKETS)
			error_log("\n" . date("Ymd H:i:s") . "  [LIB::ImpresoraTickets] Actualizando Folio Fiscal (anterior => nuevo)     (" . $f->getFolio() . " => " . $ffis->getFolioActual() .")" ,3,DEBUG_LIB_IMPRESORASTICKETS_LOG);

		//Imprimirmos facturas
		$nomtick= PATH_FACTURAS."/".$confFactura['Emisor']['rfc'].$confFactura['serie'].$confFactura['folio'].'.txt';
		$fp = fopen($nomtick, "w+");
		fwrite($fp,$factura_corte);
		fclose($fp);
		if(DEBUG_LIB_IMPRESORASTICKETS)
			error_log("\n" . date("Ymd H:i:s") . "  [LIB::ImpresoraTickets] Guardando archivo físico => " . $nomtick ,3,DEBUG_LIB_IMPRESORASTICKETS_LOG);

		//La factura imprimible.
		$imprimible= '/tmp/factura_corte_'.$id.'.txt';
		$fp = fopen($imprimible, "w+");
		fwrite($fp,$factura_corte);
		fclose($fp);
	}

	/**
	* Genera e imprime la factura de corte de tasa 0
	* @param sfEvent $ev
	* @return void
	*/
	static public function facturaCorteTasa0(sfEvent $ev)
	{
		if(DEBUG_LIB_IMPRESORASTICKETS)
			error_log("\n==================     " . date("Ymd H:i:s") . "  [LIB::ImpresoraTickets] Ingresando a FacturaCorteTasa0()     ==================",3,DEBUG_LIB_IMPRESORASTICKETS_LOG);
		$corte = $ev['corte'];
		$ordenes = $ev['ordenes'];

		//Vemos si hay alguna venta publico general tasa0
		$ft0 = CortePeer::foliosTasa0($corte);
		//Si no lo hay entonces terminamos sin mas el proceso
		if(!count($ft0['ordenes_tasa0'])) {
			if(DEBUG_LIB_IMPRESORASTICKETS)
				error_log("\n" . date("Ymd H:i:s") . "  [LIB::ImpresoraTickets] No existen ordenes tasa0 para generar Factura Tasa0 ",3,DEBUG_LIB_IMPRESORASTICKETS_LOG);
			return;
		}

        // Reingenieria Mako C. ID 13. Responsable: FE. Fecha: 10-03-2014. Descripción del cambio: No se genera factura si es negativo el iva
		if($corte->getTotalVpg()<=0) {
			if(DEBUG_LIB_IMPRESORASTICKETS)
				error_log("\n" . date("Ymd H:i:s") . "  [LIB::ImpresoraTickets] IVA NEGATIVO:  " . $corte->getTotalVpg() . " => Por lo tanto no se genera Factura. ",3,DEBUG_LIB_IMPRESORASTICKETS_LOG);
			return;
		}
		// // Fecha: 10-03-2014. Fin

		$ip = Comun::ipReal();
		$id = $corte->getId();

		$ctx = sfContext::getInstance();
		$ctx->getConfiguration()->loadHelpers('Partial');
		$ctx->getConfiguration()->loadHelpers('Facturas');

		list($confFactura,$conceptos,$ffis) = FacturaPeer::facturaCorteTasa0($corte);

		$df = generaFactura($confFactura,$conceptos);
		$cadena = $df['cadena_original'];
		$sello = $df['sello_digital'];

		$total = $confFactura['total'];
		$subTotal = $confFactura['subTotal'];
		$importe = $subTotal;
		$iva = $confFactura['Impuestos']['Traslados']['Traslado']['importe'];

		$f = new Factura();
		$f->setCentroId(sfConfig::get('app_centro_actual_id'));
		$f->setFolioFiscal($ffis);
		$f->setFechaFactura($corte->getCreatedAt());
		$f->setFolio($ffis->getFolioActual());
		$f->setCadenaOriginal($cadena);
		$f->setSello($sello);
		$f->setTotal($total);
		$f->setImporte($importe);
		$f->setSubtotal($subTotal);
		$f->setRetencion($iva);
		$f->save();

		if(DEBUG_LIB_IMPRESORASTICKETS)
			error_log("\n" . date("Ymd H:i:s") . "  [LIB::ImpresoraTickets]Se guardo TUPLA en TBL_FACTURA => " . $f->getId() ,3,DEBUG_LIB_IMPRESORASTICKETS_LOG);

		foreach ($ft0['ordenes_tasa0'] as $orden_id)
		{
			$fo = new TrFacturaOrden();
			$fo->setFacturaId($f->getId());
			$fo->setOrdenId($orden_id);
			$fo->setCentroId(sfConfig::get('app_centro_actual_id'));
			$fo->save();
			if(DEBUG_LIB_IMPRESORASTICKETS)
				error_log("\n" . date("Ymd H:i:s") . "  [LIB::ImpresoraTickets]Registro en TBL_FACTURAORDEN (factura_id, orden_id) => (" . $fo->getFacturaId() . " , " . $orden_id . ")" ,3,DEBUG_LIB_IMPRESORASTICKETS_LOG);
		}

		// Generamos tupla en CORTE_FACTURA para su manipulación
		$corte_fact = new TrCorteFactura();
		$corte_fact->setCorteId($corte->getId());
		$corte_fact->setFacturaId($f->getId());
		$corte_fact->setTipoFacturaId(sfConfig::get('app_tipo_factura_tasa0'));
		$corte_fact->setOperacionFacturaId(sfConfig::get('app_operacion_factura_corte'));
		$corte_fact->setCentroId(sfConfig::get('app_centro_actual_id'));
		$corte_fact->save();

		if(DEBUG_LIB_IMPRESORASTICKETS)
			error_log("\n" . date("Ymd H:i:s") . "  [LIB::ImpresoraTickets]Registro en TBL_CORTEFACTURA (corte_id, factura_id) => (" . $corte_fact->getCorteId() . " , " . $corte_fact->getFacturaId() . ")" ,3,DEBUG_LIB_IMPRESORASTICKETS_LOG);

		$descripcion = isset($conceptos[0]) ? $conceptos[0]['descripcion'] : '';

		$varsPartial = array(
				'corte'=>$corte,
				'importe'=>$importe,
				'subTotal'=>$subTotal,
				'total'=>$total,
				'iva'=>$iva,
				'cadena'=>$cadena,
				'sello'=>$sello,
				'ffis'=>$ffis,
				'conceptos' => $descripcion,
				'tasa'=>0
		);

		/*
        * Reingenieria  Mako. SIN - ID RQ
        * Responsables:  (BP) Bet Gader Porcayo Juárez, (LGL) Luis Gonzalez Linares
        * Fecha:        21-01-2015
        * Descripción:  Se añade opcion para configuración de Nuevas Impresoras
        */
        $Prefijo = @sfConfig::get('app_prefijo');
		switch ($Prefijo) {
			case 'oki':
				$factura_corte = get_partial('corte_caja/facturaCorte',$varsPartial);
				break;
			case 'Ch':
			case 'TMT20II':
				/*
				* En este caso se incluye el tipo Ch TMT20II
				*/
				$factura_corte = get_partial('corte_caja/factura'.$Prefijo.'Corte',$varsPartial);
				break;
			default:
				/*
				* En caso de no estar configurado el parámetro app_prefijo, o que no es alguno 
				* de los anteriores se configura con respecto a OKI, 
				*/
				$factura_corte = get_partial('corte_caja/facturaCorte',$varsPartial);
				error_log('ERROR GENERICO!!! No se ha configurado el parámetro PREFIJO o el valor NO EXISTE en la configuración. Fn=facturaCorteTasa0');
				break;
		}

		//Aumentamos el folio actual fiscal
		$ffis->setFolioActual($ffis->getFolioActual()+1);
		$ffis->save();

		if(DEBUG_LIB_IMPRESORASTICKETS)
			error_log("\n" . date("Ymd H:i:s") . "  [LIB::ImpresoraTickets] Actualizando Folio Fiscal (anterior => nuevo)      (" . $f->getFolio() . " => " . $ffis->getFolioActual() .")" ,3,DEBUG_LIB_IMPRESORASTICKETS_LOG);

		//Imprimirmos facturas
		$nomtick= PATH_FACTURAS."/".$confFactura['Emisor']['rfc'].$confFactura['serie'].$confFactura['folio'].'.txt';
		$fp = fopen($nomtick, "w+");
		fwrite($fp,$factura_corte);
		fclose($fp);
		if(DEBUG_LIB_IMPRESORASTICKETS)
			error_log("\n" . date("Ymd H:i:s") . "  [LIB::ImpresoraTickets] Guardando archivo físico => " . $nomtick ,3,DEBUG_LIB_IMPRESORASTICKETS_LOG);

		//La factura imprimible.
		$imprimible= '/tmp/factura_corte_t0_'.$id.'.txt';
		$fp = fopen($imprimible, "w+");
		fwrite($fp,$factura_corte);
		fclose($fp);
	}

	/**
	 * Genera e imprime el ticket de corte
	 * @param sfEvent $ev
	 * @return void
	 */
	static public function ticketCorte(sfEvent $ev)
	{

		$ctx = sfContext::getInstance();
        $ctx->getConfiguration()->loadHelpers('Partial');
        $ctx->getConfiguration()->loadHelpers('Facturas');

		$corte = $ev['corte'];
		$ordenes = $ev['ordenes'];

		$ip = Comun::ipReal();
		$id = $corte->getId();

		$ctx = sfContext::getInstance();
		$ctx->getConfiguration()->loadHelpers('Partial');

		$ventas_categoria = CortePeer::ventasPorCategoria($corte->getId());
		//Pasamos el total de ventas tasa 0
		$aTot = CortePeer::foliosTasa0($corte);
		$total_tasa0 = isset($aTot['total_tasa0']) ? $aTot['total_tasa0'] : 0; 
		//Pasamos un arreglo con las tasas para cada orden
		$tasas = array();
		foreach($ordenes as $orden)
		{
			$tasa0 = DetalleOrdenPeer::tasa_productos($orden);
			if($tasa0){
				$tasas[$orden->getId()] = 0;
			}
			else
			{
				$tasas[$orden->getId()] = $orden->getCentro()->getIva();
			}
		}
		/*
        * Reingenieria  Mako. SIN - ID RQ
        * Responsables:  (BP) Bet Gader Porcayo Juárez, (LGL) Luis Gonzales Linares
        * Fecha:        21-01-2015
        * Descripción:  Se añade opcion para configuración de Nuevas Impresoras
        */
        $Prefijo = @sfConfig::get('app_prefijo');
		switch ($Prefijo) {
			case 'oki':
				$ticket_resumen = get_partial('corte_caja/ticketCorte',array('ords'=>$ordenes,'cc'=>$corte,'ventas_categoria'=>$ventas_categoria, 'total_tasa0' => $total_tasa0));
				$ticket_detalle = get_partial('corte_caja/ticketsVentas',array('ordenes'=>$ordenes, 'tasas' => $tasas));
				break;
			case 'Ch':
			case 'TMT20II':
				/*
				* En este caso se incluye el tipo Ch TMT20II
				*/
				$ticket_resumen = get_partial('corte_caja/ticket'.$Prefijo.'Corte',array('ords'=>$ordenes,'cc'=>$corte,'ventas_categoria'=>$ventas_categoria, 'total_tasa0' => $total_tasa0));
				$ticket_detalle = get_partial('corte_caja/tickets'.$Prefijo.'Ventas',array('ordenes'=>$ordenes, 'tasas' => $tasas ));
				break;
			default:
				/*
				* En caso de no estar configurado el parámetro app_prefijo, o que no es alguno 
				* de los anteriores se configura con respecto a OKI, 
				*/
				$ticket_resumen = get_partial('corte_caja/ticketCorte',array('ords'=>$ordenes,'cc'=>$corte,'ventas_categoria'=>$ventas_categoria, 'total_tasa0' => $total_tasa0));
				$ticket_detalle = get_partial('corte_caja/ticketsVentas',array('ordenes'=>$ordenes, 'tasas' => $tasas));
				error_log('ERROR GENERICO!!! No se ha configurado el parámetro PREFIJO o el valor NO EXISTE en la configuración. Fn=ticketCorte');
				break;
		}


		//Imprimirmos tickets resumen
		$nomtick="/tmp/ticket_resumen".$id. ".txt";

		$fp = fopen($nomtick, "w+");
		fwrite($fp,$ticket_resumen);
		fclose($fp);

		$nomtick=PATH_TICKETS."/ticket_resumen".$id. ".txt";
		$fp = fopen($nomtick, "w+");
		fwrite($fp,$ticket_resumen);
		fclose($fp);
		//Imprimirmos tickets detalle
		$nomtick="/tmp/ticket_detalle".$id. ".txt";
		$fp = fopen($nomtick, "w+");
		fwrite($fp,$ticket_detalle);
		fclose($fp);
		//$res = exec("lp -h ".$ip." -d okipos $nomtick");

		$nomtick=PATH_TICKETS."/ticket_detalle".$id. ".txt";
		$fp = fopen($nomtick, "w+");
		fwrite($fp,$ticket_detalle);
		fclose($fp);

		error_log("Termina ticketCorte");
	}

	/**
	 * Genera e imprime el ticket de socio
	 * @param sfEvent $ev
	 * @return void
	 */
	static public function ticketSocio(sfEvent $ev)
	{
		$socio = $ev['socio'];

		$ip = Comun::ipReal();
		$id = Comun::generaId();

		$ctx = sfContext::getInstance();
		$ctx->getConfiguration()->loadHelpers('Partial');





		/*
        * Reingenieria  Mako. SIN - ID RQ
        * Responsables:  (BP) Bet Gader Porcayo Juárez, (LGL) Luis Gonzales Linares
        * Fecha:        21-01-2015
        * Descripción:  Se añade opcion para configuración de Nuevas Impresoras
        */
        $Prefijo = @sfConfig::get('app_prefijo');
		switch ($Prefijo) {
			case 'oki':
				$ticket = get_partial('registro/ticketSocio',array('socio'=>$socio));
				break;
			case 'Ch':
			case 'TMT20II':
				/*
				* En este caso se incluye el tipo Ch TMT20II
				*/
				$ticket = get_partial('registro/ticket'.$Prefijo.'Socio',array('socio'=>$socio));
				break;
			default:
				/*
				* En caso de no estar configurado el parámetro app_prefijo, o que no es alguno 
				* de los anteriores se configura con respecto a OKI, 
				*/
				$ticket = get_partial('registro/ticketSocio',array('socio'=>$socio));
				error_log('ERROR GENERICO!!! No se ha configurado el parámetro PREFIJO o el valor NO EXISTE en la configuración. Fn=ticketCorte');
				break;
		}

		$nomtick="/tmp/ticket_socio".$id. ".txt";
		$fp = fopen($nomtick, "w+");
		fwrite($fp,$ticket);
		fclose($fp);
        error_log("lp -h ".$ip." -d okipos $nomtick");
		$res = exec("lp -h ".$ip." -d okipos $nomtick");
	}
    
    /**
    * Envia correos electronicos
    *
    * @param string $email   - Direccion al a que se envia el correo
    * @param string $subject - Mensaje del correo
    * @param string $body    - Cuerpo del mensaje
    * @param array $attachs  - Arreglo con los attachs
    *
    * @return bool           - Respuesta del envio de correos
    */
    public static function sendCorreo($email, $subject, $body, $adjuntos = array() )
    {
        $mailer = sfContext::getInstance()->getMailer();
        $mensaje = Swift_Message::newInstance()
            ->setFrom(array('comprobantes@facturando.net' => 'Mako'))
            ->setTo($email)
            ->setSubject($subject)
            ->setBody($body,'text/html');
        foreach ($adjuntos as $attach)
        {
            $mensaje->attach(Swift_Attachment::fromPath($attach));
        }
        error_log('Enviando correo a '.$email.', con subject "'.$subject.'"');
        $result = $mailer->batchSend($mensaje);
        error_log('Respuesta del correo: '.($result ? 'SEND' : 'FAILED'));
    }
}
?>
