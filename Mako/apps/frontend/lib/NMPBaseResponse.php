<?php

/**
 * Clase que contiene los valores base para la respuesta de los WS de Mako a NMP
 * 
 * @author silvio.bravo
 * @copyright ENOVA
 * @category Web Services.
 *
 */

class NMPBaseResponse{
	
	
	public $success;
	public $message;
	public $data;
	
	public function __construct(){
		
		$this->Success	= "true";
		$this->message	= "";
		$this->data		= [];
		
	}
}