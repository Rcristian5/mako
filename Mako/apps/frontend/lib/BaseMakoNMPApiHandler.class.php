<?php

/**
 * This is an auto-generated SoapHandler. All changes to this file will be overwritten.
 */
class BaseMakoNMPApiHandler extends ckSoapHandler
{
	public function corte_caja_reporteVentasDiaria($centro, $fechaIni, $fechaFin)
	{
		return sfContext::getInstance()->getController()->invokeSoapEnabledAction('corte_caja', 'reporteVentasDiaria', array($centro, $fechaIni, $fechaFin));
	}
}