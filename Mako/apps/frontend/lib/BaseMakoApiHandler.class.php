<?php

/**
 * This is an auto-generated SoapHandler. All changes to this file will be overwritten.
 */
class BaseMakoApiHandler extends ckSoapHandler
{
  public function reporte_getResumenVentas($fecha_inicio, $fecha_fin)
  {
    return sfContext::getInstance()->getController()->invokeSoapEnabledAction('reporte', 'getResumenVentas', array($fecha_inicio, $fecha_fin));
  }

  public function reporte_getReporteDetalleOrdenes($fecha_inicio, $fecha_fin)
  {
    return sfContext::getInstance()->getController()->invokeSoapEnabledAction('reporte', 'getReporteDetalleOrdenes', array($fecha_inicio, $fecha_fin));
  }

  public function reporte_getMovimientosSocios()
  {
    return sfContext::getInstance()->getController()->invokeSoapEnabledAction('reporte', 'getMovimientosSocios', array());
  }

  public function reporte_getRegistroSociosRango($fecha_ini, $fecha_fin)
  {
    return sfContext::getInstance()->getController()->invokeSoapEnabledAction('reporte', 'getRegistroSociosRango', array($fecha_ini, $fecha_fin));
  }

  public function reporte_getSesiones()
  {
    return sfContext::getInstance()->getController()->invokeSoapEnabledAction('reporte', 'getSesiones', array());
  }

  public function reporte_getSesionesRango($fecha_ini, $fecha_fin)
  {
    return sfContext::getInstance()->getController()->invokeSoapEnabledAction('reporte', 'getSesionesRango', array($fecha_ini, $fecha_fin));
  }

  public function reporte_getSalidasRango($fecha_ini, $fecha_fin)
  {
    return sfContext::getInstance()->getController()->invokeSoapEnabledAction('reporte', 'getSalidasRango', array($fecha_ini, $fecha_fin));
  }

  public function reporte_getConciliacionesRango($fecha_ini, $fecha_fin)
  {
    return sfContext::getInstance()->getController()->invokeSoapEnabledAction('reporte', 'getConciliacionesRango', array($fecha_ini, $fecha_fin));
  }

  public function reporte_getOrdenesRango($fecha_ini, $fecha_fin)
  {
    return sfContext::getInstance()->getController()->invokeSoapEnabledAction('reporte', 'getOrdenesRango', array($fecha_ini, $fecha_fin));
  }

  public function reporte_getProductos($id)
  {
    return sfContext::getInstance()->getController()->invokeSoapEnabledAction('reporte', 'getProductos', array($id));
  }

  public function reporte_getReporteMetas($fecha_ini, $fecha_fin)
  {
    return sfContext::getInstance()->getController()->invokeSoapEnabledAction('reporte', 'getReporteMetas', array($fecha_ini, $fecha_fin));
  }

  public function reporte_getReporteVentasCategorias($fecha_ini, $fecha_fin)
  {
    return sfContext::getInstance()->getController()->invokeSoapEnabledAction('reporte', 'getReporteVentasCategorias', array($fecha_ini, $fecha_fin));
  }

  public function reporte_getReporteDiasOperacion($fecha_ini, $fecha_fin)
  {
    return sfContext::getInstance()->getController()->invokeSoapEnabledAction('reporte', 'getReporteDiasOperacion', array($fecha_ini, $fecha_fin));
  }

  public function reporte_getReporteOcupacionHoras($fecha_ini, $fecha_fin)
  {
    return sfContext::getInstance()->getController()->invokeSoapEnabledAction('reporte', 'getReporteOcupacionHoras', array($fecha_ini, $fecha_fin));
  }

  public function reporte_getReporteEstatusSocios($fecha_ini, $fecha_fin)
  {
    return sfContext::getInstance()->getController()->invokeSoapEnabledAction('reporte', 'getReporteEstatusSocios', array($fecha_ini, $fecha_fin));
  }

  public function reporte_getReporteSociosActivos($fecha_ini, $fecha_fin)
  {
    return sfContext::getInstance()->getController()->invokeSoapEnabledAction('reporte', 'getReporteSociosActivos', array($fecha_ini, $fecha_fin));
  }

  public function reporte_getReporteCategoriaCursos($fecha_ini, $fecha_fin)
  {
    return sfContext::getInstance()->getController()->invokeSoapEnabledAction('reporte', 'getReporteCategoriaCursos', array($fecha_ini, $fecha_fin));
  }

  public function reporte_getReporteVentaCursos($fecha_ini, $fecha_fin)
  {
    return sfContext::getInstance()->getController()->invokeSoapEnabledAction('reporte', 'getReporteVentaCursos', array($fecha_ini, $fecha_fin));
  }

  public function reporte_getReporteRankingCentralWS($semana, $anio, $fechaInicio, $fechaFin)
  {
    return sfContext::getInstance()->getController()->invokeSoapEnabledAction('reporte', 'getReporteRankingCentralWS', array($semana, $anio, $fechaInicio, $fechaFin));
  }

  public function reporte_getReporteRankingLocalWS($centroId, $semana, $anio, $fechaInicio, $fechaFin)
  {
    return sfContext::getInstance()->getController()->invokeSoapEnabledAction('reporte', 'getReporteRankingLocalWS', array($centroId, $semana, $anio, $fechaInicio, $fechaFin));
  }

  public function reporte_getReporteAsistencias($desde, $hasta)
  {
    return sfContext::getInstance()->getController()->invokeSoapEnabledAction('reporte', 'getReporteAsistencias', array($desde, $hasta));
  }

  public function registro_getSocio($socio_id)
  {
    return sfContext::getInstance()->getController()->invokeSoapEnabledAction('registro', 'getSocio', array($socio_id));
  }

  public function registro_getSocioPorUsuario($usuario)
  {
    return sfContext::getInstance()->getController()->invokeSoapEnabledAction('registro', 'getSocioPorUsuario', array($usuario));
  }

  public function registro_listaInactivos($fecha)
  {
    return sfContext::getInstance()->getController()->invokeSoapEnabledAction('registro', 'listaInactivos', array($fecha));
  }

  public function registro_agregaSocio($centroId, $datos)
  {
    return sfContext::getInstance()->getController()->invokeSoapEnabledAction('registro', 'agregaSocio', array($centroId, $datos));
  }

  public function sync_recibeSync($datos, $operacion, $clase, $pk)
  {
    return sfContext::getInstance()->getController()->invokeSoapEnabledAction('sync', 'recibeSync', array($datos, $operacion, $clase, $pk));
  }

  public function control_getListasAsistencia($desde, $hasta)
  {
    return sfContext::getInstance()->getController()->invokeSoapEnabledAction('control', 'getListasAsistencia', array($desde, $hasta));
  }

  public function conciliacion_adm_conciliacion($centro_id, $operador_id, $monto, $folio_docto_aval, $ip_caja, $observaciones)
  {
    return sfContext::getInstance()->getController()->invokeSoapEnabledAction('conciliacion_adm', 'conciliacion', array($centro_id, $operador_id, $monto, $folio_docto_aval, $ip_caja, $observaciones));
  }

  public function sesiones_getSesiones($socio_id, $fecha, $operacion, $numero_registros)
  {
    return sfContext::getInstance()->getController()->invokeSoapEnabledAction('sesiones', 'getSesiones', array($socio_id, $fecha, $operacion, $numero_registros));
  }

  public function asignacion_becas_getSolicitudesBecas($centro_id)
  {
    return sfContext::getInstance()->getController()->invokeSoapEnabledAction('asignacion_becas', 'getSolicitudesBecas', array($centro_id));
  }

  public function reporteVentasDiaria($centro, $fechaIni, $fechaFin)
  {
    return sfContext::getInstance()->getController()->invokeSoapEnabledAction('corte_caja', 'reporteVentasDiaria', array($centro, $fechaIni, $fechaFin));
  }
}