<?php
class NMPReporteVentasResponse{
	
	public $centro;
	public $fecha;
	public $hora;
	public $folioVenta;
	public $totalVenta;
	public $socioNombre;
	public $socioPaterno;
	public $socioMaterno;
	public $socioFolio;
	public $socioFechaNac;
	public $producto;
	public $categoria;
	public $cantidad;
	public $precioLista;
	public $descuento;
	public $tipoOrden;
	
	
	
	public function __construct(){
		
		$this->centro		="";
		$this->fecha		="";
		$this->hora			="";
		$this->folioVenta	="";
		$this->totalVenta	="";
		$this->producto		="";
		$this->categoria	="";
		$this->cantidad		="";
		$this->precioLista	="";
		$this->descuento	="";
		$this->tipoOrden	="";
	
	}
}