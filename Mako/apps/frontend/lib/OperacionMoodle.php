<?php 
/**
 * Clase que encapsula las operaciones con Moodle
 */

class OperacionMoodle
{
	
	static public function registrarSocioBind(sfEvent $ev)
	{
		$socio = $ev['socio'];
		self::registrarSocio($socio);				
	}
	
	static public function actualizarSocioBind(sfEvent $ev)
	{
		$socio = $ev['socio'];
		$modificaUsername = $ev['modificaUsername'];
		$usuarioAnterior = $ev['usuario_anterior'];
		self::actualizarSocio($socio, $modificaUsername, $usuarioAnterior);
	}
	
	static public function registrarSocio(Socio $socio)
	{
				
		//Se cargan las classes de tipos de objetos que espera recibir el ws de moodle.
		require_once dirname(__FILE__).'/../../../ws_proxy/editUsersInput.php';
		require_once dirname(__FILE__).'/../../../ws_proxy/userData.php';
		
		error_log("LDAP uid:".$socio->getLdapUid()."");
		try
		{
			list($cliente,$sesion) = self::clienteWsMoodle();
		}
		catch(Exception $e)
		{
			error_log("Error[OperacionMoodle.registrarSocio]: ".$e->getMessage());
			return;
		}
		
		//Instanciamos el tipo
		$usarios = new editUsersInput();
		
		//Si el correo es vacio ponemos uno default
		
		if(trim($socio->getEmail())=='' or trim($socio->getEmail())==null) $email = 'cursos@ria.mx';
		else $email = trim($socio->getEmail());  
		
		//Generamos los atributos
		$usuarios->users = array(
		    array(
		        'action'       => 'Add',
		        'confirmed'    => 1,
		        'policyagreed' => 0,
		        'deleted'      => 0,
		        'mnethostid'   => 3,
		        'username'     => $socio->getUsuario(),
		        'auth'         => 'ldap',
		        'password'     => md5($socio->getClave()),
		        'idnumber'     => $socio->getLdapUid(),
		        'firstname'    => $socio->getNombre(),
		        'lastname'     => $socio->getApepat()." ".$socio->getApemat(),
		        'email'        => $email,
		        'icq'          => '',
		        'skype'        => '',
		        'yahoo'        => '',
		        'aim'          => '',
		        'msn'          => '',
		        'phone1'       => $socio->getTel(),
		        'phone2'       => $socio->getCelular(),
		        'institution'  => 'RIA',
		        'department'   => '',
		        'address'      => Comun::filtraAcentos(substr($socio->getDirgmaps(),0,69)),
		        'city'         => substr($socio->getEstado(),0,19),
		        'country'      => 'MX',
		        'lang'         => 'es_utf8',
		        'timezone'     => '',
		        'lastip'       => '',
		        'description'  => 'SOCIO'
		    )
		);
		
		//Enviamos los datos al ws
		try
		{
			$res = $cliente->edit_users($sesion->client, $sesion->sessionkey, $usuarios);
			error_log("Se acaba de registrar en Moodle: ".$socio);			
		}
		catch (Exception $e)
		{
			error_log("Error[OperacionMoodle.registrarSocio]: ".$e->getMessage());
		}
	}
	
	/**
	 * Actualiza los datos del socio en moodle 
	 * @param Socio $socio
	 * @param boolean $modificaUsername
	 * @param string $usuarioAnterior
	 */
	static public function actualizarSocio(Socio $socio, $modificaUsername, $usuarioAnterior)
	{
		
		//Se cargan las classes de tipos de objetos que espera recibir el ws de moodle.
		require_once dirname(__FILE__).'/../../../ws_proxy/editUsersInput.php';
		require_once dirname(__FILE__).'/../../../ws_proxy/userData.php';

		try
		{
			list($cliente,$sesion) = self::clienteWsMoodle();
		}
		catch(Exception $e)
		{
			error_log("Error[OperacionMoodle.registrarSocio]: ".$e->getMessage());
			return;
		}
		
		if(trim($socio->getEmail())=='' or trim($socio->getEmail())==null) $email = 'cursos@ria.mx';
		else $email = trim($socio->getEmail());  
		
		$regUsuario = array(
		        'action'       => 'Update',
		        'confirmed'    => 1,
		        'policyagreed' => 0,
		        'deleted'      => 0,
		        'mnethostid'   => 3,
		        'username'     => $socio->getUsuario(),
		        'auth'         => 'ldap',
		        'password'     => md5($socio->getClave()),
		        'idnumber'     => $socio->getLdapUid(),
		        'firstname'    => $socio->getNombre(),
		        'lastname'     => $socio->getApepat()." ".$socio->getApemat(),
		        'email'        => $email,
		        'icq'          => '',
		        'skype'        => '',
		        'yahoo'        => '',
		        'aim'          => '',
		        'msn'          => '',
		        'phone1'       => $socio->getTel(),
		        'phone2'       => $socio->getCelular(),
		        'institution'  => 'RIA',
		        'department'   => '',
		        'address'      => Comun::filtraAcentos(substr($socio->getDirgmaps(),0,69)),
		        'city'         => substr($socio->getEstado(),0,19),
		        'country'      => 'MX',
		        'lang'         => 'es_utf8',
		        'timezone'     => '',
		        'lastip'       => '',
		        'description'  => 'SOCIO'
		    );
		
		
/*		$id_usuario='';
		if($modificaUsername == true)
		{
			$ua = $cliente->get_users($sesion->client, $sesion->sessionkey,array($usuarioAnterior),'username');
			$id_usuario = $ua->users[0]->id;
			$regUsuario['id'] = $id_usuario;
		}
*/
		//Instanciamos el tipo
		$usarios = new editUsersInput();
		
		//Generamos los atributos
		$usuarios->users = array(
			$regUsuario
		);
		
		//Enviamos los datos al ws
		try
		{
			$res = $cliente->edit_users($sesion->client, $sesion->sessionkey, $usuarios);
			error_log("Se acaba de actualizar en Moodle: ".$socio);			
		}
		catch (Exception $e)
		{
			error_log("Error[OperacionMoodle.actualizarSocio]: ".$e->getMessage());
		}
				
	}
	
	
	/**
	 * Escucha el evento de alumno.inscrito y ejecuta el enrolamiento
	 * @param sfEvent $ev
	 * @return void
	 */
	static public function inscribirAlumnoBind(sfEvent $ev)
	{
		$curso = $ev['curso'];
		$socio = $ev['socio'];
		$grupo = $ev['grupo'];
		
		self::inscribirAlumno($curso,$socio,$grupo);
	}
	
	/**
	 * Inscribe al alumno al curso en moodle
	 * 
	 * @param Curso $curso
	 * @param Socio $socio
	 * @param Grupo $grupo
	 */
	static public function inscribirAlumno(Curso $curso, Socio $socio, Grupo $grupo)
	{
		
		if(!$curso->getAsociadoMoodle())
		{
			error_log("OK[inscribirAlumno]: CURSO NO ASOCIADO A MOODLE");
			return;
		}
		
		error_log("Vamos a inscribir en moodle a ".$socio->getUsuario()." al curso ".$curso. " al grupo ".$grupo->getClave());
		
		//Se cargan las classes de tipos de objetos que espera recibir el ws de moodle.
		require_once dirname(__FILE__).'/../../../ws_proxy/editUsersInput.php';
		require_once dirname(__FILE__).'/../../../ws_proxy/userData.php';
		
		try
		{
			list($cliente,$sesion) = self::clienteWsMoodle();
		}
		catch(Exception $e)
		{
			error_log("Error[OperacionMoodle.registrarSocio]: ".$e->getMessage());
			return;
		}
		
		
		$clave = $curso->getClave();
		$usuario = $socio->getUsuario();
		$cve_grupo = $grupo->getClave();
		try
		{
			$res = $cliente->enrol_students($sesion->client, $sesion->sessionkey, $clave, array($usuario),'idnumber','username');
		}
		catch(Exception $e)
		{
			error_log("Error[OperacionMoodle.inscribirAlumno]: ".$e->getMessage());
			return;
		}
		try
		{
			$res = $cliente->add_group_member($sesion->client, $sesion->sessionkey,$cve_grupo,$clave,$usuario);
			error_log("OK[OperacionMoodle.inscribirAlumno]: $usuario|$clave|$cve_grupo");
		}
		catch(Exception $e)
		{
			error_log("Error[OperacionMoodle.inscribirAlumno]: ".$e->getMessage());
			return;
		}
	}
	
	/**
	 * Inicializa el cliente y la sesion
	 * 
	 * @throws SoapFault
	 * @return array Regresa un arreglo con el cliente y el token de sesion.
	 */
	static public function clienteWsMoodle()
	{
		
		$wsurl = sfConfig::get('app_soap_client');
		$usr = sfConfig::get('app_soap_user');
		$pwd = sfConfig::get('app_soap_password');
		
		try
		{
			//Inicializamos el cliente
			$cliente = new SoapClient($wsurl);
		}
		catch(Exception $e)
		{
			throw new SoapFault('Server', $e);
			return array();
		}

		try
		{
			//Obtenemos el token de sesion
			$sesion = $cliente->login($usr,$pwd);
		}
		catch(Exception $e)
		{
			throw new SoapFault('Server', $e);
			return array();
		}
	
		return array($cliente,$sesion);
		
	}
	
	
	static public function sso($usuario)
	{
		try
		{
			list($cliente,$sesion) = self::clienteWsMoodle();
		}
		catch(Exception $e)
		{
			error_log("Error[OperacionMoodle.registrarSocio]: ".$e->getMessage());
			return;
		}

		try
		{
			$auth = $cliente->authenticate_user($sesion->client,$sesion->sessionkey, $usuario,sfConfig::get('app_soap_sso_token'));
			return $auth;
		}
		catch(Exception $e)
		{
			error_log("Error[OperacionMoodle.sso]: $usuario: ".$e->getMessage());
			return false;		
		}
		
	}
	
	
	static public function comprobarRegistro(Socio $socio)
	{
		try
		{
			list($cliente,$sesion) = self::clienteWsMoodle();
		}
		catch(Exception $e)
		{
			error_log("Error[OperacionMoodle.registrarSocio]: ".$e->getMessage());
			return;
		}

		try
		{
	  		$usuario = $cliente->get_user_byusername($sesion->client, $sesion->sessionkey,$socio->getUsuario());
			return $usuario;
		}
		catch(Exception $e)
		{
			error_log("Error[OperacionMoodle.comprobarRegistro]: $usuario: ".$e->getMessage());
			return false;		
		}
		
	}

	
	static public function consultarRegistro($usuario)
	{
		try
		{
			list($cliente,$sesion) = self::clienteWsMoodle();
		}
		catch(Exception $e)
		{
			error_log("Error[OperacionMoodle.registrarSocio]: ".$e->getMessage());
			return;
		}

		try
		{
	  		$usuario = $cliente->get_user_byusername($sesion->client, $sesion->sessionkey,$usuario);
			return $usuario;
		}
		catch(Exception $e)
		{
			error_log("Error[OperacionMoodle.comprobarRegistro]: $usuario: ".$e->getMessage());
			return false;		
		}
		
	}
	
	
}

?>