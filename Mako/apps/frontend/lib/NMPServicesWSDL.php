<?php
class NMPServicesWSDL{


   /**
    * Metodo provisional para conectarse a NMP y realizar el registro de un socio.
    * @silvio.bravo@enova.mx
    * @param sfEvent $ev
    */
   public static function agregarSocio(sfEvent $ev){
      if ($ev->getName() == 'syncNMP.socio.registrado') {
         error_log('Registrando usuario en NMP por medio de su web service');

      } else {
         error_log('Se manda a sincronia NMP para no tardar en el registro');

         $socio     = $ev['socio'];
         SincroniaNmpPeer::sincronia('socio',
                                     'agregarSocio',
                                     $socio->getId(),
                                     'Para hacer mas rapida la inscripcion');
         return true;
      }

      try{
         $socio  = $ev['socio'];
         $client = self::cliente_soap_nmp();
         $params = self::construir_parametros($socio);

         ini_set('default_socket_timeout', 600);

         $result = $client->__soapCall('RegistrarSocio', array($params), NULL);

         error_log('Llamada a web service terminada');

         if(isset($result->StatusCenmp) and ($result->StatusCenmp == 'OK')) {
            error_log('Socio registrado correctamente en web service');
            //:TODO: bruno : 2014 07 05 : Hacer algo con este id?
            // $result->IdRegistroSocio;
            // $result->StatusCenmp // OK

            // $socio->setClienteMp($result->IdRegistroSocio);
            // $socio->save();

            error_log('[SINCRONIA] agregarSocio: '.print_r($result, true));

            SincroniaNmpPeer::sincronia('socio',
                                        'agregarSocio',
                                        $socio->getId(),
                                        'ID registro socio: ['.$result->IdRegistroSocio.']',
                                        true);

         } else {
            error_log('Ocurrio un error al registrar el usuario en el web service, se registra para intentarlo posteriormente');
            SincroniaNmpPeer::sincronia('socio', 'agregarSocio', $socio->getId(), serialize($result));
         }

      }catch( SoapFault $sp) {
         error_log("Ocurrio un error al momento de consumir el servicio de NMP para registro de socio :(");

         $sincronia = SincroniaNmpPeer::sincronia('socio', 'agregarSocio', $socio->getId(), $sp->faultstring);

         /*
          * Para hacer un mejor debug, muestra varios datos para ver donde falló
         Comun::dbg(array('request' => $client->__getLastRequest(),
                          'headers' => $client->__getLastResponseHeaders(),
                          'error'   => $client->__getLastResponse(),
                          'fault'   => array('code'   => $sp->faultcode,
                                             'string' => $sp->faultstring,
                                             'actor'  => $sp->faultactor,
                                             'detail' => $sp->faultdetail,
                                             'name'   => $sp->faultname,
                                             'header' => $sp->faultheader)),
                    'pre');

         Comun::dbg($socio, 'pre');
         Comun::dbg($params);
         */
      }
   }

   public static function actualizarSocio(sfEvent $ev){
      if ($ev->getName() == 'syncNMP.socio.actualizado') {
         error_log('Actualizando usuario en NMP');

      } else {
         error_log('Se manda a sincronia NMP para no tardar en la actualizacion');

         $socio = $ev['socio'];
         SincroniaNmpPeer::sincronia('socio',
                                     'actualizarSocio',
                                     $socio->getId(),
                                     'Para hacer mas rapido el proceso de actualizacion.');
         return true;
      }

      try{
         $socio  = $ev['socio'];
         $client = self::cliente_soap_nmp();
         $params = self::construir_parametros($socio, true);

         ini_set('default_socket_timeout', 600);

         $result = $client->__soapCall('RegistrarSocio', array($params), NULL);

         error_log('Se termina la llamada al web service');

         if(isset($result->StatusCenmp) and ($result->StatusCenmp == 'OK')) {
            // todo bien... y ahora?
            // $result->IdRegistroSocio

            error_log('Se actualizo el socio correctamente');

            $sincronia = SincroniaNmpPeer::sincronia('socio',
                                                     'actualizarSocio',
                                                     $socio->getId(),
                                                     'ID registro socio: ['.$result->IdRegistroSocio.']',
                                                     true);

         } else {
            error_log('Ocurrio un error al actualizar el socio en el web service, se registra para intentarlo posteriormente');

            $sincronia = SincroniaNmpPeer::sincronia('socio', 'actualizarSocio', $socio->getId(), serialize($result));
         }

      }catch( SoapFault $sp) {
         error_log("Ocurrio un error al momento de consumir el servicio de NMP para actualizar socio :(");

        $sincronia = SincroniaNmpPeer::sincronia('socio', 'actualizarSocio', $socio->getId(), $sp->faultstring);

        /*
         * Para debug, muestra datos para poder hacer un mejor debug
        Comun::dbg(array('request' => $client->__getLastRequest(),
                          'headers' => $client->__getLastResponseHeaders(),
                          'error'   => $client->__getLastResponse(),
                          'fault'   => array('code'   => $sp->faultcode,
                                             'string' => $sp->faultstring,
                                             'actor'  => $sp->faultactor,
                                             'detail' => $sp->faultdetail,
                                             'name'   => $sp->faultname,
                                             'header' => $sp->faultheader)),
                    'pre');
        Comun::dbg($socio, 'pre');
        Comun::dbg($params);
        */
      }
   }

   /**
    * inscripcion temporal de socio a wsdl NMP
    */
   public static function inscribirSocio(sfEvent $ev){
      if ($ev->getName() == 'syncNMP.alumno.inscrito') {
         error_log("Iniciando registro de socio en NMP WSDL :) \n");

      } else {
         error_log('Se manda a sincronia NMP para no tardar en la inscripcion');

         $curso = $ev['curso'];
         $socio = $ev['socio'];
         $grupo = $ev['grupo'];

         SincroniaNmpPeer::sincronia('alumno',
                                     'inscrito',
                                     array('curso' => $curso->getId(),
                                           'grupo' => $grupo->getId(),
                                           'socio' => $socio->getId()),
                                     'Para hacer mas rapido el proceso de inscripcion.');
         return true;
      }

      try{
         $curso = $ev['curso'];
         $socio = $ev['socio'];
         $grupo = $ev['grupo'];

         $url    = sfConfig::get('app_nmp_wsdl');
         $client = new SoapClient(
            $url,
            array(
               'soap_version' => SOAP_1_1,
               'proxy_host'   => sfConfig::get('app_nmp_proxy_host'),
               'proxy_port'   => sfConfig::get('app_nmp_proxy_port'),
               'trace'        => true,
               'exceptions'   => true,
               'encoding'     => "UTF-8"
            ));

         $params = array(
            "PerteneceTEC" => 0,
            "idCurso"      => $curso->getId(),
            "idGrupo"      => $grupo->getId(),
            "idUsuario"    => $socio->getUsuario(),
            "descCentro"   => null,
            "emailProm"    => null
         );

         $result = $client->__soapCall("Inscribir", array($params),NULL);

         error_log('Se termino la llamada al web service');

         $result = $client->__getLastRequest();

         //Validar si la llamada fue exitosa o no.
         if(true) { //Por el momento lo dejamos así, pero tiene que cambiarse por una validación real
            error_log("Se ha realizado la incripcion del socio a NMP WSDL");

            SincroniaNmpPeer::sincronia('alumno',
                                        'inscrito',
                                        array('curso' => $curso->getId(),
                                              'grupo' => $grupo->getId(),
                                              'socio' => $socio->getId()),
                                        'Id registro: []', //Registrar algo...
                                        true);

         }else{
            error_log('Ocurrió un error al inscribir al socio, se registra para volver a intentarlo despues.');

            SincroniaNmpPeer::sincronia('alumno',
                                        'inscrito',
                                        array('curso' => $curso->getId(),
                                              'grupo' => $grupo->getId(),
                                              'socio' => $socio->getId()),
                                        serialize($result));
         }
      }
      catch( SoapFault $sp){
         error_log("Ha Ocurrido un error al momento de realizar la inscripcion del socio en NMP WSDL :( \n  ");

         SincroniaNmpPeer::sincronia('alumno',
                                     'inscrito',
                                     array('curso' => $curso->getId(),
                                           'grupo' => $grupo->getId(),
                                           'socio' => $socio->getId()),
                                     $sp->faultstring);
      }
   }

   public static function cancelarInscripcionSocio(sfEvent $ev) {
      if ($ev->getName() == 'syncNMP.alumno.cancelar.inscripcion') {
         error_log('Cancelando inscripcion al curso en NMP');

      } else {
         error_log('Se manda a sincronia NMP para no tardar en la cancelacion');

         $curso = $ev['curso'];
         $socio = $ev['socio'];

         SincroniaNmpPeer::sincronia('alumno',
                                     'cancelar',
                                     array('socio' => $socio->getId(),
                                           'curso' => $curso->getId()),
                                     'Para hacer mas rapido el proceso de cancelacion.');
         return true;
      }

      try{
         $socio  = $ev['socio'];
         $curso  = $ev['curso'];
         $client = self::cliente_soap_nmp();
         //$params = self::construir_parametros($socio, true);

         ini_set('default_socket_timeout', 600);

         $result = $client->__soapCall('cancelarInscripcionSocio', array($params), NULL);

         error_log('Se termina la llamada al web service');

         if(isset($result->StatusCenmp) and ($result->StatusCenmp == 'OK')) {
            error_log('Se cancelo la inscripcion del socio correctamente');

            $sincronia = SincroniaNmpPeer::sincronia('alumno',
                                                     'cancelar',
                                                     array('socio' => $socio->getId(),
                                                           'curso' => $curso->getId()),
                                                     'Cancelacion ['.$result.']',
                                                     true);

         } else {
            error_log('Ocurrio un error al cancelar la inscripcion del socio en el web service, se registra para intentarlo posteriormente');

            $sincronia = SincroniaNmpPeer::sincronia('alumno',
                                                     'cancelar',
                                                     array('socio' => $socio->getId(),
                                                           'curso' => $curso->getId()),
                                                     serialize($result));
         }

      }catch( SoapFault $sp) {
         error_log("Ocurrio un error al momento de consumir el servicio de NMP para cancelar la inscripcion del socio :(");

        $sincronia = SincroniaNmpPeer::sincronia('alumno',
                                                 'cancelar',
                                                 array('socio' => $socio->getId(),
                                                       'curso' => $curso->getId()),
                                                 $sp->faultstring);
      }
   }

   /**
    * Construye el cliente soap para comunicarse con el web service de NMP
    * @return SoapClient El cliente soap
    */
   public static function cliente_soap_nmp() {
      return new SoapClient(sfConfig::get('app_nmp_wsdl'), array(
         'soap_version' => SOAP_1_1,
         'proxy_host'   => sfConfig::get('app_nmp_proxy_host'),
         'proxy_port'   => sfConfig::get('app_nmp_proxy_port'),
         'trace'        => true,
         'exceptions'   => true,
         'encoding'     => "UTF-8"
      ));
   }

   /**
    * Construye los parametros para enviar a
    * @param  Socio  $socio Los datos del socio
    * @return array         Los parametros para mandar al web service de NMP
    */
   public static function construir_parametros(Socio $socio, $actualizar = false){
      // Quitar la clave de region agregada por mako, pero que NMP no la necesita (solo piden 10 digitos)
      $socio_celular = (strlen($socio->getCelular()) == 13) ? substr($socio->getCelular(), 3) : '';

      //Obtener la escolaridad del socio (si indico un nivel)
      $socio_escolaridad = !is_null($socio->getNivelId()) ? substr($socio->getNivelEstudio()->getNombre(), 3) : null;

      $params = array(
         "Actualizar"                 => self::validar_nmp($actualizar                        , 'boolean',     false),
         "Correo"                     => self::validar_nmp($socio->getEmail()                 , 'string' ,     false),
         "Usuario"                    => self::validar_nmp($socio->getUsuario()               , 'string' , 30, false),
         "Pasword"                    => self::validar_nmp($socio->getClave()                 , 'string' , 20, false),
         "Nombre"                     => self::validar_nmp($socio->getNombre()                , 'string' , 64, false),
         "ApellidoPaterno"            => self::validar_nmp($socio->getApepat()                , 'string' , 64, false),
         "ApellidoMaterno"            => self::validar_nmp($socio->getApemat()                , 'string' , 64, false),
         "FechaNacimiento"            => self::validar_nmp($socio->getFechaNac()              , 'string' ,     false),
         "Telefono"                   => self::validar_nmp($socio->getTel()                   , 'string' , 10),
         "Celular"                    => self::validar_nmp($socio_celular                     , 'string' , 10),
         "Escolaridad"                => self::validar_nmp($socio_escolaridad                 , 'string' , 20),
         "Sexo"                       => self::validar_nmp($socio->getSexo()                  , 'string' , 2),
         "Calle"                      => self::validar_nmp($socio->getCalle()                 , 'string' , 256),
         "NumeroExt"                  => self::validar_nmp($socio->getNumExt()                , 'string' , 20),
         "NumeroInt"                  => self::validar_nmp($socio->getNumInt()                , 'string' , 20),
         "Colonia"                    => self::validar_nmp($socio->getColonia()               , 'string' , 64),
         "CodigoPostal"               => self::validar_nmp($socio->getCp()                    , 'string' , 10),
         "Municipio"                  => self::validar_nmp($socio->getMunicipio()             , 'string' , 64),
         "Ciudad"                     => self::validar_nmp($socio->getCiudad()                , 'string' , 64),
         "ContactoEmergencias"        => self::validar_nmp($socio->getContactoEmergencias()   , 'string' , 30),
         "ContactoEmergencia1s"       => self::validar_nmp($socio->getContactoEmergencias()   , 'string' , 30),
         "TutorResponsable"           => self::validar_nmp($socio->getTutorResponsable()      , 'string' , 128),
         "TelefonoEmergencia"         => self::validar_nmp($socio->getTelefonoEmergencia()    , 'string' , 10),
         "Trabaja"                    => self::validar_nmp($socio->getTrabaja()               , 'si/no'  , true),
         "Estudia"                    => self::validar_nmp($socio->getEstudia()               , 'si/no'  , true),
         "CURP"                       => self::validar_nmp($socio->getCurp()                  , 'string' , 20),
         "HabilidadInformatica"       => self::validar_nmp($socio->getHabilidadInformaticaId(), 'si/no'  , array(2, 3, 4, 5, 7, 8, 9)),
         "FolioCupon"                 => self::validar_nmp($socio->getFolioCupon()            , 'string' , 20),
         "ClienteMP"                  => self::validar_nmp($socio->getClienteMp()             , 'string' , 20),
         "NegocioPropio"              => self::validar_nmp($socio->getOrganizacionId()        , 'si/no'  , 5),
         "DependientesEconomicos"     => self::validar_nmp($socio->getDependientesEconomicos(), 'int'),
         "CasaPropia"                 => self::validar_nmp($socio->getViviendaEsId()          , 'si/no'  , 4),
         "UtilizaInternet"            => self::validar_nmp($socio->getInternet()              , 'si/no'  , true),
         "IdMotivo"                   => self::validar_nmp($socio->getIdMotivoCapacitacion()  , 'match'  , array('motivo_capacitacion', 'string', 2)),
         "IdEstadoCivil"              => self::validar_nmp($socio->getEstadoCivilId()         , 'match'  , array('estado_civil'       , 'string', 2)),
         "IdEstado"                   => self::validar_nmp($socio->getIdEstado()              , 'match'  , array('entidad_federativa' , 'string', 2), false),
         "IdGrado"                    => self::validar_nmp($socio->getGradoId()               , 'match'  , array('nivel_estudio'      , 'string', 2), false),
         "IdOcupacion"                => self::validar_nmp($socio->getOcupacionId()           , 'match'  , array('ocupacion'          , 'string', 2)),
         "IdActEconomica"             => self::validar_nmp($socio->getSectorId()              , 'match'  , array('sector'             , 'string', 2)),
         "IdDominioIngles"            => self::validar_nmp($socio->getDominioInglesId()       , 'match'  , array('dominio_ingles'     , 'string', 2)),
         "IdDiscapacidad"             => self::validar_nmp($socio->getDiscapacidadId()        , 'match'  , array('discapacidad'       , 'string', 2)),
         "IdMotivoAcercamiento"       => self::validar_nmp($socio->getIdMotivoAcercamiento()  , 'match'  , array('motivo_acercamiento', 'string', 2)),
         "IdTiempoActividad"          => self::validar_nmp($socio->getIdTiempoActividad()     , 'match'  , array('tiempo_actividad'   , 'string', 2)),
         "IdProblemasActualesEmpresa" => self::validar_nmp($socio->getIdProblemaEmpresa()     , 'match'  , array('problemas_empresa'  , 'string', 2)),
         "IdMedio"                    => self::validar_nmp($socio->getCanalContactoId()       , 'match'  , array('canal_contacto'     , 'string', 2))
      );

      return $params;
   }

   /**
    * Evalua si el valor dado es igual al valorSi
    *
    * @param  mixed $valor   El valor que se comparara
    * @param  mixed $valorSi El valor de referencia
    *
    * @return string         'Si' en caso de que los valores dados sean igual y 'no' en caso contrario
    */
   public function evaluaValorParaSiNo($valor, $valorSi){
      if(is_array($valorSi)) {
         return in_array($valor, $valorSi) ? 'Si' : 'No';

      }else if($valor === $valorSi){
         return 'Si';
      }

      return 'No';
   }

   public function obtener_equivalencia($id_local, $tipo_equivalencia) {
      $equivalencia = EquivalanciaNmpPeer::obtener_equivalencia($id_local, $tipo_equivalencia);

      return is_null($equivalencia) ? null : $equivalencia->getIdNmp();
   }

   /**
    * Valida el campo para que el web service de NMP no se ponga reina
    * @param  mixed     $valor            El valor que se va a validar
    * @param  string    $tipo             El tipo que espera el web service de NMP
    * @param  mixed     $parametro        El limite del valor (para strings) o el valor en caso de "si" (para el tipo "si/no")
    * @param  boolean   $puede_ser_null   Si el valor puede o no puede ser null, morira si el valor es null y no puede serlo
    * @return mixed                       El valor limpio para que el web service de NMP no la haga de a pedo...
    */
   private function validar_nmp($valor, $tipo, $parametro = 255, $puede_ser_null = true) {
      // Ordenar parametros
      if(is_bool($parametro) and ($tipo != 'si/no')) {
         $puede_ser_null = $parametro;
      }

      //Checar los parametros extra
      if($tipo == 'match') { //equivalencias
         if(!is_array($parametro)){
            $d = debug_backtrace();
            throw new Exception('El $parametro tiene que ser un arreglo (equivalencia, puede_ser_null) MAKO_ROOT/apps/frontend/lib/NMPServicesWSDL@'.$d[0]['line']);
         }
      }

      if(!$puede_ser_null and is_null($valor)) {
         $d = debug_backtrace();
         throw new Exception('El valor no puede ser nulo MAKO_ROOT/apps/frontend/lib/NMPServicesWSDL@'.$d[0]['line']);
      }

      $valor_limpio = '';

      switch ($tipo) {
         case 'string':
            $valor_limpio = Comun::limitar_cadena($valor, is_bool($parametro) ? 255 : $parametro);
            break;

         case 'int':
            $valor_limpio = intval($valor);
            break;

         case 'boolean':
            $valor_limpio = $valor ? true : false;
            break;

         case 'si/no':
            $valor_limpio = self::evaluaValorParaSiNo($valor, $parametro);
            break;

         case 'match':
            // se hace una "doble" validacion para revisar que el valor equivalente sea aceptado por el web service
            $valor_limpio = self::validar_nmp(self::obtener_equivalencia($valor, $parametro[0]), $parametro[1], $parametro[2], $puede_ser_null);
            break;
      }

      if (!is_null($valor_limpio) and is_string($valor_limpio) and empty($valor_limpio)) {
         $valor_limpio = '     ';
      }

      return $valor_limpio;
   }
}