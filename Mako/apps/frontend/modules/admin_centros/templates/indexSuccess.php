<?php use_javascript('plugins/jquery.dataTables.js') ?>
<?php use_stylesheet('datatables.css') ?>

<script>

var lTcentros;
$(document).ready(function() {

   lTcentros = $('#lista-centros').dataTable({
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": true,
        "bJQueryUI": true,
        "sPaginationType": "full_numbers",
        "aaSorting": [[1, 'asc']] 
     });
   
});

</script>

<div class="ui-widget-header ui-corner-top tsss" style="padding: 5px;">Administrar
	centros</div>
<div class="ui-widget ui-corner-bottom sombra">
	<div class="ui-widget-content ui-corner-bottom" style="padding: 5px;">

		<?php if ($sf_user->hasFlash('ok')): ?>
		<div
			class="flash_ok ui-state-highlight ts sombra-delgada ui-corner-all"
			style="padding: 5px; margin-top: 10px; margin-bottom: 10px;">
			<span style="float: left; margin-right: 0.3em;"
				class="ui-icon ui-icon-info"></span> <strong><?php echo $sf_user->getFlash('ok') ?>
			</strong>
		</div>
		<?php endif; ?>

		<br /> <span style="float: left;" class="ui-icon ui-icon-circle-plus"></span>
		<a href="<?php echo url_for('admin_centros/new') ?>"> Agregar registro
		</a> <br /> <br />


		<table id="lista-centros" style="width: 100%;">
			<thead>
				<tr>
					<th><span class="ui-icon  ui-icon-pencil"></span>
					</th>
					<th>Servidor</th>
					<th>Nombre</th>
					<th>Alias</th>
					<th>Unificado</th>
					<th>Municipio</th>
					<th>Colonia</th>
					<th>RFC</th>
					<th>Activo</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($centros as $centro): ?>
				<tr>
					<td><a
						href="<?php echo url_for('admin_centros/edit?id='.$centro->getId()) ?>">
							<span class="ui-icon  ui-icon-pencil"></span>
					</a>
					</td>
					<td><?php echo $centro->getIp() ?></td>
					<td><?php echo $centro->getNombre() ?></td>
					<td><?php echo $centro->getAlias() ?></td>
					<td><?php echo $centro->getAliasReporte() ?></td>
					<td><?php echo $centro->getMunicipio() ?></td>
					<td><?php echo $centro->getColonia() ?></td>
					<td><?php echo $centro->getRfc() ?></td>
					<td><?php echo $centro->getActivo() ?></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>


	</div>
</div>

