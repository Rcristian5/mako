<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<?php use_javascript('plugins/jquery.comboMakoCascade.js') ?>
<?php use_javascript('admin_centros/cascadaDirecciones.js') ?>


<div class="ui-widget ui-corner-all sombra">
	<div class="ui-widget-header ui-corner-top tsss" style="padding: 5px;">Administrar
		centros</div>
	<div class="ui-widget-content ui-corner-bottom">

		<div style="margin: 15px;">


			<form
				action="<?php echo url_for('admin_centros/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>"
				method="post"
				<?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
				<?php if (!$form->getObject()->isNew()): ?>
				<input type="hidden" name="sf_method" value="put" />
				<?php endif; ?>
				<table>
					<tfoot>
						<tr>
							<td colspan="2">&nbsp;<a
								href="<?php echo url_for('admin_centros/index') ?>">Regresar al
									índice</a> <?php if (!$form->getObject()->isNew()): ?> &nbsp;<?php echo link_to('Borrar registro', 'admin_centros/delete?id='.$form->getObject()->getId(), array('method' => 'delete', 'confirm' => '¿Está seguro? Esta acción no se puede deshacer.')) ?>
								<?php endif; ?> <input type="submit" value="Guardar registro"
								onclick="return requeridos();" /> <input type="button"
								value="Cancelar"
								onclick="location.href='<?php echo url_for('admin_centros/new') ?>'" />
							</td>
						</tr>
					</tfoot>
					<tbody>
						<?php echo $form ?>
					</tbody>
				</table>
			</form>

			<?php
			/****
			 *
			* ******************************************************************************************************
			* Ponemos en variables ocultas los datos por defautl para el caso de la direccion.						*
			* Reingenieria Mako																					*
			* silvio.bravo@enova.mx																				*
			* 10-Marzo-2014																						*
			* ******************************************************************************************************
			*
			*/


			$estado			= sfConfig::get('app_registro_estado_id');
			$municipio		= 0;
			$colonia		= 0;
			$datosDireccion	= $datosDireccion->getRawValue();


			if(isset($datosDireccion)){
				if (array_key_exists("entidadFederativa",$datosDireccion)){

					$estado		=$datosDireccion["entidadFederativa"];
				}
				if (array_key_exists("municipio",$datosDireccion)){
					$municipio	=$datosDireccion["municipio"];
				}
				if (array_key_exists("colonia",$datosDireccion)){
					$colonia	=$datosDireccion["colonia"];
				}

			}
			?>
			<input type="hidden" name="idEstadoVar" id="idEstadoVar"
				" 	value="<?php echo $estado;   	?>" /> <input type="hidden"
				name="idMunicipioVar" id="idMunicipioVar"
				value="<?php echo $municipio; 	?>" /> <input type="hidden"
				name="idColoniaVar" id="idColoniaVar"
				value="<?php echo $colonia; 	?>" />

		</div>
	</div>
</div>
