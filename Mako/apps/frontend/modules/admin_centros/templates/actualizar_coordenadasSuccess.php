<?php use_javascript('jquery.ui.js') ?>

<?php use_stylesheet('./admin_centros/actualizar_coordenadasSuccess.css')?>
<script
	type="text/javascript"
	src="http://maps.google.com/maps/api/js?sensor=false"></script>

<div id="content"
	class="ui-widget ui-widget-content ui-corner-all sombra">
	<div class="ui-widget ui-widget-header tssss" style="padding: 5px;">Actualizar
		coordenadas centro</div>
	<div id="update_coordinates"></div>
	<div id="update_form">
		<h2>Centros RIA</h2>

		<div class="flash_notice ui-state-highlight ui-corner-all"
			style="display: none;"></div>
		<p>
			<label for="centro">Centro</label> <select id="centro">
				<option value="0">--Selecciona---</option>
				<?php foreach ($centros as $centro):?>
				<option value="<?php echo $centro->getId() ?>">
					<?php echo $centro->getNombre() ?>
				</option>
				<?php endforeach;?>
			</select>
		</p>
		<p>
			<label for="lat">Latitud</label> <input type="text" id="lat"
				disabled="disabled" />
		</p>
		<p>
			<label for="lng">Longitud</label> <input type="text" id="lng"
				disabled="disabled" />
		</p>
		<p>
			<label for="new-lat">Nueva Latitud</label> <input type="text"
				id="new-lat" filtro="numerico" maxlength="18" />
		</p>
		<p>
			<label for="new-lng">Nueva Longitud</label> <input type="text"
				id="new-lng" filtro="numerico" maxlength="18" />
		</p>
		<p class="last">
			<input type="button" value="Actualizar Coordenadas"
				id="update-coordinates" />
		</p>
	</div>
</div>

<script type="text/javascript">
var enova = new google.maps.LatLng(19.418202534570867, -99.15938694694523);
var marker;
var map;

$(document).ready(function(){
    initialize();
    $("#new-lat, #new-lng").live('paste', function(e) {
        var lat = $('#new-lat');
        var lng = $('#new-lng');
        setTimeout( function(){pasteEvent(lat,lng);}, 1);
        
    	
    });
    $('#new-lat, #new-lng').keyup(function(){
    	map.setCenter(new google.maps.LatLng($('#new-lat').val(), $('#new-lng').val()));
        marker.setPosition(new google.maps.LatLng($('#new-lat').val(), $('#new-lng').val()));
    });
    $('#centro').change(function (){
    var centro =$(this).val(); 
    if( centro != 0){
    	$.ajax({
            url : '<?php echo url_for('admin_centros/getCoordenadas')?>',
            data : {centro : centro},
            dataType : 'json',
            beforeSend : function(){
              $('#centro, #new-lng, #new-lat, #update-coordinates').attr('disabled', true);
            },
            success: function(data) {
            	$('#centro, #new-lng, #new-lat, #update-coordinates').attr('disabled', false);
            	$('#lat').val(data.lat);
            	$('#lng').val(data.lng);
            	$('#new-lat, #new-lng').val('');
            	map.setCenter(new google.maps.LatLng(data.lat, data.lng));
                marker.setPosition(new google.maps.LatLng(data.lat, data.lng));
            },
            error: function(data){
              console.error(data);
            }
          });
    }
    });
    $('#update-coordinates').click(function (){
        var centro = $('#centro').val(),
        	lat = $('#new-lat').val(),
        	lng = $('#new-lng').val();
         
        if( centro != 0){
            if(lat != '' && lng != '')
            {
	        	$.ajax({
	                url : '<?php echo url_for('admin_centros/updateCoordenadas')?>',
	                dataType : 'json',
	                data : {centro : centro, lat : lat, lng : lng},
	                beforeSend : function(){
	                	$('#centro, #new-lng, #new-lat, #update-coordinates').attr('disabled', true);
	                },
	                success: function(data) {
		                if(data.resultado)
		                {
		                	$('.flash_notice').html('<span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-info"></span><strong>Coordenadas actualizadas</strong>');
		                	$('.flash_notice').show();
		                	setTimeout("$('.flash_notice').hide()", 5000);  
		                	$('#centro, #new-lng, #new-lat, #update-coordinates').attr('disabled', false);
	                		$('#lat, #lng, #new-lat, #new-lng').val('');
	                		$('#centro').val(0);
		                }
		                else
		                {
		                	alert('Ocurrio un error al actulizar los datos del centro, intente nuevamente');
		                }
	                },
	                error: function(data){
	                  console.error(data);
	                }
	              });
            }
            else
            {
                alert('Debes ingresar una nueva latitud y longitud');
            }
        }
        else
        {
            alert('Selecciona un centro y actualiza sus coordenadas');
        }
    });
        
});
function pasteEvent(lat, lng){
	lat = lat.val().replace("º", "");
	lng = lng.val().replace("º", "");
	$('#new-lat').val(lat);
	$('#new-lng').val(lng);
	map.setCenter(new google.maps.LatLng(lat, lng));
    marker.setPosition(new google.maps.LatLng(lat, lng));
}
function initialize() {
  var mapOptions = {
    zoom: 16,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    center: enova
  };

  map = new google.maps.Map(document.getElementById("update_coordinates"),
          mapOptions);
        
  marker = new google.maps.Marker({
    map:map,
    draggable:true,
    animation: google.maps.Animation.DROP,
    position: enova,
  });
  google.maps.event.addListener(marker, 'click', toggleBounce);
  google.maps.event.addListener(marker, 'dragend', getCoordinates);
  google.maps.event.addListener(map, 'click', function(event){
	  getClick(event.latLng);
  });
}

function toggleBounce() {

  if (marker.getAnimation() != null) {
    marker.setAnimation(null);
  } else {
    marker.setAnimation(google.maps.Animation.BOUNCE);
  }
}
function getClick(location){
	 marker.setPosition(location);
	 getCoordinates();
}
function getCoordinates() {
	$('#new-lat').val(marker.getPosition().lat());
	$('#new-lng').val(marker.getPosition().lng());
}
</script>
