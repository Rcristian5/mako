<?php

/**
 * admin_centros actions.
 *
 * @package    mako
 * @subpackage admin_centros
 * @author     David Galindo Garcia
 * @version    SVN: $Id: actions.class.php,v 1.2 2011-11-04 18:48:37 david Exp $
 */
class admin_centrosActions extends sfActions
{
	public function executeIndex(sfWebRequest $request)
	{
		$this->centros = CentroPeer::doSelect(new Criteria());
	}

	public function executeNew(sfWebRequest $request)
	{
		$this->datosDireccion	=NM01ColoniaPeer::getIdsDireccionByColoniaId(0);
		$this->form = new centroForm();
	}

	public function executeCreate(sfWebRequest $request)
	{
		$this->forward404Unless($request->isMethod(sfRequest::POST));

		$this->form = new centroForm();

		$this->processForm($request, $this->form);

		$this->setTemplate('new');
	}

	public function executeEdit(sfWebRequest $request)
	{
		$this->forward404Unless($centro = CentroPeer::retrieveByPk($request->getParameter('id')), sprintf('Object centro does not exist (%s).', $request->getParameter('id')));
		$this->form 			= new centroForm($centro);
		$this->datosDireccion	=NM01ColoniaPeer::getIdsDireccionByColoniaId(intval($centro->getIdColonia()));

	}

	public function executeUpdate(sfWebRequest $request)
	{

		 
		$this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
		$this->forward404Unless($centro = CentroPeer::retrieveByPk($request->getParameter('id')), sprintf('Object centro does not exist (%s).', $request->getParameter('id')));
		$this->form 			= new centroForm($centro);
		$this->datosDireccion	=NM01ColoniaPeer::getIdsDireccionByColoniaId(intval($request->getPostParameter('centro[id_colonia]')));

		$this->processForm($request, $this->form);
		$this->setTemplate('edit');
	}

	public function executeDelete(sfWebRequest $request)
	{
		$request->checkCSRFProtection();

		$this->forward404Unless($centro = CentroPeer::retrieveByPk($request->getParameter('id')), sprintf('Object centro does not exist (%s).', $request->getParameter('id')));
		$centro->delete();

		$this->redirect('admin_centros/index');
	}

	protected function processForm(sfWebRequest $request, sfForm $form)
	{
		 
		$form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));


		if ($form->isValid())
		{
			$centro = $form->save();


	  $this->getUser()->setFlash('ok', sprintf('Se ha guardado correctamente el registro: %s',$form->getObject()));



	  $this->redirect('admin_centros/index?id='.$centro->getId());
		}
	}
	/**
	 * Accion para mostrar el reporte de socios
	 * @param sfWebRequest $request
	 */
	public function executeActualizar_coordenadas(sfWebRequest $request){
		$criteria = new Criteria();
		$criteria->addAscendingOrderByColumn(CentroPeer::NOMBRE);
		$this->centros = CentroPeer::doSelect($criteria);
	}
	/**
	 * Accion para obtener las coordenadas del centro
	 * @param sfWebRequest $request
	 */
	public function executeGetCoordenadas(sfWebRequest $request)
	{
		if ($request->isXmlHttpRequest())
		{
			$this->forward404Unless($centro = CentroPeer::retrieveByPk($request->getParameter('centro')), sprintf('El centro no existe (%s).', $request->getParameter('centro')));

			$arreglo = array('lat'=>$centro->getLat(), 'lng'=> $centro->getLong());
			$this->getResponse()->setHttpHeader('Content-Type', 'application/json; charset=utf-8');

			return $this->renderText(json_encode($arreglo));
		}

	}
	/**
	 * Accion para obtener las coordenadas del centro
	 * @param sfWebRequest $request
	 */
	public function executeUpdateCoordenadas(sfWebRequest $request)
	{
		if ($request->isXmlHttpRequest())
		{
			$this->forward404Unless($centro = CentroPeer::retrieveByPk($request->getParameter('centro')), sprintf('El centro no existe (%s).', $request->getParameter('centro')));
			try {
				$centro->setLat($request->getParameter('lat'));
				$centro->setLong($request->getParameter('lng'));
				$centro->save();
				$arreglo = array('resultado'=> 1);
					
					
			}
			catch (ErrorException $error)
			{
				$arreglo = array('resultado'=> 0);
			}
			$this->getResponse()->setHttpHeader('Content-Type', 'application/json; charset=utf-8');

			return $this->renderText(json_encode($arreglo));
		}

	}
}
