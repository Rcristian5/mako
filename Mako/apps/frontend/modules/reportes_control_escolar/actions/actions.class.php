<?php

/**
 * reportes_control_escolar actions.
 *
 * @package    mako
 * @subpackage reportes_control_escolar
 * @author     Bixit SA de CV
 * @version    SVN: $Id: actions.class.php,v 1.2 2011-11-24 18:59:56 david Exp $
 */
class reportes_control_escolarActions extends sfActions
{
	/**
	 * Accion para mostrar el reporte mensual de los cocios inscritos a un curso
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeSocios_curso(sfWebRequest $request)
	{
		$centro = $request->getParameter('centro');
		$desde = $request->getParameter('desde');
		$hasta = $request->getParameter('hasta');
		$reporte = false;
		if($centro && $desde && $hasta)
		{
			$reporte = true;
			$this->forward404Unless($centro = CentroPeer::retrieveByPk($centro), sprintf('El centro no existe (%s).', $centro));
			$this->centroSel = $centro;
			$desdeSel = explode('-', $desde);
			$hastaSel = explode('-', $hasta);
			$this->desde = mktime(0, 0, 0, $desdeSel[1], $desdeSel[2], $desdeSel[0]);
			$this->hasta = mktime(0, 0, 0, $hastaSel[1], $hastaSel[2], $hastaSel[0]);
		}
		$this->centros = CentroPeer::listaCentrosActivosSelect();
		$this->reporte = $reporte;
	}
	/**
	 * Accion para mostrar la lista de socios inscritos por grupo a un centro
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeConsulta_inscritos(sfWebRequest $request)
	{
		$centro = $request->getParameter('centro');
		$curso = $request->getParameter('curso');
		$desde = $request->getParameter('desde');
		$hasta = $request->getParameter('hasta');
		$reporte = false;
		if($centro && $curso && $desde && $hasta)
		{
			$reporte = true;
			$this->forward404Unless($centro = CentroPeer::retrieveByPk($centro), sprintf('El centro no existe (%s).', $centro));
			$this->forward404Unless($curso = CursoPeer::retrieveByPk($curso), sprintf('El curso no existe (%s).', $curso));
			$this->centroSel = $centro;
			$this->cursoSel = $curso;
			$desdeSel = explode('-', $desde);
			$hastaSel = explode('-', $hasta);
			$this->desde = mktime(0, 0, 0, $desdeSel[1], $desdeSel[2], $desdeSel[0]);
			$this->hasta = mktime(0, 0, 0, $hastaSel[1], $hastaSel[2], $hastaSel[0]);
		}
		$this->centros = CentroPeer::listaCentrosActivosSelect();
		$this->cursos = CursoPeer::listaCursosActivosSelect();
		$this->reporte = $reporte;
	}
	/**
	 * Accion para mostrar el detalle de asistencias
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeAsistencias(sfWebRequest $request)
	{
		$centro = $request->getParameter('centro');
		$curso = $request->getParameter('curso');
		$desde = $request->getParameter('desde');
		$hasta = $request->getParameter('hasta');
		$reporte = false;
		if($centro && $curso && $desde && $hasta)
		{
			$reporte = true;
			$this->forward404Unless($centro = CentroPeer::retrieveByPk($centro), sprintf('El centro no existe (%s).', $centro));
			$this->forward404Unless($curso = CursoPeer::retrieveByPk($curso), sprintf('El curso no existe (%s).', $curso));
			$this->centroSel = $centro;
			$this->cursoSel = $curso;
			$desdeSel = explode('-', $desde);
			$hastaSel = explode('-', $hasta);
			$this->desde = mktime(0, 0, 0, $desdeSel[1], $desdeSel[2], $desdeSel[0]);
			$this->hasta = mktime(0, 0, 0, $hastaSel[1], $hastaSel[2], $hastaSel[0]);
		}
		$this->centros = CentroPeer::listaCentrosActivosSelect();
		$this->cursos = CursoPeer::listaCursosActivosSelect(true);
		$this->reporte = $reporte;
	}
	/**
	 * Accion para mostrar el detalle de calificaciones
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeCalificaciones(sfWebRequest $request)
	{
		$centro = $request->getParameter('centro');
		$curso = $request->getParameter('curso');
		$desde = $request->getParameter('desde');
		$hasta = $request->getParameter('hasta');
		$reporte = false;
		if($centro && $curso && $desde && $hasta)
		{
			$reporte = true;
			$this->forward404Unless($centro = CentroPeer::retrieveByPk($centro), sprintf('El centro no existe (%s).', $centro));
			$this->forward404Unless($curso = CursoPeer::retrieveByPk($curso), sprintf('El curso no existe (%s).', $curso));
			$this->centroSel = $centro;
			$this->cursoSel = $curso;
			$desdeSel = explode('-', $desde);
			$hastaSel = explode('-', $hasta);
			$this->desde = mktime(0, 0, 0, $desdeSel[1], $desdeSel[2], $desdeSel[0]);
			$this->hasta = mktime(0, 0, 0, $hastaSel[1], $hastaSel[2], $hastaSel[0]);
		}
		$this->centros = CentroPeer::listaCentrosActivosSelect();
		$this->cursos = CursoPeer::listaCursosActivosSelect(true);
		$this->reporte = $reporte;
	}
	/**
	 * Accion para la busqueda del historial academico de los socios
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeHistorial_academico(sfWebRequest $request)
	{
		$centro = $request->getParameter('centro');
		$folio  = $request->getParameter('folio');
		$usuario  = $request->getParameter('usuario');
		$nombre  = $request->getParameter('nombre');
		$busqueda = null;
		if($nombre)
		{
			$busqueda = $nombre;
			$campo = 'nombre';
		}
		if($usuario)
		{
			$busqueda = $usuario;
			$campo = 'usuario';
		}
		if($folio)
		{
			$busqueda = $folio;
			$campo = 'folio';
		}
		if($centro && $busqueda)
		{
			$reporte = true;
			$this->forward404Unless($centro = CentroPeer::retrieveByPk($centro), sprintf('El centro no existe (%s).', $centro));
			$this->centroSel = $centro;
			$this->busqueda = $busqueda;
			$this->campo = $campo;
		}
		$this->centros = CentroPeer::listaCentrosActivosSelect();
	}
	/**
	 * Acción para mostrar la vista del reporte
	 * @param sfRequest $request A request object
	 */
	public function executeFormatearReporte(sfWebRequest $request)
	{
		$this->forward404unless($request->isXmlHttpRequest());
		$datos = $request->getParameter('data');
		$tipo = $request->getParameter('tipo');
		$centro = $request->getParameter('centro');
		switch ($tipo)
		{
			case 'reporteInscritos':
				$desde = $request->getParameter('desde');
				$hasta = $request->getParameter('hasta');
				$periodo = $request->getParameter('periodo');

				return $this->renderPartial('vistaReporteCurso', array('datos' => $datos, 'centro' => $centro, 'desde' => $desde, 'hasta' => $hasta, 'periodo' => $periodo));
				break;
			case 'reporteAsistencias':
				$curso = $request->getParameter('curso');
				$desde = $request->getParameter('desde');
				$hasta = $request->getParameter('hasta');
				$periodo = $request->getParameter('periodo');

				return $this->renderPartial('vistaReporteAsistencias', array('datos' => $datos, 'centro' => $centro, 'desde' => $desde, 'hasta' => $hasta, 'periodo' => $periodo));
				break;
			case 'reporteCalificaciones':
				$curso = $request->getParameter('curso');
				$desde = $request->getParameter('desde');
				$hasta = $request->getParameter('hasta');
				$periodo = $request->getParameter('periodo');
				return $this->renderPartial('vistaReporteCalificaciones', array('datos' => $datos, 'centro' => $centro, 'desde' => $desde, 'hasta' => $hasta, 'periodo' => $periodo));
				break;
			case 'consultaInscritos':
				$curso = $request->getParameter('curso');

				return $this->renderPartial('vistaConsultaGrupo', array('datos' => $datos));
				break;
			case 'busquedaHistorial':
				$this->forward404Unless($centro = CentroPeer::retrieveByPk($centro), sprintf('El centro no existe (%s).', $centro));

				return $this->renderPartial('vistaHistorialBusqueda', array('datos' => $datos, 'centro' => $centro));
				break;
			case 'historialAcademico':
					
				return $this->renderPartial('vistaHistorialAcademico', array('datos' => $datos, 'json' => json_encode($datos['grupos'])));
				break;
			default:
				$this->forward404('La página solicitado no existe');
				break;
		}
	}
	/**
	 * Acción para generar el reporte de socios incritos a un curso de un centro
	 * en un mes dado
	 * @param sfRequest $request A request object
	 */
	public function executeReporte_curso(sfWebRequest $request)
	{
		$centro = $request->getParameter('centro');
		$desde = $request->getParameter('desde');
		$hasta = $request->getParameter('hasta');
		if($centro && $desde && $hasta)
		{
			$this->forward404Unless($centro = CentroPeer::retrieveByPk($centro), sprintf('El centro no existe (%s).', $centro));
			$this->getResponse()->setHttpHeader('Access-Control-Allow-Origin', '*');
			$this->getResponse()->setContentType('application/json');


			return $this->renderText($request->getParameter('method').'('.json_encode(GrupoPeer::sociosInscritosCurso($centro->getId(), $desde, $hasta)).')');
		}
		$this->forward404('La página solicitado no existe');
	}
	/**
	 * Acción para generar el reporte de socios incritos a los grupos de un centro
	 * en un mes dado
	 * @param sfRequest $request A request object
	 */
	public function executeReporte_inscritos(sfWebRequest $request)
	{
		$centro = $request->getParameter('centro');
		$curso = $request->getParameter('curso');
		$desde = $request->getParameter('desde');
		$hasta = $request->getParameter('hasta');
		if($centro && $curso && $desde && $hasta)
		{
			$this->forward404Unless($centro = CentroPeer::retrieveByPk($centro), sprintf('El centro no existe (%s).', $centro));
			$this->forward404Unless($curso = CursoPeer::retrieveByPk($curso), sprintf('El curso no existe (%s).', $curso));
			$this->getResponse()->setHttpHeader('Access-Control-Allow-Origin', '*');
			$this->getResponse()->setContentType('application/json');


			return $this->renderText($request->getParameter('method').'('.json_encode(GrupoPeer::consultaSociosInscritosCurso($centro->getId(), $desde, $hasta, $curso->getId())).')');
		}
		$this->forward404('La página solicitado no existe');
	}
	/**
	 * Accion para generar el reporte de asistencias
	 * en un mes dado
	 * @param sfRequest $request A request object
	 */
	public function executeReporte_asistencias(sfWebRequest $request)
	{
		$centro = $request->getParameter('centro');
		$desde = $request->getParameter('desde');
		$hasta = $request->getParameter('hasta');
		$curso = $request->getParameter('curso');
		if($centro && $desde && $hasta && $curso)
		{
			$this->forward404Unless($centro = CentroPeer::retrieveByPk($centro), sprintf('El centro no existe (%s).', $centro));
			$this->forward404Unless($curso = CursoPeer::retrieveByPk($curso), sprintf('El curso no existe (%s).', $curso));
			$this->getResponse()->setHttpHeader('Access-Control-Allow-Origin', '*');
			$this->getResponse()->setContentType('application/json');


			return $this->renderText($request->getParameter('method').'('.json_encode(GrupoPeer::reporteAsistencias($centro->getId(), $desde, $hasta, $curso->getId())).')');
		}
		$this->forward404('La página solicitado no existe');
	}
	/**
	 * Accion para generar el reporte de calificaciones
	 * en un mes dado
	 * @param sfRequest $request A request object
	 */
	public function executeReporte_calificaciones(sfWebRequest $request)
	{
		$centro = $request->getParameter('centro');
		$desde = $request->getParameter('desde');
		$hasta = $request->getParameter('hasta');
		$curso = $request->getParameter('curso');
		if($centro && $desde && $hasta && $curso)
		{
			$this->forward404Unless($centro = CentroPeer::retrieveByPk($centro), sprintf('El centro no existe (%s).', $centro));
			$this->forward404Unless($curso = CursoPeer::retrieveByPk($curso), sprintf('El curso no existe (%s).', $curso));
			$this->getResponse()->setHttpHeader('Access-Control-Allow-Origin', '*');
			$this->getResponse()->setContentType('application/json');


			return $this->renderText($request->getParameter('method').'('.json_encode(GrupoPeer::reporteCalificaciones($centro->getId(), $desde, $hasta, $curso->getId())).')');
		}
		$this->forward404('La página solicitado no existe');
	}
	/**
	 * Accion para mostrar la busqueda de usuarios
	 * @param sfRequest $request A request object
	 */
	public function executeBuscar_socio(sfWebRequest $request)
	{
		$busqueda = $request->getParameter('busqueda');
		$campo = $request->getParameter('campo');
		if($busqueda && $campo)
		{
			$this->getResponse()->setHttpHeader('Access-Control-Allow-Origin', '*');
			$this->getResponse()->setContentType('application/json');
				
			return $this->renderText($request->getParameter('method').'('.json_encode(SocioPeer::listaSociosJsonp($busqueda, $campo)).')');
		}
		$this->forward404('La página solicitado no existe');
	}
	/**
	 * Accion para devolver el historial academico de un socio
	 * @param sfRequest $request A request object
	 */
	public function executeDetalle_historial(sfWebRequest $request)
	{
		$this->forward404Unless($socio = SocioPeer::retrieveByPk($request->getParameter('socio')), sprintf('El socio no existe (%s).', $request->getParameter('socio')));
		$this->getResponse()->setHttpHeader('Access-Control-Allow-Origin', '*');
		$this->getResponse()->setContentType('application/json');

		return $this->renderText($request->getParameter('method').'('.json_encode(GrupoAlumnosPeer::historialAcademico($socio->getId())).')');
	}
	/**
	 * Accion para crea la constancia de estudios
	 * @param sfRequest $request A request object
	 */
	public function executeCrear_constancia(sfWebRequest $request)
	{
		$arreglo = array();

		$config = sfTCPDFPluginConfigHandler::loadConfig();

		$pdf = new sfTCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Mako');
		$pdf->SetTitle('Constancia de Estudios');
		$pdf->SetSubject('Constancia de Estudios');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$pdf->SetFont('helvetica', '', 11, '', true);
		$pdf->AddPage();
		$pdf->writeHTML($this->getPartial('constancia_estudios', array(
				'socio' => $request->getParameter('user'),
				'folio' => $request->getParameter('folio'),
				'grupo' => $request->getParameter('data'),
		)), true, false, false, false, '');

		$arreglo['file'] = 'constancia_'.$request->getParameter('folio').'_'.date('U').'.pdf';

		$pdf->Output('/tmp/'.$arreglo['file'], 'F');

		return $this->renderText(json_encode($arreglo));

		throw new sfStopException();

	}
	/**
	 * Accion para crea la constancia de estudios
	 * @param sfRequest $request A request object
	 */
	public function executeCrear_detalle_historial(sfWebRequest $request)
	{
		$arreglo = array();

		$config = sfTCPDFPluginConfigHandler::loadConfig();

		$pdf = new sfTCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Mako');
		$pdf->SetTitle('Detalle de historial académico');
		$pdf->SetSubject('Detalle de historial académico');
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$pdf->SetFont('helvetica', '', 11, '', true);
		$pdf->AddPage();
		$pdf->writeHTML($this->getPartial('detalle_historial', array(
				'socio' => $request->getParameter('user'),
				'grupo' => $request->getParameter('data'),
		)), true, false, false, false, '');

		$arreglo['file'] = 'detale_historial'.$request->getParameter('folio').'_'.date('U').'.pdf';

		$pdf->Output('/tmp/'.$arreglo['file'], 'F');

		return $this->renderText(json_encode($arreglo));

		throw new sfStopException();

	}
	/**
	 * Accion para descargar un archivo
	 * @param sfRequest $request A request object
	 */
	public function executeDownload_file(sfWebRequest $request)
	{
		$filename = $request->getParameter('file');
		if(!$filename)
		{
			$this->forward404('El archivo solicitado no existe');
		}

		$response = $this->getContext()->getResponse();
		$response->setHttpHeader('Content-Transfer-Encoding', 'binary', TRUE);
		$response->setHttpHeader('Content-Disposition','attachment; filename='.$filename, TRUE);
		$response->setHttpHeader('Content-type', 'application/pdf', TRUE);
		$response->sendHttpHeaders();
		readfile('/tmp/'.$filename);

		return sfView::NONE;
	}
}
