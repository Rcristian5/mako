<?php use_helper('Date') ?>

<h1 style="text-align: center;">CONSTANCIA DE ESTUDIOS DE INSCRIPCIÓN</h1>

<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p style="margin-top: 10em">La Red de Innovación y Aprendizaje hace
	constar que</p>
<table width="100%" border="1">
	<tr>
		<th width="30%">&nbsp;<br> &nbsp;&nbsp;NOMBRE

		</th>
		<td style="padding: 1%;">&nbsp;<br> &nbsp;&nbsp;<b><?php echo $socio ?>
		</b> <br>
		</td>
	</tr>
	<tr>
		<th style="padding: 20px;">&nbsp;<br> &nbsp;&nbsp;FOLIO <br>
		</th>
		<td style="padding: 20em;">&nbsp;<br> &nbsp;&nbsp;<b><?php echo $folio ?>
		</b> <br>
		</td>
	</tr>
</table>
<p>&nbsp;</p>
<p>
	es estudiante regular activo del centro
	<?php echo $grupo['centro']?>
	y se encuentra inscrito al siguiente curso.
</p>

<table width="100%" border="1">
	<tr>
		<th>&nbsp;<br> &nbsp;&nbsp;CLAVE <br>
		</th>
		<th>&nbsp;<br> &nbsp;&nbsp;CURSO <br>
		</th>
		<th>&nbsp;<br> &nbsp;&nbsp;PERIODO <br>
		</th>
		<th>&nbsp;<br> &nbsp;&nbsp;CALIFICAIÓN <br>
		</th>
	</tr>
	<tr>
		<td>&nbsp;<br> &nbsp;&nbsp;<b><?php echo $grupo['cursoClave']?> </b> <br>
		</td>
		<td>&nbsp;<br> &nbsp;&nbsp;<b><?php echo $grupo['curso']?> </b> <br>
		</td>
		<td>&nbsp;<br> &nbsp;&nbsp;<b><?php echo $grupo['periodo']?> </b> <br>
		</td>
		<td>&nbsp;<br> &nbsp;&nbsp;<b><?php echo $grupo['calificacion']?> </b>
			<br>
		</td>
	</tr>
</table>

<p>&nbsp;</p>
<p>
	Constancia que se expide en el Estado de <b>México</b> a los
	<?php echo date('d')?>
	días del mes de
	<?php echo ucfirst(format_date(date('m')-1, 'MMMM', 'es_MX')) ?>
	de
	<?php echo date('Y')?>
	.
</p>

<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p style="text-align: center;">
<hr size="2" width="30%">
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;FIRMA Y SELLO
</p>
