<div>
	<h1 class="ui-widget-header ui-corner-all"
		style="color: #ffffff !important">
		<?php echo $datos['socio'] ?>
	</h1>
	<table width="100%" id="cursos-socio">
		<thead>
			<tr>
				<th>Curso</th>
				<th>Grupo</th>
				<th>Centro</th>
				<th>Facilitador</th>
				<th>Estado</th>
				<th>Calificación final</th>
				<th>Asistencias(%)</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($datos['grupos'] as $key => $grupo): ?>
			<tr>
				<td><?php echo $grupo['curso'] ?>
				
				<td><a href="#" grupo="<?php echo $key ?>" class="trigger-historial"><?php echo $grupo['grupo'] ?>
				</a>
				
				<td><?php echo $grupo['centro'] ?>
				
				<td><?php echo $grupo['facilitador'] ?>
				
				<td><?php echo $grupo['estado'] ?>
				
				<td><?php echo $grupo['calificacion'] ?>
				
				<td><?php echo round($grupo['porAsistencia'])?>
			
			</tr>
			<?php endforeach;?>
		</tbody>
	</table>
	<div id="detalle-historial">
		<?php foreach ($datos['grupos'] as $key => $grupo): ?>
		<div id="<?php echo $key ?>" class="content-historial">
			<h2 class="ui-state-active ui-corner-all"
				style="color: #ffffff !important">
				<?php echo $grupo['curso'] ?>
				, Grupo
				<?php echo $grupo['grupo']?>
			</h2>
			<div class="detalle-lista-historial">

				<table width="100%" class="dataTables_wrapper">
					<tr>
						<th class="ui-widget ui-widget-header">D&iacute;a</th>
						<th class="ui-widget ui-widget-header">Asistio</th>
					</tr>
					<?php $class = 'even'?>
					<?php foreach ($grupo['listaAsistencia'] as $lista): ?>
					<tr>
						<td class="<?php echo $class ?>"><?php echo $lista['fecha'].' '.$lista['horaInicio'].' - '.$lista['horaFin'] ?>
						</td>
						<td class="<?php echo $class ?>"><?php echo $lista['asistencia'] ?>
						</td>
						<?php $class = ($class == 'even') ? 'odd' : 'even' ?>
					</tr>
					<?php endforeach;?>
				</table>

			</div>
			<div class="detalle-historial-grupo">

				<h3 class="ui-state-highlight ui-corner-all">
					Calificaci&oacute;n final:
					<?php echo $grupo['calificacion'] ?>
				</h3>
				<h3 class="ui-state-highlight ui-corner-all">
					Porcentaje de asistencias:
					<?php echo round($grupo['porAsistencia']) ?>
					%
				</h3>
				<p>
					<input type="button" class="download-detail"
						value="Descargar detalle (PDF)" grupo="<?php echo $key ?>" />
					<?php if ($grupo['conCalificacion'] == 1):?>
					<input type="button" class="download-constancia"
						value="Descargar constancia de estudios (PDF)"
						grupo="<?php echo $key ?>" />
					<?php endif; ?>
				</p>

			</div>
		</div>
		<p class="clear"></p>
		<?php endforeach;?>
	</div>
	<div id="load-document">
		Creando el documento, espere...<br> <img src="/imgs/ajax-loader.gif"
			alt="..." />
	</div>
</div>
<script>
var datos = <?php echo html_entity_decode($json) ?>;
$(function() {
	$('.trigger-historial').click(function(){
		var grupo = $(this).attr('grupo');
		$('.content-historial').hide();
		$('#'+grupo).show();
		
	 	return false;
	});
	$('.download-constancia').click(function(){
		var grupo = $(this).attr('grupo');
		
		createFile('<?php echo url_for('reportes_control_escolar/crear_constancia')?>', datos[grupo], $(this))
	});
	$('.download-detail').click(function(){
		var grupo = $(this).attr('grupo');
		
		createFile('<?php echo url_for('reportes_control_escolar/crear_detalle_historial')?>', datos[grupo], $(this))
	});
	$('.download-detail').click(function(){
		var grupo = $(this).attr('grupo');
		
		createFile($('#load-constancia'), '<?php echo url_for('reportes_control_escolar/crear_constancia')?>', datos[grupo])
	});
	GrupoTable = $('#cursos-socio').dataTable({
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",
		"sDom": '<T><"clear"><"H"lfr>t<"F"ip>',
		"oTableTools": {
			"sSwfPath": "/js/plugins/dataTables/swf/DataTables.swf",
			"aButtons": [
			 			"copy",
			 			"csv",
			 			{
			 				"sExtends": "xls",
			 				"sFieldSeperator": "|",
			 				"sCharSet" : "utf8",
			 				"sFileName" : "Detalle Grupos-Facilirtadores.csv"
				 		},
			 			{
							"sExtends": "pdf",
							"sPdfTextSize": 6,
							"sPdfHeaderColor" : "0x3FA2D0",
							"sPdfBgColor" : "0xD6E9EE",
							"sPdfAlpha"  : 0.5,
							"sFileName" : 'detalle cursos inscritos <?php echo $datos['socio'] ?>.pdf',
							"sTitle" : 'Detalle cursos inscritos <?php echo $datos['socio'] ?>'

						}
					]
			}
	});
});
function createFile(url, datos, buttonT)
{
	$.ajax({
		type: 'POST',
		dataType: "json",
		data: {
			user : '<?php echo $datos['socio'] ?>',
			folio : '<?php echo $datos['folio'] ?>',
			data : datos,
		},
		url: url,
		beforeSend : function(){
			$('#load-document').show();
			buttonT.attr("disabled", true);
      	},
		success: function(data){
			$('#load-document').hide();
			buttonT.attr("disabled", false);
			window.location.href = "<?php echo url_for('reportes_control_escolar/download_file')?>?file="+data.file;
		},
    	error  : function(e){
    		console.log(e);
    		alert('Ocurrio un error al crear el documento, contacte al administrador del sistema');
    		$('#load-document').hide();
    		buttonT.attr("disabled", false);
        }
	});	
}
</script>
