<div id="tabs-cursos" style="width: auto;">
	<ul>
		<li><a href="#detalle-global">General</a>
		</li>
		<li><a href="#detalle-curso">Por curso</a>
		</li>
	</ul>
	<div id="detalle-global">

		<table id="tabla-global" width="100%">
			<thead>
				<tr>
					<th>Socio</th>
					<th>Usuario</th>
					<th>Curso</th>
					<th>Grupo</th>
					<th>Fecha Inicio</th>
					<th>Facilitador</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($datos as $key => $curso):?>
				<?php $nombreCurso = explode(':', $key) ?>
				<?php foreach ($curso as $detalle): ?>
				<tr>
					<td><?php echo $detalle['socio']?>
					</td>
					<td><?php echo $detalle['usuario']?>
					</td>
					<td><?php echo $nombreCurso[1] ?>
					</td>
					<td><?php echo $detalle['grupo']?>
					</td>
					<td><?php echo $detalle['inicio']?>
					</td>
					<td><?php echo $detalle['facilitador']?>
					</td>
				</tr>
				<?php endforeach;?>
				<?php endforeach;?>
			</tbody>
		</table>

	</div>
	<div id="detalle-curso">

		<div id="accordion-cursos">
			<?php foreach($datos as $key => $curso):?>
			<?php $table = explode(':', $key);  // Obtemos solo la clave del curso y se asigna como id de la tabla?>
			<h3>
				<a href="#"><?php echo $key ?>
				</a>
			</h3>
			<div class="accordion-tables">
				<table id="<?php echo $table[0]?>" width="100%"
					filename="Socios <?php echo $centro.' '.$desde.' a '.$hasta.' '.$table[0] ?>.pdf"
					titletable="Reporte socios inscritos <?php echo $centro.', '.$periodo.', curso '.$table[1] ?>">
					<thead>
						<tr>
							<th>Socio</th>
							<th>Usuario</th>
							<th>Grupo</th>
							<th>Fecha Inicio</th>
							<th>Facilitador</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($curso as $detalle): ?>
						<tr>
							<td><?php echo $detalle['socio']?>
							</td>
							<td><?php echo $detalle['usuario']?>
							</td>
							<td><?php echo $detalle['grupo']?>
							</td>
							<td><?php echo $detalle['inicio']?>
							</td>
							<td><?php echo $detalle['facilitador']?>
							</td>
						</tr>
						<?php endforeach;?>
					</tbody>
				</table>
			</div>
			<?php endforeach;?>
		</div>

	</div>
</div>
<script type="text/javascript">
<?php foreach($datos as $key => $curso):?>
	<?php $tableOne = explode(':', $key); ?>
	<?php break; ?>
<?php endforeach; ?>
var GrupoTable,
    GlobalTable,
    index1 = false,
    grupoEmpty,
	firstTable = $('#<?php echo $tableOne[0] ?>');
$(function() {
	$("#tabs-cursos").tabs({
		show: function(event, ui) {
			console.log(ui);
			if(ui.index == 0)
			{
				if(index1)
				{
					if(!grupoEmpty)
					{
						GrupoTable.fnDestroy();
					}
					$("#accordion-cursos").accordion("destroy");
				}
				GlobalTable = $('#tabla-global').dataTable({
					"bPaginate": true,
					"bLengthChange": true,
					"bFilter": true,
					"bSort": true,
					"bInfo": true,
					"bAutoWidth": false,
					"bJQueryUI": true,
					"sPaginationType": "full_numbers",
					"sDom": '<T><"clear"><"H"lfr>t<"F"ip>',
					"oTableTools": {
						"sSwfPath": "/js/plugins/dataTables/swf/DataTables.swf",
						"aButtons": [
						 			"copy",
						 			"csv",
						 			{
						 				"sExtends": "xls",
						 				"sFieldSeperator": "|",
						 				"sCharSet" : "utf8",
						 				"sFileName" : "Detalle Grupos-Facilirtadores.csv"
							 		},
						 			{
										"sExtends": "pdf",
										"sPdfTextSize": 6,
										"sPdfHeaderColor" : "0x3FA2D0",
										"sPdfBgColor" : "0xD6E9EE",
										"sPdfAlpha"  : 0.5,
										"sFileName" : "Socios <?php echo $centro.' '.$desde.' a '.$hasta ?>  global.pdf",
										"sTitle" : "Reporte socios inscritos <?php echo $centro.', '.$periodo.', global' ?>",

									}
								]
						},
					"fnDrawCallback": function() {
				            $('#windows-wrapper').children('.window-column').height(parseInt($("#lista-curso").height())+170 );
					}
				});
			}
			else if(ui.index==1)
			{
				index1 = true;
				grupoEmpty = false;
				GlobalTable.fnDestroy();
				createTable(firstTable,"Socios <?php echo $centro.' '.$anio.'-'.$mes.' '.$table[0] ?>.pdf", "Reporte socios inscritos <?php echo $centro.', '.$mes.' de '.$anio.', curso '.$table[1] ?>");
				$("#accordion-cursos").accordion({
					autoHeight: false,
					collapsible: true,
					create: function(event, ui)
					{
						$('#windows-wrapper').children('.window-column').height(parseInt($("#lista-curso").height())+170 );
					},
					change: function(event, ui)
					{
						$('#windows-wrapper').children('.window-column').height(parseInt($("#lista-curso").height())+170 );
					},
					changestart: function(event, ui) {
						if(ui.oldContent.length > 0 && ui.newContent.length == 0)
						{
							ui.oldContent.hide('fast', function(){
								GrupoTable.fnDestroy();
								grupoEmpty = true;
							});
						}
						else if(ui.newContent.length > 0 && ui.oldContent.length == 0)
						{
							var filename = ui.newContent.children('table').attr('filename'),
								title = ui.newContent.children('table').attr('titletable');
							createTable($(ui.newContent.children('table')),filename, title);
							grupoEmpty = false;
						}
						else if(ui.newContent.length > 0 && ui.oldContent.length > 0)
						{
							ui.oldContent.hide('fast', function(){
								GrupoTable.fnDestroy();
								var filename = ui.newContent.children('table').attr('filename'),
									title = ui.newContent.children('table').attr('titletable');
								createTable($(ui.newContent.children('table')),filename, title);
								grupoEmpty = false;
							});
						}
					},
				});

			}

		}
	});
});
function createTable(target,filename, title)
{
	GrupoTable = target.dataTable({
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",
		"sDom": '<T><"clear"><"H"lfr>t<"F"ip>',
		"oTableTools": {
			"sSwfPath": "/js/plugins/dataTables/swf/DataTables.swf",
			"aButtons": [
			 			"copy",
			 			"csv",
			 			{
			 				"sExtends": "xls",
			 				"sFieldSeperator": "|",
			 				"sCharSet" : "utf8",
			 				"sFileName" : "Detalle Grupos-Facilirtadores.csv"
				 		},
			 			{
							"sExtends": "pdf",
							"sPdfTextSize": 6,
							"sPdfHeaderColor" : "0x3FA2D0",
							"sPdfBgColor" : "0xD6E9EE",
							"sPdfAlpha"  : 0.5,
							"sFileName" : filename,
							"sTitle" : title,

						}
					]
			},
			"fnDrawCallback": function() {
	            $('#windows-wrapper').children('.window-column').height(parseInt($("#lista-curso").height())+170 );
		}
	});
}
</script>
