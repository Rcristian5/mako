<?php use_javascript('jquery-ui-1.8.16.js') ?>
<?php use_stylesheet('jquery.windows.css') ?>
<?php use_stylesheet('datatables.css') ?>
<?php use_stylesheet('TableTools2.css') ?>
<?php use_stylesheet('control_escolar/historial_academico.css') ?>
<?php use_javascript('plugins/jquery.dataTables1.8.js') ?>
<?php use_javascript('plugins/dataTables/TableToolsPDF.js') ?>
<?php use_javascript('plugins/dataTables/ZeroClipboard1.8.js') ?>
<?php use_javascript('plugins/jquery.windows.js') ?>
<?php use_javascript('i18n/jquery-ui-i18n.js') ?>

<div id="windows-wrapper">
	<div window-title="Historial académico" window-state="min">

		<div id="detalle">
			&nbsp;
			<div id="carga-datos">
				<div class="info">
					<h1 class="ui-state-active ui-corner-all"
						style="color: #ffffff !important"></h1>
					<img src="/imgs/ajax-loader.gif" alt="..." />
				</div>
				<div class="error" style="display: none;">
					<h1 class="ui-state-error ui-corner-all"></h1>
					<input type="button" class="retry" value="Intentar nuevamente" />
				</div>
			</div>
			<div id="reporte"></div>

		</div>

		<div id="busqueda">
			<form>
				<div class="ui-widget">
					<div class="ui-widget-content ui-corner-all">
						<div class="ui-widget-header ui-corner-tl ui-corner-tr tsss"
							style="padding: 5px;">Buscar socio</div>
						<table>
							<tr>
								<th>Centro</th>
								<td><select name="centro">
										<?php foreach ($centros as $key => $centro):?>
										<option value="<?php echo $key ?>">
											<?php echo $centro ?>
										</option>
										<?php endforeach;?>
								</select>
								</td>
							</tr>
							<tr>
							
							
							<tr>
								<th>Folio</th>
								<td><input type="text" name="folio" filtro="numerico"
									class="opcion-busqueda" size="20" maxlength="30"
									autocomplete="off" id="folio" /></td>
							</tr>
							<tr>
								<th>Nombre usuario</th>
								<td><input type="text" name="usuario" size="20" id="usuario"
									class="opcion-busqueda" autocomplete="off" /></td>
							</tr>
							<tr>
								<th>Nombre completo</th>
								<td><input type="text" name="nombre" filtro="alfabetico"
									class="opcion-busqueda" size="20" id="nombre"
									autocomplete="off" /></td>
							</tr>
							<tr>
								<th>Buscar</th>
								<td><input type="submit" id="boton-buscar-socios"
									value="Buscar socio" onClick="return caracteresBusqueda()" /></td>
							</tr>
						</table>
					</div>
				</div>
			</form>
		</div>

	</div>
</div>
<div id="dialog-history">
	<div id="carga-historial">
		<div class="info">
			<h1 class="ui-state-active ui-corner-all"
				style="color: #ffffff !important"></h1>
			<img src="/imgs/ajax-loader.gif" alt="..." />
		</div>
		<div class="error" style="display: none;">
			<h1 class="ui-state-error ui-corner-all"></h1>
			<input type="button" class="retry-history"
				value="Intentar nuevamente" />
		</div>
	</div>
	<div id="historial"></div>
</div>

<script type="text/javascript">

$(document).ready(function () {
	$('#windows-wrapper').windows({
		resizable : true,
	    minimizable: false,
	    minheight : 300
	});
	$('.opcion-busqueda').focus(function (){
		var actual = $(this).attr('id');
		$('.opcion-busqueda').each(function(){
			if($(this) != actual)
			{
				$(this).val('');
			}	 
		});
	});
	$('#dialog-history').dialog({
	    resizable: false,
	   	height:   600,
		width :   900,
		autoOpen: false,
		modal:    true,
	});
	<?php if( isset($busqueda) && isset($centroSel)): ?>
	loadUsers();
	$('.retry').live('click', function(){
		$('#carga-datos .error').hide();
		$('#reporte').html('');
		loadUsers();
	});
	<?php endif;?>
});

function caracteresBusqueda()
{	
	var nombre = $('#nombre').val(),
		usuario = $('#usuario').val(),
		folio = $('#folio').val();
	
	if(folio.length > 0 && folio.length < 10)
	{
		alert("La búsqueda por folio debe tener al menos 10 caracteres.");
		return false;	
	}
	else if(usuario.length > 0 && usuario.length < 10)
	{
		alert("La búsqueda por usuario debe tener al menos 10 caracteres.");
		return false;	
	}
	else if(nombre.length > 0 && nombre.length < 10)
	{
			alert("La búsqueda por nombre debe tener al menos 10 caracteres.");
			return false;	
	}
	else if(nombre == '' && folio == '' && usuario == '')
	{
		alert("Debes elegir al menos una opción para la busqueda del socio.");
		return false;
	}
	else
	{
		return true;	
	}

}
<?php if( isset($busqueda) && isset($centroSel)): ?>
function loadUsers()
{
	$('#carga-datos').show();
	$.ajax({
		dataType: "jsonp",
		timeout : 5000,
		data: {
			busqueda : '<?php echo $busqueda ?>',
			campo    : '<?php echo $campo ?>',
		},
		url: 'http://<?php echo $centroSel->getIp() ?>/reportes_control_escolar/buscar_socio?method=?',
		beforeSend : function(){
			$('#carga-datos .info').children('h1').html('Conectando con el centro, espere');
			$('#carga-datos .info').show();
      	},
		success: function(data){
			$('#carga-datos').hide();
			if(data.detalle.length != 0)
			{
				$.ajax({
					type: 'POST',
					data: {
						data   : data.detalle,
						tipo   : data.tipo,
						centro  : '<?php echo $centroSel->getId() ?>',
					},
					url: '<?php echo url_for('reportes_control_escolar/formatearReporte')?>',
					beforeSend : function(){
						$('#carga-datos .info').show().children('h1').html('Procesando datos');
						$('#carga-datos .error').hide();
			      	},
					success: function(data){
						$('#carga-datos').hide();
						$('#reporte').html(data);
					},
			    	error  : function(e){
			    		$('#carga-datos .info').hide();
			    		$('#carga-datos .error').show().children('h1').html('No fue posible procesar los datos');
			        }
				});
			}
			else
			{
				$('#reporte').html('<h1 class="ui-state-highlight ui-corner-all">No hay datos para mostrar</h1><input type="button" class="retry" value="Intentar nuevamente" />');
			}
		},
    	error  : function(e){
    		$('#carga-datos .info').hide();
			if(e.statusText == 'timeout')
			{
				$('#carga-datos .error').show().children('h1').html('No fue posible conectar con el centro');
			}
			else
			{
				$('#carga-datos .error').show().children('h1').html('Ocurrio un error en el centro, contacte al administrador');
			}

        }
	});
}
<?php endif; ?>
</script>
