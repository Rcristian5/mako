<h1 class="ui-state-active ui-corner-all"
	style="color: #ffffff !important">
	<?php echo $centro->getNombre() ?>
</h1>

<?php foreach ($datos as $socio): ?>
<div class="ui-widget wrapper-socio">
	<div class="ui-widget-content ui-corner-all">
		<div class="ui-widget-header ui-corner-tl ui-corner-tr tsss"
			style="padding: 5px;">
			<?php echo $socio['nombre']?>
		</div>

		<table width="100%">
			<tr>
				<td width="25%" rowspan="6" valign="top" colspan="2">
					<div class="container fotoGrande">
						<img
							src="http://<?php echo $centro->getIp().'/'.($socio['foto'] != '')? 'fotos/'.$socio['foto'] : 'imgs/sin_foto_sh.jpg' ?>"
							class="main fotoGrande sombra-delgada" /> <img
							class="minor fotoGrande" src="/imgs/frame.png">
					</div>
				</td>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<th width="10%">Usuario:</th>
				<td><?php echo $socio['usuario'] ?></td>
				<th width="10%">Fecha Nac:</th>
				<td><?php echo $socio['fechaNacimiento'] ?></td>
			</tr>
			<tr>
				<th>Edad:</th>
				<td><?php echo $socio['edad'] ?> años</td>
				<th>Género:</th>
				<td><?php echo $socio['sexo'] ?></td>
			</tr>
			<tr>
				<th>Folio:</th>
				<td><?php echo $socio['folio'] ?></td>
				<th>Estudia:</th>
				<td><?php echo $socio['nivelEstudio'] ?></td>
			</tr>
			<tr>
				<th>Alta en:</th>
				<td><?php echo $socio['alta'] ?></td>
				<th>Trabaja:</th>
				<td><?php echo $socio['trabaja'] ?></td>
			</tr>
			<tr>
				<th>&nbsp;</th>
				<td>&nbsp;</td>
				<th>&nbsp;</th>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td valign="top">&nbsp;</td>
				<td colspan="5"><input type="button" class="show-history"
					value="Ver historial académico" id="<?php echo $socio['id']?>" />
				</td>
			</tr>
		</table>
	</div>
</div>
<?php endforeach; ?>

<script type="text/javascript">
var socio;
$(function() {
	$('#windows-wrapper').children('.window-column').height(parseInt($("#reporte").height())+50 );
	$('.show-history').click(function(){
		$('#dialog-history').dialog('open');
		socio = $(this).attr('id');
		historialacademico(socio);
	});
	$('.retry-history').live('click', function(){
		$('#carga-historial .error').hide();
		historialacademico(socio);
	});
});

function historialacademico(socio)
{
	$('#historial').html('');
	$('#carga-historial').show();
	$.ajax({
		dataType: "jsonp",
		timeout : 5000,
		data: {
			socio : socio,
		},
		url: 'http://<?php echo $centro->getIp() ?>/reportes_control_escolar/detalle_historial?method=?',
		beforeSend : function(){
			$('#carga-historial .info').children('h1').html('Conectando con el centro, espere');
			$('#carga-historial .info').show();
      	},
		success: function(data){
			$('#carga-historial').hide();
			if(data.detalle.length != 0)
			{
				$.ajax({
					type: 'POST',
					data: {
						data   : data.detalle,
						tipo   : data.tipo,
					},
					url: '<?php echo url_for('reportes_control_escolar/formatearReporte')?>',
					beforeSend : function(){
						$('#carga-historial .info').show().children('h1').html('Procesando datos');
						$('#carga-historial .error').hide();
			      	},
					success: function(data){
						$('#carga-historial').hide();
						$('#historial').html(data);
					},
			    	error  : function(e){
			    		$('#carga-historial .info').hide();
			    		$('#carga-historial .error').show().children('h1').html('No fue posible procesar los datos');
			        }
				});
			}
			else
			{
				$('#historial').html('<h1 class="ui-state-highlight ui-corner-all">No hay datos para mostrar</h1><input type="button" class="retry" value="Intentar nuevamente" />');
			}
		},
    	error  : function(e){
    		$('#carga-historial .info').hide();
			if(e.statusText == 'timeout')
			{
				$('#carga-historial .error').show().children('h1').html('No fue posible conectar con el centro');
			}
			else
			{
				$('#carga-historial .error').show().children('h1').html('Ocurrio un error en el centro, contacte al administrador');
			}

        }
	});
}

</script>
