<div id="accordion-cursos">
	<?php foreach($datos as $key => $detalle):?>
	<h3>
		<a href="#"><?php echo $key ?>
		</a>
	</h3>
	<div>
		<table width="100%" class="detalle-grupo-lista">
			<tr>
				<th>Grupo</th>
				<td><?php echo $key?>
				</td>
			</tr>
			<tr>
				<th>Facilitador</th>
				<td><?php echo $detalle['facilitador'] ?>
				</td>
			</tr>
			<tr>
				<th>Inicio</th>
				<td><?php echo $detalle['inicio'] ?>
				</td>
			</tr>
			<tr>
				<th>Fin</th>
				<td><?php echo $detalle['fin'] ?>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<table width="100%" class="dataTables_wrapper">
						<thead>
							<tr>
								<th
									class="ui-widget ui-widget-header <?php echo $detalle['horario']['lu'] != 'true' ? 'ui-state-disabled' : '' ?>">Lunes</th>
								<th
									class="ui-widget ui-widget-header <?php echo $detalle['horario']['ma'] != 'true' ? 'ui-state-disabled' : '' ?>">Martes</th>
								<th
									class="ui-widget ui-widget-header <?php echo $detalle['horario']['mi'] != 'true' ? 'ui-state-disabled' : '' ?>">Mi&eacute;rcoles</th>
								<th
									class="ui-widget ui-widget-header <?php echo $detalle['horario']['ju'] != 'true' ? 'ui-state-disabled' : '' ?>">Jueves</th>
								<th
									class="ui-widget ui-widget-header <?php echo $detalle['horario']['vi'] != 'true' ? 'ui-state-disabled' : '' ?>">Viernes</th>
								<th
									class="ui-widget ui-widget-header <?php echo $detalle['horario']['sa'] != 'true' ? 'ui-state-disabled' : '' ?>">S&aacute;bado</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="even"><?php echo $detalle['horario']['lu'] == 'true' ? $detalle['horario']['hi_lu'].'-'.$detalle['horario']['hf_lu'] : '' ?>
								</td>
								<td class="even"><?php echo $detalle['horario']['ma'] == 'true' ? $detalle['horario']['hi_ma'].'-'.$detalle['horario']['hf_ma'] : '' ?>
								</td>
								<td class="even"><?php echo $detalle['horario']['mi'] == 'true' ? $detalle['horario']['hi_mi'].'-'.$detalle['horario']['hf_mi'] : '' ?>
								</td>
								<td class="even"><?php echo $detalle['horario']['ju'] == 'true' ? $detalle['horario']['hi_ju'].'-'.$detalle['horario']['hf_ju'] : '' ?>
								</td>
								<td class="even"><?php echo $detalle['horario']['vi'] == 'true' ? $detalle['horario']['hi_vi'].'-'.$detalle['horario']['hf_vi'] : '' ?>
								</td>
								<td class="even"><?php echo $detalle['horario']['sa'] == 'true' ? $detalle['horario']['hi_sa'].'-'.$detalle['horario']['hf_sa'] : '' ?>
								</td>
							</tr>
						
						
						<tbody>
					
					</table>
				</td>
			</tr>
		</table>
		<table id="<?php echo $detalle['id'] ?>" class="lista-asistencias"
			width="100%"
			filename="Asistencias <?php echo $centro.' '.$detalle['curso'].$desde ?>.pdf"
			titletable="Lista de asistencias <?php echo $centro.' '.$detalle['curso'].$desde ?>">
			<thead>
				<tr>
					<th>Socio</th>
					<th>Usuario</th>
					<?php foreach ($detalle['fechas'] as $fecha): ?>
					<th><?php echo $fecha['dia'] ?>
					</th>
					<?php endforeach; ?>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($detalle['lista'] as $lista): ?>
				<tr>
					<td><?php echo $lista['socio'] ?>
					</td>
					<td><?php echo $lista['usuario'] ?>
					</td>

					<?php foreach ($lista['asistencias'] as $asistencia): ?>
					<td><?php echo $asistencia['asistencia'] ?>
					</td>
					<?php endforeach ?>

				</tr>
				<?php endforeach ?>
			</tbody>
		</table>
	</div>
	<?php endforeach;?>
</div>
<?php foreach($datos as $detalle):?>
<?php $tableOne = $detalle['id'] ?>
<?php break; ?>
<?php endforeach; ?>
<script type="text/javascript">
var GrupoTable,
	firstTable = $('#<?php echo $tableOne ?>');
$(function() {
	$('#accordion-cursos').accordion({
		autoHeight: false,
		create: function(event, ui) {
			var filename = firstTable.attr('filename'),
				title = firstTable.attr('titletable');
			$('#windows-wrapper').children('.window-column').height(parseInt($('#accordion-cursos').height())+330 );
			createTable(firstTable,filename, title); 
		},
		change: function(event, ui) {
			GrupoTable.fnDestroy();
			var filename = ui.newContent.children('table.lista-asistencias').attr('filename'),
				title = ui.newContent.children('table.lista-asistencias').attr('titletable');
			$('#windows-wrapper').children('.window-column').height(parseInt($('#accordion-cursos').height())+330 );
			createTable($(ui.newContent.children('table.lista-asistencias')),filename, title);
		}
	});
});
function createTable(target,filename, title)
{
	GrupoTable = target.dataTable({
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",
		"sDom": '<T><"clear"><"H"lfr>t<"F"ip>',
		"oTableTools": {
			"sSwfPath": "/js/plugins/dataTables/swf/DataTables.swf",
			"aButtons": [
			 			"copy",
			 			"csv",
			 			{
			 				"sExtends": "xls",
			 				"sFieldSeperator": "|",
			 				"sCharSet" : "utf8",
			 				"sFileName" : "Detalle Grupos-Facilirtadores.csv"
				 		},
			 			{
							"sExtends": "pdf",
							"sPdfTextSize": 6,
							"sPdfHeaderColor" : "0x3FA2D0",
							"sPdfBgColor" : "0xD6E9EE",
							"sPdfAlpha"  : 0.5,
							"sFileName" : filename,
							"sTitle" : title,

						}
					]
			},
		"fnDrawCallback": function() {
	            $('#windows-wrapper').children('.window-column').height(parseInt($('#lista-curso').height())+330 );
		}
	});
}
</script>
