<?php use_helper('Date') ?>

<?php use_javascript('jquery-ui-1.8.16.js') ?>
<?php use_stylesheet('jquery.windows.css') ?>
<?php use_stylesheet('datatables.css') ?>
<?php use_stylesheet('TableTools2.css') ?>
<?php use_stylesheet('control_escolar/reporte_socios.css') ?>
<?php use_javascript('plugins/jquery.dataTables1.8.js') ?>
<?php use_javascript('plugins/dataTables/TableToolsPDF.js') ?>
<?php use_javascript('plugins/dataTables/ZeroClipboard1.8.js') ?>
<?php use_javascript('plugins/jquery.windows.js') ?>
<?php use_javascript('i18n/jquery-ui-i18n.js') ?>

<div id="windows-wrapper">
	<div window-title="Consultar socios inscritos" window-state="min">
		<h1 class="nombre-reporte">Consulta de socios inscritos</h1>
		<?php if($reporte): ?>
		<table id="table-consult">
			<tr>
				<th><?php echo $centroSel->getNombre() ?>
				</th>
				<th><?php echo $cursoSel->getNombre() ?>
				</th>
				<th><?php if(date('Ymt', $desde) == date('Ymd', $hasta) ): ?> <?php echo ucfirst(format_date($desde, 'MMMM', 'es_MX')).' de '.date('Y', $desde) ?>
					<?php elseif( (date('Ymt', $desde) != date('Ymd', $hasta)) &&  (date('Ym', $desde) == date('Ym', $hasta)) ): ?>
					del <?php echo date('d', $desde).' al '.date('d', $hasta).' de '.ucfirst(format_date($desde, 'MMMM', 'es_MX')).' de '.date('Y', $desde) ?>
					<?php elseif( (date('d', $desde) == 1) &&  (date('d', $hasta) == date('t', $hasta)) && (date('y', $desde) == date('y', $hasta))  && (date('m', $desde) != date('m', $hasta)) ): ?>
					<?php echo ucfirst(format_date($desde, 'MMMM', 'es_MX')).'-'.ucfirst(format_date($hasta, 'MMMM', 'es_MX')).' de '.date('Y', $desde) ?>
					<?php elseif( date('y', $desde) == date('y', $hasta) ): ?> del <?php echo date('d', $desde).' de ',ucfirst(format_date($desde, 'MMMM', 'es_MX')).' al '.date('d', $hasta).' de '.ucfirst(format_date($hasta, 'MMMM', 'es_MX')).' de '.date('Y', $desde) ?>
					<?php else: ?> del <?php echo date('d', $desde).' de ',ucfirst(format_date($desde, 'MMMM', 'es_MX')).' de '.date('Y', $desde).' al '.date('d', $hasta).' de '.ucfirst(format_date($hasta, 'MMMM', 'es_MX')).' de '.date('Y', $hasta) ?>
					<?php endif; ?>
				</th>
				<th><input type="button" id="change-option" value="Cambiar opciones" />
				</th>
			</tr>
		</table>
		<div id="lista-grupo">
			<div class="info">
				<h1 class="ui-state-active ui-corner-all"
					style="color: #ffffff !important"></h1>
				<img src="/imgs/ajax-loader.gif" alt="..." />
			</div>
			<div class="error" style="display: none;">
				<h1 class="ui-state-error ui-corner-all"></h1>
				<input type="button" class="refresh-tab" value="Intentar nuevamente" />
			</div>
			<div class="reporte"></div>
		</div>
		<?php else: ?>
		<div id="reopen-dialog" style="display: none;">
			<input type="button" id="change-option" value="Generar reporte" />
		</div>
		<?php endif; ?>
	</div>
</div>

<?php include_partial('configurarConsulta', array(
		'centros' => $centros,
		'cursos'  => $cursos,
		'url'     => url_for('reportes_control_escolar/consulta_inscritos'),
		'title'   => 'Opciones para la consulta',
		'centroSel' => $reporte ? $centroSel->getId() : null,
		'curso'     => true,
		'cursoSel'  => $reporte ? $cursoSel->getId() : null,
		'desde'     => $desde ? date('Y-m-d', $desde) : null,
		'hasta'     => $hasta ? date('Y-m-d', $hasta) : null
)) ?>


<script type="text/javascript">
<?php if($reporte): ?>
var url = 'http://<?php echo $centroSel->getIp() ?>/reportes_control_escolar/reporte_inscritos?method=?',
target = $('#lista-grupo');
<?php endif; ?>
$(document).ready(function () {
	$('#windows-wrapper').windows({
		resizable : true,
	    minimizable: false,
	    minheight : 300
	});
	$('#change-option').click(function(){
		if($('#reopen-dialog').length > 0)
		{
			$('#reopen-dialog').hide();
		}
		$('#dialog-opciones').dialog('open');
	});
	<?php if(!$reporte): ?>
		$('#dialog-opciones').dialog('open');
	<?php else: ?>
		getReporte(target, url);
		$('.refresh-tab').live('click', function(){
			getReporte(target, url);
		});
	<?php endif; ?>
});
<?php if($reporte): ?>
function getReporte(target, url)
{
	target.children('.reporte').html('');
	target.children('.error').hide();
	target.children('.info').show().children('h1').html('Generando consulta');
	$.ajax({
		dataType: "jsonp",
		timeout : 50000,
		data: {
			centro : <?php echo $centroSel->getId() ?>,
			curso  : <?php echo $cursoSel->getId() ?>,
			desde  : '<?php echo date('Y-m-d', $desde) ?>',
			hasta  : '<?php echo date('Y-m-d', $hasta) ?>'
		},
		url: url,
		beforeSend : function(){
			target.children('.info').children('h1').html('Conectando con el centro, espere');
      	},
		success: function(data){
			target.children('.info').hide();
			target.children('.error').hide();
			if(data.detalle.length != 0)
			{
				$.ajax({
					type: 'POST',
					data: {
						data   : data.detalle,
						tipo   : data.tipo,
						curso  : '<?php echo $cursoSel->getNombre() ?>',
						centro : '<?php echo $centroSel->getNombre() ?>'
					},
					url: '<?php echo url_for('reportes_control_escolar/formatearReporte')?>',
					beforeSend : function(){
						target.children('.info').show().children('h1').html('Procesando datos');
						target.children('.error').hide();
			      	},
					success: function(data){
						target.children('.info').hide();
						target.children('.error').hide();
						target.children('.reporte').html(data);
					},
			    	error  : function(e){
						target.children('.info').hide();
						target.children('.error').show().children('h1').html('No fue posible procesar los datos');
			        }
				});
			}
			else
			{
				target.children('.reporte').html('<h1 class="ui-state-highlight ui-corner-all">No hay datos para mostrar</h1><input type="button" class="refresh-tab" value="Intentar nuevamente" />');
			}
		},
    	error  : function(e){
    		target.children('.info').hide();
			if(e.statusText == 'timeout')
			{

				target.children('.error').show().children('h1').html('No fue posible conectar con el centro');
			}
			else
			{
				target.children('.error').show().children('h1').html('Ocurrio un error en el centro, contacte al administrador');
			}

        }
	});
}
<?php endif; ?>
</script>
