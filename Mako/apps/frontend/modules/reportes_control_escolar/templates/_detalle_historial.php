<h1 bgcolor="#2191c0"
	style="border: 1px solid #4297d7; background: #2191c0; color: #eaf5f7; text-align: center;">
	&nbsp;<br>
	<?php echo $socio ?>
</h1>
<p>&nbsp;</p>
<h2 bgcolor="#6eac2c"
	style="border: 1px solid #acdd4a; background: #6eac2c; color: #ffffff; text-align: center;">
	&nbsp;<br>
	<?php echo $grupo['curso'] ?>
	, Grupo
	<?php echo $grupo['grupo']?>
</h2>
<p>&nbsp;</p>

<table border="0">
	<tr>
		<td width="50%">
			<table width="100%">
				<tr>
					<th width="60%" bgcolor="#2191c0"
						style="border: 1px solid #4297d7; color: #eaf5f7; text-align: center;">
						D&iacute;a</th>
					<th width="40%" bgcolor="#2191c0"
						style="border: 1px solid #4297d7; color: #eaf5f7; text-align: center;">
						Asistio</th>
				</tr>
				<?php $clases = array('even' => '#F1F5F8', 'odd' => '#D6E9EE' )?>
				<?php $class = 'even'?>
				<?php foreach ($grupo['listaAsistencia'] as $lista): ?>
				<tr>
					<td bgcolor="<?php echo $clases[$class] ?>"
						style="text-align: center;"><?php echo $lista['fecha'].' '.$lista['horaInicio'].' - '.$lista['horaFin'] ?>
					</td>
					<td bgcolor="<?php echo $clases[$class] ?>"
						style="text-align: center;"><?php echo $lista['asistencia'] ?>
					</td>
					<?php $class = ($class == 'even') ? 'odd' : 'even' ?>
				</tr>
				<?php endforeach;?>
			</table>
		</td>
		<td width="3%">&nbsp;</td>
		<td width="47%">
			<h3 bgcolor="#f8da4e"
				style="border: 1px solid #fcd113; color: #915608; text-align: center;">
				&nbsp;<br> Calificaci&oacute;n final:
				<?php echo $grupo['calificacion'] ?>
			</h3> &nbsp;<br>
			<h3 bgcolor="#f8da4e"
				style="border: 1px solid #fcd113; color: #915608; text-align: center;">
				&nbsp;<br> Porcentaje de asistencias:
				<?php echo $grupo['porAsistencia'] ?>
				% <br>
			</h3>
		</td>
	</tr>
</table>
