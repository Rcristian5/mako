<?php use_stylesheet('./reportes_control_escolar/_configurarConsulta.css')?>
<div id="dialog-opciones" title="<?php echo $title ?>">
	<p>
		<label for="select-centro">Centro</label> <select id="select-centro">
			<?php foreach($centros as $key => $centro): ?>
			<option value="<?php echo $key?>"
			<?php echo ($centroSel == $key) ? 'selected="selected"' : '' ?>>
				<?php echo $centro?>
			</option>
			<?php endforeach; ?>
		</select>
	</p>
	<?php if($curso):?>
	<p>
		<label for="select-curso">Curso</label> <select id="select-curso">
			<?php foreach($cursos as $key => $curso): ?>
			<option value="<?php echo $key?>"
			<?php echo ($cursoSel == $key) ? 'selected="selected"' : '' ?>>
				<?php echo $curso?>
			</option>
			<?php endforeach; ?>
		</select>
	</p>
	<?php endif; ?>
	<p>
		<label for="from">Desde</label> <input type="text" id="from"
			value="<?php echo $desde ?>" />

	</p>
	<p>
		<label for="to">Hasta</label> <input type="text" id="to"
			value="<?php echo $hasta ?>" />
	</p>
</div>

<script type="text/javascript">
$(document).ready(function () {
		$.datepicker.setDefaults( $.datepicker.regional[ "es" ] );
		var dates = $( "#from, #to" ).datepicker({
			dateFormat: 'yy-mm-dd',
			minDate : new Date(2010, 8, 1),
			firstDay: 1,
			defaultDate: 0,
			changeMonth: true,
			changeYear: true,
			numberOfMonths: 2,
			onSelect: function( selectedDate ) {
				var option = this.id == "from" ? "minDate" : "maxDate",
					instance = $( this ).data( "datepicker" ),
					date = $.datepicker.parseDate(
						instance.settings.dateFormat ||
						$.datepicker._defaults.dateFormat,
						selectedDate, instance.settings );
				dates.not( this ).datepicker( "option", option, date );
			}
		});
		$("#dialog-opciones").dialog({
		    resizable: false,
		   	height: <?php echo $curso ? 300 : 250 ?>,
			width : 550,
			autoOpen: false,
			modal: true,
		    buttons: {
			   "Aceptar": function() {
				   location.href = '<?php echo $url ?>?centro='+$('#select-centro').val()+'&curso='+$('#select-curso').val()+'&desde='+$('#from').val()+'&hasta='+$('#to').val();
				},
				"Cancelar": function() {
				  $( this ).dialog( "close" );
				}
			},
		close: function(event, ui)
		{
			if($('#reopen-dialog').length > 0)
			{
				$('#reopen-dialog').show();
			}
		}
	});
});
</script>
