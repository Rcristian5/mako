<div id="detalle-grupos" class="ui-widget">
	<div class="ui-widget ui-widget-content ui-corner-all">
		<div class="ui-widget ui-widget-header ui-corner-top">Grupos
			calendarizados</div>
		<div id="wrapper-table">
			<table width="100%" id="table-grupos">
				<thead>
					<tr>
						<th>Grupo</th>
						<th>Facilitador</th>
						<th>Inicio</th>
						<th>Disponible</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($datos as $clave => $detalle): ?>
					<tr>
						<td><?php echo $clave ?>
						</td>
						<td><?php echo $detalle['facilitador'] ?>
						</td>
						<td><?php echo $detalle['inicio'] ?>
						</td>
						<td><?php echo $detalle['disponible'] ?>
						</td>
						<td><span class="ui-icon ui-icon-note detail"
							grupo="<?php echo $detalle['id']?>"></span>
						</td>
					</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<div id="lista-inscritos" class="ui-widget">

	<div class="ui-widget ui-widget-content ui-corner-all">
		<div class="ui-widget ui-widget-header ui-corner-top">Grupos
			calendarizados</div>
		<div id="default">Selecciona un grupo para ver la lista</div>
		<?php foreach ($datos as $clave => $detalle): ?>
		<div id="<?php echo $detalle['id'] ?>" class="content-detalle-grupo">
			<table class="detalle-grupo">
				<tr>
					<th>Grupo</th>
					<td><?php echo $clave ?></td>
				</tr>
				<tr>
					<th>Facilitador</th>
					<td><?php echo $detalle['facilitador'] ?></td>
				</tr>
				<tr>
					<th>Aula</th>
					<td><?php echo $detalle['aula'] ?></td>
				</tr>
				<tr>
					<th>Cupo total</th>
					<td><?php echo $detalle['total'] ?></td>
				</tr>
				<tr>
					<th>Cupo actual</th>
					<td><?php echo $detalle['actual'] ?></td>
				</tr>
				<tr>
					<th>Cupo disponible</th>
					<td><?php echo $detalle['disponible'] ?></td>
				</tr>
				<tr>
					<th>Cupo alcanzado</th>
					<td><?php echo $detalle['alcanzado'] ?></td>
				</tr>
				<tr>
					<th>Inicio</th>
					<td><?php echo $detalle['inicio'] ?></td>
				</tr>
				<tr>
					<th>Fin</th>
					<td><?php echo $detalle['fin'] ?></td>
				</tr>
				<tr>
					<td colspan="2">
						<table width="100%" class="dataTables_wrapper">
							<thead>
								<tr>
									<th
										class="ui-widget ui-widget-header <?php echo $detalle['horario']['lu'] != 'true' ? 'ui-state-disabled' : '' ?>">Lunes</th>
									<th
										class="ui-widget ui-widget-header <?php echo $detalle['horario']['ma'] != 'true' ? 'ui-state-disabled' : '' ?>">Martes</th>
									<th
										class="ui-widget ui-widget-header <?php echo $detalle['horario']['mi'] != 'true' ? 'ui-state-disabled' : '' ?>">Mi&eacute;rcoles</th>
									<th
										class="ui-widget ui-widget-header <?php echo $detalle['horario']['ju'] != 'true' ? 'ui-state-disabled' : '' ?>">Jueves</th>
									<th
										class="ui-widget ui-widget-header <?php echo $detalle['horario']['vi'] != 'true' ? 'ui-state-disabled' : '' ?>">Viernes</th>
									<th
										class="ui-widget ui-widget-header <?php echo $detalle['horario']['sa'] != 'true' ? 'ui-state-disabled' : '' ?>">S&aacute;bado</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="even"><?php echo $detalle['horario']['lu'] == 'true' ? $detalle['horario']['hi_lu'].'-'.$detalle['horario']['hf_lu'] : '' ?>
									</td>
									<td class="even"><?php echo $detalle['horario']['ma'] == 'true' ? $detalle['horario']['hi_ma'].'-'.$detalle['horario']['hf_ma'] : '' ?>
									</td>
									<td class="even"><?php echo $detalle['horario']['mi'] == 'true' ? $detalle['horario']['hi_mi'].'-'.$detalle['horario']['hf_mi'] : '' ?>
									</td>
									<td class="even"><?php echo $detalle['horario']['ju'] == 'true' ? $detalle['horario']['hi_ju'].'-'.$detalle['horario']['hf_ju'] : '' ?>
									</td>
									<td class="even"><?php echo $detalle['horario']['vi'] == 'true' ? $detalle['horario']['hi_vi'].'-'.$detalle['horario']['hf_vi'] : '' ?>
									</td>
									<td class="even"><?php echo $detalle['horario']['sa'] == 'true' ? $detalle['horario']['hi_sa'].'-'.$detalle['horario']['hf_sa'] : '' ?>
									</td>
								</tr>
							
							
							<tbody>
						
						</table>
					</td>
				</tr>
			</table>

			<table class="lista-asistencia dataTables_wrapper" width="94%">
				<thead>
					<tr>
						<th class="ui-widget ui-widget-header">Socio</th>
						<th class="ui-widget ui-widget-header">Usuario</th>
						<th class="ui-widget ui-widget-header">Estado</th>
					</tr>
				</thead>
				<tbody>
					<?php $class="odd"?>
					<?php foreach ($detalle['lista'] as $lista): ?>
					<tr>
						<td class="<?php echo $class?>"><?php echo $lista['socio'] ?></td>
						<td class="<?php echo $class?>"><?php echo $lista['usuario'] ?></td>
						<td class="<?php echo $class?>"><?php echo $lista['estado'] ?></td>
					</tr>
					<?php $class= ($class == "odd") ? 'even' : 'odd'?>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>
		<?php endforeach ?>
	</div>
</div>
<?php foreach($datos as $key => $curso):?>
<?php $tableOne = explode(':', $key); ?>
<?php break; ?>
<?php endforeach; ?>
<script type="text/javascript">
$(function() {
	$('.detail').click(function(){
		var target = $(this).attr('grupo');
		console.log(target);
		$('.detail').removeClass('ui-state-disabled');
		$('.content-detalle-grupo').hide();
		if($('#default').is(':visible'))
		{
			$('#default').hide();
		}
		$('#'+target).show()
		$(this).addClass('ui-state-disabled');
	});
	GrupoTable = $('#table-grupos').dataTable({
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",
		"fnDrawCallback": function() {
			$('#windows-wrapper').children('.window-column').height(parseInt($("#detalle-grupos").height())+350 );
		}
	});

});
</script>

