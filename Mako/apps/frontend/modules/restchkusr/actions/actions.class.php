<?php

/**
 * restchkusr actions.
 *
 * @package    mako NMP
 * @subpackage restchkusr
 * @author     Fabrica de Software Enova
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class restchkusrActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    $arch_log    = "/tmp/log_restchkusr.log";
  		$cadena_recibida = $request->getContent();
  		$this->getResponse()->setContentType('application/json');

   		// Validamos que sea un JSON valido
  		if ( !Comun::EsJson($cadena_recibida) )
  		{
  			error_log("\n[REST_CHKUSR] La cadena enviada no es un JSON, favor de validar",3,$arch_log);
  			$this->getResponse()->setStatusCode(404);
  			return $this->renderText(json_encode(array("code" => 404, "message" => "ERROR => " . json_last_error() . " :: Json no valido, por favor verifique.", "status" => true, "data" => array())));
  		}
  		$respuesta = new stdClass();
  		$respuesta->usr_id   = null;
  		$respuesta->nombre   = null;
  		$respuesta->apepat   = null;
  		$respuesta->apemat   = null;
  		$respuesta->email    = null;
  		$respuesta->sexo     = null;
  		$respuesta->username = null;
  		$respuesta->rol      = null;
  		$respuesta->perfiles   = null;

  		switch($request->getMethod())
  		{
  			case 'POST':
  				if ( !$request->hasParameter('usr_id') || !$request->hasParameter('clave') || !$request->hasParameter('tipo_usr')) {
  					error_log("\n[REST_CHKUSR] Algún parametro obligatorio no ha sido proveido. Por favor, verifique.",3,$arch_log);
  					$this->getResponse()->setStatusCode(404);
  					return $this->renderText(json_encode(array("code" => 404, "message" => "ERROR :: Algún parametro obligatorio no ha sido proveido. Por favor, verifique.", "status" => true, "data" => array())));
  				}

  				$usr_id     = $request->getParameter('usr_id');
    			$clave  = $request->getParameter('clave');
    			$tipo_usr	= $request->getParameter('tipo_usr');

    			error_log("\n[REST_CHKUSR] PARAMS:   USR_ID=> " . $usr_id . "  ||  CLAVE=> " . $clave . "  ||  TIPO_USR=> " . $tipo_usr ,3,$arch_log);

    			switch ($tipo_usr) 
    			{
    				case "socio":
    					error_log("\n[REST_CHKUSR] Validando usuario de tipo SOCIO.",3,$arch_log);
    					$c = new Criteria();
    					$c->clearSelectColumns();
    					$c->addSelectColumn(SocioPeer::ID);
    					$c->addSelectColumn(SocioPeer::NOMBRE);
    					$c->addSelectColumn(SocioPeer::APEPAT);
    					$c->addSelectColumn(SocioPeer::APEMAT);
    					$c->addSelectColumn(SocioPeer::EMAIL);
    					$c->addSelectColumn(SocioPeer::SEXO);
    					$c->addSelectColumn(SocioPeer::USUARIO);
    					$c->addAsColumn("rol_nombre", RolPeer::NOMBRE);
    					$c->addJoin(SocioPeer::ROL_ID,RolPeer::ID,Criteria::INNER_JOIN);
    					$c->add(SocioPeer::CLAVE,$clave);
    					$c->add(SocioPeer::ID,$usr_id);

    					error_log("\n[REST_CHKUSR] QRY => " . $c->toString(),3,$arch_log);

    					$ObjSocio = SocioPeer::doSelectStmt($c);
    					$rows_soc = $ObjSocio->fetchAll(PDO::FETCH_ASSOC);
  						if ( count($rows_soc) != 1) {
  							$this->getResponse()->setStatusCode(404);
    						return $this->renderText(json_encode(array("code" => 404, "message" => "ERROR => Usuario Invalido.", "status" => true, "data" => array())));
    					}
    					foreach ($rows_soc as $row_soc) {
    						$respuesta->usr_id       = $row_soc['id'];
    						$respuesta->nombre   = $row_soc['nombre'];
    						$respuesta->apepat   = $row_soc['apepat'];
    						$respuesta->apemat   = $row_soc['apemat'];
    						$respuesta->email    = $row_soc['email'];
    						$respuesta->sexo     = $row_soc['sexo'];
    						$respuesta->username = $row_soc['usuario'];
    						$respuesta->rol      = $row_soc['rol_nombre'];
    						$respuesta->perfiles = array();
    					}
  	
    					break;
    				case "staff":
    					error_log("\n[REST_CHKUSR] Validando usuario de tipo STAFF.",3,$arch_log);
    					$c = new Criteria();
    					$c->clearSelectColumns();
    					$c->addSelectColumn(UsuarioPeer::ID);
    					$c->addSelectColumn(UsuarioPeer::NOMBRE);
    					$c->addSelectColumn(UsuarioPeer::APEPAT);
    					$c->addSelectColumn(UsuarioPeer::APEMAT);
    					$c->addSelectColumn(UsuarioPeer::EMAIL);
    					$c->addSelectColumn(UsuarioPeer::USUARIO);
    					$c->addSelectColumn(UsuarioPeer::ROL_ID);
    					$c->addAsColumn("rol_nombre", RolPeer::NOMBRE);
    					$c->addAsColumn("perfiles", "ARRAY_TO_STRING(ARRAY_AGG(CONCAT(" .  PerfilFacilitadorPeer::ID . " || ' => ' || " . PerfilFacilitadorPeer::NOMBRE . ")),'|')" );
    					$c->addJoin(UsuarioPeer::ROL_ID,RolPeer::ID, Criteria::INNER_JOIN);
    					$c->addJoin(UsuarioPeer::ID,UsuarioPerfilPeer::USUARIO_ID,Criteria::LEFT_JOIN);
    					$c->addJoin(UsuarioPerfilPeer::PERFIL_ID,PerfilFacilitadorPeer::ID, Criteria::LEFT_JOIN);
    					$c->addGroupByColumn(UsuarioPeer::ID);
    					$c->addGroupByColumn(UsuarioPeer::NOMBRE);
    					$c->addGroupByColumn(UsuarioPeer::APEPAT);
    					$c->addGroupByColumn(UsuarioPeer::APEMAT);
    					$c->addGroupByColumn(RolPeer::NOMBRE);
    					$c->add(UsuarioPeer::CLAVE,$clave);
    					$c->add(UsuarioPeer::ID,$usr_id);

    					//error_log("\n[REST_CHKUSR] Qry => " . $c->toString(),3,$arch_log);

    					$ObjUsr = UsuarioPeer::doSelectStmt($c);
    					$rows_usr = $ObjUsr->fetchAll(PDO::FETCH_ASSOC);
  						if ( count($rows_usr) != 1) {
  							$this->getResponse()->setStatusCode(404);
    						return $this->renderText(json_encode(array("code" => 404, "message" => "ERROR => Usuario Invalido.", "status" => true, "data" => array())));
    					}
    					foreach ($rows_usr as $row_usr) {
    						error_log("\n[REST_CHKUSR] ROW_USR => " . print_r($row_usr,true),3,$arch_log);
    						$respuesta->usr_id       = $row_usr['id'];
    						$respuesta->nombre   = $row_usr['nombre'];
    						$respuesta->apepat   = $row_usr['apepat'];
    						$respuesta->apemat   = $row_usr['apemat'];
    						$respuesta->email    = $row_usr['email'];
    						$respuesta->sexo     = $row_usr['sexo'];
    						$respuesta->username = $row_usr['usuario'];
    						$respuesta->rol      = $row_usr['rol_nombre'];
    						if ( $row_usr['perfiles'] === "")
    							$arr_perfiles = array();
    						else
    							$arr_perfiles = explode("|",$row_usr['perfiles']);
    						$respuesta->perfiles   = $arr_perfiles;
    					}
    					break;
    			}
    			return $this->renderText(json_encode(array("code" => 200, "message" => "", "status" => true, "data" => array($respuesta))));
  				break;
  			case 'PUT':
    			break;
    		case 'DELETE':
    			break;
    		default:
    			return $this->renderText(json_encode(array("code" => 200, "message" => "ERROR :: No existe una operación valida. Por favor, verifique.", "status" => true, "data" => array())));
    	}

    	return $this->renderText(json_encode(array("code" => 200, "message" => "No se realizó ninguna operación.", "status" => true, "data" => array())));
  }
}
