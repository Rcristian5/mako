
<?php

/**
 * dashboard_reportes actions.
 *
 * @package    mako
 * @subpackage dashboard_reportes
 * @author     Bixit SA de CV
 * @version    SVN: $Id: actions.class.php,v 1.5 2012-04-20 06:17:48 david Exp $
 */
class dashboard_reportes_tmpActions extends sfActions
{
	/**
	 * Executes index action
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeIndex(sfWebRequest $request)
	{
		$filtro = $request->getParameter('filtro');
		$desde = $request->getParameter('desde');
		$hasta = $request->getParameter('hasta');
		$parametros = $request->getParameterHolder();
		if ($filtro && $desde && $hasta)
		{
			$meses = array("", "Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
			// Obtenmos los rangos de fechas
			$arregloDesde = explode('-', $desde);
			$arregloHasta = explode('-', $hasta);
			$fechaFlag = mktime(0,0,0, $arregloDesde[1], $arregloDesde[2], $arregloDesde[0]);
			// Cargamos las fechas iniciales del reporte
			if($request->getParameter('muestra') == 'mes')
			{
				$arregloFechas = array(
						$arregloDesde[0] => array(
								$arregloDesde[1] =>  array(
										'mes'   => $meses[$arregloDesde[1]],
										'desde' => date('Y-m-d', $fechaFlag),
										'hasta' => date('Y-m-t', $fechaFlag)
								)
						)
				);
			}
			if($request->getParameter('muestra') == 'semana')
			{
				$arregloFechas[date('Y', $fechaFlag)][date('W', $fechaFlag)] = date('W', $fechaFlag);
			}
			for ($i = 0; date('Ymd',$fechaFlag) < date('Ymd', mktime(0,0,0, $arregloHasta[1], $arregloHasta[2], $arregloHasta[0])); $i++ )
			{
				$fechaFlag = mktime(0,0,0, $arregloDesde[1], $arregloDesde[2] + $i, $arregloDesde[0]);
				if($request->getParameter('muestra') == 'mes')
				{
					if(!array_key_exists(date('Y', $fechaFlag), $arregloFechas))
					{
						$arregloFechas[date('Y', $fechaFlag)][date('n', $fechaFlag)] = array(
								'mes'   => $meses[date('n', $fechaFlag)],
								'desde' => date('Y-m-d', $fechaFlag),
								'hasta' => date('Y-m-t', $fechaFlag)
						);
					}
					else
					{
						if(!array_key_exists(date('n', $fechaFlag), $arregloFechas[date('Y', $fechaFlag)] ))
						{
							$arregloFechas[date('Y', $fechaFlag)][date('n', $fechaFlag)] = array(
									'mes'   => $meses[date('n', $fechaFlag)],
									'desde' => date('Y-m-d', $fechaFlag),
									'hasta' => date('Y-m-t', $fechaFlag)
							);
						}
					}
				}
				if($request->getParameter('muestra') == 'semana')
				{
					if(!array_key_exists(date('Y', $fechaFlag), $arregloFechas))
					{
						$arregloFechas[date('Y', $fechaFlag)][date('W', $fechaFlag)] = date('W', $fechaFlag);
					}
					else
					{
						if(!array_key_exists(date('W', $fechaFlag), $arregloFechas[date('Y', $fechaFlag)] ))
						{
							$arregloFechas[date('Y', $fechaFlag)][date('W', $fechaFlag)] = date('W', $fechaFlag);
						}
					}
				}
			}
			switch ($filtro)
			{
				case 'centro':
					$reporte = $this->reporteCentro($desde, $hasta, $parametros->getAll(), $arregloFechas, $request->getParameter('muestra'));
					break;
				case 'grupo':
					$reporte = $this->reporteGrupo($desde, $hasta, $parametros->getAll(), $arregloFechas, $request->getParameter('muestra'));
					break;
				case 'facilitador':
					$reporte = $this->reporteFacilitador($desde, $hasta, $parametros->getAll(), $arregloFechas, $request->getParameter('muestra'));
					break;
				case 'ria':
					error_log(print_r($arregloFechas, true));
					$reporte = $this->reporteRIA($desde, $hasta, $parametros->getAll(), $arregloFechas, $request->getParameter('muestra'));
					break;
				case 'zona':
					$reporte = $this->reporteZona($desde, $hasta, $parametros->getAll(), $arregloFechas, $request->getParameter('muestra'));
					break;
			}
			$archivo = 'reporte_'.$filtro.'_'.$desde.'_'.$hasta.'.csv';
			$reportePath = '/tmp/'.DIRECTORY_SEPARATOR.$archivo;
			file_put_contents($reportePath, $reporte, FILE_TEXT);
			$this->setLayout(false);
			sfConfig::set('sf_web_debug', false);
			$this->getResponse()->clearHttpHeaders();
			$this->getResponse()->setHttpHeader('Pragma: public', true);
			$this->getResponse()->setContentType('application/txt');
			$this->getResponse()->setHttpHeader('Content-Disposition', 'attachment; filename='.$archivo);
			$this->getResponse()->setContent(file_get_contents($reportePath));
			 
			return sfView::NONE;
		}
	}
	/**
	 * Genera el reporte por centro
	 * @param unknown_type $desde
	 * @param unknown_type $hasta
	 */
	private function reporteCentro($desde, $hasta, $parametros, $arregloFechas, $muestra)
	{
		// Obtenmos los indicadores por dia
		// Indicadores
		$registrados = array_key_exists('registrados', $parametros) ? true : false;
		$activos = array_key_exists('activos', $parametros) ? true : false;
		$activosNuevos = array_key_exists('activos_nuevos', $parametros) ? true : false;
		$nuevos = array_key_exists('nuevos', $parametros) ? true : false;
		$cobranzaInternet = array_key_exists('cobranza_internet', $parametros) ? true : false;
		$cobranzaEducacion = array_key_exists('cobranza_educacion', $parametros) ? true : false;
		$cobranzaOtros = array_key_exists('cobranza_otros', $parametros) ? true : false;
		$cobranzaTotal = array_key_exists('cobranza_total', $parametros) ? true : false;
		$preinscritos = array_key_exists('preinscritos', $parametros) ? true : false;
		$inscripcionEsperada = array_key_exists('inscripcion_esperada', $parametros) ? true : false;
		$inscritos = array_key_exists('inscritos', $parametros) ? true : false;
		$inscritosUnicos = array_key_exists('inscritos_unicos', $parametros) ? true : false;
		$inscritosNuevos = array_key_exists('inscritos_nuevos', $parametros) ? true : false;
		$inscritosRecurrentes = array_key_exists('inscritos_recurrentes', $parametros) ? true : false;
		$alumnosActivos = array_key_exists('alumnos_activos', $parametros) ? true : false;
		$alumnosEsperados = array_key_exists('alumnos_esperados', $parametros) ? true : false;
		//$alumnosEsperados = array_key_exists('alumnos_esperados', $parametros) ? true : false;
		$actividadProgramada = array_key_exists('actividad_programada', $parametros) ? true : false;
		$graduados = array_key_exists('graduados', $parametros) ? true : false;
		$graduacionEsperada = array_key_exists('graduacion_esperada', $parametros) ? true : false;
		$desertores = array_key_exists('desertores', $parametros) ? true : false;
		$pagosProgramados = array_key_exists('pagos_programados', $parametros) ? true : false;
		$pagosCobrados = array_key_exists('pagos_cobrados', $parametros) ? true : false;
		$cobranzaProgramada = array_key_exists('cobranza_programada', $parametros) ? true : false;
		$cobranzaCobrada = array_key_exists('cobranza_cobrada', $parametros) ? true : false;
		$headerReporte = "Centro";
		$arregloLineas = array();
		$flag = 0;
		foreach ($arregloFechas as $key => $anio)
		{
			foreach ($anio as $fecha)
			{
				$reporte = array();

				if($muestra == 'mes')
				{
					$headerReporte .= $registrados ? ', '.$fecha['mes'].' '.$key.' Socios Registrados' : '';
					$headerReporte .= $activos ? ', '.$fecha['mes'].' '.$key.' Socios Activos' : '';
					$headerReporte .= $activosNuevos ? ', '.$fecha['mes'].' '.$key.' Socios Activos Nuevos' : '';
					$headerReporte .= $nuevos ? ', '.$fecha['mes'].' '.$key.' Registros Nuevos' : '';
					$headerReporte .= $cobranzaInternet ? ', '.$fecha['mes'].' '.$key.' Cobranza Internet' : '';
					$headerReporte .= $cobranzaEducacion ? ', '.$fecha['mes'].' '.$key.' Cobranza Educacion' : '';
					$headerReporte .= $cobranzaOtros ? ', '.$fecha['mes'].' '.$key.' Cobranza Otros' : '';
					$headerReporte .= $cobranzaTotal ? ', '.$fecha['mes'].' '.$key.' Cobranza Total' : '';
					$headerReporte .= $inscripcionEsperada ? ', '.$fecha['mes'].' '.$key.' Inscripcion Esperada' : '';
					$headerReporte .= $preinscritos ? ', '.$fecha['mes'].' '.$key.' Alumnos Preinscritos' : '';
					$headerReporte .= $inscritos ? ', '.$fecha['mes'].' '.$key.' Alumnos Inscritos' : '';
					$headerReporte .= $inscritosUnicos ? ', '.$fecha['mes'].' '.$key.' Alumnos Inscritos Unicos' : '';
					$headerReporte .= $inscritosNuevos ? ', '.$fecha['mes'].' '.$key.' Alumnos Inscritos Nuevos' : '';
					$headerReporte .= $inscritosRecurrentes ? ', '.$fecha['mes'].' '.$key.' Alumnos Inscritos Recurrentes' : '';
					$headerReporte .= $actividadProgramada ? ', '.$fecha['mes'].' '.$key.' Alumnos Programados' : '';
					$headerReporte .= $alumnosActivos ? ', '.$fecha['mes'].' '.$key.' Alumnos Activos' : '';
					$headerReporte .= $alumnosEsperados ? ', '.$fecha['mes'].' '.$key.' Alumnos Esperados' : '';
					$headerReporte .= $graduados ? ', '.$fecha['mes'].' '.$key.' Alumnos Graduados' : '';
					$headerReporte .= $graduacionEsperada ? ', '.$fecha['mes'].' '.$key.' Graduacion Esperada' : '';
					$headerReporte .= $desertores ? ', '.$fecha['mes'].' '.$key.' Alumnos Desertores' : '';
					$headerReporte .= $pagosProgramados ? ', '.$fecha['mes'].' '.$key.'  Pagos Programados' : '';
					$headerReporte .= $pagosCobrados ? ', '.$fecha['mes'].' '.$key.'  Pagos Cobrados' : '';
					$headerReporte .= $cobranzaProgramada ? ', '.$fecha['mes'].' '.$key.'  Cobranza Programada' : '';
					$headerReporte .= $cobranzaCobrada ? ', '.$fecha['mes'].' '.$key.' Cobranza Cobrada' : '';
				}
				if($muestra == 'semana')
				{
					$headerReporte .= $registrados ? ', Semana '.$fecha.' '.$key.' Socios Registrados' : '';
					$headerReporte .= $activos ? ', Semana '.$fecha.' '.$key.' Socios Activos' : '';
					$headerReporte .= $activosNuevos ? ', Semana '.$fecha.' '.$key.' Socios Activos Nuevos' : '';
					$headerReporte .= $nuevos ? ', Semana '.$fecha.' '.$key.' Registros Nuevos' : '';
					$headerReporte .= $cobranzaInternet ? ', Semana '.$fecha.' '.$key.' Cobranza Internet' : '';
					$headerReporte .= $cobranzaEducacion ? ', Semana '.$fecha.' '.$key.' Cobranza Educacion' : '';
					$headerReporte .= $cobranzaOtros ? ', Semana '.$fecha.' '.$key.' Cobranza Otros' : '';
					$headerReporte .= $cobranzaTotal ? ', Semana '.$fecha.' '.$key.' Cobranza Total' : '';
					$headerReporte .= $inscripcionEsperada ? ', Semana '.$fecha.' '.$key.' Inscripcion Eperada' : '';
					$headerReporte .= $preinscritos ? ', Semana '.$fecha.' '.$key.' Alumnos Preinscritos' : '';
					$headerReporte .= $inscritos ? ', Semana '.$fecha.' '.$key.' Alumnos Inscritos' : '';
					$headerReporte .= $inscritosUnicos ? ', Semana '.$fecha.' '.$key.' Alumnos Inscritos Unicos' : '';
					$headerReporte .= $inscritosNuevos ? ', Semana '.$fecha.' '.$key.' Alumnos Inscritos Nuevos' : '';
					$headerReporte .= $inscritosRecurrentes ? ', Semana '.$fecha.' '.$key.' Alumnos Inscritos Recurrentes' : '';
					$headerReporte .= $actividadProgramada ? ', Semana '.$fecha.' '.$key.' Alumnos Programados' : '';
					$headerReporte .= $alumnosActivos ? ', Semana '.$fecha.' '.$key.' Alumnos Activos' : '';
					$headerReporte .= $alumnosEsperados ? ', Semana '.$fecha.' '.$key.' Alumnos Esperados' : '';
					$headerReporte .= $graduados ? ', Semana '.$fecha.' '.$key.' Alumnos Graduados' : '';
					$headerReporte .= $graduacionEsperada ? ', Semana '.$fecha.' '.$key.' GraduacionEsperada' : '';
					$headerReporte .= $desertores ? ', Semana '.$fecha.' '.$key.' Alumnos Desertores' : '';
					$headerReporte .= $pagosProgramados ? ', Semana '.$fecha.' '.$key.'  Pagos Programados' : '';
					$headerReporte .= $pagosCobrados ? ', Semana '.$fecha.' '.$key.'  Pagos Cobrados' : '';
					$headerReporte .= $cobranzaProgramada ? ', Semana '.$fecha.' '.$key.'  Cobranza Programada' : '';
					$headerReporte .= $cobranzaCobrada ? ', Semana '.$fecha.' '.$key.' Cobranza Cobrada' : '';
				}
				// Revisamos si se requieren estas consultas
				//if($nuevos || $cobranzaOtros || $cobranzaInternet || $cobranzaTotal || $cobranzaEducacion)
				if($nuevos || $cobranzaOtros || $cobranzaInternet || $cobranzaTotal)
				{
					$sqlIndicadoresDia = "SELECT centro_id
					".($nuevos ? ",sum(registros_nuevos) as nuevos" : "");
					//$sqlIndicadoresDia .= ($cobranzaEducacion ? ",sum(cobranza_educacion) as cobranza_educacion" : "");
					$sqlIndicadoresDia .= ($cobranzaOtros ? ",sum(cobranza_otros) as cobranza_otros" : "");
					$sqlIndicadoresDia .= ($cobranzaInternet ? ",sum(cobranza_internet) as cobranza_internet" : "");
					$sqlIndicadoresDia .= ($cobranzaTotal ?  ",sum(cobranza_total) as cobranza_total" : "");
					$sqlIndicadoresDia .= " FROM reporte_indicadores_dia WHERE ";
					if($muestra == 'mes')
					{
						$sqlIndicadoresDia .= "fecha BETWEEN '".$fecha['desde']."' AND '".$fecha['hasta']."'";
					}
					if($muestra == 'semana')
					{
						$sqlIndicadoresDia .= "semana = ".$fecha." AND anio=".$key;
					}
					$sqlIndicadoresDia .= " GROUP BY centro_id";
					$connection = Propel::getConnection(ReporteIndicadoresDiaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
					$statement = $connection->prepare($sqlIndicadoresDia);
					$statement->execute();
					while($lineaIn  = $statement->fetch(PDO::FETCH_ASSOC))
					{
						$reporte[$lineaIn['centro_id']] = array(
								'nuevos'             => $nuevos ? $lineaIn['nuevos'] : '',
								//'cobranza_educacion' => $cobranzaEducacion ? $lineaIn['cobranza_educacion'] : '',
								'cobranza_otros'     => $cobranzaOtros ? $lineaIn['cobranza_otros']: '',
								'cobranza_internet'  => $cobranzaInternet ? $lineaIn['cobranza_internet'] : '',
								'cobranza_total'     => $cobranzaTotal ? $lineaIn['cobranza_total'] : ''
						);

					}
				}
				if($registrados)
				{
					$sqlRegistros = "SELECT centro_id, sum(socios_registrados) as registros FROM reporte_indicadores_dia WHERE ";
					if($muestra == 'mes')
					{
						$sqlRegistros .= "fecha <= '".$fecha['hasta']."'";
					}
					if($muestra == "semana")
					{
						$sqlRegistros .= "anio = ".$key;
						$sqlRegistros .= "AND semana = ".$fecha;
					}
					$sqlRegistros .= " GROUP BY centro_id";
					$connection = Propel::getConnection(ReporteIndicadoresDiaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
					$statement = $connection->prepare($sqlRegistros);
					$statement->execute();
					$lineaR = null;
					while($lineaR = $statement->fetch(PDO::FETCH_ASSOC))
					{
						$reporte[$lineaR['centro_id']]['registros'] = $lineaR['registros'];
					}
				}
				if($inscripcionEsperada)
				{
					$sqlInscripcionEsperada = "SELECT g.centro_id, sum(g.cupo_total) as esperada FROM grupo as g, horario as h WHERE g.horario_id = h.id ";
					if($muestra == "mes")
					{
						$sqlInscripcionEsperada .= " AND g.activo=true AND h.fecha_inicio BETWEEN '".$fecha['desde']." 00:00:00' AND '".$fecha['hasta']." 23:59:59'";
					}
					if($muestra == 'semana')
					{
						$sqlInscripcionEsperada .= " AND g.activo=true AND date_part('week', h.fecha_inicio) = ".$fecha. " AND date_part('year', h.fecha_inicio) = ".$key;
					}
					$sqlInscripcionEsperada .= " GROUP BY g.centro_id";
					error_log($sqlInscripcionEsperada);
					$connection = Propel::getConnection(ReporteIndicadoresDiaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
					$statement = $connection->prepare($sqlInscripcionEsperada);
					$statement->execute();
					 
					while($linea = $statement->fetch(PDO::FETCH_ASSOC))
					{
						$reporte[$linea['centro_id']]['inscripcion_esperada'] += $linea['esperada'];
					}
				}
				if($alumnosEsperados)
				{
					$sqlAlumnosEsperados = "SELECT g.centro_id, sum(g.cupo_total) as esperada FROM grupo as g, horario as h WHERE g.horario_id = h.id ";
					if($muestra == "mes")
					{
						$sqlAlumnosEsperados .= " AND g.activo=true AND (h.fecha_inicio, h.fecha_fin) OVERLAPS (DATE '".$fecha['desde']."', DATE '".$fecha['hasta']."') = true";
					}
					if($muestra == 'semana')
					{
						$fechasSemana = $this->diasSemana($fecha, $key);
						$sqlAlumnosEsperados .= " AND g.activo=true AND (h.fecha_inicio, h.fecha_fin) OVERLAPS (DATE '".$fechasSemana['desde']."', DATE '".$fechasSemana['hasta']."') = true";
					}
					$sqlAlumnosEsperados .= " GROUP BY g.centro_id";
					$connection = Propel::getConnection(ReporteIndicadoresDiaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
					$statement = $connection->prepare($sqlAlumnosEsperados);
					$statement->execute();

					while($linea = $statement->fetch(PDO::FETCH_ASSOC))
					{
						$reporte[$linea['centro_id']]['alumnos_esperados'] += $linea['esperada'];
					}
				}

				if($activos)
				{
					$sqlSociosActivos = "SELECT r.centro_id, s.socio_id as activos FROM reporte_indicadores_dia as r, reporte_indicadores_dia_socios_activos as s WHERE r.id = s.reporte_indicador_dia_id ";
					if($muestra == "mes")
					{
						$sqlSociosActivos .= "AND r.fecha BETWEEN '".$fecha['desde']."' AND '".$fecha['hasta']."'";
					}
					if($muestra == 'semana')
					{
						$sqlSociosActivos .= "AND r.semana = ".$fecha." AND r.anio=".$key;;
					}
					$sqlSociosActivos .= " GROUP BY r.centro_id, s.socio_id";
					$connection = Propel::getConnection(ReporteIndicadoresDiaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
					$statement = $connection->prepare($sqlSociosActivos);
					$statement->execute();
					while($linea = $statement->fetch(PDO::FETCH_ASSOC))
					{
						$reporte[$linea['centro_id']]['activos'] += 1;
					}
				}
				if($activosNuevos)
				{
					$sqlSociosActivos = "SELECT r.centro_id, s.socio_id as activos FROM reporte_indicadores_dia as r, reporte_indicadores_dia_socios_activos_nuevos as s WHERE r.id = s.reporte_indicador_dia_id ";
					if($muestra == "mes")
					{
						$sqlSociosActivos .= "AND r.fecha BETWEEN '".$fecha['desde']."' AND '".$fecha['hasta']."'";
					}
					if($muestra == 'semana')
					{
						$sqlSociosActivos .= "AND r.semana = ".$fecha." AND r.anio=".$key;;
					}
					$sqlSociosActivos .= " GROUP BY r.centro_id, s.socio_id";
					$connection = Propel::getConnection(ReporteIndicadoresDiaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
					$statement = $connection->prepare($sqlSociosActivos);
					$statement->execute();
					while($linea = $statement->fetch(PDO::FETCH_ASSOC))
					{
						$reporte[$linea['centro_id']]['activosNuevos'] += 1;
					}
				}
				/**
				 * cambiar como obtener los inscritos en base a l fecha de inicio del grupo no de de la fecha de inscripcion
				 * por facilitador cuantos lugares disponibles tiene espera un facilitador, capacidad real aula, aly
				 *
				 *
				 */
				// Alumnos Programados
				if($actividadProgramada)
				{
					$sqlSociosActivos = "SELECT r.centro_id, r.grupo_id, s.socio_id, r.grupo_id as activos FROM reporte_indicadores_dia_grupo as r, reporte_indicadores_dia_grupo_alumnos_actividad_programada as s WHERE r.id = s.reporte_indicador_dia_grupo_id ";
					if($muestra == 'mes')
					{
						$sqlSociosActivos .= "AND r.fecha BETWEEN '".$fecha['desde']."' AND '".$fecha['hasta']."' ";
					}
					if ($muestra == 'semana')
					{
						$sqlSociosActivos .= "AND r.semana = ".$fecha." AND r.anio=".$key;;
					}
					$sqlSociosActivos .= " GROUP BY r.centro_id, r.grupo_id, s.socio_id, r.grupo_id";
					$connection = Propel::getConnection(ReporteIndicadoresDiaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
					$statement = $connection->prepare($sqlSociosActivos);
					$statement->execute();
					while($linea = $statement->fetch(PDO::FETCH_ASSOC))
					{
						$reporte[$linea['centro_id']]['actividad_programada'] += 1;
					}
				}
				// Revisamos si se necesitan los alumnos activos
				if($alumnosActivos)
				{
					$sqlSociosActivos = "SELECT r.centro_id, r.grupo_id, s.socio_id as activos FROM reporte_indicadores_dia_grupo as r, reporte_indicadores_dia_grupo_alumnos_activos as s WHERE r.id = s.reporte_indicador_dia_grupo_id ";
					if($muestra == 'mes')
					{
						$sqlSociosActivos .= "AND r.fecha BETWEEN '".$fecha['desde']."' AND '".$fecha['hasta']."' ";
					}
					if ($muestra == 'semana')
					{
						$sqlSociosActivos .= "AND r.semana = ".$fecha." AND r.anio=".$key;;
					}
					$sqlSociosActivos .= " GROUP BY r.centro_id, r.grupo_id, s.socio_id";
					$connection = Propel::getConnection(ReporteIndicadoresDiaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
					$statement = $connection->prepare($sqlSociosActivos);
					$statement->execute();
					while($linea = $statement->fetch(PDO::FETCH_ASSOC))
					{
						$reporte[$linea['centro_id']]['alumnos_activos'] += 1;
					}
				}

				// Revisamos si se necesitan los alumnos inscritos
				/*
				if($inscritos)
				{
				// Buscamos los grupo que inician dentro del periodo seleccionado
				$sqlAlumnosEsperados = "SELECT g.centro_id, sum(g.cupo_total) as esperada FROM grupo as g, horario as h WHERE g.horario_id = h.id ";
				if($muestra == "mes")
				{
				$sqlAlumnosEsperados .= " AND g.activo=true AND (h.fecha_inicio, h.fecha_fin) OVERLAPS (DATE '".$fecha['desde']."', DATE '".$fecha['hasta']."') = true";
				}
				if($muestra == 'semana')
				{
				$fechasSemana = $this->diasSemana($fecha, $key);
				$sqlAlumnosEsperados .= " AND g.activo=true AND (h.fecha_inicio, h.fecha_fin) OVERLAPS (DATE '".$fechasSemana['desde']."', DATE '".$fechasSemana['hasta']."') = true";
				}
				$sqlAlumnosEsperados .= " GROUP BY g.centro_id";
				$connection = Propel::getConnection(ReporteIndicadoresDiaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
				$statement = $connection->prepare($sqlAlumnosEsperados);
				$statement->execute();
				 
				while($linea = $statement->fetch(PDO::FETCH_ASSOC))
				{
				$reporte[$linea['centro_id']]['alumnos_esperados'] += $linea['esperada'];
				}
				}
				*/
				if($preinscritos || $inscritos || $inscritosNuevos || $inscritosRecurrentes ||  $graduados ||
						$desertores || $pagosProgramados || $pagosCobrados || $cobranzaProgramada || $cobranzaCobrada
				)
				{
					$sqlIndicadoresDiaGrupo = "SELECT centro_id ";
					$sqlIndicadoresDiaGrupo .= ( $preinscritos ? ",sum(socios_preinscritos) as preinscritos" : "");
					$sqlIndicadoresDiaGrupo .= ( $inscritos ? ",sum(socios_inscritos) as inscritos" : "");
					$sqlIndicadoresDiaGrupo .= ( $inscritosNuevos ?",sum(socios_inscritos_nuevos) as inscritos_nuevos" : "");
					$sqlIndicadoresDiaGrupo .= ( $inscritosRecurrentes ? ",sum(socios_inscritos_recurrentes) as inscritos_recurrentes" : "");
					$sqlIndicadoresDiaGrupo .= ( $graduados ? ",sum(alumnos_graduados) as alumnos_graduados" : "");
					$sqlIndicadoresDiaGrupo .= ( $graduacionEsperada ? ",sum(graduacion_esperada) as graduacion_esperada" : "");
					$sqlIndicadoresDiaGrupo .= ( $desertores ? ",sum(alumnos_desertores) as alumnos_desertores" : "");
					$sqlIndicadoresDiaGrupo .= ( $pagosProgramados ? ",sum(pagos_programados) as pagos_programados" : "");
					$sqlIndicadoresDiaGrupo .= ( $pagosCobrados ? ",sum(pagos_cobrados) as pagos_cobrados" : "");
					$sqlIndicadoresDiaGrupo .= ( $cobranzaProgramada ? ",sum(cobranza_programada) as cobranza_programada" : "");
					$sqlIndicadoresDiaGrupo .= ( $cobranzaCobrada ? ",sum(cobranza_cobrada) as cobranza_cobrada" : "");
					$sqlIndicadoresDiaGrupo .=	" FROM reporte_indicadores_dia_grupo WHERE ";
					if($muestra == 'mes')
					{
						$sqlIndicadoresDiaGrupo .= "fecha BETWEEN '".$fecha['desde']."' AND '".$fecha['hasta']."' ";
					}
					if ($muestra == 'semana')
					{
						$sqlIndicadoresDiaGrupo .= "semana = ".$fecha." AND anio=".$key;;
					}
					$sqlIndicadoresDiaGrupo .= " GROUP BY centro_id";
					$connection = Propel::getConnection(ReporteIndicadoresDiaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
					$statement = $connection->prepare($sqlIndicadoresDiaGrupo);
					$statement->execute();
					while($linea = $statement->fetch(PDO::FETCH_ASSOC))
					{
						$reporte[$linea['centro_id']]['preinscritos'] = $preinscritos ? $linea['preinscritos'] : '';
						$reporte[$linea['centro_id']]['inscritos'] = $inscritos ? $linea['inscritos'] : '';
						$reporte[$linea['centro_id']]['inscritos_nuevos'] = $inscritosNuevos ? $linea['inscritos_nuevos'] : '';
						$reporte[$linea['centro_id']]['inscritos_recurrentes'] = $inscritosRecurrentes ? $linea['inscritos_recurrentes'] : '';
						//$reporte[$linea['centro_id']]['alumnos_activos'] = $alumnosActivos ? $linea['alumnos_activos'] : '';
						$reporte[$linea['centro_id']]['alumnos_graduados'] = $graduados ? $linea['alumnos_graduados'] : '';
						$reporte[$linea['centro_id']]['graduacion_esperada'] = $graduacionEsperada ? $linea['graduacion_esperada'] : '';
						$reporte[$linea['centro_id']]['alumnos_desertores'] = $desertores ? $linea['alumnos_desertores'] : '';
						$reporte[$linea['centro_id']]['pagos_programados'] = $pagosProgramados ? $linea['pagos_programados'] : '';
						$reporte[$linea['centro_id']]['pagos_cobrados'] = $pagosCobrados ? $linea['pagos_cobrados'] : '';
						$reporte[$linea['centro_id']]['cobranza_programada'] = $cobranzaProgramada ? $linea['cobranza_programada'] : '';
						$reporte[$linea['centro_id']]['cobranza_cobrada'] = $cobranzaCobrada ? $linea['cobranza_cobrada'] : '';
					}
				}
				foreach ($reporte as $keyR => $lineaR)
				{
					$lineaReporte .= $cobranzaOtros ? ', '.(isset($lineaR['cobranza_otros']) ? $lineaR['cobranza_otros'] : '0' ) : '';
					$lineaReporte = '';
					$lineaReporte .= $registrados ? ', '.(isset($lineaR['registros']) ? $lineaR['registros'] : '0' ) : '';
					$lineaReporte .= $activos ? ', '.(isset($lineaR['activos']) ? $lineaR['activos'] : '0' ) : '';
					$lineaReporte .= $activosNuevos ? ', '.(isset($lineaR['activosNuevos']) ? $lineaR['activosNuevos'] : '0' ) : '';
					$lineaReporte .= $nuevos ? ', '.(isset($lineaR['nuevos']) ? $lineaR['nuevos'] : '0' ) : '';
					$lineaReporte .= $cobranzaInternet ? ', '.(isset($lineaR['cobranza_internet']) ? $lineaR['cobranza_internet'] : '0' ) : '';
					$lineaReporte .= $cobranzaEducacion ? ', '.(isset($lineaR['cobranza_cobrada']) ? $lineaR['cobranza_cobrada'] : '0' ) : '';
					$lineaReporte .= $cobranzaOtros ? ', '.(isset($lineaR['cobranza_otros']) ? $lineaR['cobranza_otros'] : '0' ) : '';
					$lineaReporte .= $cobranzaTotal ? ', '.(isset($lineaR['cobranza_total']) ? $lineaR['cobranza_total'] : '0' ) : '';
					$lineaReporte .= $inscripcionEsperada ? ', '.(isset($lineaR['inscripcion_esperada']) ? $lineaR['inscripcion_esperada'] : '0' ) : '';
					$lineaReporte .= $preinscritos ? ', '.(isset($lineaR['preinscritos']) ? $lineaR['preinscritos'] : '0' ) : '';
					$lineaReporte .= $inscritos ? ', '.(isset($lineaR['inscritos']) ? $lineaR['inscritos'] : '0' ) : '';
					$lineaReporte .= $inscritosUnicos ? ', '.(isset($lineaR['inscritos_unicos']) ? $lineaR['inscritos_unicos'] : '0' ) : '';
					$lineaReporte .= $inscritosNuevos ? ', '.(isset($lineaR['inscritos_nuevos']) ? $lineaR['inscritos_nuevos'] : '0' ) : '';
					$lineaReporte .= $inscritosRecurrentes ? ', '.(isset($lineaR['inscritos_recurrentes']) ? $lineaR['inscritos_recurrentes'] : '0' ) : '';
					$lineaReporte .= $actividadProgramada ? ', '.(isset($lineaR['actividad_programada']) ? $lineaR['actividad_programada'] : '0' ) : '';
					$lineaReporte .= $alumnosActivos ? ', '.(isset($lineaR['alumnos_activos']) ? $lineaR['alumnos_activos'] : '0' ) : '';
					$lineaReporte .= $alumnosEsperados ? ', '.(isset($lineaR['alumnos_esperados']) ? $lineaR['alumnos_esperados'] : '0' ) : '';
					$lineaReporte .= $graduados ? ', '.(isset($lineaR['alumnos_graduados']) ? $lineaR['alumnos_graduados'] : '0' ) : '';
					$lineaReporte .= $graduacionEsperada ? ', '.(isset($lineaR['graduacion_esperada']) ? $lineaR['graduacion_esperada'] : '0' ) : '';
					$lineaReporte .= $desertores ? ', '.(isset($lineaR['alumnos_desertores']) ? $lineaR['alumnos_desertores'] : '0' ) : '';
					$lineaReporte .= $pagosProgramados ? ', '.(isset($lineaR['pagos_programados']) ? $lineaR['pagos_programados'] : '0' ) : '';
					$lineaReporte .= $pagosCobrados ? ', '.(isset($lineaR['pagos_cobrados']) ? $lineaR['pagos_cobrados'] : '0' ) : '';
					$lineaReporte .= $cobranzaProgramada ? ', '.(isset($lineaR['cobranza_programada']) ? $lineaR['cobranza_programada'] : '0' ) : '';
					$lineaReporte .= $cobranzaCobrada ? ', '.(isset($lineaR['cobranza_cobrada']) ? $lineaR['cobranza_cobrada'] : '0' ) : '';
					$arregloLineas[$keyR][$flag] = $lineaReporte;
				}
				$flag ++;
			}
		}
		$headerReporte .= "\n\r";
		$lineaReporteF = '';
		foreach ($arregloLineas as $keyL => $centro)
		{
			$centroL = CentroPeer::retrieveByPK($keyL);
			$lineaReporteF .= $centroL->getAlias();
			for ($i = 0; $i < $flag; $i++)
			{
				if(array_key_exists($i, $centro))
				{
					$lineaReporteF .= $centro[$i];
				}
				else
				{
					$lineaReporteF .= $registrados ? ', 0' : '';
					$lineaReporteF .= $activos ? ', 0' : '';
					$lineaReporteF .= $activosNuevos ? ', 0' : '';
					$lineaReporteF .= $nuevos ? ', 0' : '';
					$lineaReporteF .= $cobranzaInternet ? ', 0' : '';
					$lineaReporteF .= $cobranzaEducacion ? ', 0' : '';
					$lineaReporteF .= $cobranzaOtros ? ', 0' : '';
					$lineaReporteF .= $cobranzaTotal ? ', 0' : '';
					$lineaReporteF .= $inscripcionEsperada ? ', 0' : '';
					$lineaReporteF .= $preinscritos ? ', 0' : '';
					$lineaReporteF .= $inscritos ? ', 0' : '';
					$lineaReporteF .= $inscritosUnicos ? ', 0' : '';
					$lineaReporteF .= $inscritosNuevos ? ', 0' : '';
					$lineaReporteF .= $inscritosRecurrentes ? ', 0' : '';
					$lineaReporteF .= $actividadProgramada ? ', 0' : '';
					$lineaReporteF .= $alumnosActivos ? ', 0' : '';
					$lineaReporteF .= $alumnosEsperados ? ', 0' : '';
					$lineaReporteF .= $graduados ? ', 0' : '';
					$lineaReporteF .= $graduacionEsperada ? ', 0' : '';
					$lineaReporteF .= $desertores ? ', 0' : '';
					$lineaReporteF .= $pagosProgramados ? ', 0' : '';
					$lineaReporteF .= $pagosCobrados ? ', 0' : '';
					$lineaReporteF .= $cobranzaProgramada ? ', 0' : '';
					$lineaReporteF .= $cobranzaCobrada ? ', 0' : '';
				}
			}
			$lineaReporteF .= "\n\r";
		}

		return $headerReporte.$lineaReporteF;
	}
	/**
	 * Genera el reporte por grupo
	 * @param unknown_type $desde
	 * @param unknown_type $hasta
	 */
	private function reporteGrupo($desde, $hasta, $parametros, $arregloFechas, $muestra)
	{
		// Obtenmos los indicadores por dia
		// Indicadores
		$inscripcionEsperada = array_key_exists('inscripcion_esperada', $parametros) ? true : false;
		$preinscritos = array_key_exists('preinscritos', $parametros) ? true : false;
		$inscritos = array_key_exists('inscritos', $parametros) ? true : false;
		$inscritosUnicos = array_key_exists('inscritos_unicos', $parametros) ? true : false;
		$inscritosNuevos = array_key_exists('inscritos_nuevos', $parametros) ? true : false;
		$inscritosRecurrentes = array_key_exists('inscritos_recurrentes', $parametros) ? true : false;
		$actividadProgramda = array_key_exists('actividad_programada', $parametros) ? true : false;
		$alumnosActivos = array_key_exists('alumnos_activos', $parametros) ? true : false;
		$alumnosEsperados = array_key_exists('alumnos_esperados', $parametros) ? true : false;
		$graduados = array_key_exists('graduados', $parametros) ? true : false;
		$graduacionEsperada = array_key_exists('graduacion_esperada', $parametros) ? true : false;
		$desertores = array_key_exists('desertores', $parametros) ? true : false;
		$pagosProgramados = array_key_exists('pagos_programados', $parametros) ? true : false;
		$pagosCobrados = array_key_exists('pagos_cobrados', $parametros) ? true : false;
		$cobranzaProgramada = array_key_exists('cobranza_programada', $parametros) ? true : false;
		$cobranzaCobrada = array_key_exists('cobranza_cobrada', $parametros) ? true : false;
		$headerReporte = "Centro, Curso, Grupo";
		$arregloLineas = array();
		$flag = 0;
		foreach ($arregloFechas as $key => $anio)
		{
			foreach ($anio as $fecha)
			{
				$reporte = array();
				if($muestra == 'mes')
				{
					$headerReporte .= $inscripcionEsperada ? ', '.$fecha['mes'].' '.$key.' Inscripcion Esperada' : '';
					$headerReporte .= $preinscritos ? ', '.$fecha['mes'].' '.$key.' Alumnos Preinscritos' : '';
					$headerReporte .= $inscritos ? ', '.$fecha['mes'].' '.$key.' Alumnos Inscritos' : '';
					$headerReporte .= $inscritosUnicos ? ', '.$fecha['mes'].' '.$key.' Alumnos Inscritos Unicos' : '';
					$headerReporte .= $inscritosNuevos ? ', '.$fecha['mes'].' '.$key.' Alumnos Inscritos Nuevos' : '';
					$headerReporte .= $inscritosRecurrentes ? ', '.$fecha['mes'].' '.$key.' Alumnos Inscritos Recurrentes' : '';
					$headerReporte .= $actividadProgramda ? ', '.$fecha['mes'].' '.$key.' Alumnos Programados' : '';
					$headerReporte .= $alumnosActivos ? ', '.$fecha['mes'].' '.$key.' Alumnos Activos' : '';
					$headerReporte .= $alumnosEsperados ? ', '.$fecha['mes'].' '.$key.' Alumnos Esperados' : '';
					$headerReporte .= $graduados ? ', '.$fecha['mes'].' '.$key.' Alumnos Graduados' : '';
					$headerReporte .= $graduacionEsperada ? ', '.$fecha['mes'].' '.$key.' Graduacion Esperada' : '';
					$headerReporte .= $desertores ? ', '.$fecha['mes'].' '.$key.' Alumnos Desertores' : '';
					$headerReporte .= $pagosProgramados ? ', '.$fecha['mes'].' '.$key.'  Pagos Programados' : '';
					$headerReporte .= $pagosCobrados ? ', '.$fecha['mes'].' '.$key.'  Pagos Cobrados' : '';
					$headerReporte .= $cobranzaProgramada ? ', '.$fecha['mes'].' '.$key.'  Cobranza Programada' : '';
					$headerReporte .= $cobranzaCobrada ? ', '.$fecha['mes'].' '.$key.' Cobranza Cobrada' : '';
				}
				if($muestra == 'semana')
				{
					$headerReporte .= $inscripcionEsperada ? ', Semana '.$fecha.' '.$key.' Alumnos Preinscritos' : '';
					$headerReporte .= $preinscritos ? ', Semana '.$fecha.' '.$key.' Alumnos Preinscritos' : '';
					$headerReporte .= $inscritos ? ', Semana '.$fecha.' '.$key.' Alumnos Inscritos' : '';
					$headerReporte .= $inscritosUnicos ? ', Semana '.$fecha.' '.$key.' Alumnos Inscritos Unicos' : '';
					$headerReporte .= $inscritosNuevos ? ', Semana '.$fecha.' '.$key.' Alumnos Inscritos Nuevos' : '';
					$headerReporte .= $inscritosRecurrentes ? ', Semana '.$fecha.' '.$key.' Alumnos Inscritos Recurrentes' : '';
					$headerReporte .= $actividadProgramda ? ', Semana '.$fecha.' '.$key.' Alumnos Programados' : '';
					$headerReporte .= $alumnosActivos ? ', Semana '.$fecha.' '.$key.' Alumnos Activos' : '';
					$headerReporte .= $alumnosEsperados ? ', Semana '.$fecha.' '.$key.' Alumnos Esperados' : '';
					$headerReporte .= $graduados ? ', Semana '.$fecha.' '.$key.' Alumnos Graduados' : '';
					$headerReporte .= $graduacionEsperada ? ', Semana '.$fecha.' '.$key.' Graduacion Esperada' : '';
					$headerReporte .= $desertores ? ', Semana '.$fecha.' '.$key.' Alumnos Desertores' : '';
					$headerReporte .= $pagosProgramados ? ', Semana '.$fecha.' '.$key.'  Pagos Programados' : '';
					$headerReporte .= $pagosCobrados ? ', Semana '.$fecha.' '.$key.'  Pagos Cobrados' : '';
					$headerReporte .= $cobranzaProgramada ? ', Semana '.$fecha.' '.$key.'  Cobranza Programada' : '';
					$headerReporte .= $cobranzaCobrada ? ', Semana '.$fecha.' '.$key.' Cobranza Cobrada' : '';
				}
				if($inscripcionEsperada)
				{
					$sqlInscripcionEsperada = "SELECT g.id, sum(g.cupo_total) as esperada FROM grupo as g, horario as h WHERE g.horario_id = h.id ";
					if($muestra == "mes")
					{
						$sqlInscripcionEsperada .= "AND g.activo = true AND h.fecha_inicio BETWEEN '".$fecha['desde']." 00:00:00' AND '".$fecha['hasta']." 23:59:59'";
					}
					if($muestra == 'semana')
					{
						$sqlInscripcionEsperada .= "AND g.activo=true AND date_part('week', h.fecha_inicio) = ".$fecha. " AND date_part('year', h.fecha_inicio) = ".$key;
					}
					$sqlInscripcionEsperada .= " GROUP BY g.id";
					$connection = Propel::getConnection(ReporteIndicadoresDiaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
					$statement = $connection->prepare($sqlInscripcionEsperada);
					$statement->execute();

					while($linea = $statement->fetch(PDO::FETCH_ASSOC))
					{
						$reporte[$linea['id']]['inscripcion_esperada'] += $linea['esperada'];
					}
				}
				if($alumnosEsperados)
				{
					$sqlAlumnosEsperados = "SELECT g.id, sum(g.cupo_total) as esperada FROM grupo as g, horario as h WHERE g.horario_id = h.id ";
					if($muestra == "mes")
					{
						$sqlAlumnosEsperados .= "AND g.activo = true AND (h.fecha_inicio, h.fecha_fin) OVERLAPS (DATE '".$fecha['desde']."', DATE '".$fecha['hasta']."') = true";
					}
					if($muestra == 'semana')
					{
						$fechasSemana = $this->diasSemana($fecha, $key);
						$sqlAlumnosEsperados .= "AND g.activo=true AND (h.fecha_inicio, h.fecha_fin) OVERLAPS (DATE '".$fechasSemana['desde']."', DATE '".$fechasSemana['hasta']."') = true";
					}
					$sqlAlumnosEsperados .= " GROUP BY g.id";
					$connection = Propel::getConnection(ReporteIndicadoresDiaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
					$statement = $connection->prepare($sqlAlumnosEsperados);
					$statement->execute();

					while($linea = $statement->fetch(PDO::FETCH_ASSOC))
					{
						$reporte[$linea['id']]['alumnos_esperados'] += $linea['esperada'];
					}
				}
				// Revisamos si se necesitan la Alumnos Programados
				if($actividadProgramda)
				{
					$sqlActividad = "SELECT r.grupo_id, s.socio_id as activos FROM reporte_indicadores_dia_grupo as r, reporte_indicadores_dia_grupo_alumnos_actividad_programada as s WHERE r.id = s.reporte_indicador_dia_grupo_id ";
					if($muestra == 'mes')
					{
						$sqlActividad .= "AND r.fecha BETWEEN '".$fecha['desde']."' AND '".$fecha['hasta']."' ";
					}
					if ($muestra == 'semana')
					{
						$sqlActividad .= "AND r.semana = ".$fecha." AND r.anio=".$key;;
					}
					$sqlActividad .= " GROUP BY r.grupo_id, s.socio_id ORDER BY grupo_id	";
					$connection = Propel::getConnection(ReporteIndicadoresDiaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
					$statement = $connection->prepare($sqlActividad);
					$statement->execute();
					while($linea = $statement->fetch(PDO::FETCH_ASSOC))
					{
						$reporte[(string)$linea['grupo_id']]['actividad_programada'][] = $linea['activos'];
					}
				}
				// Revisamos si se necesitan los alumnos activos
				if($alumnosActivos)
				{
					$sqlSociosActivos = "SELECT r.grupo_id, s.socio_id as activos FROM reporte_indicadores_dia_grupo as r, reporte_indicadores_dia_grupo_alumnos_activos as s WHERE r.id = s.reporte_indicador_dia_grupo_id ";
					if($muestra == 'mes')
					{
						$sqlSociosActivos .= "AND r.fecha BETWEEN '".$fecha['desde']."' AND '".$fecha['hasta']."' ";
					}
					if ($muestra == 'semana')
					{
						$sqlSociosActivos .= "AND r.semana = ".$fecha." AND r.anio=".$key;;
					}
					$sqlSociosActivos .= " GROUP BY r.grupo_id, s.socio_id ORDER BY r.grupo_id";
					$connection = Propel::getConnection(ReporteIndicadoresDiaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
					$statement = $connection->prepare($sqlSociosActivos);
					$statement->execute();
					while($linea = $statement->fetch(PDO::FETCH_ASSOC))
					{
						$reporte[$linea['grupo_id']]['alumnos_activos'] += 1;
					}
				}
				if($preinscritos || $inscritos || $inscritosNuevos || $inscritosRecurrentes  || $graduados ||
						$desertores || $pagosProgramados || $pagosCobrados || $cobranzaProgramada || $cobranzaCobrada
				)
				{
					$sqlIndicadoresDiaGrupo = "SELECT grupo_id ";
					$sqlIndicadoresDiaGrupo .= ( $preinscritos ? ",sum(socios_preinscritos) as preinscritos" : "");
					$sqlIndicadoresDiaGrupo .= ( $inscritos ? ",sum(socios_inscritos) as inscritos" : "");
					$sqlIndicadoresDiaGrupo .=	( $inscritosNuevos ?",sum(socios_inscritos_nuevos) as inscritos_nuevos" : "");
					$sqlIndicadoresDiaGrupo .=	( $inscritosRecurrentes ? ",sum(socios_inscritos_recurrentes) as inscritos_recurrentes" : "");
					//$sqlIndicadoresDiaGrupo .=	( $alumnosActivos ? ",sum(alumnos_activos) as alumnos_activos" : "");
					$sqlIndicadoresDiaGrupo .=	( $graduados ? ",sum(alumnos_graduados) as alumnos_graduados" : "");
					$sqlIndicadoresDiaGrupo .=	( $graduacionEsperada ? ",sum(graduacion_esperada) as graduacion_esperada" : "");
					$sqlIndicadoresDiaGrupo .=	( $desertores ? ",sum(alumnos_desertores) as alumnos_desertores" : "");
					$sqlIndicadoresDiaGrupo .=	( $pagosProgramados ? ",sum(pagos_programados) as pagos_programados" : "");
					$sqlIndicadoresDiaGrupo .=	( $pagosCobrados ? ",sum(pagos_cobrados) as pagos_cobrados" : "");
					$sqlIndicadoresDiaGrupo .=	( $cobranzaProgramada ? ",sum(cobranza_programada) as cobranza_programada" : "");
					$sqlIndicadoresDiaGrupo .=	( $cobranzaCobrada ? ",sum(cobranza_cobrada) as cobranza_cobrada" : "");
					$sqlIndicadoresDiaGrupo .=	" FROM reporte_indicadores_dia_grupo WHERE ";
					if($muestra == 'mes')
					{
						$sqlIndicadoresDiaGrupo .= "fecha BETWEEN '".$fecha['desde']."' AND '".$fecha['hasta']."' ";
					}
					if ($muestra == 'semana')
					{
						$sqlIndicadoresDiaGrupo .= "semana = ".$fecha." AND anio=".$key;;
					}
					$sqlIndicadoresDiaGrupo .= " GROUP BY grupo_id";
					$connection = Propel::getConnection(ReporteIndicadoresDiaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
					$statement = $connection->prepare($sqlIndicadoresDiaGrupo);
					$statement->execute();
					while($linea = $statement->fetch(PDO::FETCH_ASSOC))
					{
						$reporte[$linea['grupo_id']]['preinscritos'] = $preinscritos ? $linea['preinscritos'] : '';
						$reporte[$linea['grupo_id']]['inscritos'] = $inscritos ? $linea['inscritos'] : '';
						$reporte[$linea['grupo_id']]['inscritos_nuevos'] = $inscritosNuevos ? $linea['inscritos_nuevos'] : '';
						$reporte[$linea['grupo_id']]['inscritos_recurrentes'] = $inscritosRecurrentes ? $linea['inscritos_recurrentes'] : '';
						//$reporte[$linea['grupo_id']]['alumnos_activos'] = $alumnosActivos ? $linea['alumnos_activos'] : '';
						$reporte[$linea['grupo_id']]['alumnos_graduados'] = $graduados ? $linea['alumnos_graduados'] : '';
						$reporte[$linea['grupo_id']]['graduacion_esperada'] = $graduacionEsperada ? $linea['graduacion_esperada'] : '';
						$reporte[$linea['grupo_id']]['alumnos_desertores'] = $desertores ? $linea['alumnos_desertores'] : '';
						$reporte[$linea['grupo_id']]['pagos_programados'] = $pagosProgramados ? $linea['pagos_programados'] : '';
						$reporte[$linea['grupo_id']]['pagos_cobrados'] = $pagosCobrados ? $linea['pagos_cobrados'] : '';
						$reporte[$linea['grupo_id']]['cobranza_programada'] = $cobranzaProgramada ? $linea['cobranza_programada'] : '';
						$reporte[$linea['grupo_id']]['cobranza_cobrada'] = $cobranzaCobrada ? $linea['cobranza_cobrada'] : '';
					}
				}
				foreach ($reporte as $keyR => $lineaR)
				{
					$lineaReporte = '';
					$lineaReporte .= $inscripcionEsperada ? ', '.(isset($lineaR['inscripcion_esperada']) ? $lineaR['inscripcion_esperada'] : '0' ) : '';
					$lineaReporte .= $preinscritos ? ', '.(isset($lineaR['preinscritos']) ? $lineaR['preinscritos'] : '0' ) : '';
					$lineaReporte .= $inscritos ? ', '.(isset($lineaR['inscritos']) ? $lineaR['inscritos'] : '0' ) : '';
					$lineaReporte .= $inscritosUnicos ? ', '.(isset($lineaR['inscritos_unicos']) ? $lineaR['inscritos_unicos'] : '0' ) : '';
					$lineaReporte .= $inscritosNuevos ? ', '.(isset($lineaR['inscritos_nuevos']) ? $lineaR['inscritos_nuevos'] : '0' ) : '';
					$lineaReporte .= $inscritosRecurrentes ? ', '.(isset($lineaR['inscritos_recurrentes']) ? $lineaR['inscritos_recurrentes'] : '0' ) : '';
					$lineaReporte .= $actividadProgramda ? ', '.(isset($lineaR['actividad_programada']) ? count($lineaR['actividad_programada']) : '0' ) : '';
					$lineaReporte .= $alumnosActivos ? ', '.(isset($lineaR['alumnos_activos']) ? $lineaR['alumnos_activos'] : '0' ) : '';
					$lineaReporte .= $alumnosEsperados ? ', '.(isset($lineaR['alumnos_esperados']) ? $lineaR['alumnos_esperados'] : '0' ) : '';
					$lineaReporte .= $graduados ? ', '.(isset($lineaR['alumnos_graduados']) ? $lineaR['alumnos_graduados'] : '0' ) : '';
					$lineaReporte .= $graduacionEsperada ? ', '.(isset($lineaR['graduacion_esperada']) ? $lineaR['graduacion_esperada'] : '0' ) : '';
					$lineaReporte .= $desertores ? ', '.(isset($lineaR['alumnos_desertores']) ? $lineaR['alumnos_desertores'] : '0' ) : '';
					$lineaReporte .= $pagosProgramados ? ', '.(isset($lineaR['pagos_programados']) ? $lineaR['pagos_programados'] : '0' ) : '';
					$lineaReporte .= $pagosCobrados ? ', '.(isset($lineaR['pagos_cobrados']) ? $lineaR['pagos_cobrados'] : '0' ) : '';
					$lineaReporte .= $cobranzaProgramada ? ', '.(isset($lineaR['cobranza_programada']) ? $lineaR['cobranza_programada'] : '0' ) : '';
					$lineaReporte .= $cobranzaCobrada ? ', '.(isset($lineaR['cobranza_cobrada']) ? $lineaR['cobranza_cobrada'] : '0' ) : '';
					$arregloLineas[$keyR][$flag] = $lineaReporte;
				}
				$flag++;
			}
		}
		$headerReporte .= "\n\r";
		$lineaReporteF = '';
		foreach ($arregloLineas as $keyL => $grupo)
		{
			$grupoL = GrupoPeer::retrieveByPK($keyL);
			if($grupoL)
			{
				$lineaReporteF .= $grupoL->getCentro()->getAlias().','.$grupoL->getCurso()->getNombre().', '.$grupoL->getClave();
				for ($i = 0; $i < $flag; $i++)
				{
					if(array_key_exists($i, $grupo))
					{
						$lineaReporteF .= $grupo[$i];
					}
					else
					{
						$lineaReporteF .= $inscripcionEsperada ? ', 0' : '';
						$lineaReporteF .= $preinscritos ? ', 0' : '';
						$lineaReporteF .= $inscritos ? ', 0' : '';
						$lineaReporteF .= $inscritosUnicos ? ', 0' : '';
						$lineaReporteF .= $inscritosNuevos ? ', 0' : '';
						$lineaReporteF .= $inscritosRecurrentes ? ', 0' : '';
						$lineaReporteF .= $alumnosActivos ? ', 0' : '';
						$lineaReporteF .= $actividadProgramda ? ', 0' : '';
						$lineaReporteF .= $alumnosEsperados ? ', 0' : '';
						$lineaReporteF .= $graduados ? ', 0' : '';
						$lineaReporteF .= $graduacionEsperada ? ', 0' : '';
						$lineaReporteF .= $desertores ? ', 0' : '';
						$lineaReporteF .= $pagosProgramados ? ', 0' : '';
						$lineaReporteF .= $pagosCobrados ? ', 0' : '';
						$lineaReporteF .= $cobranzaProgramada ? ', 0' : '';
						$lineaReporteF .= $cobranzaCobrada ? ', 0' : '';
					}
				}
				$lineaReporteF .= "\n\r";
			}
		}

		return $headerReporte.$lineaReporteF;
	}
	/**
	 * Genera el reporte por facilitador
	 * @param unknown_type $desde
	 * @param unknown_type $hasta
	 */
	private function reporteFacilitador($desde, $hasta, $parametros, $arregloFechas, $muestra)
	{
		// Obtenmos los indicadores por dia
		// Indicadores
		$inscripcionEsperada = array_key_exists('inscripcion_esperada', $parametros) ? true : false;
		$preinscritos = array_key_exists('preinscritos', $parametros) ? true : false;
		$inscritos = array_key_exists('inscritos', $parametros) ? true : false;
		$inscritosUnicos = array_key_exists('inscritos_unicos', $parametros) ? true : false;
		$inscritosNuevos = array_key_exists('inscritos_nuevos', $parametros) ? true : false;
		$inscritosRecurrentes = array_key_exists('inscritos_recurrentes', $parametros) ? true : false;
		$actividadProgramada = array_key_exists('actividad_programada', $parametros) ? true : false;
		$alumnosActivos = array_key_exists('alumnos_activos', $parametros) ? true : false;
		$alumnosEsperados = array_key_exists('alumnos_esperados', $parametros) ? true : false;
		$graduados = array_key_exists('graduados', $parametros) ? true : false;
		$graduacionEsperada = array_key_exists('graduacion_esperada', $parametros) ? true : false;
		$desertores = array_key_exists('desertores', $parametros) ? true : false;
		$pagosProgramados = array_key_exists('pagos_programados', $parametros) ? true : false;
		$pagosCobrados = array_key_exists('pagos_cobrados', $parametros) ? true : false;
		$cobranzaProgramada = array_key_exists('cobranza_programada', $parametros) ? true : false;
		$cobranzaCobrada = array_key_exists('cobranza_cobrada', $parametros) ? true : false;
		$headerReporte = "Facilitador, Perfil";
		$arregloLineas = array();
		$flag = 0;
		foreach ($arregloFechas as $key => $anio)
		{
			foreach ($anio as $fecha)
			{
				$reporte = array();
				if($muestra == 'mes')
				{
					$headerReporte .= $inscripcionEsperada ? ', '.$fecha['mes'].' '.$key.' Inscripcion Esperada' : '';
					$headerReporte .= $preinscritos ? ', '.$fecha['mes'].' '.$key.' Alumnos Preinscritos' : '';
					$headerReporte .= $inscritos ? ', '.$fecha['mes'].' '.$key.' Alumnos Inscritos' : '';
					$headerReporte .= $inscritosUnicos ? ', '.$fecha['mes'].' '.$key.' Alumnos Inscritos Unicos' : '';
					$headerReporte .= $inscritosNuevos ? ', '.$fecha['mes'].' '.$key.' Alumnos Inscritos Nuevos' : '';
					$headerReporte .= $inscritosRecurrentes ? ', '.$fecha['mes'].' '.$key.' Alumnos Inscritos Recurrentes' : '';
					$headerReporte .= $actividadProgramada ? ', '.$fecha['mes'].' '.$key.' Alumnos Programados' : '';
					$headerReporte .= $alumnosActivos ? ', '.$fecha['mes'].' '.$key.' Alumnos Activos' : '';
					$headerReporte .= $alumnosEsperados ? ', '.$fecha['mes'].' '.$key.' Alumnos Esperados' : '';
					$headerReporte .= $graduados ? ', '.$fecha['mes'].' '.$key.' Alumnos Graduados' : '';
					$headerReporte .= $graduacionEsperada ? ', '.$fecha['mes'].' '.$key.' Graduacion Esperada' : '';
					$headerReporte .= $desertores ? ', '.$fecha['mes'].' '.$key.' Alumnos Desertores' : '';
					$headerReporte .= $pagosProgramados ? ', '.$fecha['mes'].' '.$key.'  Pagos Programados' : '';
					$headerReporte .= $pagosCobrados ? ', '.$fecha['mes'].' '.$key.'  Pagos Cobrados' : '';
					$headerReporte .= $cobranzaProgramada ? ', '.$fecha['mes'].' '.$key.'  Cobranza Programada' : '';
					$headerReporte .= $cobranzaCobrada ? ', '.$fecha['mes'].' '.$key.' Cobranza Cobrada' : '';
				}
				if($muestra == 'semana')
				{
					$headerReporte .= $inscripcionEsperada ? ', Semana '.$fecha.' '.$key.' Inscripcion Esperada' : '';
					$headerReporte .= $preinscritos ? ', Semana '.$fecha.' '.$key.' Alumnos Preinscritos' : '';
					$headerReporte .= $inscritos ? ', Semana '.$fecha.' '.$key.' Alumnos Inscritos' : '';
					$headerReporte .= $inscritosUnicos ? ', Semana '.$fecha.' '.$key.' Alumnos Inscritos Unicos' : '';
					$headerReporte .= $inscritosNuevos ? ', Semana '.$fecha.' '.$key.' Alumnos Inscritos Nuevos' : '';
					$headerReporte .= $inscritosRecurrentes ? ', Semana '.$fecha.' '.$key.' Alumnos Inscritos Recurrentes' : '';
					$headerReporte .= $actividadProgramada ? ', Semana '.$fecha.' '.$key.' Alumnos Programados' : '';
					$headerReporte .= $alumnosActivos ? ', Semana '.$fecha.' '.$key.' Alumnos Activos' : '';
					$headerReporte .= $alumnosEsperados ? ', Semana '.$fecha.' '.$key.' Alumnos Esperados' : '';
					$headerReporte .= $graduados ? ', Semana '.$fecha.' '.$key.' Alumnos Graduados' : '';
					$headerReporte .= $graduacionEsperada ? ', Semana '.$fecha.' '.$key.' Graduacion Esperada' : '';
					$headerReporte .= $desertores ? ', Semana '.$fecha.' '.$key.' Alumnos Desertores' : '';
					$headerReporte .= $pagosProgramados ? ', Semana '.$fecha.' '.$key.'  Pagos Programados' : '';
					$headerReporte .= $pagosCobrados ? ', Semana '.$fecha.' '.$key.'  Pagos Cobrados' : '';
					$headerReporte .= $cobranzaProgramada ? ', Semana '.$fecha.' '.$key.'  Cobranza Programada' : '';
					$headerReporte .= $cobranzaCobrada ? ', Semana '.$fecha.' '.$key.' Cobranza Cobrada' : '';
				}

				if($inscripcionEsperada)
				{
					$sqlInscripcionEsperada = "SELECT g.facilitador_id, sum(g.cupo_total) as esperada FROM grupo as g, horario as h WHERE g.horario_id = h.id ";
					if($muestra == "mes")
					{
						$sqlInscripcionEsperada .= "AND g.activo=true AND h.fecha_inicio BETWEEN '".$fecha['desde']." 00:00:00' AND '".$fecha['hasta']." 23:59:59'";
					}
					if($muestra == 'semana')
					{
						$sqlInscripcionEsperada .= "AND g.activo=true AND date_part('week', h.fecha_inicio) = ".$fecha. " AND date_part('year', h.fecha_inicio) = ".$key;
					}
					$sqlInscripcionEsperada .= " GROUP BY g.facilitador_id";
					error_log($sqlInscripcionEsperada);
					$connection = Propel::getConnection(ReporteIndicadoresDiaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
					$statement = $connection->prepare($sqlInscripcionEsperada);
					$statement->execute();

					while($linea = $statement->fetch(PDO::FETCH_ASSOC))
					{
						$reporte[$linea['facilitador_id']]['inscripcion_esperada'] += $linea['esperada'];
					}
						
				}
				if($alumnosEsperados)
				{
					$sqlAlumnosEsperados = "SELECT g.facilitador_id, sum(g.cupo_total) as esperada FROM grupo as g, horario as h WHERE g.horario_id = h.id ";
					if($muestra == "mes")
					{
						$sqlAlumnosEsperados .= "AND g.activo=true AND (h.fecha_inicio, h.fecha_fin) OVERLAPS (DATE '".$fecha['desde']."', DATE '".$fecha['hasta']."') = true";
					}
					if($muestra == 'semana')
					{
						$fechasSemana = $this->diasSemana($fecha, $key);
						$sqlAlumnosEsperados .= "AND g.activo=true AND (h.fecha_inicio, h.fecha_fin) OVERLAPS (DATE '".$fechasSemana['desde']."', DATE '".$fechasSemana['hasta']."') = true";
					}
					$sqlAlumnosEsperados .= " GROUP BY g.facilitador_id";
					error_log($sqlAlumnosEsperados);
					$connection = Propel::getConnection(ReporteIndicadoresDiaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
					$statement = $connection->prepare($sqlAlumnosEsperados);
					$statement->execute();

					while($linea = $statement->fetch(PDO::FETCH_ASSOC))
					{
						$reporte[$linea['facilitador_id']]['alumnos_esperados'] += $linea['esperada'];
					}

				}
				if($actividadProgramada)
				{
					$sqlActividad = "SELECT r.facilitador_id, r.grupo_id, s.socio_id as activos FROM reporte_indicadores_dia_grupo as r, reporte_indicadores_dia_grupo_alumnos_actividad_programada as s WHERE r.id = s.reporte_indicador_dia_grupo_id ";
					if($muestra == 'mes')
					{
						$sqlActividad .= "AND r.fecha BETWEEN '".$fecha['desde']."' AND '".$fecha['hasta']."' ";
					}
					if ($muestra == 'semana')
					{
						$sqlActividad .= "AND r.semana = ".$fecha." AND r.anio=".$key;;
					}
					$sqlActividad .= " GROUP BY r.facilitador_id, r.grupo_id, s.socio_id";
					$connection = Propel::getConnection(ReporteIndicadoresDiaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
					$statement = $connection->prepare($sqlActividad);
					$statement->execute();
					while($linea = $statement->fetch(PDO::FETCH_ASSOC))
					{
						$reporte[$linea['facilitador_id']]['actividad_programada'] += 1;
					}
				}
				// Revisamos si se necesitan los alumnos activos
				if($alumnosActivos)
				{
					$sqlSociosActivos = "SELECT r.facilitador_id, r.grupo_id, s.socio_id as activos FROM reporte_indicadores_dia_grupo as r, reporte_indicadores_dia_grupo_alumnos_activos as s WHERE r.id = s.reporte_indicador_dia_grupo_id ";
					if($muestra == 'mes')
					{
						$sqlSociosActivos .= "AND r.fecha BETWEEN '".$fecha['desde']."' AND '".$fecha['hasta']."' ";
					}
					if ($muestra == 'semana')
					{
						$sqlSociosActivos .= "AND r.semana = ".$fecha." AND r.anio=".$key;;
					}
					$sqlSociosActivos .= " GROUP BY r.facilitador_id, r.grupo_id, s.socio_id";
					$connection = Propel::getConnection(ReporteIndicadoresDiaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
					$statement = $connection->prepare($sqlSociosActivos);
					$statement->execute();
					while($linea = $statement->fetch(PDO::FETCH_ASSOC))
					{
						$reporte[$linea['facilitador_id']]['alumnos_activos'] += 1;
					}
				}
				if($preinscritos || $inscritos || $inscritosNuevos || $inscritosRecurrentes || $graduados ||
						$desertores || $pagosProgramados || $pagosCobrados || $cobranzaProgramada || $cobranzaCobrada
				)
				{
					$sqlIndicadoresDiaGrupo = "SELECT facilitador_id ";
					$sqlIndicadoresDiaGrupo .= ( $preinscritos ? ",sum(socios_preinscritos) as preinscritos" : "");
					$sqlIndicadoresDiaGrupo .= ( $inscritos ? ",sum(socios_inscritos) as inscritos" : "");
					$sqlIndicadoresDiaGrupo .=	( $inscritosNuevos ?",sum(socios_inscritos_nuevos) as inscritos_nuevos" : "");
					$sqlIndicadoresDiaGrupo .=	( $inscritosRecurrentes ? ",sum(socios_inscritos_recurrentes) as inscritos_recurrentes" : "");
					//$sqlIndicadoresDiaGrupo .=	( $alumnosActivos ? ",sum(alumnos_activos) as alumnos_activos" : "");
					$sqlIndicadoresDiaGrupo .=	( $graduados ? ",sum(alumnos_graduados) as alumnos_graduados" : "");
					$sqlIndicadoresDiaGrupo .=	( $graduacionEsperada ? ",sum(graduacion_esperada) as graduacion_esperada" : "");
					$sqlIndicadoresDiaGrupo .=	( $desertores ? ",sum(alumnos_desertores) as alumnos_desertores" : "");
					$sqlIndicadoresDiaGrupo .=	( $pagosProgramados ? ",sum(pagos_programados) as pagos_programados" : "");
					$sqlIndicadoresDiaGrupo .=	( $pagosCobrados ? ",sum(pagos_cobrados) as pagos_cobrados" : "");
					$sqlIndicadoresDiaGrupo .=	( $cobranzaProgramada ? ",sum(cobranza_programada) as cobranza_programada" : "");
					$sqlIndicadoresDiaGrupo .=	( $cobranzaCobrada ? ",sum(cobranza_cobrada) as cobranza_cobrada" : "");
					$sqlIndicadoresDiaGrupo .=	" FROM reporte_indicadores_dia_grupo WHERE ";
					if($muestra == 'mes')
					{
						$sqlIndicadoresDiaGrupo .= "fecha BETWEEN '".$fecha['desde']."' AND '".$fecha['hasta']."' ";
					}
					if ($muestra == 'semana')
					{
						$sqlIndicadoresDiaGrupo .= "semana = ".$fecha." AND anio=".$key;;
					}
					$sqlIndicadoresDiaGrupo .= " GROUP BY facilitador_id";
					$connection = Propel::getConnection(ReporteIndicadoresDiaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
					$statement = $connection->prepare($sqlIndicadoresDiaGrupo);
					$statement->execute();
					while($linea = $statement->fetch(PDO::FETCH_ASSOC))
					{
						$reporte[$linea['facilitador_id']]['preinscritos'] = $preinscritos ? $linea['preinscritos'] : '';
						$reporte[$linea['facilitador_id']]['inscritos'] = $inscritos ? $linea['inscritos'] : '';
						$reporte[$linea['facilitador_id']]['inscritos_nuevos'] = $inscritosNuevos ? $linea['inscritos_nuevos'] : '';
						$reporte[$linea['facilitador_id']]['inscritos_recurrentes'] = $inscritosRecurrentes ? $linea['inscritos_recurrentes'] : '';
						//$reporte[$linea['facilitador_id']]['alumnos_activos'] = $alumnosActivos ? $linea['alumnos_activos'] : '';
						$reporte[$linea['facilitador_id']]['alumnos_graduados'] = $graduados ? $linea['alumnos_graduados'] : '';
						$reporte[$linea['facilitador_id']]['graduacion_esperada'] = $graduacionEsperada ? $linea['graduacion_esperada'] : '';
						$reporte[$linea['facilitador_id']]['alumnos_desertores'] = $desertores ? $linea['alumnos_desertores'] : '';
						$reporte[$linea['facilitador_id']]['pagos_programados'] = $pagosProgramados ? $linea['pagos_programados'] : '';
						$reporte[$linea['facilitador_id']]['pagos_cobrados'] = $pagosCobrados ? $linea['pagos_cobrados'] : '';
						$reporte[$linea['facilitador_id']]['cobranza_programada'] = $cobranzaProgramada ? $linea['cobranza_programada'] : '';
						$reporte[$linea['facilitador_id']]['cobranza_cobrada'] = $cobranzaCobrada ? $linea['cobranza_cobrada'] : '';
					}
				}
				foreach ($reporte as $keyR => $lineaR)
				{
					$lineaReporte = '';
					$lineaReporte .= $inscripcionEsperada ? ', '.(isset($lineaR['inscripcion_esperada']) ? $lineaR['inscripcion_esperada'] : '0' ) : '';
					$lineaReporte .= $preinscritos ? ', '.(isset($lineaR['preinscritos']) ? $lineaR['preinscritos'] : '0' ) : '';
					$lineaReporte .= $inscritos ? ', '.(isset($lineaR['inscritos']) ? $lineaR['inscritos'] : '0' ) : '';
					$lineaReporte .= $inscritosUnicos ? ', '.(isset($lineaR['inscritos_unicos']) ? $lineaR['inscritos_unicos'] : '0' ) : '';
					$lineaReporte .= $inscritosNuevos ? ', '.(isset($lineaR['inscritos_nuevos']) ? $lineaR['inscritos_nuevos'] : '0' ) : '';
					$lineaReporte .= $inscritosRecurrentes ? ', '.(isset($lineaR['inscritos_recurrentes']) ? $lineaR['inscritos_recurrentes'] : '0' ) : '';
					$lineaReporte .= $actividadProgramada ? ', '.(isset($lineaR['actividad_programada']) ? $lineaR['actividad_programada'] : '0' ) : '';
					$lineaReporte .= $alumnosActivos ? ', '.(isset($lineaR['alumnos_activos']) ? $lineaR['alumnos_activos'] : '0' ) : '';
					$lineaReporte .= $alumnosEsperados ? ', '.(isset($lineaR['alumnos_esperados']) ? $lineaR['alumnos_esperados'] : '0' ) : '';
					$lineaReporte .= $graduados ? ', '.(isset($lineaR['alumnos_graduados']) ? $lineaR['alumnos_graduados'] : '0' ) : '';
					$lineaReporte .= $graduacionEsperada ? ', '.(isset($lineaR['graduacion_esperada']) ? $lineaR['graduacion_esperada'] : '0' ) : '';
					$lineaReporte .= $desertores ? ', '.(isset($lineaR['alumnos_desertores']) ? $lineaR['alumnos_desertores'] : '0' ) : '';
					$lineaReporte .= $pagosProgramados ? ', '.(isset($lineaR['pagos_programados']) ? $lineaR['pagos_programados'] : '0' ) : '';
					$lineaReporte .= $pagosCobrados ? ', '.(isset($lineaR['pagos_cobrados']) ? $lineaR['pagos_cobrados'] : '0' ) : '';
					$lineaReporte .= $cobranzaProgramada ? ', '.(isset($lineaR['cobranza_programada']) ? $lineaR['cobranza_programada'] : '0' ) : '';
					$lineaReporte .= $cobranzaCobrada ? ', '.(isset($lineaR['cobranza_cobrada']) ? $lineaR['cobranza_cobrada'] : '0' ) : '';
					$arregloLineas[$keyR][$flag] = $lineaReporte;
				}
				$flag++;
			}
		}
		$headerReporte .= "\n\r";
		$lineaReporteF = '';
		foreach ($arregloLineas as $keyL => $facilidor)
		{
			error_log($keyL);
			if($keyL)
			{
				$usuarioL = UsuarioPeer::retrieveByPK($keyL);
				$perfiles = '';
				foreach ($usuarioL->getUsuarioPerfils() as $perfil)
				{
					$perfiles .= $perfil->getPerfilFacilitador()->getNombre().' ';
				}
				$lineaReporteF .= $usuarioL->getNombreCompleto().','.$perfiles;
				for ($i = 0; $i < $flag; $i++)
				{
					if(array_key_exists($i, $facilidor))
					{
						$lineaReporteF .= $facilidor[$i];
					}
					else
					{
						$lineaReporteF .= $inscripcionEsperada ? ', 0' : '';
						$lineaReporteF .= $preinscritos ? ', 0' : '';
						$lineaReporteF .= $inscritos ? ', 0' : '';
						$lineaReporteF .= $inscritosUnicos ? ', 0' : '';
						$lineaReporteF .= $inscritosNuevos ? ', 0' : '';
						$lineaReporteF .= $inscritosRecurrentes ? ', 0' : '';
						$lineaReporteF .= $actividadProgramada ? ', 0' : '';
						$lineaReporteF .= $alumnosActivos ? ', 0' : '';
						$lineaReporteF .= $alumnosEsperados ? ', 0' : '';
						$lineaReporteF .= $graduados ? ', 0' : '';
						$lineaReporteF .= $graduacionEsperada ? ', 0' : '';
						$lineaReporteF .= $desertores ? ', 0' : '';
						$lineaReporteF .= $pagosProgramados ? ', 0' : '';
						$lineaReporteF .= $pagosCobrados ? ', 0' : '';
						$lineaReporteF .= $cobranzaProgramada ? ', 0' : '';
						$lineaReporteF .= $cobranzaCobrada ? ', 0' : '';
					}
				}
				$lineaReporteF .= "\n\r";
			}
		}
		return $headerReporte.$lineaReporteF	;
	}
	/**
	 * Genera el reporte por centro
	 * @param unknown_type $desde
	 * @param unknown_type $hasta
	 */
	private function reporteRIA($desde, $hasta, $parametros, $arregloFechas, $muestra)
	{

		// Obtenmos los indicadores por dia
		// Indicadores
		$registrados = array_key_exists('registrados', $parametros) ? true : false;
		$activos = array_key_exists('activos', $parametros) ? true : false;
		$activosNuevos = array_key_exists('activos_nuevos', $parametros) ? true : false;
		$nuevos = array_key_exists('nuevos', $parametros) ? true : false;
		$cobranzaInternet = array_key_exists('cobranza_internet', $parametros) ? true : false;
		$cobranzaEducacion = array_key_exists('cobranza_educacion', $parametros) ? true : false;
		$cobranzaOtros = array_key_exists('cobranza_otros', $parametros) ? true : false;
		$cobranzaTotal = array_key_exists('cobranza_total', $parametros) ? true : false;
		$inscripcionEsperada = array_key_exists('inscripcion_esperada', $parametros) ? true : false;
		$preinscritos = array_key_exists('preinscritos', $parametros) ? true : false;
		$inscritos = array_key_exists('inscritos', $parametros) ? true : false;
		$inscritosUnicos = array_key_exists('inscritos_unicos', $parametros) ? true : false;
		$inscritosNuevos = array_key_exists('inscritos_nuevos', $parametros) ? true : false;
		$inscritosRecurrentes = array_key_exists('inscritos_recurrentes', $parametros) ? true : false;
		$actividadProgramada = array_key_exists('actividad_programada', $parametros) ? true : false;
		$alumnosActivos = array_key_exists('alumnos_activos', $parametros) ? true : false;
		$alumnosEsperados = array_key_exists('alumnos_esperados', $parametros) ? true : false;
		$graduados = array_key_exists('graduados', $parametros) ? true : false;
		$graduacionEsperada = array_key_exists('graduacion_esperada', $parametros) ? true : false;
		$desertores = array_key_exists('desertores', $parametros) ? true : false;
		$pagosProgramados = array_key_exists('pagos_programados', $parametros) ? true : false;
		$pagosCobrados = array_key_exists('pagos_cobrados', $parametros) ? true : false;
		$cobranzaProgramada = array_key_exists('cobranza_programada', $parametros) ? true : false;
		$cobranzaCobrada = array_key_exists('cobranza_cobrada', $parametros) ? true : false;
		$arregloLineas = array();
		$lineaReporte = '';
		//
		foreach ($arregloFechas as $key => $anio)
		{
			foreach ($anio as $fecha)
			{
				$reporte = array();
				if($muestra == 'mes')
				{
					$headerReporte .= $registrados ? ','.$fecha['mes'].' '.$key.' Socios Registrados' : '';
					$headerReporte .= $activos ? ', '.$fecha['mes'].' '.$key.' Socios Activos' : '';
					$headerReporte .= $activosNuevos ? ', '.$fecha['mes'].' '.$key.' Socios Activos Nuevos' : '';
					$headerReporte .= $nuevos ? ', '.$fecha['mes'].' '.$key.' Registros Nuevos' : '';
					$headerReporte .= $cobranzaInternet ? ', '.$fecha['mes'].' '.$key.' Cobranza Internet' : '';
					$headerReporte .= $cobranzaEducacion ? ', '.$fecha['mes'].' '.$key.' Cobranza Educacion' : '';
					$headerReporte .= $cobranzaOtros ? ', '.$fecha['mes'].' '.$key.' Cobranza Otros' : '';
					$headerReporte .= $cobranzaTotal ? ', '.$fecha['mes'].' '.$key.' Cobranza Total' : '';
					$headerReporte .= $inscripcionEsperada ? ', '.$fecha['mes'].' '.$key.' Inscripcion Esperada' : '';
					$headerReporte .= $preinscritos ? ', '.$fecha['mes'].' '.$key.' Alumnos Preinscritos' : '';
					$headerReporte .= $inscritos ? ', '.$fecha['mes'].' '.$key.' Alumnos Inscritos' : '';
					$headerReporte .= $inscritosUnicos ? ', '.$fecha['mes'].' '.$key.' Alumnos Inscritos Unicos' : '';
					$headerReporte .= $inscritosNuevos ? ', '.$fecha['mes'].' '.$key.' Alumnos Inscritos Nuevos' : '';
					$headerReporte .= $inscritosRecurrentes ? ', '.$fecha['mes'].' '.$key.' Alumnos Inscritos Recurrentes' : '';
					$headerReporte .= $actividadProgramada ? ', '.$fecha['mes'].' '.$key.' Alumnos Programados' : '';
					$headerReporte .= $alumnosActivos ? ', '.$fecha['mes'].' '.$key.' Alumnos Activos' : '';
					$headerReporte .= $alumnosEsperados ? ', '.$fecha['mes'].' '.$key.' Alumnos Esperados' : '';
					$headerReporte .= $graduados ? ', '.$fecha['mes'].' '.$key.' Alumnos Graduados' : '';
					$headerReporte .= $graduacionEsperada ? ', '.$fecha['mes'].' '.$key.' Graduacion Esperada' : '';
					$headerReporte .= $desertores ? ', '.$fecha['mes'].' '.$key.' Alumnos Desertores' : '';
					$headerReporte .= $pagosProgramados ? ', '.$fecha['mes'].' '.$key.'  Pagos Programados' : '';
					$headerReporte .= $pagosCobrados ? ', '.$fecha['mes'].' '.$key.'  Pagos Cobrados' : '';
					$headerReporte .= $cobranzaProgramada ? ', '.$fecha['mes'].' '.$key.'  Cobranza Programada' : '';
					$headerReporte .= $cobranzaCobrada ? ', '.$fecha['mes'].' '.$key.' Cobranza Cobrada' : '';
				}
				if($muestra == 'semana')
				{
					$headerReporte .= $registrados ? ',Semana '.$fecha.' '.$key.' Socios Registrados' : '';
					$headerReporte .= $activos ? ', Semana '.$fecha.' '.$key.' Socios Activos' : '';
					$headerReporte .= $activosNuevos ? ', Semana '.$fecha.' '.$key.' Socios Activos nuevos' : '';
					$headerReporte .= $nuevos ? ', Semana '.$fecha.' '.$key.' Registros Nuevos' : '';
					$headerReporte .= $cobranzaInternet ? ', Semana '.$fecha.' '.$key.' Cobranza Internet' : '';
					$headerReporte .= $cobranzaEducacion ? ', Semana '.$fecha.' '.$key.' Cobranza Educacion' : '';
					$headerReporte .= $cobranzaOtros ? ', Semana '.$fecha.' '.$key.' Cobranza Otros' : '';
					$headerReporte .= $cobranzaTotal ? ', Semana '.$fecha.' '.$key.' Cobranza Total' : '';
					$headerReporte .= $inscripcionEsperada ? ', Semana '.$fecha.' '.$key.' Inscripcion Esperada' : '';
					$headerReporte .= $preinscritos ? ', Semana '.$fecha.' '.$key.' Alumnos Preinscritos' : '';
					$headerReporte .= $inscritos ? ', Semana '.$fecha.' '.$key.' Alumnos Inscritos' : '';
					$headerReporte .= $inscritosUnicos ? ', Semana '.$fecha.' '.$key.' Alumnos Inscritos Unicos' : '';
					$headerReporte .= $inscritosNuevos ? ', Semana '.$fecha.' '.$key.' Alumnos Inscritos Nuevos' : '';
					$headerReporte .= $inscritosRecurrentes ? ', Semana '.$fecha.' '.$key.' Alumnos Inscritos Recurrentes' : '';
					$headerReporte .= $actividadProgramada ? ', Semana '.$fecha.' '.$key.' Alumnos Programados' : '';
					$headerReporte .= $alumnosActivos ? ', Semana '.$fecha.' '.$key.' Alumnos Activos' : '';
					$headerReporte .= $alumnosEsperados ? ', Semana '.$fecha.' '.$key.' Alumnos Esperados' : '';
					$headerReporte .= $graduados ? ', Semana '.$fecha.' '.$key.' Alumnos Graduados' : '';
					$headerReporte .= $graduacionEsperada ? ', Semana '.$fecha.' '.$key.' Graduacion Esperada' : '';
					$headerReporte .= $desertores ? ', Semana '.$fecha.' '.$key.' Alumnos Desertores' : '';
					$headerReporte .= $pagosProgramados ? ', Semana '.$fecha.' '.$key.'  Pagos Programados' : '';
					$headerReporte .= $pagosCobrados ? ', Semana '.$fecha.' '.$key.'  Pagos Cobrados' : '';
					$headerReporte .= $cobranzaProgramada ? ', Semana '.$fecha.' '.$key.'  Cobranza Programada' : '';
					$headerReporte .= $cobranzaCobrada ? ', Semana '.$fecha.' '.$key.' Cobranza Cobrada' : '';
				}
				// Revisamos si se requieren estas consultas
				if($nuevos || $cobranzaOtros || $cobranzaInternet || $cobranzaTotal)
				{
					$sqlIndicadoresDia = "
					SELECT sum(registros_nuevos)
					".($nuevos ? ",sum(registros_nuevos) as nuevos" : "");
					$sqlIndicadoresDia .= ($cobranzaOtros ? ",sum(cobranza_otros) as cobranza_otros" : "");
					$sqlIndicadoresDia .= ($cobranzaInternet ? ",sum(cobranza_internet) as cobranza_internet" : "");
					$sqlIndicadoresDia .= ($cobranzaTotal ?  ",sum(cobranza_total) as cobranza_total" : "");
					$sqlIndicadoresDia .= " FROM reporte_indicadores_dia WHERE ";
					if($muestra == 'mes')
					{
						$sqlIndicadoresDia .= "fecha BETWEEN '".$fecha['desde']."' AND '".$fecha['hasta']."'";
					}
					if($muestra == 'semana')
					{
						$sqlIndicadoresDia .= "semana = ".$fecha." AND anio=".$key;;
					}
					$connection = Propel::getConnection(ReporteIndicadoresDiaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
					$statement = $connection->prepare($sqlIndicadoresDia);
					$statement->execute();
					while($linea = $statement->fetch(PDO::FETCH_ASSOC))
					{
						$reporte = array(
								'nuevos'            => $nuevos ? $linea['nuevos'] : '',
								'cobranza_otros'    => $cobranzaOtros ? $linea['cobranza_otros']: '',
								'cobranza_internet' => $cobranzaInternet ? $linea['cobranza_internet'] : '',
								'cobranza_total'    => $cobranzaTotal ? $linea['cobranza_total'] : ''
						);

					}
				}
				if($registrados)
				{
					$sqlRegistros = "
					SELECT sum(socios_registrados) as registros FROM reporte_indicadores_dia WHERE ";
					if($muestra == 'mes')
					{
						$sqlRegistros .= "fecha <= '".$fecha['hasta']."'";
					}
					if($muestra == "semana")
					{
						$sqlRegistros .= "anio = ".$key;
						$sqlRegistros .= "AND semana = ".$fecha;
					}
					$connection = Propel::getConnection(ReporteIndicadoresDiaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
					$statement = $connection->prepare($sqlRegistros);
					$statement->execute();
					while($linea = $statement->fetch(PDO::FETCH_ASSOC))
					{
						$reporte['registros'] = $linea['registros'];
					}
				}
				if($activos)
				{
					$sqlSociosActivos = "SELECT s.socio_id as activos FROM reporte_indicadores_dia as r, reporte_indicadores_dia_socios_activos as s WHERE r.id = s.reporte_indicador_dia_id ";
					if($muestra == "mes")
					{
						$sqlSociosActivos .= "AND r.fecha BETWEEN '".$fecha['desde']."' AND '".$fecha['hasta']."'";
					}
					if($muestra == 'semana')
					{
						$sqlSociosActivos .= "AND r.semana = ".$fecha." AND r.anio=".$key;;
					}
					$sqlSociosActivos .= " GROUP BY  s.socio_id";
					$connection = Propel::getConnection(ReporteIndicadoresDiaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
					$statement = $connection->prepare($sqlSociosActivos);
					$statement->execute();
					while($linea = $statement->fetch(PDO::FETCH_ASSOC))
					{
						$reporte['activos'] += 1;
					}
				}
				if($activosNuevos)
				{
					$sqlSociosActivos = "SELECT s.socio_id as activos FROM reporte_indicadores_dia as r, reporte_indicadores_dia_socios_activos_nuevos as s WHERE r.id = s.reporte_indicador_dia_id ";
					if($muestra == "mes")
					{
						$sqlSociosActivos .= "AND r.fecha BETWEEN '".$fecha['desde']."' AND '".$fecha['hasta']."'";
					}
					if($muestra == 'semana')
					{
						$sqlSociosActivos .= "AND r.semana = ".$fecha." AND r.anio=".$key;;
					}
					$sqlSociosActivos .= " GROUP BY  s.socio_id";
					$connection = Propel::getConnection(ReporteIndicadoresDiaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
					$statement = $connection->prepare($sqlSociosActivos);
					$statement->execute();
					while($linea = $statement->fetch(PDO::FETCH_ASSOC))
					{
						$reporte['activos_nuevos'] += 1;
					}
				}
				if($inscripcionEsperada)
				{
					$sqlInscripcionEsperada = "SELECT sum(g.cupo_total) as esperada FROM grupo as g, horario as h WHERE g.horario_id = h.id ";
					if($muestra == "mes")
					{
						$sqlInscripcionEsperada .= "AND g.activo = true AND h.fecha_inicio BETWEEN '".$fecha['desde']." 00:00:00' AND '".$fecha['hasta']." 23:59:59'";
					}
					if($muestra == 'semana')
					{
						$sqlInscripcionEsperada .= "AND g.activo = true AND date_part('week', h.fecha_inicio) = ".$fecha. " AND date_part('year', h.fecha_inicio) = ".$key;
					}
					error_log($sqlInscripcionEsperada);
					$connection = Propel::getConnection(ReporteIndicadoresDiaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
					$statement = $connection->prepare($sqlInscripcionEsperada);
					$statement->execute();

					while($linea = $statement->fetch(PDO::FETCH_ASSOC))
					{
						$reporte['inscripcion_esperada'] += $linea['esperada'];
					}
				}
				 
				// Revisamos si se necesitan los alumnos activos
				if($alumnosEsperados)
				{
					$sqlAlumnosEsperados = "SELECT sum(g.cupo_total) as esperada FROM grupo as g, horario as h WHERE g.horario_id = h.id ";
					if($muestra == "mes")
					{
						$sqlAlumnosEsperados .= "AND g.activo = true AND (h.fecha_inicio, h.fecha_fin) OVERLAPS (DATE '".$fecha['desde']."', DATE '".$fecha['hasta']."') = true";
					}
					if($muestra == 'semana')
					{
						$fechasSemana = $this->diasSemana($fecha, $key);
						$sqlAlumnosEsperados .= "AND g.activo = true AND (h.fecha_inicio, h.fecha_fin) OVERLAPS (DATE '".$fechasSemana['desde']."', DATE '".$fechasSemana['hasta']."') = true";
					}
					error_log($sqlAlumnosEsperados);
					$connection = Propel::getConnection(ReporteIndicadoresDiaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
					$statement = $connection->prepare($sqlAlumnosEsperados);
					$statement->execute();

					while($linea = $statement->fetch(PDO::FETCH_ASSOC))
					{
						$reporte['alumnos_esperados'] += $linea['esperada'];
					}
				}
				if($actividadProgramada)
				{
					$sqlActividad = "SELECT r.grupo_id, s.socio_id as activos FROM reporte_indicadores_dia_grupo as r, reporte_indicadores_dia_grupo_alumnos_actividad_programada as s WHERE r.id = s.reporte_indicador_dia_grupo_id ";
					if($muestra == 'mes')
					{
						$sqlActividad .= "AND r.fecha BETWEEN '".$fecha['desde']."' AND '".$fecha['hasta']."' ";
					}
					if ($muestra == 'semana')
					{
						$sqlActividad .= "AND r.semana = ".$fecha." AND r.anio=".$key;;
					}
					$sqlActividad .= " GROUP BY r.grupo_id, s.socio_id";
					$connection = Propel::getConnection(ReporteIndicadoresDiaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
					$statement = $connection->prepare($sqlActividad);
					$statement->execute();
					while($linea = $statement->fetch(PDO::FETCH_ASSOC))
					{
						$reporte['actividad_programada'] += 1;
					}
				}
				if($alumnosActivos)
				{
					$sqlSociosActivos = "SELECT r.grupo_id, s.socio_id as activos FROM reporte_indicadores_dia_grupo as r, reporte_indicadores_dia_grupo_alumnos_activos as s WHERE r.id = s.reporte_indicador_dia_grupo_id ";
					if($muestra == 'mes')
					{
						$sqlSociosActivos .= "AND r.fecha BETWEEN '".$fecha['desde']."' AND '".$fecha['hasta']."' ";
					}
					if ($muestra == 'semana')
					{
						$sqlSociosActivos .= "AND r.semana = ".$fecha." AND r.anio=".$key;;
					}
					$sqlSociosActivos .= " GROUP BY r.grupo_id, s.socio_id";
					$connection = Propel::getConnection(ReporteIndicadoresDiaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
					$statement = $connection->prepare($sqlSociosActivos);
					$statement->execute();
					while($linea = $statement->fetch(PDO::FETCH_ASSOC))
					{
						$reporte['alumnos_activos'] += 1;
					}
				}
				if($preinscritos || $inscritos || $inscritosNuevos || $inscritosRecurrentes || $graduados ||
						$desertores || $pagosProgramados || $pagosCobrados || $cobranzaProgramada || $cobranzaCobrada
				)
				{
					$sqlIndicadoresDiaGrupo = "SELECT sum(socios_preinscritos)";
					$sqlIndicadoresDiaGrupo .= ( $preinscritos ? ",sum(socios_preinscritos) as preinscritos" : "");
					$sqlIndicadoresDiaGrupo .= ( $inscritos ? ",sum(socios_inscritos) as inscritos" : "");
					$sqlIndicadoresDiaGrupo .=	( $inscritosNuevos ?",sum(socios_inscritos_nuevos) as inscritos_nuevos" : "");
					$sqlIndicadoresDiaGrupo .=	( $inscritosRecurrentes ? ",sum(socios_inscritos_recurrentes) as inscritos_recurrentes" : "");
					//$sqlIndicadoresDiaGrupo .=	( $alumnosActivos ? ",sum(alumnos_activos) as alumnos_activos" : "");
					$sqlIndicadoresDiaGrupo .=	( $graduados ? ",sum(alumnos_graduados) as alumnos_graduados" : "");
					$sqlIndicadoresDiaGrupo .=	( $graduacionEsperada ? ",sum(graduacion_esperada) as graduacion_esperada" : "");
					$sqlIndicadoresDiaGrupo .=	( $desertores ? ",sum(alumnos_desertores) as alumnos_desertores" : "");
					$sqlIndicadoresDiaGrupo .=	( $pagosProgramados ? ",sum(pagos_programados) as pagos_programados" : "");
					$sqlIndicadoresDiaGrupo .=	( $pagosCobrados ? ",sum(pagos_cobrados) as pagos_cobrados" : "");
					$sqlIndicadoresDiaGrupo .=	( $cobranzaProgramada ? ",sum(cobranza_programada) as cobranza_programada" : "");
					$sqlIndicadoresDiaGrupo .=	( $cobranzaCobrada ? ",sum(cobranza_cobrada) as cobranza_cobrada" : "");
					$sqlIndicadoresDiaGrupo .=	" FROM reporte_indicadores_dia_grupo WHERE ";
					if($muestra == 'mes')
					{
						$sqlIndicadoresDiaGrupo .= "fecha BETWEEN '".$fecha['desde']."' AND '".$fecha['hasta']."' ";
					}
					if ($muestra == 'semana')
					{
						$sqlIndicadoresDiaGrupo .= "semana = ".$fecha." AND anio=".$key;;
					}
					$connection = Propel::getConnection(ReporteIndicadoresDiaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
					$statement = $connection->prepare($sqlIndicadoresDiaGrupo);
					$statement->execute();
					while($linea = $statement->fetch(PDO::FETCH_ASSOC))
					{
						$reporte['preinscritos'] = $preinscritos ? $linea['preinscritos'] : '';
						$reporte['inscritos'] = $inscritos ? $linea['inscritos'] : '';
						$reporte['inscritos_nuevos'] = $inscritosNuevos ? $linea['inscritos_nuevos'] : '';
						$reporte['inscritos_recurrentes'] = $inscritosRecurrentes ? $linea['inscritos_recurrentes'] : '';
						//$reporte['alumnos_activos'] = $alumnosActivos ? $linea['alumnos_activos'] : '';
						$reporte['alumnos_graduados'] = $graduados ? $linea['alumnos_graduados'] : '';
						$reporte['graduacion_esperada'] = $graduacionEsperada ? $linea['graduacion_esperada'] : '';
						$reporte['alumnos_desertores'] = $desertores ? $linea['alumnos_desertores'] : '';
						$reporte['pagos_programados'] = $pagosProgramados ? $linea['pagos_programados'] : '';
						$reporte['pagos_cobrados'] = $pagosCobrados ? $linea['pagos_cobrados'] : '';
						$reporte['cobranza_programada'] = $cobranzaProgramada ? $linea['cobranza_programada'] : '';
						$reporte['cobranza_cobrada'] = $cobranzaCobrada ? $linea['cobranza_cobrada'] : '';
					}
				}
				$lineaReporte .= $registrados ? ','.$reporte['registros'] : '';
				$lineaReporte .= $activos ? ','.$reporte['activos'] : '';
				$lineaReporte .= $activosNuevos ? ','.$reporte['activos_nuevos'] : '';
				$lineaReporte .= $nuevos ? ','.$reporte['nuevos'] : '';
				$lineaReporte .= $cobranzaInternet ? ','.$reporte['cobranza_internet'] : '';
				$lineaReporte .= $cobranzaEducacion ? ','.$reporte['cobranza_cobrada'] : '';
				$lineaReporte .= $cobranzaOtros ? ','.$reporte['cobranza_otros'] : '';
				$lineaReporte .= $cobranzaTotal ? ','.$reporte['cobranza_total'] : '';
				$lineaReporte .= $inscripcionEsperada ? ','.$reporte['inscripcion_esperada'] : '';
				$lineaReporte .= $preinscritos ? ','.$reporte['preinscritos'] : '';
				$lineaReporte .= $inscritos ? ','.$reporte['inscritos'] : '';
				$lineaReporte .= $inscritosUnicos ? ','.$reporte['inscritos_unicos'] : '';
				$lineaReporte .= $inscritosNuevos ? ','.$reporte['inscritos_nuevos'] : '';
				$lineaReporte .= $inscritosRecurrentes ? ','.$reporte['inscritos_recurrentes'] : '';
				$lineaReporte .= $actividadProgramada ? ','.$reporte['actividad_programada'] : '';
				$lineaReporte .= $alumnosActivos ? ','.$reporte['alumnos_activos'] : '';
				$lineaReporte .= $alumnosEsperados ? ','.$reporte['alumnos_esperados'] : '';
				$lineaReporte .= $graduados ? ','.$reporte['alumnos_graduados'] : '';
				$lineaReporte .= $graduacionEsperada ? ','.$reporte['graduacion_esperada'] : '';
				$lineaReporte .= $desertores ? ','.$reporte['alumnos_desertores'] : '';
				$lineaReporte .= $pagosProgramados ? ','.$reporte['pagos_programados'] : '';
				$lineaReporte .= $pagosCobrados ? ','.$reporte['pagos_cobrados'] : '';
				$lineaReporte .= $cobranzaProgramada ? ','.$reporte['cobranza_programada'] : '';
				$lineaReporte .= $cobranzaCobrada ? ','.$reporte['cobranza_cobrada'] : '';
			}
		}
		$headerReporte .= "\n\r";
		$lineaReporte .= "\n\r";
		$headerReporte = substr($headerReporte, 1, strlen($headerReporte));
		$lineaReporte = substr($lineaReporte, 1, strlen($lineaReporte));
		return $headerReporte.$lineaReporte;
	}
	/**
	 * Genera el reporte por zona
	 * @param unknown_type $desde
	 * @param unknown_type $hasta
	 */
	private function reporteZona($desde, $hasta, $parametros, $arregloFechas, $muestra)
	{
		// Obtenmos los indicadores por dia
		// Indicadores
		$registrados = array_key_exists('registrados', $parametros) ? true : false;
		$activos = array_key_exists('activos', $parametros) ? true : false;
		$activosNuevos = array_key_exists('activos_nuevos', $parametros) ? true : false;
		$nuevos = array_key_exists('nuevos', $parametros) ? true : false;
		$cobranzaInternet = array_key_exists('cobranza_internet', $parametros) ? true : false;
		$cobranzaEducacion = array_key_exists('cobranza_educacion', $parametros) ? true : false;
		$cobranzaOtros = array_key_exists('cobranza_otros', $parametros) ? true : false;
		$cobranzaTotal = array_key_exists('cobranza_total', $parametros) ? true : false;
		$inscripcionEsperada = array_key_exists('inscripcion_esperada', $parametros) ? true : false;
		$preinscritos = array_key_exists('preinscritos', $parametros) ? true : false;
		$inscritos = array_key_exists('inscritos', $parametros) ? true : false;
		$inscritosUnicos = array_key_exists('inscritos_unicos', $parametros) ? true : false;
		$inscritosNuevos = array_key_exists('inscritos_nuevos', $parametros) ? true : false;
		$inscritosRecurrentes = array_key_exists('inscritos_recurrentes', $parametros) ? true : false;
		$actividadProgramada = array_key_exists('actividad_programada', $parametros) ? true : false;
		$alumnosActivos = array_key_exists('alumnos_activos', $parametros) ? true : false;
		$alumnosEsperados = array_key_exists('alumnos_esperados', $parametros) ? true : false;
		$graduados = array_key_exists('graduados', $parametros) ? true : false;
		$graduacionEsperada = array_key_exists('graduacion_esperada', $parametros) ? true : false;
		$desertores = array_key_exists('desertores', $parametros) ? true : false;
		$pagosProgramados = array_key_exists('pagos_programados', $parametros) ? true : false;
		$pagosCobrados = array_key_exists('pagos_cobrados', $parametros) ? true : false;
		$cobranzaProgramada = array_key_exists('cobranza_programada', $parametros) ? true : false;
		$cobranzaCobrada = array_key_exists('cobranza_cobrada', $parametros) ? true : false;
		$headerReporte = "Centro";
		$arregloLineas = array();
		$flag = 0;
		foreach ($arregloFechas as $key => $anio)
		{
			foreach ($anio as $fecha)
			{
				$reporte = array();
				if($muestra == 'mes')
				{
					$headerReporte .= $registrados ? ', '.$fecha['mes'].' '.$key.' Socios Registrados' : '';
					$headerReporte .= $activos ? ', '.$fecha['mes'].' '.$key.' Socios Activos' : '';
					$headerReporte .= $activosNuevos ? ', '.$fecha['mes'].' '.$key.' Socios Activos Nuevos' : '';
					$headerReporte .= $nuevos ? ', '.$fecha['mes'].' '.$key.' Registros Nuevos' : '';
					$headerReporte .= $cobranzaInternet ? ', '.$fecha['mes'].' '.$key.' Cobranza Internet' : '';
					$headerReporte .= $cobranzaEducacion ? ', '.$fecha['mes'].' '.$key.' Cobranza Educacion' : '';
					$headerReporte .= $cobranzaOtros ? ', '.$fecha['mes'].' '.$key.' Cobranza Otros' : '';
					$headerReporte .= $cobranzaTotal ? ', '.$fecha['mes'].' '.$key.' Cobranza Total' : '';
					$headerReporte .= $inscripcionEsperada ? ', '.$fecha['mes'].' '.$key.' Inscripcion Esperada' : '';
					$headerReporte .= $preinscritos ? ', '.$fecha['mes'].' '.$key.' Alumnos Preinscritos' : '';
					$headerReporte .= $inscritos ? ', '.$fecha['mes'].' '.$key.' Alumnos Inscritos' : '';
					$headerReporte .= $inscritosUnicos ? ', '.$fecha['mes'].' '.$key.' Alumnos Inscritos Unicos' : '';
					$headerReporte .= $inscritosNuevos ? ', '.$fecha['mes'].' '.$key.' Alumnos Inscritos Nuevos' : '';
					$headerReporte .= $inscritosRecurrentes ? ', '.$fecha['mes'].' '.$key.' Alumnos Inscritos Recurrentes' : '';
					$headerReporte .= $actividadProgramada ? ', '.$fecha['mes'].' '.$key.' Alumnos Programados' : '';
					$headerReporte .= $alumnosActivos ? ', '.$fecha['mes'].' '.$key.' Alumnos Activos' : '';
					$headerReporte .= $alumnosEsperados ? ', '.$fecha['mes'].' '.$key.' Alumnos Esperados' : '';
					$headerReporte .= $graduados ? ', '.$fecha['mes'].' '.$key.' Alumnos Graduados' : '';
					$headerReporte .= $graduacionEsperada ? ', '.$fecha['mes'].' '.$key.' Graduacion Esperada' : '';
					$headerReporte .= $desertores ? ', '.$fecha['mes'].' '.$key.' Alumnos Desertores' : '';
					$headerReporte .= $pagosProgramados ? ', '.$fecha['mes'].' '.$key.'  Pagos Programados' : '';
					$headerReporte .= $pagosCobrados ? ', '.$fecha['mes'].' '.$key.'  Pagos Cobrados' : '';
					$headerReporte .= $cobranzaProgramada ? ', '.$fecha['mes'].' '.$key.'  Cobranza Programada' : '';
					$headerReporte .= $cobranzaCobrada ? ', '.$fecha['mes'].' '.$key.' Cobranza Cobrada' : '';
				}
				if($muestra == 'semana')
				{
					$headerReporte .= $registrados ? ', Semana '.$fecha.' '.$key.' Socios Registrados' : '';
					$headerReporte .= $activos ? ', Semana '.$fecha.' '.$key.' Socios Activos' : '';
					$headerReporte .= $activosNuevos ? ', Semana '.$fecha.' '.$key.' Socios Activos Nuevos' : '';
					$headerReporte .= $nuevos ? ', Semana '.$fecha.' '.$key.' Registros Nuevos' : '';
					$headerReporte .= $cobranzaInternet ? ', Semana '.$fecha.' '.$key.' Cobranza Internet' : '';
					$headerReporte .= $cobranzaEducacion ? ', Semana '.$fecha.' '.$key.' Cobranza Educacion' : '';
					$headerReporte .= $cobranzaOtros ? ', Semana '.$fecha.' '.$key.' Cobranza Otros' : '';
					$headerReporte .= $cobranzaTotal ? ', Semana '.$fecha.' '.$key.' Cobranza Total' : '';
					$headerReporte .= $inscripcionEsperada ? ', Semana '.$fecha.' '.$key.' Inscripcion Esperada' : '';
					$headerReporte .= $preinscritos ? ', Semana '.$fecha.' '.$key.' Alumnos Preinscritos' : '';
					$headerReporte .= $inscritos ? ', Semana '.$fecha.' '.$key.' Alumnos Inscritos' : '';
					$headerReporte .= $inscritosUnicos ? ', Semana '.$fecha.' '.$key.' Alumnos Inscritos Unicos' : '';
					$headerReporte .= $inscritosNuevos ? ', Semana '.$fecha.' '.$key.' Alumnos Inscritos Nuevos' : '';
					$headerReporte .= $inscritosRecurrentes ? ', Semana '.$fecha.' '.$key.' Alumnos Inscritos Recurrentes' : '';
					$headerReporte .= $actividadProgramada ? ', Semana '.$fecha.' '.$key.' Alumnos Programados' : '';
					$headerReporte .= $alumnosActivos ? ', Semana '.$fecha.' '.$key.' Alumnos Activos' : '';
					$headerReporte .= $alumnosEsperados ? ', Semana '.$fecha.' '.$key.' Alumnos Esperados' : '';
					$headerReporte .= $graduados ? ', Semana '.$fecha.' '.$key.' Alumnos Graduados' : '';
					$headerReporte .= $graduacionEsperada ? ', Semana '.$fecha.' '.$key.' Graduacion Esperada' : '';
					$headerReporte .= $desertores ? ', Semana '.$fecha.' '.$key.' Alumnos Desertores' : '';
					$headerReporte .= $pagosProgramados ? ', Semana '.$fecha.' '.$key.'  Pagos Programados' : '';
					$headerReporte .= $pagosCobrados ? ', Semana '.$fecha.' '.$key.'  Pagos Cobrados' : '';
					$headerReporte .= $cobranzaProgramada ? ', Semana '.$fecha.' '.$key.'  Cobranza Programada' : '';
					$headerReporte .= $cobranzaCobrada ? ', Semana '.$fecha.' '.$key.' Cobranza Cobrada' : '';
				}
				// Revisamos si se requieren estas consultas
				if($nuevos || $cobranzaOtros || $cobranzaInternet || $cobranzaTotal)
				{
					$sqlIndicadoresDia = "
					SELECT
					z.id
					".($nuevos ? ",sum(r.registros_nuevos) as nuevos" : "");
					$sqlIndicadoresDia .= ($cobranzaOtros ? ",sum(r.cobranza_otros) as cobranza_otros" : "");
					$sqlIndicadoresDia .= ($cobranzaInternet ? ",sum(r.cobranza_internet) as cobranza_internet" : "");
					$sqlIndicadoresDia .= ($cobranzaTotal ?  ",sum(r.cobranza_total) as cobranza_total" : "");
					$sqlIndicadoresDia .= " FROM reporte_indicadores_dia as r, centro as c, zona as z WHERE r.centro_id = c.id AND c.zona_id = z.id ";
					if($muestra == 'mes')
					{
						$sqlIndicadoresDia .= "AND r.fecha BETWEEN '".$fecha['desde']."' AND '".$fecha['hasta']."'";
					}
					if($muestra == 'semana')
					{
						$sqlIndicadoresDia .= "AND r.semana = ".$fecha." AND r.anio=".$key;;
					}
					$sqlIndicadoresDia .= " GROUP BY z.id";
					$connection = Propel::getConnection(ReporteIndicadoresDiaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
					$statement = $connection->prepare($sqlIndicadoresDia);
					$statement->execute();
					while($linea = $statement->fetch(PDO::FETCH_ASSOC))
					{
						$reporte[$linea['id']] = array(
								'nuevos'            => $nuevos ? $linea['nuevos'] : '',
								'cobranza_otros'    => $cobranzaOtros ? $linea['cobranza_otros']: '',
								'cobranza_internet' => $cobranzaInternet ? $linea['cobranza_internet'] : '',
								'cobranza_total'    => $cobranzaTotal ? $linea['cobranza_total'] : ''
						);

					}
				}
				if($registrados)
				{
					$sqlRegistros = "
					SELECT z.id, sum(r.socios_registrados) as registros FROM reporte_indicadores_dia as r, centro as c, zona as z WHERE r.centro_id = c.id AND c.zona_id = z.id ";
					if($muestra == 'mes')
					{
						$sqlRegistros .= "AND r.fecha <= '".$fecha['hasta']."'";
					}
					if($muestra == "semana")
					{
						$sqlRegistros .= "AND r.anio <= ".$key;
						$sqlRegistros .= "AND r.semana <= ".$fecha;
					}
					$sqlRegistros .= " GROUP BY z.id";
					$connection = Propel::getConnection(ReporteIndicadoresDiaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
					$statement = $connection->prepare($sqlRegistros);
					$statement->execute();
					while($linea = $statement->fetch(PDO::FETCH_ASSOC))
					{
						$reporte[$linea['id']]['registros'] = $linea['registros'];
					}
				}
				if($activos)
				{
					$sqlSociosActivos = "SELECT z.id,s.socio_id as activos FROM centro as c, zona as z, reporte_indicadores_dia as r, reporte_indicadores_dia_socios_activos as s WHERE r.centro_id = c.id AND c.zona_id = z.id AND r.id = s.reporte_indicador_dia_id ";
					if($muestra == "mes")
					{
						$sqlSociosActivos .= "AND r.fecha BETWEEN '".$fecha['desde']."' AND '".$fecha['hasta']."'";
					}
					if($muestra == 'semana')
					{
						$sqlSociosActivos .= "AND r.semana = ".$fecha." AND r.anio=".$key;;
					}
					$sqlSociosActivos .= " GROUP BY z.id, s.socio_id";
					$connection = Propel::getConnection(ReporteIndicadoresDiaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
					$statement = $connection->prepare($sqlSociosActivos);
					$statement->execute();
					while($linea = $statement->fetch(PDO::FETCH_ASSOC))
					{
						$reporte[$linea['id']]['activos'] += 1;
					}
				}
				if($activosNuevos)
				{
					$sqlSociosActivos = "SELECT z.id,s.socio_id as activos FROM centro as c, zona as z, reporte_indicadores_dia as r, reporte_indicadores_dia_socios_activos_nuevos as s WHERE r.centro_id = c.id AND c.zona_id = z.id AND r.id = s.reporte_indicador_dia_id ";
					if($muestra == "mes")
					{
						$sqlSociosActivos .= "AND r.fecha BETWEEN '".$fecha['desde']."' AND '".$fecha['hasta']."'";
					}
					if($muestra == 'semana')
					{
						$sqlSociosActivos .= "AND r.semana = ".$fecha." AND r.anio=".$key;;
					}
					$sqlSociosActivos .= " GROUP BY z.id, s.socio_id";
					$connection = Propel::getConnection(ReporteIndicadoresDiaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
					$statement = $connection->prepare($sqlSociosActivos);
					$statement->execute();
					while($linea = $statement->fetch(PDO::FETCH_ASSOC))
					{
						$reporte[$linea['id']]['activos_nuevos'] += 1;
					}
				}
				if($inscripcionEsperada)
				{
					$sqlInscripcionEsperada = "SELECT z.id, sum(g.cupo_total) as esperada FROM centro as c, zona as z, grupo as g, horario as h WHERE g.horario_id = h.id AND g.centro_id = c.id AND c.zona_id = z.id ";
					if($muestra == "mes")
					{
						$sqlInscripcionEsperada .= " AND g.activo=true AND h.fecha_inicio BETWEEN '".$fecha['desde']." 00:00:00' AND '".$fecha['hasta']." 23:59:59'";
					}
					if($muestra == 'semana')
					{
						$sqlInscripcionEsperada .= " AND g.activo=true AND date_part('week', h.fecha_inicio) = ".$fecha. " AND date_part('year', h.fecha_inicio) = ".$key;
					}
					$sqlInscripcionEsperada .= " GROUP BY z.id";
					error_log($sqlInscripcionEsperada);
					$connection = Propel::getConnection(ReporteIndicadoresDiaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
					$statement = $connection->prepare($sqlInscripcionEsperada);
					$statement->execute();

					while($linea = $statement->fetch(PDO::FETCH_ASSOC))
					{
						$reporte[$linea['id']]['inscripcion_esperada'] += $linea['esperada'];
					}
				}
				if($alumnosEsperados)
				{
					$sqlAlumnosEsperados = "SELECT z.id, sum(g.cupo_total) as esperada FROM centro as c, zona as z, grupo as g, horario as h WHERE g.horario_id = h.id AND g.centro_id = c.id AND c.zona_id = z.id ";
					if($muestra == "mes")
					{
						$sqlAlumnosEsperados .= " AND g.activo=true AND (h.fecha_inicio, h.fecha_fin) OVERLAPS (DATE '".$fecha['desde']."', DATE '".$fecha['hasta']."') = true";
					}
					if($muestra == 'semana')
					{
						$fechasSemana = $this->diasSemana($fecha, $key);
						$sqlAlumnosEsperados .= " AND g.activo=true AND (h.fecha_inicio, h.fecha_fin) OVERLAPS (DATE '".$fechasSemana['desde']."', DATE '".$fechasSemana['hasta']."') = true";
					}
					$sqlAlumnosEsperados .= " GROUP BY z.id";
					error_log($sqlAlumnosEsperados);
					$connection = Propel::getConnection(ReporteIndicadoresDiaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
					$statement = $connection->prepare($sqlAlumnosEsperados);
					$statement->execute();

					while($linea = $statement->fetch(PDO::FETCH_ASSOC))
					{
						$reporte[$linea['id']]['alumnos_esperados'] += $linea['esperada'];
					}
				}
				if($actividadProgramada)
				{
					$sqlSociosActivos = "SELECT z.id, r.grupo_id, s.socio_id as activos FROM centro as c, zona as z, reporte_indicadores_dia_grupo as r, reporte_indicadores_dia_grupo_alumnos_actividad_programada as s WHERE r.centro_id = c.id AND c.zona_id = z.id AND r.id = s.reporte_indicador_dia_grupo_id ";
					if($muestra == 'mes')
					{
						$sqlSociosActivos .= "AND r.fecha BETWEEN '".$fecha['desde']."' AND '".$fecha['hasta']."' ";
					}
					if ($muestra == 'semana')
					{
						$sqlSociosActivos .= "AND r.semana = ".$fecha." AND r.anio=".$key;;
					}
					$sqlSociosActivos .= " GROUP BY z.id, r.grupo_id, s.socio_id";

					$connection = Propel::getConnection(ReporteIndicadoresDiaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
					$statement = $connection->prepare($sqlSociosActivos);
					$statement->execute();
					while($linea = $statement->fetch(PDO::FETCH_ASSOC))
					{
						$reporte[$linea['id']]['actividad_programada'] += 1;
					}
				}
				// Revisamos si se necesitan los alumnos activos
				if($alumnosActivos)
				{
					$sqlSociosActivos = "SELECT z.id, r.grupo_id, s.socio_id as activos FROM centro as c, zona as z, reporte_indicadores_dia_grupo as r, reporte_indicadores_dia_grupo_alumnos_activos as s WHERE r.centro_id = c.id AND c.zona_id = z.id AND r.id = s.reporte_indicador_dia_grupo_id ";
					if($muestra == 'mes')
					{
						$sqlSociosActivos .= "AND r.fecha BETWEEN '".$fecha['desde']."' AND '".$fecha['hasta']."' ";
					}
					if ($muestra == 'semana')
					{
						$sqlSociosActivos .= "AND r.semana = ".$fecha." AND r.anio=".$key;;
					}
					$sqlSociosActivos .= " GROUP BY z.id, r.grupo_id, s.socio_id";
						
					$connection = Propel::getConnection(ReporteIndicadoresDiaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
					$statement = $connection->prepare($sqlSociosActivos);
					$statement->execute();
					while($linea = $statement->fetch(PDO::FETCH_ASSOC))
					{
						$reporte[$linea['id']]['alumnos_activos'] += 1;
					}
				}
				if($preinscritos || $inscritos || $inscritosNuevos || $inscritosRecurrentes || $graduados ||
						$desertores || $pagosProgramados || $pagosCobrados || $cobranzaProgramada || $cobranzaCobrada
				)
				{
					$sqlIndicadoresDiaGrupo = "SELECT z.id ";
					$sqlIndicadoresDiaGrupo .= ( $preinscritos ? ",sum(r.socios_preinscritos) as preinscritos" : "");
					$sqlIndicadoresDiaGrupo .= ( $inscritos ? ",sum(r.socios_inscritos) as inscritos" : "");
					$sqlIndicadoresDiaGrupo .=	( $inscritosNuevos ?",sum(r.socios_inscritos_nuevos) as inscritos_nuevos" : "");
					$sqlIndicadoresDiaGrupo .=	( $inscritosRecurrentes ? ",sum(r.socios_inscritos_recurrentes) as inscritos_recurrentes" : "");
					//$sqlIndicadoresDiaGrupo .=	( $alumnosActivos ? ",sum(r.alumnos_activos) as alumnos_activos" : "");
					$sqlIndicadoresDiaGrupo .=	( $graduados ? ",sum(r.alumnos_graduados) as alumnos_graduados" : "");
					$sqlIndicadoresDiaGrupo .=	( $graduacionEsperada ? ",sum(r.graduacion_esperada) as graduacion_esperada" : "");
					$sqlIndicadoresDiaGrupo .=	( $desertores ? ",sum(r.alumnos_desertores) as alumnos_desertores" : "");
					$sqlIndicadoresDiaGrupo .=	( $pagosProgramados ? ",sum(r.pagos_programados) as pagos_programados" : "");
					$sqlIndicadoresDiaGrupo .=	( $pagosCobrados ? ",sum(r.pagos_cobrados) as pagos_cobrados" : "");
					$sqlIndicadoresDiaGrupo .=	( $cobranzaProgramada ? ",sum(r.cobranza_programada) as cobranza_programada" : "");
					$sqlIndicadoresDiaGrupo .=	( $cobranzaCobrada ? ",sum(r.cobranza_cobrada) as cobranza_cobrada" : "");
					$sqlIndicadoresDiaGrupo .=	" FROM centro as c, zona as z, reporte_indicadores_dia_grupo AS r WHERE r.centro_id = c.id AND c.zona_id = z.id ";
					if($muestra == 'mes')
					{
						$sqlIndicadoresDiaGrupo .= "AND r.fecha BETWEEN '".$fecha['desde']."' AND '".$fecha['hasta']."' ";
					}
					if ($muestra == 'semana')
					{
						$sqlIndicadoresDiaGrupo .= "AND r.semana = ".$fecha." AND r.anio=".$key;;
					}
					$sqlIndicadoresDiaGrupo .= " GROUP BY z.id";
					$connection = Propel::getConnection(ReporteIndicadoresDiaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
					$statement = $connection->prepare($sqlIndicadoresDiaGrupo);
					$statement->execute();
					while($linea = $statement->fetch(PDO::FETCH_ASSOC))
					{
						$reporte[$linea['id']]['preinscritos'] = $preinscritos ? $linea['preinscritos'] : '';
						$reporte[$linea['id']]['inscritos'] = $inscritos ? $linea['inscritos'] : '';
						$reporte[$linea['id']]['inscritos_nuevos'] = $inscritosNuevos ? $linea['inscritos_nuevos'] : '';
						$reporte[$linea['id']]['inscritos_recurrentes'] = $inscritosRecurrentes ? $linea['inscritos_recurrentes'] : '';
						//$reporte[$linea['id']]['alumnos_activos'] = $alumnosActivos ? $linea['alumnos_activos'] : '';
						$reporte[$linea['id']]['alumnos_graduados'] = $graduados ? $linea['alumnos_graduados'] : '';
						$reporte[$linea['id']]['graduacion_esperada'] = $graduacionEsperada ? $linea['graduacion_esperada'] : '';
						$reporte[$linea['id']]['alumnos_desertores'] = $desertores ? $linea['alumnos_desertores'] : '';
						$reporte[$linea['id']]['pagos_programados'] = $pagosProgramados ? $linea['pagos_programados'] : '';
						$reporte[$linea['id']]['pagos_cobrados'] = $pagosCobrados ? $linea['pagos_cobrados'] : '';
						$reporte[$linea['id']]['cobranza_programada'] = $cobranzaProgramada ? $linea['cobranza_programada'] : '';
						$reporte[$linea['id']]['cobranza_cobrada'] = $cobranzaCobrada ? $linea['cobranza_cobrada'] : '';
					}
				}
				foreach ($reporte as $keyR => $lineaR)
				{
					$lineaReporte .= $cobranzaOtros ? ', '.(isset($lineaR['cobranza_otros']) ? $lineaR['cobranza_otros'] : '0' ) : '';
					$lineaReporte = '';
					$lineaReporte .= $registrados ? ', '.(isset($lineaR['registros']) ? $lineaR['registros'] : '0' ) : '';
					$lineaReporte .= $activos ? ', '.(isset($lineaR['activos']) ? $lineaR['activos'] : '0' ) : '';
					$lineaReporte .= $activosNuevos ? ', '.(isset($lineaR['activosNuevos']) ? $lineaR['activosNuevos'] : '0' ) : '';
					$lineaReporte .= $nuevos ? ', '.(isset($lineaR['nuevos']) ? $lineaR['nuevos'] : '0' ) : '';
					$lineaReporte .= $cobranzaInternet ? ', '.(isset($lineaR['cobranza_internet']) ? $lineaR['cobranza_internet'] : '0' ) : '';
					$lineaReporte .= $cobranzaEducacion ? ', '.(isset($lineaR['cobranza_cobrada']) ? $lineaR['cobranza_cobrada'] : '0' ) : '';
					$lineaReporte .= $cobranzaOtros ? ', '.(isset($lineaR['cobranza_otros']) ? $lineaR['cobranza_otros'] : '0' ) : '';
					$lineaReporte .= $cobranzaTotal ? ', '.(isset($lineaR['cobranza_total']) ? $lineaR['cobranza_total'] : '0' ) : '';
					$lineaReporte .= $inscripcionEsperada ? ', '.(isset($lineaR['inscripcion_esperada']) ? $lineaR['inscripcion_esperada'] : '0' ) : '';
					$lineaReporte .= $preinscritos ? ', '.(isset($lineaR['preinscritos']) ? $lineaR['preinscritos'] : '0' ) : '';
					$lineaReporte .= $inscritos ? ', '.(isset($lineaR['inscritos']) ? $lineaR['inscritos'] : '0' ) : '';
					$lineaReporte .= $inscritosUnicos ? ', '.(isset($lineaR['inscritos_unicos']) ? $lineaR['inscritos_unicos'] : '0' ) : '';
					$lineaReporte .= $inscritosNuevos ? ', '.(isset($lineaR['inscritos_nuevos']) ? $lineaR['inscritos_nuevos'] : '0' ) : '';
					$lineaReporte .= $inscritosRecurrentes ? ', '.(isset($lineaR['inscritos_recurrentes']) ? $lineaR['inscritos_recurrentes'] : '0' ) : '';
					$lineaReporte .= $actividadProgramada ? ', '.(isset($lineaR['actividad_programada']) ? $lineaR['actividad_programada'] : '0' ) : '';
					$lineaReporte .= $alumnosActivos ? ', '.(isset($lineaR['alumnos_activos']) ? $lineaR['alumnos_activos'] : '0' ) : '';
					$lineaReporte .= $alumnosEsperados ? ', '.(isset($lineaR['alumnos_esperados']) ? $lineaR['alumnos_esperados'] : '0' ) : '';
					$lineaReporte .= $graduados ? ', '.(isset($lineaR['alumnos_graduados']) ? $lineaR['alumnos_graduados'] : '0' ) : '';
					$lineaReporte .= $graduacionEsperada ? ', '.(isset($lineaR['graduacion_esperada']) ? $lineaR['graduacion_esperada'] : '0' ) : '';
					$lineaReporte .= $desertores ? ', '.(isset($lineaR['alumnos_desertores']) ? $lineaR['alumnos_desertores'] : '0' ) : '';
					$lineaReporte .= $pagosProgramados ? ', '.(isset($lineaR['pagos_programados']) ? $lineaR['pagos_programados'] : '0' ) : '';
					$lineaReporte .= $pagosCobrados ? ', '.(isset($lineaR['pagos_cobrados']) ? $lineaR['pagos_cobrados'] : '0' ) : '';
					$lineaReporte .= $cobranzaProgramada ? ', '.(isset($lineaR['cobranza_programada']) ? $lineaR['cobranza_programada'] : '0' ) : '';
					$lineaReporte .= $cobranzaCobrada ? ', '.(isset($lineaR['cobranza_cobrada']) ? $lineaR['cobranza_cobrada'] : '0' ) : '';
					$arregloLineas[$keyR][$flag] = $lineaReporte;
				}
				$flag ++;
			}
		}
		$headerReporte .= "\n\r";
		$lineaReporteF = '';
		foreach ($arregloLineas as $keyL => $zona)
		{
			$zonaL = ZonaPeer::retrieveByPK($keyL);
			$lineaReporteF .= $zonaL->getNombre();
			for ($i = 0; $i < $flag; $i++)
			{
				if(array_key_exists($i, $zona))
				{
					$lineaReporteF .= $zona[$i];
				}
				else
				{
					$lineaReporteF .= $registrados ? ', 0' : '';
					$lineaReporteF .= $activos ? ', 0' : '';
					$lineaReporteF .= $activosNuevos ? ', 0' : '';
					$lineaReporteF .= $nuevos ? ', 0' : '';
					$lineaReporteF .= $cobranzaInternet ? ', 0' : '';
					$lineaReporteF .= $cobranzaEducacion ? ', 0' : '';
					$lineaReporteF .= $cobranzaOtros ? ', 0' : '';
					$lineaReporteF .= $cobranzaTotal ? ', 0' : '';
					$lineaReporteF .= $inscripcionEsperada ? ', 0' : '';
					$lineaReporteF .= $preinscritos ? ', 0' : '';
					$lineaReporteF .= $inscritos ? ', 0' : '';
					$lineaReporteF .= $inscritosUnicos ? ', 0' : '';
					$lineaReporteF .= $inscritosNuevos ? ', 0' : '';
					$lineaReporteF .= $inscritosRecurrentes ? ', 0' : '';
					$lineaReporteF .= $actividadProgramada ? ', 0' : '';
					$lineaReporteF .= $alumnosActivos ? ', 0' : '';
					$lineaReporteF .= $alumnosEsperados ? ', 0' : '';
					$lineaReporteF .= $graduados ? ', 0' : '';
					$lineaReporteF .= $graduacionEsperada ? ', 0' : '';
					$lineaReporteF .= $desertores ? ', 0' : '';
					$lineaReporteF .= $pagosProgramados ? ', 0' : '';
					$lineaReporteF .= $pagosCobrados ? ', 0' : '';
					$lineaReporteF .= $cobranzaProgramada ? ', 0' : '';
					$lineaReporteF .= $cobranzaCobrada ? ', 0' : '';
				}
			}
			$lineaReporteF .= "\n\r";
		}

		return $headerReporte.$lineaReporteF;
	}
	 
	private function diasSemana($semana, $anio)
	{
		// Ajustamo la semana si esa es menor a 10;
			
		return array('desde' => date('Y-m-d' , strtotime($anio.'-W'.$semana.'-1')), 'hasta' => date('Y-m-d' , strtotime($anio.'-W'.$semana.'-7')) );
	}
	 
}




