
<?php use_javascript('plugins/jquery.windows.js') ?>
<?php use_javascript('jquery.ui.js') ?>
<?php use_javascript('i18n/jquery-ui-i18n.js') ?>

<?php use_stylesheet('./dashboard_reportes_tmp/indexSuccess.css')?>

<div id="windows-wrapper">
	<div window-title="Seguimiento Indicadores" window-state="min">
		<h1 class="nombre-reporte">Seguimiento Indicadores</h1>
		<form action="<?php url_for('dashboard_reportes/index') ?>">

			<div id="left">
				<fieldset>
					<legend>Opciones</legend>
					<div>
						<div>
							<label for="filtro">Filtro:</label> <select name="filtro"
								id="filtro">
								<option value="centro">Centro</option>
								<option value="grupo">Curso/Grupo</option>
								<option value="facilitador">Facilitador</option>
								<option value="ria" selected="selected">RIA</option>
								<option value="zona">Zona</option>
							</select>
						</div>
					</div>
				</fieldset>

				<fieldset>
					<legend>Per&iacute;odo</legend>
					<div>
						<label for="desde">Desde:</label> <input type="text" name="desde"
							id="desde" />
					</div>
					<div>
						<label for="hasta">Hasta:</label> <input type="text" name="hasta"
							id="hasta" />
					</div>
				</fieldset>
				<fieldset>
					<legend>Tama&ntilde;o de muestra</legend>
					<div id="muestra">
						<input type="radio" name="muestra" value="mes" id="mes"
							checked="checked"> <label for="mes">Mes</label> <input
							type="radio" name="muestra" value="semana" id="semana"> <label
							for="semana">Semana</label>
					</div>
				</fieldset>
			</div>
			<div id="right">
				<fieldset>
					<legend>Indicadores</legend>
					<table class="indicadores">
						<tr>
							<td>
								<div>
									<input type="checkbox" name="registrados" value="1"
										id="registrados" class="off"> <label for="registrados">Socios
										Registrados(#)</label>
								</div>
								<div>
									<input type="checkbox" name="activos" value="1" id="activos"
										class="off"> <label for="activos">Socios Activos(#)</label>
								</div>
								<div>
									<input type="checkbox" name="activos_nuevos" value="1"
										id="activos_nuevos" class="off"> <label for="activos">Socios
										Activos Nuevos(#)</label>
								</div>
								<div>
									<input type="checkbox" name="nuevos" value="1" id="nuevos"
										class="off"> <label for="nuevos">Registros Nuevos(#)</label>
								</div>
								<div>
									<input type="checkbox" name="cobranza_internet" value="1"
										id="cobranza_internet" class="off"> <label
										for="cobranza_internet">Cobranza Internet($)</label>
								</div>
								<div>
									<input type="checkbox" name="cobranza_educacion" value="1"
										id="cobranza_educacion" class="off"> <label
										for="cobranza_educacion">Cobranza Educaci&oacute;n($)</label>
								</div>
							</td>
							<td>
								<div>
									<input type="checkbox" name="cobranza_otros" value="1"
										id="cobranza_otros" class="off"> <label for="cobranza_otros">Cobranza
										Otros($)</label>
								</div>
								<div>
									<input type="checkbox" name="cobranza_total" value="1"
										id="cobranza_total" class="off"> <label for="cobranza_total">Cobranza
										Total($)</label>
								</div>
								<div>
									<input type="checkbox" name="inscripcion_esperada" value="1"
										id="inscripcion_esperada"> <label for="inscripcion_esperada">Incripci&oacute;n
										Esperada(#)</label>
								</div>
								<div>
									<input type="checkbox" name="preinscritos" value="1"
										id="preinscritos"> <label for="preinscritos">Alumnos
										Preinscritos(#)</label>
								</div> <!--  <div>
									<input type="checkbox" name="inscritos_unicos" value="1" id="inscritos_unicos">
									<label for="inscritos_unicos">Inscritos Activos &Uacute;nicos(#)</label>
								</div>
								-->
								<div>
									<input type="checkbox" name="inscritos" value="1"
										id="inscritos"> <label for="inscritos">Alumnos Inscritos(#)</label>
								</div>
								<div>
									<input type="checkbox" name="inscritos_nuevos" value="1"
										id="inscritos_nuevos"> <label for="inscritos_nuevos">Alumnos
										Inscritos Nuevos(#)</label>
								</div>
							</td>
							<td>
								<div>
									<input type="checkbox" name="inscritos_recurrentes" value="1"
										id="inscritos_recurrentes"> <label for="inscritos_recurrentes">Alumnos
										Inscritos Recurrentes(#)</label>
								</div>
								<div>
									<input type="checkbox" name="actividad_programada" value="1"
										id="alumnos_actividad_programada"> <label
										for="alumnos_activos">Actividad Programada(#)</label>
								</div>
								<div>
									<input type="checkbox" name="alumnos_activos" value="1"
										id="alumnos_activos"> <label for="alumnos_activos">Alumnos
										Activos(#)</label>
								</div>
								<div>
									<input type="checkbox" name="alumnos_esperados" value="1"
										id="alumnos_esperados"> <label for="alumnos_esperados">Alumnos
										Esperados(#)</label>
								</div> <!-- <div>
									<input type="checkbox" name="activos_unicos" value="1" id="activos_unicos">
									<label for="activos_unicos">Alumnos Activos &Uacute;nicos(#)</label>	
								</div>-->
								<div>
									<input type="checkbox" name="graduados" value="1"
										id="graduados"> <label for="graduados">Alumnos Graduados(#)</label>
								</div>
								<div>
									<input type="checkbox" name="graduacion_esperada" value="1"
										id="graduacion_esperada"> <label for="graduados">Graduaci&oacute;n
										Esperada(#)</label>
								</div>
							</td>
							<td>
								<div>
									<input type="checkbox" name="pagos_programados" value="1"
										id="pagos_programados"> <label for="pagos_programdos">Pagos
										Programados(#)</label>
								</div>
								<div>
									<input type="checkbox" name="pagos_cobrados" value="1"
										id="pagos_cobrados"> <label for="pagos_cobrados">Pagos
										Cobrados(#)</label>
								</div>
								<div>
									<input type="checkbox" name="cobranza_programada" value="1"
										id="cobranza_programada"> <label for="cobranza_programada">Cobranza
										Programada($)</label>
								</div>
								<div>
									<input type="checkbox" name="cobranza_cobrada" value="1"
										id="cobranza_cobrada"> <label for="cobranza_cobrada">Cobranza
										Cobrada($)</label>
								</div>
								<div>
									<input type="checkbox" name="desertores" value="1"
										id="desertores"> <label for="desertores">Alumnos Desertores(#)</label>
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="4">
								<div class="all">
									<input type="checkbox" name="todos" value="1" id="todos"> <label
										for="todos">Marcar/Desmarcar Todos</label>
								</div>
							</td>
						</tr>
					</table>
				</fieldset>
			</div>
			<div class="download">
				<input type="submit" value="Descargar reporte" id="submit-button">
			</div>
		</form>
	</div>
</div>


<script type="text/javascript">
$(document).ready(function () {
	$('#windows-wrapper').windows({
		resizable : true,
	    minimizable: false,
	    minheight : 300
	});
	$('#todos').click(function(){
		if($(this).attr('checked'))
		{
			$('input[type="checkbox"]').attr('checked', true);
		}
		else
		{
			$('input[type="checkbox"]').attr('checked', false);
		}
	});
	$('#filtro').change(function(){
		if($(this).val() == 'grupo' || $(this).val() == 'facilitador')
		{
			$('.off').attr('disabled', true);
		}
		else
		{
			$('.off').attr('disabled', false);
		}
	});
	$('form').submit(function(){
		if($('#desde').val() == '' && $('#hasta').val() == '')
		{
			alert('Ingresa un rango de fechas');
			return false;
		}
		var flag = 0;
		$('input[type="checkbox"]').each(function(){
			if($(this).attr('checked'))
			{
				flag ++;
			}
		});
		if(flag ==0)
		{
			alert('Selecciona al menos un indicador');
			return false;
		}
		
	});
	$.datepicker.setDefaults( $.datepicker.regional[ "es" ] );
	var dates = $( "#desde, #hasta" ).datepicker({
		showWeek: true,
		dateFormat: 'yy-m-d',
		minDate : new Date(2010, 8, 1),
		maxDate : 0,
		firstDay: 1,
		defaultDate: 0,
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		onSelect: function( selectedDate ) {
			var option = this.id == "desde" ? "minDate" : "maxDate",
				instance = $( this ).data( "datepicker" ),
				date = $.datepicker.parseDate(
					instance.settings.dateFormat ||
					$.datepicker._defaults.dateFormat,
					selectedDate, instance.settings );
			dates.not( this ).datepicker( "option", option, date );
		}
	});
});
</script>
