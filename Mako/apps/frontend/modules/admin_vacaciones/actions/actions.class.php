<?php

/**
 * admin_vacaciones actions.
 *
 * @package    mako
 * @subpackage admin_vacaciones
 * @author     Bixit SA de CV
 * @version    SVN: $Id: actions.class.php,v 1.2 2010/09/15 02:23:42 david Exp $
 */
class admin_vacacionesActions extends sfActions
{
	public function executeIndex(sfWebRequest $request)
	{
		$this->noLaborables = DiasNoLaborablesPeer::doSelect(new Criteria());
		$this->vacacionesPasadas = VacacionesPeer::vacacionesPasadas();
		$this->vacacionesActuales = VacacionesPeer::vacacionesActivas();
		$this->form = new VacacionesForm( null, array('url' =>  $this->getController()->genUrl('admin_vacaciones/ajaxUsuario')) );
	}

	public function executeCreate(sfWebRequest $request)
	{
		$this->forward404Unless($request->isMethod(sfRequest::POST));

		$this->form = new VacacionesForm();

		$this->processForm($request, $this->form);

		$this->noLaborables = DiasNoLaborablesPeer::doSelect(new Criteria());
		$this->vacacionesPasadas = VacacionesPeer::vacacionesPasadas();
		$this->vacacionesActuales = VacacionesPeer::vacacionesActivas();

		$this->setTemplate('index');
	}

	public function executeEdit(sfWebRequest $request)
	{
		$this->forward404Unless($vacaciones = VacacionesPeer::retrieveByPk($request->getParameter('id')), sprintf('El perido de vacaciones no existe (%s).', $request->getParameter('id')));

		$this->noLaborables = DiasNoLaborablesPeer::doSelect(new Criteria());
		$this->vacacionesPasadas = VacacionesPeer::vacacionesPasadas();
		$this->vacacionesActuales = VacacionesPeer::vacacionesActivas($vacaciones->getId());
		$this->periodo = array('id' => $vacaciones->getId(), 'usuario' => $vacaciones->getUsuarioRelatedByUsuarioId()->getNombreCompleto(), 'inicio' => $vacaciones->getFechaInicio('Y-m-d'), 'fin'=>$vacaciones->getFechaFin('Y-m-d'));

		$this->form = new VacacionesForm( $vacaciones, array('url' =>  $this->getController()->genUrl('admin_vacaciones/ajaxUsuario')) );

		$this->setTemplate('index');
	}

	public function executeUpdate(sfWebRequest $request)
	{
		$this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
		$this->forward404Unless($vacaciones = VacacionesPeer::retrieveByPk($request->getParameter('id')), sprintf('El perido de vacaciones no existe (%s).', $request->getParameter('id')));
		$this->form = new VacacionesForm($vacaciones, array('url' =>  $this->getController()->genUrl('admin_vacaciones/ajaxUsuario')));

		$this->processForm($request, $this->form);

		$this->noLaborables = DiasNoLaborablesPeer::doSelect(new Criteria());
		$this->vacacionesPasadas = VacacionesPeer::vacacionesPasadas();
		$this->vacacionesActuales = VacacionesPeer::vacacionesActivas($vacaciones->getId());
		$this->periodo = array('id' => $vacaciones->getId(), 'usuario' => $vacaciones->getUsuarioRelatedByUsuarioId()->getNombreCompleto(), 'inicio' => $vacaciones->getFechaInicio('Y-m-d'), 'fin'=>$vacaciones->getFechaFin('Y-m-d'));

		$this->setTemplate('index');
	}

	public function executeDelete(sfWebRequest $request)
	{
		$request->checkCSRFProtection();

		$this->forward404Unless($vacaciones = VacacionesPeer::retrieveByPk($request->getParameter('id')), sprintf('El perido de vacaciones no existe (%s).', $request->getParameter('id')));
		$this->getUser()->setFlash('notice', 'Perdiodo vacional para '.$vacaciones->getUsuarioRelatedByUsuarioId()->getNombreCompleto().' eliminado');
		$vacaciones->delete();

		$this->redirect('admin_vacaciones/index');
	}

	protected function processForm(sfWebRequest $request, sfForm $form)
	{
		$form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
		if ($form->isValid())
		{
			$flag = false;
			if($form->getObject()->isNew())
			{
				$flag = true;
			}
			$vacaciones = $form->save();

			$this->getUser()->setFlash('notice', 'Perdiodo vacional para '.$vacaciones->getUsuarioRelatedByUsuarioId()->getNombreCompleto().($flag ? ' creado':' actualizado'));

			$this->redirect('admin_vacaciones/index');
		}
	}

	public function executeVacacionesUsuario(sfWebRequest $request)
	{
		$this->forward404unless($request->isXmlHttpRequest());

		$arreglo = array('dias' => GrupoPeer::diasCalendarizadosFacilitador($request->getParameter('id')), 'periodo' => VacacionesPeer::vacacionesActivasFacilitador($request->getParameter('id')) );
		$this->getResponse()->setContentType('application/json');

		return $this->renderText(json_encode($arreglo));
	}

	public function executeAjaxUsuario($request)
	{
		$this->forward404unless($request->isXmlHttpRequest());
		$this->getResponse()->setContentType('application/json');

		$authors = UsuarioPeer::listaFacilitadores($request->getParameter('q'));

		return $this->renderText(json_encode($authors));
	}

}
