<?php use_stylesheet('control_escolar/control_escolar.css') ?>
<?php use_stylesheet('fullcalendar/jquery.fullcalendar.css') ?>
<?php use_stylesheet('control_escolar/admin_vacaciones.css') ?>
<?php use_stylesheet('datatables.css') ?>
<?php use_stylesheet('jquery.windows.css') ?>
<?php use_javascript('jquery.ui.js') ?>
<?php use_javascript('plugins/jquery.windows.js') ?>
<?php use_javascript('i18n/jquery-ui-i18n.js') ?>
<?php use_javascript('plugins/jquery.fullcalendar.js') ?>
<?php use_javascript('plugins/jquery.dataTables.js') ?>
<?php use_javascript('control_escolar/admin_vacaciones.js') ?>

<script type="text/javascript">
var disabledDays = [<?php foreach ($noLaborables as $dia): ?>
                  {
                    id  : 'disableds',
                    title  : '<?php echo $dia->getDescripcion() ?>',
                    start  : '<?php echo $dia->getDia('Y-m-d') ?>',
                    allDay : true,
                    className  : 'fc-disable',
                    editable: false,
                    description : '<?php echo $dia->getDescripcion() ?>'
                },
                <?php endforeach; ?>
                ];
var vacacionesPasadas = [<?php foreach ($vacacionesPasadas as $dia): ?>
                  {
                    id  : 'vacaciones-pasadas',
                    title  : '<?php echo $dia['usuario'] ?>',
                    start  : '<?php echo $dia['inicio'] ?>',
                    end    : '<?php echo $dia['fin'] ?>',
                    allDay : true,
                    className  : 'vaciones-pasadas',
                    editable: false,
                    ident : <?php echo $dia['id'] ?>
                },
                <?php endforeach; ?>
                ];
var vacacionesActuales = [<?php foreach ($vacacionesActuales as $dia): ?>
                  {
                    id  : 'vacaciones-actuales',
                    title  : '<?php echo $dia['usuario'] ?>',
                    start  : '<?php echo $dia['inicio'] ?>',
                    end    : '<?php echo $dia['fin'] ?>',
                    allDay : true,
                    className  : 'vaciones-actuales',
                    editable: false,
                    ident : <?php echo $dia['id'] ?>
                },
                <?php endforeach; ?>
                ];
var periodoActual = [<?php if(isset ($periodo)): ?>
                    {
                    id  : 'perido-actual',
                    title  : '<?php echo $periodo['usuario'] ?>',
                    start  : '<?php echo $periodo['inicio'] ?>',
                    end    : '<?php echo $periodo['fin'] ?>',
                    allDay : true,
                    className  : 'periodo-actual',
                    editable: false,
                    ident : <?php echo $periodo['id'] ?>
                    },
                    <?php endif;?>
                    ];
var $edit = '<?php echo url_for('admin_vacaciones/edit')?>'
</script>
<div id="windows-wrapper">
	<div id="dias-no" window-title="Administrador de vacaciones"
		window-state="min">
		<div>
			<?php include_partial('form', array('form' => $form)) ?>
		</div>
		<h2>Calendario de vacaciones</h2>
		<div id="calendar" style="width: 99%"></div>
		<p style="width: 99%; text-align: right">
			<b>Mostrar periodos vacacionales pasados</b> <input type="checkbox"
				id="mostrar-todos" />
		</p>
	</div>

</div>
