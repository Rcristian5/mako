<?php use_javascript('/sfFormExtraPlugin/js/jquery.autocompleter.js') ?>
<?php use_stylesheet('/sfFormExtraPlugin/css/jquery.autocompleter.css') ?>

<?php if ($form->getObject()->isNew()): ?>
<h2>Crear vacaciones</h2>
<?php else: ?>
<h2>Editar vacaciones</h2>
<?php endif; ?>

<?php use_stylesheet('./admin_vacaciones/_form.css')?>

<form
	action="<?php echo url_for('admin_vacaciones/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>"
	method="post"
	<?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>
	onsubmit="return comprobarCampos();">
	<?php if (!$form->getObject()->isNew()): ?>
	<input type="hidden" name="sf_method" value="put" />
	<?php endif; ?>
	<?php echo $form->renderHiddenFields(false) ?>
	<table>
		<tbody>
			<?php echo $form->renderGlobalErrors() ?>
			<tr>
				<th><?php echo $form['usuario_id']->renderLabel('Facilitador') ?></th>
				<td><?php echo $form['usuario_id']->renderError() ?> <?php echo $form['usuario_id']->render(array('requerido'=>1, 'filtro'=>'alfabetico', 'size'=>50, 'maxlength'=>100)) ?>
				</td>
				<th><?php echo $form['fecha_inicio']->renderLabel() ?></th>
				<td><?php echo $form['fecha_inicio']->renderError() ?> <?php echo $form['fecha_inicio']->render(array('requerido'=>1, 'filtro'=>'numerico', 'size'=>10, 'maxlength'=>10)) ?>
				</td>
				<th><?php echo $form['fecha_fin']->renderLabel() ?></th>
				<td><?php echo $form['fecha_fin']->renderError() ?> <?php echo $form['fecha_fin'] ->render(array('requerido'=>1, 'filtro'=>'numerico', 'size'=>10, 'maxlength'=>10) )?>
				</td>
				<td><img src="/imgs/ajax-circle.gif" alt="cargando…" class="load" />
				</td>
			</tr>
		</tbody>
	</table>
	<div id="periodos-pasados" style="width: 50%; margin: 5px auto;">
		<table id="table" class="display" width="100%">
			<thead>
				<tr>
					<th>Fecha Ininico</th>
					<th>Fecha Fin</th>
				</tr>
			</thead>
			<tbody>

			</tbody>
		</table>
	</div>
	<p class="botones_finales" style="width: 99%">
		<?php if (!$form->getObject()->isNew()): ?>
		&nbsp;
		<?php echo link_to('Eliminar', 'admin_vacaciones/delete?id='.$form->getObject()->getId(), array('method' => 'delete', 'confirm' => '¿Deseas elimar este perido de vacaciones?')) ?>
		<?php endif; ?>
		<input type="button" value="Cancelar"
			onclick="location.href='<?php echo url_for('admin_vacaciones/index')?>'">
		<input type="submit" value="Guardar" />
	</p>
</form>
