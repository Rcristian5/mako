<?php use_stylesheet('control_escolar/control_escolar.css') ?>
<?php use_stylesheet('datatables.css') ?>
<?php use_stylesheet('jquery.windows.css') ?>
<?php use_javascript('jquery.ui.js') ?>
<?php use_javascript('plugins/jquery.windows.js') ?>
<?php use_javascript('plugins/jquery.dataTables.js') ?>
<?php use_javascript('control_escolar/verificacion_curso.js') ?>
<?php use_stylesheet('./verificacion_curso/indexSuccess.css')?>
<div id="windows-wrapper">
	<div window-title="Verificación de cursos asociados a Moodle"
		window-state="min">

		<?php include_partial('detalleCursos', array('detalle' => $detalle)) ?>
		<?php include_partial('cursosConflicto', array('conflictos' => $conflictos)) ?>
		<?php include_partial('cursoVerificado', array('curso' => $curso, 'vinculacion' => $vinculacion)) ?>

	</div>
</div>

