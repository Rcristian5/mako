
<?php if($conflictos['conflicto']): ?>
<div>
	<h2>Cursos que presentan conflictos</h2>

	<p class="ui-widget ui-state-active ui-corner-all"
		style="width: 97%; color: #FFFFFF !important; padding: 10px">
		Cursos vinculados correctamente: <b><?php echo $conflictos['cursos_correctos']?>
		</b>
	</p>
	<p class="ui-widget ui-state-error ui-corner-all"
		style="width: 97%; color: #FFFFFF !important; padding: 10px">
		Cursos con conflictos de asociaci&oacute;n: <b><?php echo $conflictos['cursos_conflicto']?>
		</b>
	</p>
	<table id="conflicto" class="display" style="width: 100%;">
		<thead>
			<tr>
				<th>Categor&iacute;a</th>
				<th>Clave</th>
				<th>Nombre</th>
				<th>Vinculación</th>
				<th>Conflicto</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($conflictos['lista_conflicto'] as $curso): ?>
			<tr>
				<td><?php echo $curso['categoria'] ?></td>
				<td><?php echo $curso['clave'] ?></td>
				<td><?php echo $curso['nombre'] ?></td>
				<td><?php echo $curso['vinculacion'] ?></td>
				<td><?php echo $curso['conflicto'] ?></td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	<p class="botones_finales">&nbsp;</p>
</div>

<?php endif; ?>