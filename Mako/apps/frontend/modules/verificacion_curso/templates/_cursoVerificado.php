<?php if($curso): ?>
<div>
	<h2>
		Verificacion de curso "
		<?php echo $vinculacion['curso'] ?>
		"
	</h2>


	<table style="width: 99%;">
		<tbody>
			<tr>
				<th width="100">Categor&iacute;a</th>
				<td><?php echo $vinculacion['categoria'] ?></td>
				<td rowspan="4">
					<div
						class="ui-widget ui-corner-all <?php echo ($vinculacion['verificacion'] ? 'ui-state-active' : 'ui-state-error') ?>"
						style="color: #FFFFFF !important; padding: 10px;">
						<span
							class="ui-widget ui-icon <?php echo ($vinculacion['verificacion'] ? 'ui-icon-check' : 'ui-icon-closethick') ?>"
							style="float: left; margin-right: 5px"></span>
						<?php echo $vinculacion['mensaje']?>
					</div>

				</td>
			</tr>
			<tr>
				<th>Clave</th>
				<td><?php echo $vinculacion['clave'] ?></td>
			</tr>
			<tr>
				<th>Nombre</th>
				<td><?php echo $vinculacion['curso'] ?></td>
			</tr>
			<tr>
				<th>Vinculaci&oacute;n</th>
				<td><?php echo $vinculacion['vinculacion'] ?></td>
			</tr>
		</tbody>
	</table>
	<?php    print_r($vinculacion['cursos']) ?>
	<p class="botones_finales">&nbsp;</p>
</div>

<?php endif; ?>