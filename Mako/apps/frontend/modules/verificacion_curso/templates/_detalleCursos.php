<div>
	<h2>Verificacion de cursos asociados a Moodle</h2>
	<table id="detalle" class="display" style="width: 100%;">
		<thead>
			<tr>
				<th>Categor&iacute;a</th>
				<th>Clave</th>
				<th>Nombre</th>
				<th>Descripci&oacute;n</th>
				<th>Asociado a Moodle</th>
				<th>Asociado a RS</th>
				<th>&Uacute;ltima verificaci&oacute;n</th>
				<th class="acciones-tabla"></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($detalle as $curso): ?>
			<tr>
				<td><?php echo $curso->getCategoriaCurso()->getNombre() ?>
				</td>
				<td><?php echo $curso->getClave() ?>
				</td>
				<td><?php echo $curso->getNombre() ?>
				</td>
				<td><?php echo $curso->getDescripcion() ?>
				</td>
				<td><?php echo ($curso->getAsociadoMoodle() ? 'Si' : 'No' ) ?>
				</td>
				<td><?php echo ($curso->getAsociadoRs() ? 'Si' : 'No' ) ?>
				</td>
				<td><?php echo ($curso->getUltimaVerificacion() ? $curso->getUltimaVerificacion('d-m-Y g:i a') : 'No verificado') ?>
				</td>
				<td><?php echo link_to('<span class="ui-icon ui-icon-refresh bx-title" title="Verificar ahora"></span>', 'verificacion_curso/verificar?id='.$curso->getId()) ?>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	<p style="text-align: right; padding: 10px">
		<input type="button" value="Verificar todos los cursos"
			onclick="location.href='<?php echo url_for('verificacion_curso/todos')?>'" />
	</p>
	<p class="botones_finales">&nbsp;</p>
</div>
