<?php

/**
 * verificacion_curso actions.
 *
 * @package    mako
 * @subpackage verificacion_curso
 * @author     Bixit SA de CV
 * @version    SVN: $Id: actions.class.php,v 1.2 2010/09/20 19:35:59 david Exp $
 */
class verificacion_cursoActions extends sfActions
{
	/**
	 * Executes index action
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeIndex(sfWebRequest $request)
	{
		$this->detalle = CursoPeer::doSelect(new Criteria());
	}

	/**
	 * Verifica la asociacion de todos los cursos en moodle, rs y mako
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeTodos(sfWebRequest $request)
	{
		$this->detalle = CursoPeer::doSelect(new Criteria());
		if(!$conflictos = CursoPeer::verificarTodosCursos())
		{
			$this->getUser()->setFlash('notice', 'No fue posible conectar con Moodle');

			$this->redirect('verificacion_curso/index');
		}
		$this->conflictos  = $conflictos;

		$this->setTemplate("index");
	}
	/**
	 * Verifica que un curso este correctamente asociado a moodle o rs
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeVerificar(sfWebRequest $request)
	{
		$this->forward404Unless($curso = CursoPeer::retrieveByPk($request->getParameter('id')), sprintf('el curso no existe (%s).', $request->getParameter('id')));

		$this->detalle = CursoPeer::doSelect(new Criteria());
		if(!$coflicto = CursoPeer::verificarCurso($curso))
		{
			$this->getUser()->setFlash('notice', 'No fue posible conectar con Moodle');

			$this->redirect('verificacion_curso/index');
		}
		$this->vinculacion = $coflicto;
		$this->curso = true;

		$this->setTemplate("index");
	}

}
