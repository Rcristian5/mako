<?php  $Editar = $sf_user->getAttribute('EditarCalendarizacion'); ?>
<?php use_stylesheet('jquery.windows.css') ?>
<?php use_stylesheet('control_escolar/calendarizacion.css') ?>
<?php use_stylesheet('datatables.css') ?>
<?php use_stylesheet('TableTools2.css') ?>
<?php use_stylesheet('fullcalendar/jquery.fullcalendar.css') ?>

<?php use_javascript('jquery-ui-1.8.16.js') ?>
<?php use_javascript('plugins/jquery.dataTables1.8.js') ?>
<?php use_javascript('plugins/dataTables/TableToolsPDF.js') ?>
<?php use_javascript('plugins/dataTables/ZeroClipboard1.8.js') ?>
<?php use_javascript('plugins/jquery.windows.js') ?>
<?php use_javascript('plugins/jquery.qtip.js') ?>
<?php use_javascript('plugins/jquery.fullcalendar.js') ?>
<?php use_javascript('plugins/jquery.tooltip.js') ?>
<?php use_javascript('i18n/jquery-ui-i18n.js') ?>
<?php use_javascript('control_escolar/calendarizacion.js') ?>

<script type="text/javascript">
    var nuevo        = <?php echo ($nueva ? 'true' : 'false')?>,
        disabled     = [<?php foreach ($noLaborables as $dia): ?> '<?php echo $dia->getDia('Y-m-d') ?>', <?php endforeach; ?>],
        disabledDays =
            [<?php foreach ($noLaborables as $dia): ?> {
                id  : 'disableds',
                title  : '<?php echo substr($dia->getDescripcion(), 0 , 9).'…' ?>',
                start  : '<?php echo $dia->getDia('Y-m-d') ?>',
                allDay : true,
                className  : 'fc-disable',
                editable: false,
                description : '<?php echo trim($dia->getDescripcion()) ?>',
                ident : <?php echo $dia->getId() ?>
            },
            <?php endforeach; ?>];

    function loadForm(evento, dia, fecha, ddefault) {
        $.ajax({
            url : '<?php echo url_for('calendarizacion/form')?>',
            data : {
                evento   : evento,
                fecha    : fecha,
                dia      : dia,
                ddefault : ddefault,
                curso    : <?php echo ($sf_request->getParameter('curso') ? $sf_request->getParameter('curso') : 0) ?>,
                aula     : <?php echo ($sf_request->getParameter('aula') ? $sf_request->getParameter('aula') : 0 ) ?>,
                centro   : <?php echo ($sf_request->getParameter('centro') ? $sf_request->getParameter('centro') : 0) ?>
            },
            beforeSend : function() {
                if(!$('#dialog-horario').dialog("isOpen")) {
                    $('#dialog-horario').dialog("open")
                }

                $('#dialog-horario').dialog( "option", "width", 710 );
                $( "#dialog-horario" ).dialog( "option", "position",  ['left','top'] );
                $('#resumen-calendarizacion').html('<div class="load"></div>');
                $('.load').show();
            },
            success: function(data) {
                $('.load').hide();
                $('#resumen-calendarizacion').html(data);
                pegaHandlers();
            },
            error: function(data){
                console.error(data);
            }
        });
    }
</script>

<div id="windows-wrapper">
    <div window-title="Calendarización" window-state="min">
        <?php if($detalle): ?>
            <?php include_partial('detalle', array('centros' => $centros, 'adminControl' => $Editar)) ?>

            <div id="curso" title="Selecciona curso" style="display: none;">
                <div id="form-curso"></div>
            </div>

            <div id="borrado" title="Conectando con el centro" style="display: none;">
                <h1 style="padding: 15px">Conectando con el centro, espere...</h1>
            </div>

            <div id="aula" title="Selecciona aula" style="display: none;">
                <div id="form-aula"></div>
            </div>

            <div id="dialog-edit-vars" title="Actualizar aula y horarios del grupo" style="display: none;">
                <div id="edit-vars"></div>
            </div>

            <div id="dialog-change-begin" title="Reprogramar fecha de incio" style="display: none;">
                <div id="change-begin"></div>
            </div>

            <div id="dialog-change-check" title="Comprobando disponibilidad" style="display: none;">
                <div id="change-check"></div>
            </div>

            <div id="detail-groups" title="Grupos asignados">
                <div id="detail-groups-content"></div>
            </div>

            <div id="dialog-extend-payment" title="Extender fecha de cobro" style="display: none;">
                <div id="extend-payment"></div>
            </div>

            <script type="text/javascript">
                var centro;
                var curso;
                var grupos;
                var dialogTeachers;
                $(function() {
                    //Dialogo para el calendario
                    $('#dialog-calendar').dialog({
                        autoOpen: false,
                        width : 600,
                        position: ['left', 'top'],
                        resizable: false,
                        close: function(event, ui) {
                            $('.ui-icon-calendar').removeClass('ui-state-disabled');
                        }
                    });

                    //Dialog de centros
                    $('#tipo').dialog({
                        autoOpen: false,
                        modal: true,
                        height: 150,
                        minHeight: 100,
                        width: 450,
                        resizable: false
                    });

                    //Dialog de centros
                    $('#centro').dialog({
                        autoOpen: false,
                        modal: true,
                        height: 200,
                        width: 350,
                        resizable: false,
                        buttons: {
                            'Siguiente': function() {
                                centro = $('#input-centro').val();

                                if(centro != '') {
                                    $.ajax({
                                        url : '<?php echo url_for('calendarizacion/curso') ?>',
                                        beforeSend : function(){
                                            $('#fom-centro').hide();
                                            $('#centro').append('<span class="fc-load">&nbsp;</span>');
                                            $('.fc-load').show();
                                        },
                                        success: function(data) {
                                            $('.fc-load').remove();
                                            $('#form-curso').html(data);
                                            $('#centro').dialog('close');
                                            $('#curso').dialog('open');
                                        },
                                        error: function(data){
                                            console.error(data);
                                        }
                                    });
                                } else {
                                    alert('Selecciona un centro');
                                }
                            },
                            'Atras' : function() {
                                $(this).dialog('close');
                                $('#tipo').dialog('open');
                            },
                            'Cancelar' : function() {
                                $(this).dialog('close');
                            }
                        },
                        close: function() {
                            $('#from-centro').show();
                        }
                    });

                    //Dialog para cursos
                    $('#curso').dialog({
                        autoOpen: false,
                        modal: true,
                        height: 200,
                        width: 550,
                        resizable: false,
                        buttons: {
                            'Siguiente': function() {
                                curso = $('#input-curso').val();

                                if(curso != '') {
                                    $.ajax({
                                        url : '<?php echo url_for('calendarizacion/aula') ?>',
                                        data : {
                                            centro : centro
                                        },
                                        beforeSend : function(){
                                            $('#form-curso').hide();
                                            $('#curso').append('<span class="fc-load">&nbsp;</span>');
                                            $('.fc-load').show();
                                        },
                                        success: function(data) {
                                            $('.fc-load').remove();
                                            $('#form-aula').html(data);
                                            $('#curso').dialog('close');
                                            $('#aula').dialog('open');
                                        },
                                        error: function(data){
                                            console.error(data);
                                        }
                                    });

                                } else {
                                    alert('Selecciona un curso');
                                }
                            },
                            'Atras' : function() {
                                $(this).dialog('close');
                                    $('#centro').dialog('open');
                                },
                            'Cancelar' : function() {
                                $(this).dialog('close');
                            }
                        },
                        close: function() {
                            $('#form-curso').show();
                        }
                    });

                    //Dialog para aulas
                    $('#aula').dialog({
                        autoOpen: false,
                        modal: true,
                        height: 200,
                        width: 400,
                        resizable: false,
                        buttons: {
                            'Finalizar': function() {
                                aula = $('#input-aula').val();

                                if(aula != '') {
                                    location.href='<?php echo url_for('calendarizacion/nueva')?>?centro='+centro+'&curso='+curso+'&aula='+aula;
                                } else {
                                    alert('Selecciona una aula');
                                }
                            },
                            'Atras' : function() {
                                $(this).dialog('close');
                                $('#curso').dialog('open');
                            },
                            'Cancelar' : function() {
                                $(this).dialog('close');
                            }
                        },
                        close: function() {
                            //
                        }
                    });

                    //Dialogo para editar las variables del grupo
                    $( "#dialog-edit-vars" ).dialog({
                        autoOpen: false,
                        resizable: false,
                        width: 600,
                        height: 470,
                        modal: true,
                        position: ['center', 'center'],
                        buttons: {
                            'Guardar cambios': function() {
                                var dialog_edit_vars = $(this),
                                    aula             = $('#edit-vars #new-aula'),
                                    facilitador      = $('#edit-vars #new-user'),
                                    horario          = $('#edit-vars #new-horario');

                                data = {};

                                if(aula.length != 0 && aula.val() != 0) {
                                    data.aula  = aula.val();
                                    data.group = group;
                                }

                                if(facilitador.length != 0 && facilitador.val() != 0) {
                                    data.facilitador = facilitador.val();
                                    data.group = group;
                                }

                                if(horario.length != 0 && horario.val() != 0) {
                                    data.horario = horario.val();
                                    data.group = group;
                                }

                                if(typeof data.group == 'undefined') {
                                    alert('No hay nada que guardar.');
                                    dialog_edit_vars.dialog('close');
                                    return false;
                                }

                                $.ajax({
                                    url : '<?php echo url_for('calendarizacion/guardarCambiosVariables')?>',
                                    dataType: 'json',
                                    data: data,
                                    beforeSend : function(){
                                        $('#edit-vars').html('<img src="/imgs/ajax-circle.gif" id="ajax-img" alt="cargando..."/>');
                                    },
                                    success: function(data){
                                        if(typeof data.error != 'undefined') {
                                            if(data.error === false) {
                                                //Actualizar datos
                                                var tds_grupo = $('.edit-vars.ui-state-disabled').parent().parent().find('td');

                                                $(tds_grupo[0]) .html(data.data.centro);
                                                $(tds_grupo[1]) .html(data.data.curso);
                                                $(tds_grupo[2]) .html(data.data.clave);
                                                $(tds_grupo[3]) .html(data.data.facilitador);
                                                $(tds_grupo[4]) .html(data.data.aula);
                                                $(tds_grupo[5]) .html(data.data.fechas.inicio);
                                                $(tds_grupo[6]) .html(data.data.fechas.fin);
                                                $(tds_grupo[7]) .html(data.data.inscritos);
                                                $(tds_grupo[8]) .html(data.data.preinscritos);
                                                $(tds_grupo[9]) .html(data.data.horario.lunes    .inicio ? (data.data.horario.lunes    .inicio + ' - ' + data.data.horario.lunes    .fin).replace(/:00:00/g, '') : '');
                                                $(tds_grupo[10]).html(data.data.horario.martes   .inicio ? (data.data.horario.martes   .inicio + ' - ' + data.data.horario.martes   .fin).replace(/:00:00/g, '') : '');
                                                $(tds_grupo[11]).html(data.data.horario.miercoles.inicio ? (data.data.horario.miercoles.inicio + ' - ' + data.data.horario.miercoles.fin).replace(/:00:00/g, '') : '');
                                                $(tds_grupo[12]).html(data.data.horario.jueves   .inicio ? (data.data.horario.jueves   .inicio + ' - ' + data.data.horario.jueves   .fin).replace(/:00:00/g, '') : '');
                                                $(tds_grupo[13]).html(data.data.horario.viernes  .inicio ? (data.data.horario.viernes  .inicio + ' - ' + data.data.horario.viernes  .fin).replace(/:00:00/g, '') : '');
                                                $(tds_grupo[14]).html(data.data.horario.sabado   .inicio ? (data.data.horario.sabado   .inicio + ' - ' + data.data.horario.sabado   .fin).replace(/:00:00/g, '') : '');

                                                dialog_edit_vars.dialog('close');
                                                alert(data.msg);
                                            }
                                        }
                                    },
                                    error  : function(e){
                                        console.log(e);
                                    }
                                });
                            },
                            'Cancelar': function() {
                                $( this ).dialog( "close" );
                            }
                        },
                        close: function(event, ui) {
                            group  = 0;
                            $('.edit-vars').removeClass('ui-state-disabled');
                        }
                    });

                    // Reprogramar fecha de inicio
                    $( "#dialog-change-begin" ).dialog({
                        autoOpen: false,
                        resizable: false,
                        width: 500,
                        height: 700,
                        modal: true,
                        buttons: {
                            "Comprobar disponibilidad": function() {
                                if(nuevaFecha != '') {
                                    if(group != 0) {
                                        $.ajax({
                                            url : "<?php echo url_for('calendarizacion/comprobarDisponibilidadNuevoPeriodo')?>",
                                            data: {
                                                group : group,
                                                inicio: $('#new-begin-date').html()
                                            },
                                            beforeSend : function(){
                                                $('#change-check').html('<h2>Comprobando disponibilidad</h2><img src="/imgs/ajax-circle.gif" id="ajax-img" alt="cargango..."/>');
                                                $("#dialog-change-check").dialog('open');
                                            },
                                            success: function(data){
                                                $('#change-check').html(data);
                                            },
                                            error  : function(e){
                                                console.log(e);
                                            }
                                        });

                                    } else {
                                        alert('No se selecciono un grupo válido');
                                    }

                                } else {
                                    alert('Selecciona una fecha valida');
                                }
                            },
                            "Cancelar": function() {
                                $(this).dialog( "close" );
                            }
                        },
                        close: function(event, ui) {
                            group  = 0;
                            $('.change-begin').removeClass('ui-state-disabled');
                            nuevaFecha = '';
                        }
                    });

                    // Extender fecha de cobro
                    $( "#dialog-extend-payment" ).dialog({
                        autoOpen: false,
                        resizable: false,
                        width: 500,
                        height: 450,
                        modal: true,
                        buttons: {
                            "Guardar": function() {
                                console.log($('#extend-to-active').val());

                                if($('#extend-to-active').val() == 0) {
                                    if($('#extend-to').val() != '' ) {
                                        if(group != 0) {
                                            location.href='<?php echo url_for('calendarizacion/extendPagoSave')?>/group/'+group+'/plazo/'+$('#extend-to').val()+'/activo/'+$('#extend-to-active').attr('checked');

                                        } else {
                                            alert('No se selecciono un grupo válido');
                                        }

                                    } else {
                                        alert('Tienes que seleccionar una fecha');
                                    }
                                }

                                if($('#extend-to-active').val() == 1) {
                                    if(group != 0) {
                                        location.href='<?php echo url_for('calendarizacion/extendPagoSave')?>/group/'+group+'/plazo/'+$('#extend-to').val()+'/activo/'+$('#extend-to-active').attr('checked');

                                    } else {
                                        alert('No se selecciono un grupo válido');
                                    }
                                }
                            },
                            "Cancelar": function() {
                                $(this).dialog( "close" );
                            }
                        },
                        close: function(event, ui) {
                            group  = 0;

                            $('.extend-payment').removeClass('ui-state-disabled');
                            $('#extend-to')     .datepicker( "hide" );
                            $('#extend-to')     .datepicker( "destroy" );
                        }
                    });

                    $("#borrado").dialog({
                        height: 150,
                        width : 600,
                        modal: true,
                        autoOpen : false
                    });

                    // dialogo para comprobar y editar la nueva fecha de inicio
                    $("#dialog-change-check").dialog({
                        autoOpen: false,
                        resizable: false,
                        width: 650,
                        height: 300,
                        modal: true,
                    });

                    dialogTeachers = $('#detail-groups').dialog({
                        height: 500,
                        width: 800,
                        minHeight: 500,
                        minWidth: 800,
                        autoOpen: false,
                        modal: true,
                        resizable: true,
                        close: function(event, ui) {
                            $(rowSelect).children().removeClass('highlight');
                        }
                    });

                    $('.bad-button').live('click', function(){
                        $("#dialog-change-check").dialog('close');
                        return false;
                    });

                    $('#change-check a').live('mouseover', function(){
                        $(this).addClass('ui-state-hover');
                    }).live('mouseout', function(){
                        $(this).removeClass('ui-state-hover');
                    });

                    // Mostrando grupo
                    $('.show-groups').live('click', function(){
                        posRow    = FTable.fnGetPosition( this.parentNode.parentNode );
                        rowSelect = FTable.fnGetNodes(posRow);

                        var ident    = $(this).attr('id'),
                            period   = $(this).attr('accion'),
                            prevUser = $(rowSelect).prev().children('#indicator').attr('user'),
                            nextUser = $(rowSelect).next().children('#indicator').attr('user');

                        dialogTeachers.dialog('open');

                        $.ajax({
                            url : "<?php echo url_for('calendarizacion/detalleFacilitadorGrupos')?>",
                            data: {
                                ident : ident,
                                period: period,
                                next  : (nextUser == undefined) ? 0 : nextUser,
                                prev  : (prevUser == undefined) ? 0 : prevUser
                            },
                            beforeSend: function(){
                                $('#detail-groups-content').html('<img src="/imgs/ajax-circle.gif" id="ajax-img" alt="cargango..."/>');
                            },
                            success: function(data){
                                $('#detail-groups-content').html(data);
                                $(rowSelect).children().addClass('highlight');
                            },
                            error: function(e){
                                console.log(e);
                            }
                        });

                        return false;
                    });

                    // Acciones para cambiar de facilitador
                    $('.prev-user, .next-user').live('mouseover', function() {
                        if(!$(this).hasClass('ui-state-disabled')) {
                            $(this).addClass('ui-state-hover');
                        }
                    }).live('mouseout', function() {
                        if(!$(this).hasClass('ui-state-disabled')) {
                            $(this).removeClass('ui-state-hover');
                        }
                    }).live('click', function(){
                        if(!$(this).hasClass('ui-state-disabled')) {
                            var user = $(this).attr('user');

                            $(rowSelect).children().removeClass('highlight');
                            $('#detail-groups-contents').html('');

                            posRow    = FTable.fnGetPosition( document.getElementById(user) );
                            rowSelect = FTable.fnGetNodes(posRow);

                            var prevUser = $(rowSelect).prev().children('#indicator').attr('user'),
                                nextUser = $(rowSelect).next().children('#indicator').attr('user');

                            $.ajax({
                                url : "<?php echo url_for('calendarizacion/detalleFacilitadorGrupos')?>",
                                data: {
                                    ident : user,
                                    period: 'mes',
                                    next  : (nextUser == undefined) ? 0 : nextUser,
                                    prev  : (prevUser == undefined) ? 0 : prevUser
                                },
                                beforeSend: function(){
                                    $('#detail-groups-content').html('<img src="/imgs/ajax-circle.gif" id="ajax-img" alt="cargango..."/>');
                                },
                                success: function(data){
                                    $('#detail-groups-content').html(data);
                                    $(rowSelect).children().addClass('highlight');
                                },
                                error: function(e){
                                    console.log(e);
                                }
                            });
                        }

                        return false;
                    });

                    // Acciones para cambiar de periodo
                    $('.prev-period, .next-period').live('mouseover', function(){
                        if(!$(this).hasClass('ui-state-disabled')) {
                            $(this).addClass('ui-state-hover');
                        }
                    }).live('mouseout', function(){
                        if(!$(this).hasClass('ui-state-disabled')) {
                            $(this).removeClass('ui-state-hover');
                        }
                    }).live('click', function(){
                        if(!$(this).hasClass('ui-state-disabled')) {
                            var user = $(this).attr('user');
                            var period = $(this).attr('period');

                            $('#detalle-facilitadores-grrupos').html('');

                            $.ajax({
                                url : "<?php echo url_for('calendarizacion/detalleFacilitadorGruposPeriodo')?>",
                                data: {
                                    ident : user,
                                    period: period
                                },
                                beforeSend: function(){
                                    $('#detalle-facilitadores-grrupos').html('<img src="/imgs/ajax-circle.gif" id="ajax-img" alt="cargango..."/>');
                                },
                                success: function(data){
                                    $('#detalle-facilitadores-grrupos').html(data);
                                },
                                error: function(e){
                                    console.log(e);
                                }
                            });
                        }

                        return false;
                    });

                    // Boton nuevo
                    $('#nuevo').click(function() {
                        $('#centro').dialog('open');
                        return false;
                    });

                    // Tabs para el detalle
                    $("#tabs-details").tabs({
                        heightStyle: "content",
                        spinner    : '<img src="/imgs/ajax-small.gif" alt="Cargando..."/>',
                        ajaxOptions: {
                            error: function( xhr, status, index, anchor ) {
                                $( anchor.hash ).html("No fue posible obtener la lista, intenta refescando la página");
                            }
                        }
                    });
                });
            </script>
        <?php endif; ?>

        <?php if($nueva): ?>
            <?php include_partial('nueva', array('nueva' => $nueva)) ?>
        <?php endif; ?>

        <div id="dialog-calendar" title="Calendario">
            <div id="calendar" style="width: 99%"></div>
        </div>

        <p class="botones_finales"></p>
    </div>
</div>
