<table style="padding-top: 15px">
    <tr>
        <th>
            <label for="input-aula">Aula</label>
        </th>
        <td>
            <select id="input-aula">
                <option value="">--Selecciona un aula--</option>
                <?php foreach ($aulas as $aula): ?>
                    <option value="<?php echo $aula->getId()?>">
                        <?php echo $aula->getNombre() ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </td>
    </tr>
</table>
