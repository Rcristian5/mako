<?php use_stylesheet('./calendarizacion/_gruposCalendarizados.css')?>
<div style="width: 100%; margin: 10px 0;" id="detail-groups">

    <div id="table-loader"></div>

    <h1>
        <?php echo $tipoCurso ?>
    </h1>

    <div id="filter"
        class="fg-toolbar ui-toolbar ui-widget-header ui-corner-tl ui-corner-tr ui-helper-clearfix">
        <div class="dataTables_length" id="grupos_length">
            <label> Mostrar <select size="1" name="grupos_length"
                onchange="changeLength(this)">
                    <option <?php echo ($length ==10) ? 'selected="selected"' : '' ?>
                        value="10">10</option>
                    <option <?php echo ($length ==25) ? 'selected="selected"' : '' ?>
                        value="25">25</option>
                    <option <?php echo ($length ==50) ? 'selected="selected"' : '' ?>
                        value="50">50</option>
                    <option <?php echo ($length ==100) ? 'selected="selected"' : '' ?>
                        value="100">100</option>
            </select> registros
            </label>
        </div>
        <div id="grupos_filter" class="dataTables_filter">
            <p>
                <label>Filtrar por:</label> <select size="1">
                    <option
                    <?php echo ($camp == 'centro') ? 'selected="selected"' : '' ?>
                        value="centro">Centro</option>
                    <option
                    <?php echo ($camp == 'clave') ? 'selected="selected"' : '' ?>
                        value="clave">Clave</option>
                    <option
                    <?php echo ($camp == 'curso') ? 'selected="selected"' : '' ?>
                        value="curso">Curso</option>
                    <option
                    <?php echo ($camp == 'facilitador') ? 'selected="selected"' : '' ?>
                        value="facilitador">Facilitador</option>
                </select>
            </p>
            <p>
                <label>Buscar:</label> <input type="text"
                    onkeyup="filterWordKey(event, this)"
                    value="<?php echo ($filter!= ''  && $filter) ? $filter : '' ?>">
            </p>
            <p>
                <button onclick="filterWordClick(this)">Buscar</button>
                <?php if($filter!= ''  && $filter):?>
                <button onclick="cleanWord(this)">Limpiar</button>
                <?php endif;?>
            </p>
        </div>
    </div>

    <table id="grupos" class="display" width="100%">
        <thead>
            <tr class="head-table">
                <th width="50">Centro <span
                    class="DataTables_sort_icon css_right ui-icon <?php echo  ($order == 'centro' ) ? ( ($dir == 'asc') ? 'ui-icon-triangle-1-n' : 'ui-icon-triangle-1-s') : 'ui-icon-carat-2-n-s'  ?> order"
                    order="centro"></span>
                </th>
                <th>Curso <span
                    class="DataTables_sort_icon css_right ui-icon <?php echo  ($order == 'curso' ) ? ( ($dir == 'asc') ? 'ui-icon-triangle-1-n' : 'ui-icon-triangle-1-s') : 'ui-icon-carat-2-n-s'  ?> order"
                    order="curso"></span>
                </th>
                <th>Clave <span
                    class="DataTables_sort_icon css_right ui-icon <?php echo  ($order == 'clave' ) ? ( ($dir == 'asc') ? 'ui-icon-triangle-1-n' : 'ui-icon-triangle-1-s') : 'ui-icon-carat-2-n-s'  ?> order"
                    order="clave"></span>
                </th>
                <th>Facilitador <span
                    class="DataTables_sort_icon css_right ui-icon <?php echo  ($order == 'facilitador' ) ? ( ($dir == 'asc') ? 'ui-icon-triangle-1-n' : 'ui-icon-triangle-1-s') : 'ui-icon-carat-2-n-s'  ?> order"
                    order="facilitador"></span>
                </th>
                <th>Aula</th>
                <th>Incio <span
                    class="DataTables_sort_icon css_right ui-icon <?php echo  ($order == 'inicio' ) ? ( ($dir == 'asc') ? 'ui-icon-triangle-1-n' : 'ui-icon-triangle-1-s') : 'ui-icon-carat-2-n-s'  ?> order"
                    order="inicio"></span>
                </th>
                <th>Fin <span
                    class="DataTables_sort_icon css_right ui-icon <?php echo  ($order == 'fin' ) ? ( ($dir == 'asc') ? 'ui-icon-triangle-1-n' : 'ui-icon-triangle-1-s') : 'ui-icon-carat-2-n-s'  ?> order"
                    order="fin"></span>
                </th>
                <th>Pre-Inscritos</th>
                <th>Inscritos</th>
                <th width="32">Lu</th>
                <th width="32">Ma</th>
                <th width="32">Mi</th>
                <th width="32">Ju</th>
                <th width="32">Vi</th>
                <th width="32">S&aacute;</th>
                <th class="acciones-tabla"></th>
                <!-- condicional para mostrar acciones segun perfirl que accesa
                    04/11/2014
                    responsable: IMB
                    -->
                <?php if(isset($iniciaron) && $sf_user->getAttribute('usuario')->getRolId() != 6 && $sf_user->getAttribute('usuario')->getRolId() != 12): ?>
                    <th class="acciones-tabla"></th>
                    <th class="acciones-tabla"></th>
                <?php endif; ?>
                <?php if(isset($proximos) && $sf_user->getAttribute('usuario')->getRolId() != 6 && $sf_user->getAttribute('usuario')->getRolId() != 12): ?>
                    <th class="acciones-tabla"></th>
                    <th class="acciones-tabla"></th>
                    <th class="acciones-tabla"></th>
                    <th class="acciones-tabla"></th>
                <?php endif; ?>

            </tr>
        </thead>
        <tbody>
            <?php foreach ($grupos as $detalle): ?>
            <tr>
                <td><?php echo strtoupper($detalle->getCentro()->getAlias()) ?>
                </td>
                <td><?php echo $detalle->getCurso()->getNombre() ?></td>
                <td class="small-text"><?php echo $detalle->getClave() ?></td>
                <td><?php echo ($detalle->getUsuarioRelatedByFacilitadorId() ? $detalle->getUsuarioRelatedByFacilitadorId()->getNombreCompleto() : '' ) ?>
                </td>
                <td class="small-text"><?php echo $detalle->getAula()->getNombre() ?>
                </td>
                <td><?php echo $detalle->getHorario()->getFechaInicio('d-m-Y') ?></td>
                <td><?php echo $detalle->getHorario()->getFechaFin('d-m-Y') ?></td>
                <td><?php echo $detalle->inscritos ?></td>
                <td><?php echo $detalle->preinscritos ?></td>
                <td><?php echo ($detalle->getHorario()->getLunes() ? $detalle->getHorario()->getHiLu('H').' - '.$detalle->getHorario()->getHfLu('H') : '' ) ?>
                </td>
                <td><?php echo ($detalle->getHorario()->getMartes() ? $detalle->getHorario()->getHiMa('H').' - '.$detalle->getHorario()->getHfMa('H') : '' ) ?>
                </td>
                <td><?php echo ($detalle->getHorario()->getMiercoles() ? $detalle->getHorario()->getHiMi('H').' - '.$detalle->getHorario()->getHfMi('H') : '' ) ?>
                </td>
                <td><?php echo ($detalle->getHorario()->getJueves() ? $detalle->getHorario()->getHiJu('H').' - '.$detalle->getHorario()->getHfJu('H') : '' ) ?>
                </td>
                <td><?php echo ($detalle->getHorario()->getViernes() ? $detalle->getHorario()->getHiVi('H').' - '.$detalle->getHorario()->getHfVi('H') : '' ) ?>
                </td>
                <td><?php echo ($detalle->getHorario()->getSabado() ? $detalle->getHorario()->getHiSa('H').' - '.$detalle->getHorario()->getHfSa('H') : '' ) ?>
                </td>
                <td><?php if($sf_user->getAttribute('usuario')->getRolId() != 12):
                echo link_to('<span class="ui-icon ui-icon-calendar bx-title" title="Mostrar en el calendario" id="'.$detalle->getId().'" anio="'.$detalle->getHorario()->getFechaInicio('Y').'" mes="'.($detalle->getHorario()->getFechaInicio('n') - 1).'" dia="'.$detalle->getHorario()->getFechaInicio('d').'" ></span>', 'calendarizacion/index');
                endif; ?>
                </td>

                <?php if(isset($iniciaron) && $sf_user->getAttribute('usuario')->getRolId() != 6 && $sf_user->getAttribute('usuario')->getRolId() != 12): ?>
                <td
                <?php echo $detalle->getPlazoFechaCobro() ?  ($detalle->getPlazoFechaCobro()->getActivo() ? 'class="ui-state-highlight"' : 'class="ui-state-error"') : ''  ?>><span
                    class="ui-icon ui-icon-cart bx-title extend-payment"
                    group="<?php echo $detalle->getId() ?>"
                    title="<?php echo $detalle->getPlazoFechaCobro() ? ($detalle->getPlazoFechaCobro()->getActivo() ? 'Fecha de cobro extendida hasta '. $detalle->getPlazoFechaCobro()->getFechaFin('d/m/Y') : 'Plazo desactivado'): 'Extender fecha de cobro'  ?>"></span>
                </td>
                <td><span class="ui-icon ui-icon-trash bx-title delete-group"
                    group="<?php echo $detalle->getId() ?>"
                    ip="<?php echo $detalle->getCentro()->getIp() ?>"
                    title="borrar grupo"></span></td>
                <?php endif; ?>

                <?php if(isset($proximos) && $sf_user->getAttribute('usuario')->getRolId() != 6 && $sf_user->getAttribute('usuario')->getRolId() != 12): ?>

                    <td>
                        <span class="ui-icon ui-icon-pencil bx-title edit-vars"
                              group="<?php echo $detalle->getId() ?>"
                              title="Editar">
                        </span>
                    </td>
                    <td <?php echo $detalle->getPlazoFechaCobro() ? ($detalle->getPlazoFechaCobro()->getActivo() ? 'class="ui-state-highlight"' : 'class="ui-state-error"') : ''  ?> >
                        <span class="ui-icon ui-icon-cart bx-title extend-payment"
                              group="<?php echo $detalle->getId() ?>"
                              title="<?php echo $detalle->getPlazoFechaCobro() ? ($detalle->getPlazoFechaCobro()->getActivo() ? 'Fecha de cobro extendida hasta '. $detalle->getPlazoFechaCobro()->getFechaFin('d/m/Y') : 'Plazo desactivado'): 'Extender fecha de cobro'  ?>">
                        </span>
                    </td>
                    <td>
                        <span class="ui-icon ui-icon-wrench bx-title change-begin"
                              group="<?php echo $detalle->getId() ?>"
                              title="Reprogramar fecha de inicio">
                        </span>
                    </td>
                    <td>
                        <span class="ui-icon ui-icon-trash bx-title delete-group"
                              group="<?php echo $detalle->getId() ?>"
                              ip="<?php echo $detalle->getCentro()->getIp() ?>"
                              title="Borrar grupo">
                        </span>
                    </td>
                <?php endif; ?>
            </tr>
            <?php endforeach;?>
        </tbody>
    </table>

    <?php if (count($detalles) > 10): ?>
    <div id="paginador"
        class="fg-toolbar ui-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix">
        <div id="paginador-loader"></div>
        <div id="resumen-paginador" class="dataTables_info">
            Mostrando <span id="firs"><?php echo $detalles->getFirstIndice()?> </span>
            a <span id="last"><?php echo $detalles->getLastIndice()?> </span> de
            <span id="total"><?php echo count($detalles)?> </span>
        </div>

        <div id="navegacion-paginador"
            class="pagination dataTables_paginate fg-buttonset ui-buttonset fg-buttonset-multi ui-buttonset-multi paging_full_numbers">
            <span
                class="first ui-corner-tl ui-corner-bl fg-button ui-button ui-state-default <?php echo ($detalles->getPage() == 1) ? 'ui-state-disabled' : '' ?>"
                onclick="loadingGrupoTables(this);$('#grupos').closest('.ui-tabs-panel').load(urlDataTable, { 'page': '1', 'length' : lengthTable, 'order' : order, 'dir' : direction, 'filter': filter, 'camp' : camp }, function(){ loadGrupoTables(this); } );">
                Primero </span> <span
                class="previous ui-corner-tl ui-corner-bl fg-button ui-button ui-state-default <?php echo ($detalles->getPage() == 1) ? 'ui-state-disabled' : '' ?>"
                onclick="loadingGrupoTables(this);$('#grupos').closest('.ui-tabs-panel').load(urlDataTable, { 'page': '<?php echo $detalles->getPreviousPage() ?>', 'length' : lengthTable, 'order' : order, 'dir' : direction 'filter': filter, 'camp' : camp }, function(){ loadGrupoTables(this); } );">
                Previo </span> <span> <?php foreach ($detalles->getLinks() as $page): ?>
                <?php if ($page == $detalles->getPage()): ?> <span
                class="fg-button ui-button ui-state-default active ui-state-disabled">
                    <?php echo $page ?>
            </span> <?php else: ?> <span
                class="fg-button ui-button ui-state-default"
                onclick="loadingGrupoTables(this);$('#grupos').closest('.ui-tabs-panel').load(urlDataTable, { 'page': '<?php echo $page ?>', 'length' : lengthTable, 'order' : order, 'dir' : direction, 'filter': filter, 'camp' : camp }, function(){ loadGrupoTables(this); } );">
                    <?php echo $page ?>
            </span> <?php endif; ?> <?php endforeach; ?>

            </span> <span
                class="next ui-corner-tl ui-corner-bl fg-button ui-button ui-state-default <?php echo ($detalles->getPage() == $detalles->getNextPage())? 'ui-state-disabled' : '' ?>"
                onclick="loadingGrupoTables(this);$('#grupos').closest('.ui-tabs-panel').load(urlDataTable, { 'page': '<?php echo $detalles->getNextPage() ?>', 'length' : lengthTable, 'order' : order, 'dir' : direction, 'filter': filter, 'camp' : camp }, function(){ loadGrupoTables(this); } );">
                Siguiente </span> <span
                class="last ui-corner-tl ui-corner-bl fg-button ui-button ui-state-default <?php echo ($detalles->getPage() == $detalles->getLastPage())? 'ui-state-disabled' : '' ?>"
                onclick="loadingGrupoTables(this);$('#grupos').closest('.ui-tabs-panel').load(urlDataTable, { 'page': '<?php echo $detalles->getLastPage() ?>', 'length' : lengthTable, 'order' : order, 'dir' : direction, 'filter': filter, 'camp' : camp }, function(){ loadGrupoTables(this); } );">
                Último </span>
        </div>
    </div>
    <?php endif; ?>

</div>
<p class="botones_finales">&nbsp;</p>
<script type="text/javascript">
var group = 0;
var nuevaFecha = '';
urlDataTable = '<?php echo url_for('calendarizacion/grupos'.$action) ?>';
grupos = [<?php foreach ($detalles as $grupo): ?><?php foreach ($grupo->getHorario()->getFechasHorarios() as $fecha): ?>
                  {
                    id     : 'grupo_<?php echo $grupo->getId() ?>',
                    title  : '<?php echo $grupo->getClave() ?>',
                    start  : '<?php echo $fecha->getFecha('Y-m-d').' '.$fecha->getHoraInicio('H:i') ?>',
                    end    : '<?php echo $fecha->getFecha('Y-m-d').' '.$fecha->getHoraFin('H:i') ?>',
                    allDay : false,
                    className  : 'fc-calendarizados grupo_<?php echo $grupo->getId() ?>',
                    editable: false
                }, <?php endforeach; ?><?php endforeach; ?>];
$('#calendar').fullCalendar( 'removeEvents' );
$('#calendar').fullCalendar( 'addEventSource', disabledDays );
$('#calendar').fullCalendar( 'addEventSource', grupos );
$(function(){
    $('.change-begin').click(function(){
        group = $(this).attr('group');
        $(this).addClass('ui-state-disabled');
        $( "#dialog-change-begin" ).dialog("open");
        $.ajax({
            url  : "<?php echo url_for('calendarizacion/editFechaInicio')?>",
            data : { group   : group },
            beforeSend : function(){
                    $('#change-begin').html('<img src="/imgs/ajax-circle.gif" id="ajax-img" alt="cargango..."/>');
              },
            success: function(data){
                    $('#change-begin').html(data);
                },
            error  : function(e){
                    console.log(e);
                }
        });
    });
    $('.edit-vars').click(function(){
        group = $(this).attr('group');

        $(this).addClass('ui-state-disabled');
        $('#dialog-edit-vars').dialog('open');

        $.ajax({
            url : '<?php echo url_for('calendarizacion/editVariables')?>',
            data: { group   : group },
            beforeSend : function(){
                $('#edit-vars').html('<img src="/imgs/ajax-circle.gif" id="ajax-img" alt="cargango..."/>');
            },
            success: function(data){
                $('#edit-vars').html(data);
            },
            error  : function(e){
                console.log(e);
            }
        });
    });
    $('.extend-payment').click(function(){
        group = $(this).attr('group');
        $(this).addClass('ui-state-disabled');
        $( "#dialog-extend-payment" ).dialog("open");
        $.ajax({
            url  : "<?php echo url_for('calendarizacion/extendPago')?>",
            data : { group   : group },
            beforeSend : function(){
                    $('#extend-payment').html('<img src="/imgs/ajax-circle.gif" id="ajax-img" alt="cargango..."/>');
              },
            success: function(data){
                    $('#extend-payment').html(data);
                },
            error  : function(e){
                    console.log(e);
                }
        });
    });
    <?php if(isset($proximos) || isset($iniciaron)): ?>
    $('.delete-group').click(function(){
            group = $(this).attr('group');
            ip = $(this).attr('ip');
            target = $(this);
            $.ajax({
            url  : "<?php echo url_for('calendarizacion/verificarSincronizacion')?>",
            data : { group   : group },
            dataType: 'json',
            beforeSend : function(){
                            target.addClass('loader-fb');
              },
            success: function(data){
                    target.addClass('ui-state-disabled');
                    target.removeClass('loader-fb');
                            if(data.sincronizado == 1)
                            {
                            if(confirm('Se verificara primero en el centro que el grupo no tenga socios inscritos, SI HAY SOCIOS INSCRITOS O PREINSCRITOS EL GRUPO NO PUEDE SER BORRADO \n\n ¿Deseas continuar?'))
                                    {
                                            $.ajax({
                                                    dataType: "jsonp",
                                                    timeout : 30000,
                                                    data: {
                                                            grupo : group
                                                    },
                                                    url: 'http://'+ ip+'/calendarizacion/consulta_inscritos_borrar?method=?',
                                                    beforeSend : function(){
                                                            $("#borrado").dialog("open");
                                            },
                                                    success: function(data){
                                                            $("#borrado").dialog("close");
                                                            if(data.resultado == 'ok')
                                                            {
                                                                    if(confirm('¿Deseas elimar este grupo?'))
                                                                    {
                                                                            window.location = "<?php echo url_for('calendarizacion/delete')?>?id="+group;
                                                                    }
                                                                    else
                                                                    {
                                                                            target.removeClass('ui-state-disabled');
                                                                    }
                                                            }
                                                            else
                                                            {
                                                                    alert(data.resultado);
                                                                    target.removeClass('ui-state-disabled');
                                                            }
                                                    },
                                            error  : function(e){
                                                    $("#borrado").dialog("close");
                                                    target.removeClass('ui-state-disabled');
                                                    console.log('Error al conectar con el servidor'+e);
                                                            alert('No fue posible conectar con el centro');
                                            }
                                            });
                                    }
                                    else
                                    {
                                            target.removeClass('ui-state-disabled');
                                    }
                            }
                    else
                    {
                            if(confirm('El grupo NO SE HA SINCRONIZADO con el centro \n\n ¿Deseas eliminarlo ahora?'))
                                    {
                                    window.location = "<?php echo url_for('calendarizacion/delete')?>?id="+group;
                                    }
                                    else
                                    {
                                            target.removeClass('ui-state-disabled');
                                    }
                    }

                    },
            error  : function(e){
                    console.log(e);
                }
        });
    });
<?php endif;?>
});


</script>
