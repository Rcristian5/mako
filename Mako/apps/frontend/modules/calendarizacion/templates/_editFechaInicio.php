<?php use_stylesheet('./calendarizacion/_editFechaInicio.css')?>
<h1>Reprogramar fecha de inicio</h1>

<p>
    <label>Grupo:</label> <span><?php echo $grupo->getClave() ?> </span>
</p>
<p>
    <label>Curso:</label> <span><?php echo $grupo->getCurso()->getNombre() ?></span>
</p>
<p>
    <label>Centro:</label> <span><?php echo $grupo->getCentro()->getNombre() ?></span>
</p>
<p>
    <label>Facilitador asignado:</label> <span><?php echo $grupo->getUsuarioRelatedByFacilitadorId()->getNombreCompleto() ?></span>
</p>
<h2>
    Perido:
    <?php echo $grupo->getHorario()->getFechaInicio('d/m/Y') ?>
    -
    <?php echo $grupo->getHorario()->getFechaFin('d/m/Y') ?>
</h2>
<table width="100%">
    <thead>
        <tr>
            <th class="ui-state-default<?php echo ($grupo->getHorario()->getLunes() ? '' : ' ui-state-disabled' )?>">
                Lunes
            </th>
            <th class="ui-state-default<?php echo ($grupo->getHorario()->getMartes() ? '' : ' ui-state-disabled' )?>">
                Martes
            </th>
            <th class="ui-state-default<?php echo ($grupo->getHorario()->getMiercoles() ? '' : ' ui-state-disabled' )?>">
                Mi&eacute;rcoles
            </th>
            <th class="ui-state-default<?php echo ($grupo->getHorario()->getJueves() ? '' : ' ui-state-disabled' )?>">
                Jueves
            </th>
            <th class="ui-state-default<?php echo ($grupo->getHorario()->getViernes() ? '' : ' ui-state-disabled' )?>">
                Viernes
            </th>
            <th class="ui-state-default<?php echo ($grupo->getHorario()->getSabado() ? '' : ' ui-state-disabled' )?>">
                S&aacute;bado
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                <?php echo ($grupo->getHorario()->getLunes() ? $grupo->getHorario()->getHiLu('H:i').'-'.$grupo->getHorario()->getHfLu('H:i') : '' )?>
            </td>
            <td>
                <?php echo ($grupo->getHorario()->getMartes() ? $grupo->getHorario()->getHiMa('H:i').'-'.$grupo->getHorario()->getHfMa('H:i') : '' )?>
            </td>
            <td>
                <?php echo ($grupo->getHorario()->getMiercoles() ? $grupo->getHorario()->getHiMi('H:i').'-'.$grupo->getHorario()->getHfMi('H:i') : '' )?>
            </td>
            <td>
                <?php echo ($grupo->getHorario()->getJueves() ? $grupo->getHorario()->getHiJu('H:i').'-'.$grupo->getHorario()->getHfJu('H:i') : '' )?>
            </td>
            <td>
                <?php echo ($grupo->getHorario()->getViernes() ? $grupo->getHorario()->getHiVi('H:i').'-'.$grupo->getHorario()->getHfVi('H:i') : '' )?>
            </td>
            <td>
                <?php echo ($grupo->getHorario()->getSabado() ? $grupo->getHorario()->getHiSa('H:i').'-'.$grupo->getHorario()->getHfSa('H:i') : '' )?>
            </td>

    </tbody>
</table>

<div id="datepicker-change-begin"></div>

<h2 id="nueva-fecha" class="fecha-no-seleccionada">
    Selecciona la nueva fecha de inicio
</h2>

<script type="text/javascript">
var fechaInicio = '<?php echo $grupo->getHorario()->getFechaInicio('Y-m-d')?>';

$(function() {
    $.datepicker.setDefaults( $.datepicker.regional[ "es" ] );
    $("#datepicker-change-begin").datepicker({
        numberOfMonths: 2,
        firstDay: 1,
        minDate: 1,
        dateFormat: 'yy-mm-dd',
        beforeShowDay: disableDays,
        onSelect: function(dateText, inst) {
            if(dateText != fechaInicio)
            {
                nuevaFecha = dateText;
                $('#nueva-fecha').html('Nueva fecha de inicio: <span id="new-begin-date">'+dateText+'</span>')
                                 .removeClass('fecha-no-seleccionada');
            }
            else
            {
                nuevaFecha = '';
                $('#nueva-fecha').html('Selecciona la nueva fecha de inicio')
                                 .addClass('fecha-no-seleccionada');

                alert('Elige una fecha diferente a la fecha actual de inicio');
            }
        }
    });
    $('.fecha-inicio a').addClass('ui-state-disabled');
});

function disableDays(date) {
       var
            lunes     = <?php echo $grupo->getHorario()->getLunes() ? 1 : 0 ?>,
            martes    = <?php echo $grupo->getHorario()->getMartes() ? 1 : 0 ?>,
            miercoles = <?php echo $grupo->getHorario()->getMiercoles() ? 1 : 0 ?>,
            jueves    = <?php echo $grupo->getHorario()->getJueves() ? 1 : 0 ?>,
            viernes   = <?php echo $grupo->getHorario()->getViernes() ? 1 : 0 ?>,
            sabado    = <?php echo $grupo->getHorario()->getSabado() ? 1 : 0 ?>,
            inicio    = '<?php echo $grupo->getHorario()->getFechaInicio('Y').($grupo->getHorario()->getFechaInicio('n')-1).$grupo->getHorario()->getFechaInicio('j') ?>',
            fin       = '<?php echo $grupo->getHorario()->getFechaFin('Y').($grupo->getHorario()->getFechaFin('n')-1).$grupo->getHorario()->getFechaFin('j') ?>',
            arreglo   = [],
            hoy       = String(date.getFullYear())+String(date.getMonth())+String(date.getDate());

      switch(date.getDay())
      {
        case 1:
            if(lunes)
            {
                arreglo.push(true);
            }
        break;
        case 2:
            if(martes)
            {
                arreglo.push(true);
            }
        break;
        case 3:
            if(miercoles)
            {
                arreglo.push(true);
            }
        break;
        case 4:
            if(jueves)
            {
                arreglo.push(true);
            }
        break;
        case 5:
            if(viernes)
            {
                arreglo.push(true);
            }
        break;
        case 6:
            if(sabado)
            {
                arreglo.push(true);
            }
        break;
        case 0:
            arreglo.push(false);
        break;
      }

      if(hoy == inicio)
      {
          arreglo.push('periodo-grupo fecha-inicio');
          arreglo.push('Fecha de inicio');
      }

      if(hoy == fin)
      {
          arreglo.push('periodo-grupo');
          arreglo.push('Fecha de fin');
      }

      return arreglo;
}
</script>
