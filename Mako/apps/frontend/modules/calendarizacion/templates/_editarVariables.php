<style type="text/css">
    #edit-vars h1 {
        margin-bottom: 40px !important;
    }

    #edit-vars label {
        display: inline-block;
        width: 130px;
        font-weight: bold !important;
    }

    #edit-vars span {
        color: #136902;
    }

    #edit-vars p {
        text-align: left;
    }

    #edit-vars button {
        height: 24px;
        cursor: pointer;
    }

    .error {
        color: #e10000 !important;
    }
</style>

<script type="text/javascript">
    $(function(){
        $('#edit-vars button').click(function(){
            var btn = $(this);

            $.ajax({
                url: '<?php echo url_for('calendarizacion/getGrupo') ?>',
                data: {
                    grupo_id: group
                },
                dataType: 'json',
                success: function(data) {
                    if(typeof data.error != 'undefined') {
                        if(data.error === false) {
                            centro  = data.data.centro;
                            curso   = data.data.curso;

                            $.ajax({
                                url : '<?php echo url_for('calendarizacion/aula') ?>',
                                data : {
                                    centro : centro
                                },
                                beforeSend: function() {
                                    $('#dialog-edit-vars').append('<span class="fc-load">&nbsp;</span>');
                                    $('.fc-load')         .show();
                                },
                                success: function(form_aulas) {
                                    $('.fc-load')         .remove();
                                    $('#form-aula')       .html(form_aulas);
                                    $('#dialog-edit-vars').dialog('close');
                                    $('#aula')            .dialog('open');
                                    $('#input-aula')      .val(data.data.aula);
                                },
                                error: function(data){
                                    console.error(data);
                                }
                            });

                        } else {
                            alert(data.msg);
                        }
                    }
                }
            });
        });
    });
</script>

<h1>Actualizar información del grupo</h1>
<p>
    <label>Grupo:</label> <span><?php echo $grupo->getClave() ?> </span>
</p>
<p>
    <label>Curso:</label> <span><?php echo $grupo->getCurso()->getNombre() ?></span>
</p>
<p>
    <label>Centro:</label> <span><?php echo $grupo->getCentro()->getAlias() ?></span>
</p>
<p>
    <label>Aula:</label> <span><?php echo $grupo->getAula()->getNombre() ?></span>
</p>
<p>
    <label>Inicia:</label> <span><?php echo $grupo->getHorario()->getFechaInicio('d-m-Y') ?></span>
</p>
<p>
    <label>Termina:</label> <span><?php echo $grupo->getHorario()->getFechaFin('d-m-Y') ?></span>
</p>
<p>
    <label>Perfil requerido:</label> <span><?php echo $grupo->getCurso()->getPerfilFacilitador()->getNombre() ?></span>
</p>
<p>
    <label>Facilitador asignado:</label> <span><?php echo $grupo->getUsuarioRelatedByFacilitadorId()->getNombreCompleto() ?></span>
</p>
<p>
    <label id="label-facilitador">Nuevo facilitador</label>
    <?php if(count($facilitadores) == 0): ?>
        <span class="error">No hay facilitadores disponibles</span>
    <?php else: ?>
        <select id="new-user">
            <option value="0">--Selecciona un facilitador--</option>
            <?php foreach ($facilitadores as $facilitador): ?>
                <option value="<?php echo $facilitador['id']?>">
                    <?php echo $facilitador['nombre'] ?>
                </option>
            <?php endforeach;?>
        </select>
    <?php endif;?>
</p>
<p>
    <button id="change-aula" class="ui-button ui-state-default ui-button-text-only" title="Cambiar aula, horario y/o facilitador.">Re-calendarizar</button>
</p>