
<form
  action="<?php echo url_for('calendarizacion/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '?curso='.$cursoId.'&fecha='.$fecha.'&aula='.$aulaId.'&centro='.$centroId)) ?>"
  method="post"
  <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
  <?php if (!$form->getObject()->isNew()): ?>
  <input type="hidden" name="sf_method" value="put" />
  <?php endif; ?>
  <div class="resumen-calendarizacion">
    <table width="100%">
      <tr>
        <th>Inicia</th>
        <td><?php echo $dia ?></td>
        <th>Finaliza</th>
        <td class="finaliza"></td>
      </tr>
      <tr>
        <th>Centro</th>
        <td><?php echo $centro ?></td>
        <th>Curso</th>
        <td><?php echo $curso ?></td>
      </tr>
      <tr>
        <th>Aula</th>
        <td><?php echo $aula ?></td>
        <th>Facilitador</th>
        <td class="disponibilidad"><input type="button" id="disponibilidad"
          class="botones" value="Comprobar disponibilidad" /></td>
      </tr>
    </table>
  </div>

  <div class="dias-calendarizacion">
    <table class="detalle" width="100%">
      <thead>
        <tr>
          <th>Lunes <?php echo $form['lunes']->render(array('class'=>'lunes')) ?>
          </th>
          <th>Martes <?php echo $form['martes']->render(array('class'=>'martes')) ?>
          </th>
          <th>Mi&eacute;rcoles <?php echo $form['miercoles']->render(array('class'=>'miercoles')) ?>
          </th>
          <th>Jueves <?php echo $form['jueves']->render(array('class'=>'jueves')) ?>
          </th>
          <th>Viernes <?php echo $form['viernes']->render(array('class'=>'viernes')) ?>
          </th>
          <th>S&aacute;bado <?php echo $form['sabado']->render(array('class'=>'sabado')) ?>
          </th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td><?php echo $form['hi_lu']->render(array('class'=>'lunes-sel')) ?>
          </td>
          <td><?php echo $form['hi_ma']->render(array('class'=>'martes-sel')) ?>
          </td>
          <td><?php echo $form['hi_mi']->render(array('class'=>'miercoles-sel')) ?>
          </td>
          <td><?php echo $form['hi_ju']->render(array('class'=>'jueves-sel')) ?>
          </td>
          <td><?php echo $form['hi_vi']->render(array('class'=>'viernes-sel')) ?>
          </td>
          <td><?php echo $form['hi_sa']->render(array('class'=>'sabado-sel')) ?>
          </td>
        </tr>
        <tr>
          <td><?php echo $form['hf_lu']->render(array('class'=>'lunes-sel')) ?>
          </td>
          <td><?php echo $form['hf_ma']->render(array('class'=>'martes-sel')) ?>
          </td>
          <td><?php echo $form['hf_mi']->render(array('class'=>'miercoles-sel')) ?>
          </td>
          <td><?php echo $form['hf_ju']->render(array('class'=>'jueves-sel')) ?>
          </td>
          <td><?php echo $form['hf_vi']->render(array('class'=>'viernes-sel')) ?>
          </td>
          <td><?php echo $form['hf_sa']->render(array('class'=>'sabado-sel')) ?>
          </td>
        </tr>
      </tbody>
    </table>

  </div>
  <div class="disponibles" style="text-align: right; display: none;">
    Mostrar dias con problemas <input type="checkbox" id="mostrar-detalle" />
    <div id="tabla-detalle-dias" style="display: none; margin-top: 10px">
      <table id="detalle-dias" width="100%">
        <thead>
          <tr>
            <th>D&iacute;a</th>
            <th>Curso</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>

  </div>
  <p class="botones_finales" style="width: 99%">
    <?php echo $form->renderHiddenFields(false) ?>
    <?php if (!$form->getObject()->isNew()): ?>
    <?php echo link_to('Eliminar', 'calendarizacion/delete?id='.$form->getObject()->getId(), array('method' => 'delete', 'confirm' => '¿Deseas elimar este evento?', 'class'=>'botones')) ?>
    <?php endif; ?>
    <input type="button" id="cancelar" class="botones" value="Cancelar"
      onclick="location.href = '<?php echo url_for('calendarizacion/index') ?>'" />
    <input type="submit" id="guardar" class="botones" value="Guardar"
      onclick="return valido();" />
  </p>

</form>
<script type="text/javascript">
    $('.detalle').dataTable({
    "bPaginate": false,
    "bLengthChange": false,
    "bFilter": false,
    "bSort": false,
    "bInfo": false,
    "bAutoWidth": false,
                "bJQueryUI": true
                });
    oTable = $('#detalle-dias').dataTable({
    "bPaginate": false,
    "bLengthChange": false,
    "bFilter": true,
    "bSort": true,
    "bInfo": true,
    "bAutoWidth": false,
                "bJQueryUI": true
                });

    $('.detalle th input').each(function(){
        comprobarSelect($(this));
    });

    $('.detalle th input').click(function(){
        comprobarSelect($(this));
        if(!$('#disponibilidad').is(':visible'))
        {
            $('#disponibilidad').next().remove();
            $('#disponibilidad').show();
            $('#horario_fecha_fin').val('');
            $('.finaliza').html('');
        }
    });

    $('.detalle td select').change(function(){
        if(!$('#disponibilidad').is(':visible'))
        {
            $('#disponibilidad').next().remove();
            $('#disponibilidad').show();
            $('#horario_fecha_fin').val('');
            $('.finaliza').html('');
        }
        var nombre = $(this).attr('id')
        var hora = nombre.substring(8, 10);
        if(hora == 'hi')
        {
            var fin = nombre.replace(/hi/g, 'hf')
            $('#'+fin+' option[value~="'+(parseInt($('#'+nombre).val())+1)+'"]').attr('selected', 'selected');
        }
    });

    $('#disponibilidad').click(function(){
        var sel = 0;
        var horas = 0;
        $('.detalle th').removeClass('ui-state-error');
        //Comprobamos que se este seleccionado al menos una opcion
        $('.detalle th input').each(function(){
            if($(this).is(':checked'))
            {
                var dia = $(this).attr('class');
                if(parseInt($('.'+dia+'-sel[id*=hour][id*=hf]').val()) <= parseInt($('.'+dia+'-sel[id*=hour][id*=hi]').val()) )
                {
                    horas++
                    $(this).parent().addClass('ui-state-error');
                }
                sel++;
            }
        });
        if(sel!=0)
        {
            if(horas==0)
            {
                $.ajax({
                  url : '<?php echo url_for('calendarizacion/disponibilidad') ?>',
                  dataType : 'json',
                  data : {centro    : $('#horario_centro_id').val(),
                          aula      : $('#horario_aula_id').val(),
                          curso     : $('#horario_curso_id').val(),
                          inicio    : $('#horario_fecha_inicio').val(),
                          lunes     : $('#horario_lunes').attr('checked'),
                          hi_lu     : $('#horario_hi_lu_hour').val(),
                          hf_lu     : $('#horario_hf_lu_hour').val(),
                          martes    : $('#horario_martes').attr('checked'),
                          hi_ma     : $('#horario_hi_ma_hour').val(),
                          hf_ma     : $('#horario_hf_ma_hour').val(),
                          miercoles : $('#horario_miercoles').attr('checked'),
                          hi_mi     : $('#horario_hi_mi_hour').val(),
                          hf_mi     : $('#horario_hf_mi_hour').val(),
                          jueves    : $('#horario_jueves').attr('checked'),
                          hi_ju     : $('#horario_hi_ju_hour').val(),
                          hf_ju     : $('#horario_hf_ju_hour').val(),
                          viernes   : $('#horario_viernes').attr('checked'),
                          hi_vi     : $('#horario_hi_vi_hour').val(),
                          hf_vi     : $('#horario_hf_vi_hour').val(),
                          sabado    : $('#horario_sabado').attr('checked'),
                          hi_sa     : $('#horario_hi_sa_hour').val(),
                          hf_sa     : $('#horario_hf_sa_hour').val()
                          },
                  beforeSend : function(){
                      $('#disponibilidad').hide();
                      $('.disponibilidad').append('<img src="/imgs/ajax-circle.gif" alt="cargando…"/>');
                      $('.finaliza').html('<img src="/imgs/ajax-circle.gif" alt="cargando…"/>');
                      $('#guardar').attr('disabled',true);
                      $('#calendar').fullCalendar( 'removeEvents', 'nuevo-curso' );
                  },
                  success: function(data) {
                      var i;
                      var evento = [];
                      var fechaFinal;
                      $('#disponibilidad').next().remove();
                      $('#guardar').attr('disabled',false);
                      $('.finaliza').html('');
                      if(data.disponible.disponible=='si')
                      {
                          if(data.facilitadores.length > 0)
                          {
                              var facilitador = '<select name="horario[facilitador]" id="horario_facilitador">';
                              for(i= 0; i <= data.facilitadores.length-1; i++)
                              {
                                facilitador += '<option value="'+data.facilitadores[i].id+'">'+data.facilitadores[i].nombre+'</option>';
                              }
                              facilitador += '</select>';
                          }
                          else
                          {
                            facilitador = '<span class="ui-state-error">No hay facilitadores disponibles</span>';
                          }
                          $('.disponibilidad').append(facilitador);
                          for(i = 0; i<= data.fechas.length-1; i++)
                          {
                            evento = [{
                                        id        : 'nuevo-curso',
                                        title     : '<?php echo $clave ?>',
                                        start     : data.fechas[i].inicio,
                                        end       : data.fechas[i].fin,
                                        className : 'fc-nuevo',
                                        editable: false,
                                        allDay : false,
                                        description : 'Nuevo curso'
                                    }];
                             $('#calendar').fullCalendar( 'addEventSource', evento );
                          }
                          $('#horario_fecha_fin').val(data.fechas[data.fechas.length-1].fecha);
                          fechaFinal = new Date(data.fechaFinal);
                          $('.finaliza').html( $.fullCalendar.formatDate( fechaFinal, "dddd, dd ") +'de'+$.fullCalendar.formatDate( fechaFinal, " MMMM ")+'de'+$.fullCalendar.formatDate( fechaFinal, " yyyy"), $.fullCalendar.formatDate( fechaFinal, "yyyy-MM-dd") );
                      }
                      else
                      {
                        alert('No es posible calendarizar el curso en el rango de horas seleccionado, ajusta las horas');
                        if(data.disponible.dias.lunes == 1)
                        {
                            $('.lunes').parent().addClass('ui-state-error');
                        }
                        if(data.disponible.dias.martes == 1)
                        {
                            $('.martes').parent().addClass('ui-state-error');
                        }
                        if(data.disponible.dias.miercoles == 1)
                        {
                            $('.miercoles').parent().addClass('ui-state-error');
                        }
                        if(data.disponible.dias.jueves == 1)
                        {
                            $('.jueves').parent().addClass('ui-state-error');
                        }
                        if(data.disponible.dias.viernes == 1)
                        {
                            $('.viernes').parent().addClass('ui-state-error');
                        }
                        if(data.disponible.dias.sabado == 1)
                        {
                            $('.sabado').parent().addClass('ui-state-error');
                        }
                        var i;
                        oTable.fnClearTable();
                        for(i = 0; i<= data.disponible.detalle.length-1; i++)
                        {
                            oTable.fnAddData( [
                                data.disponible.detalle[i].dia,
                                data.disponible.detalle[i].curso
                                ]);
                        }
                        $('.disponibles').show();
                        $('#dialog-horario').dialog( "option", "resizable", true );
                        $('#mostrar-detalle').change(function(){
                            $('#tabla-detalle-dias').toggle();
                        });
                      }
                  },
                  error: function(data){
                    console.error('Error');
                    console.error(data);
                  }
              });
           }
           else
           {
              alert('La hora de fin no puede ser menor o igual a la hora de inicio');
           }
        }
        else
        {
            alert('Selecciona al menos un día');
        }
    });
function valido()
{
    if($('#horario_fecha_fin').val()!='')
    {
        if($('#horario_facilitador').length > 0)
        {
            if($('#horario_facilitador').val()!='')
            {
                return true;
            }
            else
            {
                alert('Debes seleccionar un facilitador');

                return false
            }
        }
        else
        {
            alert('No hay facilitadores dispononibles, intenta con otro horario');

            return false
        }

    }
    else
    {
        alert('Debes comprobar la disponibilidad de horario');

        return false;
    }
}
</script>
