<?php use_stylesheet('./calendarizacion/_detalleFacilitadorGrupos.css')?>

<h1>
    <a href="#" user="<?php echo $anteriorFacilitador ?>"
        class="ui-state-default ui-corner-all prev-user<?php echo (!$anteriorFacilitador)? ' ui-state-disabled' : ''?>"
        title="Siguiente facilitador"><span
        class="ui-icon ui-icon-circle-triangle-w"></span> </a>
    <?php echo $detalle['facilitador']?>
    <a href="#" user="<?php echo $siguienteFacilitador ?>"
        class="ui-state-default ui-corner-all next-user<?php echo (!$siguienteFacilitador)? ' ui-state-disabled' : ''?>"
        title="Anterior facilitador"><span
        class="ui-icon ui-icon-circle-triangle-e"></span> </a>
</h1>

<p>
    Centros a los que esta asignado:
    <?php echo $detalle['centros'] ?>
</p>
<p>
    Perfiles que posee:
    <?php echo $detalle['perfiles'] ?>
</p>

<div id="detalle-facilitadores-grrupos">
    <?php include_partial('tablaFacilitadorGrupos', array('detalle' => $detalle)) ?>
</div>
