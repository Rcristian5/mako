<?php use_stylesheet('control_escolar/control_escolar.css') ?>
<?php use_stylesheet('control_escolar/transferir.css') ?>
<?php use_stylesheet('jquery.windows.css') ?>
<?php use_javascript('jquery.ui.js') ?>
<?php use_javascript('plugins/jquery.windows.js') ?>
<?php use_javascript('control_escolar/transferir.js') ?>

<div id="windows-wrapper">
    <div id="div-transferir" window-title="Transferir alumos"
        window-state="min">
        <?php if ($sf_user->hasFlash('notice')): ?>
        <div class="flash_notice ui-state-highlight ui-corner-all">
            <span style="float: left; margin-right: 0.3em;"
                class="ui-icon ui-icon-info"></span> <strong><?php echo $sf_user->getFlash('notice') ?>
            </strong>
        </div>
        <?php endif; ?>
        <h2>Transferir Alumnos</h2>
        <div id="curso-original"
            class="ui-widget-content ui-corner-all sortables">
            <h3>
                Grupo
                <?php echo $grupo->getClave()?>
            </h3>
            <input type="hidden" id="grupo-actual"
                value="<?php echo $grupo->getId() ?>" />

            <div class="ui-widget-header ui-corner-all datos-curso">
                <p>
                    <span>Curso:</span>
                    <?php echo $grupo->getCurso()->getNombre()?>
                </p>
                <p>
                    <span>Fecha Inicio:</span>
                    <?php echo $dias[$grupo->getHorario()->getFechaInicio('N')].' '.$grupo->getHorario()->getFechaInicio('d').' de '.$meses[$grupo->getHorario()->getFechaInicio('n')].' de '.$grupo->getHorario()->getFechaInicio('Y') ?>
                </p>
                <p>
                    <span>Hora:</span>
                    <?php echo $dia->getHoraInicio('h:i a')?>
                </p>
                <p>
                    <span>Facilitador:</span>
                    <?php echo $tranferencia['facilitador']->getNombreCompleto() ?>
                </p>
            </div>
            <h3>Lista de alumnos</h3>
            <div id="alumnos-curso">
                <?php foreach($tranferencia['alumnos'] as $alumno):?>
                <div class="ui-widget ui-state-default ui-corner-all alumnos"
                    id="<?php echo $alumno['alumno']->getAlumnoId() ?>">
                    <?php echo $alumno['alumno']->getAlumnos()->getSocio()->getNombreCompleto() ?>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
        <div id="curso-tranferencia"
            class="ui-widget-content ui-corner-all sortables">
            <h3>
                Grupo que imparten el curso
                <?php echo $grupo->getCurso()->getNombre()?>
            </h3>
            <div id="accordion-grupos">
                <?php foreach($tranferencia['listas'] as $lista): ?>
                <h3>
                    <a href="#">Grupo <?php echo $lista['clave'] ?>, <?php echo $lista['fecha_inicio'].' '.$lista['hora_inicio'] ?>
                    </a>
                </h3>
                <div class="lista-grupos">
                    <div class="ui-widget-header ui-corner-all datos-curso">
                        <p>
                            <span>Aula:</span>
                            <?php echo $lista['aula'] ?>
                        </p>
                        <p>
                            <span>Facilitador:</span>
                            <?php echo $lista['facilitador'] ?>
                        </p>
                    </div>
                    <?php if(count($lista['alumnos'])==0): ?>
                    <div class="ui-state-error">No hay alumnos en este grupo</div>
                    <?php else: ?>
                    <?php foreach($lista['alumnos'] as $alumno):?>
                    <div class="ui-widget ui-state-highlight ui-corner-all en-grupo">
                        <?php echo $alumno->getAlumnos()->getSocio()->getNombreCompleto() ?>
                    </div>
                    <?php endforeach; ?>
                    <?php endif; ?>
                    <div id="<?php echo $lista['id'] ?>"
                        class="ui-state-active drop-here">Arrastra aqu&iacute; a los
                        alumnos que quieras transferir</div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>
