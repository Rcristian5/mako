<style type="text/css">
#edit-user h1 {
    margin-bottom: 40px !important;
}

#edit-user label {
    display: inline-block;
    width: 130px;
    font-weight: bold !important;
}

#edit-user span {
    color: #136902;
}

#edit-user p {
    text-align: left;
}

.error {
    color: #e10000 !important;
}

#label-facilitador {
    margin-top: 10px;
}
</style>

<h1>Actualizar facilitador</h1>
<p>
    <label>Grupo:</label> <span><?php echo $grupo->getClave() ?> </span>
</p>
<p>
    <label>Curso:</label> <span><?php echo $grupo->getCurso()->getNombre() ?></span>
</p>
<p>
    <label>Centro:</label> <span><?php echo $grupo->getCentro()->getNombre() ?></span>
</p>
<p>
    <label>Aula:</label> <span><?php echo $grupo->getAula()->getNombre() ?></span>
</p>
<p>
    <label>Inicia:</label> <span><?php echo $grupo->getHorario()->getFechaInicio('d-m-Y') ?></span>
</p>
<p>
    <label>Termina:</label> <span><?php echo $grupo->getHorario()->getFechaFin('d-m-Y') ?></span>
</p>
<p>
    <label>Perfil requerido:</label> <span><?php echo $grupo->getCurso()->getPerfilFacilitador()->getNombre() ?></span>
</p>
<p>
    <label>Facilitador asignado:</label> <span><?php echo $grupo->getUsuarioRelatedByFacilitadorId()->getNombreCompleto() ?></span>
</p>
<p>
    <label id="label-facilitador">Nuevo facilitador</label>
    <?php if(count($facilitadores) == 0): ?>
        <span class="error">No hay facilitadores disponibles</span>
    <?php else: ?>
        <select id="new-user">
            <option value="0">--Selecciona un facilitador--</option>
            <?php foreach ($facilitadores as $facilitador): ?>
                <option value="<?php echo $facilitador['id']?>">
                    <?php echo $facilitador['nombre'] ?>
                </option>
            <?php endforeach;?>
        </select>
    <?php endif;?>
</p>
