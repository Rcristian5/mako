<?php use_stylesheet('./calendarizacion/_extendPago.css')?>
<h1>Extender fecha de cobro</h1>

<p>
    <label>Grupo:</label> <span><?php echo $grupo->getClave() ?> </span>
</p>
<p>
    <label>Curso:</label> <span><?php echo $grupo->getCurso()->getNombre() ?></span>
</p>
<p>
    <label>Centro:</label> <span><?php echo $grupo->getCentro()->getNombre() ?></span>
</p>
<p>
    <label>Facilitador asignado:</label> <span><?php echo $grupo->getUsuarioRelatedByFacilitadorId()->getNombreCompleto() ?></span>
</p>
<p>
    <label>Fecha de incio:</label> <span><?php echo $grupo->getHorario()->getFechaInicio('d/m/Y') ?></span>
</p>
<p>
    <label>Fecha de fin:</label> <span><?php echo $grupo->getHorario()->getFechaFin('d/m/Y') ?></span>
</p>
<p>
    <label>Extender cobro hasta</label>
    <input type="text" id="extend-to" value="<?php echo ($grupo->getPlazoFechaCobro() ? $grupo->getPlazoFechaCobro()->getFechaFin('Y-m-d') : '') ?>" />
</p>

<p>
    <label>L&iacute;mite Extenci&oacute;n Cobro:</label> <span><?php echo $fechaLimite; ?></span>
</p>

<p>
    <label>Plazo activo</label>
    <input type="checkbox"
          id="extend-to-active"
          value="<?php echo ($grupo->getPlazoFechaCobro() ? 1 : 0) ?>"
          <?php echo ($grupo->getPlazoFechaCobro() ? ($grupo->getPlazoFechaCobro()->getActivo() ? 'checked="checked"' : '' ) : 'checked="checked" disabled') ?> />
</p>
<script>
$(function() {
    $.datepicker.setDefaults( $.datepicker.regional[ "es" ] );
    $( "#extend-to" ).datepicker({
        dateFormat: 'yy-mm-dd',
        firstDay: 1,
        minDate: <?php echo  ( ($grupo->getHorario()->getFechaInicio('Ymd') < date('Ymd') ) ? 0 : 'new Date('.$grupo->getHorario()->getFechaInicio('Y').', '.($grupo->getHorario()->getFechaInicio('n')-1).', '.($grupo->getHorario()->getFechaInicio('j')+1).')')  ?>,
        maxDate: "<?php echo $fechaLimite; ?>",
        beforeShowDay: disableDays,
        onSelect: function(dateText, inst) {
            $('#extend-to-active').attr('checked', true);
        }
    }).keypress(function(){
        return false;
    });
});

function disableDays(date) {
    var
        arreglo = [],
        inicio  = '<?php echo $grupo->getHorario()->getFechaInicio('Y').($grupo->getHorario()->getFechaInicio('n')-1).$grupo->getHorario()->getFechaInicio('j') ?>',
        fin     = '<?php echo $grupo->getHorario()->getFechaFin('Y').($grupo->getHorario()->getFechaFin('n')-1).$grupo->getHorario()->getFechaFin('j') ?>',
        hoy     = String(date.getFullYear())+String(date.getMonth())+String(date.getDate());

    if(date.getDay() == 0)
    {
        arreglo.push(false);
    }
    else
    {
        arreglo.push(true);
    }

    if(hoy == inicio)
    {
        arreglo.push('periodo-grupo fecha-inicio');
        arreglo.push('Fecha de inicio');
    }

    if(hoy == fin)
    {
        arreglo.push('periodo-grupo');
        arreglo.push('Fecha de fin');
    }

    return arreglo;
}
</script>
