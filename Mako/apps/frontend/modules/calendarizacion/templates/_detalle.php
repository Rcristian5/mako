<?php  $Editar = $sf_user->getAttribute('EditarCalendarizacion'); ?>

<?php use_stylesheet('./calendarizacion/_detalle.css')?>
<p class="nuevo-curso">
        <?php if($Editar): ?>
	<input type="button" value="Calendarizar curso" id="nuevo" />
	<?php endif; ?>
<p>
<div id="tabs-details" style="width: 98%">
    <ul>
        <li>
            <a href="<?php echo url_for('calendarizacion/listaGrupos') ?>">
                <span> </span> Lista por grupos
            </a>
        </li>
        <li>
            <a href="<?php echo url_for('calendarizacion/listaFacilitadores') ?>">
                <span> </span> Lista por facilitador
            </a>
        </li>
    </ul>
</div>

<div id="centro" title="Selecciona centro">
    <div id="form-centro">
        <table style="padding-top: 15px">
            <tr>
                <th>
                    <label for="input-centro">Centro</label>
                </th>
                <td>
                    <select id="input-centro">
                        <option value="">--Selecciona un centro--</option>
                        <?php foreach ($centros as $centro): ?>
                            <option value="<?php echo $centro->getId()?>">
                                <?php echo $centro->getNombre() ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </td>
            </tr>
        </table>
    </div>
</div>
