<?php use_helper('Date') ?>
<?php use_stylesheet('./calendarizacion/_listaFacilitadores.css')?>

<div id="wrapper-facilitadores">

    <h1>Lista de Facilitadores</h1>

    <table id="lista-facilitadores" class="display" width="100%">
        <thead>
            <tr class="head-table">
                <th colspan="1" rowspan="2">Nombre</th>
                <th colspan="1" rowspan="2" width="120">Centro(s)</th>
                <th colspan="1" rowspan="2" width="150">Perfil</th>
                <th colspan="6" rowspan="1"
                    class="ui-state-default cursos-asignados">Grupo a los que se ha
                    asignado en...</th>
            </tr>
            <tr>
                <th colspan="1" rowspan="1" width="75" class="cursos-asignados">
                    Este mes<br /> (<?php echo ucfirst(format_date($hoy, 'MMMM', 'es_MX')) ?>)
                </th>
                <th colspan="1" rowspan="1" width="130" class="cursos-asignados">
                    Los pr&oacute;ximos meses<br /> (despu&eacute;s de <?php echo ucfirst(format_date($hoy, 'MMMM', 'es_MX')) ?>)
                </th>
                <th colspan="1" rowspan="1" width="130" class="cursos-asignados">
                    Los &uacute;ltimos 2 meses<br /> (<?php echo ucfirst(format_date($dosMeses, 'MMMM', 'es_MX')) ?>
                    - <?php echo ucfirst(format_date($hoy, 'MMMM', 'es_MX')) ?>)
                </th>
                <th colspan="1" rowspan="1" width="130" class="cursos-asignados">
                    Los &uacute;ltimos 6 meses <br /> (<?php echo ucfirst(format_date($seisMeses, 'MMMM', 'es_MX')) ?>
                    - <?php echo ucfirst(format_date($hoy, 'MMMM', 'es_MX')) ?>)
                </th>
                <th colspan="1" rowspan="1" width="75" class="cursos-asignados">
                    Este año<br /> (<?php echo date('Y') ?>)
                </th>
                <th colspan="1" rowspan="1" width="50" class="cursos-asignados">
                    Todos</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($facilitadores as $facilitador): ?>
            <tr id="<?php echo $facilitador['id'] ?>">
                <td><?php echo $facilitador['nombre'] ?></td>
                <td><?php echo $facilitador['centros'] ?></td>
                <td><?php echo $facilitador['perfiles'] ?></td>
                <td class="cursos-asignados class" id="indicator"
                    user="<?php echo $facilitador['id'] ?>"><?php echo ($facilitador['mes'] > 0 ? '<a href="#" class="show-groups" id="'.$facilitador['id'].'" accion="mes">'.$facilitador['mes'].'</a>' : $facilitador['mes'] ) ?>
                </td>
                <td class="cursos-asignados"><?php echo ($facilitador['pos'] > 0 ? '<a href="#" class="show-groups" id="'.$facilitador['id'].'" accion="pos">'.$facilitador['pos'].'</a>' : $facilitador['pos'] ) ?>
                </td>
                <td class="cursos-asignados"><?php echo ($facilitador['bimestre'] > 0 ? '<a href="#" class="show-groups" id="'.$facilitador['id'].'" accion="bimestre">'.$facilitador['bimestre'].'</a>' : $facilitador['bimestre'] ) ?>
                </td>
                <td class="cursos-asignados"><?php echo ($facilitador['semestre'] > 0 ? '<a href="#" class="show-groups" id="'.$facilitador['id'].'" accion="semestre">'.$facilitador['semestre'].'</a>' : $facilitador['semestre'] ) ?>
                </td>
                <td class="cursos-asignados"><?php echo ($facilitador['anual'] > 0 ? '<a href="#" class="show-groups" id="'.$facilitador['id'].'" accion="anual">'.$facilitador['anual'].'</a>' : $facilitador['anual'] ) ?>
                </td>
                <td class="cursos-asignados"><?php echo ($facilitador['todos'] > 0 ? '<a href="#" class="show-groups" id="'.$facilitador['id'].'" accion="todos">'.$facilitador['todos'].'</a>' : $facilitador['todos'] ) ?>
                </td>
            </tr>
            <?php endforeach;?>
        </tbody>
        </tbody>
    </table>

</div>
<script type="text/javascript">
var FTable,
    rowSelect,
    posRow,
    nextUser,
    prevUser;
$(function() {
    FTable = $('#lista-facilitadores').dataTable({
        "bPaginate": true,
        "bLengthChange": true,
        "bFilter": true,
        "bSort": true,
        "bInfo": true,
        "bAutoWidth": false,
        "bJQueryUI": true,
        "sPaginationType": "full_numbers",
        "sDom": '<T><"clear"><"H"lfr>t<"F"ip>',
        "oTableTools": {
            "sSwfPath": "/js/plugins/dataTables/swf/DataTables.swf",
            "aButtons": [
                        "copy",
                        "csv",
                        {
                            "sExtends": "xls",
                            "sFieldSeperator": "|",
                            "sCharSet" : "utf8",
                            "sFileName" : "Detalle Grupos-Facilirtadores.csv"
                        },
                        {
                            "sExtends": "pdf",
                            "sPdfOrientation": "landscape",
                            "sPdfTextSize": 6,
                            "sPdfTableHeaders" : "Nombre|Centros(s)|Perfil|Gruos en este mes|Gruos en los próximos meses|Grupos en los últimos 2 meses|Grupos en los últimos 6 meses|Grupos en este año|Todos",
                            "sPdfOmitRows" : "0",
                            "sPdfHeaderColor" : "0x3FA2D0",
                            "sPdfBgColor" : "0xD6E9EE",
                            "sPdfAlpha"  : 0.5,
                            "sFileName" : "Detalle Grupos-Facilirtadores.pdf",
                            "sTitle" : "Detalle Grupos-Facilirtadores"

                        }
                    ]
            }
    });
});
</script>
