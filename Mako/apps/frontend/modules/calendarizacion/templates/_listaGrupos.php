<?php use_stylesheet('./calendarizacion/_listaGrupos.css')?>

<div id="tabs-by-group" class="tabs-bottom">
    <ul>
        <li><a href="<?php echo url_for('calendarizacion/gruposProximos') ?>"><span>
            </span> Grupos pr&oacute;ximos</a></li>
        <li><a href="<?php echo url_for('calendarizacion/gruposIniciaron') ?>"><span>
            </span> Grupos que han iniciado</a></li>
        <li><a
            href="<?php echo url_for('calendarizacion/gruposFinalizados').'?centro=1' ?>"><span>
            </span> Grupos finalizados</a></li>
    </ul>
</div>

<script type="text/javascript">
var GTable;
var lengthTable;
var urlDataTable;
var order = '';
var direction = '';
var filter = '';
var camp;
$(function() {
    $("#tabs-by-group").tabs({
        heightStyle: "content",
        spinner: '<img src="/imgs/ajax-small.gif" alt="Cargando..."/>',
        select: function(event, ui) {
            $('#grupos').remove();
        },
        load: function(event, ui) {
            loadGrupoTables();
            order = '';
            direction = '';
            filter = '';
            camp = '';
         },
        ajaxOptions: {
        error: function( xhr, status, index, anchor ) {
                $( anchor.hash ).html("No fue posible obtener la lista, intenta refescando la página");
            }
        }
    });
    $( ".tabs-bottom .ui-tabs-nav, .tabs-bottom .ui-tabs-nav > *" )
    .removeClass( "ui-corner-all ui-corner-top" )
    .addClass( "ui-corner-bottom" );

    // move the nav to the bottom
    $( ".tabs-bottom .ui-tabs-nav" ).appendTo( ".tabs-bottom" );

     $('#cambiar-centro-fin').live('click', function(){
           target =$(this).parent().parent().parent().parent().parent().parent();
           $.ajax({
                url  : "<?php echo url_for('calendarizacion/gruposFinalizados')?>",
                data : { centro   : $('#centros-finalizaron').val() },
                beforeSend : function(){
                       $("#tabs-by-group .ui-tabs-nav li.ui-tabs-selected").prepend('<img src="/imgs/ajax-small.gif" alt="Cargando..."/>');
                  },
                success: function(data){
                    $("#tabs-by-group .ui-tabs-nav li.ui-tabs-selected img").remove();
                    target.html(data);
                    loadGrupoTables();
                    },
                error  : function(e){
                        console.log(e);
                    }
            });

        });

    $(".tabs-bottom .ui-tabs-nav, .tabs-bottom .ui-tabs-nav > *").removeClass("ui-corner-all ui-corner-top").addClass("ui-corner-bottom");

    $('.order').live('click', function(){
        order = $(this).attr('order');
        if($(this).hasClass('ui-icon-triangle-1-n'))
        {
            direction = 'desc';
        }
        else
        {
            direction = 'asc';
        }
        loadingGrupoTables($(this));
        $('#grupos').closest('.ui-tabs-panel').load(urlDataTable, { 'page': 1, 'length' : lengthTable, 'order' : order, 'dir' : direction, 'filter': filter, 'camp' : camp }, function(){ loadGrupoTables(); } );
    });
});
function filterWordKey(event, target){
    if(event.keyCode == 13)
    {
        filter = $(target).val();
        camp = $(target).parent().prev().children('select').val();
        loadingGrupoTables(target);
        $('#grupos').closest('.ui-tabs-panel').load(urlDataTable, { 'page': 1, 'length' : lengthTable, 'order' : order, 'dir' : direction, 'filter': filter, 'camp' : camp }, function(){ loadGrupoTables(); } );
    }
}
function filterWordClick(target){
    filter = $(target).parent().prev().children('input').val();
    camp = $(target).parent().prev().prev().children('select').val();
    loadingGrupoTables(target);
    $('#grupos').closest('.ui-tabs-panel').load(urlDataTable, { 'page': 1, 'length' : lengthTable, 'order' : order, 'dir' : direction, 'filter': filter, 'camp' : camp }, function(){ loadGrupoTables(); } );
}
function cleanWord(target){
    filter = '';
    camp = '';
    loadingGrupoTables(target);
    $('#grupos').closest('.ui-tabs-panel').load(urlDataTable, { 'page': 1, 'length' : lengthTable, 'order' : order, 'dir' : direction, 'filter': filter, 'camp' : camp }, function(){ loadGrupoTables(); } );
}
function changeLength(target){
    loadingGrupoTables(target);
    $('#grupos').closest('.ui-tabs-panel').load(urlDataTable, { 'page': 1, 'length' : lengthTable, 'order' : order, 'dir' : direction, 'filter': filter, 'camp' : camp }, function(){ loadGrupoTables(); } );
}
function loadingGrupoTables(target){
    parent = $(target).closest('.ui-tabs-panel');
    lengthTable = parent.children('#detail-groups').children('#filter').children('#grupos_length').children('label').children('select').val();
    parent.children('#detail-groups').children('#table-loader').show();
    parent.children('#detail-groups').children('#table-loader').width(parent.children('#detail-groups').width());
    parent.children('#detail-groups').children('#table-loader').height(parent.children('#detail-groups').height());
    parent.children('#detail-groups').children('#paginador').children('#paginador-loader').show();
}
function loadGrupoTables(){
    $('#paginador-loader').hide();

    GTable = $('#grupos').dataTable({
        "bPaginate": false,
        "bLengthChange": false,
        "bFilter": false,
        "bSort": false,
        "bInfo": false,
        "bAutoWidth": false,
        "bJQueryUI": true,
        "sPaginationType": "full_numbers",
    });
}
</script>
