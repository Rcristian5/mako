<?php use_stylesheet('control_escolar/control_escolar.css') ?>
<?php use_stylesheet('control_escolar/calendarizacion.css') ?>
<?php use_stylesheet('jquery.windows.css') ?>
<?php use_stylesheet('jquery.fullcalendar.css') ?>
<?php use_javascript('jquery.ui.js') ?>
<?php use_javascript('plugins/jquery.windows.js') ?>
<?php use_javascript('plugins/jquery.qtip.js') ?>
<?php use_javascript('plugins/jquery.fullcalendar.js') ?>
<?php use_javascript('control_escolar/calendarizacion.nueva.js') ?>
<script type="text/javascript">
  disabled = [<?php foreach ($dias as $dia): ?> '<?php echo $dia['disable'] ?>', <?php endforeach; ?>];
  disabledDays = [<?php foreach ($dias as $dia): ?>
                  {
                    id  : 'disableds',
                    title  : '<?php echo $dia['title'] ?>',
                    start  : '<?php echo $dia['start'] ?>',
                    allDay : true,
                    className  : 'fc-disable',
                    editable: false,
                    description : '<?php echo $dia['description'] ?>'
                },
                <?php endforeach; ?>
                ];
  cursosCalendarizados = [<?php foreach ($cursos as $curso): ?>
                  {
                    id     : 'calendarizado',
                    title  : '<?php echo $curso['clave'] ?>',
                    start  : '<?php echo substr($curso['dia'], 0,10).' '.$curso['hora_inicio'] ?>',
                    end    : '<?php echo substr($curso['dia'], 0,10).' '.$curso['hora_fin'] ?>',
                    allDay : false,
                    className   : 'fc-calendarizados <?php echo $curso['grupo'] ?> fc-categoria-<?php echo $curso['categoria'] ?>',
                    editable: false,
                    description : '<span class="qtip-header"><b><?php echo $curso['nombre'] ?></b></span> <span class="qtip-title">Hora de inicio:</span> <b><?php echo substr($curso['hora_inicio'], 0,5) ?></b> <br> <span class="qtip-title">Hora de fin:</span>  <b><?php echo substr($curso['hora_fin'],0,5) ?></b> <br> <span class="qtip-title">Aula:</span> <b><?php echo $curso['aula'] ?></b> <br> <span class="qtip-title">Facilitador:</span> <b><?php echo $curso['facilitador'] ?></b> <span></span><span class="ui-widget-content ui-icon ui-icon-pencil edit-facilitador" grupo="<?php echo $curso['id'] ?>"></span><br> <span class="qtip-title">Inscritos:</span> <b><?php echo $curso['cupo'] ?></b> <br> <span class="qtip-cancel"> <input type="button" value="Cancelar curso"  onclick="confirmDelete(<?php echo $curso['id'] ?>)"/> </span> <br> <span class="qtip-change"> <input type="button" value="Transferir alumnos"  onclick="confirmChange(<?php echo $curso['id'] ?>)"/> </span>',
                    cupo :  <?php echo $curso['cupo'] ?>
                },
                <?php endforeach; ?>
                ];
 actualDay = [];
 horaMinima = '08:00';
 horaFinal = '09';
 horaMaxima = '20:00';
 duracionTotal = 0;
 restricciones = '';
 selyear = <?php echo $sel[0]?>;
 selmoth = <?php echo $sel[1]-1?>;
 selday = <?php echo $sel[2]*1?>;
</script>

<div id="ventana-curso">
	<?php include_partial('cursos', array('form' => $form)) ?>
</div>

<div id="windows-wrapper">
	<div id="div-calendarizar" window-title="Calendarizar curso"
		window-state="min">
		<?php if ($sf_user->hasFlash('notice')): ?>
		<div class="flash_notice ui-state-highlight ui-corner-all">
			<span style="float: left; margin-right: 0.3em;"
				class="ui-icon ui-icon-info"></span> <strong><?php echo $sf_user->getFlash('notice') ?>
			</strong>
		</div>
		<?php endif; ?>
		<h2>Calendarizar curso</h2>
		<?php if(count($categorias)>0): ?>
		<p>
			Categoria de curso : <select id="categorias-curso">
				<option value="0">Todas</option>
				<?php foreach($categorias as $categoria):?>
				<option value="<?php echo $categoria->getId()?>">
					<?php echo $categoria->getNombre()?>
				</option>
				<?php endforeach; ?>
			</select>

		</p>
		<?php endif; ?>
		<?php if(count($detalles)>0): ?>
		<div id="dias-semana" class="dias-semana">
			<span class="title-days">Selecciona los días para impartir el curso</span>
			<?php $w = new sfWidgetFormChoice(array('expanded' => true,'multiple' => true,'choices'  => array(1=>'Lunes',2=>'Martes',3=>'Miércoles',4=>'Jueves',5=>'Viernes',6=>'Sábado', 0=>'Domingo'))); ?>
			<?php echo $w->render('week_day') ?>
			<p style="clear: both">&nbsp;</p>
		</div>
		<?php endif; ?>
		<!--Calendario de cursos -->
		<div id="calendar" style="width: 98%; margin: 0 auto"></div>
		<!--Botones de opciones -->
		<p id="select-facilitador">
			<label for="facilitador">Facilitador que impartira el curso</label> <select
				id="facilitador">
				<option value="">--Selecciona un facilitador--</option>
			</select>
		</p>
		<div class="botones_finales" style="text-align: right">
			<?php if(count($detalles)>0): ?>

			<input type="button" id="guardar" value="Finalizar calendarización" />
			<br /> <br /> <input type="button" id="limpiar-seleccion"
				value="Limpiar selección" /> <input type="button" id="comprobar"
				value="Comprobar disponibilidad" /> <input type="button"
				id="abrir-ventana" value="Calendarizar otro curso" /> <input
				type="button" id="abrir-ventana" value="Cacelar Calendarización"
				onclick="location.href = 'new'" />
			<script type="text/javascript">
                $('#week_day_1').attr('checked', true);
                $('#week_day_2').attr('checked', true);
                $('#week_day_3').attr('checked', true);
                $('#week_day_4').attr('checked', true);
                $('#week_day_5').attr('checked', true);
                duracionTotal = <?php echo $detalles['datos']->getDuracion() ?>;
              <?php if($detalles['restriccion_horario']): ?>
                <?php if($detalles['restriccion_horario']->getHorasMinimo()!=''): ?>
                  horaMinima = '<?php echo $detalles['restriccion_horario']->getHorasMinimo('H:i') ?>';
                  horaFinal = parseInt(<?php echo $detalles['restriccion_horario']->getHorasMinimo('H') ?>,10)+1;
                  if(parseInt(horaFinal,10)<10)
                  {
                     horaFinal = '0'+horaFinal;
                  }
                  restricciones =  '<span class="qtip-long-title">Hora mínima de inicio:</span> <b><?php echo $detalles['restriccion_horario']->getHorasMinimo('H:i') ?></b> <br>';
                <?php endif;?>
                <?php if($detalles['restriccion_horario']->getHorasMaximo()!=''): ?>
                    restricciones +=  '<span class="qtip-long-title">Hora máxima de fin:</span> <b><?php echo $detalles['restriccion_horario']->getHorasMaximo('H:i') ?></b> <br>';
                <?php endif;?>
              <?php endif; ?>
              actualDay =  [{
                    id     : 'nuevo-curso',
                    title  : '<?php echo $detalles['datos']->getClave() ?>',
                    start  : '<?php echo $sel[0].'-'.$sel[1].'-'.$sel[2] ?> '+horaMinima,
                    end    : '<?php echo $sel[0].'-'.$sel[1].'-'.$sel[2] ?> '+horaFinal+':00',
                    allDay : false,
                    className  : 'fc-nuevo-curso',
                    editable: true,
                    description : '<span class="qtip-header"><b>Calendarizando <br /><?php echo $detalles['datos']->getNombre() ?> </b></span> '+restricciones
                }];
              console.log(horaFinal);
            </script>

			<?php else: ?>
			<input type="button" id="abrir-ventana"
				value="Calendarizar un nuevo curso" />
			<?php endif;?>
		</div>
		<p>&nbsp;</p>
	</div>
</div>
