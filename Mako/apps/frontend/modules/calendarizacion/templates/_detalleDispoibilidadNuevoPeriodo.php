<?php if($detalle['disponible']['disponible'] == 'si'): ?>
    <?php if($detalle['facilitadorDisponible']['disponible']): ?>
        <h1 class="ok">La nueva fecha de inicio esta disponible</h1>
        <p class="button-set">
            <a href="#" class="ui-widget ui-state-default ui-corner-all bad-button">
                Cancelar
            </a>
            <a href="<?php echo url_for('calendarizacion/updateFechaInicio?grupo='.$detalle['grupo'].'&inicio='.$detalle['inicio'])?>"
               class="ui-widget ui-state-default ui-corner-all">
               Reprogramar fecha de inicio
            </a>
        </p>
    <?php else: ?>
        <h1 class="error">
            El facilitador no esta disponible en el periodo seleccionado.
        </h1>
        <p>
            Raz&oacute;n:
            <b>
                <?php echo $detalle['facilitadorDisponible']['razon'] ?>
            </b>
        </p>
        <?php if(count($detalle['facilitadorDisponible']['grupos']) != 0): ?>
            <b>Grupos a los que esta asignado</b>
            <table width="100%">
                <thead>
                    <tr class="odd">
                        <th class="ui-state-default">Curso</th>
                        <th class="ui-state-default">Grupo</th>
                        <th class="ui-state-default">Fecha de inicio</th>
                        <th class="ui-state-default">Fecha de termino</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($detalle['facilitadorDisponible']['grupos'] as $grupo => $detalle): ?>
                        <td><?php echo $detalle['curso']?></td>
                        <td><?php echo $grupo?></td>
                        <td><?php echo $detalle['inicio']?></td>
                        <td><?php echo $detalle['fin']?></td>
                    <?php endforeach;?>
                </tbody>
            </table>
        <?php endif; ?>
        <p class="button-set">
            <a href="#" class="ui-widget ui-state-default ui-corner-all bad-button">Retroceder</a>
        </p>
    <?php endif ?>
<?php else: ?>
    <h1 class="error">
        No es posible reprogramar el curso en la nueva fecha de inicio.
    </h1>
    <p>
        Puede ser que el aula o el horario del curso no esten disponibles, elige otra fecha.
    </p>
    <p class="button-set">
        <a href="#" class="ui-widget ui-state-default ui-corner-all bad-button">
            Retroceder
        </a>
    </p>
<?php endif;?>
