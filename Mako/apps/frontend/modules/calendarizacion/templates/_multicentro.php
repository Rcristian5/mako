<?php use_stylesheet('control_escolar/cursos_disponibles.css') ?>

<p style="text-align: right; width: 99%;">
    <input type="button" value="Cancelar"
        onclick="location.href = '<?php echo url_for('calendarizacion/index') ?>'" />
</p>
<p class="botones_finales" style="width: 99%"></p>

<div id="dialog-horario" title="Calendarizar nuevo curso">

    <div id="resumen-calendarizacion">
        <p
            style="text-align: center; padding: 20px; margin: 0; font-size: 14px;">
            <b>Selecciona un d&iacute;a</b>

        </p>
    </div>
</div>

<script type="text/javascript">

var calendarizados = [<?php foreach ($nueva as $dia): ?>
                  {
                    id        : 'calendarizados',
                    title     : '<?php echo substr($dia['grupo'], 0 , 10).'…' ?>',
                    start     : '<?php echo $dia['inicio'] ?>',
                    end       : '<?php echo $dia['fin'] ?>',
                    allDay    : false,
                    className : 'fc-calendarizados',
                    editable: false
                },
                <?php endforeach; ?>
                ];
$('#calendar').fullCalendar( 'addEventSource', calendarizados);
    $('#dialog-horario').dialog({
        width  : 250,
        height : 340,
        resizable: false,
        position: ['left','top']
    });


</script>
