<?php use_helper('Date') ?>

<h2>
    <?php switch ($detalle['periodo']):
          case "mes": ?>
    <?php $periodo = ucfirst(format_date($detalle['hoy'], 'MMMM', 'es_MX')).' de '.date('Y') ?>
    <a href="#" period="0"
        class="ui-state-default ui-corner-all ui-state-disabled prev-period"><span
        class="ui-icon ui-icon-circle-triangle-w"></span> </a> Este mes (
    <?php echo ucfirst(format_date($detalle['hoy'], 'MMMM', 'es_MX')) ?>
    ) <a href="#" period="pos"
        user="<?php echo $detalle['idFacilitador'] ?>"
        class="ui-state-default ui-corner-all next-period"
        title="Los próximos meses (después de <?php echo ucfirst(format_date($detalle['hoy'], 'MMMM', 'es_MX')) ?>)"><span
        class="ui-icon ui-icon-circle-triangle-e"></span> </a>
    <?php break; ?>
    <?php case "pos": ?>
    <?php $periodo = 'Posterior a '.ucfirst(format_date($detalle['hoy'], 'MMMM', 'es_MX')).' de '.date('Y') ?>
    <a href="#" period="mes" user="<?php echo $detalle['idFacilitador'] ?>"
        class="ui-state-default ui-corner-all prev-period"
        title="Este mes (<?php echo ucfirst(format_date($detalle['hoy'], 'MMMM', 'es_MX')) ?>)"><span
        class="ui-icon ui-icon-circle-triangle-w"></span> </a> Los
    pr&oacute;ximos meses (despu&eacute;s de
    <?php echo ucfirst(format_date($detalle['hoy'], 'MMMM', 'es_MX')) ?>
    ) <a href="#" period="bimestre"
        user="<?php echo $detalle['idFacilitador'] ?>"
        class="ui-state-default ui-corner-all next-period"
        title="Los últimos 2 meses (<?php echo ucfirst(format_date($detalle['dosMeses'], 'MMMM', 'es_MX')) ?> - <?php echo ucfirst(format_date($detalle['hoy'], 'MMMM', 'es_MX')) ?>)"><span
        class="ui-icon ui-icon-circle-triangle-e"></span> </a>
    <?php break; ?>
    <?php case 'bimestre': ?>
    <?php $periodo = ucfirst(format_date($detalle['dosMeses'], 'MMMM', 'es_MX')).'-'.ucfirst(format_date($detalle['hoy'], 'MMMM', 'es_MX')) ?>
    <a href="#" period="pos" user="<?php echo $detalle['idFacilitador'] ?>"
        class="ui-state-default ui-corner-all prev-period"
        title="Los próximos meses (despu&eacute;s de <?php echo ucfirst(format_date($detalle['hoy'], 'MMMM', 'es_MX')) ?>)"><span
        class="ui-icon ui-icon-circle-triangle-w"></span> </a> Los
    &uacute;ltimos 2 meses (
    <?php echo ucfirst(format_date($detalle['dosMeses'], 'MMMM', 'es_MX')) ?>
    -
    <?php echo ucfirst(format_date($detalle['hoy'], 'MMMM', 'es_MX')) ?>
    ) <a href="#" period="semestre"
        user="<?php echo $detalle['idFacilitador'] ?>"
        class="ui-state-default ui-corner-all next-period"
        title="Los &uacute;ltimos 6 meses (<?php echo ucfirst(format_date($detalle['seisMeses'], 'MMMM', 'es_MX')) ?> - <?php echo ucfirst(format_date($detalle['hoy'], 'MMMM', 'es_MX')) ?>)"><span
        class="ui-icon ui-icon-circle-triangle-e"></span> </a>
    <?php break; ?>
    <?php case 'semestre': ?>
    <?php $periodo = ucfirst(format_date($detalle['seisMeses'], 'MMMM', 'es_MX')).'-'.ucfirst(format_date($detalle['hoy'], 'MMMM', 'es_MX')) ?>
    <a href="#" period="bimestre"
        user="<?php echo $detalle['idFacilitador'] ?>"
        class="ui-state-default ui-corner-all prev-period"
        title="Los &uacute;ltimos 2 meses<br/>(<?php echo ucfirst(format_date($detalle['dosMeses'], 'MMMM', 'es_MX')) ?> - <?php echo ucfirst(format_date($detalle['hoy'], 'MMMM', 'es_MX')) ?>)"><span
        class="ui-icon ui-icon-circle-triangle-w"></span> </a> Los
    &uacute;ltimos 6 meses (
    <?php echo ucfirst(format_date($detalle['seisMeses'], 'MMMM', 'es_MX')) ?>
    -
    <?php echo ucfirst(format_date($detalle['hoy'], 'MMMM', 'es_MX')) ?>
    ) <a href="#" period="anual"
        user="<?php echo $detalle['idFacilitador'] ?>"
        class="ui-state-default ui-corner-all next-period"
        title="Este año (<?php echo date('Y') ?>)"><span
        class="ui-icon ui-icon-circle-triangle-e"></span> </a>
    <?php break; ?>
    <?php case 'anual': ?>
    <?php $periodo = 'Año '.date('Y') ?>
    <a href="#" period="semestre"
        user="<?php echo $detalle['idFacilitador'] ?>"
        class="ui-state-default ui-corner-all prev-period"
        title="Los &uacute;ltimos 6 meses (<?php echo ucfirst(format_date($detalle['seisMeses'], 'MMMM', 'es_MX')) ?> - <?php echo ucfirst(format_date($detalle['hoy'], 'MMMM', 'es_MX')) ?>)"><span
        class="ui-icon ui-icon-circle-triangle-w"></span> </a> Este año (
    <?php echo date('Y') ?>
    ) <a href="#" period="todos"
        user="<?php echo $detalle['idFacilitador'] ?>"
        class="ui-state-default ui-corner-all next-period"
        title="Todos los grupos"><span
        class="ui-icon ui-icon-circle-triangle-e"></span> </a>
    <?php break; ?>
    <?php case 'todos': ?>
    <?php $periodo = 'Todos los grupos asignados a '.ucfirst(format_date($detalle['hoy'], 'MMMM', 'es_MX')).' de '.date('Y') ?>
    <a href="#" period="anual"
        user="<?php echo $detalle['idFacilitador'] ?>"
        class="ui-state-default ui-corner-all prev-period"
        title="Este año (<?php echo date('Y') ?>)"><span
        class="ui-icon ui-icon-circle-triangle-w"></span> </a> Todos <a
        href="#" period="0"
        class="ui-state-default ui-corner-all ui-state-disabled next-period"><span
        class="ui-icon ui-icon-circle-triangle-e "></span> </a>
    <?php break; ?>
    <?php endswitch; ?>
</h2>
<table id="tabla-facilitadores-grupos" class="display" width="100%">
    <thead>
        <tr>
            <th>Curso</th>
            <th>Grupo</th>
            <th>Inicio</th>
            <th>Termino</th>
            <th>Num. Inscritos</th>
            <th>Num. Preinscritos</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($detalle['grupos'] as $grupo): ?>
        <tr>
            <td><?php echo $grupo['curso'] ?></td>
            <td><?php echo $grupo['grupo'] ?></td>
            <td><?php echo $grupo['inicio'] ?></td>
            <td><?php echo $grupo['fin'] ?></td>
            <td><?php echo $grupo['inscritos'] ?></td>
            <td><?php echo $grupo['preinscritos'] ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<script type="text/javascript">
var DTable;
$(function() {
    DTable = $('#tabla-facilitadores-grupos').dataTable({
        "bPaginate": false,
        "bLengthChange": false,
        "bFilter": true,
        "bSort": true,
        "bInfo": true,
        "bAutoWidth": false,
        "bJQueryUI": true,
        "sDom": '<T><"clear"><"H"lfr>t<"F"ip>',
        "aaSorting": [[2, 'asc']],
        "oTableTools": {
            "sSwfPath": "/js/plugins/dataTables/swf/DataTables.swf",
            "aButtons": [
                        "copy",
                        "csv",
                        {
                            "sExtends": "xls",
                            "sFieldSeperator": "|",
                            "sCharSet": "utf8",
                            "sFileName" : "Detalle Grupos-<?php echo $detalle['facilitador']?>.csv"
                        },
                        {
                            "sExtends": "pdf",
                            "sPdfTextSize": 6,
                            "sPdfHeaderColor" : "0x3FA2D0",
                            "sPdfBgColor" : "0xD6E9EE",
                            "sPdfAlpha"  : 0.5,
                            "sFileName" : "Detalle <?php echo $detalle['facilitador'] ?>-<?php echo $periodo ?>.pdf",
                            "sTitle" : "Detalle <?php echo $detalle['facilitador'] ?>, <?php echo $periodo ?>",
                            "sPdfMessage" : "Centros a los que esta asignado: <?php echo $detalle['centros'] ?>. Perfiles que posee: <?php echo $detalle['perfiles'] ?>"
                        }
                    ]
            }
    });
});
</script>
