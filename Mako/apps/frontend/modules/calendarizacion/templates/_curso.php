
<table style="padding-top: 15px">
    <tr>
        <th>
            <label for="input-curso">Curso</label>
        </th>
        <td>
            <select id="input-curso">
                <option value="">--Selecciona un curso--</option>
                <?php foreach ($cursos as $curso): ?>
                    <option value="<?php echo $curso->getId()?>">
                        <?php echo $curso->getNombre() ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </td>
    </tr>
</table>
