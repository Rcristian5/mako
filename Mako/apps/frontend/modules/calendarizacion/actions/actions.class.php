<?php

/**
 * calendarizacion actions.
 *
 * @package    mako
 * @subpackage calendarizacion
 * @author     David Galindo Garcia
 * @version    SVN: $Id: actions.class.php,v 1.32 2011-11-04 18:54:12 david Exp $
 */
class calendarizacionActions extends sfActions {

    public function executeIndex(sfWebRequest $request, $data) {
        $Autenticado = $this->getUser()->isAuthenticated();
        if (!$Autenticado)
            $this->redirect('/index.php');

        /*
          Verificamos que tenga permisos para ejecutar agregar, editar, borrar
          Solo a los roles de Promotor y Supervisor pueden ver la info, No editar
          Solo a los roles __ pueden ver y editar información
          BGPJ Bet Gader
         */
        $Usuario = $this->getUser()->getAttribute('usuario');
        if (!(
                $Usuario->getRolId() == sfConfig::get('app_rol_promotor_id') || 
                $Usuario->getRolId() == sfConfig::get('app_rol_supervisor_id') ||
                $Usuario->getRolId() == sfConfig::get('app_rol_coordinador_academico_id')
                )) {
            error_log('El Usuario ' . $Usuario->getNombreCompleto() . 'No tiene permisos para ingresar al modulo Calendarizacion');
            $this->redirect('/index.php');
        }

        $IpParam = $this->request->getParameter('ip');
        $Ip = ($IpParam == '') ? Comun::ipReal() : $IpParam;
        error_log('Ip:                  ' . $Ip . '              ');

        $Computadora = ComputadoraPeer::getPcPorIp($Ip);
        $TipoPc = ($Computadora != null) ? $Computadora->getTipoId() : sfConfig::get('app_externa_tipo_pc_id');
        $Usuario = $this->getUser()->getAttribute('usuario');

        $this->Editar = false;
        if (
                $Usuario->getRolId() == sfConfig::get('app_rol_coordinador_academico_id')
        ) {
            error_log('Puede modificar en Calendarizacion');
            $this->Editar = true;
        }
        $this->getUser()->setAttribute('EditarCalendarizacion', $this->Editar);
        $usuario = $this->getUser()->getAttribute('usuario');

        $this->forwardUnless($usuario, 'sesiones', 'index');

        $this->adminControl = ($usuario->getRolId() == sfConfig::get('app_rol_administrador_control_escolar_id') ? true : false);
        $this->detalle = true;
        $this->centros = CentroPeer::listaCentrosActivos();
        $this->noLaborables = DiasNoLaborablesPeer::doSelect(new Criteria());
    }

    public function executeCurso(sfWebRequest $request) {
        $this->forward404unless($request->isXmlHttpRequest());

        return $this->renderPartial('curso', array('cursos' => CursoPeer::listaCursosActivos()));
    }

    public function executeAula(sfWebRequest $request) {
        $this->forward404unless($request->isXmlHttpRequest());

        return $this->renderPartial('aula', array('aulas' => AulaPeer::listaAulasActivas($request->getParameter('centro'))));
    }

    public function executeNueva(sfWebRequest $request) {
        $this->noLaborables = DiasNoLaborablesPeer::doSelect(new Criteria());
        $this->nueva = GrupoPeer::listaGruposCalendarizados($request->getParameter('centro'), $request->getParameter('aula'));

        $this->setTemplate('index');
    }

    /**
     * Calendarizacion multicentro
     *
     * @param sfWebRequest $request
     */
    public function executeNueva_multicentro(sfWebRequest $request) {
        $this->noLaborables = DiasNoLaborablesPeer::doSelect(new Criteria());
        $this->nuevaMulticentro = GrupoPeer::listaGruposCalendarizados($request->getParameter('centro'), $request->getParameter('aula'));

        $this->setTemplate('index');
    }

    public function executeForm(sfWebRequest $request) {
        $this->forward404unless($request->isXmlHttpRequest());
        $id = $request->getParameter('evento');
        $dia = $request->getParameter('dia');
        $fecha = $request->getParameter('fecha');
        $curso = CursoPeer::retrieveByPK($request->getParameter('curso'));
        $aula = AulaPeer::retrieveByPK($request->getParameter('aula'));
        $centro = CentroPeer::retrieveByPK($request->getParameter('centro'));
        $default = $request->getParameter('ddefault');

        if ($id != "null") {
            //esto es de otra cosa :)
            //return $this->renderPartial('form', array('form'=>new DiasNoLaborablesForm(DiasNoLaborablesPeer::retrieveByPK($id)), 'dia'=>$dia));
        }

        return $this->renderPartial('form', array('form' => new HorarioForm(null, array('fecha' => $fecha, 'curso' => $curso->getId(), 'aula' => $aula->getId(), 'centro' => $centro->getId(), 'default' => $default)),
                    'fecha' => $fecha,
                    'dia' => $dia,
                    'curso' => $curso->getNombre(),
                    'cursoId' => $curso->getId(),
                    'aula' => $aula->getNombre(),
                    'aulaId' => $aula->getId(),
                    'centro' => $centro->getNombre(),
                    'centroId' => $centro->getId(),
                    'clave' => $curso->getClave()
        ));
    }

    public function executeDisponibilidad(sfWebRequest $request) {
        $this->forward404unless($request->isXmlHttpRequest());
        $dias = array('lunes' => $request->getParameter('lunes'),
            'martes' => $request->getParameter('martes'),
            'miercoles' => $request->getParameter('miercoles'),
            'jueves' => $request->getParameter('jueves'),
            'viernes' => $request->getParameter('viernes'),
            'sabado' => $request->getParameter('sabado'),
        );
        $horas = array('hi_lu' => $request->getParameter('hi_lu'), 'hf_lu' => $request->getParameter('hf_lu'),
            'hi_ma' => $request->getParameter('hi_ma'), 'hf_ma' => $request->getParameter('hf_ma'),
            'hi_mi' => $request->getParameter('hi_mi'), 'hf_mi' => $request->getParameter('hf_mi'),
            'hi_ju' => $request->getParameter('hi_ju'), 'hf_ju' => $request->getParameter('hf_ju'),
            'hi_vi' => $request->getParameter('hi_vi'), 'hf_vi' => $request->getParameter('hf_vi'),
            'hi_sa' => $request->getParameter('hi_sa'), 'hf_sa' => $request->getParameter('hf_sa'),
        );
        $noLaborables = DiasNoLaborablesPeer::listaNoLaborables();

        $fechasJS = $this->generaDias($request->getParameter('inicio'), $request->getParameter('curso'), $dias, $horas, $noLaborables, true);
        $fechasPHP = $this->generaDias($request->getParameter('inicio'), $request->getParameter('curso'), $dias, $horas, $noLaborables);
        $fechaFinal = explode('-', $fechasJS[count($fechasJS) - 1]['fecha']);

        $arreglo = array('fechas' => $fechasJS,
            'fechaFinal' => date("F d, Y H:i:s", mktime(0, 0, 0, $fechaFinal[1], $fechaFinal[2], $fechaFinal[0])),
            'disponible' => HorarioPeer::comprobarPeriodo($request->getParameter('centro'), $request->getParameter('aula'), $request->getParameter('inicio'), $fechasJS[count($fechasJS) - 1]['fecha'], $fechasPHP),
            'facilitadores' => HorarioPeer::facilitadoresDisponibles($request->getParameter('curso'), $request->getParameter('centro'), $request->getParameter('aula'), $fechasPHP, $request->getParameter('inicio'), $fechasJS[count($fechasJS) - 1]['fecha'])
        );

        $this->getResponse()->setHttpHeader('Content-type', 'text/json');

        return $this->renderText(json_encode($arreglo));
    }

    /**
     * Comprueba si se ha sincronizado ya un grupo para eliminarlo
     * @param sfWebRequest $request
     */
    public function executeVerificarSincronizacion(sfWebRequest $request) {
        $this->forward404unless($request->isXmlHttpRequest());
        $pkAsync = $request->getParameter('group');
        $criteria = new Criteria();
        $criteria->add(SincroniaPeer::PK_ASYNC, $pkAsync);
        $sincronia = SincroniaPeer::doSelectOne($criteria);

        return $this->renderText(json_encode(array('sincronizado' => true ? 1 : 0)));
    }

    public function executeTransferir(sfWebRequest $request) {
        $this->forward404Unless($grupo = GrupoPeer::retrieveByPk($request->getParameter('id')), sprintf('El grupo no existe (%s).', $request->getParameter('id')));
        $d = new Criteria();
        $d->add(DiasPeer::HORARIO_ID, $grupo->getHorarioId());
        $this->dia = DiasPeer::doSelectOne($d);
        $this->grupo = $grupo;
        $this->tranferencia = GrupoPeer::obtenDatosTransferencia($grupo);
        $this->meses = array(1 => 'Enero', 2 => 'Febrero',
            3 => 'Marzo', 4 => 'Abril',
            5 => 'Mayo', 6 => 'Junio',
            7 => 'Julio', 8 => 'Agosto',
            9 => 'Septiembre', 10 => 'Octubre',
            11 => 'Noviembre', 12 => 'Diciembre',
        );
        $this->dias = array(1 => 'Lunes', 2 => 'Martes',
            3 => 'Miércoles', 4 => 'Jueves',
            5 => 'Viernes', 6 => 'Sábado',
            7 => 'Domingo');
    }

    public function executeDefault(sfWebRequest $request) {
        $this->dias = DiasNoLaborablesPeer::diasNoLaborables();
        $this->form = new GrupoForm();
    }

    public function executeCreate(sfWebRequest $request) {
        $this->forward404Unless($request->isMethod(sfRequest::POST));
        $fecha = $request->getParameter('fecha');
        $curso = $request->getParameter('curso');
        $aula = $request->getParameter('aula');
        $centro = $request->getParameter('centro');
        $this->form = new HorarioForm(null, array('fecha' => $fecha, 'curso' => $curso, 'aula' => $aula, 'centro' => $centro));

        $this->processForm($request, $this->form);

        $this->setTemplate('index');
    }

    public function executeUpdateData(sfWebRequest $request) {
        if ($request->isXmlHttpRequest()) {
            $alumno = $request->getParameter('alumno');
            $grupo = $request->getParameter('grupo');
            $nuevo = $request->getParameter('nuevo');
            GrupoPeer::updateGrupo($alumno, $grupo, $nuevo);
            $this->dispatcher->notify(new sfEvent($this, 'grupo.actualizado', array('grupo_id_original' => $grupo, 'alumno_id' => $alumno, 'grupo_id_nuevo' => $nuevo)));
            $this->getResponse()->setHttpHeader('Content-Type', 'application/json; charset=utf-8');

            return $this->renderText(json_encode(array()));
        }
    }

    public function executeFacilitadores(sfWebRequest $request) {
        if ($request->isXmlHttpRequest()) {
            $fechas = explode('|', $request->getParameter('dates'));
            $inicio = explode('|', $request->getParameter('inicio'));
            $fin = explode('|', $request->getParameter('fin'));
            $horario = array('fecha' => $fechas[0], 'hora_inicio' => $inicio[0], 'hora_fin' => $fin[0]);
            $arreglo = UsuarioPeer::getFacilitadoresCapacidad($this->getUser()->getAttribute('curso_calendarizado'), $horario);
            $this->getResponse()->setHttpHeader('Content-Type', 'application/json; charset=utf-8');

            return $this->renderText(json_encode($arreglo));
        }
    }

    public function executeCambiarFacilitadores(sfWebRequest $request) {
        if ($request->isXmlHttpRequest()) {
            $grupo = GrupoPeer::retrieveByPK($request->getParameter('grupo'));
            $this->getUser()->setAttribute('cambio_facilitador', $grupo->getId());
            $curso = CursoPeer::retrieveByPK($grupo->getCursoId());
            $c = new Criteria();
            $c->add(DiasPeer::HORARIO_ID, $grupo->getHorarioId());
            $dias = DiasPeer::doSelectOne($c);
            $horario = array('fecha' => $dias->getDia(), 'hora_inicio' => $dias->getHoraInicio(), 'hora_fin' => $dias->getHoraFin());
            $arreglo = UsuarioPeer::getFacilitadoresCapacidad($curso, $horario);
            error_log($arreglo[0]['id']);
            $this->getResponse()->setHttpHeader('Content-Type', 'application/json; charset=utf-8');

            return $this->renderText(json_encode($arreglo));
        }
    }

    public function executeEditGroup(sfWebRequest $request) {
        $this->forward404Unless($grupo = GrupoPeer::retrieveByPk($request->getParameter('group')), sprintf('El grupo no existe (%s).', $request->getParameter('group')));
        $this->forward404Unless($facilitador = UsuarioPeer::retrieveByPk($request->getParameter('facilitador')), sprintf('El facilitador no existe (%s).', $request->getParameter('facilitador')));

        $grupo->setFacilitadorId($facilitador->getId());
        $grupo->save();

        $this->dispatcher->notify(new sfEvent($grupo, 'grupo.modificado', array('grupo' => $grupo)));
        $this->getUser()->setFlash('notice', 'Se actualizó el facilitador asignado al grupo ' . $grupo->getClave() . '.');

        $this->redirect('calendarizacion/index');
    }

    public function executeUpdate(sfWebRequest $request) {
        $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
        $this->forward404Unless($Grupo = GrupoPeer::retrieveByPk($request->getParameter('id')), sprintf('Object Grupo does not exist (%s).', $request->getParameter('id')));
        $this->form = new GrupoForm($Grupo);

        $this->processForm($request, $this->form);

        $this->setTemplate('edit');
    }

    public function executeDelete(sfWebRequest $request) {
        $this->forward404Unless($grupo = GrupoPeer::retrieveByPk($request->getParameter('id')), sprintf('El grupo no existe (%s).', $request->getParameter('id')));
        $this->getUser()->setFlash('notice', 'Grupo "' . $grupo->getClave() . '" borrado');

        $grupo->setActivo(false);
        $grupo->save();
        $this->dispatcher->notify(new sfEvent($grupo, 'grupo.modificado', array('grupo' => $grupo)));

        $this->redirect('calendarizacion/index');
    }

    public function executeAjaxFechas(sfWebRequest $request) {
        if ($request->isXmlHttpRequest()) {

            $fechas = explode('|', $request->getParameter('dates'));
            $inicio = explode('|', $request->getParameter('inicio'));
            $fin = explode('|', $request->getParameter('fin'));
            $horario_id = GrupoPeer::saveHorario($fechas[0], $fechas[count($fechas) - 1], $this->getUser()->getAttribute('usuario')->getId());
            GrupoPeer::saveDias($fechas, $inicio, $fin, $horario_id);
            $datos = array('curso' => $this->getUser()->getAttribute('curso_calendarizado'),
                'aula' => $this->getUser()->getAttribute('aula_calendarizado'),
                'horario_id' => $horario_id,
                'operador_id' => $this->getUser()->getAttribute('usuario')->getId(),
                'facilitador_id' => $request->getParameter('facilitador'));
            GrupoPeer::saveGrupo($datos);

            $this->getResponse()->setHttpHeader('Content-Type', 'application/json; charset=utf-8');

            return $this->renderText(json_encode(array()));
        }
    }

    /**
     * Accion AJAX para mostrar el detalle de los grupos
     * por iniciar
     * @param sfWebRequest $request
     */
    public function executeListaGrupos(sfWebRequest $request) {
        $this->forward404unless($request->isXmlHttpRequest());

        $tipoCurso = 'Cursos que han iniciado';
        $detalle = GrupoPeer::listaGruposCalendarizados(null, null, 'actuales');

        return $this->renderPartial('listaGrupos', array('detalles' => $detalle, 'tipoCurso' => $tipoCurso));
    }

    /**
     * Accion AJAX para mostrar los grupos proximos a iniciar
     * @param sfWebRequest $request
     */
    public function executeGruposProximos(sfWebRequest $request) {
        $this->forward404unless($request->isXmlHttpRequest());

        $usuario = $this->getUser()->getAttribute('usuario');
        $adminControl = (($usuario->getRolId() == sfConfig::get('app_rol_administrador_control_escolar_id')) ? true : false);
        $tipoCurso = 'Cursos próximos';
        $detalle = GrupoPeer::listaGruposCalendarizados(
                        null, null, 'proximos', $request->getParameter('page'), $request->getParameter('length'), $request->getParameter('order'), $request->getParameter('dir'), $request->getParameter('filter'), $request->getParameter('camp')
        );

        $detalles = $detalle;

        foreach ($detalle as $det) {
            $alumnos = GrupoPeer::getAlumnosInscritosYpreInscritos($det);

            $det->inscritos = $alumnos['inscritos'];
            $det->preinscritos = $alumnos['preinscritos'];
        }

        return $this->renderPartial('gruposCalendarizados', array(
                    'action' => 'Proximos',
                    'adminControl' => $adminControl,
                    'detalles' => $detalles,
                    'grupos' => $detalle->getResults(),
                    'tipoCurso' => $tipoCurso,
                    'proximos' => true,
                    'length' => $request->getParameter('length'),
                    'order' => $request->getParameter('order') ? $request->getParameter('order') : 'centro',
                    'dir' => $request->getParameter('dir') ? $request->getParameter('dir') : 'asc',
                    'filter' => $request->getParameter('filter') ? $request->getParameter('filter') : '',
                    'camp' => $request->getParameter('camp') ? $request->getParameter('camp') : 'centro'
        ));
    }

    /**
     * Accion AJAX para mostrar los grupos que han inciado
     * @param sfWebRequest $request
     */
    public function executeGruposIniciaron(sfWebRequest $request) {
        $this->forward404unless($request->isXmlHttpRequest());
        $usuario = $this->getUser()->getAttribute('usuario');
        $adminControl = ($usuario->getRolId() == sfConfig::get('app_rol_administrador_control_escolar_id') ? true : false);
        $tipoCurso = 'Cursos que han iniciado';
        //listaGruposCalendarizados($centro = null, $aula = null, $tipo = null, $pag, $length, $order, $dir, $filter, $camp)
        $detalle = GrupoPeer::listaGruposCalendarizados(
                        null, null, 'actuales', $request->getParameter('page'), $request->getParameter('length'), $request->getParameter('order'), $request->getParameter('dir'), $request->getParameter('filter'), $request->getParameter('camp')
        );

        $detalles = $detalle;

        foreach ($detalle as $det) {
            $alumnos = GrupoPeer::getAlumnosInscritosYpreInscritos($det);

            $det->inscritos = $alumnos['inscritos'];
            $det->preinscritos = $alumnos['preinscritos'];
        }

        return $this->renderPartial('gruposCalendarizados', array(
                    'action' => 'Iniciaron',
                    'adminControl' => $adminControl,
                    'detalles' => $detalle,
                    'grupos' => $detalle->getResults(),
                    'tipoCurso' => $tipoCurso,
                    'iniciaron' => true,
                    'length' => $request->getParameter('length'),
                    'order' => $request->getParameter('order') ? $request->getParameter('order') : 'centro',
                    'dir' => $request->getParameter('dir') ? $request->getParameter('dir') : 'asc',
                    'filter' => $request->getParameter('filter') ? $request->getParameter('filter') : '',
                    'camp' => $request->getParameter('camp') ? $request->getParameter('camp') : 'centro'
        ));
    }

    /**
     * Accion AJAX para mostrar los grupos que finalizaron
     * @param sfWebRequest $request
     */
    public function executeGruposFinalizados(sfWebRequest $request) {
        $this->forward404unless($request->isXmlHttpRequest());

        $tipoCurso = 'Cursos finalizados';
        $usuario = $this->getUser()->getAttribute('usuario');
        $adminControl = ($usuario->getRolId() == sfConfig::get('app_rol_administrador_control_escolar_id') ? true : false);
        $detalle = GrupoPeer::listaGruposCalendarizados(
                        null, null, 'finalizados', $request->getParameter('page'), $request->getParameter('length'), $request->getParameter('order'), $request->getParameter('dir'), $request->getParameter('filter'), $request->getParameter('camp')
        );

        $detalles = $detalle;

        foreach ($detalle as $det) {
            $alumnos = GrupoPeer::getAlumnosInscritosYpreInscritos($det);

            $det->inscritos = $alumnos['inscritos'];
            $det->preinscritos = $alumnos['preinscritos'];
        }

        return $this->renderPartial('gruposCalendarizados', array(
                    'action' => 'Finalizados',
                    'adminControl' => $adminControl,
                    'detalles' => $detalle,
                    'grupos' => $detalle->getResults(),
                    'centros' => $centros,
                    'tipoCurso' => $tipoCurso,
                    'finalizaron' => true,
                    'length' => $request->getParameter('length'),
                    'order' => $request->getParameter('order') ? $request->getParameter('order') : 'centro',
                    'dir' => $request->getParameter('dir') ? $request->getParameter('dir') : 'asc',
                    'filter' => $request->getParameter('filter') ? $request->getParameter('filter') : '',
                    'camp' => $request->getParameter('camp') ? $request->getParameter('camp') : 'centro'
        ));
    }

    /**
     * Accion AJAX para mostrar el detalle de los facilitadores
     * @param sfWebRequest $request
     */
    public function executeListaFacilitadores(sfWebRequest $request) {
        $this->forward404unless($request->isXmlHttpRequest());
        $hoy = date('d-m-Y');
        $dosMeses = mktime(date('H'), date('i'), date('s'), date('n') - 1, date('j'), date('Y'));
        $seisMeses = mktime(date('H'), date('i'), date('s'), date('n') - 5, date('j'), date('Y'));
        return $this->renderPartial('listaFacilitadores', array('hoy' => $hoy, 'dosMeses' => $dosMeses, 'seisMeses' => $seisMeses, 'facilitadores' => GrupoPeer::listaFacilitadores()));
    }

    /**
     * Accion AJAX para mostrar el la lista de los grupos asignados a un facilitador
     * @param sfWebRequest $request
     * @return Ambigous <sfView::NONE, string>
     */
    public function executeDetalleFacilitadorGrupos(sfWebRequest $request) {
        $this->forward404unless($request->isXmlHttpRequest());

        $detalle = GrupoPeer::detalleGruposFacilitador($request->getParameter('ident'), $request->getParameter('period'));
        // $grupos  = $detalle->getResults();

        foreach ($detalle['grupos'] as $id => $grupo) {
            $alumnos = GrupoPeer::getAlumnosInscritosYpreInscritos(GrupoPeer::retrieveByPK($grupo['grupoId']));

            $detalle['grupos'][$id]['inscritos'] = $alumnos['inscritos'];
            $detalle['grupos'][$id]['preinscritos'] = $alumnos['preinscritos'];
        }

        return $this->renderPartial('detalleFacilitadorGrupos', array(
                    'detalle' => $detalle,
                    'grupos' => $grupos,
                    'siguienteFacilitador' => $request->getParameter('next'),
                    'anteriorFacilitador' => $request->getParameter('prev'),
                    'periodo' => $request->getParameter('period'),
        ));
    }

    /**
     * Accion AJAX para mostrar el formulario para editar un facilitador de un grupo
     * @param sfWebRequest $request
     * @return Ambigous <sfView::NONE, string>
     */
    public function executeEditFacilitador(sfWebRequest $request) {
        $this->forward404unless($request->isXmlHttpRequest());
        $this->forward404Unless($grupo = GrupoPeer::retrieveByPk($request->getParameter('group')), sprintf('El grupo no existe(%s).', $request->getParameter('group')));
        $fechas = array();
        foreach ($grupo->getHorario()->getFechasHorarios() as $dia) {
            $fecha[$dia->getFecha('Y-m-d')] = array(
                'fecha' => $dia->getFecha('Y-m-d'),
                'dia_semana' => $dia->getFecha('Y-m-d'),
                'hi' => $dia->getHoraInicio('H:i'),
                'hf' => $dia->getHoraFin('H:i'),
                'hih' => $dia->getHoraInicio('H'),
                'hfh' => $dia->getHoraFin('H'),
                'inicio' => $dia->getFecha('Y-m-d') . ' ' . $dia->getHoraInicio('H:i'),
                'fin' => $dia->getFecha('Y-m-d') . ' ' . $dia->getHoraFin('H:i')
            );
        }

        return $this->renderPartial('editarFacilitador', array(
                    'grupo' => $grupo,
                    'facilitadores' => HorarioPeer::facilitadoresDisponibles($grupo->getCursoId(), $grupo->getCentroId(), $grupo->getAulaId(), $fechas, $grupo->getHorario()->getFechaInicio('Y-m-d'), $grupo->getHorario()->getFechaFin('Y-m-d'), $grupo->getFacilitadorId())
        ));
    }

    public function executeEditVariables(sfWebRequest $request) {
        $this->forward404unless($request->isXmlHttpRequest());
        $this->forward404Unless($grupo = GrupoPeer::retrieveByPk($request->getParameter('group')), sprintf('El grupo no existe(%s).', $request->getParameter('group')));

        $fechas = array();
        foreach ($grupo->getHorario()->getFechasHorarios() as $dia) {
            $fecha[$dia->getFecha('Y-m-d')] = array(
                'fecha' => $dia->getFecha('Y-m-d'),
                'dia_semana' => $dia->getFecha('Y-m-d'),
                'hi' => $dia->getHoraInicio('H:i'),
                'hf' => $dia->getHoraFin('H:i'),
                'hih' => $dia->getHoraInicio('H'),
                'hfh' => $dia->getHoraFin('H'),
                'inicio' => $dia->getFecha('Y-m-d') . ' ' . $dia->getHoraInicio('H:i'),
                'fin' => $dia->getFecha('Y-m-d') . ' ' . $dia->getHoraFin('H:i')
            );
        }

        return $this->renderPartial('editarVariables', array(
                    'grupo' => $grupo,
                    'facilitadores' => HorarioPeer::facilitadoresDisponibles($grupo->getCursoId(), $grupo->getCentroId(), $grupo->getAulaId(), $fechas, $grupo->getHorario()->getFechaInicio('Y-m-d'), $grupo->getHorario()->getFechaFin('Y-m-d'), $grupo->getFacilitadorId())
        ));
    }

    public function executeGuardarCambiosVariables(sfWebRequest $request) {
        $this->forward404unless($request->isXmlHttpRequest());
        $this->forward404Unless($grupo = GrupoPeer::retrieveByPk($request->getParameter('group')), sprintf('El grupo no existe(%s).', $request->getParameter('group')));

        $response = array(
            'msg' => 'Cambios guardados correctamente.',
            'error' => false,
            'data' => array()
        );

        $this->getResponse()->setContentType('application/json');

        if ($request->hasParameter('facilitador')) {
            $this->forward404Unless($facilitador = UsuarioPeer::retrieveByPk($request->getParameter('facilitador')), sprintf('El facilitador no existe(%s).', $request->getParameter('facilitador')));

            if (HorarioPeer::checar_disponibilidad($grupo, $facilitador)) {
                $grupo->setFacilitadorId($facilitador->getId());
                $grupo->save();

                $this->dispatcher->notify(new sfEvent($grupo, 'grupo.modificado', array('grupo' => $grupo)));
            } else {
                $response['msg'] = 'No es posible cambiar el facilitador, no esta disponible.';
                $response['error'] = false;

                return $this->renderText(json_encode($response));
            }
        }

        $alumnos = GrupoPeer::getAlumnosInscritosYpreInscritos($grupo);

        $response['data'] = array(
            'centro' => strtoupper($grupo->getCentro()->getAlias()),
            'curso' => $grupo->getCurso()->getNombre(),
            'clave' => $grupo->getClave(),
            'facilitador' => $grupo->getUsuarioRelatedByFacilitadorId()->getNombreCompleto(),
            'aula' => $grupo->getAula()->getNombre(),
            'inscritos' => $alumnos['inscritos'],
            'preinscritos' => $alumnos['preinscritos'],
            'fechas' => array(
                'inicio' => date('d-m-Y', strtotime($grupo->getHorario()->getFechaInicio())),
                'fin' => date('d-m-Y', strtotime($grupo->getHorario()->getFechaFin()))
            ),
            'horario' => array(
                'id' => $grupo->getHorarioId(),
                'lunes' => array(
                    'inicio' => $grupo->getHorario()->getHiLu(),
                    'fin' => $grupo->getHorario()->getHfLu()
                ),
                'martes' => array(
                    'inicio' => $grupo->getHorario()->getHiMa(),
                    'fin' => $grupo->getHorario()->getHfMa()
                ),
                'miercoles' => array(
                    'inicio' => $grupo->getHorario()->getHiMi(),
                    'fin' => $grupo->getHorario()->getHfMi()
                ),
                'jueves' => array(
                    'inicio' => $grupo->getHorario()->getHiJu(),
                    'fin' => $grupo->getHorario()->getHfJu()
                ),
                'viernes' => array(
                    'inicio' => $grupo->getHorario()->getHiVi(),
                    'fin' => $grupo->getHorario()->getHfVi()
                ),
                'sabado' => array(
                    'inicio' => $grupo->getHorario()->getHiSa(),
                    'fin' => $grupo->getHorario()->getHfSa()
                )
            )
        );

        return $this->renderText(json_encode($response));
    }

    /**
     * Accion ajax para mostrar la lista de grupos asiganados a un facilitador en un periodo
     * @param sfWebRequest $request
     */
    public function executeDetalleFacilitadorGruposPeriodo(sfWebRequest $request) {
        $this->forward404unless($request->isXmlHttpRequest());

        return $this->renderPartial('tablaFacilitadorGrupos', array(
                    'detalle' => GrupoPeer::detalleGruposFacilitador($request->getParameter('ident'), $request->getParameter('period'))
        ));
    }

    /**
     * Accion ajax para mostrar el formulario para editar la fecha de inicio de un grupo
     * @param sfWebRequest $request
     * @return Ambigous <sfView::NONE, string>
     */
    public function executeEditFechaInicio(sfWebRequest $request) {
        $this->forward404unless($request->isXmlHttpRequest());
        $this->forward404Unless($grupo = GrupoPeer::retrieveByPk($request->getParameter('group')), sprintf('El grupo no existe(%s).', $request->getParameter('group')));

        return $this->renderPartial('editFechaInicio', array('grupo' => $grupo));
    }

    /**
     * Accion ajax para mostrar el formulario para extender la fecha de cobro
     * @param sfWebRequest $request
     * @return Ambigous <sfView::NONE, string>
     */
    public function executeExtendPago(sfWebRequest $request) {
        $this->forward404unless($request->isXmlHttpRequest());
        $this->forward404Unless($grupo = GrupoPeer::retrieveByPk($request->getParameter('group')), sprintf('El grupo no existe(%s).', $request->getParameter('group')));
        $fechaFinalExtension = GrupoPeer::getFechaFinalExtensionPago($grupo, 2);
        return $this->renderPartial('extendPago', array('grupo' => $grupo, "fechaLimite" => $fechaFinalExtension));
    }

    /**
     * Accion para guardar el plazo de pago asignado a un grupo
     * @param sfWebRequest $request
     * @return Ambigous <sfView::NONE, string>
     */
    public function executeExtendPagoSave(sfWebRequest $request) {
        $this->forward404Unless($grupo = GrupoPeer::retrieveByPk($request->getParameter('group')), sprintf('El grupo no existe(%s).', $request->getParameter('group')));
        // Si ya existe el plazo se actualiza
        if ($grupo->getPlazoFechaCobro()) {
            if ($request->getParameter('activo') == 'false') {
                $grupo->getPlazoFechaCobro()->setActivo(false);
                $grupo->getPlazoFechaCobro()->setOperadorId($this->getUser()->getAttribute('usuario')->getId());
                $grupo->getPlazoFechaCobro()->save();
                $this->getUser()->setFlash('notice', 'Se desactivó la extensión de cobro para el grupo ' . $grupo->getClave() . '.');
            } else {
                $grupo->getPlazoFechaCobro()->setActivo(true);
                $grupo->getPlazoFechaCobro()->setFechaFin($request->getParameter('plazo'));
                $grupo->getPlazoFechaCobro()->setOperadorId($this->getUser()->getAttribute('usuario')->getId());
                $grupo->getPlazoFechaCobro()->save();
                $this->getUser()->setFlash('notice', 'Se actualizó la fecha de extensión de cobro para el grupo ' . $grupo->getClave() . '.');
            }
            // Evento para añadir a la sincronia el registro
            $this->dispatcher->notify(new sfEvent($grupo->getPlazoFechaCobro(), 'plazo.modificado', array('plazo' => $grupo->getPlazoFechaCobro())));
        }
        // Si no se ha guardado el palzo, se guarda
        else {
            $plazo = new PlazoFechaCobro();
            $plazo->setGrupoId($grupo->getId());
            $plazo->setFechaFin($request->getParameter('plazo'));
            $plazo->setOperadorId($this->getUser()->getAttribute('usuario')->getId());
            $plazo->save();
            // Evento para añadir a la sincronia el registro
            $this->dispatcher->notify(new sfEvent($plazo, 'plazo.creado', array('plazo' => $plazo)));
            $this->getUser()->setFlash('notice', 'Se extiende la fecha de cobro para el grupo ' . $grupo->getClave() . '.');
        }

        //Evento de grupo creado
        $this->redirect('calendarizacion/index');
    }

    /**
     * Accion ajax para mostrar el resultado de la disponibilidad de horario
     * @param sfWebRequest $request
     * @return Ambigous <sfView::NONE, string>
     */
    public function executeComprobarDisponibilidadNuevoPeriodo(sfWebRequest $request) {
        $this->forward404unless($request->isXmlHttpRequest());
        $this->forward404Unless($grupo = GrupoPeer::retrieveByPk($request->getParameter('group')), sprintf('El grupo no existe(%s).', $request->getParameter('group')));

        $dias = array('lunes' => $grupo->getHorario()->getLunes() ? 'true' : 'false',
            'martes' => $grupo->getHorario()->getMartes() ? 'true' : 'false',
            'miercoles' => $grupo->getHorario()->getMiercoles() ? 'true' : 'false',
            'jueves' => $grupo->getHorario()->getJueves() ? 'true' : 'false',
            'viernes' => $grupo->getHorario()->getViernes() ? 'true' : 'false',
            'sabado' => $grupo->getHorario()->getSabado() ? 'true' : 'false',
        );
        $horas = array('hi_lu' => $grupo->getHorario()->getHiLu('G'), 'hf_lu' => $grupo->getHorario()->getHfLu('G'),
            'hi_ma' => $grupo->getHorario()->getHiMa('G'), 'hf_ma' => $grupo->getHorario()->getHfMa('G'),
            'hi_mi' => $grupo->getHorario()->getHiMi('G'), 'hf_mi' => $grupo->getHorario()->getHfMi('G'),
            'hi_ju' => $grupo->getHorario()->getHiJu('G'), 'hf_ju' => $grupo->getHorario()->getHfJu('G'),
            'hi_vi' => $grupo->getHorario()->getHiVi('G'), 'hf_vi' => $grupo->getHorario()->getHfVi('G'),
            'hi_sa' => $grupo->getHorario()->getHiSa('G'), 'hf_sa' => $grupo->getHorario()->getHfSa('G'),
        );
        $noLaborables = DiasNoLaborablesPeer::listaNoLaborables();

        $fechasJS = $this->generaDias($request->getParameter('inicio'), $grupo->getCursoId(), $dias, $horas, $noLaborables, true);
        $fechasPHP = $this->generaDias($request->getParameter('inicio'), $grupo->getCursoId(), $dias, $horas, $noLaborables);
        $fechaFinal = explode('-', $fechasJS[count($fechasJS) - 1]['fecha']);

        return $this->renderPartial('detalleDispoibilidadNuevoPeriodo', array('detalle' => array(
                        'grupo' => $grupo->getId(),
                        'inicio' => $request->getParameter('inicio'),
                        'disponible' => HorarioPeer::comprobarPeriodo($grupo->getCentroId(), $grupo->getAulaId(), $request->getParameter('inicio'), $fechasJS[count($fechasJS) - 1]['fecha'], $fechasPHP, $grupo->getHorarioId()),
                        'facilitadorDisponible' => HorarioPeer::comprobarDisponibilidadFacilitador($grupo->getFacilitadorId(), $grupo->getId(), $fechasPHP, $request->getParameter('inicio'), $fechasJS[count($fechasJS) - 1]['fecha'])
        )));
    }

    /**
     * Accion para actualizar la fecha de inicio de un grupo
     * @param sfWebRequest $request
     */
    public function executeUpdateFechaInicio(sfWebRequest $request) {
        $this->forward404Unless($grupo = GrupoPeer::retrieveByPk($request->getParameter('grupo')), sprintf('El grupo no existe (%s).', $request->getParameter('grupo')));

        $mensaje = "";
        $dias = array(
            'lunes' => $grupo->getHorario()->getLunes() ? 'true' : 'false',
            'martes' => $grupo->getHorario()->getMartes() ? 'true' : 'false',
            'miercoles' => $grupo->getHorario()->getMiercoles() ? 'true' : 'false',
            'jueves' => $grupo->getHorario()->getJueves() ? 'true' : 'false',
            'viernes' => $grupo->getHorario()->getViernes() ? 'true' : 'false',
            'sabado' => $grupo->getHorario()->getSabado() ? 'true' : 'false',
        );
        $horas = array(
            'hi_lu' => $grupo->getHorario()->getHiLu('G'), 'hf_lu' => $grupo->getHorario()->getHfLu('G'),
            'hi_ma' => $grupo->getHorario()->getHiMa('G'), 'hf_ma' => $grupo->getHorario()->getHfMa('G'),
            'hi_mi' => $grupo->getHorario()->getHiMi('G'), 'hf_mi' => $grupo->getHorario()->getHfMi('G'),
            'hi_ju' => $grupo->getHorario()->getHiJu('G'), 'hf_ju' => $grupo->getHorario()->getHfJu('G'),
            'hi_vi' => $grupo->getHorario()->getHiVi('G'), 'hf_vi' => $grupo->getHorario()->getHfVi('G'),
            'hi_sa' => $grupo->getHorario()->getHiSa('G'), 'hf_sa' => $grupo->getHorario()->getHfSa('G'),
        );
        $noLaborables = DiasNoLaborablesPeer::listaNoLaborables();
        $fechasJS = $this->generaDias($request->getParameter('inicio'), $grupo->getCursoId(), $dias, $horas, $noLaborables, true);
        $fechasPHP = $this->generaDias($request->getParameter('inicio'), $grupo->getCursoId(), $dias, $horas, $noLaborables);
        $fechaFinal = explode('-', $fechasJS[count($fechasJS) - 1]['fecha']);

        // Actualizamos los datos de las fechas guardadas
        $criteria = new Criteria();
        $criteria->add(FechasHorarioPeer::HORARIO_ID, $grupo->getHorarioId());
        $criteria->addAscendingOrderByColumn(FechasHorarioPeer::FECHA);
        foreach (FechasHorarioPeer::doSelect($criteria) as $fechas) {
            $fechaUpdate = current($fechasPHP);
            $fechas->setFecha($fechaUpdate['fecha']);
            $fechas->save();
            next($fechasPHP);
        }
        $grupo->getHorario()->setFechaInicio($request->getParameter('inicio'));
        $grupo->getHorario()->setFechaFin($fechasJS[count($fechasJS) - 1]['fecha']);
        $grupo->getHorario()->save();
        // Trigger para actualizar horario
        $this->dispatcher->notify(new sfEvent($grupo->getHorario(), 'horario.modificado', array('horario' => $grupo->getHorario())));
        // Revisamos si el grupo tiene extensio de pago
        if ($grupo->getPlazoFechaCobro()) {
            // Si la tiene la desactivamos
            $grupo->getPlazoFechaCobro()->setActivo(false);
            $grupo->getPlazoFechaCobro()->save();
            $mensaje = "Se desactivó la extensión de cobro";
            $this->dispatcher->notify(new sfEvent($grupo->getPlazoFechaCobro(), 'plazo.modificado', array('plazo' => $grupo->getPlazoFechaCobro())));
        }
        $this->getUser()->setFlash('notice', 'Se reprogramo la fecha de inicio del grupo ' . $grupo->getClave() . '. ' . $mensaje);
        //Evento de grupo creado
        $this->redirect('calendarizacion/index');
    }

    /**
     * Acción JSONP para consultar en los centros si un grupo tiene socios inscritos
     * @param sfRequest $request A request object
     */
    public function executeConsulta_inscritos_borrar(sfWebRequest $request) {
        $grupoId = $request->getParameter('grupo');
        if ($grupoId) {
            $this->forward404Unless($grupo = GrupoPeer::retrieveByPk($grupoId), sprintf('El grupo no existe (%s).', $grupoId));
            $this->getResponse()->setHttpHeader('Access-Control-Allow-Origin', '*');
            $this->getResponse()->setContentType('application/json');

            return $this->renderText($request->getParameter('method') . '(' . json_encode(GrupoPeer::consultaEstadoAlumnosGrupo($grupo)) . ')');
        }
        $this->forward404('La página solicitado no existe');
    }

    protected function processForm(sfWebRequest $request, sfForm $form) {
        $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
        if ($form->isValid()) {
            $datos = $request->getParameter('horario');
            $facilitador = $datos['facilitador'];
            $horario = $form->save();
            $dias = array('lunes' => ($horario->getLunes() ? 'true' : 'false'),
                'martes' => ($horario->getMartes() ? 'true' : 'false'),
                'miercoles' => ($horario->getMiercoles() ? 'true' : 'false'),
                'jueves' => ($horario->getJueves() ? 'true' : 'false'),
                'viernes' => ($horario->getViernes() ? 'true' : 'false'),
                'sabado' => ($horario->getSabado() ? 'true' : 'false'),
            );
            $horas = array('hi_lu' => $horario->getHiLu('G'), 'hf_lu' => $horario->getHfLu('G'),
                'hi_ma' => $horario->getHiMa('G'), 'hf_ma' => $horario->getHfMa('G'),
                'hi_mi' => $horario->getHiMi('G'), 'hf_mi' => $horario->getHfMi('G'),
                'hi_ju' => $horario->getHiJu('G'), 'hf_ju' => $horario->getHfJu('G'),
                'hi_vi' => $horario->getHiVi('G'), 'hf_vi' => $horario->getHfVi('G'),
                'hi_sa' => $horario->getHiSa('G'), 'hf_sa' => $horario->getHfSa('G'),
            );
            $noLaborables = DiasNoLaborablesPeer::listaNoLaborables();
            $fechasPHP = $this->generaDias($horario->getFechaInicio('Y-m-d'), $horario->getCursoId(), $dias, $horas, $noLaborables);
            foreach ($fechasPHP as $fecha) {
                $dia = new FechasHorario();
                $dia->setHorarioId($horario->getId());
                $dia->setFecha($fecha['fecha']);
                $dia->setHoraInicio($fecha['hi']);
                $dia->setHoraFin($fecha['hf']);
                $dia->save();
            }

            $this->dispatcher->notify(new sfEvent($horario, 'horario.creado', array('horario' => $horario)));

            $grupo = new Grupo();
            $grupo->setCentroId($horario->getCentroId());
            $grupo->setCursoId($horario->getCursoId());
            $grupo->setAulaId($horario->getAulaId());
            $grupo->setHorarioId($horario->getId());
            $grupo->setFacilitadorId($facilitador);
            $grupo->setClave(
                    GrupoPeer::generaClave(
                            $horario->getCurso()->getClave(), $horario->getCentro()->getAlias(), $horario->getFechaInicio('d/m/Y')
                    )
            );
            $grupo->setCupoTotal($horario->getAula()->getCapacidad());
            $grupo->setDuracion($horario->getCurso()->getDuracion());
            $grupo->setOperadorId($this->getUser()->getAttribute('usuario')->getId());
            $grupo->save();

            $this->dispatcher->notify(new sfEvent($grupo, 'grupo.creado', array('grupo' => $grupo)));

            $notice = GrupoPeer::publicarMoodle($grupo->getId());

            $this->getUser()->setFlash('notice', 'Se creo el grupo ' . $grupo->getClave() . '. ' . $notice);

            //Evento de grupo creado

            $this->redirect('calendarizacion/index');
        }
    }

    /**
     *
     * @param time $fecha - Fecha de incio del curso
     * @param int $cursoId - Id del curso
     * @param array $dias - Arreglo de dias
     * @param array $horas - Arreglo de horas
     * @param bool $js - Flag para saber si sebe devolverse el arraglo js
     *
     * @return array - Arreglo con las fechas deun periodo
     */
    protected function generaDias($fecha, $cursoId, $dias, $horas, $noLaborables, $js = false) {
        $inicio = explode('-', $fecha);
        $dia = (int) $inicio[2];
        $mes = (int) $inicio[1];
        $anio = (int) $inicio[0];
        $curso = CursoPeer::retrieveByPK($cursoId);
        $duracion = $curso->getDuracion();
        error_log($duracion);
        error_log(print_r($dias, true));
        error_log(print_r($horas, true));
        error_log(date("Y-m-d", mktime(0, 0, 0, $mes, $dia, $anio)));
        $fechasPHP = array();
        $fechasJS = array();
        do {
            $diaSemana = date("w", mktime(0, 0, 0, $mes, $dia, $anio));
            $diaActual = date("Y-m-d", mktime(0, 0, 0, $mes, $dia, $anio));
            if (!in_array($diaActual, $noLaborables)) {
                switch ($diaSemana) {
                    case 1:
                        if ($dias['lunes'] == 'true' || $dias['lunes'] == 'checked') {
                            $h = $horas['hf_lu'] - $horas['hi_lu'];
                            $duracion = $duracion - $h;
                            $fechasPHP[$diaActual] = array('fecha' => $diaActual,
                                'dia_semana' => $diaSemana,
                                'hi' => date("H:i", mktime($horas['hi_lu'], 0, 0, $mes, $dia, $anio)),
                                'hf' => date("H:i", mktime($horas['hf_lu'], 0, 0, $mes, $dia, $anio)),
                                'hih' => date("H", mktime($horas['hi_lu'], 0, 0, $mes, $dia, $anio)),
                                'hfh' => date("H", mktime($horas['hf_lu'], 0, 0, $mes, $dia, $anio)),
                                'inicio' => date("Y-m-d H:i", mktime($horas['hi_lu'], 0, 0, $mes, $dia, $anio)),
                                'fin' => date("Y-m-d H:i", mktime($horas['hf_lu'], 0, 0, $mes, $dia, $anio)));
                            $fechasJS[] = array('fecha' => $diaActual,
                                'inicio' => date("Y-m-d H:i", mktime($horas['hi_lu'], 0, 0, $mes, $dia, $anio)),
                                'fin' => date("Y-m-d H:i", mktime($horas['hf_lu'], 0, 0, $mes, $dia, $anio)));
                        }
                        break;
                    case 2:
                        if ($dias['martes'] == 'true' || $dias['martes'] == 'checked') {
                            $h = $horas['hf_ma'] - $horas['hi_ma'];
                            $duracion = $duracion - $h;
                            $fechasPHP[$diaActual] = array('fecha' => $diaActual,
                                'dia_semana' => $diaSemana,
                                'hi' => date("H:i", mktime($horas['hi_ma'], 0, 0, $mes, $dia, $anio)),
                                'hf' => date("H:i", mktime($horas['hf_ma'], 0, 0, $mes, $dia, $anio)),
                                'hih' => date("Hi", mktime($horas['hi_ma'], 0, 0, $mes, $dia, $anio)),
                                'hfh' => date("Hi", mktime($horas['hf_ma'], 0, 0, $mes, $dia, $anio)),
                                'inicio' => date("Y-m-d H:i", mktime($horas['hi_ma'], 0, 0, $mes, $dia, $anio)),
                                'fin' => date("Y-m-d H:i", mktime($horas['hf_ma'], 0, 0, $mes, $dia, $anio)));
                            $fechasJS[] = array('fecha' => $diaActual,
                                'inicio' => date("Y-m-d H:i", mktime($horas['hi_ma'], 0, 0, $mes, $dia, $anio)),
                                'fin' => date("Y-m-d H:i", mktime($horas['hf_ma'], 0, 0, $mes, $dia, $anio)));
                        }
                        break;
                    case 3:
                        if ($dias['miercoles'] == 'true' || $dias['miercoles'] == 'checked') {
                            $h = $horas['hf_mi'] - $horas['hi_mi'];
                            $duracion = $duracion - $h;
                            $fechasPHP[$diaActual] = array('fecha' => $diaActual,
                                'dia_semana' => $diaSemana,
                                'hi' => date("H:i", mktime($horas['hi_mi'], 0, 0, $mes, $dia, $anio)),
                                'hf' => date("H:i", mktime($horas['hf_mi'], 0, 0, $mes, $dia, $anio)),
                                'hih' => date("H", mktime($horas['hi_mi'], 0, 0, $mes, $dia, $anio)),
                                'hfh' => date("H", mktime($horas['hf_mi'], 0, 0, $mes, $dia, $anio)),
                                'inicio' => date("Y-m-d H:i", mktime($horas['hi_mi'], 0, 0, $mes, $dia, $anio)),
                                'fin' => date("Y-m-d H:i", mktime($horas['hf_mi'], 0, 0, $mes, $dia, $anio)));
                            $fechasJS[] = array('fecha' => $diaActual,
                                'inicio' => date("Y-m-d H:i", mktime($horas['hi_mi'], 0, 0, $mes, $dia, $anio)),
                                'fin' => date("Y-m-d H:i", mktime($horas['hf_mi'], 0, 0, $mes, $dia, $anio)));
                        }
                        break;
                    case 4:
                        if ($dias['jueves'] == 'true' || $dias['jueves'] == 'checked') {
                            $h = $horas['hf_ju'] - $horas['hi_ju'];
                            $duracion = $duracion - $h;
                            $fechasPHP[$diaActual] = array('fecha' => $diaActual,
                                'dia_semana' => $diaSemana,
                                'hi' => date("H:i", mktime($horas['hi_ju'], 0, 0, $mes, $dia, $anio)),
                                'hf' => date("H:i", mktime($horas['hf_ju'], 0, 0, $mes, $dia, $anio)),
                                'hih' => date("H", mktime($horas['hi_ju'], 0, 0, $mes, $dia, $anio)),
                                'hfh' => date("H", mktime($horas['hf_ju'], 0, 0, $mes, $dia, $anio)),
                                'inicio' => date("Y-m-d H:i", mktime($horas['hi_ju'], 0, 0, $mes, $dia, $anio)),
                                'fin' => date("Y-m-d H:i", mktime($horas['hf_ju'], 0, 0, $mes, $dia, $anio)));
                            $fechasJS[] = array('fecha' => $diaActual,
                                'inicio' => date("Y-m-d H:i", mktime($horas['hi_ju'], 0, 0, $mes, $dia, $anio)),
                                'fin' => date("Y-m-d H:i", mktime($horas['hf_ju'], 0, 0, $mes, $dia, $anio)));
                        }
                        break;
                    case 5:
                        if ($dias['viernes'] == 'true' || $dias['viernes'] == 'checked') {
                            $h = $horas['hf_vi'] - $horas['hi_vi'];
                            $duracion = $duracion - $h;
                            $fechasPHP[$diaActual] = array('fecha' => $diaActual,
                                'dia_semana' => $diaSemana,
                                'hi' => date("H:i", mktime($horas['hi_vi'], 0, 0, $mes, $dia, $anio)),
                                'hf' => date("H:i", mktime($horas['hf_vi'], 0, 0, $mes, $dia, $anio)),
                                'hih' => date("H", mktime($horas['hi_vi'], 0, 0, $mes, $dia, $anio)),
                                'hfh' => date("H", mktime($horas['hf_vi'], 0, 0, $mes, $dia, $anio)),
                                'inicio' => date("Y-m-d H:i", mktime($horas['hi_vi'], 0, 0, $mes, $dia, $anio)),
                                'fin' => date("Y-m-d H:i", mktime($horas['hf_vi'], 0, 0, $mes, $dia, $anio)));
                            $fechasJS[] = array('fecha' => $diaActual,
                                'inicio' => date("Y-m-d H:i", mktime($horas['hi_vi'], 0, 0, $mes, $dia, $anio)),
                                'fin' => date("Y-m-d H:i", mktime($horas['hf_vi'], 0, 0, $mes, $dia, $anio)));
                        }
                        break;
                    case 6:
                        if ($dias['sabado'] == 'true' || $dias['sabado'] == 'checked') {
                            $h = $horas['hf_sa'] - $horas['hi_sa'];
                            $duracion = $duracion - $h;
                            $fechasPHP[$diaActual] = array('fecha' => $diaActual,
                                'dia_semana' => $diaSemana,
                                'hi' => date("H:i", mktime($horas['hi_sa'], 0, 0, $mes, $dia, $anio)),
                                'hf' => date("H:i", mktime($horas['hf_sa'], 0, 0, $mes, $dia, $anio)),
                                'hih' => date("H", mktime($horas['hi_sa'], 0, 0, $mes, $dia, $anio)),
                                'hfh' => date("H", mktime($horas['hf_sa'], 0, 0, $mes, $dia, $anio)),
                                'inicio' => date("Y-m-d H:i", mktime($horas['hi_sa'], 0, 0, $mes, $dia, $anio)),
                                'fin' => date("Y-m-d H:i", mktime($horas['hf_sa'], 0, 0, $mes, $dia, $anio)));
                            $fechasJS[] = array('fecha' => $diaActual,
                                'inicio' => date("Y-m-d H:i", mktime($horas['hi_sa'], 0, 0, $mes, $dia, $anio)),
                                'fin' => date("Y-m-d H:i", mktime($horas['hf_sa'], 0, 0, $mes, $dia, $anio)));
                        }
                        break;
                }
            }
            $dia++;
        } while ($duracion > 0);

        $fechas = $fechasPHP;

        if ($js) {
            $fechas = $fechasJS;
        }

        return $fechas;
    }

    public function executeGetGrupo(sfWebRequest $request) {
        $this->forward404unless($request->isXmlHttpRequest());
        $this->forward404Unless($grupo = GrupoPeer::retrieveByPk($request->getParameter('grupo_id')), sprintf('El grupo no existe(%s).', $request->getParameter('grupo_id')));

        $response = array(
            'msg' => '',
            'error' => false,
            'data' => array(
                'centro' => $grupo->getCentroId(),
                'curso' => $grupo->getCursoId(),
                'aula' => $grupo->getAulaId()
            )
        );

        $this->getResponse()->setContentType('application/json');

        return $this->renderText(json_encode($response));
    }

}
