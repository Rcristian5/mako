<h4>
	<?php echo ($form->getObject()->isNew() ? 'Crear evento' : 'Editar evento') ?>
</h4>

<form
	action="<?php echo url_for('dias_laborables/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>"
	method="post"
	<?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
	<?php if (!$form->getObject()->isNew()): ?>
	<input type="hidden" name="sf_method" value="put" />
	<?php endif; ?>
	<table id="table-form-dias">
		<tbody>
			<tr>
				<th><label>Fecha</label></th>
				<th><?php echo $dia ?></th>
			</tr>
			<tr>
				<th><?php echo $form['descripcion']->renderLabel('Evento') ?></th>
				<td><?php echo $form['descripcion']->renderError() ?> <?php echo $form['descripcion']->render(array('requerido'=>1, 'filtro'=>'alfanumerico', 'cols'=>38, 'rows'=>2)) ?>
				</td>
			</tr>
		</tbody>
	</table>
	<p class="botones_finales">
		<?php echo $form->renderHiddenFields(false) ?>
		<?php if (!$form->getObject()->isNew()): ?>
		<?php echo link_to('Eliminar', 'dias_laborables/delete?id='.$form->getObject()->getId(), array('method' => 'delete', 'confirm' => '¿Deseas elimar este evento?', 'class'=>'botones')) ?>

		<?php endif; ?>
		<input type="submit" class="botones" value="Guardar"
			onclick="return comparaRequeridos();" />
	</p>
</form>
