<?php use_stylesheet('control_escolar/control_escolar.css') ?>
<?php use_stylesheet('control_escolar/dias_laborables.css') ?>
<?php use_stylesheet('jquery.windows.css') ?>
<?php use_stylesheet('fullcalendar/jquery.fullcalendar.css') ?>
<?php use_javascript('jquery.ui.js') ?>
<?php use_javascript('plugins/jquery.windows.js') ?>
<?php use_javascript('plugins/jquery.fullcalendar.js') ?>
<?php use_javascript('control_escolar/dias_laborables.js') ?>

<script type="text/javascript">
var disabledDays = [<?php foreach ($dias as $dia): ?>
                  {
                    id  : 'disableds',
                    title  : '<?php echo $dia->getDescripcion() ?>',
                    start  : '<?php echo $dia->getDia('Y-m-d') ?>',
                    allDay : true,
                    className  : 'fc-disable',
                    editable: false,
                    description : '<?php echo $dia->getDescripcion() ?>',
                    ident : <?php echo $dia->getId() ?>
                },
                <?php endforeach; ?>
                ];
</script>
<div id="windows-wrapper">
	<div id="dias-no" window-title="Días no laborables" window-state="min">
		<div id="calendar" style="width: 99%;"></div>
	</div>

</div>
