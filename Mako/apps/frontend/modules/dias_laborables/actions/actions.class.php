<?php

/**
 * laborables actions.
 *
 * @package    mako
 * @subpackage laborables
 * @author     Bixit SA de CV
 * @version    SVN: $Id: actions.class.php,v 1.4 2010/09/24 23:42:33 david Exp $
 */
class dias_laborablesActions extends sfActions
{
	public function executeIndex(sfWebRequest $request)
	{
		$this->dias = DiasNoLaborablesPeer::doSelect(new Criteria());
		$this->form = new DiasNoLaborablesForm();
	}

	public function executeForm(sfWebRequest $request)
	{
		$this->forward404unless($request->isXmlHttpRequest());
		$id = $request->getParameter('evento');
		$dia = $request->getParameter('dia');
		$fecha = $request->getParameter('fecha');
		if(!DiasNoLaborablesPeer::diaOcupado($fecha))
		{
			if($id!="null")
			{
				return $this->renderPartial('form', array('form'=>new DiasNoLaborablesForm(DiasNoLaborablesPeer::retrieveByPK($id)), 'dia'=>$dia));
			}
			$form = new DiasNoLaborablesForm();
			$form->setDefault('dia', $fecha);

			return $this->renderPartial('form', array('form'=>$form, 'dia'=>$dia ));
		}
		else
		{
			return $this->renderPartial('bloqueado');
		}
	}

	public function executeCreate(sfWebRequest $request)
	{
		$this->forward404Unless($request->isMethod(sfRequest::POST));
		$this->dias = DiasNoLaborablesPeer::diasNoLaborables();
		$this->form = new DiasNoLaborablesForm();

		$this->processForm($request, $this->form);

		$this->setTemplate('new');
	}

	public function executeEdit(sfWebRequest $request)
	{
		$this->forward404Unless($DiasNoLaborables = DiasNoLaborablesPeer::retrieveByPk($request->getParameter('id')), sprintf('Object DiasNoLaborables does not exist (%s).', $request->getParameter('id')));
		$this->form = new DiasNoLaborablesForm($DiasNoLaborables);
	}

	public function executeUpdate(sfWebRequest $request)
	{
		$this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
		$this->forward404Unless($DiasNoLaborables = DiasNoLaborablesPeer::retrieveByPk($request->getParameter('id')), sprintf('Object DiasNoLaborables does not exist (%s).', $request->getParameter('id')));
		$this->form = new DiasNoLaborablesForm($DiasNoLaborables);

		$this->processForm($request, $this->form);

		$this->setTemplate('edit');
	}

	public function executeDelete(sfWebRequest $request)
	{
		$request->checkCSRFProtection();

		$this->forward404Unless($DiasNoLaborables = DiasNoLaborablesPeer::retrieveByPk($request->getParameter('id')), sprintf('El evenot no existe (%s).', $request->getParameter('id')));
		$DiasNoLaborables->delete();

		$this->getUser()->setFlash('notice', "Evento eliminado");
		$this->redirect('dias_laborables/index');
	}

	protected function processForm(sfWebRequest $request, sfForm $form)
	{
		$form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
		if ($form->isValid())
		{
			$mensaje = 'Evento actualizado';
			$flag = false;
			if($form->getObject()->isNew())
			{
				$mensaje = 'Evento guardado';
				$flag = true;
			}
			$form->setDefault('operador_id', $this->getUser()->getAttribute('usuario')->getId());
			$dia = $form->save();
			//Parte que recorre dias
			//DiasNoLaborablesPeer::moverFechasForward($evento->getDia('Y-m-d'));

			//Evento NolaborableCreado o NolaborableModificado
			($flag ? $this->dispatcher->notify(new sfEvent($this, 'nolaborable.creado', array( 'dia' => $dia ))) : $this->dispatcher->notify(new sfEvent($this, 'nolaborable.modificado', array( 'dia' => $dia ))) );

			$this->getUser()->setFlash('notice', $mensaje);
			$this->redirect('dias_laborables/index');
		}
	}
}
