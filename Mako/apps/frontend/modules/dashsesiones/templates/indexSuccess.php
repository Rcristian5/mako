<?php
/**
 * Plantilla del dashboard de ocupacion de pcs
 */
use_stylesheet('home.css');
use_javascript('home/home.js');
use_javascript('jquery.ui.js');
?>

<?php use_javascript('plugins/jquery.dataTables.js') ?>
<?php use_stylesheet('datatables.css') ?>

<?php use_javascript('plugins/date.js') ?>
<?php use_javascript('plugins/dataTables/datatables.orden-fecha.js') ?>
<?php use_javascript('plugins/dataTables/datatables.orden-decimal.js') ?>

<script>
$(document).ready(function() 
{
	var aTabsId = new Array();
	$('#tabs ul a').each(function(){
		aTabsId.push($(this).attr('href'));
	});
	
	$("#tabs").tabs();

	var idx = $("#tabs").tabs('option', 'selected');
	tamTabs(aTabsId[idx]);
		
	$("#tabs").bind( "tabsshow", function(event, ui) {
		tamTabs('#'+ui.panel.id);
	});

	var $wcm= $("#verDetalleSocio").dialog({
		modal: true,
		autoOpen: false,	    		
		position: ['center','center'],
		title:	'Ver detalle',
		height: 550,
		width: 1100,
		buttons: {
			'Cerrar': function() 
			{
				$(this).dialog('close');
			}							
		}
	});

	function verDetalle(idSocio){
		var UrlVerDetalle = "<?php echo url_for('registro/verDetalle') ?>";
		$("#verDetalleSocio").load(								 
			UrlVerDetalle,
			{
				id: idSocio
			}			
		); 
		$wcm.dialog('open');					
	};

	$('.usuario').click(function(){
		var socio_id = $(this).attr('socio_id');
		verDetalle(socio_id);
	});

	setTimeout("document.location.href=this.location.href;",60000);
	
});

/**
 * Redimensiona el area de los tabs al espacio que ocupan los elementos flotantes
 */
function tamTabs(tab){
	var maxtop = 0;
	$(tab+' .boton-home').each(function(){
		var p = $(this).position();
		var h = $(this).height();
		if((p.top+h) > maxtop) maxtop = p.top+h;
	});
	$(tab).height(maxtop);
}
</script>

<?php use_stylesheet('./dashsesiones/indexSuccess.css')?>

<!-- Ver detalle socio -->
<div class="ui-widget ui-corner-all sombra">
	<div id="verDetalleSocio" style="display: none"></div>
</div>
<!-- /Ver detalle socio -->


<div class="ui-widget ui-corner-all sombra">
	<div id="tabs">
		<ul>
			<?php foreach($Secciones as $sc): ?>
			<?php if($sc->getEnDashboard()): ?>
			<li><a href="#tab-<?php echo $sc->getId() ?>"><?php echo $sc->getNombre() ?>
			</a></li>
			<?php endif; ?>
			<?php endforeach; ?>
		</ul>

		<?php $sec_anterior = 0;?>
		<?php foreach($Computadoras as $pc): ?>
		<?php if(!$pc->getSeccion()->getEnDashboard()) continue; ?>
		<?php
		//$pc= new Computadora();
		$sec[$pc->getSeccionId()]++;
		$s = $pc->getSesions();
		$sec_actual = $pc->getSeccionId();
		if(count($s)>0)
		{
			$sesion = $s[0];
			//Usuario en sesion
			$usr = $sesion->getSocio()->getUsuario();
			//id de socio
			$socio_id = $sesion->getSocioId();
			//Folio de socio
			$folio = $sesion->getSocio()->getFolio();
			//Tiempo de socio
			$saldo = $sesion->getSaldo();
			//Hora de inicio de sesion
			$fini = $sesion->getFechaLogin("H:i:s");
			//Tiempo transcurrido desde el inicio de sesion
			$segst = time() - $sesion->getFechaLogin(null)->format('U');
			$trans = Comun::seg2hm($segst);
			//Resta por consumir
			$segsf = abs($saldo - $segst);
			$resta = Comun::seg2hm($segsf);

			//$clase='ui-state-highlight';
			$clase='';
			$disp="visibility:visible;";
			$tpc="PC Ocupada";
		}
		else
		{
			$usr = "";
			$folio = "000000";
			$saldo = "0000";
			$fini = "00:00";
			$trans = "1234";
			$resta="1234";
			$clase='';
			$disp="visibility:hidden;";
			$tpc="PC libre";
		}
		?>

		<?php if(($sec_actual != $sec_anterior)&& $sec_anterior !=0 ): ?>
		<?php  $sec_anterior = $sec_actual; ?>
	</div>
	<?php endif; ?>
	<?php if($sec[$pc->getSeccionId()]==1): ?>
	<div id="tab-<?php echo $pc->getSeccionId() ?>">
		<?php endif; ?>

		<div
			class="<?php echo $clase ?> boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada">
			<div>
				<span class="numpc ui-state-default ui-corner-tr"
					title="Número de PC"> <?php echo $pc->getAlias()?>
				</span> <span style="<?php echo $disp?>" class="inicio ui-state-default ui-corner-tl" title="Hora de inicion de sesión">
					<?php echo $fini ?>
				</span>
			</div>

			<img src="/imgs/iconos/libre.png" border="0"
				title="<?php echo $tpc?>" width="64" height="64" />

			<div style="text-align: left;">
				<span style="<?php echo $disp?>" class="usuario ui-state-highlight" socio_id="<?php echo $socio_id ?>">
					<?php echo $usr ?>
				</span>
			</div>

			<div>
				<span style="<?php echo $disp?>" class="trans ui-state-default ui-corner-bl" title="Tiempo transcurrido desde inicio de sesión">
					<?php echo $trans ?>
				</span> <span style="<?php echo $disp?>" class="resta ui-state-default ui-corner-br" title="Tiempo restante del socio">
					<?php echo $resta ?>
				</span>
			</div>

		</div>

		<?php if(($sec_actual != $sec_anterior)&& $sec_anterior ==0 ): ?>
		<?php  $sec_anterior = $sec_actual; ?>
		<?php endif; ?>
		<?php endforeach; ?>

	</div>
</div>
</div>
