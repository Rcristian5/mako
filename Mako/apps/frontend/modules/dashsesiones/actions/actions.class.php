<?php

/**
 * dashsesiones actions.
 *
 * @package    mako
 * @subpackage dashsesiones
 * @author     Bixit SA de CV
 * @version    SVN: $Id: actions.class.php,v 1.2 2010/09/27 09:16:00 eorozco Exp $
 */
class dashsesionesActions extends sfActions
{

	public function executeIndex(sfWebRequest $request)
	{

		$c = new Criteria();
		$c->add(ComputadoraPeer::CENTRO_ID,sfConfig::get('app_centro_actual_id'));
		$c->addAscendingOrderByColumn(ComputadoraPeer::SECCION_ID);
		$c->addAscendingOrderByColumn(ComputadoraPeer::ID);
		$this->Computadoras = ComputadoraPeer::doSelect($c);

		//Las secciones del centro
		$c = new Criteria();
		$c->add(SeccionPeer::CENTRO_ID,sfConfig::get('app_centro_actual_id'));
		$this->Secciones = SeccionPeer::doSelect($c);
	}

}


