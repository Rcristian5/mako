<h1>Conciliacions List</h1>

<table>
	<thead>
		<tr>
			<th>Id</th>
			<th>Centro</th>
			<th>Operador</th>
			<th>Monto</th>
			<th>Folio docto aval</th>
			<th>Folio</th>
			<th>Ip caja</th>
			<th>Created at</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($Conciliacions as $Conciliacion): ?>
		<tr>
			<td><a
				href="<?php echo url_for('conciliacion_adm/show?id='.$Conciliacion->getId()) ?>"><?php echo $Conciliacion->getId() ?>
			</a></td>
			<td><?php echo $Conciliacion->getCentroId() ?></td>
			<td><?php echo $Conciliacion->getOperadorId() ?></td>
			<td><?php echo $Conciliacion->getMonto() ?></td>
			<td><?php echo $Conciliacion->getFolioDoctoAval() ?></td>
			<td><?php echo $Conciliacion->getFolio() ?></td>
			<td><?php echo $Conciliacion->getIpCaja() ?></td>
			<td><?php echo $Conciliacion->getCreatedAt() ?></td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<a href="<?php echo url_for('conciliacion_adm/new') ?>">New</a>
