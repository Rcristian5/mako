<?php use_javascript('plugins/jquery.dataTables.js') ?>
<?php use_javascript('jquery.ui.js') ?>
<?php use_stylesheet('datatables.css') ?>
<script>
var lsT;
$(document).ready(function() {

   lsT = $('#lista-conciliaciones').dataTable({
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
        "bJQueryUI": true,
        "sPaginationType": "full_numbers"
     });
   
});
</script>
<br />
<div class="ui-widget sombra">
	<div class="ui-widget-content ui-corner-all ts" style="padding: 5px;">

		<table width="100%" id="lista-conciliaciones">
			<caption>Conciliaciones aplicadas</caption>
			<thead>
				<tr>
					<th>Centro</th>
					<th>Operador</th>
					<th>Monto($)</th>
					<th>Folio Referencia</th>
					<th>Folio</th>
					<th>Ip caja</th>
					<th>Fecha Conciliación</th>
					<th>Observaciones</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($Conciliaciones as $conc): ?>
				<tr id="conciliacion_<?php echo $conc->getId() ?>">
					<td><?php echo $conc->getCentro()->getNombre() ?></td>
					<td><?php echo $conc->getUsuario() ?></td>
					<td align="right"><?php echo $conc->getMonto() ?></td>
					<td><?php echo $conc->getFolioDoctoAval() ?></td>
					<td align="right"><?php echo sprintf('%05d',$conc->getFolio()) ?></td>
					<td align="center"><?php echo $conc->getIpCaja() ?></td>
					<td align="center"><?php echo $conc->getCreatedAt() ?></td>
					<td><?php echo $conc->getObservaciones() ?></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
