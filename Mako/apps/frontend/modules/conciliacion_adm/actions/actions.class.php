<?php

/**
 * conciliacion_adm actions.
 *
 * @package    mako
 * @subpackage conciliacion_adm
 * @author     Bixit SA de CV
 * @version    SVN: $Id: actions.class.php,v 1.3 2010/08/31 01:32:38 eorozco Exp $
 */
class conciliacion_admActions extends sfActions
{

	public function executeIndex(sfWebRequest $request)
	{
		$this->Conciliacions = ConciliacionPeer::doSelect(new Criteria());
	}

	public function executeShow(sfWebRequest $request)
	{
		$this->Conciliacion = ConciliacionPeer::retrieveByPk($request->getParameter('id'));
		$this->forward404Unless($this->Conciliacion);
	}

	public function executeNew(sfWebRequest $request)
	{

		$c = new Criteria();
		$c->addDescendingOrderByColumn(ConciliacionPeer::ID);
		if(sfConfig::get('app_centro_actual_id') != sfConfig::get('app_central_id'))
		{
			$c->add(ConciliacionPeer::CENTRO_ID,sfConfig::get('app_centro_actual_id'));
			$ip_actual = Comun::ipReal();
			$c->add(ConciliacionPeer::IP_CAJA, $ip_actual);
			 
			//TODO: también se debe meter un rango de fecha a traer y traer más con un paginador
		}
		$this->form = new ConciliacionForm();
		$this->Conciliaciones = ConciliacionPeer::doSelect($c);
		 
	}

	public function executeCreate(sfWebRequest $request)
	{
		$this->forward404Unless($request->isMethod(sfRequest::POST));

		$this->form = new ConciliacionForm();

		$this->processForm($request, $this->form);

		$this->setTemplate('new');
	}

	protected function processForm(sfWebRequest $request, sfForm $form)
	{
		$form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
		if ($form->isValid())
		{
			//Asociamos el operador que esta dando salida.
			$operador = $this->getUser()->getAttribute('usuario');
			$operador_id = $operador->getId();

			$es_local = true;
			 
			//Si el centro actual es diferente de central quiere decir q la conciliacion viene de la caja
			if(sfConfig::get('app_centro_actual_id') != sfConfig::get('app_central_id'))
			{
					
				$orden = new Orden();
				$orden->setTipoId(sfConfig::get('app_orden_conciliacion_administrativa_id'));
				$orden->setUsuario($operador);
				$orden->setSocioId(null);
				$orden->setCentroId(sfConfig::get('app_centro_actual_id'));
				$orden->setIpCaja($form->getObject()->getIpCaja());
				$orden->setTotal($form['monto']->getValue());
				$orden->setFormaPagoId(sfConfig::get('app_pago_efectivo_id'));
					
				//Asociamos la ip de la caja.
				$form->getObject()->setIpCaja(Comun::ipReal());
				$form->getObject()->setCentroId(sfConfig::get('app_centro_actual_id'));
				$form->getObject()->setFolio(FolioConciliacionPeer::folio($form->getObject()->getCentroId()));
				$form->getObject()->setUsuario($operador);
				$form->getObject()->setOrden($orden);
					
				$Conciliacion = $form->save();
				$this->getUser()->setFlash('ok', 'Se ha reportado la conciliación correctamente');
				$this->redirect('conciliacion_adm/new');

			}
			else
			{
				try  //El try de la conexion al cliente
				{
					//Inicializamos el cliente del ws para el centro dado:
					$p = sfConfig::get('app_proto_wsdl');
					$p='http://';

					$centro = CentroPeer::retrieveByPK($form['centro_id']->getValue());
						
					$chost = $centro->getHostname();
					$wsdl = sfConfig::get('app_path_mako_wsdl');
					$wsdl = 'MakoApi.wsdl';
					$cliente = @new SoapClient($p.$chost."/".$wsdl);

				}
				catch (Exception $e)
				{
					$ermsg = "Error:[".$this->getModuleName().".".$this->getActionName()."]: $chost  Err: ".$e->getMessage();
					error_log($ermsg);
					$this->getUser()->setFlash('error', 'No ha sido posible conectarse al centro a conciliar, reporte al área técnica del fallo en la comunicación con el centro e intente nuevamente cuando la comunicación sea reestablecida.');
					$this->redirect('conciliacion_adm/new');
				}
					
				try //El try de la llamada al web method remoto
				{
					$res = $cliente->conciliacion_adm_conciliacion(
							$form['centro_id']->getValue(),
							$operador_id,
							$form['monto']->getValue(),
							$form['folio_docto_aval']->getValue(),
							$form['ip_caja']->getValue(),
							$form['observaciones']->getValue()
					);

					//Obtenemos los ids de resultado remoto.
					$aRes = $res->item;
					$conciliacion_id = $aRes[0];
					$orden_id = $aRes[1];
					$folio = $aRes[2];

					$this->getUser()->setFlash('ok', 'Se ha reportado la conciliación correctamente. ');
				}
				catch (Exception $e)
				{
					$ermsg = "Error:[".$this->getModuleName().".".$this->getActionName()."]: $chost  Err: ".$e->getMessage();
					error_log($ermsg);
					$this->getUser()->setFlash('error', 'Ha ocurrido un error al ejecutar la conciliación remota: '.$ermsg);
				}
				//Generamos la orden de conciliacion que se va a guardar localmente, Esta se guarda solo en central como copia de la enviada
					
				$orden = new Orden();
				$orden->setCentroId($form['centro_id']->getValue());
				$orden->setTipoId(sfConfig::get('app_orden_conciliacion_administrativa_id'));
				$orden->setUsuario($operador);
				$orden->setFolio($folio); //Viene de la respuesta del ws
				$orden->setSocioId(null);
				$orden->setIpCaja($form['ip_caja']->getValue());
				$orden->setTotal($form['monto']->getValue());
				$orden->setFormaPagoId(sfConfig::get('app_pago_efectivo_id'));
					
				$form->getObject()->setCentroId($form['centro_id']->getValue());
				$form->getObject()->setOperadorId($operador_id);
				$form->getObject()->setIpCaja($form['ip_caja']->getValue());
				$form->getObject()->setMonto($form['monto']->getValue());
				$form->getObject()->setObservaciones($form['observaciones']->getValue());
				$form->getObject()->setFolioDoctoAval($form['folio_docto_aval']->getValue());
				$form->getObject()->setId($conciliacion_id);
				$form->getObject()->setFolio($folio);

				//Asociamos la orden de conciliación con esta conciliacion
				$orden->setId($orden_id); //Viene de la respuesta del ws
				$form->getObject()->setOrden($orden);
				$form->getObject()->save();
					
			}
			$this->redirect('conciliacion_adm/new');
		}
	}

	/**
	 * Web Method para recibir las conciliaciones administrativas desde central
	 * Este sólo se ejecuta por los centros locales, no por central.
	 *
	 * @WSMethod(webservice='MakoApi')
	 *
	 * @param string $centro_id Identificador del centro que se va a conciliar.
	 * @param string $operador_id Identificador del usuario que realizo la conciliación.
	 * @param string $monto Monto de la conciliación.
	 * @param string $folio_docto_aval Folio que avala la solicitud de la conciliación.
	 * @param string $ip_caja IP de la caja a conciliar.
	 * @param string $observaciones Observaciones al respecto.
	 *
	 * @return string[] Regresa un arreglo con el id de la orden de Conciliacion y el id de la conciliacion
	 */
	public function executeConciliacion($request)
	{
		//Asociamos el operador que esta realizando la operacion.
		$centro_id = $request->getParameter('centro_id');
		$operador_id = $request->getParameter('operador_id');
		$monto = $request->getParameter('monto');
		$folio_docto_aval = $request->getParameter('folio_docto_aval');
		$ip_caja = $request->getParameter('ip_caja');
		$observaciones = $request->getParameter('observaciones');

		//Obtenemos el folio de la conciliacion para el centro dado.
		$folio_conciliacion = FolioConciliacionPeer::folio($centro_id);
		 
		//Generamos la conciliación
		$conciliacion = new Conciliacion();

		$conciliacion->setIpCaja($ip_caja);
		$conciliacion->setOperadorId($operador_id);
		$conciliacion->setCentroId($centro_id);
		$conciliacion->setMonto($monto);
		$conciliacion->setFolioDoctoAval($folio_docto_aval);
		$conciliacion->setObservaciones($observaciones);
		$conciliacion->setFolio($folio_conciliacion);
		 
		//Generamos la orden de conciliacion
		$orden = new Orden();

		$orden->setTipoId(sfConfig::get('app_orden_conciliacion_administrativa_id'));
		$orden->setCentroId($centro_id);
		$orden->setOperadorId($operador_id);
		$orden->setSocioId(null);
		$orden->setIpCaja($ip_caja);
		$orden->setTotal($monto);

		$orden->setFormaPagoId(sfConfig::get('app_pago_efectivo_id'));

		//Folio de la orden
		$orden->setFolio($conciliacion->getFolio());
		 
		//Asociamos la orden de conciliación con esta conciliacion
		$conciliacion->setOrden($orden);

		try
		{
			$conciliacion->save();

			//se incrementa el folio de conciliacion para el centro dado.
			FolioConciliacionPeer::folioSiguiente($centro_id);
				
			$this->result = array($conciliacion->getId(),$orden->getId(),$folio_conciliacion);
				
			return sfView::SUCCESS;
				
		}
		catch (Exception $e)
		{
			$me = $this->isSoapRequest() ? new SoapFault('Server', $e->getMessage().": ".'') : $e->getMessage().": ".'';
			throw $me;
				
			return sfView::ERROR;
		}
	}

}
