El grupo <b><?php echo $grupo->getClave() ?></b> ah sido cancelado por falta de socios
inscritos (<?php echo $alumnos['inscritos'] ?>) o preinscritos (<?php echo $alumnos['preinscritos'] ?>)

<hr>

<table>
    <caption style="border: solid #000 1px; border-bottom: none;">
        <h2>Detalles del grupo</h2>
    </caption>
    <tr>
        <th style="padding: 5px; border: solid #000 1px; border-right: none; border-bottom: none;">Centro</th>
        <td style="padding: 5px; border: solid #000 1px; border-bottom: none"><?php echo $grupo->getCentro()->getNombre() ?></td>
    </tr>
    <tr>
        <th style="padding: 5px; border: solid #000 1px; border-right: none; border-bottom: none;">Curso</th>
        <td style="padding: 5px; border: solid #000 1px; border-bottom: none"><?php echo $grupo->getCurso()->getNombre() ?></td>
    </tr>
    <tr>
        <th style="padding: 5px; border: solid #000 1px; border-right: none; border-bottom: none;">Clave</th>
        <td style="padding: 5px; border: solid #000 1px; border-bottom: none"><?php echo $grupo->getClave() ?></td>
    </tr>
    <tr>
        <th style="padding: 5px; border: solid #000 1px; border-right: none; border-bottom: none;">Facilitador</th>
        <td style="padding: 5px; border: solid #000 1px; border-bottom: none"><?php echo $grupo->getUsuarioRelatedByFacilitadorId()->getNombre() ?></td>
    </tr>
    <tr>
        <th style="padding: 5px; border: solid #000 1px; border-right: none; border-bottom: none;">Aula</th>
        <td style="padding: 5px; border: solid #000 1px; border-bottom: none"><?php echo $grupo->getAula()->getNombre() ?></td>
    </tr>
    <tr>
        <th style="padding: 5px; border: solid #000 1px; border-right: none; border-bottom: none;">Inicio</th>
        <td style="padding: 5px; border: solid #000 1px; border-bottom: none"><?php echo $grupo->getHorario()->getFechaInicio() ?></td>
    </tr>
    <tr>
        <th style="padding: 5px; border: solid #000 1px; border-right: none;">Fin</th>
        <td style="padding: 5px; border: solid #000 1px;"><?php echo $grupo->getHorario()->getFechaFin() ?></td>
    </tr>
</table>