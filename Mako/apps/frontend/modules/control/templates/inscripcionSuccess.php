<?php use_stylesheet('control_escolar/control_escolar.css') ?>
<?php use_stylesheet('control_escolar/inscripcion.css') ?>
<?php use_stylesheet('jquery.windows.css') ?>
<?php use_stylesheet('jquery.fullcalendar.css') ?>
<?php use_javascript('jquery.ui.js') ?>
<?php use_javascript('plugins/jquery.windows.js') ?>
<?php use_javascript('plugins/jquery.fullcalendar.js') ?>
<?php use_javascript('control_escolar/registro_control.js') ?>
<?php use_javascript('control_escolar/inscripcion.js') ?>


<script type="text/javascript">
  $(function() {
    $("#lista-categorias").tabs({
              ajaxOptions: {
                            beforeSend : function(){
                              $('#loading').show();
                            },
                            complete : function(){
                              $('#loading').hide();
                            }
                          },
              show: function(event, ui) {
                            $('.info').height(220);
                         }
        });
});

</script>
<div id="ventana-buscador-socios">
    <div id="buscador">
        <?php include_component('registro', 'buscaSocios') ?>
    </div>
</div>
<div id="ventana-calendar">
    <div id="datos-pre-inscribir">
        <div id="datos-finales"></div>
        <p class="botones_finales" style="text-align: right">
            <input type="hidden" id="curso-pre-inscribir" /> <input type="button"
                value="Pre-Iscribir a este curso" id="end" />
        </p>
    </div>
    <div id="calendar"></div>
</div>

<div id="windows-wrapper">
    <div id="registro" window-title="Datos del alumno" window-state="min">

        <h2>Datos del Socio</h2>

        <div id="resultados-socio" class="ui-corner-all ui-widget-content">
            <div id="resultados-busqueda">
                <?php if(isset($matricula )) : ?>
                <?php include_component('registro', 'buscaSocios',array('id_buscador'=>'','socio_id'=>$matricula)) ?>
                <?php endif ?>
            </div>
            <input type="button" id="buscar-nuevo" value="Buscar socio"
                style="float: right;" />
            <p>&nbsp;</p>
        </div>
        <?php include_partial('registro', array('form' => $registro, 'matricula'=>$matricula, 'documentos'=>$entregado)) ?>
        <?php include_partial('inscrito', array('tomados' => $tomados)) ?>

    </div>

    <div id="cursos" window-title="Detalle de cursos" window-state="max">
        <h2>Detalle de cursos</h2>
        <div id="loading">
            Cargando…<br /> <img src="/imgs/ajax-loader.gif" />
        </div>
        <div id="lista-categorias">
            <ul class="lista-categorias">
                <?php foreach ($categorias as $categoria ): ?>
                <li><a href="categoria?id=<?php echo $categoria->getId()?>"><?php echo $categoria->getNombre() ?>
                </a></li>
                <?php endforeach ?>
            </ul>
        </div>
    </div>

</div>

