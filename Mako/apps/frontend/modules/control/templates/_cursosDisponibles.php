<?php use_stylesheet('./control/_cursosDisponibles.css')?>
<div class="datos-usuario">

    <h3>Cursos</h3>

    <div class="tabs">

        <ul>
            <li><a href="#tab-preinscritos">Preinscritos</a>
            </li>

            <li><a href="#tab-actuales">Actuales</a>
            </li>
            <li><a href="#tab-finalizados">Finalizados</a>
            </li>
        </ul>


        <div id="tab-preinscritos" class="preinscritos curso">
            <table class="cursos-preinscritos" width="100%">
                <thead>
                    <tr>
                        <th>Clave grupo</th>
                        <th>Nombre</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>

                    <?php foreach ($preinscritos as $key => $preinscrito): ?>
                    <tr>
                        <td><?php echo $preinscrito['clave']?></td>
                        <td><?php echo $preinscrito['nombre']?></td>
                        <!--Corrección ortográfica: "Elimnar" por "Eliminar" a raíz de ID: 38. del modulo PVP. Responsable: FE.-->
                        <td><?php echo link_to('<span class="ui-icon ui-icon-trash bx-title borrar" title="Eliminar"></span>', 'control/deletePreinscripcion?id='.$key) ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>

                </tbody>
            </table>
        </div>


        <div id="tab-finalizados" class="finalizados curso">

            <table class="cursos-finalizados" width="100%">
                <thead>
                    <tr>
                        <th>Clave grupo</th>
                        <th>Nombre</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>

                    <?php foreach ($finalizados as $finalizado): ?>
                    <tr>
                        <td><?php echo $finalizado['clave']?></td>
                        <td><?php echo $finalizado['nombre']?></td>
                        <td width="120"><?php echo ($finalizado['adeudo'] ? '<input type="button" value="Condonar deuda" class="condonar-deuda" grupo="' . $finalizado['grupo_id']  .  '" " socio="'.$socio->getId().'" />': '') ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>

                </tbody>
            </table>

        </div>


        <div id="tab-actuales" class="actuales curso">
            <table class="cursos-actuales" width="100%">
                <thead>
                    <tr>
                        <th>Clave grupo</th>
                        <th>Nombre</th>
                        <th>Fecha Inicio</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>

                    <?php foreach ($actuales as $actual): ?>
                    <tr>
                        <td><?php echo $actual['clave']?></td>
                        <td><?php echo $actual['nombre']?></td>
                        <td><?php echo $actual['fecha_inicio']?></td>
                        <td width="120"><?php echo ($actual['iniciado'] ? '<input type="button" value="Marcar como abandonado" class="abandonar-grupo" grupo="'.$actual['id'].'" socio="'.$socio->getUsuario().'" />': '') ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>

                </tbody>
            </table>
        </div>

    </div>

</div>

<div class="cursos-disponibles">
    <h3>Cursos disponibles</h3>
    <p style="text-align: right; padding: 0; margin: 0 5px 5px 0;">
        <a href="#"
            onclick="$(document).trigger('preinscripcion.cancelada'); return false;"><span
            class="ui-icon ui-icon-refresh bx-title"
            style="display: inline-block;" title="Refrescar"></span> </a>
    </p>

    <div class="tabs">
        <ul>
            <?php if(count($categorias)>0): ?>
            <?php foreach ($categorias as $categoria): ?>
            <li><a href="#tabs-<?php echo $categoria['id'] ?>"><?php echo $categoria['nombre'] ?>
            </a></li>
            <?php endforeach; ?>
            <?php else: ?>
            <li><a href="#tab-0">No hay cursos disponibles</a></li>
            <?php endif; ?>

        </ul>
        <?php if(count($categorias)>0): ?>
        <?php foreach ($categorias as $categoria): ?>
        <div id="tabs-<?php echo $categoria['id'] ?>">

            <div class="accordion">
                <?php foreach ($cursos[$categoria['id']] as $curso):?>
                <h3>
                    <a href="#"><?php echo $curso['clave'].' '.$curso['nombre'] ?> </a>
                </h3>
                <div>

                    <table class="horario" width="100%">
                        <thead>
                            <tr>
                                <th>Grupo</th>
                                <th>Aula</th>
                                <th>Inicia</th>
                                <th>Termina</th>
                                <th>Lu</th>
                                <th>Ma</th>
                                <th>Mi</th>
                                <th>Ju</th>
                                <th>Vi</th>
                                <th>S&aacute;</th>
                                <th>Libre</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($horarios[$curso['id']] as $horario): ?>
                            <tr>
                                <td><?php echo $horario['grupo']?></td>
                                <td><?php echo $horario['aula']?></td>
                                <td><?php echo $horario['inicio']?></td>
                                <td><?php echo $horario['fin']?></td>
                                <td><?php echo $horario['lunes']?></td>
                                <td><?php echo $horario['martes']?></td>
                                <td><?php echo $horario['miercoles']?></td>
                                <td><?php echo $horario['jueves']?></td>
                                <td><?php echo $horario['viernes']?></td>
                                <td><?php echo $horario['sabado']?></td>
                                <td><?php echo $horario['disponible']?></td>
                                <td><?php echo link_to('<span class="ui-icon ui-icon-note bx-title lista" title="Ver lista del grupo"></span>', 'control/lista_alumnos?grupo='.$horario['id']) ?>
                                </td>
                                <!--
                Reingeniería PV | Responsable: FE. - Fecha: 22-10-2013. - ID: 34. - Campo: Cursos disponibles / Icono Enrolar a curso.
                Descripción del cambio: Se cambio leyenda enrolar por preinscribir.
                -->
                                <td><?php echo ( $horario['adeudo'] == 1 ? '<a href="#" class="ui-state-error adeudo"><span class="ui-icon ui-icon-calendar bx-title" title="Tiene adeudo de pagos a un curso anterior"></span></a>' : link_to('<span class="ui-icon ui-icon-calendar bx-title enrolar" title="Preinscribir al curso"></span>', 'control/preinscripcion?grupo='.$horario['id'].'&socio='.$socio->getId())) ?>
                                </td>
                                <!-- Fecha: 22-10-2013. - Fin -->
                            </tr>
                            <?php endforeach; ?>

                        </tbody>
                    </table>

                </div>
                <?php endforeach; ?>
            </div>

        </div>
        <?php endforeach; ?>
        <?php else: ?>
        <div id="tab-0">
            <p
                style="text-align: center; padding: 30px 0; font-size: 14px; font-weight: bold">
                No existen cursos para el perfil del socio</p>
        </div>
        <?php endif; ?>
    </div>

</div>
<div id="lista-alumnos" title="Lista de Alumnos"></div>
<div id="abandono-auth" title="Autorizar abandono">
    <h3>Se requiere la autorizaci&oacute;n para esta operaci&oacute;n</h3>
    <table>
        <tr>
            <th>Usuario</th>
            <td><input type="text" id="auth-usuario" requerido="1" /></td>
        </tr>
        <tr>
            <th>Clave</th>
            <td><input type="password" id="auth-pass" requerido="1" /></td>
        </tr>
        <tr>
            <td colspan="2" id="abandono-message"></td>
        </tr>
    </table>
</div>
<script type="text/javascript">
var Pre;
var grupo;
var socio;
var accionAuth;
$(function() {

        $(".tabs").tabs();

        $( "#lista-alumnos" ).dialog({
            height: 500,
                        width: 400,
            modal: true,
                        autoOpen: false,
            close: function( event, ui ) {
                        $( "#lista-alumnos" ).html('');
                    }
        });
        $('.adeudo').click(function(){
            alert('Imposible enrolar a este grupo, el socio tiene adeudos de pago a un curso anterior.\nConsulte la pestaña "Finalizados" para obtener más información');
            return false;
        });
        $("#abandono-auth").dialog({
            height: 250,
                        width: 420,
            modal: true,
                        autoOpen: false,
                        buttons: {
                "Autorizar": function() {
                                    var dialogo = $(this);
                                    var urlAction;
                                    var usuario = $(this).find('#auth-usuario').val(), clave = $(this).find('#auth-pass').val();
                                    if(usuario == '' || clave == '')
                                    {
                                        alert('El usuario y contraseña son requeridos');
                                    }
                                    else
                                    {
                                        if(accionAuth == 'abandonar')
                                        {
                                            urlAction =  '<?php echo url_for('control/autorizar_abandono') ?>';
                                        }
                                        if(accionAuth == 'condonar')
                                        {
                                            urlAction =  '<?php echo url_for('control/condonar_deuda') ?>';
                                        }
                                        $.ajax({
                                            url : urlAction,
                                            dataType : 'json',
                                            data : {grupo : grupo, socio : socio, usuario : usuario, clave : clave },
                                            beforeSend : function(){
                                                $("#abandono-message").html('<p style="text-align:center"><img src="/imgs/ajax-loader.gif" alt="..."/><p>');
                                            },
                                            success: function(data) {
                                                $("#abandono-message").html('');
                                                if(data.auth == 1)
                                                {
                                                    dialogo.dialog( "close" );
                                                    dialogo.find('#auth-usuario').val('');
                                                    dialogo.find('#auth-pass').val('');
                                                    $(document).trigger('preinscripcion.cancelada');
                                                    /*Reingeniería PV | Responsable: FE. - Fecha: 29-10-2013. - ID: 44. - Campo: Ventana desplegable Cursos.
                                                    Descripción del cambio: Se crea mensaje de afirmación de abandono de curso.*/
                                                    alert("El curso ha sido abandonado");
                                                    /*Fecha: 29-10-2013. - Fin*/
                                                }
                                                else
                                                {
                                                    dialogo.find('#auth-usuario').val('').focus();
                                                    dialogo.find('#auth-pass').val('');
                                                    alert('Usuario y/o Clave incorrectos');
                                                }
                                          }
                                       });
                                    }
                },
                "Cancelar": function() {
                    $(this).dialog( "close" );
                }
            }
        });

       $(".accordion").accordion({
           collapsible: true,
           autoHeight: false
       });

       $(".cursos-finalizados").dataTable({
        "bPaginate": true,
        "bLengthChange": false,
        "bFilter": true,
        "bSort": true,
        "bInfo": false,
        "bAutoWidth": false,
                "bJQueryUI": true,
                 "sPaginationType": "full_numbers"
                });

       Pre = $(".cursos-preinscritos").dataTable({
        "bPaginate": true,
        "bLengthChange": false,
        "bFilter": true,
        "bSort": true,
        "bInfo": false,
        "bAutoWidth": false,
                "bJQueryUI": true,
                "sPaginationType": "full_numbers",
                "aoColumns": [null,
                              null,
                              {"bSortable": false}
                             ]
                });
       $(".cursos-actuales").dataTable({
        "bPaginate": true,
        "bLengthChange": false,
        "bFilter": true,
        "bSort": true,
        "bInfo": false,
        "bAutoWidth": false,
                "bJQueryUI": true,
                "sPaginationType": "full_numbers",
                "aoColumns": [null,
                              null,
                              null,
                              {"bSortable": false}

                             ]
                });

       $(".horario").dataTable({
        "bPaginate": true,
        "bLengthChange": false,
        "bFilter": true,
        "bSort": true,
        "bInfo": false,
        "bAutoWidth": false,
        "bJQueryUI": true,
        "sPaginationType": "full_numbers",
        "aoColumns": [
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                {"bSortable": false},
                                {"bSortable": false}

                             ],
        "aaSorting": [[2, 'asc']]
         });

       $("input[name='radio']").change(function(){
           $(".curso").hide();
           $("."+$(this).attr('id')).show();
       });
       //Eventos para mostrar lista de alumnos
       $('.lista').click(function(){
           var url = $(this).parent().attr('href');
           $.ajax({
                  url : url,
                  beforeSend : function(){
                      $( "#lista-alumnos" ).dialog('open');
                      $( "#lista-alumnos" ).html('<p style="text-align:center">Cargando...<p><p style="text-align:center"><img src="/imgs/ajax-loader.gif" alt="..."/><p>');
                  },
                  success: function(data) {
                      $( "#lista-alumnos" ).html(data);
                  }
               });

           return false;
       });
       //Eventos para enrolar y borrar
       $('.enrolar').click(function(){
           var link = $(this).parent();
           var url = $(this).parent().attr('href');

           $.ajax({
                  url : url,
                  dataType : 'json',
                  beforeSend : function(){
                      link.hide(500);
                  },
                  success: function(data) {
                      if(data.estatus)
                      {
                          console.log('ok');
                        link.parent().parent().addClass('ui-state-disabled');
                        Pre.fnAddData( [
                                data.clave,
                                data.nombre,
                                '<a href="<?php echo url_for('control/deletePreinscripcion')?>/id/'+data.id+'"><span class="ui-icon ui-icon-trash bx-title borrar" title="Eliminar"></span></a>'
                                ]);
                        $('#radio-preinscritos').addClass('ui-state-highlight');
                        handlerBorrar();
                      }
                      else
                      {
                        alert(data.detalle)
                      }
                  }
               });

           return false;
       });
       // Evento para abandonar grupo
       $('.abandonar-grupo').click(function(){
            grupo = $(this).attr('grupo');
            socio = $(this).attr('socio');

            /*Reingeniería PV | Responsable: FE. - Fecha: 29-10-2013. - ID: 43. - Campo: Ventana desplegable Cursos.
            Descripción del cambio: Se crea mensaje de confirmación de abandono de curso.*/
            curso_gru  = $(this).parent().parent().find('td:first-child').text();
            curso_nom  = $(this).parent().parent().find('td:not([class])').html();
            accionAuth = 'abandonar';

            if(confirm("¿Estás seguro de abandonar el grupo '"+curso_gru+"' del curso '"+curso_nom+"'?"))
            {
                $( "#abandono-auth" ).dialog("open");
            }
            /*Fecha: 29-10-2013. - Fin*/
       });
       // Evento para condonar deuda
       $('.condonar-deuda').click(function(){
            grupo = $(this).attr('grupo');
            socio = $(this).attr('socio');
            accionAuth = 'condonar';
            $("#abandono-auth").dialog("open");
       });
       function handlerBorrar()
       {
           $('.borrar').click(function(){
               if(confirm("¿Deseas eliminar al socio del curso?"))
               {
                   var link = $(this).parent();
                       var pos = this.parentNode;
                   var url = $(this).parent().attr('href');
                   $.ajax({
                          url : url,
                          beforeSend : function(){
                              link.hide(500);
                          },
                          success: function(data) {
                              var aPos = Pre.fnGetPosition( pos.parentNode.parentNode );
                              Pre.fnDeleteRow( aPos );
                              $(document).trigger('preinscripcion.cancelada');
                          }
                       });
                   return false;
               }
               return false;
           });
       }

       handlerBorrar();
});
</script>
