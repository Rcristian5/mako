<script type="text/javascript">
  $(function() {
    var icons = {
            header: "ui-icon-circle-arrow-e",
            headerSelected: "ui-icon-circle-arrow-s"
              };
        $("#cursos-tomados").accordion({
                           collapsible: true,
                           header: 'h3',
                           icons:icons
                          });
    });

</script>
<div>
    <h2>Cursos que toma el socio</h2>
    <div id="cursos-tomados">
        <?php foreach($tomados as $curso): ?>
        <h3>
            <a href="#"><?php echo $curso['nombre']?> </a>
        </h3>
        <div class="tomados">
            <p>
                <span>Clave: </span>
                <?php echo $curso['clave']?>
            </p>
            <p>
                <span>Descripci&oacute;n: </span>
                <?php echo $curso['descripcion']?>
            </p>
            <p>
                <span>Fecha de inicio: </span>
                <?php echo substr($curso['fecha_inicio'],0,10)?>
            </p>
            <p>
                <?php if($curso['preinscrito']):?>
                <input type="button" value="Eliminar"
                    onclick="location.href='cancelar?id=<?php echo $curso['id']?>';" />
                <?php endif; ?>
            </p>
        </div>
        <?php endforeach ?>
    </div>
</div>
