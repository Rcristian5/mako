<?php if( count($cursos['grupos']) > 0 && count($cursos['curso']) > 0 ): 
?>
    <div id="lista-cursos">
        <h2>Grupos del mes</h2>
        <div id="accordion-cursos">
            <?php foreach ($cursos['curso'] as $key => $curso): ?>
                <h3>
                    <a href="#"><?php echo $curso ?> </a>
                </h3>
                <div>
                    <table class="display detalle-cursos" width="100%">
                        <thead>
                            <tr>
                                <th><font size="0.5">Grupo</font></th>
                                <th><font size="0.5">Aula</font></th>
                                <th><font size="0.5">Inicio</font></th>
                                <th><font size="0.5">Cupo</font></th>
                                <th><font size="0.5">Disponible</font></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        <!--    Correcion tamaño de tabla
                                04/11/2014
                                responsable: IMB"
                            -->
                            <?php foreach ($cursos['grupos'][$key] as $grupo): ?>
                                <tr>
                                    <td><font size="0.5"><?php echo $grupo['clave']; ?></font></td>
                                    <td><font size="0.5"><?php echo $grupo['aula']; ?></font></td>
                                    <td><font size="0.5"><?php echo $grupo['fecha_inicio']; ?></font></td>
                                    <td><font size="0.5"><?php echo $grupo['cupo_total']; ?></font></td>
                                    <td>
                                        <?php echo ($grupo['cupo_disponible'] ? $grupo['cupo_disponible'] : 'CUPO ALCANZADO')?>
                                    </td>
                                    <td>
                                        <a href="#" class="show-group" grupo-id="<?php echo $grupo['id'] ?>">
                                            <span class="ui-icon ui-icon-note"></span>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            <?php endforeach; ?>
        </div>
    </div>

    <script type="text/javascript">
        $(function() {
            $( "#accordion-cursos" ).accordion();

            $('.show-group').click(function(){
               $.ajax({
                    url : '<?php echo url_for('control/lista_grupo') ?>',
                    data :
                    {
                        id : $(this).attr('grupo-id')
                    },
                    success: function(data)
                    {
                        $('#detalle-grupo-alumno').html(data)
                    },
                    error: function(data)
                    {
                        console.error(data);
                    }
                });
            });
        });
    </script>
<?php else: ?>
    <h4 style="text-align: center; font-size: 13px">
        No hay grupos calendarizados para el mes seleccionado.
    </h4>
<?php endif;?>