<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<form
    action="<?php echo url_for('control/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>"
    method="post"
    <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
    <?php if (!$form->getObject()->isNew()): ?>
    <input type="hidden" name="sf_method" value="put" />
    <?php endif; ?>
    <table>
        <tbody>
            <?php foreach ($form->getGlobalErrors() as $nombre => $error): ?>
            <li><?php echo $nombre.': '.$error ?></li>
            <?php endforeach; ?>
            <tr>
                <th><?php echo $form['matricula']->renderLabel('Matricula') ?></th>
                <td><?php echo $form['matricula']->renderError() ?> <?php echo $form['matricula']->render(array('disabled'=>'disabled','value'=>$matricula)) ?>
                </td>
            </tr>
            <tr>
                <th><?php echo $form['acta_nacimiento']->renderLabel() ?></th>
                <td><?php echo $form['acta_nacimiento']->renderError() ?> <?php echo $form['acta_nacimiento'] ?>
                    <?php if(isset($documentos)): ?> <?php if(isset($documentos['acta'])): ?>
                    <span class="ui-icon ui-icon-check" title="Documento entregado"
                    style="float: right;"></span> <?php else :?> <span
                    class="ui-icon ui-icon-alert" title="Falta entregar este documento"
                    style="float: right"></span> <?php endif; ?> <?php endif; ?>
                </td>
            </tr>
            <tr>
                <th><?php echo $form['id_fotografia']->renderLabel() ?></th>
                <td><?php echo $form['id_fotografia']->renderError() ?> <?php echo $form['id_fotografia'] ?>
                    <?php if(isset($documentos)): ?> <?php if(isset($documentos['iden'])): ?>
                    <span class="ui-icon ui-icon-check" title="Documento entregado"
                    style="float: right"></span> <?php else :?> <span
                    class="ui-icon ui-icon-alert" title="Falta entregar este documento"
                    style="float: right"></span> <?php endif; ?> <?php endif; ?>
                </td>
            </tr>
            <tr>
                <th><?php echo $form['comprobante_domicilio']->renderLabel() ?></th>
                <td><?php echo $form['comprobante_domicilio']->renderError() ?> <?php echo $form['comprobante_domicilio'] ?>
                    <?php if(isset($documentos)): ?> <?php if(isset($documentos['comprobante'])): ?>
                    <span class="ui-icon ui-icon-check" title="Documento entregado"
                    style="float: right"></span> <?php else :?> <span
                    class="ui-icon ui-icon-alert" title="Falta entregar este documento"
                    style="float: right"></span> <?php endif; ?> <?php endif; ?>
                </td>
            </tr>
            <tr>
                <th><?php echo $form['curp']->renderLabel() ?></th>
                <td><?php echo $form['curp']->renderError() ?> <?php echo $form['curp'] ?>
                    <?php if(isset($documentos)): ?> <?php if(isset($documentos['curp'])): ?>
                    <span class="ui-icon ui-icon-check" title="Documento entregado"
                    style="float: right"></span> <?php else :?> <span
                    class="ui-icon ui-icon-alert" title="Falta entregar este documento"
                    style="float: right"></span> <?php endif; ?> <?php endif; ?>
                </td>
            </tr>
        </tbody>
    </table>
    <p class="botones_finales">
        <?php echo $form->renderHiddenFields(false) ?>
        <?php if (!$form->getObject()->isNew()): ?>
        <!--&nbsp;<?php echo link_to('Delete', 'curso/delete?id='.$form->getObject()->getId(), array('method' => 'delete', 'confirm' => 'Are you sure?')) ?>-->
        <?php endif; ?>
        <input type="button" value="Cancelar"
            onclick="location.href='registro'" />
        <?php if ($form->getObject()->isNew()): ?>
        <input type="submit" value="Guardar" />
        <?php else : ?>
        <input type="submit" value="Actualizar datos" />
        <?php endif; ?>
    </p>
</form>
