El grupo <?php echo $grupo->getClave() ?> ah sido cancelado por falta de socios inscritos (<?php echo $alumnos['inscritos'] ?>) o preinscritos (<?php echo $alumnos['preinscritos'] ?>)

Detalles del grupo
================================================================================
Centro: <?php echo $grupo->getCentro()->getNombre() ?>

================================================================================
Curso: <?php echo $grupo->getCurso()->getNombre() ?>

================================================================================
Clave: <?php echo $grupo->getClave() ?>

================================================================================
Facilitador: <?php echo $grupo->getUsuarioRelatedByFacilitadorId()->getNombre() ?>

================================================================================
Aula: <?php echo $grupo->getAula()->getNombre() ?>

================================================================================
Fecha inicio: <?php echo $grupo->getHorario()->getFechaInicio() ?>

================================================================================
Fecha fin: :<?php echo $grupo->getHorario()->getFechaFin() ?>

================================================================================