<h3>
    Grupo
    <?php echo $grupo['nombre'] ?>
</h3>

<table id="detalle-lista" width="100%">
    <thead>
        <tr>
            <th>Socio</th>
            <th>Estado</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($grupo['lista'] as $alumno ): ?>
        <tr>
            <td><?php echo $alumno['nombre']?></td>
            <td><?php echo $alumno['estado']?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<script type="text/javascript">
$(document).ready(function()
{
        $('#detalle-lista').dataTable({
        "bPaginate": false,
        "bLengthChange": false,
        "bFilter": true,
        "bSort": true,
        "bInfo": true,
        "bAutoWidth": false,
        "bJQueryUI": true,
        "sPaginationType": "full_numbers"
        });

 });
</script>
