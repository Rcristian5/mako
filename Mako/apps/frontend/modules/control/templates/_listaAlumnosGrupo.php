<?php if(isset ($grupo)): ?>
<h2>
    <?php echo $grupo->getClave() ?>
</h2>
<table>
    <tr>
        <th>Curso:</th>
        <td><?php echo $grupo->getCurso()->getNombre() ?></td>
    </tr>
    <tr>
        <th>Descripci&oacute;n:</th>
        <td><?php echo $grupo->getCurso()->getDescripcion() ?></td>
    </tr>
    <tr>
        <th>Facilitador:</th>
        <td><?php echo $grupo->getUsuarioRelatedByFacilitadorId()->getNombreCompleto() ?>
        </td>
    </tr>
    <tr>
        <th>Inicia:</th>
        <td><?php echo $grupo->getHorario()->getFechaInicio('Y-m-d') ?></td>
    </tr>
    <tr>
        <th>Termina:</th>
        <td><?php echo $grupo->getHorario()->getFechaFin('Y-m-d') ?></td>
    </tr>
    <tr>
        <th>Lunes:</th>
        <td><?php echo ($grupo->getHorario()->getHiLu() ? $grupo->getHorario()->getHiLu('H:i') : '').' - '.($grupo->getHorario()->getHfLu() ? $grupo->getHorario()->getHfLu('H:i') : '') ?>
        </td>
    </tr>
    <tr>
        <th>Martes:</th>
        <td><?php echo ($grupo->getHorario()->getHiMa() ? $grupo->getHorario()->getHiMa('H:i') : '').' - '.($grupo->getHorario()->getHfMa() ? $grupo->getHorario()->getHfMa('H:i') : '') ?>
        </td>
    </tr>
    <tr>
        <th>Mi&eacute;rcoles:</th>
        <td><?php echo ($grupo->getHorario()->getHiMi() ? $grupo->getHorario()->getHiMi('H:i') : '').' - '.($grupo->getHorario()->getHfMi() ? $grupo->getHorario()->getHfMi('H:i') : '') ?>
        </td>
    </tr>
    <tr>
        <th>Jueves:</th>
        <td><?php echo ($grupo->getHorario()->getHiJu() ? $grupo->getHorario()->getHiJu('H:i') : '').' - '.($grupo->getHorario()->getHfJu() ? $grupo->getHorario()->getHfJu('H:i') : '') ?>
        </td>
    </tr>
    <tr>
        <th>Viernes:</th>
        <td><?php echo ($grupo->getHorario()->getHiVi() ? $grupo->getHorario()->getHiVi('H:i') : '').' - '.($grupo->getHorario()->getHfVi() ? $grupo->getHorario()->getHfVi('H:i') : '') ?>
        </td>
    </tr>
    <tr>
        <th>S&aacute;bado:</th>
        <td><?php echo ($grupo->getHorario()->getHiSa() ? $grupo->getHorario()->getHiSa('H:i') : '').' - '.($grupo->getHorario()->getHfSa() ? $grupo->getHorario()->getHfSa('H:i') : '') ?>
        </td>
    </tr>
    <tr>
        <th>Restricciones de horario</th>
        <td><?php if($grupo->getCurso()->getRestriccionesHorario()): ?>
            <ul class="lista-restricciones">
                <?php foreach ($grupo->getCurso()->getRestriccionesHorarios() as $restriccionHorario): ?>
                <?php echo ($restriccionHorario->getHoraInicio() ? '<li>'.$restriccionHorario->getHoraInicio('H:i').'</li>' : '') ?>
                <?php echo ($restriccionHorario->getHoraFin() ? '<li>'.$restriccionHorario->getHoraFin('H:i').'</li>' : '') ?>
                <?php endforeach;?>
            </ul> <?php else:?> <b>No tiene restricciones de horario</b> <?php endif;?>
        </td>
    </tr>
    <tr>
        <th>Restricciones de socio</th>
        <td><?php if($grupo->getCurso()->getRestriccionesSocio()): ?>
            <ul class="lista-restricciones">
                <?php foreach ($grupo->getCurso()->getRestriccionesSocios() as $restriccionSocio): ?>
                    <?php echo (($restriccionSocio->getSexo() != ' ')          ? '<li>Sexo: '.$restriccionSocio->getSexo().'</li>'                                                      : ''); ?>
                    <?php echo ($restriccionSocio->getOcupacionId()            ? '<li>Ocupaci&oacute;n: '.$restriccionSocio->getOcupacion()->getNombre().'</li>'                        : '') ?>
                    <?php echo ($restriccionSocio->getEstudia()                ? '<li>DEBE SER ESTUDIANTE</li>'                                                                         : '') ?>
                    <?php echo ($restriccionSocio->getNivelEstudioId()         ? '<li>&Uacute;ltimo nivel de estudios: '.$restriccionSocio->getNivelEstudio()->getNombre().'</li>'      : '') ?>
                    <?php echo ($restriccionSocio->getHabilidadInformaticaId() ? '<li>Habilidad inform&aacute;tica: '.$restriccionSocio->getHabilidadInformatica()->getNombre().'</li>' : '') ?>
                    <?php echo ($restriccionSocio->getDominioInglesId()        ? '<li>Dominio del ingl&eacute;s: '.$restriccionSocio->getDominioIngles()->getNombre().'</li>'           : '') ?>
                <?php endforeach;?>
            </ul> <?php else:?> <b>No tiene restricciones de socio</b> <?php endif;?>
        </td>
    </tr>
</table>
<h2>Socios enrolados al curso</h2>
<table class="display" width="100%" id="lista-socios">
    <thead>
        <tr>
            <th></th>
            <th>Socio</th>
            <th>Estado</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($grupo->getGrupoAlumnoss() as $alumnos): ?>
        <tr>
            <td align="center"><a href="javascript:void(0);" class="usuario"
                socio_id="<?php echo $alumnos->getAlumnos()->getSocio()->getId() ?>">
                    <span class="ui-icon  ui-icon-circle-zoomin"></span>
            </a>
            </td>
            <!-- 29/10/2014
                 permitir que se vean caracteres especiales en exportacion a excel 
                 responsable: IMD -->
            <td><?php echo htmlentities($alumnos->getAlumnos()->getSocio()->getNombreCompleto()) ?>
            </td>
            <td><?php echo htmlentities(($alumnos->getPreinscrito() ? 'PREINSCRITO' : 'INSCRITO')) ?>
            </td>
            <!-- Reingeniería PV | Responsable: YD => FE. - Fecha: 21-11-2013. - ID: 38. - Campo: Pestaña preinscritos.
        Descripción del cambio: Se modifico etiqueta Elimnar por Eliminar--> 
            <td><?php if($alumnos->getPreinscrito()): ?><a
                href="<?php echo url_for('control/deletePreinscripcion?id='.$alumnos->getId())?>"
                class="des-preinscribir"><span
                    class="ui-icon ui-icon-trash bx-title" title="Eliminar"></span> </a>
                <!-- Fecha: 21-11-2013. - Fin.--> <?php endif; ?>
            </td>
        </tr>
        <?php endforeach;?>
    </tbody>
</table>
<!-- Despliegue de exportacion excel en formato UTF, responsable: IMB, 28/10/2014 -->

<script type="text/javascript" charset="UTF-8">
var listaSocio;

 

$(document).ready(function() {
    listaSocio = $('#lista-socios').dataTable( {
    "bPaginate": false,
        "bLengthChange": false,
        "bFilter": true,
        "bSort": true,
        "bInfo": true,
        "bAutoWidth": true,
        "bJQueryUI": true,
        "sDom": 'T<"clear">R<"H"lfr>t<"F"ip>',
        "oTableTools": {
                       "sSwfPath": "/js/plugins/dataTables/DataTables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf",
                       "aButtons": [
                               {
                                       "sExtends": "csv",
                                       "sTitle"     : "Lista de Alumnos",
                                       "sButtonText": "Exportar XLS"
                               },
                               {
                                       "sExtends"   : "pdf",
                                       "sTitle"     : "Lista de Alumnos",
                                       "sButtonText": "Exportar PDF"
                               }
                       ]
               },
               "oLanguage": {
                "sUrl": "/js/plugins/dataTables/DataTables/extras/TableTools/lenguaje/dataTables.spanish.txt"
                }

    });
    

    $('.des-preinscribir').click(function(){
               if(confirm("¿Deseas eliminar al socio del curso?"))
               {
                       var link = $(this), pos = this;
                   $.ajax({
                           url : link.attr('href'),
                       beforeSend : function()
                           {
                               link.hide(500)
                       },
                       success: function(data) {
                              var aPos = listaSocio.fnGetPosition( pos.parentNode.parentNode );
                              listaSocio.fnDeleteRow( aPos );
                          }
                   });

                   return false;
               }
               return false;
           });

    var $wcm= $("#verDetalleSocio").dialog({
        modal: true,
        autoOpen: false,
        position: ['center','center'],
        title:  'Ver detalle',
        height: 550,
        width: 1100,
        buttons: {
            'Cerrar': function()
            {
                $(this).dialog('close');
            }
        }
    });

    function verDetalle(idSocio){
        var UrlVerDetalle = "<?php echo url_for('registro/verDetalle');?>";
        $("#verDetalleSocio").load(
            UrlVerDetalle,
            {
                id: idSocio
            }
        );
        $wcm.dialog('open');
    };

    $('.usuario').click(function(){
        var socio_id = $(this).attr('socio_id');
        verDetalle(socio_id);
    });

});
</script>
<?php else: ?>
<h4 style="text-align: center; font-size: 13px; padding: 20px 0;">Selecciona
    un grupo</h4>
<?php endif;?>
