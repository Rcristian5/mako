<script type="text/javascript">
  var icons = {
            header: "ui-icon-circle-arrow-e",
            headerSelected: "ui-icon-circle-arrow-s"
              };
  $(".accordion-cursos").accordion({
                           collapsible: true,
                           header: 'h3',
                           icons:icons
   });

      $('.no-seriados').click(function(){
        $('#ventana-calendar').dialog('open');
        $('#calendar').fullCalendar( 'changeView', 'agendaWeek' );
        $('#calendar').fullCalendar( 'removeEvents' );
        $('#calendar').fullCalendar( 'addEventSource', noSeriados[$(this).parent().parent().attr('id')]);
        $('#datos-pre-inscribir').hide();
      });

      $('.seriados').click(function(){
        $('#ventana-calendar').dialog('open');
        $('#calendar').fullCalendar( 'changeView', 'agendaWeek' );
        $('#calendar').fullCalendar( 'removeEvents' );
        $('#calendar').fullCalendar( 'addEventSource', seriados[$(this).parent().parent().attr('id')]);
        $('#datos-pre-inscribir').hide();
      });


   noSeriados = {<?php foreach ($cursosNoSeriados['horario'] as $indice => $horario): ?>
                  '<?php echo $indice ?>'  :  [
                                       <?php foreach ($horario as $dia): ?>
                                            {
                                              id         : '<?php echo $dia['id'] ?>',
                                              title      : '<?php echo $dia['nombre'] ?>',
                                              start      : '<?php echo substr($dia['fecha_inicio'], 0,10).' '.$dia['hora_inicio'] ?>',
                                              end        : '<?php echo substr($dia['fecha_inicio'], 0,10).' '.$dia['hora_fin'] ?>',
                                              allDay     : false,
                                              editable   : false,
                                              className  : 'fc-nuevo-curso  <?php echo substr($dia['fecha_fin'], 0,10) ?>',
                                           },
                                      <?php endforeach; ?>
                                      ],
        <?php endforeach; ?>
   };

   seriados = {<?php foreach ($cursosSeriados['horario'] as $indice => $horario): ?>
                '<?php echo $indice ?>' : [
                                       <?php foreach ($horario as $dia): ?>
                                            {
                                              id        : '<?php echo $dia['id'] ?>',
                                              title     : '<?php echo $dia['nombre'] ?>',
                                              start     : '<?php echo substr($dia['fecha_inicio'], 0,10).' '.$dia['hora_inicio'] ?>',
                                              end       : '<?php echo substr($dia['fecha_inicio'], 0,10).' '.$dia['hora_fin'] ?>',
                                              allDay    : false,
                                              editable  : false,
                                              className : 'fc-nuevo-curso <?php echo substr($dia['fecha_fin'], 0,10) ?>',
                                            },
                                      <?php endforeach; ?>
                                      ],
        <?php endforeach; ?>
   };

</script>

<?php if((count($cursosNoSeriados['cursos'])>0) || (count($cursosSeriados['cursos'])>0)): ?>
<div class="accordion-cursos">
    <?php foreach ($cursosNoSeriados['cursos'] as $curso): ?>
    <h3>
        <a href="#"><?php echo $curso['nombre'] ?> </a>
    </h3>
    <div class="info" id="<?php echo $curso['id'] ?>">
        <p>
            <span>Clave:</span>
            <?php echo $curso['clave'] ?>
        </p>
        <p>
            <span>Duraci&oacute;n:</span>
            <?php echo $curso['duracion'] ?>
            Hrs.
        </p>
        <p>
            <span>Descripción del curso:</span>
            <?php echo $curso['descripcion']?>
        </p>

        <p class="botones_finales" style="text-align: right">
            <span class="ui-state-highlight seriado">Este curso no tiene
                seriaci&oacute;n</span> <input type="button"
                value="Mostrar calendario" class="no-seriados" />
        </p>
    </div>
    <?php endforeach; ?>

    <?php foreach ($cursosSeriados['cursos'] as $curso): ?>
    <h3>
        <a href="#"><?php echo $curso['nombre'] ?> </a>
    </h3>
    <div class="info" id="<?php echo $curso['id'] ?>">
        <p>
            <span>Clave:</span>
            <?php echo $curso['clave'] ?>
        </p>
        <p>
            <span>Duraci&oacute;n:</span>
            <?php echo $curso['duracion'] ?>
            Hrs.
        </p>
        <p>
            <span>Descripción del curso:</span>
            <?php echo $curso['descripcion']?>
        </p>

        <p class="botones_finales" style="text-align: right">
            <span class="ui-state-highlight seriado">Este curso tiene
                seriaci&oacute;n con <?php echo $curso['nombre_seriado']?>
            </span>
            <?php if(isset ($terminados[$curso['id_seriado']])): ?>
            <input type="button" value="Mostrar calendario" class="seriados" />
            <?php else: ?>

            <span class="ui-state-error seriado">Debe cursar primero <?php echo $curso['nombre_seriado']?>
            </span>
            <?php endif; ?>
        </p>
    </div>
    <?php endforeach; ?>
</div>

<?php else: ?>
<div class="ui-state-error no-cursos">Por el momento no hay cursos para
    esta categor&iacute;a</div>
<?php endif; ?>