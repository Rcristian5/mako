<div id="list-fechas">

    <p class="fecha-lista">
        <span><label for="anio-lista">Año</label>
        </span> <span> <select id="anio-lista">
                <?php foreach ($fechas['anios'] as $anio): ?>
                <option value="<?php echo $anio['anio'] ?>"
                <?php echo ($anio['default'] ? 'selected="selected"':'') ?>>
                    <?php echo $anio['anio'] ?>
                </option>
                <?php endforeach;?>
        </select>
        </span>
    </p>

    <p class="fecha-lista">
        <span><label for="mes-lista">Mes</label>
        </span> <span> <select id="mes-lista">
                <?php foreach ($fechas['meses'][$fechas['default']] as $mes): ?>
                <option value="<?php echo $mes['numero'] ?>"
                <?php echo ($mes['default'] ? 'selected="selected"':'') ?>><?php echo $mes['mes'] ?></option>
                <?php endforeach;?>
        </select

        </span>
    </p>

<?php /*
    <p class="facilitador-lista">
        <span style="width: 70px;"><label for="facilitador-lista">Facilitador</label>
        </span> <span> <select id="facilitadorId"name="facilitadorId">
                <option value="" <?php ($facilitadorId==null)?'selected="selected"':"" ?> >TODOS</option>
                <?php  foreach ($facilitadores as $facilitador): ?>
                <option <?php echo($facilitador["id"]== $facilitadorId)?'selected="selected"':"" ?> value="<?php echo  $facilitador["id"]; ?>"><?php echo  $facilitador["nombre"]; ?></option>
                <?php endforeach;?>
        </select

        </span>
    </p>

    <p class="categoria-lista">
        <span style="width: 10px;"><label for="categoria-lista">Categor&iacute;a</label>
                </span> <span><select name="categoriaId" id="categoriaId">
                <option id="" value=""    <?php ($categoriaId==null)?'selected="selected"':"" ?> >TODAS</option>
                <?php $padreaux=""; foreach ($categorias as $padre=>$categoria): ?>
                    <optgroup label="<?php echo $padre; ?>">
                        <?php foreach($categoria as $dato): ?>
                            <option  <?php echo($dato["id"]== $categoriaId)?'selected="selected"':"" ?> value="<?php echo $dato["id"]; ?>"><?php echo $dato["nombre"]; ?></option>
                        <?php endforeach; ?>
                    </optgroup>
                <?php endforeach;?>
                </select>


        </span>
    </p>
 */?>

    <br />
    <p class="fecha-lista">
        <input type="button" value="Mostrar grupos" id="show-day" />
    </p>
</div>

<script type="text/javascript">
var meses = new Object();
<?php foreach ($fechas['meses'] as $key => $meses): ?>
meses["<?php echo $key ?>"] =  [ <?php foreach ($meses as $mes) :?>{"numero" : "<?php echo $mes['numero'] ?>", "mes" : "<?php echo $mes['mes']?>" }, <?php endforeach;?> ];
<?php endforeach;?>;
$(function() {
    $('#anio-lista').change(function(){
        var option = "";
        for(var i = 0; i <= meses[$(this).val()].length -1; i++)
        {
            option += '<option value="'+meses[$(this).val()][i].numero+'">'+meses[$(this).val()][i].mes+'</option>'
        }
        $('#mes-lista').html(option);
    });
    $('#show-day').click(function(){
        var categoria   = $("#categoriaId").attr("value");
        var facilitador = $("#facilitadorId").attr("value");

        location.href = '<?php echo url_for('control/listas') ?>?anio='+$('#anio-lista').val()+'&mes='+$('#mes-lista').val() + "&categoria=" + categoria + "&facilitador=" + facilitador;
    });




});
</script>
