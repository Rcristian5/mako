<?php use_stylesheet('control_escolar/control_escolar.css') ?>
<?php use_stylesheet('control_escolar/listas.css') ?>
<?php use_stylesheet('datatables.css') ?>
<?php //use_stylesheet('TableTools.css') ?>
<?php use_stylesheet('jquery.windows.css') ?>
<?php use_javascript('jquery.ui.js') ?>
<?php use_javascript('plugins/jquery.windows.js') ?>
<?php use_javascript('plugins/dataTables/DataTables/media/js/jquery.dataTables.js') ?>
<?php //use_javascript('plugins/dataTables/DataTables/extras/TableTools/media/js/TableTools.min.js') ?>
<?php //use_javascript('plugins/dataTables/ZeroClipboard.js') ?>
<?php use_javascript('plugins/date.js') ?>
<?php //use_javascript('plugins/dataTables/datatables.orden-fecha.js') ?>
<?php //use_javascript('plugins/dataTables/datatables.orden-decimal.js') ?>


<style type="text/css" media="screen">
//@import "/js/plugins/dataTables/DataTables/extras/TableTools/media/css/TableTools.css";
//@import "/js/plugins/dataTables/DataTables/media/css/demo_table_jui.css";
div.dataTables_wrapper { font-size: 12px; }
table.display thead th, table.display td { font-size: 12px; }
</style>

<!-- Ver detalle socio -->
<div id="verDetalleSocio"></div>
<!-- /Ver detalle socio -->

<div id="windows-wrapper">
    <div id="columnaIzquierda" window-title="Cursos" window-state="min">

        <?php
            include_partial('listaCursos', array(
                'cursos'        => $cursos,
                'fechas'        => $fechas,
                "facilitadores" => $facilitadores,
                "categorias"    => $categorias,
                "facilitadorId" => $facilitadorId,
                "categoriaId"   => $categoriaId
            ))
        ?>

    </div>

    <div id="columnaDerecha" window-title="Cursos registrados"
        window-state="max">
        <div id="detalle-grupo-alumno">
            <?php include_partial('listaAlumnosGrupo') ?>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function()
{
    $('.detalle-cursos').dataTable( {
    "bPaginate": false,
        "bLengthChange": false,
        "bFilter": false,
        "bSort": true,
        "bInfo": true,
        "bAutoWidth": false,
        "bJQueryUI": true,
        "sDom": 'T<"clear">R<"H"lfr>t<"F"ip>',
//        "oTableTools": {
//                       "sSwfPath": "/js/plugins/dataTables/DataTables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf",
//                       "aButtons": [
////                               {
////                                       "sExtends": "csv",
////                                       "sTitle"     : "Lista de Alumnos",
////                                       "sButtonText": "Exportar XLS"
////                               },
////                               {
////                                       "sExtends"   : "pdf",
////                                       "sTitle"     : "Lista de Alumnos",
////                                       "sButtonText": "Exportar PDF"
////                               }
//                       ]
//               },
               "oLanguage": {
                "sUrl": "/js/plugins/dataTables/DataTables/extras/TableTools/lenguaje/dataTables.spanish.txt"
                }

    });
    $('#windows-wrapper').windows();

});
</script>
