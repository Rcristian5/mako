<?php use_stylesheet('control_escolar/control_escolar.css') ?>
<?php use_stylesheet('jquery.windows.css') ?>
<?php use_javascript('jquery.ui.js') ?>
<?php use_javascript('plugins/jquery.windows.js') ?>
<?php use_javascript('control_escolar/registro_control.js') ?>


<div id="ventana-buscador-socios">
    <div id="buscador">
        <?php include_component('registro', 'buscaSocios') ?>
    </div>
</div>

<div id="windows-wrapper">
    <div id="registro" window-title="Datos del alumno" window-state="min">

        <h2>Datos del Socio</h2>

        <div id="resultados-socio" class="ui-corner-all ui-widget-content">
            <div id="resultados-busqueda">
                <?php if(isset($matricula )) : ?>
                <?php include_component('registro', 'buscaSocios',array('id_buscador'=>'','socio_id'=>$matricula)) ?>
                <?php endif ?>
            </div>
            <input type="button" id="buscar-nuevo" value="Buscar socio"
                style="float: right;" />
            <p>&nbsp;</p>
        </div>
        <?php include_partial('registro', array('form' => $registro, 'matricula'=>$matricula)) ?>

    </div>

    <div id="cursos" window-title="Detalle de cursos" window-state="max">
        <h2>Detalle de cursos</h2>
        <?php if(isset ($matricula)): ?>
        <p class="ui-state-error">Este socio no está resgristrado en control
            escolar.</p>
        <p>Para mostrar la lista de cursos disponibles es necesario llenar el
            formulario de la izquierda y guardar estos datos.</p>
        <?php endif;?>
    </div>

</div>

