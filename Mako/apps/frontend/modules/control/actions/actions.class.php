<?php

/**
 * control actions.
 *
 * @package    mako
 * @subpackage control
 * @author     Bixit SA de CV
 * @version    SVN: $Id: actions.class.php,v 1.19 2011-11-04 18:48:37 david Exp $
 */
class controlActions extends sfActions
{
    public function executeIndex(sfWebRequest $request)
    {
        $this->expedientes = ExpedientePeer::doSelect(new Criteria());
    }
    /**
     * Modulo para mostrar las listas de alumnos en los cursos calendarizados por centro
     * @param sfWebRequest $request
     */
    public function executeListas(sfWebRequest $request)
    {
        //$facilitador         = ($request->hasParameter("facilitador"))   ?((trim($request->getParameter("facilitador"))  !="")?$request->getParameter("facilitador") :null):null;
        //$categoria           = ($request->hasParameter("categoria"))     ?((trim($request->getParameter("categoria"))    !="")?$request->getParameter("categoria")   :null):null;
        $this->fechas        = HorarioPeer::fehasLista($request->getParameter('anio'), $request->getParameter('mes'));
        $this->cursos        = HorarioPeer::cursosPeriodo($request->getParameter('anio'), $request->getParameter('mes'),$facilitador, $categoria);
        $this->facilitadores = UsuarioPeer::getFacilitadores();
        $tmpcategorias       = CategoriaCursoPeer::listaCategoria();
        //$this->facilitadorId = $facilitador;
        //$this->categoriaId   = $categoria;

        $vtcategorias        = array();
        $padres              = array();

        foreach ($tmpcategorias as $key => $categoria) {
            if($categoria->getProfundidad() >1){

                $vtcategorias[$padres[$categoria->getPadre()]][] = array(
                    "id"     => $categoria->getId(),
                    "nombre" => $categoria->getNombre()
                );
            }
            else{
                $padres[$categoria->getId()] = $categoria->getNombre();

            }
        }

        $this->categorias = $vtcategorias;
    }
    /**
     * Accion que regresa el partial de la lista del grupo
     * @param sfWebRequest $request
     */
    public function executeLista_grupo(sfWebRequest $request)
    {
        $this->forward404unless($request->isXmlHttpRequest());
        $this->forward404Unless($grupo = GrupoPeer::retrieveByPk($request->getParameter('id')), sprintf('El grupo no existe(%s).', $request->getParameter('id')));


        return $this->renderPartial('listaAlumnosGrupo', array('grupo'=>$grupo));
    }

    public function executeCursosDisponibles(sfWebRequest $request)
    {
        $this->forward404unless($request->isXmlHttpRequest());
        $socio = SocioPeer::retrieveByPK($request->getParameter('id'));
        $arreglo = GrupoPeer::cursosDisponibles($request->getParameter('id'));
        $cursos = GrupoPeer::listaCursosSocio($request->getParameter('id'));
        return $this->renderPartial('cursosDisponibles', array('socio'        => $socio,
                'categorias'   => $arreglo['categorias'],
                'cursos'       => $arreglo['cursos'],
                'horarios'     => $arreglo['horario'],
                'finalizados'  => $cursos['finalizados'],
                'actuales'     => $cursos['actuales'],
                'preinscritos' => $cursos['preinscritos']
        ));
    }
    /**
     * Accion que guarda el abandono de un socio a un curso
     * @param sfWebRequest $request
     */
    public function executeAutorizar_abandono(sfWebRequest $request)
    {
        $this->forward404unless($request->isXmlHttpRequest());
        $respuesta = array('auth' => 0);
        if(UsuarioPeer::autorizarAbandono($request->getParameter('usuario'), $request->getParameter('clave')) != null)
        {
            chdir(sfConfig::get('sf_root_dir'));
            $task = new makoRecursarTask($this->dispatcher, new sfFormatter());
            $arguments = array();
            $options = array('grupo-id' => $request->getParameter('grupo'), 'usuario' => $request->getParameter('socio'));
            $task->run($arguments, $options);

            // Buscamos el grupo y actualizamos el cupo
            $grupo = GrupoPeer::retrieveByPK($request->getParameter('grupo'));
            if($grupo->getCupoAlcanzado())
            {
                $grupo->setCupoAlcanzado(false);
            }
            $grupo->setCupoActual($grupo->getCupoActual()-1);
            $grupo->save();
            // Buscamos el registro en grupo alumnos y lo borramos
            $c2 = new Criteria();
            $c2->add(SocioPeer::USUARIO,$request->getParameter('socio'));
            $socio = SocioPeer::doSelectOne($c2);

            $criteria = new Criteria();
            $criteria->add(GrupoAlumnosPeer::GRUPO_ID, $request->getParameter('grupo'));
            $criteria->add(GrupoAlumnosPeer::ALUMNO_ID, $socio->getId());
            $grupoAlumnos = GrupoAlumnosPeer::doSelectOne($criteria);
            if($grupoAlumnos)
            {
                $grupoAlumnos->delete();
            }
            else
            {
                error_log('NO SE ENCONTRO EL REGISTRO DEL SOCIO '.$request->getParameter('socio'). ' GRUPO'. $request->getParameter('grupo'));
            }

            $respuesta['auth'] = 1;
        }

        return $this->renderText(json_encode($respuesta));
    }


    /**
     * Accion que condona la deuda de un curso
     * @param sfWebRequest $request
     */
    public function executeCondonar_deuda(sfWebRequest $request)
    {
        $this->forward404unless($request->isXmlHttpRequest());
        $respuesta = array('auth' => 0);
        if(UsuarioPeer::autorizarAbandono($request->getParameter('usuario'), $request->getParameter('clave')) != null)
        {
            $grupo = GrupoPeer::retrieveByPK($request->getParameter('grupo'));
            // Si el usuario se identifica correctamente se procede a cancelar los pagos
            $critetia= new Criteria();
            $critetia->add(AlumnoPagosPeer::SOCIO_ID,$request->getParameter('socio'));
            $critetia->add(AlumnoPagosPeer::CURSO_ID,$grupo->getCursoId());
            $critetia->add(AlumnoPagosPeer::GRUPO_ID,$grupo->getId());
            $alumnoPago = AlumnoPagosPeer::doSelectOne($critetia);
            if($alumnoPago != null)
            {
                //Obtenemos el orden_id de este pago
                $orden = $alumnoPago->getOrden();
                $subProductoDe = $alumnoPago->getSubProductoDe();
                if($orden != null)
                {
                    //Obtenemos los detalle_orden_id relacionados con esta orden
                    $detalleOrdenes = $orden->getDetalleOrdens();
                    foreach ($detalleOrdenes as $detalleOrden)
                    {
                        foreach($detalleOrden->getSocioPagoss() as $socioPago)
                        {
                            $ordenBaseId = $socioPago->getOrdenBaseId();
                            $criteriaN = new Criteria();
                            $criteriaN->add(SocioPagosPeer::ORDEN_BASE_ID,$ordenBaseId);
                            $criteriaN->add(SocioPagosPeer::SUB_PRODUCTO_DE,$subProductoDe);
                            $aSocioPagos = SocioPagosPeer::doSelect($criteriaN);
                            foreach($aSocioPagos as $sp)
                            {
                                if($sp->getPagado() == false )
                                {
                                    error_log("\tID: ".$sp->getId()." cancelado: ".($sp->getCancelado()?'SI':'NO')." DO ".$sp->getDetalleOrdenId()." SID ".$sp->getSocioId()." BASE ORDEN ".$sp->getOrdenBaseId());
                                    $sp->setCancelado(true);
                                    $sp->save();
                                }
                            }

                        }
                    }

                }
            }
            $respuesta['auth'] = 1;
        }

        return $this->renderText(json_encode($respuesta));
    }


    public function executePreinscripcion(sfWebRequest $request)
    {
        $this->forward404unless($request->isXmlHttpRequest());

        $grupo = $request->getParameter('grupo');
        $socio = $request->getParameter('socio');

        $arreglo = GrupoAlumnosPeer::preincribe($socio, $grupo);

        $this->getResponse()->setHttpHeader('Content-type','text/json');

        return $this->renderText(json_encode($arreglo));
    }

    public function executeDeletePreinscripcion(sfWebRequest $request)
    {
        $this->forward404unless($request->isXmlHttpRequest());

        $grupoAlumnos = GrupoAlumnosPeer::retrieveByPK($request->getParameter('id'));
        $nuevoGrupoAlumnos = $grupoAlumnos;
        if($grupoAlumnos->getGrupo()->getCupoAlcanzado())
        {
            $grupoAlumnos->getGrupo()->setCupoAlcanzado(false);
        }
        $grupoAlumnos->getGrupo()->setCupoActual($grupoAlumnos->getGrupo()->getCupoActual()-1);
        $grupoAlumnos->getGrupo()->save();
        $grupoAlumnos->delete();

        $this->dispatcher->notify(new sfEvent($this, 'alumno.preinscripcion.cancelada', array( 'grupo_alumnos' => $nuevoGrupoAlumnos )));

        $this->getResponse()->setHttpHeader('Content-type','text/json');

        return $this->renderText(json_encode(array()));
    }
    public function executeLista_alumnos(sfWebRequest $request)
    {
        $this->forward404unless($request->isXmlHttpRequest());

        return $this->renderPartial('listaAlumnos', array('grupo' => GrupoAlumnosPeer::listaGrupoAlumnos($request->getParameter('grupo')) ));
    }

    public function executeRegistro(sfWebRequest $request)
    {
        $this->getUser()->getAttributeHolder()->remove('socio_id');
        if(ExpedientePeer::tieneExpediente($request->getParameter('id')))
        {
            $this->getUser()->setAttribute('socio_id', $request->getParameter('id'));
            $this->redirect('control/inscripcion');
        }
        $this->registro = new ExpedienteForm();
        $this->matricula = $request->getParameter('id');
    }

    public function executeInscripcion(sfWebRequest $request)
    {
        $id = ExpedientePeer::obtenExpediente($this->getUser()->getAttribute('socio_id'));
        $this->forward404Unless($expediente = ExpedientePeer::retrieveByPk($id), 'El expediente no existe');
        $this->registro = new ExpedienteForm($expediente);
        $this->matricula = $this->getUser()->getAttribute('socio_id');
        $this->entregado = ExpedientePeer::obtenEntregados($expediente->getId());
        $this->categorias = CategoriaCursoPeer::listaCategoria();
        $this->tomados = GrupoAlumnosPeer::cursosTomados($this->getUser()->getAttribute('socio_id'));

    }

    public function executeCategoria(sfWebRequest $request)
    {
        $this->cursosNoSeriados = CursoPeer::listaCursosNoSeriados($request->getParameter('id'), $this->getUser()->getAttribute('socio_id'));
        $this->cursosSeriados = CursoPeer::listaCursosSeriados($request->getParameter('id'), $this->getUser()->getAttribute('socio_id'));
        $this->terminados = CursosFinalizadosPeer::listaFinalizados($this->getUser()->getAttribute('socio_id'));

    }

    public function executeInscribir(sfWebRequest $request)
    {
        $this->forward404Unless($grupo = GrupoPeer::retrieveByPk($request->getParameter('id')), sprintf('El curso no existe(%s).', $request->getParameter('id')));
        $datos = array('grupo'=>$grupo, 'socio' => $this->getUser()->getAttribute('socio_id'));
        GrupoAlumnosPeer::preincribe($datos);
        ListaAsistenciaPeer::crearListas($grupo,$this->getUser()->getAttribute('socio_id'),$this->getUser()->getAttribute('usuario')->getId());
        $this->redirect('control/inscripcion');
    }

    public function executeCancelar(sfWebRequest $request)
    {
        $this->forward404Unless($grupoAlumno = GrupoAlumnosPeer::retrieveByPk($request->getParameter('id')), sprintf('La relacion  no existe(%s).', $request->getParameter('id')));
        GrupoAlumnosPeer::cancelaPreinscripcion($grupoAlumno->getGrupoId());
        $this->dispatcher->notify(new sfEvent($this, 'alumno.preinscripcion.cancelada', array( 'grupo_alumnos' => $grupoAlumno )));
        $grupoAlumno->delete();
        $this->redirect('control/inscripcion');
    }

    public function executeCreate(sfWebRequest $request)
    {
        $this->forward404Unless($request->isMethod(sfRequest::POST));

        $this->registro = new expedienteForm();

        $this->processForm($request, $this->registro);

        $this->setTemplate('registro');
    }

    public function executeUpdate(sfWebRequest $request)
    {
        $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
        $this->forward404Unless($expediente = ExpedientePeer::retrieveByPk($request->getParameter('id')), sprintf('Object expediente does not exist (%s).', $request->getParameter('id')));
        $this->registro = new ExpedienteForm($expediente);
        $this->matricula = $this->getUser()->getAttribute('socio_id');
        $this->entregado = ExpedientePeer::obtenEntregados($expediente->getId());
        $this->categorias = CategoriaCursoPeer::listaCategoria();

        $this->processForm($request, $this->registro);

        $this->setTemplate('inscripcion');
    }

    public function executeDelete(sfWebRequest $request)
    {
        $request->checkCSRFProtection();

        $this->forward404Unless($expediente = ExpedientePeer::retrieveByPk($request->getParameter('id')), sprintf('Object expediente does not exist (%s).', $request->getParameter('id')));
        $expediente->delete();

        $this->redirect('control/index');
    }

    protected function processForm(sfWebRequest $request, sfForm $form)
    {
        $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
        if ($form->isValid())
        {

            if($form->getObject()->isNew())
            {
                ExpedientePeer::guardaAlumno($form->getValue('alumno_id'));
            }
            $expediente = ExpedientePeer::guardaExpediente($form);
            $this->redirect('control/registro?id='.$expediente->getAlumnoId());
        }
        else
        {
            echo'dab';
        }
    }
    /**
     * Dado un rango de fechas regresa la listas de asistencias
     *
     * @WSMethod(webservice='MakoApi')
     * @param string $desde Fecha de inicio de la consulta
     * @param string $hasta Fecha de fin de la consulta
     * @return string Regresa un arreglo serializado del tipo $res[]=$listaAsistencia
     */
    public function executeGetListasAsistencia(sfWebRequest $request)
    {
        $configuration = ProjectConfiguration::getApplicationConfiguration( 'frontend', 'prod', true);
        $context = sfContext::createInstance($configuration);

        $centro_id = sfConfig::get('app_centro_actual_id');
        $desde = pg_escape_string($request->getParameter('desde'));
        $hasta = pg_escape_string($request->getParameter('hasta'));
        $listaAsistencia = ListaAsistenciaPeer::consolidaListaAsistencias($desde, $hasta, $centro_id);

        $this->result = serialize($listaAsistencia);

        if(count($reporte) == 0)
        {
            $me = new SoapFault('Server', "[GetListasAsistencia]: No existen registros para el rango de fechas consultadas $desde a $hasta en el centro_id $centro_id.");
            throw $me;
            return sfView::ERROR;
        }
        else
        {
            return sfView::SUCCESS;
        }
    }
}
