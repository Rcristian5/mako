<?php
/**
 * Factura de la venta
 */
?>
a1<?php echo sfConfig::get('app_nombre_empresa')."\n" ?>
<?php echo $orden->getCentro()->getCalle() ?> <?php echo $orden->getCentro()->getNumExt() ?> <?php echo $orden->getCentro()->getNumInt() ?>
 <?php echo $orden->getCentro()->getColonia() ?> <?php echo $orden->getCentro()->getMunicipio() ?>, <?php echo $orden->getCentro()->getEstado() ?>
, MEXICO, C.P. <?php echo $orden->getCentro()->getCp() ."\n"?>
ID TS:<?php echo $orden->getId() ."\n" ?>
F A C T U R A  O R I G I N A L
a0------------------------------------------------
Serie: <?php echo $ffis->getSerie() ."\n" ?>
Folio: <?php echo $folio."\n" ?>
Fecha: <?php echo $orden->getCreatedAt() ."\n" ?>
------------------------------------------------
No. de certificado:<?php echo (isset($certificado) ? $certificado :$ffis->getNumCertificado()) ."\n" ?>
A.  de aprobacion: <?php echo $ffis->getAnoAprobacion() ."\n" ?>
No. de aprobacion: <?php echo $ffis->getNoAprobacion() ."\n" ?>
------------------------------------------------
Expedido a:
<?php echo $orden->getRazonSocial() ."\n" ?>
RFC: <?php echo $orden->getRfc() ."\n" ?>
<?php echo $orden->getCalleFiscal() ?> <?php echo $orden->getNumExtFiscal() ?> <?php echo $orden->getNumIntFiscal()."\n" ?>
<?php echo $orden->getColoniaFiscal() ?> <?php echo $orden->getCpFiscal() ?> <?php echo $orden->getMunicipioFiscal()."\n" ?>
<?php echo $orden->getEstadoFiscal()."\n"?>
MEXICO
------------------------------------------------
      Producto     Cant.    Unidad      P/Unit
------------------------------------------------
<?php echo html_entity_decode($cadenaconceptos)."\n" ?>
<?php echo sprintf("%48s","Importe: $".sprintf("%9s",sprintf("%.2f",$importe)))."\n" ?>
<?php echo sprintf("%48s","IVA(".$tasa."%): $".sprintf("%9s",sprintf("%.2f",$iva)))."\n" ?>
<?php echo sprintf("%48s","TOTAL: $".sprintf("%9s",sprintf("%.2f",$total)))."\n" ?>
<?php echo $totalPalabras."\n" ?>
Forma de pago: UNA SOLA EXHIBICION
Metodo de pago: EFECTIVO
------------------------------------------------
<?php echo $orden->getCentro()->getRazonSocial() ."\n" ?>
<?php echo $orden->getCentro()->getRfc() ."\n" ?>
<?php echo $orden->getCentro()->getDireccion()."\n" ?>
PERSONA MORAL CON FINES NO LUCRATIVOS
---------------CADENA ORIGINAL------------------
<?php echo html_entity_decode($cadena)."\n" ?>
--------------------SELLO-----------------------
<?php echo $sello."\n" ?>
------------------------------------------------
a1Este documento es la impresion de
un Comprobante Fiscal Digital
------------------------------------------------
<?php echo ($caf_prods)? "VALE CAFETERIA\n" : "" ?>
a0<?php echo $caf_prods."\n" ?>
d2




