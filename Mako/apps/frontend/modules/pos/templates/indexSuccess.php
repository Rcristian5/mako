
<!-- Ejemplo de dos buscadores de socios con contextos diferentes en el mismo documento -->

<!-- Encerramos las llamadas al componente en divs con ids distintos en sus diferentes contextos -->

<div id="buscador-ctx1">
	<?php include_component('registro', 'buscaSocios',
			array(
					'socio_id'=>$socio_id,
					'qNombre'=>$qNombre,
					'qFolio'=>$qFolio,
					'qUsuario'=>$qUsuario
			))
			?>
</div>


<div id="buscador-ctx2">
	<?php include_component('registro', 'buscaSocios',
			array(
					'socio_id'=>$socio_id,
					'qNombre'=>$qNombre,
					'qFolio'=>$qFolio,
					'qUsuario'=>$qUsuario
			))
			?>
</div>

<script>
$(document).ready(function(){

	//Para el buscador 1 -------------------------------------------------------------
	//Cuando dan click al botón buscar del buscador de contexto 1
	$("#buscador-ctx1").bind('socios.buscador.buscando',function(){
		console.debug('Buscando en buscador 1...');
		
	});


	//Cuando termina de buscar el buscador de contexto 1
	$("#buscador-ctx1").bind('socios.buscador.encontrados',function(){
		console.debug('Terminando en b1...');	
	});


	//Cuando dan click para seleccionar a un socio en el buscador 1
	$("#buscador-ctx1").bind('socio.seleccionado',function(ev, socio_id, socio_nombre){
		console.debug('Seleccionado en el B1: '+socio_id+' Nombre: '+socio_nombre);
	});


	//Para el buscador 2-------------------------------------------------------------
	//Cuando dan click al botón buscar del buscador de contexto 2
	$("#buscador-ctx2").bind('socios.buscador.buscando',function(){
		console.debug('Buscando en buscador 2...');
		
	});


	//Cuando termina de buscar el buscador de contexto 2
	$("#buscador-ctx2").bind('socios.buscador.encontrados',function(){
		console.debug('Terminando en b2...');	
	});


	//Cuando dan click para seleccionar a un socio en el buscador 2
	$("#buscador-ctx2").bind('socio.seleccionado',function(ev, socio_id, socio_nombre){
		console.debug('Seleccionado en el B2: '+socio_id+' Nombre: '+socio_nombre);
	});
	
	
});

</script>

