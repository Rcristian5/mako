<?php
/**
 * Ticket de venta para impresoras chafas de 40 caracteres por linea
 * @version: $Id: _ticketChVenta.php,v 1.2 2010/11/25 05:03:58 eorozco Exp $
 */
?>
<?php echo sfConfig::get('app_nombre_empresa')."\n" ?>
Folio: <?php echo sprintf("%08d",$orden->getFolio()) ?> - <?php echo $orden->getCentro()->getNombre()."\n" ?>
Fecha: <?php echo $orden->getCreatedAt()."\n" ?>
ID TS: <?php echo $orden->getId()."\n" ?>
Usuario: <?php echo $orden->getSocio()->getUsuario()."\n" ?>
Cred: <?php echo $orden->getSocio()->getFolio()."\n" ?>
----------------------------------------
    Producto              Cant. Subtotal
----------------------------------------
<?php echo $cadp."\n" ?>
<?php echo sprintf("%40s","TOTAL: $".sprintf("%8s",$total))."\n" ?>
<?php echo sprintf("%40s","Importe: $".sprintf("%8s",$importe))."\n" ?>
<?php echo sprintf("%40s","IVA: $".sprintf("%8s",$iva))."\n" ?>

<?php echo $totalPalabras."\n" ?>
----------------------------------------
<?php echo $orden->getCentro()->getRazonSocial()."\n" ?>
R.F.C. <?php echo $orden->getCentro()->getRfc()."\n" ?>
<?php echo $orden->getCentro()->getDireccion()."\n" ?>
----------------------------------------
Conserve su ticket para aclaraciones.
Gracias por su compra.
----------------------------------------
<?php echo ($caf_prods)? "VALE CAFETERIA\n" : "" ?>
<?php echo $caf_prods."\n" ?>
V
p ?
