<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<?php use_stylesheet('start/jquery.ui.custom.css') ?>
<?php use_javascript('jquery.ui.js') ?>
<?php use_javascript('debug.js') ?>
<form>

	<input tabindex="1" type="text" class="ui-widget-content ui-corner-all"
		name="auto_promotor_ref_id" id="auto_promotor_ref_id" size="50"
		nouppercase="1" /> <input tabindex="1" type="text"
		class="ui-widget-content ui-corner-all" name="socio_promotor_ref_id"
		id="socio_promotor_ref_id" size="50" nouppercase="1" />

</form>

<script>
jQuery(document).ready(function() {
	var datosRes = [];
	/**
	 * Formatea la persiana del campo autocomplete
	 * @param row Son los datos en forma de array, este array se define en la función parse
	 * @return string
	 */
	function formatItem(row) 
	{
		debug('pasa 2');
		return row[0] + " <span >(<strong>Categoría: " + row[1]	+ "</strong>)</span>";
	}

	/**
	 * Formatea el resultado de coincidencias en la persiana, indicando las subcadenas coincidentes 
	 * @param row Son los datos en forma de array, este array se define en la función parse
	 * @return string
	 */
	function formatResult(row) 
	{
		debug('pasa 3');
		return row[0].replace(/(<.+?>)/gi, '');
	}

	/**
	 * Construye el arreglo de datos del autocompletador
	 * @param data
	 * @return
	 */
	function parser(data) {
		
		var parsed = [];
		for (key in data) {
			parsed[parsed.length] = {
					data: [ data[key],
							key 
					        ],
					        value: data[key],
					        result: data[key]
			};
		}
		datosRes= parsed;
		debug('pasa 1 '+parsed.length);
		return parsed;
	}


	/**
	 * Procesa el registro seleccionado 
	 * @param event
	 * @param data
	 * @param formatted
	 * @return
	 */
	function seleccionado(event, data, formatted) 
	{
		
		$("#socio_promotor_ref_id").val(data[1]);
		
		debug('SELECIONADO pasa 4. '+ data[0]);
	}
	
	/**
	 * Auto complete buscador de productos
	 */
	$("#auto_promotor_ref_id").autocomplete(
			'consultaPromotores',
			{
				extraParams: {
					'limit': 20
					},
				dataType: 'json',
				min: 2,
				parse: parser,
				autoFill: true,
				mustMatch: true,
				formatItem: formatItem,
				formatResult: formatResult
			}).result(seleccionado);
	
});	


</script>
