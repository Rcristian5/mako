<script>

url_Datos = '<?php echo url_for('registro/buscaDatosDireccion') ?>';    

</script>

<!--
/*
 * Componentes Bixit
 *
 * Copyright (c) 2009-2011 Bixit SA de CV
 * Bajo Licencia dual MIT (http://www.bixit.mx/legal/MIT-LICENSE.txt)
 * y GPL (http://www.bixit.mx/legal/GPL-LICENSE.txt).
 *
 */
-->
<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<?php use_javascript('jquery.ui.js') ?>
<?php use_javascript('JSON.js') ?>
<?php use_stylesheet('../sfFormExtraPlugin/css/jquery.autocompleter.css') ?>
<?php use_javascript('plugins/jquery.dataTables.js') ?>
<?php use_stylesheet('datatables.css') ?>

<?php use_javascript('plugins/jquery.dataTables.js') ?>
<?php use_javascript('plugins/jquery.autocompleter.js') ?>
<?php use_javascript('plugins/jquery.sprintf.js') ?>
<?php use_javascript('plugins/jquery.jeditable.js') ?>
<?php use_javascript('plugins/jquery.comboMakoCascade.js') ?>
<?php use_javascript('pos/combosDireccion.js') ?>
<?php use_javascript('pos/buscarProductos.js') ?>
<?php use_javascript('pos/buscarSocios.js') ?>
<?php use_javascript('pos/tablaProductos.js') ?>
<?php use_javascript('pos/datosFiscales.js') ?>
<?php use_javascript('pos/venta.js') ?>
<?php use_javascript('pos/cupones.js') ?>
<?php use_javascript('pos/cobranza.js') ?>
<?php use_javascript('pos/cursos.js') ?>

<?php use_javascript('plugins/date.js') ?>
<?php use_javascript('plugins/dataTables/datatables.orden-fecha.js') ?>
<?php use_javascript('plugins/dataTables/datatables.orden-decimal.js') ?>

<?php use_javascript('debug.js') ?>

<!-- Url de la acción ajax que toma el script buscaProductos.js -->
<input
    type="hidden" name="url_accion" id="url_accion"
    value="<?php echo url_for('pos/arregloProductosPos') ?>" />

<!-- Esta variable está definida en la acción y fué pasada como parámetro get -->
<input
    type="hidden" name="socio_id" id="socio_id" />

<!-- Url de la accion de datos fiscales Se usa en las funciones de datosFiscales.js -->
<input
    type="hidden" name="urlGetFiscales" id="urlGetFiscales"
    value="<?php echo url_for('pos/getFiscales') ?>" />

<!-- Url de la accion de guardar venta se usa en el las funciones de venta.js -->
<input
    type="hidden" name="urlRealizarVenta" id="urlRealizarVenta"
    value="<?php echo url_for('pos/realizarVenta') ?>" />

<!-- Url de la accion de buscar cupones por redimir del socio -->
<input
    type="hidden" name="urlGetCuponesPorRedimir"
    id="urlGetCuponesPorRedimir"
    value="<?php echo url_for('pos/cuponesPorRedimir') ?>" />

<!-- Url de la accion de cupones redimidos del socio -->
<input
    type="hidden" name="urlGetCuponesRedimidos" id="urlGetCuponesRedimidos"
    value="<?php echo url_for('pos/cuponesRedimidos') ?>" />

<!-- Url de la accion de por pagar del socio -->
<input
    type="hidden" name="urlGetPagosPendientes" id="urlGetPagosPendientes"
    value="<?php echo url_for('pos/pagosPendientes') ?>" />

<!-- Url de la accion para obtener un subproducto opcion de cobranza -->
<input
    type="hidden" name="urlGetSubProducto" id="urlGetSubProducto"
    value="<?php echo url_for('pos/subProducto') ?>" />

<!-- Url de la accion para otorgar cupones manualmente -->
<input
    type="hidden" name="urlGetCuponesManual" id="urlGetCuponesManual"
    value="<?php echo url_for('admin_cupones/otorgarCuponManual') ?>" />

<!-- Url de la accion para acuerdo de pago -->
<input
    type="hidden" name="urlGetAcuerdoPago" id="urlGetAcuerdoPago"
    value="<?php echo url_for('pos/acuerdoPago') ?>" />

<!-- Url de la accion para cursos del socio -->

<input
    type="hidden" name="urlCursosSocio" id="urlCursosSocio"
    value="<?php echo url_for('control/cursosDisponibles') ?>" />

<!-- Url de la accion para ver detalle del socio -->

<input
    type="hidden" name="urlVerDetalle" id="urlVerDetalle"
    value="<?php echo url_for('registro/verDetalle') ?>" />

<fieldset style="display: none" id="otorgar-cupon">
    <strong>Código: </strong> <input name="codigo-cupon" type="text"
        id="codigo-cupon" tabindex="1" maxlength="16" size="16"
        filtro="numerico"> <br /> <strong>Validador:</strong> <input
        name="validador" type="text" id="validador" size="4" maxlength="4"
        value="" tabindex="1" filtro="numerico">
</fieldset>

<?php use_stylesheet('./pos/newSuccess.css')?>

<!-- Ver detalle socio -->
<div class="ui-widget ui-corner-all sombra">
    <div id="verDetalleSocio" style="display: none"></div>
</div>
<!-- /Ver detalle socio -->

<div class="ui-widget-content ui-corner-all sombra clearfix"
    style="padding: 5px;" id="contenedor-general">
    <div class="ui-widget-header ui-corner-top tssss" style="padding: 5px;">
        Punto de venta</div>

    <div style="padding-left: 5px; padding-top: 5px; display: block;"
        class="ui-widget-content ui-corner-bottom">
        <!-- Reingeniería PV | Responsable: YD => FE. - Fecha: 21-11-2013. - ID: 13. - Campo: Producto.
        Descripción del cambio: Se agrego etiqueta 'Seleccionar producto'-->
        <span class="vp-titulo ui-state-default ui-corner-right">Seleccionar
            producto:</span>
        <div>
            <input tabindex="1" type="text"
                class="ui-widget-content ui-corner-all" name="autocomp_productos"
                id="autocomp_productos" size="50" nouppercase="1" />
        </div>
        <!-- Fecha: 21-11-2013. - Fin-->
    </div>

    <div class="izq">

        <!-- Comienza bloque de preview de datos -->
        <div class="ui-widget-content ui-corner-all">

            <div id="vp-socio-seleccionado"
                style="display: none; padding-left: 5px; padding-top: 5px;"
                class="vp-titulo">
                <span class="vp-titulo ui-state-default ui-corner-right">Socio:</span>
                <span id="nombre" class="vp-socio-seleccionado"></span> <span
                    class="nombre-pago ui-state-highlight"> <a
                    href="javascript:void(0);" id="liga-otorgar-cupon">Redimir Cupón</a>
                </span>

                <hr />
            </div>

            <div id="vp-socio-fiscales" style="display: none; padding-left: 5px;">
                <span class="vp-titulo ui-state-default ui-corner-right">Datos
                    Fiscales:</span>
                <div style="margin-top: 4px;">
                    <strong>Razón social:</strong> <span id="vp-razon_social"
                        class="vp-socio-fiscales"></span> <strong>RFC:</strong> <span
                        id="vp-rfc" class="vp-socio-fiscales"></span> <br /> <strong>Domicilio
                        fiscal </strong> <strong>Calle:</strong> <span
                        id="vp-calle_fiscal" class="vp-socio-fiscales"></span> <strong>Número
                        ext.:</strong> <span id="vp-num_ext_fiscal"
                        class="vp-socio-fiscales"></span> <strong>Número int.:</strong> <span
                        id="vp-num_int_fiscal" class="vp-socio-fiscales"></span> <strong>Colonia:</strong>
                    <span id="vp-colonia_fiscal" class="vp-socio-fiscales"></span> <strong>CP:</strong>
                    <span id="vp-cp_fiscal" class="vp-socio-fiscales"></span> <strong>Municipio:</strong>
                    <span id="vp-municipio_fiscal" class="vp-socio-fiscales"></span>
                </div>
                <hr />
            </div>

            <div id="vista_previa_prod" style="display: none;">
                <div style="margin: 5px;">
                    <span class="vp-titulo ui-state-default ui-corner-right">Detalle
                        del producto:</span>
                    <div style="margin-top: 4px;">
                        <div style="font-style: italic">
                            <strong>Categoría</strong> <span id="categoria"></span> <strong>Precio
                                de lista</strong> $<span id="precio_lista"></span> <strong>Descuento:</strong>
                            <span id="descuento"></span>%
                        </div>

                        <strong>Descripción:</strong> <span id="descripcion"></span>
                    </div>


                    <div style="display: none;" id="producto-cobranza">
                        <span class="vp-titulo ui-state-default ui-corner-right">Opciones
                            de pago:<br />
                        </span> Este producto cuenta con más opciones de pago:<br />
                        <div id="lista-opciones-pago"></div>
                    </div>

                    <!-- Plantilla de opciones de pago -->
                    <span class="opcion-pago" id="plantilla-opcion-pago"
                        style="display: none; padding-left: 5px;"> <span
                        class="nombre-pago ui-state-highlight"> <a
                            href="javascript:void(0);" class="ligas-opciones-pago">Pago
                                diario</a>
                    </span> (<span class="numero-pagos">25 pagos</span>)
                    </span>
                    <!-- Fin plantilla opciones de pago -->

                </div>
            </div>
        </div>
        <!-- FIN bloque de preview de datos -->

        <!-- Comienza bloque de carrito de compras -->
        <div
            class="ui-widget-content ui-corner-all contenedor_carrito clearfix">


            <table id="carrito" width="100%">
                <thead>
                    <tr class="ui-corner-tl ui-corner-tr">
                        <th class="ui-widget-header">Código</th>
                        <th class="ui-widget-header">Categoría</th>
                        <th class="ui-widget-header">Producto</th>
                        <th class="ui-widget-header">Cantidad</th>
                        <th class="ui-widget-header">Precio lista($)</th>
                        <th class="ui-widget-header">Descuento(%)</th>
                        <th class="ui-widget-header">Sub total($)</th>
                        <th class="ui-widget-header centrable">X</th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="6" class="sumable ui-widget-header">Total: $</td>
                        <td id="total" class="sumable ui-widget-header" nowrap="nowrap"></td>
                    </tr>
                    <!--Reingeniería PV | Responsable: YD => FE. - Fecha: 23-11-2013. - ID: 17. - Campo: Cuadro del detalle.
                        Descripción del cambio: Se crea el botón limpiar carrito y se coloca una imagen.-->
                    <tr>
                        <td colspan="7"></td>
                        <td class="centrable">
                            <div id="puede_limpiar_venta" style="display: none;">
                                <button id="limpiar-venta" class="ui-button"
                                    title="Limpiar carrito">
                                    <img src="/css/limpiar-venta.png" width="36" height="36"
                                        border="0" />
                                </button>
                            </div>
                        </td>
                    </tr>
                    <!--Fecha: 23-11-2013. - Fin-->
                </tfoot>
            </table>

            <br /> <br />

            <div id="mensaje_cajero" style="float: right">
                <table>

                    <!--
                    /**
                    Fecha   :17 Feb 2014
                    Descripción: Se agrego La funcionalidad para regisstrar pagos con tarjeta de credito.
                    PArtimos de un combo para seleccionar un tipo de pago
                    y una caja de texto para solicitar el folio del baucher en caso de que el pago se haya
                    realizado con tarjeta de crédito.
                    Creado por: silvio.bravo@enova.mx
                    ***/
                 -->
                    <?php if($appUsarFormaPago==1): ?>
                    <tr>
                        <td>Forma de Pago:</td>
                        <td><select style="font-size: large;" id="forma_pago_id"
                            name="forma_pago_id" class="ui-widget-content ui-corner-all">
                                <!-- <option value="1" selected="selected">Efectivo</option>
                                <option value="2">Tarjeta Bancaria</option> -->
                                <?php  foreach ($formasPagoArray as $formapago): ?>

                                <option
                                <?= ($formapago->getId()==1)?'selected="selected"':""; ?>
                                    value="<?= $formapago->getId(); ?>">
                                    <?= $formapago->getNombre() ?>
                                </option>

                                <?php endforeach; ?>
                        </select>
                        </td>
                    </tr>
                    <?php else: ?>
                    <!-- /**
                            Si esta configurado para que no presente formas de pago no se presenaran y todos los pagos se realizaran
                            en efectivo.
                    */  -->
                    <input type="hidden" name="forma_pago_id" id="forma_pago_id"
                        value="1" />


                    <?php endIf; ?>


                    <tr>
                        <td id="label_a_pagar">A pagar:</td>
                        <td id="content_pay_value" nowrap="nowrap">$<span id="a_pagar"></span>
                        </td>
                    </tr>


                    <tr>
                        <td id="label_pago">Recibido:</td>
                        <td><input tabindex="1" type="text"
                            class="ui-widget-content ui-corner-all" name="pago" id="pago"
                            filtro="numerico" size="5" maxlength="8"
                            onkeyup="return cambio();" />
                        </td>
                    </tr>

                    <tr>
                        <td id="label_cambio">Cambio:</td>
                        <td nowrap="nowrap">$<span id="cambio"></span>
                        </td>
                    </tr>
                </table>
                <input type="hidden" id="num_aprobacion_tarjeta"
                    name="num_aprobacion_tarjeta" />
            </div>

        </div>
        <!-- FIN bloque de carrito de compras -->

        <!-- Comienza bloque de botones de control -->
        <div id="puede_realizar_venta" style="display: none;">
            <button id="realizar-venta"
                class="ui-button ui-state-default ui-corner-all">Realizar venta</button>
            <label class="labelChk">Requiere factura</label> <input
                name="req_factura" id="req_factura" type="checkbox" value="1" />
        </div>
        <!-- FIN bloque de botones de control -->


        <!-- Se cierra columna izquierda -->

    </div>

    <div>
        <!-- Comienza contenedor derecho -->

        <!-- Comienza bloque de buscador de socios -->
        <div class="ui-corner-all ui-widget-content contenedor-usuario"
            id="contenedor-usuario">
            <div id="ventana-buscador-socios">
                <div id="buscador">
                    <?php include_component('registro', 'buscaSocios') ?>
                </div>
            </div>
        </div>
        <!-- FIN bloque de buscador de socios -->


        <!-- Finaliza contenedor derecho -->
    </div>

</div>

<!-- Comienza bloque de Captura de Datos Fiscales -->

<fieldset style="display: none"
    id="fiscales">
    <legend>Datos Facturación</legend>

    <label>Nombre o Razón Social </label>
    <input name="factura[razon_social]"
           type="text"
           id="razon_social"
           maxlength="80"
           value=""
           tabindex="1"
           filtro="razon_social">
    <br /><br />

    <label class="lbFactura">RFC</label>

    <!-- TODO: Realizar validación de RFC -->

    <input name="factura[rfc]"
           type="text"
           id="rfc"
           size="13"
           maxlength="13"
           value=""
           tabindex="1"
           filtro="rfc">

    <br /><br />

    Dirección: <br />

    <?php
    /*
     *****************************************************
    *   Se agrego combos para el caso de la direccion   *
    * (estado, municipio, colonia                       *
            *   Reingenieria Mako                               *
            *   silvio.bravo@enova.mx                           *
            *   05-Marzo-2014                                   *
            *****************************************************

            Mako Ajustes Pruebas
            BP
            Se coloca a colonia como Input Text
            */
    ?>

    <label class="lbFactura">Estado</label> <select
        name="cmbEntidadFederativa" id="cmbEntidadFederativa" tabindex="1">
        <?php foreach($entidadesFederativas as $entidad): ?>
        <option value="<?= $entidad->getId(); ?>"><?= trim(strtoupper($entidad->getNombre())) ?></option>
        <?php endforeach; ?>
    </select> <input type="hidden" name="factura[estado]"
        id="estado_fiscal" tabindex="1"> <br /> <label class="lbFactura">
        Municipio</label> <select name="cmbMunicipio" id="cmbMunicipio"></select>
    <input name="factura[municipio]" type="hidden" id="municipio_fiscal"
        value="" tabindex="1" filtro="alfanumeric"> <br /> <label
        class="lbFactura"> Colonia</label><input name="factura[colonia]" type="text"
        maxlength="60" id="colonia_fiscal" value="" tabindex="1" filtro="alfanumeric"> <br />



    <!--Reingeniería PV | Responsable: YD => FE, LG. - Fecha: 20-11-2013. - ID: 58,60. - Campo: Calle, Num. Interior.
     Descripción del cambio: Se agrega atributo maxlength y filtro alfanumerico.-->
    <label class="lbFactura"> Calle</label> <input
        name="factura[calle_fiscal]" type="text" id="calle_fiscal" value=""
        maxlength="50" tabindex="1" filtro="alfanueric"> <br />
    <!--
        Responsable:ES
        Fecha: 22/10/2013
        Id:59
        Campo:Num. Exterior y Num. Interior se agrega la validación agregando el atributo "filtro"
        -->
    <label class="lbFactura"> Num. Exterior</label> <input
        name="factura[num_ext]" type="text" id="num_ext_fiscal" value=""
        maxlength="25" size="15" tabindex="1" filtro="alfanumeric"> <br /> <label
        class="lbFactura"> Num. Interior</label> <input
        name="factura[num_int]" type="text" id="num_int_fiscal" value=""
        maxlength="20" size="15" tabindex="1" filtro="alfanumeric">
    <!--Fecha: 20-11-2013. -->
    <br />

    <?php
    /******************************************
     *  @@ LGL
    * Reingeniería PV
    * Responsable: LG
    *   Fecha; 29-Oct-2013
    *   ID's : 61,62
    *   ID: 64 , Cancelado el campo es un SELECT y no permite modificar la información del mismo
    *   Campos: COLONIA y MUNICIPIO
    *   Descripción: Se realiza validación para solo permitir caracteres alfanumericos.
    *
    ******************************************/
    ?>



    <label class="lbFactura"> Código Postal </label> <input
        name="factura[cp]" type="text" id="cp_fiscal" size="10" maxlength="5"
        filtro="numerico" value="" tabindex="1">

</fieldset>
<!-- Fin bloque de Captura de Datos Fiscales -->

<!-- Comienza bloque de ventana de mensaje de venta completada -->
<div id="dialogo" style="display: none">
    Se ha realizado la venta. <br /> Tome el comprobante de la impresora
</div>
<!-- Termina bloque de ventana de mensaje de venta completada -->

<!-- Comienza bloque de pestañas emergentes-->

<div id="top_tabs"
    style="position: absolute; outline 0px none; top: 0px; left: auto; right: 0; z-index: 1000;">

    <div class="sombra ui-corner-bottom" id="panel-cupones"
        style="display: none; float: right; margin-left: 8px;">
        <div class="ui-tabs-panel ui-widget-content" id="lista-cupones"
            style="padding: 5px;"></div>
        <div class="ui-state-default ui-corner-bottom"
            style="padding: 5px; z-index: 1002; cursor: pointer;" align="center"
            id="handler-cupones">Cupones</div>
    </div>

    <div class="sombra ui-corner-bottom" id="panel-cobranza"
        style="display: none; float: right; margin-left: 8px;">

        <div class="ui-tabs-panel ui-widget-content " id="lista-cobros"
            style="padding: 5px;"></div>

        <div class="ui-state-default ui-corner-bottom"
            style="padding: 5px; z-index: 1002; cursor: pointer;" align="center"
            id="handler-cobranza">Pagos pendientes</div>
    </div>

    <div class="ui-widget-overlay" id="overlay-cursos"
        style="display: none; z-index: 1001; position: fixed;"></div>
    <div align="center" class="sombra ui-corner-bottom" id="panel-cursos"
        style="float: right; margin-left: 8px; padding: 0; border: 0; z-index: 1002; position: relative;">

        <div id="lista-cursos"
            style="background-color: white; padding-left: 15px; padding-right: 15px; overflow-y: auto;"
            class="sombra"></div>
        <img src="/imgs/ajax-loader.gif" id="ajax-loader"
            style="display: none;">
        <div class="ui-state-default ui-corner-bottom sombra"
            style="padding: 5px; z-index: 1002; cursor: pointer; -moz-box-shadow: 0px 0px 30px -8px #888;"
            align="center" id="handler-cursos">Cursos</div>
    </div>

</div>

<div id="cupon-plantilla" class="cupon-detalle"
    style="cursor: move; display: none;" cupon_id="" template="1">
    <p class="ui-state-default nombre-promocion"></p>
    <span>Código</span> <span class="cupon-codigo"></span><br /> <span>Descuento</span>
    <span class="cupon-descuento"></span><br /> <span>Vigente de:</span><span
        class="cupon-vigencia-de"></span> <br /> <span>Vigente a:</span><span
        class="cupon-vigencia-a"></span>
    <div style="height: 10px;">
        <span class="ui-icon ui-icon-scissors" style="float: left;"></span>
        <hr style="float: right; width: 90%;">
    </div>
</div>

<div id="cobranza-plantilla" class="cobranza-detalle"
    style="cursor: pointer; display: none;" proucto_id="" template="1">
    <p class="ui-state-default nombre-producto"></p>
    <span>Págese antes de:</span> <span class="fecha-pagar"></span> <br />
    <hr />
</div>
<span id="acuerdo-plantilla"
    style="display: none; z-index: 1000; margin-left: 5px;">Acuerdo de
    pago: <span id="chk-ap"
    class="check-acuerdo ui-icon ui-icon-circle-close"
    style="float: right;" /></span>
</span>



<?php
/*
 *
* ******************************************************************************************
* Generamos ventana emergente para capturar el numero de baucher en caso de que el pago *
* se realice a traves de tarjeta de credito                                             *
* la ventana se activa al momento de presionar el boton de realizar venta.                  *
* la logica de la activacion de la ventana se encuentra en js/pos/venta.js                  *
* @author silvio.bravo@enova.mx 18 Feb 2014                                             *
* @category Reingenieria Mako                                                               *
* @copyright ENOVA                                                                          *
* ******************************************************************************************
*/


?>
<div id="catch-baucher-window"
    style="display: none; z-index: 2000; margin-left: 5px;">
    <div id="">
        <table style="margin: 10px auto;">


            <tr>
                <td style="font-size: x-large" id="msg-no-aprob">Ingrese el No. Referencia:</td>
            </tr>
            <tr>
                <td><input type="text" id="baucherNumber" name="baucherNumber"
                    filtro="numerico" size="25" maxlength="25"
                    class="ui-widget-content ui-corner-all" style="font-size: x-large" />
                </td>
            </tr>
        </table>
    </div>
</div>


<?php
/*
 *
************************************************************************************
* Agregamos variables para cargar por default a un socio y asignarle un producto,   *
* en caso de que se requera esa funcionalidad                                       *
* @author silvio.bravo@enova.mx 24 Marzo 2014                                       *
* @category Reingenieria Mako                                                       *
* @copyright ENOVA                                                                  *
*************************************************************************************
*
*/
?>

<?php if(count($extraparams)>0): ?>

<input
    type="hidden" id="extraSocioId" value="<?= $extraparams["socioId"]; ?>" />
<input
    type="hidden" id="extraProductoId"
    value="<?= $extraparams["productoId"]; ?>" />
<input
    type="hidden" id="extraSocioFolio"
    value="<?= $extraparams["socioFolio"]; ?>" />
<input
    type="hidden" id="extraProductoData"
    value="<?= $extraparams["datosProducto"] ?>" />

<?php endif; ?>


<!-- Termina bloque de pestañas emergentes -->
<style>#TitleModal{font-size: 13px; font-weight: bold; text-align: center; padding-top: 25px;}</style>