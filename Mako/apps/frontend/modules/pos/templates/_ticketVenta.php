<?php
/**
 * Ticket de venta
 * @version: $Id: _ticketVenta.php,v 1.6 2010/11/25 05:03:58 eorozco Exp $
 */
?>
a1<?php echo sfConfig::get('app_nombre_empresa')."\n" ?>
Folio: <?php echo sprintf("%08d",$orden->getFolio()) ?> - <?php echo $orden->getCentro()->getNombre()."\n" ?>
Fecha: <?php echo $orden->getCreatedAt()."\n" ?>
ID TS: <?php echo $orden->getId()."\n" ?>
Usuario: <?php echo $orden->getSocio()->getUsuario()."\n" ?>
Cred: <?php echo $orden->getSocio()->getFolio()."\n" ?>
a0------------------------------------------------
            Producto           Cant.    Subtotal
------------------------------------------------
<?php echo $cadp."\n" ?>
<?php echo sprintf("%48s","TOTAL: $".sprintf("%9s",$total))."\n" ?>
<?php echo sprintf("%48s","Importe: $".sprintf("%9s",$importe))."\n" ?>
<?php echo sprintf("%48s","IVA: $".sprintf("%9s",$iva))."\n" ?>
<?php echo $totalPalabras."\n" ?>
------------------------------------------------
<?php echo $orden->getCentro()->getRazonSocial()."\n" ?>
R.F.C. <?php echo $orden->getCentro()->getRfc()."\n" ?>
<?php echo $orden->getCentro()->getDireccion()."\n" ?>
------------------------------------------------
a1Conserve su ticket para aclaraciones.
Gracias por su compra.
------------------------------------------------
<?php echo ($caf_prods)? "VALE CAFETERIA\n" : "" ?>
a0<?php echo $caf_prods."\n" ?>
d2



