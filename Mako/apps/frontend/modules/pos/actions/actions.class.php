<?php
/*
 * Componentes Bixit
*
* Copyright (c) 2009-2011 Bixit SA de CV
* Bajo Licencia dual MIT (http://www.bixit.mx/legal/MIT-LICENSE.txt)
* y GPL (http://www.bixit.mx/legal/GPL-LICENSE.txt).
*
*/
/**
 * pos actions.
 *
 * @package    bixit
 * @subpackage pos
 * @author     Bixit SA de CV
 * @version    SVN: $Id: actions.class.php,v 1.15 2011-04-08 20:42:05 eorozco Exp $
 */
class posActions extends sfActions
{
	public function executeIndex(sfWebRequest $request)
	{
		//$this->redirect('pos/new');
		$this->socio_id =  $request->getParameter("socio_id");
		$this->qFolio =  $request->getParameter("qFolio");
		$this->qNombre =  $request->getParameter("qNombre");
		$this->qUsuario =  $request->getParameter("qUsuario");
	}

	public function executeNew(sfWebRequest $request)
	{

		/**
		 *
		 * ************************************************************************************************************
		 *
		 * @title Reingenieria Mako
		 * @author silvio.bravo@enova.mx
		 * @date 27 Feb 2014
		 * @desciption Agregamos el la consulta al catalodo de formas de pago para presentarlas
		 * al usuario dentro de la pantalla de punto de venta.
		 *
		 * ***********************************************************************************************************
		 *
		 * **/

		//TODO: Se debe validar que haya un id_usuario válido si no enviar msg de error.
		$extraparams				=array("socioId"=>"", "socioFolio"=>"", "productoId"=>"", "datosProducto"=>"");
		$this->form 				= new DetalleOrdenForm();
		$c 							= new Criteria();
		$c->addAscendingOrderByColumn(FormaPagoPeer::ID);
		$this->formasPagoArray		= FormaPagoPeer::doSelect($c);
		$this->socio_id 			= $request->getParameter("socio_id");
		$this->appUsarFormaPago		= (sfConfig::get('app_pv_muestra_formas_pago')==false)?0:1;
		$this->entidadesFederativas	= EntidadFederativaPeer::getAll();


		/**
		 * ******************************************************************************************
		 * Agregamos funcionalidad para evaluar si se esta enviando un id de socio 					*
		 * y un producto que deba ser cargado por default.											*
		 * @author silvio.bravo@enova.mx 24 Marzo 2014												*
		 * @copyright Enova																			*
		 * @category Reingenieria Mako																*
		 * ******************************************************************************************
		 */

		if($request->hasParameter("redirected") && $request->hasParameter("socioId") && $request->hasParameter("productoId")){
				
			$mysocio							= SocioPeer::retrieveByPK($request->getParameter("socioId"));
			$extraparams["socioId"]				= $request->getParameter("socioId");
			$producto							= ProductoPeer::retrieveByPK($request->getParameter("productoId"));
			$extraparams["socioFolio"]			= $mysocio->getFolio();
			if($producto){
				
				$extraparams["productoId"]		= $request->getParameter("productoId");
				$extraparams["datosProducto"]	= json_encode(ProductoPeer::getProductosJustByCode($producto->getCodigo()));
			}
		}
		$this->extraparams			= $extraparams;
	}


	/**
	 * Imprime el arreglo de productos en formato JSON
	 * Este método recibe en el request el parámetro "q" que es la cadena a buscar y el parámetro limit, que es el
	 * limit del query. Estos parametros los recibe normalmente del ajaxautocomplete.
	 * Regresa un arreglo JSON con el id del producto y el nombre
	 * @param sfWebRequest $request
	 * @return string Arreglo JSON
	 */
	public function executeArregloProductosPos(sfWebRequest $request)
	{
		$this->getResponse()->setContentType('application/json');

		$productos = ProductoPeer::arregloProductosPos(
				$request->getParameter('q'),
				$request->getParameter('limit'),
				$request->getParameter('socio_id')
		);

		return $this->renderText(json_encode($productos));
	}

	/**
	 * Regresa un arreglo JSON con productos consultados por código
	 * @param sfWebRequest $request
	 * @return string json
	 */
	public function executeProcutosPorCodigo(sfWebRequest $request)
	{
		$this->getResponse()->setContentType('application/json');

		$productos = ProductoPeer::arregloProductosPorCodigo(
				$request->getParameter('q'),
				$request->getParameter('limit'),
				$request->getParameter('socio_id')
		);

		return $this->renderText(json_encode($productos));
	}

	/**
	 * Regresa un arreglo JSON con los datos fiscales del socio
	 * @param sfWebRequest $request
	 * @return string json
	 */
	public function executeGetFiscales(sfWebRequest $request)
	{
		$this->getResponse()->setContentType('application/json');

		$fiscales = SocioPeer::getFiscales($request->getParameter('socio_id'));

		return $this->renderText(json_encode($fiscales));
	}

	/**
	 * Envía los datos de la venta y regresa un arreglo JSON con el resultado de la operación.
	 * @param sfWebRequest $request
	 * @return string json
	 */
	public function executeRealizarVenta(sfWebRequest $request)
	{

		$this->getResponse()->setContentType('application/json');

		$cajero = $this->getUser()->getAttribute('usuario');
		$res	='OK';
		$msg 	= 'Se ha realizado la venta con éxito';

		$datosVenta = $request->getParameter('venta');

		//Decodificamos la cadena
		$v = json_decode($datosVenta,true);

		//error_log(serialize($v));
		$socio_id 					= (isset($v['socio_id']))? $v['socio_id']: 0;
		$total 						= (isset($v['total']))? $v['total']: 0;
		$fiscales 					= (isset($v['fiscales']))? $v['fiscales']: array();
		$es_factura 				= (isset($v['es_factura']))? $v['es_factura']: '';
		$venta 						= (isset($v['venta']))? $v['venta']: array();
		$forma_pago					= (isset($v['forma_pago_id']))? $v['forma_pago_id']: 1;
		$num_aprobacion_tarjeta		= (isset($v['forma_pago_id']) && $v['forma_pago_id']>=2)? $v['num_aprobacion_tarjeta']: null;


		//error_log("socio_id: ".$socio_id." total: ".$total." fiscales: ".$fiscales." es fact ".$es_factura." arts: ".count($venta));

		//Validamos si hay socio y si existe
		if($socio_id == 0)
		{
			error_log("ERROR[executeRealizarVenta] $socio_id No se selecciono un socio.");
			return $this->renderText(json_encode(array(
					'resultado'=>'error',
					'msg'=>'No se seleccionó un socio válido. Seleccione un socio para realizar una venta'
			)
			));
		}
		//error_log("Pasa 1");
		$socio = SocioPeer::retrieveByPk($socio_id);
		if($socio == null)
		{
			error_log("ERROR[executeRealizarVenta] $socio_id No existe.");
			return $this->renderText(json_encode(array(
					'resultado'=>'error',
					'msg'=>'No se seleccionó un socio válido. Seleccione un socio para realizar una venta'
			)
			));
		}
		//error_log("Pasa 2");
		//Verificamos si hay productos en la venta
		if(count($venta) == 0)
		{
			error_log("ERROR[executeRealizarVenta] No hay productos que vender.");
			return $this->renderText(json_encode(array(
					'resultado'=>'error',
					'msg'=>'Debe agregar los productos a vender.'
			)
			));
		}
		//error_log("Pasa 3");
		//Verificamos si ya comenzo y si el pago es total
		//Si el pago es extemporaneo y total no es posible realizar la venta
		//Si el curso ya terminó no es posible realizar la venta
		/*		$res = $this->verificacionesCursoClubIdea($venta,$socio->getId());
		if($res == "CURSO_FINALIZADO")
		{
		error_log("ERROR[executeRealizarVenta] Hay un curso finalizado.");
		return $this->renderText(json_encode(array(
				'resultado'=>'error',
				'msg'=>'El curso(s) que se intenta vender ha finalizado. No es posible realizar la venta.'
		)
		));
		exit;
		}
		elseif($res == "CURSO_EXTEMPORANEO")
		{
		error_log("ERROR[executeRealizarVenta] .");
		return $this->renderText(json_encode(array(
				'resultado'=>'error',
				'msg'=>'El curso(s) ya ha(n) comenzado, no es posible realizar el pago total. Proceda a realizar la venta en parcialidades.'
		)
		));
		}
		*/
		//Se implementa distincion de ventas producto tasa0 y se emite otro ticket o factura dependiendo del caso para separar los impuestosd declarados.
		$ordenesVenta = array();
		$ordenesVenta['tasa0'] = array();
		$ordenesVenta['tasa16'] = array();
		foreach($venta as $item) {
			//Si son libros no aplica iva por lo tanto se debe crear otra orden de venta.
			if($item[1] == 'Libros') {
				$ordenesVenta['tasa0']['items'][] = $item;
				$ordenesVenta['tasa0']['total'] = isset($ordenesVenta['tasa0']['total']) ? $ordenesVenta['tasa0']['total'] + $item[6] : $item[6];
			}
			else {
				error_log("Se deteca otro producto o servicio");
				$ordenesVenta['tasa16']['items'][] = $item;
				$ordenesVenta['tasa16']['total'] = isset($ordenesVenta['tasa16']['total']) ? $ordenesVenta['tasa16']['total'] + $item[6] : $item[6];
			}
		}

		foreach($ordenesVenta as $tipov => $items)
		{
			if(!count($items)) continue;

			if($tipov == 'tasa0')
			{
				$orden = new Orden();
				$orden->setUsuario($cajero);
				$orden->setSocio($socio);
				$orden->setCentroId(sfConfig::get('app_centro_actual_id'));
				$orden->setTipoId(sfConfig::get('app_orden_venta_id'));
				$orden->setTotal($items['total']);
				$orden->setEsFactura($es_factura);
				$orden->setFolio(FolioVentaPeer::folio(sfConfig::get('app_centro_actual_id')));
				$orden->setIpCaja(Comun::ipReal());
				$orden->setFormaPago(FormaPagoPeer::retrieveByPK($forma_pago));
				$orden->setNumAprobacionTarjeta($num_aprobacion_tarjeta);
				$venta = $orden->agregarProductos($items['items']);
				if($es_factura)
				{
					$orden->agregarDatosFiscales($fiscales);
				}
				$orden->save();
				//Si ya salvamos los datos entonces actualizamos el folio de ventas
				FolioVentaPeer::folioSiguiente(sfConfig::get('app_centro_actual_id'));
				$res = "ok"; $msg="Venta realizada con éxito";
				$resultado = array('resultado'=>$res,'msg'=>$msg);
				$this->dispatcher->notify(new sfEvent($this, 'orden.venta.realizada', array(
						'socio_id' => $socio_id,
						'orden_id' => $orden->getId(),
						'orden' => $orden,
						'fiscales' => $fiscales,
						'tasa0' => true
				)));
				$this->guardaSincroniaSap($orden);
			}
			elseif($tipov == 'tasa16')
			{
				$orden = new Orden();
				$orden->setUsuario($cajero);
				$orden->setSocio($socio);
				$orden->setCentroId(sfConfig::get('app_centro_actual_id'));
				$orden->setTipoId(sfConfig::get('app_orden_venta_id'));
				$orden->setTotal($items['total']);
				$orden->setEsFactura($es_factura);
				$orden->setFolio(FolioVentaPeer::folio(sfConfig::get('app_centro_actual_id')));
				$orden->setIpCaja(Comun::ipReal());
				$orden->setFormaPago(FormaPagoPeer::retrieveByPK($forma_pago));
				$orden->setNumAprobacionTarjeta($num_aprobacion_tarjeta);
					
				$venta = $orden->agregarProductos($items['items']);
				if($es_factura)
				{
					$orden->agregarDatosFiscales($fiscales);
				}
				$orden->save();
				//Si ya salvamos los datos entonces actualizamos el folio de ventas
				FolioVentaPeer::folioSiguiente(sfConfig::get('app_centro_actual_id'));
				$res = "ok"; $msg="Venta realizada con éxito";
				$resultado = array('resultado'=>$res,'msg'=>$msg);
				$this->dispatcher->notify(new sfEvent($this, 'orden.venta.realizada', array(
						'socio_id' => $socio_id,
						'orden_id' => $orden->getId(),
						'orden' => $orden,
						'fiscales' => $fiscales
				)));
				$this->guardaSincroniaSap($orden);
			}
		}
		return $this->renderText(json_encode($resultado));
	}

	/**
	 * Guarda la venta en la tabla sincronia_sap para la sincronizacion con SAP
	 * @param sfWebRequest $request
	 * @return string json
	 */
	public function guardaSincroniaSap($orden)
	{
			$sincronia_sap = new SincroniaSap();
			$sincronia_sap->setOperacion('Agregar_Venta');
			$sincronia_sap->setPkReferencia($orden->getId());
			$sincronia_sap->setCompleto(false);
			$sincronia_sap->setCreatedAt(time());
			$sincronia_sap->save();

		return $sincronia_sap;
	}

	/**
	 * Regresa un arreglo JSON con los datos de los cupones por redimir del socio
	 * @param sfWebRequest $request
	 * @return string json
	 */
	public function executeCuponesPorRedimir(sfWebRequest $request)
	{
		$this->getResponse()->setContentType('application/json');
		$cupones = CuponPeer::cuponesPorRedimir($request->getParameter('socio_id'));
		return $this->renderText(json_encode($cupones));
	}

	/**
	 * Regresa un arreglo JSON con los datos de los cupones redimidos
	 * @param sfWebRequest $request
	 * @return string json
	 */
	public function executeCuponesRedimidos(sfWebRequest $request)
	{
		$this->getResponse()->setContentType('application/json');
		$cupones = CuponPeer::cuponesRedimidos($request->getParameter('socio_id'));
		return $this->renderText(json_encode($cupones));
	}

	/**
	 * Regresa un arreglo JSON con los datos de los subproductos de cobranza
	 * @param sfWebRequest $request
	 * @return string json
	 */
	public function executeSubProducto(sfWebRequest $request)
	{

		$this->getResponse()->setContentType('application/json');

		$c = new Criteria();
		$c->add(SubproductoCobranzaPeer::SUB_PRODUCTO_DE,$request->getParameter('subproducto_de'));
		$c->add(SubproductoCobranzaPeer::COBRO_ID,$request->getParameter('cobro_id'));
		$c->add(SubproductoCobranzaPeer::NUMERO_PAGO,1);
		$c->setLimit(1);
		$producto=array();
		$sp = SubproductoCobranzaPeer::doSelectOne($c);

		$prod = $sp->getProductoRelatedByProductoId();

		$socio = SocioPeer::retrieveByPK($request->getParameter('socio_id'));
		$promocion = PromocionPeer::descuento($prod,$socio);
			
		$descuento = 0;
		$promocion_id = "";
			
		if($promocion != null)
		{
			$descuento = $promocion->getDescuento();
			$promocion_id = $promocion->getId();
		}

		$producto = array(
				'producto_id'=>$sp->getProductoId(),
				'nombre' => $prod->getNombre(),
				'precio_lista' => $prod->getPrecioLista(),
				'descripcion' => $prod->getDescripcion(),
				'categoria' => $prod->getCategoriaProducto()->getNombre(),
				'existencia' => 100,
				'codigo' => $prod->getCodigo(),
				'descuento' => $descuento,
				'promocion_id' => $promocion_id
		);

		return $this->renderText(json_encode($producto));
	}


	/**
	 * Regresa un arreglo JSON con los datos de los pagos pendientes del socio
	 * @param sfWebRequest $request
	 * @return string json
	 */
	public function executePagosPendientes(sfWebRequest $request)
	{
		$this->getResponse()->setContentType('application/json');
		$pagos = SocioPagosPeer::pagosPendientes($request->getParameter('socio_id'));
		return $this->renderText(json_encode($pagos));
	}

	/**
	 * Setea la bandera de acuerdo de pago.
	 * @param sfWebRequest $request
	 */
	public function executeAcuerdoPago(sfWebRequest $request)
	{
		$this->getResponse()->setContentType('application/json');
		$res = SocioPagosPeer::acuerdoPago($request->getParameter('pago_id'),$request->getParameter('acuerdo_pago'));
		return $this->renderText(json_encode($res));
	}

	/**
	 * Verififaciones de pagos de cursos extemporaneos para club idea
	 * @param unknown_type $ventas
	 * @param unknown_type $socio_id
	 */
	private function verificacionesCursoClubIdea($ventas,$socio_id)
	{

		foreach($ventas as $v)
		{
			$producto_id = (isset($v[7])) ? $v[7] : 0;
			if($producto_id == 0) continue;

			$p = ProductoPeer::retrieveByPK($producto_id);
			list($f1,$f2)=ProductoPeer::fechasCurso($producto_id,$socio_id);
			error_log("F1: $f1, F2: $f2");

			if($p->getCategoriaId() == sfConfig::get('app_categoria_presenciales_id'))
			{
				list($tsini,$tsfin) = ProductoPeer::fechasCurso($producto_id,$socio_id,"U");
				if($tsini == 0 || $tsfin == 0) return "SIN_FECHA";

				$ts = date("U");
				if($tsfin < $ts && $tsfin > 0)
				{
					error_log("Curso extemporaneo");
					return "CURSO_FINALIZADO";
				}
				if(!$p->getEsSubproducto())
				{
					//Es un pago completo
					if($tsini < $ts && $tsini > 0)
					{
						//Es un curso extemporaneo, no se puede pagar completo
						error_log("Curso extemporaneo");
						return "CURSO_EXTEMPORANEO";
					}
				}

			}
		}
	}

}