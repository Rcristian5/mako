<?php use_stylesheet('control_escolar/control_escolar.css') ?>
<?php use_stylesheet('datatables.css') ?>
<?php use_stylesheet('jquery.windows.css') ?>
<?php use_javascript('jquery.ui.js') ?>
<?php use_javascript('plugins/jquery.windows.js') ?>
<?php use_javascript('plugins/jquery.dataTables.js') ?>
<?php use_javascript('control_escolar/categoria_curso.js') ?>

<div id="windows-wrapper">
	<div id="columnaIzquierda" window-title="Categorías Curso"
		window-state="min">

		<?php include_partial('form', array('form' => $form)) ?>

	</div>

	<div id="columnaDerecha" window-title="Lista de categorías"
		window-state="max">

		<h2>Lista de categor&iacute;as</h2>

		<?php include_partial('detalleCategorias', array('detalle' => $detalle)) ?>

	</div>

</div>

