<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<?php use_stylesheet('./categoria_curso/_form.css')?>

<?php if ($form->getObject()->isNew()): ?>
<h2>Crear categoría</h2>
<?php else: ?>
<h2>Editar categoría</h2>
<?php endif; ?>

<form
	action="<?php echo url_for('categoria_curso/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>"
	method="post"
	<?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>
	onsubmit="return requeridos();">
	<?php if (!$form->getObject()->isNew()): ?>
	<input type="hidden" name="sf_method" value="put" />
	<?php endif; ?>
	<?php echo $form->renderHiddenFields(false) ?>
	<table class="formulario" width="100%">
		<tbody>
			<?php echo $form->renderGlobalErrors() ?>
			<tr>
				<th><?php echo $form['padre']->renderLabel('Categoria Padre') ?></th>
				<td><?php echo $form['padre']->renderError() ?> <?php echo $form['padre'] ?>
				</td>
			</tr>
			<tr>
				<th><?php echo $form['nombre']->renderLabel() ?></th>
				<td><?php echo $form['nombre']->renderError() ?> <?php echo $form['nombre']->render(array('requerido'=>1,'nouppercase'=>1, 'filtro'=>'alfanumerico', 'size'=>42)) ?>
				</td>
			</tr>
			<tr>
				<th><?php echo $form['siglas']->renderLabel() ?></th>
				<td><?php echo $form['siglas']->renderError() ?> <?php echo $form['siglas']->render(array('requerido'=>1, 'filtro'=>'alfabetico', 'size'=>2, 'maxlength'=>2 )) ?>
				</td>
			</tr>
			<tr>
				<th><?php echo $form['descripcion']->renderLabel('Descripción') ?></th>
				<td><?php echo $form['descripcion']->renderError() ?> <?php echo $form['descripcion']->render(array('requerido'=>1, 'filtro'=>'alfanumerico', 'cols'=>40, 'rows'=>4)) ?>
				</td>
			</tr>
		</tbody>
	</table>
	<p class="botones_finales">
		<input type="button" value="Cancelar"
			onclick="location.href='<?php echo url_for('categoria_curso/index')?>'">
		<input type="submit" value="Guardar" />
	</p>
</form>
