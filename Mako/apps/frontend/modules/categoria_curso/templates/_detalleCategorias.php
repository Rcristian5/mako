<table id="detalle_categorias" class="display" style="width: 100%;">
	<thead>
		<tr>
			<th class="acciones-tabla"></th>
			<th>Categor&iacute;a padre</th>
			<th>Nombre</th>
			<th>Descripci&oacute;n</th>
			<th class="acciones-tabla"></th>
			<th class="acciones-tabla"></th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($detalle as $categoria): ?>
		<tr>
			<td><a
				href="<?php echo url_for('categoria_curso/edit?id='.$categoria->getId()) ?>"><span
					class="ui-icon ui-icon-pencil bx-title" title="Editar categoria"></span>
			</a>
			</td>
			<td><?php echo ($categoria->getPadre() ? $categoria->getCategoriaCursoRelatedByPadre()->getNombre() : 'PRINCIPAL' ) ?>
			</td>
			<td><?php echo $categoria->getNombre() ?>
			</td>
			<td><?php echo $categoria->getDescripcion() ?>
			</td>
			<td><?php echo ($categoria->getActiva() ? link_to('<span class="ui-icon ui-icon-bullet bx-title" title="Desactivar categoría"></span>', 'categoria_curso/disabled?id='.$categoria->getId(), array('confirm' => '¿Deseas desactivar esta categoría?')) : link_to('<span class="ui-icon ui-icon-radio-on bx-tittle" title="Activar categoría"></span>', 'categoria_curso/enabled?id='.$categoria->getId(), array('confirm' => '¿Deseas activar esta categoría?')) ) ?>
			</td>
			<td><?php echo link_to('<span class="ui-icon ui-icon-trash bx-title" title="Borrar categoría"></span>', 'categoria_curso/delete?id='.$categoria->getId(), array('method' => 'delete', 'confirm' => '¿Deseas elimar esta categoría? \n Todos los datos asociados a esta categoría serán borrados')) ?>
			</td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>
