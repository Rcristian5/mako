<?php

/**
 * categoria_curso actions.
 *
 * @package    mako
 * @subpackage categoria_curso
 * @author     Bixit SA de CV
 * @version    SVN: $Id: actions.class.php,v 1.2 2010/09/03 17:33:50 david Exp $
 */
class categoria_cursoActions extends sfActions
{
	public function executeIndex(sfWebRequest $request)
	{
		$this->detalle = CategoriaCursoPeer::doSelect(new Criteria());
		$this->form = new CategoriaCursoForm(null, array('operador_id' => $this->getUser()->getAttribute('usuario')->getId()));
	}

	public function executeCreate(sfWebRequest $request)
	{
		$this->forward404Unless($request->isMethod(sfRequest::POST));

		$this->form = new CategoriaCursoForm(null, array('operador_id' => $this->getUser()->getAttribute('usuario')->getId()));

		$this->processForm($request, $this->form);

		$this->detalle = CategoriaCursoPeer::doSelect(new Criteria());

		$this->setTemplate('index');
	}

	public function executeEdit(sfWebRequest $request)
	{
		$this->forward404Unless($CategoriaCurso = CategoriaCursoPeer::retrieveByPk($request->getParameter('id')), sprintf('Object CategoriaCurso does not exist (%s).', $request->getParameter('id')));

		$this->form = new CategoriaCursoForm($CategoriaCurso, array('operador_id' => $this->getUser()->getAttribute('usuario')->getId()));
		$this->detalle = CategoriaCursoPeer::doSelect(new Criteria());

		$this->setTemplate('index');
	}

	public function executeUpdate(sfWebRequest $request)
	{
		$this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
		$this->forward404Unless($CategoriaCurso = CategoriaCursoPeer::retrieveByPk($request->getParameter('id')), sprintf('Object CategoriaCurso does not exist (%s).', $request->getParameter('id')));

		$this->form = new CategoriaCursoForm($CategoriaCurso, array('operador_id' => $this->getUser()->getAttribute('usuario')->getId()));

		$this->processForm($request, $this->form);

		$this->detalle = CategoriaCursoPeer::doSelect(new Criteria());
		$this->setTemplate('index');
	}

	public function executeDelete(sfWebRequest $request)
	{
		$request->checkCSRFProtection();

		$this->forward404Unless($categoria = CategoriaCursoPeer::retrieveByPk($request->getParameter('id')), sprintf('Object CategoriaCurso does not exist (%s).', $request->getParameter('id')));
		$this->getUser()->setFlash('notice', 'Categoría "'.$categoria->getNombre().'" borrada');
		$categoria->delete();

		$this->redirect('categoria_curso/index');
	}

	/**
	 * Ejecuta la acción para desactivar un registro
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeEnabled(sfWebRequest $request)
	{
		$this->forward404Unless($categoria = CategoriaCursoPeer::retrieveByPk($request->getParameter('id')), sprintf('La categoria no existe (%s).', $request->getParameter('id')));
		$categoria->setActiva(true);
		$categoria->setOperadorId($this->getUser()->getAttribute('usuario')->getId());
		$categoria->save();

		$this->getUser()->setFlash('notice', 'Categoría "'.$categoria->getNombre().'" activada');
		$this->redirect('categoria_curso/index');
	}

	/**
	 * Ejecuta la acción para desactivar un registro
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeDisabled(sfWebRequest $request)
	{
		$this->forward404Unless($categoria = CategoriaCursoPeer::retrieveByPk($request->getParameter('id')), sprintf('La categoria no existe (%s).', $request->getParameter('id')));
		$categoria->setActiva(false);
		$categoria->setOperadorId($this->getUser()->getAttribute('usuario')->getId());
		$categoria->save();

		$this->getUser()->setFlash('notice', 'Categoría "'.$categoria->getNombre().'" desactivada');
		$this->redirect('categoria_curso/index');
	}

	protected function processForm(sfWebRequest $request, sfForm $form)
	{

		$form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
		if ($form->isValid())
		{
			$mensaje = 'Categoría "'.$form->getValue('nombre').'" actualizada';
			if($form->getObject()->isNew())
			{
				$mensaje = 'Categoría "'.$form->getValue('nombre').'" creada';
			}
			$categoria = $form->save();
			if($categoria->getPadre())
			{
				$path = $categoria->getCategoriaCursoRelatedByPadre()->getPath().'/'.$categoria->getId();
			}
			else
			{
				$path = '/'.$categoria->getId();
			}
			$categoria->setPath($path);
			$categoria->save();

			$this->getUser()->setFlash('notice', $mensaje);
			$this->redirect('categoria_curso/index');
		}
	}
}
