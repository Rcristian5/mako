<h1>Usuarios List</h1>

<table>
	<thead>
		<tr>
			<th>Id</th>
			<th>Rol</th>
			<th>Usuario</th>
			<th>Clave</th>
			<th>Nombre</th>
			<th>Apepat</th>
			<th>Apemat</th>
			<th>Nombre completo</th>
			<th>Email</th>
			<th>Operador</th>
			<th>Activo</th>
			<th>Created at</th>
			<th>Updated at</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($Usuarios as $Usuario): ?>
		<tr>
			<td><a
				href="<?php echo url_for('admin_usuarios/show?id='.$Usuario->getId()) ?>"><?php echo $Usuario->getId() ?>
			</a></td>
			<td><?php echo $Usuario->getRolId() ?></td>
			<td><?php echo $Usuario->getUsuario() ?></td>
			<td><?php echo $Usuario->getClave() ?></td>
			<td><?php echo $Usuario->getNombre() ?></td>
			<td><?php echo $Usuario->getApepat() ?></td>
			<td><?php echo $Usuario->getApemat() ?></td>
			<td><?php echo $Usuario->getNombreCompleto() ?></td>
			<td><?php echo $Usuario->getEmail() ?></td>
			<td><?php echo $Usuario->getOperadorId() ?></td>
			<td><?php echo $Usuario->getActivo() ?></td>
			<td><?php echo $Usuario->getCreatedAt() ?></td>
			<td><?php echo $Usuario->getUpdatedAt() ?></td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<a href="<?php echo url_for('admin_usuarios/new') ?>">New</a>
