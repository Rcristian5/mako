<table>
	<tbody>
		<tr>
			<th>Id:</th>
			<td><?php echo $Usuario->getId() ?>
			</td>
		</tr>
		<tr>
			<th>Rol:</th>
			<td><?php echo $Usuario->getRolId() ?>
			</td>
		</tr>
		<tr>
			<th>Usuario:</th>
			<td><?php echo $Usuario->getUsuario() ?>
			</td>
		</tr>
		<tr>
			<th>Clave:</th>
			<td><?php echo $Usuario->getClave() ?>
			</td>
		</tr>
		<tr>
			<th>Nombre:</th>
			<td><?php echo $Usuario->getNombre() ?>
			</td>
		</tr>
		<tr>
			<th>Apepat:</th>
			<td><?php echo $Usuario->getApepat() ?>
			</td>
		</tr>
		<tr>
			<th>Apemat:</th>
			<td><?php echo $Usuario->getApemat() ?>
			</td>
		</tr>
		<tr>
			<th>Nombre completo:</th>
			<td><?php echo $Usuario->getNombreCompleto() ?>
			</td>
		</tr>
		<tr>
			<th>Email:</th>
			<td><?php echo $Usuario->getEmail() ?>
			</td>
		</tr>
		<tr>
			<th>Operador:</th>
			<td><?php echo $Usuario->getOperadorId() ?>
			</td>
		</tr>
		<tr>
			<th>Activo:</th>
			<td><?php echo $Usuario->getActivo() ?>
			</td>
		</tr>
		<tr>
			<th>Created at:</th>
			<td><?php echo $Usuario->getCreatedAt() ?>
			</td>
		</tr>
		<tr>
			<th>Updated at:</th>
			<td><?php echo $Usuario->getUpdatedAt() ?>
			</td>
		</tr>
	</tbody>
</table>

<hr />

<a
	href="<?php echo url_for('admin_usuarios/edit?id='.$Usuario->getId()) ?>">Edit</a>
&nbsp;
<a href="<?php echo url_for('admin_usuarios/index') ?>">List</a>
