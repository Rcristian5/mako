<?php use_javascript('plugins/jquery.dataTables.js') ?>
<?php use_javascript('jquery.ui.js') ?>
<?php use_stylesheet('datatables.css') ?>
<script>
var lsU;
$(document).ready(function() {

   lsU = $('#lista-usuarios').dataTable({
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
        "bJQueryUI": true,
        "sPaginationType": "full_numbers"
     });
   
});
</script>

<div class="ui-widget ui-corner-all sombra">
	<div class="ui-widget-content ui-corner-all">
		<div class="ui-widget-header ui-corner-top tsss" style="padding: 5px;">Administración
			de recursos humanos</div>

		<table width="100%" id="lista-usuarios">
			<thead>
				<tr>
					<th>Modificar</th>
					<th>Rol</th>
					<th>Usuario</th>
					<th>Nombre</th>
					<th>Activo</th>
					<th>Fecha alta</th>
					<th>Fecha Mod.</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($Usuarios as $Usuario): ?>
				<tr>
					<td align="center"><a
						href="<?php echo url_for('admin_usuarios/edit?id='.$Usuario->getId()) ?>">
							<span class="ui-icon  ui-icon-pencil"></span>
					</a>
					</td>
					<td><?php echo $Usuario->getRol()->getNombre() ?></td>
					<td><?php echo $Usuario->getUsuario() ?></td>
					<td><?php echo $Usuario ?></td>
					<td><?php echo ($Usuario->getActivo())?'SI':'NO' ?></td>
					<td><?php echo $Usuario->getCreatedAt("%Y-%m-%d") ?></td>
					<td><?php echo $Usuario->getUpdatedAt("%Y-%m-%d") ?></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
