<?php

/**
 * admin_usuarios actions.
 *
 * @package    mako
 * @subpackage admin_usuarios
 * @author     Bixit SA de CV
 * @version    SVN: $Id: actions.class.php,v 1.3 2010/08/31 01:30:47 eorozco Exp $
 */
class admin_usuariosActions extends sfActions
{
	public function executeIndex(sfWebRequest $request)
	{
		$this->Usuarios = UsuarioPeer::doSelect(new Criteria());
	}

	public function executeShow(sfWebRequest $request)
	{
		$this->Usuario = UsuarioPeer::retrieveByPk($request->getParameter('id'));

		$this->forward404Unless($this->Usuario);
	}

	public function executeNew(sfWebRequest $request)
	{
		$this->form = new UsuarioForm();
		$this->Usuarios = UsuarioPeer::doSelect(new Criteria());
	}

	public function executeCreate(sfWebRequest $request)
	{
		$this->forward404Unless($request->isMethod(sfRequest::POST));

		$this->form = new UsuarioForm();

		$this->processForm($request, $this->form);

		$this->setTemplate('new');
	}

	public function executeEdit(sfWebRequest $request)
	{
		$this->forward404Unless($Usuario = UsuarioPeer::retrieveByPk($request->getParameter('id')), sprintf('Object Usuario does not exist (%s).', $request->getParameter('id')));
		$this->form = new UsuarioForm($Usuario);
	}

	public function executeUpdate(sfWebRequest $request)
	{
		$this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
		$this->forward404Unless($Usuario = UsuarioPeer::retrieveByPk($request->getParameter('id')), sprintf('Object Usuario does not exist (%s).', $request->getParameter('id')));
		$this->form = new UsuarioForm($Usuario);

		$this->processForm($request, $this->form);

		$this->setTemplate('edit');
	}

	public function executeDelete(sfWebRequest $request)
	{
		$request->checkCSRFProtection();

		$this->forward404Unless($Usuario = UsuarioPeer::retrieveByPk($request->getParameter('id')), sprintf('Object Usuario does not exist (%s).', $request->getParameter('id')));
		$Usuario->delete();

		$this->redirect('admin_usuarios/index');
	}

	protected function processForm(sfWebRequest $request, sfForm $form)
	{
		$form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));

		if ($form->isValid())
		{
			$Usuario = $form->save();
			if(!$form->isNew())
			{
				$u_anterior = $form->cambioNombreUsuario() ? $form->getUsuarioAnterior()->getUsuario():'';
				$pars_ev=array('modificaUsername'=>$form->cambioNombreUsuario(),'usuario_anterior'=>$u_anterior);
				sfProjectConfiguration::getActive()->getEventDispatcher()->notify(new sfEvent($Usuario, 'usuario.actualizado',$pars_ev));

				$this->getUser()->setFlash('ok', sprintf('Se ha modificado el usuario %s de nombre %05s correctamente',$Usuario->getUsuario(),$Usuario));
			}
			else
			{
				sfProjectConfiguration::getActive()->getEventDispatcher()->notify(new sfEvent($Usuario, 'usuario.registrado'));
				$this->getUser()->setFlash('ok', sprintf('Se ha creado el usuario %s de nombre %05s correctamente',$Usuario->getUsuario(),$Usuario));
			}

			$this->redirect('admin_usuarios/new');
		}
	}
}
