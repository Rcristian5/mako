<?php

/**
 * registro actions.
 *
 * @package    mako
 * @subpackage registro
 * @author     Bixit SA de CV
 * @version    SVN: $Id: actions.class.php,v 1.13 2010/08/31 01:46:40 eorozco Exp $
 */
class registrociActions extends sfActions
{
	public function executeIndex(sfWebRequest $request)
	{

		$this->Socios = SocioPeer::listaSocios($request->getParameter('idSocio'), $request->getParameter('qNombre'),$request->getParameter('qUsuario'),$request->getParameter('qFolio'), $request->getParameter('limit'));

		if(empty($request['idSocio']) && empty($request['qNombre']) && empty($request['qUsuario']) && empty($request['qFolio']))$this->msg = '';
		else{
			if(!count($this->Socios)){
				$this->msg = 'No se han encontrado socios coincidentes con los datos proporcionados.';
			}
		}
	}


	public function executeShow(sfWebRequest $request)
	{
		$this->Socio = SocioPeer::retrieveByPk($request->getParameter('id'));
		$this->forward404Unless($this->Socio);
	}

	public function executeNew(sfWebRequest $request)
	{

		$Socio = SocioPeer::retrieveByPk($request->getParameter('id'));
		//$this->form = new SocioForm($Socio);
		$this->form = new SocioForm($Socio, array('url' => $this->getController()->genUrl('registroci/consultaEmpresas')));
	}

	public function executeCreate(sfWebRequest $request)
	{
		$this->forward404Unless($request->isMethod(sfRequest::POST));

		$this->form = new SocioForm();

		$this->processForm($request, $this->form);

		$this->setTemplate('new');

	}

	public function executeEdit(sfWebRequest $request)
	{
		$this->forward404Unless($Socio = SocioPeer::retrieveByPk(
				$request->getParameter('id')),
				sprintf('Object Socio does not exist (%s).',
						$request->getParameter('id'))
		);

		//$this->form = new SocioForm($Socio);
		$this->form = new SocioForm($Socio, array('url' => $this->getController()->genUrl('registroci/consultaEmpresas')));
	}

	public function executeUpdate(sfWebRequest $request)
	{
		$this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
		$this->forward404Unless($Socio = SocioPeer::retrieveByPk($request->getParameter('id')), sprintf('Object Socio does not exist (%s).', $request->getParameter('id')));
		$this->form = new SocioForm($Socio);

		$this->processForm($request, $this->form);

		$this->setTemplate('edit');
	}

	public function executeDelete(sfWebRequest $request)
	{
		$request->checkCSRFProtection();

		$this->forward404Unless($Socio = SocioPeer::retrieveByPk($request->getParameter('id')), sprintf('Object Socio does not exist (%s).', $request->getParameter('id')));
		$Socio->delete();

		$this->redirect('registroci/index');
	}

	protected function processForm(sfWebRequest $request, sfForm $form)
	{

		$form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));

		if ($form->isValid())
		{
			//Asociamos el operador que esta dando salida.
			$operador = $this->getUser()->getAttribute('usuario')->getId();
			$form->getObject()->setOperadorId($operador);

			if($form->getObject()->isNew())
			{
				//Seteamos el centro al que pertenece el usuario
				$criteria = new Criteria();
				$criteria->add(CentroPeer::NOMBRE, sfConfig::get('app_centro_actual'), Criteria::EQUAL);
				$form->getObject()->setCentro(CentroPeer::doSelectOne($criteria));
					
				//Agregamos el rol default del socio
				$criteria = new Criteria();
				$criteria->add(RolPeer::NOMBRE, sfConfig::get('app_rol_socios'), Criteria::EQUAL);
				$form->getObject()->setRol(RolPeer::doSelectOne($criteria));

				//Construimos un nombre de usuario válido
				$post = $request->getPostParameters();
				$nombre = $post['socio']['nombre'];
				$apepat = $post['socio']['apepat'];

				$form->getObject()->setUsuario($form->getObject()->generaUsuario($nombre, $apepat));

				//Generamos la clave.
				$form->getObject()->setClave(Comun::generaClave());
					
				sfProjectConfiguration::getActive()->getEventDispatcher()->notify(new sfEvent($this, 'socio.registrado', array(
						'socio' => $form->getObject()
				)));
			}
			else
			{
				//Si ha cambiado el nombre o el apellido, recreamos el nombre de usuario
				if(in_array(SocioPeer::NOMBRE,$form->getObject()->modifiedColumns) || in_array(SocioPeer::APEPAT,$form->getObject()->modifiedColumns))
				{

					error_log("Entra en el que detecto que el nombre fue modificado");
					//Pasamos al evento del ldap el nombre anterior para modificar el cn
					$socioAnterior = SocioPeer::retrieveByPK($form->getObject()->getId());
					$usuario_anterior = $socioAnterior->getUsuario();
					$modificaUsername = true;

					//Construimos un nombre de usuario válido
					//TODO: buscar tbien en ldap para mako.
					$form->getObject()->setUsuario($form->getObject()->generaUsuario($form->getObject()->getNombre(), $form->getObject()->getApepat()));
				}
					
				sfProjectConfiguration::getActive()->getEventDispatcher()->notify(new sfEvent($this, 'socio.actualizado', array(
						'socio' => $form->getObject(),
						'modificaUsername' => $modificaUsername,
						'usuario_anterior' => $usuario_anterior
				)));
			}

			$Socio = $form->save();

			//$this->redirect('registro/new?id='.$Socio->getId());
			$this->msg = "Datos guardados correctamente.";
			$this->redirect('registroci/index?idSocio='.$Socio->getId());

		}
	}

	protected function executeLista(sfWebRequest $request)
	{
		$this->Socios = SocioPeer::doSelect(new Criteria());
	}


	/**
	 * Ejecuta la busqueda de socios
	 * @param sfWebRequest $request
	 * @return unknown_type
	 */

	public function executeBuscaSocio(sfWebRequest $request)
	{
		$this->Socios = SocioPeer::listaSocios(NULL,$request->getParameter('qNombre'),$request->getParameter('qUsuario'),$request->getParameter('qFolio'), $request->getParameter('limit'));
		if(!count($this->Socios)){
			$this->msg = 'No se han encontrado socios coincidentes con los datos proporcionados.';
		}
		return $this->renderPartial('registroci/listaSocios',array('Socios'=>$this->Socios,'msg'=>$this->msg));
	}


	/**
	 * Acción que recibe los datos de la webcam y los almacena en el filesystem como una fotografía jpg.
	 * retornando el nombre de la fotografía.
	 * @param sfWebRequest $request
	 * @return string
	 */
	public function executeSubirFoto(sfWebRequest $request)
	{
		//TODO: Ver como eliminar las fotos que no se ocuparon
		$this->getResponse()->setContentType('text/javascript');
		$path = sfConfig::get('sf_root_dir').DIRECTORY_SEPARATOR.sfConfig::get('sf_web_dir_name').'web'.DIRECTORY_SEPARATOR.sfConfig::get('app_dir_fotos_usuarios');
		$nombre_img = "";
		$nombre_img = date('YmdHis') . '.jpg';
		$filename = $path.DIRECTORY_SEPARATOR.$nombre_img;
		$result = file_put_contents( $filename, file_get_contents('php://input') );
		if (!$result)
		{
			$msgerr="ERROR[TomaFoto]: No se pudo escribir el archivo $filename, checar permisos de escritura.";
			error_log($msgerr );
			return $this->renderText("ERROR");
		}
		return $this->renderText($nombre_img);
	}
	/**
	 * Baja de Socio
	 * @param sfWebRequest $request
	 * @return array
	 */

	public function executeBaja(sfWebRequest $request)
	{

		$this->getResponse()->setContentType('application/json');

		$Socio = SocioPeer::retrieveByPk($request->getParameter('socio_id'));
		$Socio->setActivo(0);
		$Socio->save();

		return $this->renderText(json_encode(array('msg'=>"El socio ha sido dado de baja correctamente.")));


	}

	/**
	 * Ejecuta la reposicion de la tarjeta, guarda el nuevo folio y levanta el cargo.
	 * @param sfWebRequest $request
	 * @return array
	 */
	public function executeReposicionTarjeta (sfWebRequest $request)
	{
		$this->getResponse()->setContentType('application/json');

		//Necesitamos estar seguros q la operacion se ejecuta desde caja
		$pc = ComputadoraPeer::getPcPorIp(Comun::ipReal());
		if($pc != null)
		{
			$tipo_pc = $pc->getTipoId();
		}
		else
		{
			return $this->renderText(json_encode(array('msg'=>"Esta operación debe hacerse desde la PC de Caja.")));
			exit;
		}
		if($tipo_pc != sfConfig::get('app_caja_tipo_pc_id'))
		{
			return $this->renderText(json_encode(array('msg'=>"Esta operación debe hacerse desde la PC de Caja.")));
			exit;
		}

		$Socio = SocioPeer::retrieveByPk($request->getParameter('socio_id'));
		$Socio->setFolio($request->getParameter('folio'));
		$Socio->save();

		$cajero = $this->getUser()->getAttribute('usuario');
		$this->dispatcher->notify(new sfEvent($this, 'socio.reposicion_credencial', array(
				'socio' => $Socio,
				'cajero_id' => $cajero->getId()
		)));

		return $this->renderText(json_encode(array('msg'=>"El folio ha sido cambiado correctamente.")));
	}

	/**
	 * Ejecuta la impresion de los datos del usuario.
	 * @param sfWebRequest $request
	 * @return array
	 */
	public function executeImprimirUsuarioClave (sfWebRequest $request)
	{
		$this->getResponse()->setContentType('application/json');
		$Socio = SocioPeer::retrieveByPk($request->getParameter('socio_id'));

		$this->dispatcher->notify(new sfEvent($this, 'socio.ticket.solicitado', array(
				'socio' => $Socio
		)));

		return $this->renderText(json_encode(array('msg'=>"Datos impresos correctamente.")));
	}
	/**
	 * Regresa el partial con los detalles del socio y su historial de compras y de sesiones.
	 * @param sfWebRequest $request
	 * @return unknown_type
	 */
	public function executeVerDetalle(sfWebRequest $request)
	{
		$this->Socio = SocioPeer::retrieveByPk($request->getParameter('id'));
		$Ordenes = OrdenPeer::historialComprasSocios($request->getParameter('id'));
		$Sesiones = SesionHistoricoPeer::historialSesionesSocios($request->getParameter('id'));
		$Cursos = GrupoAlumnosPeer::cursosTomadosDetalle($request->getParameter('id'));

		return $this->renderPartial('registroci/verDetalle',
				array('Socio'=>$this->Socio,
						'Ordenes' => $Ordenes,
						'Sesiones' => $Sesiones,
						'Cursos' => $Cursos));
	}


	/**
	 * Ejecuta la consulta de lista promotores y la regresa en formato json.
	 * @param sfWebRequest $request
	 * @return string
	 */
	public function executeConsultaEmpresas(sfWebRequest $request)
	{
		$this->getResponse()->setContentType('application/json');
		$empresas = EmpresaPeer::listaEmpresas($request->getParameter('q'), $request->getParameter('limit'));
		return $this->renderText(json_encode($empresas));
	}

}