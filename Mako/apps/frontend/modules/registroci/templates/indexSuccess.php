<?php use_javascript('registro/edicionSocio.js') ?>
<?php use_javascript('jquery.ui.js') ?>
<?php use_javascript('plugins/jquery.dataTables.js') ?>
<?php use_stylesheet('datatables.css') ?>


<!-- Ver detalle socio -->
<div id="verDetalleSocio" style="display: none"></div>
<!-- /Ver detalle socio -->

<script>
	//Ver detalle socio
			var $wcm= $("#verDetalleSocio").dialog(
			{
			    		modal: true,
						autoOpen: false,	    		
			    		position: ['center','center'],
			    		title:	'Ver detalle',
			    		//setter
			    		height: 550,
			    		width: 1100,
			    		buttons: {
							'Cerrar': function() 
				    		{
								$(this).dialog('close');
							}							
						}
			});


			function verDetalle(idSocio)
					{
						 var UrlVerDetalle = "<?php echo url_for('registroci/verDetalle') ?>";
							$("#verDetalleSocio").load(								 
								UrlVerDetalle,
								 {
									 id: idSocio
								 }			
								 
						); 
				
							$wcm.dialog('open');					
					};
</script>
<div class="ui-widget-content ui-corner-all sombra"
	style="padding: 5px;" id="contenedor-general">

	<?php if (isset($msg) && $msg!= ''): ?>
	<div
		class="flash_notice ts ui-state-highlight ui-corner-all sombra-delgada"
		style="float: left; padding: 15px;">
		<span style="float: left; margin-right: 0.3em;"
			class="ui-icon ui-icon-info"></span> <strong><?php echo $msg ?> </strong>
	</div>
	<?php endif; ?>

	<table width="100%" border="0">
		<tr>
			<td width="75%"><?php foreach ($Socios as $Socio): ?>
				<form>
					<div class="ui-widget">
						<div class="ui-widget-content ui-corner-all">
							<div class="ui-widget-header ui-corner-tl ui-corner-tr tsss"
								style="padding: 5px;">
								Folio:
								<?php echo $Socio->getFolio() ?>
							</div>


							<!-- BEGIN datosSocio -->
							<table width="100%" border="0">
								<tr>
									<td width="20%" rowspan="5" valign="top"><div
											class="container fotoGrande">
											<img
												src="<?php echo ($Socio->getFoto() != '')? '/fotos/'.$Socio->getFoto() : '/imgs/sin_foto_sh.jpg' ?>"
												class="main fotoGrande sombra-delgada" /> <img
												class="minor fotoGrande" src="/imgs/frame.png">
										</div></td>
									<th width="10%">Nombre:</th>
									<td><?php echo $Socio->getNombre().' '.$Socio->getApepat().' '.$Socio->getApemat() ?>
									</td>
									<th width="10%">Edad:</th>
									<td colspan="2">
										<!-- Valor reposicion credencial --> <script> var costoRepCred = '<?php echo sfConfig::get('app_costo_reposicion_tarjeta')?>';</script>
										<div id="reposicionCred<?php echo $Socio->getId() ?>"
											style="display: none">
											<label for="folio">Folio:</label> <input type="text"
												name="folio<?php echo $Socio->getId() ?>"
												id="folio<?php echo $Socio->getId() ?>"
												value="<?php echo $Socio->getFolio() ?>" maxlength="50"
												size="50" tabindex="1" filtro='numerico' requerido="1" /> <br />
											<input type="button" value="Guardar"
												onClick="
				             if ( requeridos() ) {
					            $.getJSON('<?php echo url_for('registroci/reposicionTarjeta') ?>', 
					            	{ socio_id: <?php echo $Socio->getId() ?>,
					            	folio: document.getElementById('folio<?php echo $Socio->getId() ?>').value }, 
					            	function(res){
   									 alert(res.msg);
   									 document.getElementById('reposicionCred'+<?php echo $Socio->getId() ?>).style.display ='none';
									}); 
							}" />
										</div> <?php echo $Socio->getEdad() ?> años<br />
									</td>
								</tr>
								<tr>
									<th>Usuario:</th>
									<td><?php echo $Socio->getUsuario() ?></td>
									<th>Género:</th>
									<td colspan="2"><?php echo ($Socio->getSexo()=='M') ? 'Masculino':'Femenino' ?>
									</td>
								</tr>


								<tr>
									<th>Fecha Nac:</th>
									<td><?php echo $Socio->getFechaNac("%d-%m-%Y") ?></td>
									<th>Tiempo PC:</th>
									<td colspan="2"><?php echo floor($Socio->getSaldo()/60) . " minuto(s) y " . $Socio->getSaldo() % 60;   ?>
										segundo(s) <br></td>
								</tr>
								<tr>
									<th>Fecha alta:</th>
									<td><?php echo $Socio->getCreatedAt("%d-%m-%Y") ?></td>
									<th>&nbsp;</th>
									<td colspan="2">&nbsp;</td>
								</tr>

								<tr>
									<td colspan="5"><input type="button" name="botonVerDetalle"
										id="botonVerDetalle" value="Ver detalle"
										onClick="verDetalle(<?php echo $Socio->getId() ?>)" /> <input
										type="button" name="editar" id="editar" value="Modificar"
										onclick="location.href='<?php echo url_for('registroci/edit?id='.$Socio->getId()) ?>'" />
										<input type="button" name="imprimir" id="imprimir"
										value="Imprimir usuario y clave"
										onclick="          
					            $.getJSON('<?php echo url_for('registroci/imprimirUsuarioClave') ?>', 
					            	{ socio_id: <?php echo $Socio->getId() ?>}, 
					            	function(res){
   									 alert(res.msg);   									
									}); 
							" /> <input type="button" value="Reposición Cred."
										onclick="return reposicionCredencial('<?php echo $Socio->getId() ?>', costoRepCred);" />
										<input type="button" name="button" id="button"
										value="Dar de baja"
										onclick="
				             if ( confirm('¿Está seguro que desea dar de baja este socio?') ) {
					            $.getJSON('<?php echo url_for('registro/baja') ?>', 
					            	{ socio_id: <?php echo $Socio->getId() ?> }, 
					            	function(res){
   									 alert(res.msg);
									}); 
							}" /></td>
								</tr>
							</table>
						</div>
					</div>
				</form> <!-- END datosSocio --> <?php endforeach; ?>
			</td>
			<td width="25%" valign="top">
				<form>
					<div class="ui-widget">
						<div class="ui-widget-content ui-corner-all">
							<div class="ui-widget-header ui-corner-tl ui-corner-tr tsss"
								style="padding: 5px;">Buscar socio</div>

							<table>
								<tbody>
									<tr>
										<th>Folio</th>
										<td><input type="text" name="qFolio" filtro="numerico"
											size="20" maxlength="30" id="qFolio" /></td>
									</tr>
									<tr>
										<th>Nombre usuario</th>
										<td><input type="text" name="qUsuario" size="20" id="qUsuario" />
										</td>
									</tr>
									<tr>
										<th>Nombre completo</th>
										<td><input type="text" name="qNombre" filtro="alfabetico"
											size="20" id="qNombre" /></td>
									</tr>
									<tr>
										<th>Buscar</th>
										<td><input type="submit" id="boton-buscar-socios"
											value="Buscar socio" onClick="return caracteresBusqueda()" />
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</form>
			</td>
		</tr>
	</table>

</div>
