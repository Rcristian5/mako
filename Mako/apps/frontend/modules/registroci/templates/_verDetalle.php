<script>

	$(function() {
		var $tabsvd = $('#tabsVerDetalle').tabs();
	
		$("#tabsVerDetalle").tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
		$("#tabsVerDetalle li").removeClass('ui-corner-top').addClass('ui-corner-left');
	
	});
	
	
	var oTable;
	$(document).ready(function() {
	
	   oTable = $('.registros').dataTable({
			"bPaginate": true,
			"bLengthChange": true,
			"bFilter": true,
			"bSort": true,
			"bInfo": true,
			"bAutoWidth": false,
	        "bJQueryUI": true,
	        "sPaginationType": "full_numbers"
	     });
	   
	});
</script>


<?php use_stylesheet('./registroci/_verDetalle.css')?>


<div id="contenedorVerDetalle">
	<div id="tabsVerDetalle">

		<ul id="tabMenu-vd">
			<li><a href="#personales-vd">Datos personales</a></li>
			<li><a href="#direccion-vd">Dirección</a></li>
			<li><a href="#datosFiscales-vd">Datos fiscales</a></li>
			<li><a href="#complementarios-vd">Datos complementarios</a></li>
			<li><a href="#historial_compras-vd">Historial de compras</a></li>
			<li><a href="#historial_sesiones-vd">Historial Tiempo PC</a></li>
			<li><a href="#historial_cursos-vd">Historial Cursos</a></li>
		</ul>


		<div id="personales-vd" style="width: 80%;">
			<h3>
				<?php echo $Socio->getNombre().' '.$Socio->getApepat().' '.$Socio->getApemat() ?>
			</h3>


			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="15%"><div class="container fotoGrande">
							<img
								src="<?php echo ($Socio->getFoto() != '')? '/fotos/'.$Socio->getFoto() : '/imgs/sin_foto_sh.jpg' ?>"
								class="main fotoGrande" /> <img class="minor fotoGrande"
								src="/imgs/frame.png">
						</div></td>
					<td width="20%"><strong>Folio: <br /> Usuario: <br /> Edad: <br />
							Sexo: <br /> Fecha nac: <br /> <br /> Centro alta: <br /> Rol:
					</strong></td>
					<td width="69%"><?php echo $Socio->getFolio() ?><br> <?php echo $Socio->getUsuario() ?><br>
						<?php echo $Socio->getEdad() ?> años <br> <?php echo ($Socio->getSexo()=='M') ? 'Masculino':'Femenino' ?><br>
						<?php echo $Socio->getFechaNac("%d-%m-%Y") ?><br> <br> <?php echo $Socio->getCentro()->getNombre() ?><br>
						<?php echo $Socio->getRol() ?><br></td>
				</tr>
				<tr>
					<td><strong>Teléfono casa:<br> Celular: <br> Email: <br> <br>
							Tiempo PC: Vigente: <br> Operador: <br> Activo:<br> Fecha alta:<br>
							Fecha ultima modificación:
					</strong></td>
					<td colspan="2"><br> <?php echo $Socio->getTel() ?><br> <?php echo $Socio->getCelular() ?><br>
						<?php echo $Socio->getEmail() ?><br> <br> <?php echo floor($Socio->getSaldo()/60) . " minuto(s) y " . $Socio->getSaldo() % 60;   ?>
						segundo(s) <br> <?php echo ($Socio->getVigente()=='1') ? 'Si':'No' ?><br>
						<?php echo $Socio->getUsuarioRelatedByOperadorId() ?><br> <?php echo ($Socio->getActivo()=='1') ? 'Si':'No' ?><br>
						<?php echo $Socio->getCreatedAt("%d-%m-%Y") ?><br> <?php echo $Socio->getUpdatedAt("%d-%m-%Y") ?><br>
						<br>
					</td>
				</tr>
			</table>

		</div>

		<div id="direccion-vd">
			<h3>Direccion</h3>
			<label>Estado:</label>
			<?php echo $Socio->getEstado() ?>
			<br /> <label>Municipio:</label>
			<?php echo $Socio->getMunicipio() ?>
			<br /> <label>Colonia:</label>
			<?php echo $Socio->getColonia() ?>
			<br /> <label>Cp:</label>
			<?php echo $Socio->getCp() ?>
			<br /> <label>Calle:</label>
			<?php echo $Socio->getCalle() ?>
			<br /> <label>Num ext:</label>
			<?php echo $Socio->getNumExt() ?>
			<br /> <label>Num int:</label>
			<?php echo $Socio->getNumInt() ?>
			<br />
		</div>

		<div id="datosFiscales-vd">
			<h3>Datos Fiscales</h3>
			<label>Razon social:</label>
			<?php echo $Socio->getRazonSocial() ?>
			<br /> <label>RFC:</label>
			<?php echo $Socio->getRfc() ?>
			<br /> <label>Calle:</label>
			<?php echo $Socio->getCalleFiscal() ?>
			<br /> <label>Colonia:</label>
			<?php echo $Socio->getColoniaFiscal() ?>
			<br /> <label>CP:</label>
			<?php echo $Socio->getCpFiscal() ?>
			<br /> <label>Estado:</label>
			<?php echo $Socio->getEstadoFiscal() ?>
			<br /> <label>Municipio:</label>
			<?php echo $Socio->getMunicipioFiscal() ?>
			<br /> <label>Num ext:</label>
			<?php echo $Socio->getNumExtFiscal() ?>
			<br /> <label>Num int:</label>
			<?php echo $Socio->getNumIntFiscal() ?>
			<br /> <label>Pais:</label>
			<?php echo $Socio->getPaisFiscal() ?>
			<br />
		</div>



		<div id="complementarios-vd">
			<h3>Datos complementarios</h3>
			<label>Salud:</label>
			<?php echo $Socio->getDiscapacidad() ?>
			<br /> <label>Escolaridad:</label>
			<?php echo $Socio->getNivelEstudio() ?>
			<br /> <label>Empresa referente:</label>
			<?php echo $Socio->getEmpresaRelatedByEmpresa() ?>
			<br /> <label>Parentesco con Socio:</label>
			<?php //echo $Socio->getSocioRelatedBySocioRefId() ?>
			<br /> <label>Parentesco:</label>
			<?php //echo $Socio->getParentesco() ?>
			<br />


			<h3>Contacto en caso de emergencia</h3>

			<label>Nombre:</label>
			<?php echo $Socio->getErNombre() . ' ' . $Socio->getErApepat() . ' ' . $Socio->getErApemat()?>
			<br /> <label>Fecha nac:</label>
			<?php echo $Socio->getErFechaNac("%d-%m-%Y") ?>
			<br /> <label>Sexo:</label>
			<?php echo ($Socio->getErSexo()=='M') ? 'Masculino':'Femenino' ?>
			<br /> <br /> <label>Telefono casa:</label>
			<?php echo $Socio->getErTel() ?>
			<br /> <label>Celular:</label>
			<?php echo $Socio->getErCelular() ?>
			<br /> <label>Email:</label>
			<?php echo $Socio->getErEmail() ?>
			<br> <br>

		</div>


		<div id="historial_compras-vd">
			<h3>Historial de compras</h3>
			<table width="850" border="0" id="registrosCompras" class="registros">
				<thead>
					<tr>
						<th>Folio</th>
						<th>Tipo de orden</th>
						<th>Comprobante</th>
						<th>Producto</th>
						<th>Fecha</th>
						<th>Cant.</th>
						<th>Unidad</th>
						<th>Precio Unitario</th>
						<th>Descuento</th>
						<th>Total</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($Ordenes as $orden)
					{
						foreach($orden->getDetalleOrdens() as $item)
		{?>
					<!-- BEGIN carrito -->
					<tr>
						<td><?php echo sprintf("%010s", $orden->getFolio()); ?></td>
						<td><?php echo $item->getOrden()->getTipoOrden()->getNombre(); ?>
						</td>
						<td><?php echo ($orden->getEsFactura()=='1') ? 'Factura':'Ticket'  ?>
						</td>
						<td><?php echo $item->getProducto()->getNombre() ?></td>
						<td><?php echo $orden->getCreatedAt("%d-%m-%Y") ?></td>
						<td align="right"><?php echo  number_format($item->getCantidad(), 0, ',', ' '); ?>
						</td>
						<td><?php echo $item->getProducto()->getUnidadProducto()->getNombre()?>
						</td>
						<td align="right">$ <?php echo $item->getPrecioLista() ?>
						</td>
						<td align="right"><?php  echo  number_format($item->getDescuento(), 2, ',', ' '); ?>
							%</td>
						<td align="right" nowrap="nowrap">$ <?php echo $item->getSubtotal() ?>
						</td>
					</tr>
					<!-- END carrito -->

					<?php } 
} ?>
				</tbody>
			</table>
		</div>



		<div id="historial_sesiones-vd">
			<h3>Historial Tiempo PC</h3>
			<table width="850" border="0" id="registrosSesiones"
				class="registros">
				<thead>
					<tr>
						<th>Fecha inicio</th>
						<th>Fecha fin</th>
						<th>Tiempo PC utilizado</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($Sesiones as $sesion)
	{?>
					<!-- BEGIN carrito -->
					<tr>
						<td><?php echo sprintf("%010s", $sesion->getFechaLogin()); ?></td>
						<td><?php echo sprintf("%010s", $sesion->getFechaLogout()); ?></td>
						<td><?php echo floor($sesion->getOcupados()/60) . " minuto(s) y " . $sesion->getOcupados() % 60;   ?>
							segundo(s)</td>
					</tr>
					<!-- END carrito -->

					<?php } ?>
				</tbody>
			</table>
		</div>



		<div id="historial_cursos-vd">
			<h3>Historial Cursos</h3>
			<table width="850" border="0" id="registrosCursos" class="registros">
				<thead>
					<tr>
						<th>Clave de grupo</th>
						<th>Horario</th>
						<th>Nombre</th>
						<th>Clave</th>
						<th>Categoria</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					setlocale(LC_TIME,'es_MX.UTF-8');
					foreach($Cursos as $curso)
					{

						?>
					<!-- BEGIN cursos -->
					<tr>
						<td><?php echo $curso->getGrupo()->getClave()?></td>
						<td><?php foreach ($curso->getGrupo()->getHorario()->getDiass() as $sepa){
							echo strftime('%a %d de %b', strtotime($sepa->getDia())) . ' de '.strftime('%H:%S',strtotime($sepa->getHoraInicio())) . ' a ' . strftime('%H:%S', strtotime($sepa->getHoraFin())) .'<br>';
							//echo strftime('%a %d %b', strtotime($sepa->getDia()));
						}?>
						</td>
						<td><?php echo $curso->getGrupo()->getCurso()->getNombre() ?></td>
						<td><?php echo $curso->getGrupo()->getCurso()->getClave()  ?>
						</td>
						<td><?php echo $curso->getGrupo()->getCurso()->getCategoriaCurso()    ?>
						</td>
					</tr>
					<!-- END cursos -->

					<?php } ?>
				</tbody>
			</table>
		</div>



	</div>
</div>


