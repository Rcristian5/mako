<?php
//Time set para mexico
date_default_timezone_set('America/Mexico_City');
setlocale (LC_TIME, 'es_ES.UTF-8');
?>

<?php if(!isset($id_buscador)) $id_buscador = 'ctx-'.rand(100,999) ?>

<form action="<?php echo url_for('registroci/buscaSocio') ?>">

	<table>
		<tr>
			<td>
				<fieldset class="forma-buscador-socios"
					id="buscador-socios-<?php echo $id_buscador ?>">
					<legend>Buscar</legend>
					<table>
						<tbody>
							<tr>
								<th>Folio</th>
								<td><input type="text" name="qFolio" filtro="numerico" size="20"
									maxlength="30" id="qFolio-<?php echo $id_buscador ?>" 
									onkeyup="validaCampo(this,event)" /></td>
							</tr>
							<tr>
								<th>Nombre usuario</th>
								<td><input type="text" name="qUsuario" size="20"
									id="qUsuario-<?php echo $id_buscador ?>" 
									onkeyup="validaCampo(this,event)" /></td>
							</tr>
							<tr>
								<th>Nombre completo</th>
								<td><input type="text" name="qNombre" filtro="alfabetico"
									size="20" id="qNombre-<?php echo $id_buscador ?>" 
									onkeyup="validaCampo(this,event)" /></td>
							</tr>
							<tr>
								<th>Buscar</th>
								<td><input type="button"
									id="boton-buscar-socios-<?php echo $id_buscador ?>"
									value="Buscar socio"
									onclick="
							$('#buscador-socios-<?php echo $id_buscador ?>').trigger('socios.buscador.buscando');
							$('#resultados-buscador-socios-<?php echo $id_buscador ?>').load(
							 $(this).parents('form').attr('action'),
							 {
								 'qNombre': $('#qNombre-<?php echo $id_buscador ?>').val(),
								 'qUsuario': $('#qUsuario-<?php echo $id_buscador ?>').val(),
								 'qFolio': $('#qFolio-<?php echo $id_buscador ?>').val()
							 },
							 function(v1,v2){$('#buscador-socios-<?php echo $id_buscador ?>').trigger('socios.buscador.encontrados');}
						);
						" />
								</td>
							</tr>
						</tbody>
					</table>
				</fieldset>

				<hr />

				<div id="resultados-buscador-socios-<?php echo $id_buscador ?>"
					class="resultados-buscador-socios">
					<?php include_partial('registroci/listaSocios', array('Socios' => $Socios,'msg'=>$msg,'id_buscador'=>$id_buscador)) ?>
				</div>
			</td>
		</tr>
	</table>
</form>
