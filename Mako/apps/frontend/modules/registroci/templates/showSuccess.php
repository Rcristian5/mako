<table>
	<tbody>
		<tr>
			<th>Id:</th>
			<td><?php echo $Socio->getId() ?>
			</td>
		</tr>
		<tr>
			<th>Centro:</th>
			<td><?php echo $Socio->getCentroId() ?>
			</td>
		</tr>
		<tr>
			<th>Rol:</th>
			<td><?php echo $Socio->getRolId() ?>
			</td>
		</tr>
		<tr>
			<th>Usuario:</th>
			<td><?php echo $Socio->getUsuario() ?>
			</td>
		</tr>
		<tr>
			<th>Clave:</th>
			<td><?php echo $Socio->getClave() ?>
			</td>
		</tr>
		<tr>
			<th>Folio:</th>
			<td><?php echo $Socio->getFolio() ?>
			</td>
		</tr>
		<tr>
			<th>Nombre:</th>
			<td><?php echo $Socio->getNombre() ?>
			</td>
		</tr>
		<tr>
			<th>Apepat:</th>
			<td><?php echo $Socio->getApepat() ?>
			</td>
		</tr>
		<tr>
			<th>Apemat:</th>
			<td><?php echo $Socio->getApemat() ?>
			</td>
		</tr>
		<tr>
			<th>Nombre completo:</th>
			<td><?php echo $Socio->getNombreCompleto() ?>
			</td>
		</tr>
		<tr>
			<th>Fecha nac:</th>
			<td><?php echo $Socio->getFechaNac() ?>
			</td>
		</tr>
		<tr>
			<th>Sexo:</th>
			<td><?php echo $Socio->getSexo() ?>
			</td>
		</tr>
		<tr>
			<th>Foto:</th>
			<td><?php echo $Socio->getFoto() ?>
			</td>
		</tr>
		<tr>
			<th>Huella:</th>
			<td><?php echo $Socio->getHuella() ?>
			</td>
		</tr>
		<tr>
			<th>Email:</th>
			<td><?php echo $Socio->getEmail() ?>
			</td>
		</tr>
		<tr>
			<th>Tel:</th>
			<td><?php echo $Socio->getTel() ?>
			</td>
		</tr>
		<tr>
			<th>Celular:</th>
			<td><?php echo $Socio->getCelular() ?>
			</td>
		</tr>
		<tr>
			<th>Id estado:</th>
			<td><?php echo $Socio->getIdEstado() ?>
			</td>
		</tr>
		<tr>
			<th>Estado:</th>
			<td><?php echo $Socio->getEstado() ?>
			</td>
		</tr>
		<tr>
			<th>Ciudad:</th>
			<td><?php echo $Socio->getCiudad() ?>
			</td>
		</tr>
		<tr>
			<th>Id del:</th>
			<td><?php echo $Socio->getIdDel() ?>
			</td>
		</tr>
		<tr>
			<th>Municipio:</th>
			<td><?php echo $Socio->getMunicipio() ?>
			</td>
		</tr>
		<tr>
			<th>Id municipio:</th>
			<td><?php echo $Socio->getIdMunicipio() ?>
			</td>
		</tr>
		<tr>
			<th>Colonia:</th>
			<td><?php echo $Socio->getColonia() ?>
			</td>
		</tr>
		<tr>
			<th>Id asentamiento:</th>
			<td><?php echo $Socio->getIdAsentamiento() ?>
			</td>
		</tr>
		<tr>
			<th>Cp:</th>
			<td><?php echo $Socio->getCp() ?>
			</td>
		</tr>
		<tr>
			<th>Calle:</th>
			<td><?php echo $Socio->getCalle() ?>
			</td>
		</tr>
		<tr>
			<th>Num ext:</th>
			<td><?php echo $Socio->getNumExt() ?>
			</td>
		</tr>
		<tr>
			<th>Num int:</th>
			<td><?php echo $Socio->getNumInt() ?>
			</td>
		</tr>
		<tr>
			<th>Razon social:</th>
			<td><?php echo $Socio->getRazonSocial() ?>
			</td>
		</tr>
		<tr>
			<th>Rfc:</th>
			<td><?php echo $Socio->getRfc() ?>
			</td>
		</tr>
		<tr>
			<th>Telefono fiscal:</th>
			<td><?php echo $Socio->getTelefonoFiscal() ?>
			</td>
		</tr>
		<tr>
			<th>Calle fiscal:</th>
			<td><?php echo $Socio->getCalleFiscal() ?>
			</td>
		</tr>
		<tr>
			<th>Colonia fiscal:</th>
			<td><?php echo $Socio->getColoniaFiscal() ?>
			</td>
		</tr>
		<tr>
			<th>Cp fiscal:</th>
			<td><?php echo $Socio->getCpFiscal() ?>
			</td>
		</tr>
		<tr>
			<th>Ciudad fiscal:</th>
			<td><?php echo $Socio->getCiudadFiscal() ?>
			</td>
		</tr>
		<tr>
			<th>Estado fiscal:</th>
			<td><?php echo $Socio->getEstadoFiscal() ?>
			</td>
		</tr>
		<tr>
			<th>Municipio fiscal:</th>
			<td><?php echo $Socio->getMunicipioFiscal() ?>
			</td>
		</tr>
		<tr>
			<th>Num ext fiscal:</th>
			<td><?php echo $Socio->getNumExtFiscal() ?>
			</td>
		</tr>
		<tr>
			<th>Num int fiscal:</th>
			<td><?php echo $Socio->getNumIntFiscal() ?>
			</td>
		</tr>
		<tr>
			<th>Pais fiscal:</th>
			<td><?php echo $Socio->getPaisFiscal() ?>
			</td>
		</tr>
		<tr>
			<th>Empresa:</th>
			<td><?php echo $Socio->getEmpresa() ?>
			</td>
		</tr>
		<tr>
			<th>Canal contacto:</th>
			<td><?php echo $Socio->getCanalContactoId() ?>
			</td>
		</tr>
		<tr>
			<th>Liga:</th>
			<td><?php echo $Socio->getLiga() ?>
			</td>
		</tr>
		<tr>
			<th>Recomienda:</th>
			<td><?php echo $Socio->getRecomiendaId() ?>
			</td>
		</tr>
		<tr>
			<th>Otros contacto:</th>
			<td><?php echo $Socio->getOtrosContacto() ?>
			</td>
		</tr>
		<tr>
			<th>Socio ref:</th>
			<td><?php echo $Socio->getSocioRefId() ?>
			</td>
		</tr>
		<tr>
			<th>Promotor ref:</th>
			<td><?php echo $Socio->getPromotorRefId() ?>
			</td>
		</tr>
		<tr>
			<th>Discapacidad:</th>
			<td><?php echo $Socio->getDiscapacidadId() ?>
			</td>
		</tr>
		<tr>
			<th>Habilidad informatica:</th>
			<td><?php echo $Socio->getHabilidadInformaticaId() ?>
			</td>
		</tr>
		<tr>
			<th>Dominio inglés:</th>
			<td><?php echo $Socio->getDominioInglesId() ?>
			</td>
		</tr>
		<tr>
			<th>Estudia:</th>
			<td><?php echo $Socio->getEstudia() ?>
			</td>
		</tr>
		<tr>
			<th>Nivel:</th>
			<td><?php echo $Socio->getNivelId() ?>
			</td>
		</tr>
		<tr>
			<th>Profesion actual:</th>
			<td><?php echo $Socio->getProfesionActualId() ?>
			</td>
		</tr>
		<tr>
			<th>Estudio actual:</th>
			<td><?php echo $Socio->getEstudioActualId() ?>
			</td>
		</tr>
		<tr>
			<th>Otro estudio actual:</th>
			<td><?php echo $Socio->getOtroEstudioActual() ?>
			</td>
		</tr>
		<tr>
			<th>Tipo escuela:</th>
			<td><?php echo $Socio->getTipoEscuelaId() ?>
			</td>
		</tr>
		<tr>
			<th>Beca:</th>
			<td><?php echo $Socio->getBeca() ?>
			</td>
		</tr>
		<tr>
			<th>Tipo estudio:</th>
			<td><?php echo $Socio->getTipoEstudioId() ?>
			</td>
		</tr>
		<tr>
			<th>Grado:</th>
			<td><?php echo $Socio->getGradoId() ?>
			</td>
		</tr>
		<tr>
			<th>Profesion ultimo:</th>
			<td><?php echo $Socio->getProfesionUltimoId() ?>
			</td>
		</tr>
		<tr>
			<th>Estudio ultimo:</th>
			<td><?php echo $Socio->getEstudioUltimoId() ?>
			</td>
		</tr>
		<tr>
			<th>Otro estudio ultimo:</th>
			<td><?php echo $Socio->getOtroEstudioUltimo() ?>
			</td>
		</tr>
		<tr>
			<th>Trabaja:</th>
			<td><?php echo $Socio->getTrabaja() ?>
			</td>
		</tr>
		<tr>
			<th>Organizacion:</th>
			<td><?php echo $Socio->getOrganizacionId() ?>
			</td>
		</tr>
		<tr>
			<th>Sector:</th>
			<td><?php echo $Socio->getSectorId() ?>
			</td>
		</tr>
		<tr>
			<th>Posicion:</th>
			<td><?php echo $Socio->getPosicionId() ?>
			</td>
		</tr>
		<tr>
			<th>Ocupacion:</th>
			<td><?php echo $Socio->getOcupacionId() ?>
			</td>
		</tr>
		<tr>
			<th>Observaciones:</th>
			<td><?php echo $Socio->getObservaciones() ?>
			</td>
		</tr>
		<tr>
			<th>Saldo:</th>
			<td><?php echo $Socio->getSaldo() ?>
			</td>
		</tr>
		<tr>
			<th>Vigente:</th>
			<td><?php echo $Socio->getVigente() ?>
			</td>
		</tr>
		<tr>
			<th>Operador:</th>
			<td><?php echo $Socio->getOperadorId() ?>
			</td>
		</tr>
		<tr>
			<th>Activo:</th>
			<td><?php echo $Socio->getActivo() ?>
			</td>
		</tr>
		<tr>
			<th>Created at:</th>
			<td><?php echo $Socio->getCreatedAt() ?>
			</td>
		</tr>
		<tr>
			<th>Updated at:</th>
			<td><?php echo $Socio->getUpdatedAt() ?>
			</td>
		</tr>
	</tbody>
</table>

<hr />

<a href="<?php echo url_for('registroci/edit?id='.$Socio->getId()) ?>">Edit</a>
&nbsp;
<a href="<?php echo url_for('registroci/index') ?>">List</a>
