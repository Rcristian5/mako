<?php use_javascript('registro/mapas.js') ?>
<input
	type="hidden" name="centro_lat" id="centro_lat"
	value="<?php echo sfConfig::get('app_centro_lat') ?>">
<input
	type="hidden" name="centro_long" id="centro_long"
	value="<?php echo sfConfig::get('app_centro_long') ?>">
<input
	type="hidden" name="socio_lat" id="socio_lat"
	value="<?php echo $Socio->getLat() ?>">
<input
	type="hidden" name="socio_long" id="socio_long"
	value="<?php echo $Socio->getLong() ?>">

<div id="map_canvas"
	class="sombra-delgada"></div>



<script
	src="http://maps.google.com/maps?file=api&amp;v=2.x&amp;key=<?php echo sfConfig::get('app_gmaps_key');?>"
	type="text/javascript"></script>
<script
	src="http://www.google.com/uds/api?file=uds.js&amp;v=1.0"
	type="text/javascript"></script>
<script
	src="http://www.google.com/uds/solutions/mapsearch/gsmapsearch.js?mode=new"
	type="text/javascript"></script>
