<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<?php use_javascript('registroci/validaSocio.js') ?>
<?php use_javascript('jquery.ui.js') ?>
<?php use_javascript('plugins/jquery.autocompleter.js') ?>
<?php use_stylesheet('tabs.css') ?>
<?php use_javascript('JSON.js') ?>
<?php use_javascript('debug.js') ?>
<?php use_javascript('registro/mapas.js') ?>

<?php use_stylesheet('./registroci/editSuccess.css') ?>

<!-- Busca socio -->
<div id="buscadorSocio" style="display: none">
	<?php include_component('registro', 'buscaSocios') ?>
</div>
<!-- /Busca socio -->


<!-- Busca homonimo -->
<div id="buscadorHomonimo" style="display: none">

	<div id="msgHomonimo"
		class="flash_ok ui-state-highlight ts sombra-delgada ui-corner-all"
		style="display: none; padding: 5px; margin-top: 10px; margin-bottom: 10px;">
		<span style="float: left; padding-right: 0.3em;"
			class="ui-icon ui-icon-info"></span> <strong>Se han detectado
			coincidencias con socio(s) registrado(s) anteriormente.<br> Por favor
			valide antes de registrar al socio.
		</strong>
	</div>

	<div id="msgFolioDuplicado"
		class="flash_ok ui-state-highlight ts sombra-delgada ui-corner-all"
		style="display: none; padding: 5px; margin-top: 10px; margin-bottom: 10px;">
		<span style="float: left; pading-right: 0.3em;"
			class="ui-icon ui-icon-info"></span> <strong>Se han detectado
			coincidencias con folio(s) registrado(s) anteriormente.<br> Por favor
			valide antes de registrar al socio.
		</strong>
	</div>

	<div id="buscadorHomonimoLista"></div>
</div>
<!-- /Busca homonimo -->

<!-- Ver detalle socio -->
<div id="verDetalleSocio"
	style="display: none"></div>
<!-- /Ver detalle socio -->


<!-- Alta Empresa -->
<div id="verAltaEmpresa"
	style="display: none"></div>
<!-- /Alta Empresa -->


<form name="registro"
	action="<?php echo url_for('registroci/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>"
	method="post"
	<?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
	<?php if (!$form->getObject()->isNew()): ?>
	<input type="hidden" name="sf_method" value="put" />
	<?php endif; ?>

	<script type="text/javascript">
	$(function() {
		//$("#tabs").tabs();
		var $tabs = $('#tabs').tabs();
		var next=0;
		var i = 0;
	
		$(".ui-tabs-panel").each(function(i){

		  var totalSize = $(".ui-tabs-panel").size() - 1;
		  if (i != totalSize) {
				
		      next = i+1;
	   		  $(this).append("<a href='#' class='next-tab mover' rel='" + next + "'>Siguiente &#187;</a>");
		  }

		  if (i != 0) {
		      prev = i-1;
	   		  $(this).append("<a href='#' class='prev-tab mover' rel='" + prev + "'>&#171; Anterior</a>");
		  }
			i++;
		});

		$('.next-tab, .prev-tab').click(function() {
				//alert($(this).attr("rel"));
			   $tabs.tabs('select', Number($(this).attr("rel")));
	           return false;
	    });

		//Buscador Socio
		var $wcm= $("#buscadorSocio").dialog(
			{
			    		modal: true,
						autoOpen: false,	    		
			    		position: ['center','center'],
			    		title:	'Buscador de socio',
			    		//setter
			    		height: 530,
			    		width: 300,
			    		buttons: {
							'Cerrar': function() 
				    		{
								$(this).dialog('close');
							},
							'Cancelar': function() 
				    		{
								$(this).dialog('close');
							}
							
						}
			});

			$("#boton-abre-ventana-socio").click(function()
			{
				$wcm.dialog('open');
			});


			$("#buscadorSocio").bind('socio.seleccionado',function(ev, socio_ref_id, socio_ref_nombre){

					document.getElementById('socio_socio_ref_id').value = socio_ref_id;
					document.getElementById('socio_ref_nombre').innerHTML = socio_ref_nombre;
	
					$wcm.dialog('close');
			});
			//
			//Homonimo
			var $wcmHom= $("#buscadorHomonimo").dialog(
			{
			    		modal: true,
						autoOpen: false,	    		
			    		position: ['center','center'],
			    		//title:	'Homónimos detectados',
			    		//setter
			    		height: 530,
			    		width: 300,
			    		buttons: {
							'Cerrar': function() 
				    		{
								$(this).dialog('close');
							},
							'Cancelar': function() 
				    		{
								$(this).dialog('close');
							}
							
						}
			});

			$('#socio_apemat').blur(function()
			{
				 var UrlListaHomonimos = "<?php echo url_for('registro/buscaSocio') ?>";
				 var nombreCompleto = document.getElementById('socio_nombre').value + ' ' + document.getElementById('socio_apepat').value + ' ' + document.getElementById('socio_apemat').value;
				 var titulo = 'Homonimo detectado';
				 var texto ='Se han detectado coincidencias con socio(s) registrado(s) anteriormente.<br> Por favor valide antes de registrar al socio.';
					$("#buscadorHomonimoLista").load(
					 
						 UrlListaHomonimos,
						 {
							 qNombre: nombreCompleto
						 },				 
						 function(v1,v2){$("#buscadorHomonimo").trigger('socio.homonimos.encontrados',[titulo,texto]);}
						 
				); 
			
			});

			//Escuchamos evento socio.homonimos.encontrados y mostramos la alerta en caso de q haya encontrado algo
			$("#buscadorHomonimo").bind('socio.homonimos.encontrados',function(ev, titulo, texto){
				
				$("#buscadorHomonimo a").text('Ver detalle');				
				var encontrados = $("#buscadorHomonimo a").length;
				if(encontrados > 0){
					$("#msgHomonimo").show();
					$("#msgFolioDuplicado").hide();
					$wcmHom.dialog('open');
					$wcmHom.dialog( 'option' , 'title' , titulo);
					
				}
			});
			
			$("#buscadorHomonimo").bind('socio.seleccionado',function(ev, socio_ref_id, socio_ref_nombre){
				verDetalle(socio_ref_id);
			});

			//Ver detalle socio
			var $wcmDetalle= $("#verDetalleSocio").dialog(
			{
			    		modal: true,
						autoOpen: false,	    		
			    		position: ['center','center'],
			    		title:	'Ver detalle',
			    		//setter
			    		height: 550,
			    		width: 1100,
			    		buttons: {
							'Cerrar': function() 
				    		{
								$(this).dialog('close');
							}							
						}
			});


			function verDetalle(idSocio)
					{
						 var UrlVerDetalle = "<?php echo url_for('registro/verDetalle') ?>";
							$("#verDetalleSocio").load(								 
								UrlVerDetalle,
								 {
									 id: idSocio
								 }			
								 
						); 
							$wcmDetalle.dialog('open');					
					};
			//

			//Verifica Folio
			$('#socio_folio').blur(function()
			{
			 var UrlVerificaFolio = "<?php echo url_for('registro/buscaSocio') ?>";
			 var folioV = document.getElementById('socio_folio').value;
					$("#buscadorHomonimoLista").load(
								 
						 UrlVerificaFolio,
						 {
							 qFolio: folioV
						 },
						 function(v1,v2){$("#buscadorHomonimo").trigger('socio.folio.duplicado');}
								 
					); 
				
			});	

			//Escuchamos evento del folio verificado, socio.seleccionado
			$("#buscadorHomonimo").bind('socio.folio.duplicado',function(ev)
			{
				var encontrados = $("#buscadorHomonimo a").length;
				if(encontrados > 0){
					document.getElementById('socio_folio').value = '';
					var titulo = 'Folio duplicado';
					var texto = '';
					$("#msgHomonimo").hide();
					$("#msgFolioDuplicado").show();
					$wcmHom.dialog('open');
					$wcmHom.dialog( 'option' , 'title' , titulo);
				}
			});	



			//Dar de alta empresa
			var $wcmAltaEmpresa= $("#verAltaEmpresa").dialog(
			{
			    		modal: true,
						autoOpen: false,	    		
			    		position: ['center','center'],
			    		title:	'Alta Empresa',
			    		//setter
			    		height: 580,
			    		width: 750,
			    		buttons: {
							'Cerrar': function() 
				    		{
								$(this).dialog('close');
							}, 
							'Cancelar': function() 
				    		{
								$(this).dialog('close');
							}							
						}
			});
			

			$("#boton-abre-ventana-empresa").click(function()
			{
				 var UrlAltaEmpresa = "<?php echo url_for('empresa/altaEmpresa') ?>";
				 	$("#verAltaEmpresa").load(				 
						 UrlAltaEmpresa
					); 
				$wcmAltaEmpresa.dialog('open');	
			});
			
			$("#verAltaEmpresa").bind('empresa.seleccionada',function(ev){
				$wcmAltaEmpresa.dialog('close');
		});
	});
	

	
	</script>

	<div id="page-wrap" class="sombra">

		<div id="tabs">

			<ul id="tabMenu" class="tsss">
				<li><a href="#personales">Datos personales</a></li>
				<li><a href="#complementarios">Datos complementarios</a></li>
			</ul>


			<div id="personales" class="ui-tabs-panel">

				<h3>Identificación</h3>
				<div>
					<?php include_partial('registroci/capturaFoto') ?>
					<?php include_partial('registroci/capturaHuella') ?>
					<p>
						<?php echo $form['nombre']->renderLabel() ?>
						<?php echo $form['nombre']->render(array('requerido'=>'1','filtro'=>'alfabetico','size'=>'50','maxlength'=>'30')) ?>
						<?php echo $form['nombre']->renderError() ?>
						<br />

						<?php echo $form['apepat']->renderLabel("Apellido Paterno") ?>
						<?php echo $form['apepat']->render(array('requerido'=>'1','filtro'=>'alfabetico','size'=>'50','maxlength'=>'30')) ?>
						<?php echo $form['apepat']->renderError() ?>
						<br />

						<?php echo $form['apemat']->renderLabel("Apellido Materno") ?>
						<?php echo $form['apemat']->render(array('filtro'=>'alfabetico','size'=>'50','maxlength'=>'30')) ?>
						<?php echo $form['apemat']->renderError() ?>
						<br />

						<?php echo $form['fecha_nac']->renderLabel("Fecha de Nacimiento") ?>
						<?php echo $form['fecha_nac']->render(array('requerido'=>'1')) ?>
						<?php echo $form['fecha_nac']->renderError() ?>
						<br />

						<?php echo $form['sexo']->renderLabel("Género") ?>
						<?php echo $form['sexo']->render(array('requerido'=>'1')) ?>
						<?php echo $form['sexo']->renderError() ?>
						<br />

					</p>
				</div>
				<h3>Contacto</h3>
				<br>
				<?php echo $form['tel']->renderLabel("Teléfono casa") ?>
				<?php echo $form['tel']->render(array('filtro'=>'numerico', 'maxlength'=>'12')) ?>
				<?php echo $form['tel']->renderError() ?>
				<br />

				<?php echo $form['celular']->renderLabel("Teléfono celular") ?>
				<?php echo $form['celular']->render(array('filtro'=>'numerico', 'maxlength'=>'11')) ?>
				<?php echo $form['celular']->renderError() ?>
				<br />

				<?php echo $form['email']->renderLabel() ?>
				<?php echo $form['email']->render(array('nouppercase'=>'1')) ?>
				<?php echo $form['email']->renderError() ?>

				<h3>Domicilio</h3>
				<div>
					<div class="der">
						<br>
						<?php echo $form['id_estado']->renderLabel("Estado") ?>
						<?php echo $form['id_estado']->render() ?>
						<?php echo $form['id_estado']->renderError() ?>
						<br />

						<?php echo $form['municipio']->renderLabel() ?>
						<?php echo $form['municipio']->render() ?>
						<?php echo $form['municipio']->renderError() ?>
						<br />

						<?php echo $form['colonia']->renderLabel() ?>
						<?php echo $form['colonia']->render() ?>
						<?php echo $form['colonia']->renderError() ?>
						<br />

						<?php echo $form['cp']->renderLabel("C.P.") ?>
						<?php echo $form['cp']->render(array('filtro'=>'numerico','size'=>'5','maxlength'=>'5')) ?>
						<?php echo $form['cp']->renderError() ?>
						<br />

						<?php echo $form['calle']->renderLabel() ?>
						<?php echo $form['calle']->render() ?>
						<?php echo $form['calle']->renderError() ?>
						<br />

						<?php echo $form['num_ext']->renderLabel("Número exterior") ?>
						<?php echo $form['num_ext']->render() ?>
						<?php echo $form['num_ext']->renderError() ?>
						<br />

						<?php echo $form['num_int']->renderLabel("Número interior") ?>
						<?php echo $form['num_int']->render() ?>
						<?php echo $form['num_int']->renderError() ?>
						<br />
					</div>
					<div class="der">
						<?php echo $form['lat']->render() ?>
						<?php echo $form['long']->render() ?>
						<input type="hidden" name="centro_lat" id="centro_lat"
							value="<?php echo sfConfig::get('app_centro_lat') ?>"> <input
							type="hidden" name="centro_long" id="centro_long"
							value="<?php echo sfConfig::get('app_centro_long') ?>">
						<?php echo $form['dirgmaps']->render() ?>
						<div id="map_canvas" class="sombra-delgada"></div>
					</div>
				</div>
				<h3>Membresía</h3>
				<br>
				<?php echo $form['folio']->renderLabel("Folio credencial") ?>
				<?php echo $form['folio']->render(array('requerido'=>'1','filtro'=>'numerico','size'=>'15','maxlength'=>'13')) ?>
				<?php echo $form['folio']->renderError() ?>
				<br />

				<?php echo $form['clave']->renderLabel("Clave") ?>
				<?php echo $form['clave']->render(array('value'=>$form->getObject()->getClave(),'requerido'=>'1','size'=>'15','maxlength'=>'30')) ?>
				<?php echo $form['clave']->renderError() ?>
				<br>
				<?php echo $form['clave_confirmacion']->renderLabel("Clave confirmación") ?>
				<?php echo $form['clave_confirmacion']->render(array('value'=>$form->getObject()->getClave(),'requerido'=>'1','size'=>'15','maxlength'=>'30')) ?>
				<?php echo $form['clave_confirmacion']->renderError() ?>

			</div>

			<div id="complementarios" class="ui-tabs-panel ui-tabs-hide">

				<?php echo $form['discapacidad_id']->renderLabel("Salud") ?>
				<?php echo $form['discapacidad_id'] ?>
				<?php echo $form['discapacidad_id']->renderError() ?>
				<br />

				<?php echo $form['nivel_id']->renderLabel("Escolaridad") ?>
				<?php echo $form['nivel_id']?>
				<?php echo $form['nivel_id']->renderError() ?>
				<br />

				<?php echo $form['empresa']->renderLabel("Empresa referente") ?>
				<?php echo $form['empresa']?>
				<?php echo $form['empresa']->renderError() ?>

				<a href="javascript:void(0);" id="boton-abre-ventana-empresa">Alta</a>

				<br />


				<?php echo $form['socio_ref_id']->renderLabel("Parentesco con Socio") ?>
				<?php echo $form['socio_ref_id'] ?>
				<div id="socio_ref_nombre">
					<?php echo $form->getObject()->getSocioRelatedBySocioRefId() ?>
				</div>
				<a href="javascript:void(0);" id="boton-abre-ventana-socio">Buscar
					Socio</a>
				<?php echo $form['socio_ref_id']->renderError() ?>
				<br />

				<?php echo $form['parentesco_id']->renderLabel("Parentesco") ?>
				<?php echo $form['parentesco_id'] ?>
				<?php echo $form['parentesco_id']->renderError() ?>
				<br />

				<h3>Contacto en caso de emergencia</h3>

				<?php echo $form['er_nombre']->renderLabel("Nombre") ?>
				<?php echo $form['er_nombre']->render(array('requerido'=>'1','filtro'=>'alfabetico','size'=>'50','maxlength'=>'30')) ?>
				<?php echo $form['er_nombre']->renderError() ?>
				<br />

				<?php echo $form['er_apepat']->renderLabel("Apellido Paterno") ?>
				<?php echo $form['er_apepat']->render(array('requerido'=>'1','filtro'=>'alfabetico','size'=>'50','maxlength'=>'30')) ?>
				<?php echo $form['er_apepat']->renderError() ?>
				<br />

				<?php echo $form['er_apemat']->renderLabel("Apellido Materno") ?>
				<?php echo $form['er_apemat']->render(array('filtro'=>'alfabetico','size'=>'50','maxlength'=>'30')) ?>
				<?php echo $form['er_apemat']->renderError() ?>
				<br />

				<?php echo $form['er_fecha_nac']->renderLabel("Fecha de Nacimiento") ?>
				<?php echo $form['er_fecha_nac'] ?>
				<?php echo $form['er_fecha_nac']->renderError() ?>
				<br />

				<?php echo $form['er_sexo']->renderLabel("Género") ?>
				<?php echo $form['er_sexo']->render(array('requerido'=>'1')) ?>
				<?php echo $form['er_sexo']->renderError() ?>
				<br />

				<?php echo $form['er_tel']->renderLabel("Teléfono casa") ?>
				<?php echo $form['er_tel']->render(array('requerido'=>'1','filtro'=>'numerico', 'maxlength'=>'11')) ?>
				<?php echo $form['er_tel']->renderError() ?>
				<br />

				<?php echo $form['er_celular']->renderLabel("Teléfono celular") ?>
				<?php echo $form['er_celular']->render(array('filtro'=>'numerico', 'maxlength'=>'11')) ?>
				<?php echo $form['er_celular']->renderError() ?>
				<br />

				<?php echo $form['er_email']->renderLabel("E-mail") ?>
				<?php echo $form['er_email']->render(array('nouppercase'=>'1')) ?>
				<?php echo $form['er_email']->renderError() ?>




			</div>

			<?php echo $form['foto']->render() ?>
			<?php echo $form['huella']->render() ?>

			<br />
			<?php echo $form['id'] ?>
			<?php echo $form['_csrf_token'] ?>

			<input type="submit" value="Guardar"
				onclick="return comparaRequeridos();" /> <input type="button"
				value="Cancelar"
				onclick="location.href='/frontend_dev.php/registroci'">
		</div>
	</div>
</form>

<script
	src="http://maps.google.com/maps?file=api&amp;v=2.x&amp;key=<?php echo sfConfig::get('app_gmaps_key');?>"
	type="text/javascript"></script>
<script
	src="http://www.google.com/uds/api?file=uds.js&amp;v=1.0"
	type="text/javascript"></script>
<script
	src="http://www.google.com/uds/solutions/mapsearch/gsmapsearch.js?mode=new"
	type="text/javascript"></script>
