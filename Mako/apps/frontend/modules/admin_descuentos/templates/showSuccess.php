<?php use_javascript('plugins/jquery.dataTables.js') ?>
<?php use_stylesheet('datatables.css') ?>

<script>
$(document).ready(function() {

	var mxw = 450;
	console.log('dp.h:'+$('#datos-promocion').height());
	console.log('lr.h:'+$('#lista-reglas').height());

	
});
</script>

<div class="ui-widget-header ui-corner-top tsss" style="padding: 5px;">Promoción</div>
<div class="ui-widget ui-corner-bottom sombra">
	<div class="ui-widget-content ui-corner-bottom" style="padding: 15px;">

		<div id="ui-contenedor" style="min-height: 200px;">

			<table style="float: left; margin-right: 40px;" id="datos-promocion">
				<caption>
					<h3>Datos de la promoción</h3>
				</caption>
				<tbody>

					<tr>
						<th>Nombre de la promoción:</th>
						<td><?php echo $Promocion->getNombre() ?></td>
					</tr>
					<tr>
						<th>Descripción:</th>
						<td style="max-width: 600px"><?php echo $Promocion->getDescripcion() ?>
						</td>
					</tr>

					<tr>
						<th>Vigente de:</th>
						<td><?php echo $Promocion->getVigenteDe("%Y-%m-%d") ?></td>
					</tr>
					<tr>
						<th>Vigente a:</th>
						<td><?php echo $Promocion->getVigenteA("%Y-%m-%d") ?></td>
					</tr>
					<tr>
						<th>Activo:</th>
						<td><?php echo ($Promocion->getActivo())?'SI':'NO' ?></td>
					</tr>
					<tr>
						<th>Descuento:</th>
						<td><?php echo $Promocion->getDescuento() ?> (%)</td>
					</tr>
					<tr>
						<th>Creado:</th>
						<td><?php echo $Promocion->getCreatedAt() ?></td>
					</tr>
					<tr>
						<th>Ultima modificación:</th>
						<td><?php echo $Promocion->getUpdatedAt() ?></td>
					</tr>
				</tbody>
			</table>


			<table id="lista-productos">
				<caption>
					<h3>Productos en esta promoción</h3>
				</caption>
				<tbody>

					<?php foreach ($Promocion->getPromocionProductos() as $pp):?>
					<tr>
						<td><li><?php echo $pp->getProducto() ?></li></td>
					</tr>
					<?php endforeach; ?>

				</tbody>
			</table>

		</div>

		<hr />
		<a
			href="<?php echo url_for('admin_descuentos/edit?id='.$Promocion->getId()) ?>">Modificar</a>
		| <a href="<?php echo url_for('admin_descuentos/index') ?>">Ver
			promociones</a>

	</div>
</div>
<br />


<?php include_partial('formReglas', array('formReglas' => $formReglas)) ?>
<br />
<?php include_partial('listaReglas', array('PromocionReglas' => $PromocionReglas)) ?>




