<?php use_javascript('plugins/jquery.dataTables.js') ?>
<?php use_stylesheet('datatables.css') ?>

<script>
var lsT;
$(document).ready(function() {

   lsT = $('#lista-descuentos').dataTable({
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
        "bJQueryUI": true,
        "sPaginationType": "full_numbers"
     });
   
});
</script>

<div class="ui-widget-header ui-corner-top tsss" style="padding: 5px;">Promociones</div>
<div class="ui-widget ui-corner-bottom sombra">
	<div class="ui-widget-content ui-corner-bottom" style="padding: 5px;">

		<?php if ($sf_user->hasFlash('ok')): ?>
		<div
			class="flash_ok ui-state-highlight ts sombra-delgada ui-corner-all"
			style="padding: 5px; margin-top: 10px; margin-bottom: 10px;">
			<span style="float: left; margin-right: 0.3em;"
				class="ui-icon ui-icon-info"></span> <strong><?php echo $sf_user->getFlash('ok') ?>
			</strong>
		</div>
		<?php endif; ?>

		<br /> <span style="float: left;" class="ui-icon ui-icon-circle-plus"></span>
		<a href="<?php echo url_for('admin_descuentos/new') ?>"> Agregar
			descuento </a> <br /> <br />
		<table id="lista-descuentos" width="100%">
			<thead>
				<tr>
					<th>Revisar</th>
					<th>Nombre</th>
					<th>Vigente de</th>
					<th>Vigente a</th>
					<th>Activo</th>
					<th>Descuento(%)</th>
					<th>Creación</th>
					<th>Actualización</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($Promocions as $Promocion): ?>
				<tr>
					<td align="center" class=""><a
						href="<?php echo url_for('admin_descuentos/show?id='.$Promocion->getId()) ?>">
							<span class="ui-icon  ui-icon-circle-zoomin"></span>
					</a>
					</td>
					<td><?php echo $Promocion->getNombre() ?></td>
					<td><?php echo $Promocion->getVigenteDe("%Y-%m-%d") ?></td>
					<td><?php echo $Promocion->getVigenteA("%Y-%m-%d") ?></td>
					<td><?php echo ($Promocion->getActivo())?'SI':'NO' ?></td>
					<td align="right"><?php echo $Promocion->getDescuento() ?></td>
					<td><?php echo $Promocion->getCreatedAt() ?></td>
					<td><?php echo $Promocion->getUpdatedAt() ?></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

	</div>
</div>
