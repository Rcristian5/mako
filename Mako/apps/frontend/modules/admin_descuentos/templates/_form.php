<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<?php use_javascript('jquery.ui.js') ?>
<?php use_javascript('AdminDescuentoVal.js') ?>

<?php use_stylesheet('./admin_descuentos/_form.css')?>

<script>
// Reingenieria Mako C. ID 35. Responsable:  FE. Fecha: 23-01-2014.  Descripción del cambio: Se valido campos de fecha inicial y final.
/*
$(document).ready(function() 
{
	$(".datepicker").datepicker({
				firstDay: 1,
				changeYear: true,
				changeMonth: true,
				dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
				dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
				monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
				minDate: new Date(2010, 1 - 1, 1),
				constrainInput: true,
				dateFormat: 'yy-mm-dd'
	});
});
*/
// Fecha: 23-01-2014 - Fin.

</script>

<div class="ui-widget-header ui-corner-top tsss" style="padding: 5px;">Generar
	nueva promoción</div>
<div class="ui-widget ui-corner-bottom sombra">
	<div class="ui-widget-content ui-corner-bottom" style="padding: 15px;">


		<?php if ($sf_user->hasFlash('ok')): ?>
		<div
			class="flash_ok ui-state-highlight ts sombra-delgada ui-corner-all"
			style="padding: 5px; margin-top: 10px; margin-bottom: 10px;">
			<span style="float: left; margin-right: 0.3em;"
				class="ui-icon ui-icon-info"></span> <strong><?php echo $sf_user->getFlash('ok') ?>
			</strong>
			<!-- Reingenieria Mako CP. ID 37. Responsable:  ES. Fecha: 09-04-2014.  Descripción del cambio: ****.-->
			<script>
            confirma =confirm('¿Desea agregar una regla de descuento?');
            if(confirma){
                $(location).attr('href','../../show?id='+<?php echo $form->getObject()->getId();?>); 
            }
        </script>
			<!-- Fecha: 09-04-2014 Fin-->
		</div>
		<?php endif; ?>


		<form
			action="<?php echo url_for('admin_descuentos/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>"
			method="post"
			<?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
			<?php if (!$form->getObject()->isNew()): ?>
			<input type="hidden" name="sf_method" value="put" />
			<?php endif; ?>
			<table id="lista-descuentos">
				<tfoot>
					<tr>
						<td colspan="2">
							<hr /> &nbsp;<a
							href="<?php echo url_for('admin_descuentos/index') ?>">Ver
								Promociones</a> <?php if (!$form->getObject()->isNew()): ?> |
							&nbsp;<a
							href="<?php echo url_for('admin_descuentos/show?id='.$form->getObject()->getId()) ?>">Administrar
								reglas de descuento</a> <?php endif; ?> <input type="submit"
							value="Guardar" onclick="return requeridos();" />
						</td>
					</tr>
				</tfoot>
				<tbody>
					<?php echo $form ?>
				</tbody>
			</table>
		</form>
	</div>
</div>
