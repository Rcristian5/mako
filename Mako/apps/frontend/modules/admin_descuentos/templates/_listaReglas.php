<script>
var lsT;
$(document).ready(function() {

   lsT = $('#lista-reglas').dataTable({
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
        "bJQueryUI": true,
        "sPaginationType": "full_numbers"
     });
   
});
</script>

<div class="ui-widget-header ui-corner-top tsss" style="padding: 5px;">Reglas
	de descuento</div>
<div class="ui-widget ui-corner-bottom sombra">
	<div class="ui-widget-content ui-corner-bottom" style="padding: 5px;">

		<table id="lista-reglas">
			<thead>
				<tr>
					<th align="center"><span class="ui-icon  ui-icon-circle-close"></span>
					</th>
					<th align="center">Sexo</th>
					<th align="center">Edad</th>
					<th align="center">Nivel estudio</th>
					<th align="center">Discapacidad</th>
					<th align="center">Recomendados</th>
					<th align="center">Centro alta</th>
					<th align="center">Centro venta</th>
					<th align="center">Mun.</th>
					<th align="center">Col.</th>
					<th align="center">CP.</th>
					<th align="center">Hab. Inf.</th>
					<th align="center">Dom. Ing.</th>
					<th align="center">Profesión</th>
					<th align="center">Ocupación</th>
					<th align="center">Posición</th>
					<th align="center">Estudia</th>
					<th align="center">Trabaja</th>
					<th align="center">Beca</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($PromocionReglas as $PromocionRegla): //$PromocionRegla = new PromocionRegla();?>
				<tr>
					<td align="center"><a
						href="<?php echo url_for('admin_descuentos/deleteReglas?id='.$PromocionRegla->getId()) ?>"
						onclick="return confirm('Va a eliminar la regla de descuento de esta promoción. ¿Continuar?')">
							<span class="ui-icon  ui-icon-circle-close"></span>
					</a>
					</td>
					<td align="center"><?php echo ($PromocionRegla->getSexo()) ? (($PromocionRegla->getSexo()=='F')?'Femenino':'Masculino'):'--' ?>
					</td>
					<td align="center"><?php echo $PromocionRegla->getEdadDe().' - '.$PromocionRegla->getEdadA() ?>
					</td>
					<td align="center"><?php echo ($PromocionRegla->getNivelEstudioRelatedByNivelEstudio())? $PromocionRegla->getNivelEstudioRelatedByNivelEstudio():'--' ?>
					</td>
					<td align="center"><?php echo ($PromocionRegla->getDiscapacidad())? $PromocionRegla->getDiscapacidad():'--' ?>
					</td>
					<td align="center"><?php echo $PromocionRegla->getRecomendadosDe().' - '.$PromocionRegla->getRecomendadosA() ?>
					</td>
					<td align="center"><?php echo ($PromocionRegla->getCentroAltaId())? $PromocionRegla->getCentroRelatedByCentroAltaId():'--' ?>
					</td>
					<td align="center"><?php echo ($PromocionRegla->getCentroVentaId())? $PromocionRegla->getCentroRelatedByCentroVentaId():'--' ?>
					</td>
					<td align="center"><?php echo ($PromocionRegla->getMunicipio())? $PromocionRegla->getMunicipio():'--' ?>
					</td>
					<td align="center"><?php echo ($PromocionRegla->getColonia())? $PromocionRegla->getColonia():'--' ?>
					</td>
					<td align="center"><?php echo ($PromocionRegla->getCp())? $PromocionRegla->getCp():'--' ?>
					</td>
					<td align="center"><?php echo ($PromocionRegla->getHabilidadInformaticaId())? $PromocionRegla->getHabilidadInformatica():'--' ?>
					</td>
					<td align="center"><?php echo ($PromocionRegla->getDominioInglesId())? $PromocionRegla->getDominioIngles():'--' ?>
					</td>
					<td align="center"><?php echo ($PromocionRegla->getProfesionId())? $PromocionRegla->getProfesion():'--' ?>
					</td>
					<td align="center"><?php echo ($PromocionRegla->getOcupacionId())? $PromocionRegla->getOcupacion():'--' ?>
					</td>
					<td align="center"><?php echo ($PromocionRegla->getPosicionId())? $PromocionRegla->getPosicion():'--' ?>
					</td>
					<td align="center"><?php echo ($PromocionRegla->getEstudia())? $PromocionRegla->getEstudia():'--' ?>
					</td>
					<td align="center"><?php echo ($PromocionRegla->getTrabaja())? $PromocionRegla->getTrabaja():'--' ?>
					</td>
					<td align="center"><?php echo ($PromocionRegla->getBeca())? $PromocionRegla->getBeca():'--' ?>
					</td>

				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

	</div>
</div>
