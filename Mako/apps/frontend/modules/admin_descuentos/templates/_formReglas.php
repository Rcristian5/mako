<?php use_stylesheets_for_form($formReglas) ?>
<?php use_javascripts_for_form($formReglas) ?>
<?php use_javascript('jquery.ui.js') ?>

<script type="text/javascript">
	$(function() {
		$("#slider-range").slider({
			range: true,
			min: 0,
			max: 500,
			values: [3, 80],
			slide: function(event, ui) {
				$("#amount").val('$' + ui.values[0] + ' - $' + ui.values[1]);
			}
		});

	});
</script>


<div class="ui-widget-header ui-corner-top tsss" style="padding: 5px;">Administrar
	reglas de descuento</div>
<div class="ui-widget ui-corner-bottom sombra">
	<div class="ui-widget-content ui-corner-bottom" style="padding: 15px;">

		<?php if ($sf_user->hasFlash('ok')): ?>
		<div
			class="flash_ok ui-state-highlight ts sombra-delgada ui-corner-all"
			style="padding: 5px; margin-top: 10px; margin-bottom: 10px;">
			<span style="float: left; margin-right: 0.3em;"
				class="ui-icon ui-icon-info"></span> <strong><?php echo $sf_user->getFlash('ok') ?>
			</strong>
		</div>
		<?php endif; ?>

		<form
			action="<?php echo url_for('admin_descuentos/'.($formReglas->getObject()->isNew() ? 'createReglas' : 'updateReglas').(!$formReglas->getObject()->isNew() ? '?id='.$formReglas->getObject()->getId() : '')) ?>"
			method="post"
			<?php $formReglas->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
			<?php if (!$formReglas->getObject()->isNew()): ?>
			<input type="hidden" name="sf_method" value="put" />
			<?php endif; ?>
			<table>
				<tfoot>
					<tr>
						<td colspan="2">
							<hr /> <?php if (!$formReglas->getObject()->isNew()): ?> &nbsp;<?php echo link_to('Borrar', 'admin_descuentos/deleteReglas?id='.$formReglas->getObject()->getId(), array('method' => 'delete', 'confirm' => '¿Está seguro?')) ?>
							<?php endif; ?> <input type="submit"
							value="<?php echo ($formReglas->getObject()->isNew())? 'Agregar regla de descuento':'Modificar regla' ?>" />
						</td>
					</tr>
				</tfoot>
				<tbody>
					<?php echo $formReglas ?>
				</tbody>
			</table>
		</form>
	</div>
</div>
