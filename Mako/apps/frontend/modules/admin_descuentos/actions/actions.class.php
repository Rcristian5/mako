<?php

/**
 * admin_descuentos actions.
 *
 * @package    mako
 * @subpackage admin_descuentos
 * @author     Bixit SA de CV
 * @version    SVN: $Id: actions.class.php,v 1.3 2010/12/10 20:12:33 eorozco Exp $
 */
class admin_descuentosActions extends sfActions
{
	public function executeIndex(sfWebRequest $request)
	{
		$this->Promocions = PromocionPeer::doSelect(new Criteria());
	}

	public function executeShow(sfWebRequest $request)
	{
		$this->Promocion = PromocionPeer::retrieveByPk($request->getParameter('id'));
		$this->PromocionReglas = $this->Promocion->getPromocionReglas();

		$regla = new PromocionRegla();
		$regla->setPromocion($this->Promocion);
		$this->formReglas = new PromocionReglaForm($regla);

		$this->forward404Unless($this->Promocion);
	}

	public function executeNew(sfWebRequest $request)
	{
		$this->form = new PromocionForm();
	}

	public function executeCreate(sfWebRequest $request)
	{
		$this->forward404Unless($request->isMethod(sfRequest::POST));

		$this->form = new PromocionForm();

		$this->processForm($request, $this->form);

		$this->setTemplate('new');
	}

	public function executeCreateReglas(sfWebRequest $request)
	{
		$this->forward404Unless($request->isMethod(sfRequest::POST));

		$this->formReglas = new PromocionReglaForm();

		$this->processFormReglas($request, $this->formReglas);

		$this->setTemplate('show');
	}


	public function executeEdit(sfWebRequest $request)
	{
		$this->forward404Unless($Promocion = PromocionPeer::retrieveByPk($request->getParameter('id')), sprintf('Object Promocion does not exist (%s).', $request->getParameter('id')));
		$this->form = new PromocionForm($Promocion);
	}

	public function executeEditReglas(sfWebRequest $request)
	{
		$this->forward404Unless($PromocionRegla = PromocionReglaPeer::retrieveByPk($request->getParameter('id')), sprintf('Object PromocionReglaci does not exist (%s).', $request->getParameter('id')));
		$this->formReglas = new PromocionReglaForm($PromocionRegla);
		$this->processFormReglas($request, $this->formReglas);
	}


	public function executeUpdate(sfWebRequest $request)
	{
		$this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
		$this->forward404Unless($Promocion = PromocionPeer::retrieveByPk($request->getParameter('id')), sprintf('Object Promocion does not exist (%s).', $request->getParameter('id')));
		$this->form = new PromocionForm($Promocion);

		$this->processForm($request, $this->form);

		$this->setTemplate('edit');
	}

	public function executeDelete(sfWebRequest $request)
	{
		$request->checkCSRFProtection();

		$this->forward404Unless($Promocion = PromocionPeer::retrieveByPk($request->getParameter('id')), sprintf('Object Promocion does not exist (%s).', $request->getParameter('id')));
		$Promocion->delete();
		$this->redirect('admin_descuentos/index');
	}

	public function executeDeleteReglas(sfWebRequest $request)
	{


		$this->forward404Unless($PromocionRegla = PromocionReglaPeer::retrieveByPk($request->getParameter('id')), sprintf('Object Promocion does not exist (%s).', $request->getParameter('id')));
		$promocion_id = $PromocionRegla->getPromocionId();
		$regla_id = $PromocionRegla->getId();
		$PromocionRegla->delete();
		$ReglaBorrada = new PromocionRegla();
		$ReglaBorrada->setId($regla_id);
		sfProjectConfiguration::getActive()->getEventDispatcher()->notify(new sfEvent($ReglaBorrada, 'promocionregla.borrada',$ReglaBorrada));

		$this->getUser()->setFlash('ok', 'Se ha eliminado correctamente la regla de descuento.');
		$this->redirect('admin_descuentos/show?id='.$promocion_id);
	}

	protected function processForm(sfWebRequest $request, sfForm $form)
	{
		$form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
		if ($form->isValid())
		{
			$Promocion = $form->save();
			if(!$form->isNew())
			{
				sfProjectConfiguration::getActive()->getEventDispatcher()->notify(new sfEvent($Promocion, 'promocion.actualizado',$Promocion));
			}
			else
			{
				sfProjectConfiguration::getActive()->getEventDispatcher()->notify(new sfEvent($Promocion, 'promocion.creado',$Promocion));
			}

			$this->getUser()->setFlash('ok', 'Se han guardado correctamente los detalles de la promoción.');
			$this->redirect('admin_descuentos/edit?id='.$Promocion->getId());
		}
	}


	protected function processFormReglas(sfWebRequest $request, sfForm $formReglas)
	{
		$formReglas->bind($request->getParameter($formReglas->getName()), $request->getFiles($formReglas->getName()));
		if ($formReglas->isValid())
		{
			$PromocionRegla = $formReglas->save();
			sfProjectConfiguration::getActive()->getEventDispatcher()->notify(new sfEvent($PromocionRegla, 'promocionregla.creada',$PromocionRegla));
			$this->getUser()->setFlash('ok', 'Se ha agregado correctamente la nueva regla de descuento.');
			$this->redirect('admin_descuentos/show?id='.$PromocionRegla->getPromocionId());
		}
	}

}
