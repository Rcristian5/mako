<?php

/**
 * sesiones actions.
 *
 * @package    mako
 * @subpackage sesiones
 * @author     Bixit SA de CV
 * @version    SVN: $Id: actions.class.php,v 1.28 2012-03-02 05:04:34 eorozco Exp $
 */
class sesionesActions extends sfActions {

    var $request = null;

    public function executeIndex(sfWebRequest $request) {
        error_log('************* Index Sesiones');
        //1. Verificar el tipo de usuario
        //2. Verificar el tipo de pc
        $this->getContext()->getConfiguration()->loadHelpers('Partial');

        $ip = Comun::ipReal();
        $pc = ComputadoraPeer::getPcPorIp($ip);
        if ($pc != null) {
            error_log('************* Index Sesiones PC 1');
            $tipo_pc = $pc->getTipoId();
        } else {
            error_log('************* Index Sesiones Pc default');
            //Si no aparece en el catálogo entónces es una solicitud externa al centro
            $tipo_pc = sfConfig::get('app_externa_tipo_pc_id');
        }

        //Es un socio ?
        if (SesionPeer::esSocio($ip)) {
            error_log('************* Index Sesiones Es Socio');
            //Esta entrando desde una pc autorizada para socios?
            if ($tipo_pc == sfConfig::get('app_usuario_tipo_pc_id')) {
                $this->getUser()->setAuthenticated(true);
                $this->getUser()->setAttribute('usuario', SesionPeer::esSocio($ip));
                $this->redirect('dashsocios/index');
            } else {
                error_log("Tipo pc incorrecto, no se puede crear sesion.");
                $this->setTemplate('sinPermiso');
                $this->getUser()->setFlash('notice', 'No tiene permiso para acceder a esta PC.');
            }
        }
        //Es un operador?
        elseif (SesionPeer::esOperador($ip)) {
            error_log('************* Index Sesiones Es operador');
            //Para liga de brainpop en su dash
            $c = CentroPeer::retrieveByPK(sfConfig::get('app_centro_actual_id'));
            $this->centro = strtolower($c->getAlias());

            $op = SesionPeer::esOperador($ip);
            $this->getUser()->setAttribute('usuario', $op);
            error_log(" *********** se detecta: pc:" . $tipo_pc . " rol: " . $op->getRolId());
            $this->redirect('sesiones/authExterna');
        } elseif ($tipo_pc == sfConfig::get('app_externa_tipo_pc_id') && !$this->getUser()->isAuthenticated()) {
            //Se trata de una solicitud externa.
            //Mostramos plantilla de login
            error_log('+++ No esta autenticado y es externo:');
            $this->getUser()->setAttribute('auth_externo', true);
        } elseif ($tipo_pc == sfConfig::get('app_externa_tipo_pc_id') && $this->getUser()->isAuthenticated()) {
            $this->rolUsuarioRemoto($this->getUser()->getAttribute('usuario'));
        } elseif (sfConfig::get('app_centro_actual_id') == sfConfig::get('app_central_id') && !$this->getUser()->isAuthenticated()) {
            error_log('+++ No esta autenticado y es interno a la ria accediendo a central:');
            $this->getUser()->setAttribute('auth_externo', true);
        } else {
            $this->redirect('sesiones/authExterna');
            $this->setTemplate('sinSesion');
            $this->getUser()->setAuthenticated(false);
            $this->getUser()->setFlash('notice', 'No tiene una sesión abierta.');
        }

        //error_log("SESION PC: ".$pc->getTipoPc()->getNombre()." ROL ".$op->getRol()->getNombre());
        $this->Sesions = SesionPeer::doSelect(new Criteria());
    }

    /**
     * Realiza el proceso de autenticacion y generación de sesion para usuario que accede desde una ip externa
     * @param sfWebRequest $request
     * @return void
     */
    public function executeAuthExterna(sfWebRequest $request) {
        error_log("Entra a la auth externa");
        $usuario = $request->getParameter('usuario', $this->getUser()->hasAttribute('usuario') ? $this->getUser()->getAttribute('usuario')->getUsuario() : null);
        $clave = $request->getParameter('clave', $this->getUser()->hasAttribute('usuario') ? $this->getUser()->getAttribute('usuario')->getClave() : null);

        error_log('++++++++++++ Existe Usuario ' . ($this->getUser()->hasAttribute('usuario') ? '---Si---' : 'No'));

        $c = new Criteria();
        $c->add(UsuarioPeer::USUARIO, $usuario);
        $c->add(UsuarioPeer::CLAVE, $clave);
        $c->add(UsuarioPeer::ACTIVO, true);
        $u = UsuarioPeer::doSelectOne($c);
        $this->request = $request;

        if ($u != null) {
            error_log('-----------+++ El usuario está loguedo');
            $this->rolUsuarioRemoto($u);
        } else {
            $this->getUser()->setAuthenticated(false);
            $this->getUser()->setFlash('notice', 'No se han encontrado sus datos en el sistema o el usuario ha sido desactivado.');
            $this->getUser()->setAttribute('auth_externo', true);
            $this->setTemplate('index');
        }
    }


    /**
    * Función que obtiene el tipo de PC del usuario logeado al sistema
    * 
    * A. Validamos que no existe el parámetro TIPO_PC, ya que si existe se estan realizando pruebas por QA o DEVOPS
    *
    * B. En caso de no existe el parámetro
    *    1.- se busca la PC en base de datos y se regresa su TIPO
    *    2.- Si no existe en base de datos, se trata de una PC externa
    *
    * @@ LGL   Luis González
    * 2016-06-27
    */
    private function obtieneTipoPc()
    {
        error_log("Obteniendo tipo PC...");
        $IpParam = $this->request->getParameter('ip');
        $Ip = ($IpParam == '')? Comun::ipReal() : $IpParam;
        error_log('Ip:                  ' . $Ip. '              ');
        $Computadora = ComputadoraPeer::getPcPorIp($Ip);
        if ( !is_null($Computadora))
            return $Computadora->getTipoId();
        if ( $this->request->hasParameter('tipo_pc') )
        {
            switch ($this->request->getParameter('tipo_pc'))
                {
                    case 'caja':
                        error_log('TIPO_PC:                  Caja              ');
                        return sfConfig::get('app_caja_tipo_pc_id');
                        break;
                    case 'socio':
                        error_log('TIPO_PC:                  Socio              ');
                        return sfConfig::get('app_usuario_tipo_pc_id');
                        break;
                    case 'operacion':
                        error_log('TIPO_PC:                  Operación          ');
                        return sfConfig::get('app_operacion_tipo_pc_id');
                        break;
                    case 'admin':
                        error_log('TIPO_PC:                  Admin              ');
                        return sfConfig::get('app_administracion_tipo_pc_id');
                        break;
                }
        }
        return sfConfig::get('app_externa_tipo_pc_id');
    }

    /**
    * Función que obtiene el rol del usuario logeado al sistema
    * 
    * A. Validamos que no existe el parámetro rol_usr, ya que si existe se estan realizando pruebas por QA o DEVELOPERS
    *
    * B. En caso de no existe el parámetro
    *    1.- se obtiene el rol del usuario logeado
    *
    * @@ LGL   Luis González
    * 2016-06-27
    */
    private function obtieneRolUsr($Usr)
    {
        error_log("Obteniendo RolUsr...");
        if ( $this->request->hasParameter('rol_usr') )
        {
            switch ($this->request->getParameter('rol_usr'))
                {
                    case 'sup':
                        error_log('ROL:          SuperUsuario        ');
                        return sfConfig::get('app_rol_super_usuario_id');
                        break;
                    case 'caj':
                        error_log('ROL:                  Cajero             ');
                        return sfConfig::get('app_rol_cajero_id');
                        break;
                    case 'fac':
                        error_log('ROL:                  Facilitador        ');
                        return sfConfig::get('app_rol_facilitador_id');
                        break;
                    case 'pro':
                        error_log('ROL:                  Promotor           ');
                        return sfConfig::get('app_rol_promotor_id');
                        break;
                    case 'sop':
                        error_log('ROL:                  Soptec             ');
                        return sfConfig::get('app_rol_soptec_id');
                        break;
                    case 'adm':
                        error_log('ROL:                  Admin              ');
                        return sfConfig::get('app_rol_admin_id');
                        break;
                    case 'soc':
                        error_log('ROL:                  Socio              ');
                        return sfConfig::get('app_rol_socio_id');
                        break;
                    case 'ces':
                        error_log('ROL:                  Control Escolar    ');
                        return sfConfig::get('app_rol_admin_ctrl_esc_id');
                        break;
                    case 'admo':
                        error_log('ROL:                  Admin Ops           ');
                        return sfConfig::get('app_rol_admin_operaciones_id');
                        break;
                    case 'coa':
                        error_log('ROL:         Coordinador Academico        ');
                        return sfConfig::get('app_rol_coordinador_academico_id');
                        break;
                }
        }
        return $Usr->getRolId();
    }

    /**
     * Revisión de Roles y Permisos por usuarios, asignados por Caja y por PC Aula para PMC
     * 
     * A. Primero identificamos si la ip de la que se conecta viene desde caja, esto se hace:
     *       1. Obteniendo la IP del cual se está conectando u obtenemos la ip del parametro request
     *       2. Buscando el tipo de PC por el cual está en comunicación, es decir,
     *          buscamos en base de datos o en la configuración en el app.yml, con la ip antes obtenida 
     *          relacionamos su tipo de PC, 
     * B. Habiendo obtenido la IP y su Tipo PC, Identificamos el Rol al que pertenece el Usuario,
     *    con esto determinamos el home y su menu
     *
     * - Asignacion
     *       Si es Tipo PC = Caja? menu y home son de Caja, es decir menu[]Caja y home[]Caja, 
     * - menu[] y menu[]Caja se encuentran en ~/Mako/apps/frontend/templates
     * - home[] y home[]Caja se encuentran en ~/Mako/apps/frontend/modules/sesiones/templates
     *
     * BGPJ Bet Porcayo 
     * 03-Enero-2015 -> 04-Enero-2015
     */
    private function rolUsuarioRemoto(Usuario $Usuario) {
        $Menu = array();
        $Menu['administrador_escolar'] = false;
        $Menu['administrador_operaciones'] = false;
        $Menu['administrador'] = false;
        $Menu['caja'] = false;
        $Menu['facilitador'] = false;
        $Menu['promotor'] = false;
        $Menu['registro'] = false;
        $Menu['soporte_tecnico'] = false;
        $Menu['super_usuario'] = false;
        $this->Herramientas = false;

        $TipoPc    = $this->obtieneTipoPc();
        $RolUsr_Id = $this->obtieneRolUsr($Usuario);

        $EsAutentidado = true;
        $EsExterno = true;
        $Permisos = array();
        $Permisos['administrar_productos'] = false;
        $Permisos['administrar_descuentos'] = false;
        $Permisos['administrar_cupones'] = false;
        $Permisos['administrar_usuarios'] = false;
        $Permisos['administrador_vacaciones'] = false;
        $Permisos['administrador_categorias'] = false;
        $Permisos['carga_cursos'] = false;
        $Permisos['verificacion_cursos'] = false;
        $Permisos['modelador_temarios'] = false;
        $Permisos['administrador_aulas'] = false;
        $Permisos['consulta_socios_inscritos'] = false;
        $Permisos['ubicacion_centros'] = false;
        $Permisos['busqueda_categoria'] = false;
        $Permisos['solicitud_beca'] = false;
        $Permisos['administracion_centros'] = false;
        $Permisos['folio_fiscal_centro'] = false;
        $Permisos['cancelacion_grupo'] = false;
        $Permisos['reporte_socios_inscritos'] = false;
        $Permisos['reporte_asistencias'] = false;
        $Permisos['reporte_calificaciones'] = false;
        $Permisos['historial_academico'] = false;
        $Permisos['seguimiento_indicadores'] = false;
        $Permisos['indicadores_operacion'] = false;
        $Permisos['indicadores_registro_ventas_inscripcion'] = false;
        $Permisos['indicadores_capacidad_instalada'] = false;
        $Permisos['indicadores_estatus_socios'] = false;
        $Permisos['lista_socios_inactivos'] = false;
        $Permisos['conciliacion_administrativa'] = false;
        $Permisos['administracion_recursos'] = false;
        $Permisos['categorias_productos'] = false;

        $Permisos['registro_socios'] = false;
        $Permisos['busqueda_socios'] = false;
        $Permisos['punto_venta'] = false;
        $Permisos['movimientos_dia'] = false;
        $Permisos['salida_caja'] = false;
        $Permisos['corte_caja'] = false;
        $Permisos['cancelacion_venta'] = false;
        $Permisos['reporte_vajilla'] = false;
        $Permisos['programacion_estado_grupos'] = false;
        $Permisos['control_asistencia_lectura'] = false;
        $Permisos['control_asistencia'] = false;
        $Permisos['registro_asistencia'] = false;
        $Permisos['registro_asistencia_lectura'] = false;
        $Permisos['ocupacion_pc'] = false;
        $Permisos['encendido_apagado_pc'] = false;
        $Permisos['historial_salida'] = false;
        $Permisos['historial_corte'] = false;
        $Permisos['georeferencia_socios'] = false;
        $Permisos['administracion_secciones'] = false;
        $Permisos['administracion_pcs'] = false;
        $Permisos['actualizar_coordenadas_centro'] = false;
        $Permisos['calendarizacion_cursos'] = false;
        $Permisos['administracion_metas'] = false;
        
        $Permisos['administrar_dias_laborables'] = false;



        $NombreHome = 'Home ';



        switch ($RolUsr_Id) {

            case sfConfig::get('app_rol_super_usuario_id'): // 1 Super Usuario
                $EsAutentidado = true;
                $NombreHome .= 'Super Usuario';

                $Permisos['administrar_productos'] = true;
                $Permisos['administrar_descuentos'] = true;
                $Permisos['administrar_cupones'] = true;
                $Permisos['administracion_centros'] = true;
                $Permisos['folio_fiscal_centro'] = true;
                $Permisos['seguimiento_indicadores'] = true;
                $Permisos['indicadores_operacion'] = true;
                $Permisos['indicadores_registro_ventas_inscripcion'] = true;
                $Permisos['indicadores_capacidad_instalada'] = true;
                $Permisos['indicadores_estatus_socios'] = true;
                $Permisos['lista_socios_inactivos'] = true;
                $Permisos['conciliacion_administrativa'] = true;
                $Permisos['categorias_productos'] = true;

                $Permisos['registro_socios'] = true;
                $Permisos['busqueda_socios'] = true;
                $Permisos['punto_venta'] = true;
                $Permisos['movimientos_dia'] = true;
                $Permisos['salida_caja'] = true;
                $Permisos['corte_caja'] = true;
                $Permisos['cancelacion_venta'] = true;
                $Permisos['reporte_vajilla'] = true;
                $Permisos['programacion_estado_grupos'] = true;
                $Permisos['control_asistencia_lectura'] = true;
                $Permisos['ocupacion_pc'] = true;
                $Permisos['encendido_apagado_pc'] = true;
                $Permisos['historial_salida'] = true;
                $Permisos['historial_corte'] = true;
                $Permisos['georeferencia_socios'] = true;
                $Permisos['administracion_secciones'] = true;
                $Permisos['administracion_pcs'] = true;
                $Permisos['actualizar_coordenadas_centro'] = true;
                $Permisos['administracion_metas'] = true;




                break;

            case sfConfig::get('app_rol_cajero_id'): // 2 - Cajero ó Anfitrion
                $EsAutentidado = true;
                $EsExterno = false;
                $NombreHome .= 'Anfitrión';

                switch ($TipoPc) {
                    case sfConfig::get('app_caja_tipo_pc_id'):
                        $Permisos['registro_socios'] = true;
                        $Permisos['busqueda_socios'] = true;
                        $Permisos['ocupacion_pc'] = true;
                        $Permisos['programacion_estado_grupos'] = true;
                        $Permisos['punto_venta'] = true;
                        $Permisos['movimientos_dia'] = true;
                        $Permisos['salida_caja'] = true;
                        $Permisos['corte_caja'] = true;
                        $Permisos['cancelacion_venta'] = true;
                        $Permisos['reporte_vajilla'] = true;
                        $Permisos['encendido_apagado_pc'] = true;
                        $Menu['caja'] = true;
                        break;
                    case sfConfig::get('app_operacion_tipo_pc_id'):
                        $Permisos['registro_socios'] = true;
                        $Permisos['busqueda_socios'] = true;
                        $Permisos['ocupacion_pc'] = true;
                        $Permisos['programacion_estado_grupos'] = true;
                        $Permisos['reporte_vajilla'] = true;

                        $this->Herramientas = true;
                        break;

                    default:
                        $Permisos['busqueda_socios'] = true;
                        $Permisos['ocupacion_pc'] = true;
                        $Permisos['programacion_estado_grupos'] = true;
                        $Permisos['control_asistencia_lectura'] = true;
                        $Permisos['reporte_vajilla'] = true;

                        $this->Herramientas = true;
                        break;
                }


                /********************************************
                 ** PERMISOS GENERALES SIN IMPORTAR TIPO DE PC
                 **   @@ LGL  2016-06-23
                 *******************************************/
                $Permisos['control_asistencia_lectura'] = false;
                $Permisos['control_asistencia'] = false;
                $Permisos['registro_asistencia'] = false;
                $Permisos['registro_asistencia_lectura'] = true;

                break;

            case sfConfig::get('app_rol_facilitador_id'): // 3 Facilitador (Cómputo e Inglés)
                $EsAutentidado = true;
                $EsExterno = false;
                $NombreHome .= 'Facilitador';

                switch ($TipoPc) {
                    case sfConfig::get('app_caja_tipo_pc_id'):
                        $Permisos['registro_socios'] = true;
                        $Permisos['busqueda_socios'] = true;
                        $Permisos['ocupacion_pc'] = true;
                        $Permisos['programacion_estado_grupos'] = true;
                        break;

                    case sfConfig::get('app_operacion_tipo_pc_id'):
                        $Permisos['registro_socios'] = true;
                        $Permisos['busqueda_socios'] = true;
                        $Permisos['ocupacion_pc'] = true;
                        $Permisos['programacion_estado_grupos'] = true;
                        $this->Herramientas = true;
                        break;

                    default:
                        $Permisos['busqueda_socios'] = true;
                        $Permisos['ocupacion_pc'] = true;
                        $Permisos['programacion_estado_grupos'] = true;
                        $this->Herramientas = true;
                        break;
                }

                /********************************************
                 ** PERMISOS GENERALES SIN IMPORTAR TIPO DE PC
                 **   @@ LGL  2016-06-23
                 *******************************************/
                $Permisos['control_asistencia_lectura'] = false;
                $Permisos['control_asistencia'] = true;
                $Permisos['registro_asistencia'] = true;
                $Permisos['registro_asistencia_lectura'] = false;

                break;

            case sfConfig::get('app_rol_facilitador_encargado_id'): // 4 Facilitador Admin o Encargado
                $EsAutentidado = true;
                $EsExterno = false;
                $NombreHome .= 'Facilitador Encargado';

                switch ($TipoPc) {
                    case sfConfig::get('app_caja_tipo_pc_id'):
                        $Permisos['registro_socios'] = true;
                        $Permisos['busqueda_socios'] = true;
                        $Permisos['ocupacion_pc'] = true;
                        $Permisos['programacion_estado_grupos'] = true;
                        $Permisos['punto_venta'] = true;
                        $Permisos['movimientos_dia'] = true;
                        $Permisos['salida_caja'] = true;
                        $Permisos['corte_caja'] = true;
                        $Permisos['cancelacion_venta'] = true;
                        $Permisos['reporte_vajilla'] = true;
                        $Permisos['encendido_apagado_pc'] = true;
                        $Permisos['historial_salida'] = true;
                        $Permisos['historial_corte'] = true;
                        $Permisos['georeferencia_socios'] = true;

                        $Menu['caja'] = true;
                        break;
                    case sfConfig::get('app_operacion_tipo_pc_id'):
                        $Permisos['registro_socios'] = true;
                        $Permisos['busqueda_socios'] = true;
                        $Permisos['ocupacion_pc'] = true;
                        $Permisos['programacion_estado_grupos'] = true;
                        $Permisos['reporte_vajilla'] = true;
                        $Permisos['encendido_apagado_pc'] = true;
                        $Permisos['historial_salida'] = true;
                        $Permisos['historial_corte'] = true;
                        $Permisos['georeferencia_socios'] = true;
                        $Menu['registro'] = true;

                        $this->Herramientas = true;
                        break;

                    default:
                        $Permisos['busqueda_socios'] = true;
                        $Permisos['ocupacion_pc'] = true;
                        $Permisos['programacion_estado_grupos'] = true;
                        $Permisos['reporte_vajilla'] = true;
                        $Permisos['encendido_apagado_pc'] = true;
                        $Permisos['historial_salida'] = true;
                        $Permisos['historial_corte'] = true;
                        $Permisos['georeferencia_socios'] = true;

                        $this->Herramientas = true;
                        break;
                }

                /********************************************
                 ** PERMISOS GENERALES SIN IMPORTAR TIPO DE PC
                 **   @@ LGL  2016-06-23
                 *******************************************/
                $Permisos['control_asistencia_lectura'] = false;
                $Permisos['control_asistencia'] = true;
                $Permisos['registro_asistencia'] = true;
                $Permisos['registro_asistencia_lectura'] = false;
                break;

            case sfConfig::get('app_rol_soporte_tecnico_id'): // 5 Soporte técnico
                $EsAutentidado = true;
                $EsExterno = false;
                $NombreHome .= 'Soporte técnico';

                switch ($TipoPc) {
                    case sfConfig::get('app_caja_tipo_pc_id'):
                        $Permisos['busqueda_socios'] = true;
                        $Permisos['programacion_estado_grupos'] = true;
                        $Permisos['ocupacion_pc'] = true;
                        $Permisos['encendido_apagado_pc'] = true;
                        $Permisos['historial_salida'] = true;
                        $Permisos['historial_corte'] = true;
                        $Permisos['administracion_secciones'] = true;
                        $Permisos['administracion_pcs'] = true;
                        $Permisos['actualizar_coordenadas_centro'] = true;

                        break;
                    case sfConfig::get('app_operacion_tipo_pc_id'):
                        $Permisos['busqueda_socios'] = true;
                        $Permisos['programacion_estado_grupos'] = true;
                        $Permisos['ocupacion_pc'] = true;

                        $this->Herramientas = true;
                        break;

                    default:
                        $Permisos['busqueda_socios'] = true;
                        $Permisos['programacion_estado_grupos'] = true;
                        $Permisos['ocupacion_pc'] = true;
                        $Permisos['encendido_apagado_pc'] = true;
                        $Permisos['historial_salida'] = true;
                        $Permisos['historial_corte'] = true;
                        $Permisos['administracion_secciones'] = true;
                        $Permisos['administracion_pcs'] = true;
                        $Permisos['actualizar_coordenadas_centro'] = true;

                        $this->Herramientas = true;
                        break;
                }

                /********************************************
                 ** PERMISOS GENERALES SIN IMPORTAR TIPO DE PC
                 **   @@ LGL  2016-06-23
                 *******************************************/
                $Permisos['control_asistencia_lectura'] = false;
                $Permisos['control_asistencia'] = false;
                $Permisos['registro_asistencia'] = false;
                $Permisos['registro_asistencia_lectura'] = true;

                break;

            case sfConfig::get('app_rol_supervisor_id'): // 6 Administrador de Centro - Supervisor
                $EsAutentidado = true;
                $EsExterno = false;
                $NombreHome .= 'Supervisor';
                $this->Herramientas = true;

                switch ($TipoPc) {
                    case sfConfig::get('app_caja_tipo_pc_id'):
                        $Permisos['registro_socios'] = true;
                        $Permisos['busqueda_socios'] = true;
                        $Permisos['ocupacion_pc'] = true;
                        $Permisos['programacion_estado_grupos'] = true;
                        $Permisos['punto_venta'] = true;
                        $Permisos['movimientos_dia'] = true;
                        $Permisos['salida_caja'] = true;
                        $Permisos['corte_caja'] = true;
                        $Permisos['cancelacion_venta'] = true;
                        $Permisos['reporte_vajilla'] = true;
                        $Permisos['encendido_apagado_pc'] = true;
                        $Permisos['historial_salida'] = true;
                        $Permisos['historial_corte'] = true;
                        $Permisos['georeferencia_socios'] = true;
                        $Permisos['calendarizacion_cursos'] = true;
                        $Permisos['administracion_metas'] = true;
                        
                        $Menu['caja'] = true;
                        break;
                    case sfConfig::get('app_operacion_tipo_pc_id'):
                        $Permisos['registro_socios'] = true;
                        $Permisos['busqueda_socios'] = true;
                        $Permisos['ocupacion_pc'] = true;
                        $Permisos['programacion_estado_grupos'] = true;
                        $Permisos['reporte_vajilla'] = true;
                        $Permisos['encendido_apagado_pc'] = true;
                        $Permisos['historial_salida'] = true;
                        $Permisos['historial_corte'] = true;
                        $Permisos['georeferencia_socios'] = true;
                        $Permisos['calendarizacion_cursos'] = true;
                        $Permisos['administracion_metas'] = true;

                        break;

                    default:
                        $Permisos['busqueda_socios'] = true;
                        $Permisos['ocupacion_pc'] = true;
                        $Permisos['programacion_estado_grupos'] = true;
                        $Permisos['reporte_vajilla'] = true;
                        $Permisos['encendido_apagado_pc'] = true;
                        $Permisos['historial_salida'] = true;
                        $Permisos['historial_corte'] = true;
                        $Permisos['georeferencia_socios'] = true;
                        $Permisos['calendarizacion_cursos'] = true;
                        $Permisos['administracion_metas'] = true;

                        break;
                }

                /********************************************
                 ** PERMISOS GENERALES SIN IMPORTAR TIPO DE PC
                 **   @@ LGL  2016-06-23
                 *******************************************/
                $Permisos['control_asistencia_lectura'] = false;
                $Permisos['control_asistencia'] = true;
                $Permisos['registro_asistencia'] = true;
                $Permisos['registro_asistencia_lectura'] = false;

                break;

            /* case sfConfig::get('app_rol_socio_id'): // 7
              $EsAutentidado = false;
              $NombreHome .= 'Socio';

              switch ($TipoPc) {
              case sfConfig::get('app_caja_tipo_pc_id'):
              # code...
              break;
              case sfConfig::get('app_operacion_tipo_pc_id'):
              # code...
              break;

              default:
              # code...
              break;
              }

              break; */

            case sfConfig::get('app_rol_administrador_control_escolar_id'): // 8 Administrador de Control Escolar
                $EsAutentidado = true;
                $NombreHome .= 'Administrador de Control Escolar';

                $Permisos['administrar_productos'] = true;
                $Permisos['administrar_descuentos'] = true;
                $Permisos['administrador_vacaciones'] = true;
                $Permisos['administrador_categorias'] = true;
                $Permisos['carga_cursos'] = true;
                $Permisos['verificacion_cursos'] = true;
                $Permisos['modelador_temarios'] = true;
                $Permisos['administrador_aulas'] = true;
                $Permisos['calendarizacion_cursos'] = true;
                $Permisos['consulta_socios_inscritos'] = true;
                $Permisos['cancelacion_grupo'] = true;
                $Permisos['reporte_socios_inscritos'] = true;
                $Permisos['historial_academico'] = true;
                $Permisos['seguimiento_indicadores'] = true;
                
                $Permisos['programacion_estado_grupos'] = true;
                $Permisos['control_asistencia'] = true;
                $Permisos['punto_venta'] = true;
                $Permisos['busqueda_socios'] = true;


                $Menu['administrador_escolar'] = true;
                break;

            case sfConfig::get('app_rol_administrador_operaciones_id'): // 9 Administrador de Operaciones - Gerente de Centros RIA
                $EsAutentidado = true;
                $NombreHome .= 'Gerente de Centros RIA';

                $Permisos['busqueda_socios'] = true;
                $Permisos['administrar_productos'] = true;
                $Permisos['administrar_descuentos'] = true;
                $Permisos['administrar_cupones'] = true;
                $Permisos['seguimiento_indicadores'] = true;
                $Permisos['indicadores_operacion'] = true;
                $Permisos['indicadores_registro_ventas_inscripcion'] = true;
                $Permisos['indicadores_capacidad_instalada'] = true;
                $Permisos['indicadores_estatus_socios'] = true;
                $Permisos['lista_socios_inactivos'] = true;
                $Permisos['conciliacion_administrativa'] = true;
                $Permisos['administracion_recursos'] = true;
                $Permisos['georeferencia_socios'] = true;
                $Permisos['actualizar_coordenadas_centro'] = true;
                $Permisos['administracion_metas'] = true;

                $Menu['administrador_operaciones'] = true;

                break;

            case sfConfig::get('app_rol_supervisor_indicadores_id'): // 10 Supervisor Indicadores - Analista de mineria de Datos
                $EsAutentidado = true;
                $NombreHome .= 'Supervisor de Indicadores';

                $Permisos['indicadores_registro_ventas_inscripcion'] = true;
                $Permisos['indicadores_capacidad_instalada'] = true;
                $Permisos['indicadores_estatus_socios'] = true;
                $Permisos['lista_socios_inactivos'] = true;

                break;

            case sfConfig::get('app_rol_coordinador_academico_id'): // 11 Coordinador academico
                $EsAutentidado = true;
                $NombreHome .= 'Coordinador Académico';

                $Permisos['administrador_vacaciones'] = true;
                $Permisos['carga_cursos'] = true;
                $Permisos['modelador_temarios'] = true;
                $Permisos['administrador_aulas'] = true;
                $Permisos['calendarizacion_cursos'] = true;
                $Permisos['consulta_socios_inscritos'] = true;
                $Permisos['reporte_socios_inscritos'] = true;
                $Permisos['historial_academico'] = true;
                $Permisos['seguimiento_indicadores'] = true;
                $Permisos['indicadores_operacion'] = true;
                $Permisos['indicadores_registro_ventas_inscripcion'] = true;
                $Permisos['indicadores_capacidad_instalada'] = true;
                $Permisos['indicadores_estatus_socios'] = true;
                $Permisos['lista_socios_inactivos'] = true;
                /********************************************
                 ** PERMISOS GENERALES SIN IMPORTAR TIPO DE PC
                 **   @@ LGL  2016-06-23
                 *******************************************/
                $Permisos['control_asistencia_lectura'] = false;
                $Permisos['control_asistencia'] = true;
                $Permisos['registro_asistencia'] = true;
                $Permisos['registro_asistencia_lectura'] = false;


                break;

            case sfConfig::get('app_rol_promotor_id'): // 12 - Promotor
                $EsAutentidado = true;
                $NombreHome .= 'Promotor';


                switch ($TipoPc) {
                    case sfConfig::get('app_caja_tipo_pc_id'):
                        $Permisos['programacion_estado_grupos'] = true;
                        $Permisos['georeferencia_socios'] = true;
                        $Permisos['georeferencia_socios'] = true;
                        $Permisos['calendarizacion_cursos'] = true;
                        $Permisos['administracion_metas'] = true;

                        break;
                    case sfConfig::get('app_operacion_tipo_pc_id'):
                        $Permisos['programacion_estado_grupos'] = true;
                        $Permisos['georeferencia_socios'] = true;
                        $Permisos['georeferencia_socios'] = true;
                        $Permisos['calendarizacion_cursos'] = true;
                        $Permisos['administracion_metas'] = true;

                        $this->Herramientas = true;
                        break;

                    default:
                        $Permisos['programacion_estado_grupos'] = true;
                        $Permisos['georeferencia_socios'] = true;
                        $Permisos['georeferencia_socios'] = true;
                        $Permisos['calendarizacion_cursos'] = true;
                        $Permisos['administracion_metas'] = true;

                        $this->Herramientas = true;
                        break;
                }

                /********************************************
                 ** PERMISOS GENERALES SIN IMPORTAR TIPO DE PC
                 **   @@ LGL  2016-06-23
                 *******************************************/
                $Permisos['control_asistencia_lectura'] = false;
                $Permisos['control_asistencia'] = true;
                $Permisos['registro_asistencia'] = true;
                $Permisos['registro_asistencia_lectura'] = false;

                break;

            default:
                error_log('Sin permiso para usuario externo: ' . $RolUsr_Id);
                $this->getUser()->setFlash('notice', 'No tiene permiso para acceder al sistema por este medio');
                $EsAutentidado = false;
        }

        /*
         * Enviamos a su respectiva pantalla de Home y Menu
         */
        $TipoUsuario .= ($TipoPc == sfConfig::get('app_caja_tipo_pc_id')) ? 'Caja' : 'Aula';
        $this->setTemplate(($EsAutentidado) ? 'homeUsuario' : 'sinPermiso');
        $this->getUser()->setAuthenticated($EsAutentidado);
        $this->getUser()->setAttribute('menu', 'menuUsuario');
        $this->getUser()->setAttribute('menuData', $Menu);
        $this->getUser()->setAttribute('usuario', $Usuario);
        $this->getUser()->setAttribute('auth_externo', $EsExterno);
        $this->Permisos = $Permisos;
        $this->NombreHome = $NombreHome;


        /* Notificaciones al Log */
        error_log('Rol de Usuario:      ' . $RolUsr_Id . '              ');
        error_log('Username de Usuario: ' . $Usuario->getUsuario() . '              ');
        error_log('Nombre de Usuario:   ' . $Usuario->getNombreCompleto() . '              ');
        error_log('Tipo de Usuario:     ' . $TipoUsuario . '              ');
        error_log('Tipo de PC:          ' . $TipoPc . '              ');
        error_log('Es autenticado:      ' . $EsAutentidado . '              ');

        /*
          En caso de se asignen las herramintas colavorativas
         */
        if ($this->Herramientas) {
            //Para liga de brainpop en su dash
            $c = CentroPeer::retrieveByPK(sfConfig::get('app_centro_actual_id'));
            $this->centro = strtolower($c->getAlias());
        }
        error_log('------------ executeLogin');
    }

    /**
     * Simple echo para poder mantener viva la sesión de usuario en la maquina.
     * TODO: Investigar más al respecto de como se puede hacer esto sin hacer requests
     *
     */
    public function executeKeepAlive(sfWebRequest $request) {
        $this->getResponse()->setContentType('application/json');
        $ts = date("Y-m-d H:i:s");
        $this->getUser()->setAttribute('ts_ping', $ts);
        $res = array('resultado' => 'ok', 'msg' => 'Se ha ampliado expiración de sesión a las: ' . $ts);
        return $this->renderText(json_encode($res));
    }

    /**
     * Crea una entrada en la tabla de sesion, se ejecuta desde los scripts gdm de los clientes, no desde el browser.
     * Cuando una máquina cliente autentica a un usuario y muiestra su sesión en el escritorio se ejecuta un script
     * que dispara esta acción
     * @param sfWebRequest $request
     * @return bool
     */
    public function executeLogin(sfWebRequest $request) {
        $this->getResponse()->setContentType('text/html');
        $u = strtolower($request->getParameter('usuario'));
        $pc = ComputadoraPeer::getPcPorIp(Comun::ipReal());

        $usuario = UsuarioPeer::getUsuarioPorUsername($u);
        if ($usuario) {
            //Revisamos si en la pc hay sesion, si la hay la eliminamos de la tabla
            $sants = SesionOperadorPeer::getSesionOperadorPorIp($ip);
            foreach ($sants as $sant) {
                if ($sant != null)
                    SesionOperadorPeer::eliminaSesion($sant);
            }

            $sesion = new SesionOperador();
            $sesion->setUsuario($usuario->getUsuario());
            $sesion->setUsuarioId($usuario->getId());
            $sesion->setCentroId(sfConfig::get('app_centro_actual_id'));
            $sesion->setFechaLogin(date("Y-m-d H:i:s"));
            $sesion->setIp(Comun::ipReal());
            $sesion->save();
            error_log("El usuario $u ha iniciado sesion en PC " . Comun::ipReal());

            $this->dispatcher->notify(new sfEvent($this, 'usuario.login', array(
                'sesion' => $sesion
            )));

            return $this->renderText('1');
        }

        $socio = SocioPeer::porNombreUsuario($u);
        if ($socio) {
            //Revisar si existe sesion y si la hay borrarla de la tabla
            $sant = SesionPeer::getSesionPorIp(Comun::ipReal());
            if ($sant != null) {
                $h = SesionPeer::eliminaSesion($sant);
                $this->dispatcher->notify(new sfEvent($this, 'socio.logout', array(
                    'sesion_historico' => $h
                )));
            }

            $ts = date("Y-m-d H:i:s");
            $centro_id = sfConfig::get('app_centro_actual_id');
            $sesion = SesionPeer::crearSesion($socio, $pc, $ts, $centro_id);

            error_log("El socio $u ha iniciado sesion");

            $this->dispatcher->notify(new sfEvent($this, 'socio.login', array(
                'sesion' => $sesion
            )));

            return $this->renderText('1');
        }
        //Si estamos aqui es que no esta el socio registrado en el centro o que es un usuario local de pc, como root o administrador
        //Podemos proceder a importar datos del socio desde el centro.
        elseif ($u != 'root' or $u != 'administrador') {
            error_log("$u NO esta registrado en la base. Tratando de importar desde su centro de origen.");

            if ($this->importarSocio($u)) {
                $sant = SesionPeer::getSesionPorIp(Comun::ipReal());
                if ($sant != null) {
                    $h = SesionPeer::eliminaSesion($sant);
                    $this->dispatcher->notify(new sfEvent($this, 'socio.logout', array(
                        'sesion_historico' => $h
                    )));
                }

                $ts = date("Y-m-d H:i:s");
                $centro_id = sfConfig::get('app_centro_actual_id');
                $socio = SocioPeer::porNombreUsuario($u);
                $sesion = SesionPeer::crearSesion($socio, $pc, $ts, $centro_id);

                error_log("El socio $u ha iniciado sesion");

                $this->dispatcher->notify(new sfEvent($this, 'socio.login', array(
                    'sesion' => $sesion
                )));

                return $this->renderText('1');
            }
        }
        return $this->renderText('0');
    }

    /**
     * Ejecuta el logout cuando se cierra sesion en una pc cliente
     * @param sfWebRequest $request
     * @return raw text
     */
    public function executeLogout(sfWebRequest $request) {

        $this->getResponse()->setContentType('text/html');
        $u = strtolower($request->getParameter('usuario'));
        $ipparam = $request->getParameter('ip');
        if ($ipparam == '') {
            $ip = Comun::ipReal();
        } else {
            $ip = $ipparam;
        }


        $transcurrido = 0;

        //Revisamos si se trata de un usuario
        $usuario = UsuarioPeer::getUsuarioPorUsername($u);
        if ($usuario) {
            //obtenemos la sesion del operador
            $opSes = $usuario->getSesionOperadors();
            if (!count($opSes) > 0) {
                error_log("Ningun $u en sesion");
                return $this->renderText('0');
            }
            foreach ($opSes as $sesion) {
                //Botamos la sesion que corresponde a este usuario en esta ip

                if ($sesion->getIp() == $ip) {

                    $h = SesionOperadorPeer::eliminaSesion($sesion);
                    $this->dispatcher->notify(new sfEvent($this, 'usuario.logout', array(
                        'sesion_historico' => $h
                    )));
                }
            }
            return $this->renderText('1');
        }

        // Revisamos si se trata de un socio
        $socio = SocioPeer::porNombreUsuario($u);
        if ($socio) {
            //obtenemos las sesiones del socio
            $opSes = $socio->getSesions();
            if (!count($opSes) > 0) {
                error_log("-Ningun $u en sesion");
                return $this->renderText('0');
            }
            foreach ($opSes as $sesion) {

                //Botamos la sesion de esta ip de este usuario

                if ($sesion->getIp() == $ip) {
                    $h = SesionPeer::eliminaSesion($sesion);
                    $this->dispatcher->notify(new sfEvent($this, 'socio.logout', array(
                        'sesion_historico' => $h
                    )));
                }

                //error_log($tlogin);
            }
            return $this->renderText('1');
        }

        error_log("El usuario no esta registrado en la base");
        return $this->renderText('0');
    }

    /**
     * Dado un pk de socio regresa una cadena que corresponde al arreglo de datos serializado de las sesiones del socio
     * en este centro.
     *
     * @WSMethod(webservice='MakoApi')
     * @param string $socio_id (opcional)
     * @param string $fecha Fecha (opcional)
     * @param string $operacion Operacion comparacion con la fecha: Criteria::LESS_EQUAL, Criteria::EQUAL, Criteria::LESS_EQUAL, etc (opcional)
     * @param integer $numero_registros Número de registros a consultar (opcional)
     *
     * @return string[] Regresa un arreglo de sesion_historico serializado
     */
    public function executeGetSesiones(sfWebRequest $request) {
        $socio_id = pg_escape_string($request->getParameter('socio_id'));
        $fecha = pg_escape_string($request->getParameter('fecha'));
        $op = pg_escape_string($request->getParameter('operacion'));
        $num = pg_escape_string($request->getParameter('numero_registros'));


        $c = new Criteria();
        if ($fecha != '')
            $c->add(SesionHistoricoPeer::FECHA_LOGIN, $fecha . ' 00:00:00', ($op != '') ? $op : null);
        if ($num != '')
            $c->setLimit($num);

        if ($socio_id != '') {
            $socio = SocioPeer::retrieveByPK($socio_id);
            $c->add(SesionHistoricoPeer::SOCIO_ID, $socio_id);
            if ($socio == null) {
                $me = $this->isSoapRequest() ? new SoapFault('Server', 'No existe el usuario solicitado') : 'No existe el usuario solicitado';
                throw $me;
                return sfView::ERROR;
            }
        }

        $aS = array();
        $sesiones = SesionHistoricoPeer::doSelect($c);
        foreach ($sesiones as $sesion) {
            $aS[] = serialize($sesion->toArray());
        }

        $this->result = $aS;

        return sfView::SUCCESS;
    }

    /**
     * Elimina la sesion del usuario
     * @param sfWebRequest $request
     */
    public function executeLogoutExt(sfWebRequest $request) {
        $this->getUser()->setAuthenticated(false);
        $this->getUser()->setAttribute('menu', null);
        $this->getUser()->setAttribute('usuario', null);
        $this->getUser()->setAttribute('auth_externo', true);
        $this->setTemplate('index');
    }

    /**
     * Se usa para importar los datos de un socio que no este dado de alta en la base local y exista en ldap.
     *
     * @param string $usuario
     * @param int $centro_id
     * @param bigint $socio_id
     */
    public function importarSocio($usuario) {
        error_log("Usr:" . $usuario);
        $this->getContext()->getConfiguration()->loadHelpers('Ldap');
        try {
            $res = getInfoAltaSocio($usuario);
            error_log("Respuesta del ldap: $usuario: " . print_r($res, true));
        } catch (Exception $e) {
            error_log("Error al conectarse al ldap. No es posible determinar de que centro proviene el socio");
        }

        if (is_array($res)) {
            list($centro_id, $socio_id) = $res;
            error_log("Tratando de importar el socio con datos: $centro_id $socio_id");

            $centro = CentroPeer::retrieveByPK($centro_id);

            try {
                $cliente = new SoapClient("http://" . $centro->getIp() . "/MakoApi.wsdl");
            } catch (Exception $e) {
                error_log("Error al conectar cliente de ws " . $e->getMessage());
                return false;
            }

            try {
                $sres = $cliente->registro_getSocio($socio_id);
                $asoc = unserialize($sres);
                $socio = new Socio();
                $socio->fromArray($asoc);
                $socio->save();
                error_log("Se han importado los datos del socio correctamente. ");

                return true;
            } catch (Exception $e) {
                error_log("Error al ejecutar metodo del ws reportes_getReporteDetalleOrdenes. " . $e->getMessage());
                return false;
            }
        } else {
            error_log("No es posible determinar de que centro proviene el socio");
            return false;
        }
    }

}
