<?php
/**
 * Plantilla del home del super usuario
 */
use_stylesheet('home.css');
use_javascript('home/home.js');
?>

<div class="ui-widget ui-widget-content ui-corner-all sombra"
	style="min-width: 500px; min-height: 500px;">
	<div class="ui-widget ui-widget-header tssss" style="padding: 5px;">Home
		Administrador de Operaciones</div>

	<div class="menu-home">

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('registro/index') ?>"> <img
				src="/imgs/iconos/buscar.png" border="0" title="Búsqueda de socios"
				width="64" height="64" /> <br /> <span>Búsqueda de socios</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('dias_laborables/index') ?>"> <img
				src="/imgs/iconos/fecha.png" border="0"
				title="Administrador de días laborables" width="64" height="64" /> <br />
				<span>Administrador de días laborables</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('admin_productos/index') ?>"> <img
				src="/imgs/iconos/correr.png" border="0"
				title="Administrar productos" width="64" height="64" /> <br /> <span>Administrar
					productos</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('admin_descuentos/index') ?>"> <img
				src="/imgs/iconos/correr.png" border="0"
				title="Administrar descuentos" width="64" height="64" /> <br /> <span>Administrar
					descuentos</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada">
			<a href="<?php echo url_for('admin_cupones/index') ?>"> <img
				src="/imgs/iconos/correr.png" border="0" title="Administrar cupones"
				width="64" height="64" /> <br /> <span>Administrar cupones</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('conciliacion_adm/new') ?>"> <img
				src="/imgs/iconos/correr.png" border="0"
				title="Conciliación administrativa" width="64" height="64" /> <br />
				<span>Conciliación administrativa</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('admin_aulas/index') ?>"> <img
				src="/imgs/iconos/correr.png" border="0"
				title="Administrador de recursos" width="64" height="64" /> <br /> <span>Administrador
					de recursos</span>
			</a>
		</div>

		<!-- Reingenieria Mako C. ID 19. Responsable:  FE. Fecha: 17-01-2014.  
                Descripción del cambio: Se activa modulo para los siguientes roles: Supervisores, Encargados y para el Gerente de Operaciones. -->
		<?php if ($sf_user->getAttribute('auth_externo')):?>
		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('salida_caja/index') ?>"> <img
				src="/imgs/iconos/salida_caja.png" border="0"
				title="Historial de salidas de caja" width="64" height="64" /> <br />
				<span>Historial de salidas de caja</span>
			</a>
		</div>
		<!-- Fecha: 17-01-2014 - FIN -->

		<!-- Reingenieria Mako C. ID 20. Responsable:  FE. Fecha: 17-01-2014.  
                Descripción del cambio: Se activa modulo para los siguientes roles: Supervisores, Encargados y para el Gerente de Operaciones. -->
		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('corte_caja/index') ?>"> <img
				src="/imgs/iconos/corte.png" border="0"
				title="Historial Cortes de caja" width="64" height="64" /> <br /> <span>Historial
					de cortes de caja.</span>
			</a>
		</div>
		<?php endif; ?>
		<!-- Fecha: 17-01-2014 - FIN -->

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('metas/index') ?>"> <img
				src="/imgs/iconos/target.png" border="0"
				title="Administración de metas" width="64" height="64" /> <br /> <span>Administración
					de metas</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('reporte/index') ?>"> <img
				src="/imgs/iconos/kchart.png" border="0"
				title="Indicadores de operación" width="64" height="64" /> <br /> <span>Indicadores
					de operación</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('reporte/mapa') ?>"> <img
				src="/imgs/iconos/gmaps.png" border="0" title="Georeferencia Socios"
				width="64" height="64" /> <br /> <span>Georeferencia Socios</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('reporte/indicadores') ?>"> <img
				src="/imgs/iconos/kchart.png" border="0"
				title="Indicadores Registro, Ventas, Inscripción" width="64"
				height="64" /> <br /> <span>Indicadores Registro, Ventas,
					Inscripción</span>
			</a>
		</div>
		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('reporte/indicadoresCapacidad') ?>"> <img
				src="/imgs/iconos/kchart.png" border="0"
				title="Indicadores Capacidad Instalada" width="64" height="64" /> <br />
				<span>Indicadores Capacidad Instalada</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a
				href="<?php echo url_for('reporte/indicadoresDetalleZoom?caso=estatus_socios') ?>">
				<img src="/imgs/iconos/kchart.png" border="0"
				title="Indicadores Estatus Socios" width="64" height="64" /> <br />
				<span>Indicadores Estatus Socios</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('reporte/listaInactivos') ?>"> <img
				src="/imgs/iconos/usuarios_inactivos.png" border="0"
				title="Listas de socios inactivos" width="64" height="64" /> <br />
				<span>Listas de socios inactivos</span>
			</a>
		</div>


		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a
				href="<?php echo url_for('admin_centros/actualizar_coordenadas') ?>">
				<img src="/imgs/iconos/gmaps.png" border="0"
				title="Actualizar coordenadas" width="64" height="64" /> <br /> <span>Actualizar
					coordenas de centros</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('dashboard_reportes/index') ?>"> <img
				src="/imgs/iconos/kchart.png" border="0"
				title="Seguimiento indicadrores" width="64" height="64" /> <br /> <span>Seguimiento
					indicadrores</span>
			</a>
		</div>
	</div>
</div>
