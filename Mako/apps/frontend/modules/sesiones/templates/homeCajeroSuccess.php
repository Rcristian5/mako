<?php
/**
 * Plantilla del home de cajero
 */
use_stylesheet('home.css');
use_javascript('home/home.js');
?>

<div class="ui-widget ui-widget-content ui-corner-all sombra" style="min-width: 500px; min-height: 500px;">
	<div class="ui-widget ui-widget-header ui-corner-tl ui-corner-tr tssss" style="padding: 5px;">Home Cajero</div>
	
	<div class="menu-home" >
	
		
		<div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('pos/new') ?>">
				<img src="/imgs/iconos/venta.png" border="0" title="Punto de venta" width="64" height="64"/>
				<br/>
				<span>Punto de venta</span>
			</a>
		</div>
		<div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('salida_caja/guiAuth') ?>">
				<img src="/imgs/iconos/salida_caja.png" border="0" title="Salida de caja" width="64" height="64"/>
				<br/>
				<span>Salida de caja</span>
			</a>
		</div>
		
		<div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('corte_caja/guiAuth') ?>">
				<img src="/imgs/iconos/corte.png" border="0" title="Corte de caja" width="64" height="64"/>
				<br/>
				<span>Corte de caja</span>
			</a>
		</div>
		
		
		<div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('cancelacion_venta/guiAuth') ?>">
				<img src="/imgs/iconos/cancelar.png" border="0" title="Cancelación de venta" width="64" height="64"/>
				<br/>
				<span>Cancelación de venta</span>
			</a>
		</div>
	
		<div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('registro/new') ?>">
				<img src="/imgs/iconos/usuarios.png" border="0" title="Registro de socios" width="64" height="64"/>
				<br/>
				<span>Registro de socios</span>
			</a>
		</div>
	
		<div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('registro/index') ?>">
				<img src="/imgs/iconos/buscar.png" border="0" title="Búsqueda de socios" width="64" height="64"/>
				<br/>
				<span>Búsqueda de socios</span>
			</a>
		</div>

		<div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('dashsesiones/index') ?>">
				<img src="/imgs/iconos/libre.png" border="0" title="Ocupación de PCs" width="64" height="64"/>
				<br/>
				<span>Ocupación de PCs</span>
			</a>
		</div>
		
		<div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('asignacion_becas/index') ?>">
				<img src="/imgs/iconos/correr.png" border="0" title="Registro de solicitud de becas" width="64" height="64"/>
				<br/>
				<span>Registro de solicitud de becas</span>
			</a>
		</div>
		<div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                            <a href="<?php echo url_for('control/listas') ?>">
                                    <img src="/imgs/iconos/bitacora.png" border="0" title="Programación y estado de grupos" width="64" height="64"/>
                                    <br/>
                                    <span>Programaci&oacute;n y estado de grupos</span>
                            </a>
                </div>

		<div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                        <a href="http://<?php echo $_SERVER['SERVER_ADDR'];?>/forapi/" target="_blank">
                                <img src="/imgs/iconos/correr.png" border="0" title="Control de asistencia" width="64" height="64"/>
                                <br/>
                                <span>Actividades No Académicas</span>
                        </a>
		</div>
		<div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="http://<?php echo $_SERVER['SERVER_ADDR'];?>/forapi/"> 
				<img src="/imgs/iconos/checklist.png" border="0" title="CheckList" width="64" height="64" /> <br /> 
				<span>Checklist</span>
				</a>
		</div>

		
	</div>


</div>
