<?php
/**
 * Plantilla del home del super usuario
 */
use_stylesheet('home.css');
use_javascript('home/home.js');
?>

<div class="ui-widget ui-widget-content ui-corner-all sombra">
    <div class="ui-widget ui-widget-header tssss" style="padding: 5px;"><?php echo $NombreHome ?></div>

    <div class="menu-home clearfix">

        <?php
        if ($Herramientas)
            include_partial('herramientas', array('centro' => $centro));
        ?>

        <?php if ($Permisos['registro_socios']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="<?php echo url_for('registro/new') ?>">
                    <img src="/imgs/iconos/usuarios.png" border="0" title="Registro de socios" width="64" height="64" />
                    <br/> 
                    <span>Registro de socios</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['busqueda_socios']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="<?php echo url_for('registro/index') ?>"> 
                    <img src="/imgs/iconos/buscar.png" border="0" title="Búsqueda de socios" width="64" height="64" />
                    <br/> 
                    <span>Búsqueda de socios</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['ocupacion_pc']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="<?php echo url_for('dashsesiones/index') ?>">
                    <img src="/imgs/iconos/libre.png" border="0" title="Ocupación de PCs" width="64" height="64" />
                    <br/> 
                    <span>Ocupación de PCs</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['programacion_estado_grupos']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="<?php echo url_for('control/listas') ?>"> 
                    <img src="/imgs/iconos/bitacora.png" border="0" title="Programación y estado de grupos" width="64" height="64" />
                    <br />
                    <span>Programaci&oacute;n y estado de grupos</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['punto_venta']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="<?php echo url_for('pos/new') ?>"> 
                    <img src="/imgs/iconos/venta.png" border="0" title="Punto de venta" width="64" height="64" />
                    <br/>
                    <span>Punto de venta</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['cancelacion_venta']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="<?php echo url_for('cancelacion_venta/guiAuth') ?>"> 
                    <img src="/imgs/iconos/cancelar.png" border="0" title="Cancelación de venta" width="64" height="64" />
                    <br />
                    <span>Cancelación de venta</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['movimientos_dia']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="<?php echo url_for('corte_caja/resumenHoy') ?>"> 
                    <img src="/imgs/iconos/buscar.png" border="0" title="Movimientos del Dia" width="64" height="64" />
                    <br/> 
                    <span>Movimientos del dia</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['reporte_vajilla']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="<?php echo url_for('corte_caja/resumenValija') ?>"> 
                    <img src="/imgs/iconos/buscar.png" border="0" title="Reporte de Valija" width="64" height="64" />
                    <br />
                    <span>Reporte de valija</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['salida_caja']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="<?php echo url_for('salida_caja/guiAuth') ?>"> 
                    <img src="/imgs/iconos/salida_caja.png" border="0" title="Salida de caja" width="64" height="64" />
                    <br/>
                    <span>Salida de caja</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['corte_caja']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="<?php echo url_for('corte_caja/guiAuth') ?>">
                    <img src="/imgs/iconos/corte.png" border="0" title="Corte de caja" width="64" height="64" />
                    <br/> 
                    <span>Corte de caja</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['encendido_apagado_pc']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="<?php echo url_for('switch_pcs/index') ?>">
                    <img src="/imgs/iconos/shutdown.png" border="0" title="Administración de energía PCs" width="64" height="64"/>
                    <br/>
                    <span>Encendido y apagado PCs</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['historial_salida']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="<?php echo url_for('salida_caja/index') ?>"> 
                    <img src="/imgs/iconos/salida_caja.png" border="0" title="Historial de salidas de caja" width="64" height="64" /> 
                    <br />
                    <span>Historial de salidas de caja</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['historial_corte']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="<?php echo url_for('corte_caja/index') ?>"> 
                    <img src="/imgs/iconos/corte.png" border="0" title="Historial Cortes de caja" width="64" height="64" />
                    <br />
                    <span>Historial de cortes de caja.</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['georeferencia_socios']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="<?php echo url_for('reporte/mapa') ?>">
                    <img src="/imgs/iconos/gmaps.png" border="0" title="Georeferencia Socios" width="64" height="64" />
                    <br />
                    <span>Georeferencia Socios</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['administracion_secciones']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="<?php echo url_for('admin_secciones/index') ?>"> 
                    <img src="/imgs/iconos/libre.png" border="0" title="Administración de secciones" width="64" height="64" />
                    <br />
                    <span>Administración de secciones</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['administracion_pcs']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="<?php echo url_for('admin_pc/index') ?>"> 
                    <img src="/imgs/iconos/libre.png" border="0" title="Administración de PCs" width="64" height="64" />
                    <br />
                    <span>Administración de PCs</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['actualizar_coordenadas_centro']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="<?php echo url_for('admin_centros/actualizar_coordenadas') ?>">
                    <img src="/imgs/iconos/gmaps.png" border="0" title="Actualizar coordenadas" width="64" height="64" />
                    <br /> 
                    <span>Actualizar coordenas de centros</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['calendarizacion_cursos']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="<?php echo url_for('calendarizacion/index') ?>">
                    <img src="/imgs/iconos/calendarizacion.png" border="0" title="Calendarización de cursos" width="64" height="64" />
                    <br />
                    <span>Calendarización de cursos</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['administracion_metas']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="<?php echo url_for('metas/index') ?>"> 
                    <img src="/imgs/iconos/target.png" border="0" title="Administración de metas" width="64" height="64" /> 
                    <br />
                    <span>Administración de metas</span>
                </a>
            </div>
        <?php } ?>


        <?php /*
         * Módulos de Corporativo
         */ ?>
        <?php if ($Permisos['solicitud_beca']) { ?>
        <?php } ?>

        <?php if ($Permisos['administracion_centros']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="<?php echo url_for('admin_centros/index') ?>">
                    <img src="/imgs/iconos/home.png" border="0" title="Administración de centros" width="64" height="64" />
                    <br />
                    <span>Administración de centros</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['folio_fiscal_centro']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="<?php echo url_for('admin_foliofiscal/index') ?>">
                    <img src="/imgs/iconos/correr.png" border="0" title="Administración del folio fiscal de centro" width="64" height="64" />
                    <br />
                    <span>Folio fiscal de centro</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['administrar_productos']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="<?php echo url_for('admin_productos/index') ?>"> 
                    <img src="/imgs/iconos/correr.png" border="0" title="Administrar productos" width="64" height="64" />
                    <br />
                    <span>Administrar productos</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['administrar_descuentos']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="<?php echo url_for('admin_descuentos/index') ?>">
                    <img src="/imgs/iconos/correr.png" border="0" title="Administrar descuentos" width="64" height="64" />
                    <br />
                    <span>Administrar descuentos</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['administrar_cupones']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada">
                <a href="<?php echo url_for('admin_cupones/index') ?>">
                    <img src="/imgs/iconos/correr.png" border="0" title="Administrar cupones" width="64" height="64" />
                    <br />
                    <span>Administrar cupones</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['administrar_dias_laborables']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="<?php echo url_for('dias_laborables/index') ?>">
                    <img src="/imgs/iconos/fecha.png" border="0" title="Administrador de días laborables" width="64" height="64" />
                    <br />
                    <span>Administrador de días laborables</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['administrar_usuarios']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="<?php echo url_for('admin_usuarios/new') ?>">
                    <img src="/imgs/iconos/correr.png" border="0" title="Administrar usuarios" width="64" height="64"/>
                    <br/>
                    <span>Administrar usuarios</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['administrador_vacaciones']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="<?php echo url_for('admin_vacaciones/index') ?>">
                    <img src="/imgs/iconos/cal.png" border="0" title="Administrador de vacaciones" width="64" height="64" />
                    <br />
                    <span>Administrador de vacaciones</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['administrador_categorias']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="<?php echo url_for('categoria_curso/index') ?>">
                    <img src="/imgs/iconos/admin_categorias.png" border="0" title="Administrador de categorías de curso" width="64" height="64" />
                    <br />
                    <span>Administrador de categor&iacute;as</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['carga_cursos']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="<?php echo url_for('curso/index') ?>">
                    <img src="/imgs/iconos/carga_curso.png" border="0" title="Carga de cursos" width="64" height="64" />
                    <br />
                    <span>Carga de cursos</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['verificacion_cursos']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="<?php echo url_for('verificacion_curso/index') ?>">
                    <img src="/imgs/iconos/clean.png" border="0" title="Verificación de cursos" width="64" height="64" />
                    <br />
                    <span>Verificaci&oacute;n de cursos</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['modelador_temarios']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="<?php echo url_for('modelador/index') ?>">
                    <img src="/imgs/iconos/modelador_curso.png" border="0" title="Modelador de temarios" width="64" height="64" />
                    <br />
                    <span>Modelador de temarios</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['administrador_aulas']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="<?php echo url_for('admin_aulas/index') ?>">
                    <img src="/imgs/iconos/admin.png" border="0" title="Administrador de aulas" width="64" height="64" />
                    <br />
                    <span>Administrador de aulas</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['consulta_socios_inscritos']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="<?php echo url_for('reportes_control_escolar/consulta_inscritos') ?>">
                    <img src="/imgs/iconos/buscar_doc.png" border="0" title="Consulta de socios inscritos a curso por grupo" width="64" height="64" />
                    <br />
                    <span>Consulta de socios inscritos</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['ranking_ria']) { ?>
        <?php } ?>

        <?php if ($Permisos['ubicacion_centros']) { ?>
        <?php } ?>

        <?php if ($Permisos['busqueda_categoria']) { ?>
        <?php } ?>

        <?php if ($Permisos['cancelacion_grupo']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="<?php echo url_for('cancelacion_grupo/index') ?>">
                    <img src="/imgs/iconos/cancelar_grupo.png" border="0" title="Cancelación de grupo" width="64" height="64" />
                    <br />
                    <span>Cancelaci&oacute;n de grupo</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['reporte_socios_inscritos']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="<?php echo url_for('reportes_control_escolar/socios_curso') ?>">
                    <img src="/imgs/iconos/bitacora.png" border="0" title="Reporte mensual de socios inscritos a cursos" width="64" height="64" />
                    <br />
                    <span>Reporte socios inscritos a cursos</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['reporte_asistencias']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="<?php echo url_for('reportes_control_escolar/asistencias') ?>">
                    <img src="/imgs/iconos/asistencia.png" border="0" title="Reporte de asistencias" width="64" height="64" />
                    <br />
                    <span>Reporte de asistencias</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['reporte_calificaciones']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="<?php echo url_for('reportes_control_escolar/calificaciones') ?>">
                    <img src="/imgs/iconos/evaluaciones.png" border="0" title="Reporte de calificaciones" width="64" height="64" />
                    <br /> <span>Reporte de calificaciones</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['historial_academico']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="<?php echo url_for('reportes_control_escolar/historial_academico') ?>">
                    <img src="/imgs/iconos/usuarios.png" border="0" title="Busqueda de historial académico" width="64" height="64" />
                    <br />
                    <span>Historial Acad&eacute;mico</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['seguimiento_indicadores']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="<?php echo url_for('dashboard_reportes/index') ?>">
                    <img src="/imgs/iconos/kchart.png" border="0" title="Seguimiento indicadrores" width="64" height="64" />
                    <br />
                    <span>Seguimiento indicadrores</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['indicadores_operacion']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="<?php echo url_for('reporte/index') ?>">
                    <img src="/imgs/iconos/kchart.png" border="0" title="Indicadores de operación" width="64" height="64" />
                    <br />
                    <span>Indicadores de operación</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['indicadores_registro_ventas_inscripcion']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="<?php echo url_for('reporte/indicadores') ?>">
                    <img src="/imgs/iconos/kchart.png" border="0" title="Indicadores Registro, Ventas, Inscripción" width="64" height="64" />
                    <br />
                    <span>Indicadores Registro, Ventas, Inscripción</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['indicadores_capacidad_instalada']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="<?php echo url_for('reporte/indicadoresCapacidad') ?>">
                    <img src="/imgs/iconos/kchart.png" border="0" title="Indicadores Capacidad Instalada" width="64" height="64" />
                    <br />
                    <span>Indicadores Capacidad Instalada</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['indicadores_estatus_socios']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="<?php echo url_for('reporte/indicadoresDetalleZoom?caso=estatus_socios') ?>">
                    <img src="/imgs/iconos/kchart.png" border="0" title="Indicadores Estatus Socios" width="64" height="64" />
                    <br />
                    <span>Indicadores Estatus Socios</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['lista_socios_inactivos']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="<?php echo url_for('reporte/listaInactivos') ?>">
                    <img src="/imgs/iconos/usuarios_inactivos.png" border="0" title="Listas de socios inactivos" width="64" height="64" />
                    <br />
                    <span>Listas de socios inactivos</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['conciliacion_administrativa']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="<?php echo url_for('conciliacion_adm/new') ?>">
                    <img src="/imgs/iconos/correr.png" border="0" title="Conciliación administrativa" width="64" height="64" />
                    <br />
                    <span>Conciliación administrativa</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['administracion_recursos']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="<?php echo url_for('admin_aulas/index') ?>">
                    <img src="/imgs/iconos/correr.png" border="0" title="Administrador de recursos" width="64" height="64" />
                    <br /> <span>Administrador de recursos</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['categorias_productos']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="<?php echo url_for('admin_catrep/index') ?>">
                    <img src="/imgs/iconos/correr.png" border="0" title="Admin. de categorías de productos para reportes" width="64" height="64" />
                    <br />
                    <span>Categorías de productos</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['control_asistencia']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="/asistencia/index.php?oprid=<?php echo sfContext::getInstance()->getUser()->getAttribute('usuario')->getId() ?>">
                    <img src="/imgs/iconos/correr.png" border="0" title="Control de asistencia" width="64" height="64"/>
                    <br/>
                    <span>Control de Asistencia Extempor&aacute;nea</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['control_asistencia_lectura']) { ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="/asistencia/historico_facilitador.php">
                    <img src="/imgs/iconos/correr.png" border="0" title="Control de asistencia" width="64" height="64"/>
                    <br/>
                    <span>Control de Asistencia Extempor&aacute;</span>
                </a>
            </div>
        <?php } ?>
        <?php if ($Permisos['registro_asistencia']){ ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <!--<a href="/asistencia/index.php">-->
                <a href="/asistencia/index.php?oprid=<?php echo sfContext::getInstance()->getUser()->getAttribute('usuario')->getId() ?>&tpas=Registro">
                    <img src="/imgs/iconos/correr.png" border="0" title="Pase de Lista" width="64" height="64"/>
                    <br/>
                    <span>Registro de Asistencia</span>
                </a>
            </div>
        <?php } ?>
        <?php if ($Permisos['registro_asistencia_lectura']){ ?>
            <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <a href="/asistencia/historico_facilitador.php">
                    <img src="/imgs/iconos/correr.png" border="0" title="Pase de Lista" width="64" height="64"/>
                    <br/>
                    <span>Registro de Asistencia</span>
                </a>
            </div>
        <?php } ?>

        <?php if ($Permisos['']) { ?>

        <?php } ?>

    </div>
</div>
</div>
