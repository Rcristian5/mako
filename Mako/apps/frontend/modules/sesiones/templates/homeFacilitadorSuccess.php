<?php
/**
 * Plantilla del home de cajero
 */
use_stylesheet('home.css');
use_javascript('home/home.js');
?>

 <!--
                /*****************************************************
        			Propuesta roles y permisos 2014
        			Responsable: IMB.
        			Fecha: 10-09-2014. 
        			Cambio en los roles y perfiles, segun el documento
       			******************************************************/ 
       			-->

<div class="ui-widget ui-widget-content ui-corner-all sombra"
	style="min-width: 500px; min-height: 500px;">
	<div class="ui-widget ui-widget-header ui-corner-tl ui-corner-tr tssss"
		style="padding: 5px;">Home Facilitador</div>

	<div class="menu-home">

		<?php include_partial('herramientas', array('centro'=>$centro))?>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('registro/index') ?>"> <img
				src="/imgs/iconos/buscar.png" border="0" title="Búsqueda de socios"
				width="64" height="64" /> <br /> <span>Búsqueda de socios</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('dashsesiones/index') ?>"> <img
				src="/imgs/iconos/libre.png" border="0" title="Ocupación de PCs"
				width="64" height="64" /> <br /> <span>Ocupación de PCs</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('control/listas') ?>"> <img
				src="/imgs/iconos/bitacora.png" border="0"
				title="Programación y estado de grupos" width="64" height="64" /> <br />
				<span>Programaci&oacute;n y estado de grupos</span>
			</a>
		</div>

                <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                        <a href="/asistencia/historico_facilitador.php">
                                <img src="/imgs/iconos/correr.png" border="0" title="Control de asistencia" width="64" height="64"/>
                                <br/>
                                <span>Control de Asistencia</span>
                        </a>
                </div>



		<!-- Reingenieria Mako C. ID 38. Responsable:  FE. Fecha: 17-01-2014.  Descripción del cambio: Se desactiva modulo para los siguientes roles: Todos excepto superusuario. -->
		<!--<div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="< ?php echo url_for('asignacion_becas/index') ?>">
				<img src="/imgs/iconos/correr.png" border="0" title="Registro de solicitud de becas" width="64" height="64"/>
				<br/>
				<span>Registro de solicitud de becas</span>
			</a>
		</div>-->
		<!-- Fecha: 17-01-2014 - FIN -->


	</div>
