<!-- Herramientas colaborativas -->

<div class="divisor ui-widget ui-widget-header" align="center">
	Herramientas colaborativas</div>

<div
	class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
	<a href="http://lms.edomex.proacceso.mx/" target="_blank"> <img
		src="/imgs/moodle.png" border="0" title="Moodle Socios" width="64"
		height="64" /> <br /> <span>Moodle Socios</span>
	</a>
</div>
<?php /*
 * Se quita el modulo KT knowledgetree, a solicitud de Euler Sanchez e Isabel Baxin.
<div
	class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
	<a href="https://knowledgetree.enova.mx:8085/" target="_blank"> <img
		src="/imgs/kt.png" border="0" title="Knowledge Tree (KT)" width="64"
		height="64" /> <br /> <span>Knowledge Tree (KT)</span>
	</a>
</div>*/?>

<!--
                /*****************************************************
        			Propuesta roles y permisos 2014
        			Responsable: IMB.
        			Fecha: 14-10-2014. 
        			Cambio en los roles y perfiles, segun el documento
       			******************************************************/ 
-->

<div
	class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
	<a href="http://mail.google.com/" target="_blank"> <img
		src="/imgs/email2.png" border="0" title="Gmail" width="64"
		height="64" /> <br /> <span>Gmail</span>
	</a>
</div>


<div
	class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
	<a href="https://wordpress.edomex.proacceso.mx" target="_blank"> <img
		src="/imgs/wp.png" border="0" title="Blog interno" width="64"
		height="64" /> <br /> <span>Blog interno</span>
	</a>
</div>

<div
	class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
	<a href="https://lms.enova.mx:8088/" target="_blank"> <img
		src="/imgs/moodle_capa.png" border="0" title="Moodle Capacitación"
		width="64" height="64" /> <br /> <span>Capacitación</span>
	</a>
</div>

<div
	class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
	<a href="https://mantis.enova.mx:8083/" target="_blank"> <img
		src="/imgs/mantis.gif" border="0" title="Mantis" width="64"
		height="64" /> <br /> <span>Mantis</span>
	</a>
</div>

<div
	class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
	<a
		href="http://esp.brainpop.com/user/loginDo.weml?user=ria-<?php echo $centro ?>&password=ria"
		target="_blank"> <img src="/imgs/brainpop.gif" border="0"
		title="BrainPop" width="64" height="64" /> <br /> <span>BrainPop</span>
	</a>
</div>

<div
	class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
	<a
		href="http://www.brainpopesl.com/user/loginDo.weml?user=ria-<?php echo $centro ?>&password=ria"
		target="_blank"> <img src="/imgs/brainpop_esl.gif" border="0"
		title="BrainPop ESL" width="64" height="64" /> <br /> <span>BrainPop
			ESL</span>
	</a>
</div>

<!-- Fin herramientas -->
<div class="clearfix"></div>
<div class="divisor ui-widget ui-widget-header" align="center">Módulos
	Mako</div>
