<?php
/**
 * Plantilla del home de cajero
 */
use_stylesheet('home.css');
use_javascript('home/home.js');
?>

<div class="ui-widget ui-widget-content ui-corner-all sombra"
	style="min-width: 500px; min-height: 500px;">
	<div class="ui-widget ui-widget-header ui-corner-tl ui-corner-tr tssss"
		style="padding: 5px;">Home Coordinador Académico</div>

	<div class="menu-home">


		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('modelador/index') ?>"> <img
				src="/imgs/iconos/modelador_curso.png" border="0"
				title="Modelador de temarios" width="64" height="64" /> <br /> <span>Modelador
					de temarios</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('curso/index') ?>"> <img
				src="/imgs/iconos/carga_curso.png" border="0"
				title="Carga de cursos" width="64" height="64" /> <br /> <span>Carga
					de cursos</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('calendarizacion/index') ?>"> <img
				src="/imgs/iconos/calendarizacion.png" border="0"
				title="Calendarización de cursos" width="64" height="64" /> <br /> <span>Calendarización
					de cursos</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('admin_aulas/index') ?>"> <img
				src="/imgs/iconos/admin.png" border="0"
				title="Administrador de aulas" width="64" height="64" /> <br /> <span>Administrador
					de aulas</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('admin_vacaciones/index') ?>"> <img
				src="/imgs/iconos/cal.png" border="0"
				title="Administrador de vacaciones" width="64" height="64" /> <br />
				<span>Administrador de vacaciones</span>
			</a>
		</div>
		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a
				href="<?php echo url_for('reportes_control_escolar/socios_curso') ?>">
				<img src="/imgs/iconos/bitacora.png" border="0"
				title="Reporte mensual de socios inscritos a cursos" width="64"
				height="64" /> <br /> <span>Reporte socios inscritos a cursos</span>
			</a>
		</div>
		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a
				href="<?php echo url_for('reportes_control_escolar/consulta_inscritos') ?>">
				<img src="/imgs/iconos/buscar_doc.png" border="0"
				title="Consulta de socios inscritos a curso por grupo" width="64"
				height="64" /> <br /> <span>Consulta de socios inscritos</span>
			</a>
		</div>
		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a
				href="<?php echo url_for('reportes_control_escolar/asistencias') ?>">
				<img src="/imgs/iconos/asistencia.png" border="0"
				title="Reporte de asistencias" width="64" height="64" /> <br /> <span>Reporte
					de asistencias</span>
			</a>
		</div>
		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a
				href="<?php echo url_for('reportes_control_escolar/calificaciones') ?>">
				<img src="/imgs/iconos/evaluaciones.png" border="0"
				title="Reporte de calificaciones" width="64" height="64" /> <br /> <span>Reporte
					de calificaciones</span>
			</a>
		</div>
		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a
				href="<?php echo url_for('reportes_control_escolar/historial_academico') ?>">
				<img src="/imgs/iconos/usuario.png" border="0"
				title="Busqueda de historial académico" width="64" height="64" /> <br />
				<span>Hisotrial Academico</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('reporte/index') ?>"> <img
				src="/imgs/iconos/kchart.png" border="0"
				title="Indicadores de operación" width="64" height="64" /> <br /> <span>Indicadores
					de operación</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('reporte/indicadores') ?>"> <img
				src="/imgs/iconos/kchart.png" border="0"
				title="Indicadores Registro, Ventas, Inscripción" width="64"
				height="64" /> <br /> <span>Indicadores Registro, Ventas,
					Inscripción</span>
			</a>
		</div>
		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('reporte/indicadoresCapacidad') ?>"> <img
				src="/imgs/iconos/kchart.png" border="0"
				title="Indicadores Capacidad Instalada" width="64" height="64" /> <br />
				<span>Indicadores Capacidad Instalada</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a
				href="<?php echo url_for('reporte/indicadoresDetalleZoom?caso=estatus_socios') ?>">
				<img src="/imgs/iconos/kchart.png" border="0"
				title="Indicadores Estatus Socios" width="64" height="64" /> <br />
				<span>Indicadores Estatus Socios</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('reporte/listaInactivos') ?>"> <img
				src="/imgs/iconos/usuarios_inactivos.png" border="0"
				title="Listas de socios inactivos" width="64" height="64" /> <br />
				<span>Listas de socios inactivos</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('dashboard_reportes/index') ?>"> <img
				src="/imgs/iconos/kchart.png" border="0"
				title="Seguimiento indicadrores" width="64" height="64" /> <br /> <span>Seguimiento
					indicadrores</span>
			</a>
		</div>

	</div>
</div>
