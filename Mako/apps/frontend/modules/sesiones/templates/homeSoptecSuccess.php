<?php
/**
 * Plantilla del home del super usuario
 */
use_stylesheet('home.css');
use_javascript('home/home.js');
?>

<div class="ui-widget ui-widget-content ui-corner-all sombra"
	style="min-width: 500px; min-height: 500px;">
	<div class="ui-widget ui-widget-header tssss" style="padding: 5px;">Home
		Soporte técnico</div>

	<div class="menu-home clearfix">

		<?php include_partial('herramientas', array('centro'=>$centro))?>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('registro/index') ?>"> <img
				src="/imgs/iconos/buscar.png" border="0" title="Búsqueda de socios"
				width="64" height="64" /> <br /> <span>Búsqueda de socios</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('dashsesiones/index') ?>"> <img
				src="/imgs/iconos/libre.png" border="0" title="Ocupación de PCs"
				width="64" height="64" /> <br /> <span>Ocupación de PCs</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('admin_pc/index') ?>"> <img
				src="/imgs/iconos/libre.png" border="0"
				title="Administración de PCs" width="64" height="64" /> <br /> <span>Administración
					de PCs</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('admin_secciones/index') ?>"> <img
				src="/imgs/iconos/libre.png" border="0"
				title="Administración de secciones" width="64" height="64" /> <br />
				<span>Administración de secciones</span>
			</a>
		</div>
		<?php if ($sf_user->getAttribute('auth_externo') && sfConfig::get('app_centro_actual_id') == sfConfig::get('app_central_id')):?>
		<!-- Se desactiva módulo para Mako's locales-->
		<!--<div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php //echo url_for('admin_usuarios/new') ?>">
				<img src="/imgs/iconos/correr.png" border="0" title="Administrar usuarios" width="64" height="64"/>
				<br/>
				<span>Administrar usuarios</span>
			</a>
		</div>-->
<!-- Se comenta segun requerimiento documento Liberacion Mako Reingenieria 01/12/2014 responsable:IMB 
		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('admin_centros/index') ?>"> <img
				src="/imgs/iconos/home.png" border="0"
				title="Administración de centros" width="64" height="64" /> <br /> <span>Administración
					de centros</span>
			</a>
		</div>
		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('admin_productos/index') ?>"> <img
				src="/imgs/iconos/correr.png" border="0"
				title="Administración de Productos" width="64" height="64" /> <br />
				<span>Administración de Productos</span>
			</a>
		</div>
-->
		<?php endif; ?>
		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('switch_pcs/index') ?>"> <img
				src="/imgs/iconos/shutdown.png" border="0"
				title="Administración de energía PCs" width="64" height="64" /> <br />
				<span>Encendido y apagado PCs</span>
			</a>
		</div>

		<?php if ($sf_user->getAttribute('auth_externo')):?>
		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('salida_caja/index') ?>"> <img
				src="/imgs/iconos/salida_caja.png" border="0"
				title="Historial de salidas de caja" width="64" height="64" /> <br />
				<span>Historial de salidas de caja</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('corte_caja/index') ?>"> <img
				src="/imgs/iconos/corte.png" border="0"
				title="Historial Cortes de caja" width="64" height="64" /> <br /> <span>Historial
					de cortes de caja.</span>
			</a>
		</div>
		<?php endif; ?>

<!-- Se comenta segun requerimiento documento Liberacion Mako Reingenieria 01/12/2014 responsable:IMB 

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('reporte/indicadores') ?>"> <img
				src="/imgs/iconos/kchart.png" border="0"
				title="Indicadores Registro, Ventas, Inscripción" width="64"
				height="64" /> <br /> <span>Indicadores Registro, Ventas,
					Inscripción</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('reporte/indicadoresCapacidad') ?>"> <img
				src="/imgs/iconos/kchart.png" border="0"
				title="Indicadores Capacidad Instalada" width="64" height="64" /> <br />
				<span>Indicadores Capacidad Instalada</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a
				href="<?php echo url_for('reporte/indicadoresDetalleZoom?caso=estatus_socios') ?>">
				<img src="/imgs/iconos/kchart.png" border="0"
				title="Indicadores Estatus Socios" width="64" height="64" /> <br />
				<span>Indicadores Estatus Socios</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('reporte/index') ?>"> <img
				src="/imgs/iconos/target.png" border="0"
				title="Alcance y seguimiento a metas" width="64" height="64" /> <br />
				<span>Alcance y seguimiento a metas</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('reporte/listaInactivos') ?>"> <img
				src="/imgs/iconos/usuarios_inactivos.png" border="0"
				title="Listas de socios inactivos" width="64" height="64" /> <br />
				<span>Listas de socios inactivos</span>
			</a>
		</div>
-->

		<?php if ($sf_user->getAttribute('auth_externo') && sfConfig::get('app_centro_actual_id') != sfConfig::get('app_central_id')):?>
		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('control/listas') ?>"> <img
				src="/imgs/iconos/bitacora.png" border="0"
				title="Programación y estado de grupos" width="64" height="64" /> <br />
				<span>Programaci&oacute;n y estado de grupos</span>
			</a>
		</div>
		<?php endif; ?>


		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a
				href="<?php echo url_for('admin_centros/actualizar_coordenadas') ?>">
				<img src="/imgs/iconos/gmaps.png" border="0"
				title="Actualizar coordenadas" width="64" height="64" /> <br /> <span>Actualizar
					coordenas de centros</span>
			</a>
		</div>

	</div>
</div>
