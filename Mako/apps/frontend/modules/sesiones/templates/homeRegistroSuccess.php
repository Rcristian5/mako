<?php
/**
 * Plantilla del home del promotor
 */
use_stylesheet('home.css');
use_javascript('home/home.js');
?>

<div class="ui-widget ui-widget-content ui-corner-all sombra" style="min-width: 500px; min-height: 500px; padding: 5px;">
	<div class="ui-widget ui-widget-header ui-corner-all tssss" style="padding: 5px;">Home Registro</div>
	
	<div class="menu-home">
	
		<div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('registro/new') ?>">
				<img src="/imgs/iconos/usuarios.png" border="0" title="Registro de socios" width="64" height="64"/>
				<br/>
				<span>Registro de socios</span>
			</a>
		</div>
	
		<div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('registro/index') ?>">
				<img src="/imgs/iconos/buscar.png" border="0" title="Búsqueda de socios" width="64" height="64"/>
				<br/>
				<span>Búsqueda de socios</span>
			</a>
		</div>

		<div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('asignacion_becas/index') ?>">
				<img src="/imgs/iconos/correr.png" border="0" title="Registro de solicitud de becas" width="64" height="64"/>
				<br/>
				<span>Registro de solicitud de becas</span>
			</a>
		</div>

        <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
            <a href="<?php echo url_for('control/listas') ?>">
            	<img src="/imgs/iconos/bitacora.png" border="0" title="Programación y estado de grupos" width="64" height="64"/>
                <br/>
                <span>Programaci&oacute;n y estado de grupos</span>
			</a>
		</div>
                
		<div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('dashsesiones/index') ?>">
				<img src="/imgs/iconos/libre.png" border="0" title="Ocupación de PCs" width="64" height="64"/>
				<br/>
				<span>Ocupación de PCs</span>
			</a>
		</div>

		<div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('switch_pcs/index') ?>">
				<img src="/imgs/iconos/shutdown.png" border="0" title="Administración de energía PCs" width="64" height="64"/>
				<br/>
				<span>Encendido y apagado PCs</span>
			</a>
		</div>
          
		<div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                        <a href="http://<?php echo $_SERVER['SERVER_ADDR'];?>/forapi/" target="_blank">
                                <img src="/imgs/iconos/correr.png" border="0" title="Control de asistencia" width="64" height="64"/>
                                <br/>
                                <span>Actividades No Académicas</span>
                        </a>
		</div>
		<div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="http://<?php echo $_SERVER['SERVER_ADDR'];?>/forapi/"> 
				<img src="/imgs/iconos/checklist.png" border="0" title="CheckList" width="64" height="64" /> <br /> 
				<span>Checklist</span>
				</a>
		</div>

		
	</div>

</div>
