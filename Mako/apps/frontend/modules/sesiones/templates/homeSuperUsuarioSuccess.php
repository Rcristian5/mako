<?php
/**
 * Plantilla del home del super usuario
 */
use_stylesheet('home.css');
use_javascript('home/home.js');
?>

<div class="ui-widget ui-widget-content ui-corner-all sombra">
	<div class="ui-widget ui-widget-header tssss" style="padding: 5px;">Home
		Super Usuario</div>

	<div class="menu-home clearfix">

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('dias_laborables/index') ?>"> <img
				src="/imgs/iconos/fecha.png" border="0"
				title="Administrador de días laborables" width="64" height="64" /> <br />
				<span>Administrador de días laborables</span>
			</a>
		</div>

		<!-- SE deshabilita para local
		<div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php //echo url_for('admin_usuarios/new') ?>">
				<img src="/imgs/iconos/correr.png" border="0" title="Administrar usuarios" width="64" height="64"/>
				<br/>
				<span>Administrar usuarios</span>
			</a>
		</div>
		!-->

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('admin_productos/index') ?>"> <img
				src="/imgs/iconos/correr.png" border="0"
				title="Administrar productos" width="64" height="64" /> <br /> <span>Administrar
					productos</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('admin_descuentos/index') ?>"> <img
				src="/imgs/iconos/correr.png" border="0"
				title="Administrar descuentos" width="64" height="64" /> <br /> <span>Administrar
					descuentos</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada">
			<a href="<?php echo url_for('admin_cupones/index') ?>"> <img
				src="/imgs/iconos/correr.png" border="0" title="Administrar cupones"
				width="64" height="64" /> <br /> <span>Administrar cupones</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('conciliacion_adm/new') ?>"> <img
				src="/imgs/iconos/correr.png" border="0"
				title="Conciliación administrativa" width="64" height="64" /> <br />
				<span>Conciliación administrativa</span>
			</a>
		</div>


		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('pos/new') ?>"> <img
				src="/imgs/iconos/venta.png" border="0" title="Punto de venta"
				width="64" height="64" /> <br /> <span>Punto de venta</span>
			</a>
		</div>
		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('salida_caja/guiAuth') ?>"> <img
				src="/imgs/iconos/salida_caja.png" border="0" title="Salida de caja"
				width="64" height="64" /> <br /> <span>Salida de caja</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('corte_caja/guiAuth') ?>"> <img
				src="/imgs/iconos/corte.png" border="0" title="Corte de caja"
				width="64" height="64" /> <br /> <span>Corte de caja</span>
			</a>
		</div>


		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('cancelacion_venta/guiAuth') ?>"> <img
				src="/imgs/iconos/cancelar.png" border="0"
				title="Cancelación de venta" width="64" height="64" /> <br /> <span>Cancelación
					de venta</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('registro/new') ?>"> <img
				src="/imgs/iconos/usuarios.png" border="0"
				title="Registro de socios" width="64" height="64" /> <br /> <span>Registro
					de socios</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('registro/index') ?>"> <img
				src="/imgs/iconos/buscar.png" border="0" title="Búsqueda de socios"
				width="64" height="64" /> <br /> <span>Búsqueda de socios</span>
			</a>
		</div>


		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('dashsesiones/index') ?>"> <img
				src="/imgs/iconos/libre.png" border="0" title="Ocupación de PCs"
				width="64" height="64" /> <br /> <span>Ocupación de PCs</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('admin_pc/index') ?>"> <img
				src="/imgs/iconos/libre.png" border="0"
				title="Administración de PCs" width="64" height="64" /> <br /> <span>Administración
					de PCs</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('admin_secciones/index') ?>"> <img
				src="/imgs/iconos/libre.png" border="0"
				title="Administración de secciones" width="64" height="64" /> <br />
				<span>Administración de secciones</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('switch_pcs/index') ?>"> <img
				src="/imgs/iconos/shutdown.png" border="0"
				title="Administración de energía PCs" width="64" height="64" /> <br />
				<span>Encendido y apagado PCs</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('asignacion_becas/index') ?>"> <img
				src="/imgs/iconos/correr.png" border="0"
				title="Registro de solicitud de becas" width="64" height="64" /> <br />
				<span>Registro de solicitud de becas</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('control/listas') ?>"> <img
				src="/imgs/iconos/bitacora.png" border="0"
				title="Programación y estado de grupos" width="64" height="64" /> <br />
				<span>Programaci&oacute;n y estado de grupos</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('admin_foliofiscal/index') ?>"> <img
				src="/imgs/iconos/correr.png" border="0"
				title="Administración del folio fiscal de centro" width="64"
				height="64" /> <br /> <span>Folio fiscal de centro</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('admin_centros/index') ?>"> <img
				src="/imgs/iconos/home.png" border="0"
				title="Administración de centros" width="64" height="64" /> <br /> <span>Administración
					de centros</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('admin_catrep/index') ?>"> <img
				src="/imgs/iconos/correr.png" border="0"
				title="Admin. de categorías de productos para reportes" width="64"
				height="64" /> <br /> <span>Categorías de productos</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('metas/index') ?>"> <img
				src="/imgs/iconos/target.png" border="0"
				title="Administración de metas" width="64" height="64" /> <br /> <span>Administración
					de metas</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('reporte/index') ?>"> <img
				src="/imgs/iconos/kchart.png" border="0"
				title="Indicadores de operación" width="64" height="64" /> <br /> <span>Indicadores
					de operación</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('reporte/mapa') ?>"> <img
				src="/imgs/iconos/gmaps.png" border="0" title="Georeferencia Socios"
				width="64" height="64" /> <br /> <span>Georeferencia Socios</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('reporte/indicadores') ?>"> <img
				src="/imgs/iconos/kchart.png" border="0"
				title="Indicadores Registro, Ventas, Inscripción" width="64"
				height="64" /> <br /> <span>Indicadores Registro, Ventas,
					Inscripción</span>
			</a>
		</div>
		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('reporte/indicadoresCapacidad') ?>"> <img
				src="/imgs/iconos/kchart.png" border="0"
				title="Indicadores Capacidad Instalada" width="64" height="64" /> <br />
				<span>Indicadores Capacidad Instalada</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a
				href="<?php echo url_for('reporte/indicadoresDetalleZoom?caso=estatus_socios') ?>">
				<img src="/imgs/iconos/kchart.png" border="0"
				title="Indicadores Estatus Socios" width="64" height="64" /> <br />
				<span>Indicadores Estatus Socios</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('reporte/listaInactivos') ?>"> <img
				src="/imgs/iconos/usuarios_inactivos.png" border="0"
				title="Listas de socios inactivos" width="64" height="64" /> <br />
				<span>Listas de socios inactivos</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a
				href="<?php echo url_for('admin_centros/actualizar_coordenadas') ?>">
				<img src="/imgs/iconos/gmaps.png" border="0"
				title="Actualizar coordenadas" width="64" height="64" /> <br /> <span>Actualizar
					coordenas de centros</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('dashboard_reportes/index') ?>"> <img
				src="/imgs/iconos/kchart.png" border="0"
				title="Seguimiento indicadrores" width="64" height="64" /> <br /> <span>Seguimiento
					indicadrores</span>
			</a>
                </div>

                <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <!--<a href="/asistencia/index.php">-->
                        <a href="/asistencia/index.php?oprid=<?php echo sfContext::getInstance()->getUser()->getAttribute('usuario')->getId() ?>">
                        <img src="/imgs/iconos/correr.png" border="0" title="Control de asistencia" width="64" height="64"/>
                        <br/>
                        <span>Control de Asistencia</span>
                        </a>
                </div>

	</div>
</div>
