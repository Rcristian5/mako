<?php
/**
 * Plantilla del home del supervisor de estadisticas
 */
use_stylesheet('home.css');
use_javascript('home/home.js');
?>

<div class="ui-widget ui-widget-content ui-corner-all sombra"
	style="min-width: 500px; min-height: 500px;">
	<div class="ui-widget ui-widget-header tssss" style="padding: 5px;">Home
		Supervisión de Indicadores.</div>

	<div class="menu-home">


		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('reporte/indicadores') ?>"> <img
				src="/imgs/iconos/kchart.png" border="0"
				title="Indicadores Registro, Ventas, Inscripción" width="64"
				height="64" /> <br /> <span>Indicadores Registro, Ventas,
					Inscripción</span>
			</a>
		</div>
		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('reporte/indicadoresCapacidad') ?>"> <img
				src="/imgs/iconos/kchart.png" border="0"
				title="Indicadores Capacidad Instalada" width="64" height="64" /> <br />
				<span>Indicadores Capacidad Instalada</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a
				href="<?php echo url_for('reporte/indicadoresDetalleZoom?caso=estatus_socios') ?>">
				<img src="/imgs/iconos/kchart.png" border="0"
				title="Indicadores Estatus Socios" width="64" height="64" /> <br />
				<span>Indicadores Estatus Socios</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('reporte/listaInactivos') ?>"> <img
				src="/imgs/iconos/usuarios_inactivos.png" border="0"
				title="Listas de socios inactivos" width="64" height="64" /> <br />
				<span>Listas de socios inactivos</span>
			</a>
		</div>

	</div>
</div>
