<?php
/**
 * Plantilla del home del Adminisrador
 */
use_stylesheet('home.css');
use_javascript('home/home.js');
?>

 <!--
                /*****************************************************
        			Propuesta roles y permisos 2014
        			Responsable: IMB.
        			Fecha: 10-09-2014. 
        			Cambio en los roles y perfiles, segun el documento
       			******************************************************/ 
       			-->

<div class="ui-widget ui-widget-content ui-corner-all sombra"
	style="min-width: 500px; min-height: 1000px; padding: 5px;">
	<div class="ui-widget ui-widget-header ui-corner-all"
		style="padding: 5px;">Home Facilitador ISAED Adm.</div>

	<div class="menu-home">

		<?php include_partial('herramientas', array('centro'=>$centro))?>

		<!-- Reingenieria Mako C. ID 19. Responsable:  FE. Fecha: 17-01-2014.  
                Descripción del cambio: Se activa modulo para los siguientes roles: Supervisores, Encargados y para el Gerente de Operaciones. -->
		<?php if ($sf_user->getAttribute('auth_externo')):?>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('registro/new') ?>"> <img
				src="/imgs/iconos/usuarios.png" border="0"
				title="Registro de socios" width="64" height="64" /> <br /> <span>Registro
					de socios</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('registro/index') ?>"> <img
				src="/imgs/iconos/buscar.png" border="0" title="Búsqueda de socios"
				width="64" height="64" /> <br /> <span>Búsqueda de socios</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('pos/new') ?>"> <img
				src="/imgs/iconos/venta.png" border="0" title="Punto de venta"
				width="64" height="64" /> <br /> <span>Punto de venta</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('corte_caja/resumenHoy') ?>"> <img
				src="/imgs/iconos/buscar.png" border="0" title="Movimientos del Dia"
				width="64" height="64" /> <br /> <span>Movimientos del dia</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('cancelacion_venta/guiAuth') ?>"> <img
				src="/imgs/iconos/cancelar.png" border="0"
				title="Cancelación de venta" width="64" height="64" /> <br /> <span>Cancelación
					de venta</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('salida_caja/index') ?>"> <img
				src="/imgs/iconos/salida_caja.png" border="0"
				title="Historial de salidas de caja" width="64" height="64" /> <br />
				<span>Historial de salidas de caja</span>
			</a>
		</div>
		<!-- Fecha: 17-01-2014 - FIN -->

		<!-- Reingenieria Mako C. ID 20. Responsable:  FE. Fecha: 17-01-2014.  
                Descripción del cambio: Se activa modulo para los siguientes roles: Supervisores, Encargados y para el Gerente de Operaciones. -->
		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('corte_caja/index') ?>"> <img
				src="/imgs/iconos/corte.png" border="0"
				title="Historial Cortes de caja" width="64" height="64" /> <br /> <span>Historial
					de cortes de caja.</span>
			</a>
		</div>
		<?php endif; ?>
		<!-- Fecha: 17-01-2014 - FIN -->

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('corte_caja/resumenValija') ?>"> <img
				src="/imgs/iconos/buscar.png" border="0" title="Reporte de Valija"
				width="64" height="64" /> <br /> <span>Reporte de valija</span>
			</a>
        </div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('control/listas') ?>"> <img
				src="/imgs/iconos/bitacora.png" border="0"
				title="Programación y estado de grupos" width="64" height="64" /> <br />
				<span>Programaci&oacute;n y estado de grupos</span>
			</a>
		</div>

    	  <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">                
                        <a href="/asistencia/index.php?oprid=<?php echo sfContext::getInstance()->getUser()->getAttribute('usuario')->getId() ?>">
                        <img src="/imgs/iconos/correr.png" border="0" title="Control de asistencia" width="64" height="64"/>
                        <br/>
                        <span>Control de asistencia</span>
                        </a>
         </div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('dashsesiones/index') ?>"> <img
				src="/imgs/iconos/libre.png" border="0" title="Ocupación de PCs"
				width="64" height="64" /> <br /> <span>Ocupación de PCs</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('switch_pcs/index') ?>"> <img
				src="/imgs/iconos/shutdown.png" border="0"
				title="Administración de energía PCs" width="64" height="64" /> <br />
				<span>Encendido y apagado PCs</span>
			</a>
        </div>

         <div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('reporte/index') ?>"> <img
				src="/imgs/iconos/kchart.png" border="0"
				title="Indicadores de operación" width="64" height="64" /> <br /> 
				<span>Ranking RIA</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('reporte/mapa') ?>"> <img
				src="/imgs/iconos/gmaps.png" border="0" title="Georeferencia Socios"
				width="64" height="64" /> <br /> <span>Georeferencia Socios</span>
			</a>
		</div>


                <!--Se quita a peticion de Isabel Baxin, responsable IMB, 27/10/2014 

             <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada">
                        <a href="<?php echo url_for('admin_cupones/index') ?>">
                                <img src="/imgs/iconos/correr.png" border="0" title="Administrar cupones" width="64" height="64"/>
                                <br/>
                                <span>Administrar cupones</span>
                        </a>
             </div>
             -->
	

		<!-- Reingenieria Mako C. ID 38. Responsable:  FE. Fecha: 17-01-2014.  Descripción del cambio: Se desactiva modulo para los siguientes roles: Todos excepto superusuario. -->
		<!--<div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="< ?php echo url_for('asignacion_becas/index') ?>">
				<img src="/imgs/iconos/correr.png" border="0" title="Registro de solicitud de becas" width="64" height="64"/>
				<br/>
				<span>Registro de solicitud de becas</span>
			</a>
		</div>-->
		<!-- Fecha: 17-01-2014 - FIN -->

	
        <!--Se quita a peticion de Isabel Baxin, responsable IMB, 15/10/2014 

         <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada">
            <a href="<?php echo url_for('admin_cupones/index') ?>">
               <img src="/imgs/iconos/correr.png" border="0" title="Administrar cupones" width="64" height="64"/>
                 <br/>
             <span>Administrar cupones</span>
            </a>
         </div>

         -->    

	</div>
</div>
