<?php
/**
 * Plantilla del home de cajero
 */
use_stylesheet('home.css');
use_javascript('home/home.js');
?>

<div class="ui-widget ui-widget-content ui-corner-all sombra"
	style="min-width: 500px; min-height: 500px;">
	<div class="ui-widget ui-widget-header ui-corner-tl ui-corner-tr tssss"
		style="padding: 5px;">Home Administrador de Control Académico</div>

	<div class="menu-home">

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('categoria_curso/index') ?>"> <img
				src="/imgs/iconos/admin_categorias.png" border="0"
				title="Administrador de categorías de curso" width="64" height="64" />
				<br /> <span>Administrador de categor&iacute;as</span>
			</a>
		</div>
		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('curso/index') ?>"> <img
				src="/imgs/iconos/carga_curso.png" border="0"
				title="Carga de cursos" width="64" height="64" /> <br /> <span>Carga
					de cursos</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('modelador/index') ?>"> <img
				src="/imgs/iconos/modelador_curso.png" border="0"
				title="Modelador de temarios" width="64" height="64" /> <br /> <span>Modelador
					de temarios</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('calendarizacion/index') ?>"> <img
				src="/imgs/iconos/calendarizacion.png" border="0"
				title="Calendarización de cursos" width="64" height="64" /> <br /> <span>Calendarización
					de cursos</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('cancelacion_grupo/index') ?>"> <img
				src="/imgs/iconos/cancelar_grupo.png" border="0"
				title="Cancelación de grupo" width="64" height="64" /> <br /> <span>Cancelaci&oacute;n
					de grupo</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('admin_aulas/index') ?>"> <img
				src="/imgs/iconos/admin.png" border="0"
				title="Administrador de aulas" width="64" height="64" /> <br /> <span>Administrador
					de aulas</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('dias_laborables/index') ?>"> <img
				src="/imgs/iconos/fecha.png" border="0"
				title="Administrador de días laborables" width="64" height="64" /> <br />
				<span>Administrador de días laborables</span>
			</a>
		</div>
		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('verificacion_curso/index') ?>"> <img
				src="/imgs/iconos/clean.png" border="0"
				title="Verificación de cursos" width="64" height="64" /> <br /> <span>Verificaci&oacute;n
					de cursos</span>
			</a>
		</div>
		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('admin_vacaciones/index') ?>"> <img
				src="/imgs/iconos/cal.png" border="0"
				title="Administrador de vacaciones" width="64" height="64" /> <br />
				<span>Administrador de vacaciones</span>
			</a>
		</div>
		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a
				href="<?php echo url_for('reportes_control_escolar/socios_curso') ?>">
				<img src="/imgs/iconos/bitacora.png" border="0"
				title="Reporte mensual de socios inscritos a cursos" width="64"
				height="64" /> <br /> <span>Reporte socios inscritos a cursos</span>
			</a>
		</div>
		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a
				href="<?php echo url_for('reportes_control_escolar/consulta_inscritos') ?>">
				<img src="/imgs/iconos/buscar_doc.png" border="0"
				title="Consulta de socios inscritos a curso por grupo" width="64"
				height="64" /> <br /> <span>Consulta de socios inscritos</span>
			</a>
		</div>
		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a
				href="<?php echo url_for('reportes_control_escolar/asistencias') ?>">
				<img src="/imgs/iconos/asistencia.png" border="0"
				title="Reporte de asistencias" width="64" height="64" /> <br /> <span>Reporte
					de asistencias</span>
			</a>
		</div>
		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a
				href="<?php echo url_for('reportes_control_escolar/calificaciones') ?>">
				<img src="/imgs/iconos/evaluaciones.png" border="0"
				title="Reporte de calificaciones" width="64" height="64" /> <br /> <span>Reporte
					de calificaciones</span>
			</a>
		</div>
		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a
				href="<?php echo url_for('reportes_control_escolar/historial_academico') ?>">
				<img src="/imgs/iconos/usuarios.png" border="0"
				title="Busqueda de historial académico" width="64" height="64" /> <br />
				<span>Historial Acad&eacute;mico</span>
			</a>
		</div>
		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('dashboard_reportes/index') ?>"> <img
				src="/imgs/iconos/kchart.png" border="0"
				title="Seguimiento indicadrores" width="64" height="64" /> <br /> <span>Seguimiento
					indicadrores</span>
			</a>
		</div>
		<?php 
		/*
			@@ LGL :: ENOVA
		Se agregan 3 modulos PUNTO VENTA, BÚSQUEDA DE SOCIOS y PROGRAMACIÓN Y ESTADO DE GRUPOS
		A PETICIÓN DE OPERACIONES
		TICKET MANTIS: 39845
		2013-05-23
		- Se pone validación de que solo pueda accederse de forma externa y no en central para la replicación a centros
		*/
	if ($sf_user->getAttribute('auth_externo') && sfConfig::get('app_centro_actual_id') != sfConfig::get('app_central_id')):?>
		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('pos/new') ?>"> <img
				src="/imgs/iconos/venta.png" border="0" title="Punto de venta"
				width="64" height="64" /> <br /> <span>Punto de venta</span>
			</a>
		</div>
		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('registro/index') ?>"> <img
				src="/imgs/iconos/buscar.png" border="0" title="Búsqueda de socios"
				width="64" height="64" /> <br /> <span>Búsqueda de socios</span>
			</a>
		</div>
		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('control/listas') ?>"> <img
				src="/imgs/iconos/bitacora.png" border="0"
				title="Programación y estado de grupos" width="64" height="64" /> <br />
				<span>Programaci&oacute;n y estado de grupos</span>
			</a>
                </div>

                <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                <!--<a href="/asistencia/index.php">-->
                <a href="/asistencia/index.php?oprid=<?php echo sfContext::getInstance()->getUser()->getAttribute('usuario')->getId() ?>">
                <img src="/imgs/iconos/correr.png" border="0" title="Control de asistencia" width="64" height="64"/>
                <br/>
                <span>Control de Asistencia</span>
                </a>
                </div>


		<?php endif;?>

	</div>
</div>
