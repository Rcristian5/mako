<?php
/**
 * Plantilla del home del Promotor
 */
use_stylesheet('home.css');
use_javascript('home/home.js');
?>

 <!--
                /*****************************************************
        			Propuesta roles y permisos 2014
        			Responsable: IMB.
        			Fecha: 10-09-2014. 
        			Cambio en los roles y perfiles, segun el documento
       			******************************************************/ 
       			-->

<div class="ui-widget ui-widget-content ui-corner-all sombra"
	style="min-width: 500px; min-height: 300px; padding: 5px;">
	<div class="ui-widget ui-widget-header ui-corner-all"
		style="padding: 5px;">Home Promotor</div>

	<div class="menu-home">

		<?php //if ($sf_user->getAttribute('auth_externo')):?>


		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('control/listas') ?>"> <img
				src="/imgs/iconos/bitacora.png" border="0"
				title="Programación y estado de grupos" width="64" height="64" /> <br />
				<span>Programaci&oacute;n y estado de grupos</span>
			</a>
		</div>
		
		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('corte_caja/resumenValija') ?>"> <img
				src="/imgs/iconos/buscar.png" border="0" title="Reporte de Valija"
				width="64" height="64" /> <br /> <span>Reporte de valija</span>
			</a>
        </div>


         <div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('reporte/mapa') ?>"> <img
				src="/imgs/iconos/gmaps.png" border="0" title="Georeferencia Socios"
				width="64" height="64" /> <br /> <span>Georeferencia Socios</span>
			</a>
		</div>

		<div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('reporte/index') ?>"> <img
				src="/imgs/iconos/kchart.png" border="0"
				title="Indicadores de operación" width="64" height="64" /> <br /> 
				<span>Ranking RIA</span>
			</a>
		</div>

		 <div
				class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
				<a href="<?php echo url_for('calendarizacion/index') ?>"> <img
				src="/imgs/iconos/calendarizacion.png" border="0"
				title="Calendarización de cursos" width="64" height="64" /> <br /> <span>Calendarización
					de cursos</span>
				</a>
				</div>

				<div
				class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
				<a
				href="<?php echo url_for('reportes_control_escolar/consulta_inscritos') ?>">
				<img src="/imgs/iconos/buscar_doc.png" border="0"
				title="Consulta de socios inscritos a curso por grupo" width="64"
				height="64" /> <br /> <span>Consulta de socios inscritos</span>
				</a>
				</div>

				<div
				class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
				<a href="<?php echo url_for('metas/index') ?>"> <img
				src="/imgs/iconos/target.png" border="0"
				title="Administración de metas" width="64" height="64" /> <br /> <span>Administración
					de metas</span>
				</a>
				</div>

				 <div
			class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
			<a href="<?php echo url_for('reporte/mapa') ?>"> <img
				src="/imgs/iconos/gmaps.png" border="0" title="Georeferencia Socios"
				width="64" height="64" /> <br /> <span>Georeferencia Socios</span>
			</a>
		</div>

                <div class="boton-home ui-corner-all ui-widget ui-widget-content sombra-delgada ts">
                        <a href="/asistencia/historico_facilitador.php">
                                <img src="/imgs/iconos/correr.png" border="0" title="Control de asistencia" width="64" height="64"/>
                                <br/>
                                <span>Control de Asistencia</span>
                        </a>
                </div>

	</div>
</div>
