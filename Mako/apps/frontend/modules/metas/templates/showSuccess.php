<table>
	<tbody>
		<tr>
			<th>Centro:</th>
			<td><?php echo $Metas->getCentroId() ?>
			</td>
		</tr>
		<tr>
			<th>Categoria fija:</th>
			<td><?php echo $Metas->getCategoriaFijaId() ?>
			</td>
		</tr>
		<tr>
			<th>Categoria producto:</th>
			<td><?php echo $Metas->getCategoriaProductoId() ?>
			</td>
		</tr>
		<tr>
			<th>Categoria beca:</th>
			<td><?php echo $Metas->getCategoriaBecaId() ?>
			</td>
		</tr>
		<tr>
			<th>Fecha inicio:</th>
			<td><?php echo $Metas->getFechaInicio() ?>
			</td>
		</tr>
		<tr>
			<th>Fecha fin:</th>
			<td><?php echo $Metas->getFechaFin() ?>
			</td>
		</tr>
		<tr>
			<th>Semana:</th>
			<td><?php echo $Metas->getSemana() ?>
			</td>
		</tr>
		<tr>
			<th>Anio:</th>
			<td><?php echo $Metas->getAnio() ?>
			</td>
		</tr>
		<tr>
			<th>Meta:</th>
			<td><?php echo $Metas->getMeta() ?>
			</td>
		</tr>
		<tr>
			<th>Entero:</th>
			<td><?php echo $Metas->getEntero() ?>
			</td>
		</tr>
		<tr>
			<th>Usuario:</th>
			<td><?php echo $Metas->getUsuarioId() ?>
			</td>
		</tr>
		<tr>
			<th>Activo:</th>
			<td><?php echo $Metas->getActivo() ?>
			</td>
		</tr>
		<tr>
			<th>Created at:</th>
			<td><?php echo $Metas->getCreatedAt() ?>
			</td>
		</tr>
		<tr>
			<th>Updated at:</th>
			<td><?php echo $Metas->getUpdatedAt() ?>
			</td>
		</tr>
		<tr>
			<th>Id:</th>
			<td><?php echo $Metas->getId() ?>
			</td>
		</tr>
	</tbody>
</table>

<hr />

<a href="<?php echo url_for('metas/edit?id='.$Metas->getId()) ?>">Edit</a>
&nbsp;
<a href="<?php echo url_for('metas/index') ?>">List</a>
