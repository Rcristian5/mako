<?php  $Editar = $sf_user->getAttribute('EditarMetas'); ?>

<?php use_javascript('plugins/jquery.dataTables.js') ?>
<?php use_javascript('plugins/dataTables/datatables.orden-decimal.js') ?>
<?php use_stylesheet('datatables.css') ?>

<script>

var lTMetass;
$(document).ready(function() {

   lTMetass = $('#lista-Metass').dataTable({
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": true,
        "bJQueryUI": true,
        "sPaginationType": "full_numbers",
        <?php if($Editar){ ?>
        "aaSorting": [[2, 'asc']] ,
        <?php }else{ ?>
        "aaSorting": [[0, 'asc']] ,
        <?php } ?>
        
        "iDisplayLength": 50,
        <?php if($Editar){ ?>
        "aoColumns": [null, null,null, null,null,{ "sType": "decimal" },null]
        <?php }else{ ?>
        "aoColumns": [null, null,null,{ "sType": "decimal" },null]
        <?php } ?>
     });

<?php if($Editar): ?>
   $('.baja').click(function(){
		if(confirm("¿Deseas eliminar la meta?"))
		{
			var link = $(this), pos = this;
			$.ajax({
			url : link.attr('href'),
			beforeSend : function()
			{
				link.hide(500)
			},
			success: function(data) {
				var aPos = lTMetass.fnGetPosition( pos.parentNode.parentNode );
				lTMetass.fnDeleteRow( aPos );
			}
		});

		return false;
		}
		return false;
	});
   <?php endif; ?>
   
});

	

</script>

<div class="ui-widget-header ui-corner-top tsss" style="padding: 5px;">Administrador
	de Metas</div>
<div class="ui-widget ui-corner-bottom sombra">
	<div class="ui-widget-content ui-corner-bottom" style="padding: 5px;">

		<?php if ($sf_user->hasFlash('ok')): ?>
		<div
			class="flash_ok ui-state-highlight ts sombra-delgada ui-corner-all"
			style="padding: 5px; margin-top: 10px; margin-bottom: 10px;">
			<span style="float: left; margin-right: 0.3em;"
				class="ui-icon ui-icon-info"></span> <strong><?php echo $sf_user->getFlash('ok') ?>
			</strong>
		</div>
		<?php endif; ?>

		
		 <?php 
       /*****************************************************
		Responsable: IMB. BGPJ
		Fecha: 08-12-2014. 05-01-2015
		Condicional para perfil de supervisor, Promotor Adm y Super Usuario
       ******************************************************/ 
       	?>

		<?php if($Editar): ?>
		<br /> 
		<span style="float: left;" class="ui-icon ui-icon-circle-plus"></span>
		<a href="<?php echo url_for('metas/new') ?>"> Agregar registro </a> <br />
		<?php endif; ?>
		<br />


		<table id="lista-Metass" style="width: 100%;">
			<thead>
				<tr>
					<?php if($Editar): ?>
					<th></th>
					<th>Eliminar</th>
					<?php endif; ?>
					<th>Centro</th>
					<th>Alias</th>
					<th>Categoría</th>
					<th>Meta</th>
					<th>Fecha inicio</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($Metass as $Metas): ?>
				<tr>
			<?php if($Editar): ?>
					<td>
					<?php if($Metas->getFechaInicio()>date('Y-m-d')){?> 
						<a href="<?php echo url_for('metas/edit?id='.$Metas->getId()) ?>"> 
							<span class="ui-icon  ui-icon-pencil"></span> 
						</a>
					<?php } ?>
					</td>
					<td>
					<?php if($Metas->getFechaInicio()>date('Y-m-d')){ ?> 
						<a href="<?php echo url_for('metas/borrar')?>/id/<?php echo $Metas->getId()?>" class="baja"> 
							<span title="Eliminar" class="ui-icon ui-icon-trash bx-title borrar"></span>
						</a>
					 <?php } else { ?> 
						 <a href="<?php echo url_for('metas/baja')?>/id/<?php echo $Metas->getId()?>" class="baja"> 
						 	<span title="Eliminar" class="ui-icon ui-icon-trash bx-title borrar"></span>
						 </a> 
					<?php } ?>
					</td>
			<?php endif; ?>

					<td><?php echo $Metas->getCentro() ?></td>
					<td><?php echo $Metas->getCentro()->getAliasReporte() ?></td>
					<td><?php echo $Metas->getCategoriaMetasFijas() . $Metas->getCategoriaReporte() . $Metas->getBeca() ?></td>
					<td align="right"> <?php  if($Metas->getEntero()!=True) echo '$ '	;
						echo $Metas->getMeta() ;

						?>
					</td>
					<td><?php echo $Metas->getFechaInicio() ?></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
