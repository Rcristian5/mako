<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>


<div class="ui-widget ui-corner-all sombra">
	<div class="ui-widget-header ui-corner-top tsss" style="padding: 5px;">Administrador
		de Metas</div>
	<div class="ui-widget-content ui-corner-bottom">

		<div style="margin: 15px;">


			<form
				action="<?php echo url_for('metas/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>"
				method="post"
				<?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
				<?php if (!$form->getObject()->isNew()): ?>
				<input type="hidden" name="sf_method" value="put" />
				<?php endif; ?>
				<table>
					<tfoot>
						<tr>
							<td colspan="2">&nbsp;<a
								href="<?php echo url_for('metas/index') ?>">Regresar al índice</a>
								<?php if (!$form->getObject()->isNew()): ?> &nbsp;<?php //echo link_to('Borrar registro', 'metas/delete?id='.$form->getObject()->getId(), array('method' => 'delete', 'confirm' => '¿Está seguro? Esta acción no se puede deshacer.')) ?>
								<?php endif; ?> <input type="submit" value="Guardar registro" />
								<input type="button" value="Cancelar"
								onclick="location.href='<?php echo url_for('metas/new') ?>'" />
							</td>
						</tr>
					</tfoot>
					<tbody>
						<?php echo $form ?>
					</tbody>
				</table>
			</form>


		</div>
	</div>
</div>
