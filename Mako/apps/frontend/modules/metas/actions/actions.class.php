<?php

/**
 * metas actions.
 *
 * @package    mako
 * @subpackage metas
 * @author     Bixit SA de CV
 * @version    SVN: $Id: actions.class.php,v 1.5 2011/03/04 18:13:27 eorozco Exp $
 */
class metasActions extends sfActions
{
	public function executeIndex(sfWebRequest $request)
	{
            $Autenticado = $this->getUser()->isAuthenticated();
            if (!$Autenticado)
                $this->redirect('/index.php');

            /*
              Verificamos que tenga permisos para ejecutar agregar, editar, borrar
              Solo a los roles de Promotor y Supervisor pueden ver la info, No editar
              Solo a los roles __ pueden ver y editar información
              BGPJ Bet Gader
             */
            $Usuario = $this->getUser()->getAttribute('usuario');
            if (!(
                    $Usuario->getRolId() == sfConfig::get('app_rol_promotor_id') || 
                    $Usuario->getRolId() == sfConfig::get('app_rol_supervisor_id') ||
                    $Usuario->getRolId() == sfConfig::get('app_rol_administrador_operaciones_id')
                    )) {
                error_log('El Usuario ' . $Usuario->getNombreCompleto() . 'No tiene permisos para ingresar al modulo Metas');
                $this->redirect('/index.php');
            }

            $IpParam = $this->request->getParameter('ip');
            $Ip = ($IpParam == '') ? Comun::ipReal() : $IpParam;
            error_log('Ip:                  ' . $Ip . '              ');

            $Computadora = ComputadoraPeer::getPcPorIp($Ip);
            $TipoPc = ($Computadora != null) ? $Computadora->getTipoId() : sfConfig::get('app_externa_tipo_pc_id');

            $this->Editar = false;
            if (
                    $Usuario->getRolId() == sfConfig::get('app_rol_administrador_operaciones_id')
            ) {
                error_log('Puede modificar en Metas');
                $this->Editar = true;
            }
            $this->getUser()->setAttribute('EditarMetas', $this->Editar);
		$c = new Criteria();
		$c->add(MetasPeer::ACTIVO,TRUE);
		$this->Metass = MetasPeer::doSelect($c);
	}

	public function executeShow(sfWebRequest $request)
	{
		$this->Metas = MetasPeer::retrieveByPk($request->getParameter('id'));
		$this->forward404Unless($this->Metas);
	}

	public function executeNew(sfWebRequest $request)
	{
		$this->form = new MetasForm();
	}

	public function executeCreate(sfWebRequest $request)
	{
		$this->forward404Unless($request->isMethod(sfRequest::POST));

		$this->form = new MetasForm();

		$this->processForm($request, $this->form);

		$this->setTemplate('new');
	}

	public function executeEdit(sfWebRequest $request)
	{
		$this->forward404Unless($Metas = MetasPeer::retrieveByPk($request->getParameter('id')), sprintf('Object Metas does not exist (%s).', $request->getParameter('id')));
		$this->form = new MetasForm($Metas);
	}

	public function executeUpdate(sfWebRequest $request)
	{
		$this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
		$this->forward404Unless($Metas = MetasPeer::retrieveByPk($request->getParameter('id')), sprintf('Object Metas does not exist (%s).', $request->getParameter('id')));
		$this->form = new MetasForm($Metas);

		$this->processForm($request, $this->form);

		$this->setTemplate('edit');
	}

	public function executeDelete(sfWebRequest $request)
	{
		$request->checkCSRFProtection();

		$this->forward404Unless($Metas = MetasPeer::retrieveByPk($request->getParameter('id')), sprintf('Object Metas does not exist (%s).', $request->getParameter('id')));
		$Metas->delete();

		$this->redirect('metas/index');
	}

	//da de baja una meta pasada
	public function executeBaja(sfWebRequest $request)
	{
		//$this->forward404unless($request->isXmlHttpRequest());
		error_log("Baja de la meta ID: ".$request->getParameter('id'));
		$Metas = MetasPeer::retrieveByPk($request->getParameter('id'));
		$Metas->setActivo('0');
		$Metas->save();
		sfProjectConfiguration::getActive()->getEventDispatcher()->notify(new sfEvent($Metas, 'meta.actualizada',$Metas));
		$this->getResponse()->setHttpHeader('Content-type','text/json');

		$this->redirect('metas/index');
	}

	//borra una meta futura
	public function executeBorrar(sfWebRequest $request)
	{
		$this->forward404unless($request->isXmlHttpRequest());

		$Metas = MetasPeer::retrieveByPk($request->getParameter('id'));
		$centro_id = $Metas->getCentroId();
		$Metas->delete();
		$MetaBorrada = new Metas();
		$MetaBorrada->setId($request->getParameter('id'));
		$MetaBorrada->setCentroId($centro_id);

		sfProjectConfiguration::getActive()->getEventDispatcher()->notify(new sfEvent($MetaBorrada, 'meta.borrada',$MetaBorrada));
		$this->getResponse()->setHttpHeader('Content-type','text/json');

		return $this->renderText(json_encode(array()));
	}

	protected function processForm(sfWebRequest $request, sfForm $form)
	{
		$form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
		if ($form->isValid())
		{
			 
			$usuario = $this->getUser()->getAttribute('usuario')->getId();
			$form->getObject()->setUsuarioId($usuario);

			$Metas = $form->save();


	  $this->getUser()->setFlash('ok', 'Se ha guardado correctamente el registro');

	  if($form->isNew())
	  {
	  	sfProjectConfiguration::getActive()->getEventDispatcher()->notify(new sfEvent($Metas, 'meta.creada',$Metas));
	  }
	  else
	  {
	  	sfProjectConfiguration::getActive()->getEventDispatcher()->notify(new sfEvent($Metas, 'meta.actualizada',$Metas));
	  }
	   

	  //$this->redirect('metas/index?id='.$Metas->getId());
	  $this->redirect('metas/index');
		}
	}
}
