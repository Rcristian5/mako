<?php

/**
 * admin_aulas actions.
 *
 * @package    mako
 * @subpackage admin_aulas
 * @author     Bixit SA de CV
 * @version    SVN: $Id: actions.class.php,v 1.2 2010/09/24 23:42:33 david Exp $
 */
class admin_aulasActions extends sfActions
{
	public function executeIndex(sfWebRequest $request)
	{
		$this->detalle = AulaPeer::doSelect(new Criteria());
		$this->form = new AulaForm();
	}

	public function executeCreate(sfWebRequest $request)
	{
		$this->forward404Unless($request->isMethod(sfRequest::POST));

		$this->form = new AulaForm();

		$this->processForm($request, $this->form);

		$this->detalle = AulaPeer::doSelect(new Criteria());

		$this->setTemplate('new');
	}

	public function executeEdit(sfWebRequest $request)
	{
		$this->forward404Unless($Aula = AulaPeer::retrieveByPk($request->getParameter('id')), sprintf('La aula no existe (%s).', $request->getParameter('id')));

		$this->form = new AulaForm($Aula);
		$this->detalle = AulaPeer::doSelect(new Criteria());

		$this->setTemplate('index');
	}

	public function executeUpdate(sfWebRequest $request)
	{
		$this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
		$this->forward404Unless($Aula = AulaPeer::retrieveByPk($request->getParameter('id')), sprintf('La aula no existe (%s).', $request->getParameter('id')));
		$this->form = new AulaForm($Aula);

		$this->processForm($request, $this->form);

		$this->detalle = AulaPeer::doSelect(new Criteria());

		$this->setTemplate('edit');
	}

	/**
	 * Ejecuta la acción para desactivar un registro
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeEnabled(sfWebRequest $request)
	{
		$this->forward404Unless($Aula = AulaPeer::retrieveByPk($request->getParameter('id')), sprintf('La aula no existe (%s).', $request->getParameter('id')));
		$Aula->setActivo(true);
		$Aula->save();

		$this->getUser()->setFlash('notice', 'Aula "'.$Aula->getNombre().'" activada');
		$this->redirect('admin_aulas/index');
	}

	/**
	 * Ejecuta la acción para desactivar un registro
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeDisabled(sfWebRequest $request)
	{
		$this->forward404Unless($Aula = AulaPeer::retrieveByPk($request->getParameter('id')), sprintf('La aula no existe (%s).', $request->getParameter('id')));
		$Aula->setActivo(false);
		$Aula->save();

		$this->getUser()->setFlash('notice', 'Aula "'.$Aula->getNombre().'" desactivada');
		$this->redirect('admin_aulas/index');
	}

	public function executeDelete(sfWebRequest $request)
	{
		$request->checkCSRFProtection();

		$this->forward404Unless($Aula = AulaPeer::retrieveByPk($request->getParameter('id')), sprintf('La aula no existe (%s).', $request->getParameter('id')));
		$this->getUser()->setFlash('notice', 'Aula '.$Aula->getNombre().' borrada');
		$Aula->delete();

		$this->redirect('admin_aulas/index');
	}

	protected function processForm(sfWebRequest $request, sfForm $form)
	{
		$form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
		if ($form->isValid())
		{
			$mensaje = 'Aula "'.$form->getValue('nombre').'" actualizada';
			$flag = false;
			if($form->getObject()->isNew())
			{
				$mensaje = 'Aula "'.$form->getValue('nombre').'" creada';
				$flag = true;
			}
			$form->getObject()->setOperadorId($this->getUser()->getAttribute('usuario')->getId());

			$Aula = $form->save();

			//Evento AulaCreada o AulaModificada
			($flag ? $this->dispatcher->notify(new sfEvent($this, 'aula.creada', array( 'aula' => $Aula ))) : $this->dispatcher->notify(new sfEvent($this, 'aula.modificada', array( 'aula' => $Aula ))) );

			$this->getUser()->setFlash('notice', $mensaje);
			$this->redirect('admin_aulas/index');
		}
	}
}
