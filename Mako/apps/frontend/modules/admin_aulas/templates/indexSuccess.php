<?php use_stylesheet('control_escolar/control_escolar.css') ?>
<?php use_stylesheet('datatables.css') ?>
<?php use_stylesheet('jquery.windows.css') ?>
<?php use_javascript('jquery.ui.js') ?>
<?php use_javascript('plugins/jquery.windows.js') ?>
<?php use_javascript('plugins/jquery.dataTables.js') ?>
<?php use_javascript('control_escolar/admin_aulas.js') ?>

<div id="windows-wrapper">
	<div id="columnaIzquierda" window-title="Aulas" window-state="min">

		<?php include_partial('form', array('form' => $form)) ?>

	</div>

	<div id="columnaDerecha" window-title="Lista de aulas"
		window-state="max">

		<h2>Lista de aulas</h2>

		<?php include_partial('detalleAulas', array('detalle' => $detalle)) ?>

	</div>

</div>

