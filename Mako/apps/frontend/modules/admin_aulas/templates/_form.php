<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<?php if ($form->getObject()->isNew()): ?>
<h2>Crear aula</h2>
<?php else: ?>
<h2>Editar aula</h2>
<?php endif; ?>

<form
	action="<?php echo url_for('admin_aulas/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>"
	method="post"
	<?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>
	onsubmit="return requeridos();">
	<?php if (!$form->getObject()->isNew()): ?>
	<input type="hidden" name="sf_method" value="put" />
	<?php endif; ?>
	<table>
		<tfoot>
			<tr>
				<td colspan="2"><?php echo $form->renderHiddenFields(false) ?>
				</td>
			</tr>
		</tfoot>
		<tbody>
			<?php echo $form->renderGlobalErrors() ?>
			<tr>
				<th><?php echo $form['centro_id']->renderLabel() ?></th>
				<td><?php echo $form['centro_id']->renderError() ?> <?php echo $form['centro_id'] ?>
				</td>
			</tr>
			<tr>
				<th><?php echo $form['nombre']->renderLabel() ?></th>
				<td><?php echo $form['nombre']->renderError() ?> <?php echo $form['nombre']->render(array('requerido'=>1, 'filtro'=>'alfanumerico', 'size'=>42)) ?>
				</td>
			</tr>
			<tr>
				<th><?php echo $form['descripcion']->renderLabel() ?></th>
				<td><?php echo $form['descripcion']->renderError() ?> <?php echo $form['descripcion']->render(array('requerido'=>1, 'filtro'=>'alfanumerico')) ?>
				</td>
			</tr>
			<tr>
				<th><?php echo $form['capacidad']->renderLabel() ?></th>
				<td><?php echo $form['capacidad']->renderError() ?> <?php echo $form['capacidad']->render(array('requerido'=>1, 'filtro'=>'numerico', 'size'=>4, 'maxlength'=>8)) ?>
				</td>
			</tr>
		</tbody>
	</table>
	<p class="botones_finales">
		<input type="button" value="Cancelar"
			onclick="location.href='<?php echo url_for('admin_aulas/index')?>'">
		<input type="submit" value="Guardar" />
	</p>
</form>
