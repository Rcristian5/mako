<table id="detalle-aulas" class="display" style="width: 100%;">
	<thead>
		<tr>
			<th class="acciones-tabla"></th>
			<th>Centro</th>
			<th>Nombre</th>
			<th>Descripci&oacute;n</th>
			<th>Capacidad</th>
			<th>Disponible</th>
			<th class="acciones-tabla"></th>
			<th class="acciones-tabla"></th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($detalle as $aula): ?>
		<tr>
			<td><a
				href="<?php echo url_for('admin_aulas/edit?id='.$aula->getId()) ?>"><span
					class="ui-icon ui-icon-pencil bx-title" title="Editar categoria"></span>
			</a>
			</td>
			<td><?php echo $aula->getCentro()->getNombre() ?>
			</td>
			<td><?php echo $aula->getNombre() ?>
			</td>
			<td><?php echo $aula->getDescripcion() ?>
			</td>
			<td><?php echo $aula->getCapacidad() ?>
			</td>
			<td><?php echo ($aula->getDisponible() ? 'Si' : 'No' ) ?>
			</td>
			<td><?php echo ($aula->getActivo() ? link_to('<span class="ui-icon ui-icon-bullet bx-title" title="Desactivar aula"></span>', 'admin_aulas/disabled?id='.$aula->getId(), array('confirm' => '¿Deseas desactivar esta aula?')) : link_to('<span class="ui-icon ui-icon-radio-on bx-tittle" title="Activar aula"></span>', 'admin_aulas/enabled?id='.$aula->getId(), array('confirm' => '¿Deseas activar esta aula?')) ) ?>
			</td>
			<td><?php echo ($aula->getDisponible() ? link_to('<span class="ui-icon ui-icon-trash bx-title" title="Borrar cuenta"></span>', 'admin_aulas/delete?id='.$aula->getId(), array('method' => 'delete', 'confirm' => '¿Deseas elimar esta aula? \n Todos los datos asociados a esta aula serán borrados')) : '<span class="ui-icon ui-icon-trash bx-title ui-state-disabled" title="Borrar cuenta"></span>' ) ?>
			</td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>
