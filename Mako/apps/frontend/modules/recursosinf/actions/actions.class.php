<?php

/**
 * recursosinf actions.
 *
 * @package    mako
 * @subpackage recursosinf
 * @author     Bixit SA de CV
 * @version    SVN: $Id: actions.class.php,v 1.2 2010/04/09 12:03:40 david Exp $
 */
class recursosinfActions extends sfActions
{
	public function executeIndex(sfWebRequest $request)
	{
		$this->RecursoInfraestructuras = RecursoInfraestructuraPeer::doSelect(new Criteria());
	}

	public function executeNew(sfWebRequest $request)
	{
		$this->form = new RecursoInfraestructuraForm();
		$this->detalle = RecursoInfraestructuraPeer::listaRecursos();
	}

	public function executeCreate(sfWebRequest $request)
	{
		$this->forward404Unless($request->isMethod(sfRequest::POST));
		$this->detalle = RecursoInfraestructuraPeer::listaRecursos();
		$this->form = new RecursoInfraestructuraForm();
		$this->processForm($request, $this->form);

		$this->setTemplate('new');
	}

	public function executeEdit(sfWebRequest $request)
	{
		$this->forward404Unless($RecursoInfraestructura = RecursoInfraestructuraPeer::retrieveByPk($request->getParameter('id')), sprintf('Object RecursoInfraestructura does not exist (%s).', $request->getParameter('id')));
		$this->form = new RecursoInfraestructuraForm($RecursoInfraestructura);
		$this->detalle = RecursoInfraestructuraPeer::listaRecursos();
	}

	public function executeUpdate(sfWebRequest $request)
	{
		$this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
		$this->forward404Unless($RecursoInfraestructura = RecursoInfraestructuraPeer::retrieveByPk($request->getParameter('id')), sprintf('Object RecursoInfraestructura does not exist (%s).', $request->getParameter('id')));
		$this->form = new RecursoInfraestructuraForm($RecursoInfraestructura);
		$this->detalle = RecursoInfraestructuraPeer::listaRecursos();
		$this->processForm($request, $this->form);

		$this->setTemplate('edit');
	}

	public function executeDelete(sfWebRequest $request)
	{
		$request->checkCSRFProtection();

		$this->forward404Unless($RecursoInfraestructura = RecursoInfraestructuraPeer::retrieveByPk($request->getParameter('id')), sprintf('Object RecursoInfraestructura does not exist (%s).', $request->getParameter('id')));
		$RecursoInfraestructura->delete();

		$this->redirect('recursosinf/index');
	}

	protected function processForm(sfWebRequest $request, sfForm $form)
	{
		$form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
		if ($form->isValid())
		{
			if($form->getObject()->isNew())
			{
				$flag = true;
			}
			else
			{
				$flag = false;
			}
			$form->getObject()->setOperadorId($this->getUser()->getAttribute('usuario')->getId());
			$recurso = $form->save();
			if($flag)
			{
				$this->getUser()->setFlash('notice', sprintf('Recurso %s guardado', $recurso->getNombre()));
			}
			else
			{
				$this->getUser()->setFlash('notice', sprintf('Recurso %s editado', $recurso->getNombre()));
			}

			$this->redirect('recursosinf/new');
		}
	}
}

