<h1>RecursoInfraestructuras List</h1>

<table>
	<thead>
		<tr>
			<th>Id</th>
			<th>Centro</th>
			<th>Codigo</th>
			<th>Nombre</th>
			<th>Capacidad</th>
			<th>Activo</th>
			<th>Operador</th>
			<th>Created at</th>
			<th>Updated at</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($RecursoInfraestructuras as $RecursoInfraestructura): ?>
		<tr>
			<td><a
				href="<?php echo url_for('recursosinf/edit?id='.$RecursoInfraestructura->getId()) ?>"><?php echo $RecursoInfraestructura->getId() ?>
			</a></td>
			<td><?php echo $RecursoInfraestructura->getCentroId() ?></td>
			<td><?php echo $RecursoInfraestructura->getCodigo() ?></td>
			<td><?php echo $RecursoInfraestructura->getNombre() ?></td>
			<td><?php echo $RecursoInfraestructura->getCapacidad() ?></td>
			<td><?php echo $RecursoInfraestructura->getActivo() ?></td>
			<td><?php echo $RecursoInfraestructura->getOperadorId() ?></td>
			<td><?php echo $RecursoInfraestructura->getCreatedAt() ?></td>
			<td><?php echo $RecursoInfraestructura->getUpdatedAt() ?></td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<a href="<?php echo url_for('recursosinf/new') ?>">New</a>
