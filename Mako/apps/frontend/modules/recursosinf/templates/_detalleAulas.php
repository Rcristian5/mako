<table id="detalle_recurso" class="display" style="width: 100%;">
	<thead>
		<tr>
			<th>Centro</th>
			<th>C&oacute;digo</th>
			<th>Nombre</th>
			<th>Capacidad</th>
			<th>Compuesta</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($detalle as $recurso): ?>
		<tr id="<?php echo $recurso['id'] ?>">
			<td><?php echo $recurso['centro'] ?>
			</td>
			<td><?php echo $recurso['codigo'] ?>
			</td>
			<td><?php echo $recurso['nombre'] ?>
			</td>
			<td><?php echo $recurso['capacidad'] ?>
			</td>
			<td><?php if($recurso['compuesta']){
				echo 'Si';
			}else{ echo 'No';
			} ?></td>
		</tr>
		<?php endforeach ?>
	</tbody>
</table>
