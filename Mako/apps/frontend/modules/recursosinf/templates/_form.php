<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<form
	action="<?php echo url_for('recursosinf/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>"
	method="post"
	<?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
	<?php if (!$form->getObject()->isNew()): ?>
	<input type="hidden" name="sf_method" value="put" />
	<?php endif; ?>
	<table>
		<tbody>
			<?php echo $form->renderGlobalErrors() ?>
			<tr>
				<th><?php echo $form['centro_id']->renderLabel('Centro') ?></th>
				<td><?php echo $form['centro_id']->renderError() ?> <?php echo $form['centro_id'] ?>
				</td>
			</tr>
			<tr>
				<th><?php echo $form['codigo']->renderLabel('Código') ?></th>
				<td><?php echo $form['codigo']->renderError() ?> <?php echo $form['codigo']->render(array('requerido'=>1, 'filtro'=>'alfanumerico', 'size'=>10, 'maxlength'=>50)) ?>
				</td>
			</tr>
			<tr>
				<th><?php echo $form['nombre']->renderLabel('Nombre') ?></th>
				<td><?php echo $form['nombre']->renderError() ?> <?php echo $form['nombre']->render(array('requerido'=>1, 'filtro'=>'alfanumerico', 'size'=>30, 'maxlength'=>255)) ?>
				</td>
			</tr>
			<tr>
				<th><?php echo $form['capacidad']->renderLabel() ?></th>
				<td><?php echo $form['capacidad']->renderError() ?> <?php echo $form['capacidad']->render(array('requerido'=>1, 'filtro'=>'numerico', 'size'=>5, 'maxlength'=>10)) ?>
				</td>
			</tr>
			<tr>
				<th><?php echo $form['compuesta']->renderLabel('Aula compuesta') ?>
				</th>
				<td><?php echo $form['compuesta']->renderError() ?> <?php echo $form['compuesta']?>
				</td>
			</tr>
			<tr class="compuesta">
				<th><?php echo $form['recursoCompuesta']['compuesta_id_1']->renderLabel('Aula 1') ?>
				</th>
				<td><?php echo $form['recursoCompuesta']['compuesta_id_1']->renderError() ?>
					<?php echo $form['recursoCompuesta']['compuesta_id_1']->render(array('requerido'=>1, 'class'=>'compuesta-sel')) ?>
				</td>
			</tr>
			<tr class="compuesta">
				<th><?php echo $form['recursoCompuesta']['compuesta_id_2']->renderLabel('Aula 2') ?>
				</th>
				<td><?php echo $form['recursoCompuesta']['compuesta_id_2']->renderError() ?>
					<?php echo $form['recursoCompuesta']['compuesta_id_2']->render(array('requerido'=>1,'class'=>'compuesta-sel')) ?>
				</td>
			</tr>
		</tbody>
	</table>
	<?php echo $form->renderHiddenFields(false) ?>
	<p class="botones_finales">
		<?php if (!$form->getObject()->isNew()): ?>
		<!--&nbsp;<?php echo link_to('Delete', 'curso/delete?id='.$form->getObject()->getId(), array('method' => 'delete', 'confirm' => 'Are you sure?')) ?>-->
		<input type="button" value="Cancelar"
			onclick="location.href='/recursos/new'">
		<?php endif; ?>
		<input type="submit" value="Guardar"
			onclick="return comparaRequeridos();" />
	</p>
</form>
