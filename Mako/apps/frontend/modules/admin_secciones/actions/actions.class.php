<?php

/**
 * admin_secciones actions.
 *
 * @package    mako
 * @subpackage admin_secciones
 * @author     Bixit SA de CV
 * @version    SVN: $Id: actions.class.php,v 1.2 2011/01/13 03:49:47 eorozco Exp $
 */
class admin_seccionesActions extends sfActions
{
	public function executeIndex(sfWebRequest $request)
	{
		$c = new Criteria();
		$centro_id = sfConfig::get('app_centro_actual_id');
		if($centro_id != sfConfig::get('app_central_id'))
		{
			$c->add(SeccionPeer::CENTRO_ID,$centro_id);
		}
		 
		$this->Seccions = SeccionPeer::doSelect($c);
	}

	public function executeNew(sfWebRequest $request)
	{
		$this->form = new SeccionForm();
	}

	public function executeCreate(sfWebRequest $request)
	{
		$this->forward404Unless($request->isMethod(sfRequest::POST));

		$this->form = new SeccionForm();

		$this->processForm($request, $this->form);

		$this->setTemplate('new');
	}

	public function executeEdit(sfWebRequest $request)
	{
		$this->forward404Unless($Seccion = SeccionPeer::retrieveByPk($request->getParameter('id')), sprintf('Object Seccion does not exist (%s).', $request->getParameter('id')));
		$this->form = new SeccionForm($Seccion);
	}

	public function executeUpdate(sfWebRequest $request)
	{
		$this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
		$this->forward404Unless($Seccion = SeccionPeer::retrieveByPk($request->getParameter('id')), sprintf('Object Seccion does not exist (%s).', $request->getParameter('id')));
		$this->form = new SeccionForm($Seccion);

		$this->processForm($request, $this->form);

		$this->setTemplate('edit');
	}

	public function executeDelete(sfWebRequest $request)
	{
		$request->checkCSRFProtection();

		$this->forward404Unless($Seccion = SeccionPeer::retrieveByPk($request->getParameter('id')), sprintf('Object Seccion does not exist (%s).', $request->getParameter('id')));
		$Seccion->delete();

		$this->redirect('admin_secciones/index');
	}

	protected function processForm(sfWebRequest $request, sfForm $form)
	{
		$form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
		if ($form->isValid())
		{
			$Seccion = $form->save();


	  $this->getUser()->setFlash('ok', sprintf('Se ha guardado correctamente el registro',$form->getObject()));



	  $this->redirect('admin_secciones/index?id='.$Seccion->getId());
		}
	}
}
