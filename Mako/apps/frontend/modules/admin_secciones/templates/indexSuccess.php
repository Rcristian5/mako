<?php use_javascript('plugins/jquery.dataTables.js') ?>
<?php use_stylesheet('datatables.css') ?>

<script>

var lTSeccions;
$(document).ready(function() {

   lTSeccions = $('#lista-Seccions').dataTable({
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": true,
        "bJQueryUI": true,
        "sPaginationType": "full_numbers",
        "aaSorting": [[1, 'asc']] 
     });
   
});

</script>

<div class="ui-widget-header ui-corner-top tsss" style="padding: 5px;">Administrador
	de secciones de centro</div>
<div class="ui-widget ui-corner-bottom sombra">
	<div class="ui-widget-content ui-corner-bottom" style="padding: 5px;">

		<?php if ($sf_user->hasFlash('ok')): ?>
		<div
			class="flash_ok ui-state-highlight ts sombra-delgada ui-corner-all"
			style="padding: 5px; margin-top: 10px; margin-bottom: 10px;">
			<span style="float: left; margin-right: 0.3em;"
				class="ui-icon ui-icon-info"></span> <strong><?php echo $sf_user->getFlash('ok') ?>
			</strong>
		</div>
		<?php endif; ?>

		<br /> <span style="float: left;" class="ui-icon ui-icon-circle-plus"></span>
		<a href="<?php echo url_for('admin_secciones/new') ?>"> Agregar
			registro </a> <br /> <br />


		<table id="lista-Seccions" style="width: 100%;">
			<thead>
				<tr>
					<th><span class="ui-icon  ui-icon-pencil"></span></th>
					<th>Centro</th>
					<th>Nombre</th>
					<th>En dashboard</th>
					<th>Activo</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($Seccions as $Seccion): ?>
				<tr>
					<td><a
						href="<?php echo url_for('admin_secciones/edit?id='.$Seccion->getId()) ?>">
							<span class="ui-icon  ui-icon-pencil"></span>
					</a>
					</td>
					<td><?php echo $Seccion->getCentro() ?></td>
					<td><?php echo $Seccion->getNombre() ?></td>
					<td><?php echo $Seccion->getEnDashboard() ? 'SI':'NO' ?></td>
					<td><?php echo $Seccion->getActivo() ? 'SI':'NO' ?></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>


	</div>
</div>

