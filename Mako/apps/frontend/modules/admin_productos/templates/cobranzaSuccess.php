<?php

?>

<script>
$(document).ready(function() 
{

	$(document).bind('producto.seleccionado',function(){ 
		
		$('#editar-cobranza').load();
		

	});

	
});
</script>

<div class="ui-widget ui-corner-all sombra">
	<div class="ui-widget-content ui-corner-all">
		<div class="ui-widget-header ui-corner-top tsss" style="padding: 5px;">Administración
			de opciones de cobranza</div>

		<p>Seleccione un producto para administrar sus opciones de cobranza.</p>

		<?php include_partial('admin_productos/listaProductos',array('Productos'=>$Productos))?>

	</div>
</div>

<div id="editar-cobranza"></div>
