<?php use_javascript('plugins/jquery.dataTables.js') ?>
<?php use_javascript('jquery.ui.js') ?>
<?php use_stylesheet('datatables.css') ?>

<?php 

/**
 * Definimos cual es el el monto a pagar de acuerdo a las modalidades y el tiempo de cada periodo
 * Siempre se calcula el valor ya que puede ser que venga de una modificacion y el numero de pagos crezca, el cual ya no corresponderia
 * con que el se tendria en la db. Esto porque ya no se le permite al usuario realizar modificaciones sobre los campos 	monto y tiempo.
 *
 * @author silvio.bravo 02 Abril 2014
 * @category Reingenieria Mako
 * @copyright ENOVA
 *
 */

$tiempo		= $Producto->getTiempo();
$precio		= $Producto->getPrecioLista();
$numeroPagos= 0;
foreach ($ModalidadesCobranza as $mc){
	$numeroPagos +=$mc->getNumeroPagos();
}
$periodosegundos	= $tiempo/$numeroPagos;
$montoperiodo		= number_format($precio / $numeroPagos,2,".","");
$horaperiodo		= sprintf("%02d",intval(  $periodosegundos/3600));
$minutosperiodo		= sprintf("%02d",intval( ($periodosegundos%3600)/60 ));
$segundos			= sprintf("%02d",intval( ($periodosegundos%60)));
//$segundos			= sprintf("%02d",intval( ($periodosegundos-($horaperiodo * 3600))-($minutosperiodo*60)));
$tiempoformato		= "{$horaperiodo}:{$minutosperiodo}" . ( (intval($segundos)>0)?":{$segundos}":"" );

?>

<script>
var pT;
$(document).ready(function() {

   cT = $('.tablas-cobranza').dataTable({
		"bPaginate": false,
		"bLengthChange": false,
		"bFilter": false,
		"bSort": false,
		"bInfo": false,
		"bAutoWidth": false,
        "bJQueryUI": true
        //,
        //"sPaginationType": "full_numbers"
     });

});

</script>

<div class="ui-widget-header ui-corner-top tsss" style="padding: 5px;">Detalle
	del producto</div>
<div class="ui-widget ui-corner-bottom sombra">
	<div class="ui-widget-content ui-corner-bottom" style="padding: 15px;">

		<div id="ui-contenedor" class="clearfix">

			<table style="float: left; margin-right: 40px;" id="datos-producto">
				<caption>
					<h3>Datos del producto</h3>
				</caption>
				<tbody>
					<tr>
						<th>Id:</th>
						<td><?php echo $Producto->getId() ?></td>
					</tr>
					<tr>
						<th>Categoria:</th>
						<td><?php echo $Producto->getCategoriaProducto() ?></td>
					</tr>
					<tr>
						<th>Tipo:</th>
						<td><?php echo $Producto->getTipoProducto() ?></td>
					</tr>
					<tr>
						<th>Unidad:</th>
						<td><?php echo $Producto->getUnidadProducto() ?></td>
					</tr>
					<tr>
						<th>Codigo:</th>
						<td><?php echo $Producto->getCodigo() ?></td>
					</tr>
					<tr>
						<th>Nombre:</th>
						<td><?php echo $Producto->getNombre() ?></td>
					</tr>
					<tr>
						<th>Descripcion:</th>
						<td><?php echo $Producto->getDescripcion() ?></td>
					</tr>
					<tr>
						<th>Precio lista:</th>
						<td><?php echo $Producto->getPrecioLista() ?></td>
					</tr>
					<tr>
						<th>Tiempo uso PC:</th>
						<td><?php echo ($Producto->getTiempo())? Comun::segsATxt($Producto->getTiempo()):0; ?>
						</td>
					</tr>
					<tr>
						<th>Activo:</th>
						<td><?php echo ($Producto->getActivo())?'SI':'NO' ?></td>
					</tr>
					<tr>
						<th>Venta unitaria:</th>
						<td><?php echo ($Producto->getVentaUnitaria())?'SI':'NO' ?></td>
					</tr>
					<tr>
						<th>Visible directo en caja:</th>
						<td><?php echo ($Producto->getEnPv())?'SI':'NO' ?></td>
					</tr>

					<tr>
						<th>Fecha de creación.:</th>
						<td><?php echo $Producto->getCreatedAt() ?></td>
					</tr>
					<tr>
						<th>Fecha de modificación:</th>
						<td><?php echo $Producto->getUpdatedAt() ?></td>
					</tr>
				</tbody>
			</table>

			<?php if(count($ListaProdsPaq)):?>
			<table id="lista-productos">
				<caption>
					<h3>Productos en este paquete</h3>
				</caption>
				<tbody>

					<?php foreach ($ListaProdsPaq as $pp): ?>
					<tr>
						<td><li><?php echo $pp->getProductoRelatedByProductoId() ?></li></td>
					</tr>
					<?php endforeach; ?>

				</tbody>
			</table>
			<?php endif; ?>
		</div>

		<hr />

		<a
			href="<?php echo url_for('admin_productos/edit?id='.$Producto->getId()) ?>">Modificar</a>
		| <a href="<?php echo url_for('admin_productos/index') ?>">Lista de
			productos</a>


	</div>
</div>

<br />
<br />

<?php if(count($ModalidadesCobranza)>0): ?>

<div class="ui-widget-header ui-corner-top tsss" style="padding: 5px;">Administración
	de opciones de cobranza</div>
<div class="ui-widget ui-corner-bottom sombra">
	<div class="ui-widget-content ui-corner-bottom clearfix"
		style="padding: 15px;" id="cont-tablas-cobranza">

		<?php if ($sf_user->hasFlash('ok')): ?>
		<div
			class="flash_ok ui-state-highlight ts sombra-delgada ui-corner-all"
			style="padding: 5px; margin-top: 10px; margin-bottom: 10px;">
			<span style="float: left; margin-right: 0.3em;"
				class="ui-icon ui-icon-info"></span> <strong><?php echo $sf_user->getFlash('ok') ?>
			</strong>
		</div>
		<?php endif; ?>

		<?php if ($sf_user->hasFlash('error')): ?>
		<div
			class="flash_ok ui-state-error tssss sombra-delgada ui-corner-all"
			style="padding: 5px; margin-top: 10px; margin-bottom: 10px;">
			<span style="float: left; margin-right: 0.3em;"
				class="ui-icon ui-icon-info"></span> <strong><?php echo $sf_user->getFlash('error') ?>
			</strong>
		</div>
		<?php endif; ?>


		<form
			action="<?php echo url_for('admin_productos/guardarCostoCobranza') ?>"
			method="post">

			<input type="submit" name="boton" value="Guardar"
				onclick="return requeridos();" /> <br />
			<?php
			//Lenamos un arreglo con los valores de productos existentes.
			$arSubps = $Producto->getSubproductoCobranzasRelatedBySubProductoDeJoinTipoCobro();
			$arVals = array();
			foreach($arSubps as $r)
			{
				$arVals[$Producto->getId()][$r->getCobroId()][$r->getNumeroPago()-1]	= array(
						'precio' => $r->getProductoRelatedByProductoId()->getPrecioLista(),
						'tiempo' => $r->getProductoRelatedByProductoId()->getTiempo(),
						'subproducto_id' => $r->getProductoRelatedByProductoId()->getId()
				);
			}

			?>
			//
			<?php foreach ($ModalidadesCobranza as $mc): ?>
			<div style="float: left;">
				<table class="tablas-cobranza">
					<thead>
						<tr>
							<th class="ui-default-state"><?php echo $mc->getTipoCobro()->getNombre() ?>
							</th>
							<th class="ui-default-state">Monto ($)</th>
							<!-- 				
				<th  class="ui-default-state">
					Código curso
				</th>
 -->
							<th class="ui-default-state">Tiempo (Hrs)</th>

						</tr>
					</thead>
					<tbody>
						<?php for ($i=0; $i < $mc->getNumeroPagos(); $i++): ?>
						<tr>
							<td><?php echo $mc->getProducto() ?> - <?php echo $mc->getTipoCobro()->getNombre() ?>
								<?php echo $i+1 ?> de <?php echo $mc->getNumeroPagos()?>
							</td>
							<td><input type="text"
								name="preciosCob[<?php echo $Producto->getId() ?>][<?php echo $mc->getCobroId()?>][<?php echo $i?>][precio]"
								filtro="numerico" requerido='1' size="8" maxlength="8"
								readonly="readonly"
								value="<?php echo $montoperiodo; //echo (isset($arVals[$Producto->getId()][$mc->getCobroId()][$i]['precio']))? $arVals[$Producto->getId()][$mc->getCobroId()][$i]['precio']:'' ?>" />
							</td>
							<!-- 
				<td>
					<input type="text" 
					
						name="preciosCob[<?php echo $Producto->getId() ?>][<?php echo $mc->getCobroId()?>][<?php echo $i?>][codigo]" 
						filtro="alfanumerico" requerido='1' size="8" maxlength="60"
						value="<?php echo (isset($arVals[$Producto->getId()][$mc->getCobroId()][$i]['codigo']))? $arVals[$Producto->getId()][$mc->getCobroId()][$i]['codigo']:'' ?>"
					/>
				</td>
 				-->
							<td><input type="text" name="auxtiempo[]" requerido="1"
								value="<?= $tiempoformato; ?>" size="8" maxlength="8"
								readonly="readonly" /> <input type="hidden"
								name="preciosCob[<?php echo $Producto->getId() ?>][<?php echo $mc->getCobroId()?>][<?php echo $i?>][tiempo]"
								filtro="numerico" requerido='1' size="10" maxlength="10"
								value="<?php echo $periodosegundos; //echo (isset($arVals[$Producto->getId()][$mc->getCobroId()][$i]['precio']))? $arVals[$Producto->getId()][$mc->getCobroId()][$i]['tiempo']:'' ?>" />
								<input type="hidden"
								name="preciosCob[<?php echo $Producto->getId() ?>][<?php echo $mc->getCobroId()?>][<?php echo $i?>][subproducto_id]"
								value="<?php echo (isset($arVals[$Producto->getId()][$mc->getCobroId()][$i]['subproducto_id']))? $arVals[$Producto->getId()][$mc->getCobroId()][$i]['subproducto_id']:'' ?>" />
							</td>
						</tr>
						<?php endfor; ?>
					</tbody>
				</table>
			</div>
			<?php endforeach; ?>




		</form>
	</div>
</div>
<?php endif; ?>
