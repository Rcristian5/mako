
<div class="ui-widget">
	<div class="ui-widget-content ui-corner-all" style="padding: 5px;">

		<table id="lista-min-productos" width="100%">
			<caption>Seleccione los productos a incluir en este paquete</caption>
			<thead>
				<tr>
					<th>Categoría</th>
					<th>Código</th>
					<th>Nombre</th>
					<th>Precio de lista($)</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($Productos as $Producto): ?>
				<tr id="prod_<?php echo $Producto->getId() ?>"
					onclick="$(this).trigger('producto.seleccionado',['<?php echo $Producto->getId(); ?>',this]);">
					<td class="categoria"><?php echo $Producto->getCategoriaProducto() ?>
					</td>
					<td class="codigo"><?php echo $Producto->getCodigo() ?></td>
					<td class="nombre"><?php echo $Producto->getNombre() ?></td>
					<td class="precio" align="right"><?php echo $Producto->getPrecioLista() ?>
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

	</div>
</div>
