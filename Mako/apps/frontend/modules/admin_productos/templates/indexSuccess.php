<?php use_javascript('plugins/jquery.dataTables.js') ?>
<?php use_javascript('jquery.ui.js') ?>
<?php use_stylesheet('datatables.css') ?>

<script>

var pT;
$(document).ready(function() {

   pT = $('#lista-productos').dataTable({
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
        "bJQueryUI": true,
        "sPaginationType": "full_numbers",
        "aaSorting": [[5, 'asc']]        
     });
   
});

</script>

<div class="ui-widget-header ui-corner-top tsss" style="padding: 5px;">Administración
	de productos</div>
<div class="ui-widget ui-corner-bottom sombra">
	<div class="ui-widget-content ui-corner-bottom" style="padding: 5px;">
		<?php if ($sf_user->hasFlash('error')): ?>
		<div
			class="flash_error ui-state-error ts sombra-delgada ui-corner-all"
			style="padding: 5px; margin-top: 10px; margin-bottom: 10px;">
			<span style="float: left; margin-right: 0.3em;"
				class="ui-icon ui-icon-error"></span> <strong><?php echo $sf_user->getFlash('error') ?>
			</strong>
		</div>
		<?php endif; ?>

		<br /> <span style="float: left;" class="ui-icon ui-icon-circle-plus"></span>
		<a href="<?php echo url_for('admin_productos/new') ?>"> Nuevo Producto
		</a>
		<div style="float: right">
			<a href="<?php echo url_for('admin_productos/index') ?>?activo=f">
				Productos inactivos </a> <br> <a
				href="<?php echo url_for('admin_productos/index') ?>?activo=t">
				Productos activos </a>
		</div>
		<br /> <br />

		<table id="lista-productos">
			<thead>
				<tr>
					<th>Revisar</th>
					<th>Categoría</th>
					<th>Tipo</th>
					<th>Unidad</th>
					<th>Código</th>
					<th>Nombre</th>
					<th>Descripción</th>
					<th>Precio lista($)</th>
					<th>Tiempo</th>
					<th>Activo</th>
					<th>Creado</th>
					<th>Modificado</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($Productos as $Producto): ?>
				<tr>
					<td align="center"><a
						href="<?php echo url_for('admin_productos/show?id='.$Producto->getId()) ?>">
							<span class="ui-icon  ui-icon-circle-zoomin"></span>
					</a>
					</td>
					<td><?php echo $Producto->getCategoriaProducto() ?></td>
					<td><?php echo $Producto->getTipoProducto() ?></td>
					<td><?php echo $Producto->getUnidadProducto() ?></td>
					<td><?php echo $Producto->getCodigo() ?></td>
					<td><?php echo $Producto->getNombre() ?></td>
					<td><?php echo $Producto->getDescripcion() ?></td>
					<td align="right"><?php echo $Producto->getPrecioLista() ?></td>
					<td align="right"><?php echo Comun::segsATxt($Producto->getTiempo()) ?>
					</td>
					<td align="center"><?php echo ($Producto->getActivo())?'SI':'NO' ?>
					</td>
					<td><?php echo $Producto->getCreatedAt() ?></td>
					<td><?php echo $Producto->getUpdatedAt() ?></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

	</div>
</div>

