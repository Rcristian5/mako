<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<?php use_javascript('plugins/jquery.dataTables.js') ?>
<?php use_javascript('jquery.ui.js') ?>
<?php use_stylesheet('datatables.css') ?>


<script>
$(document).ready(function() {

	//Si se trata de un paquete mostramos la tabla de productos a seleccionar para el paquete.
	if($('#producto_tipo_id').val() == 3)
	{
		$('#producto_paquete_list').show('slow');
	}
	else
	{
		$('#producto_paquete_list').hide('slow');
	}

	$('#producto_tipo_id').change(function()
	{
		if($(this).val() == 3)
		{
			$('#producto_paquete_list').show('slow');
		}
		else
		{
			$('#producto_paquete_list').hide('slow');
		}
		console.log($(this).val());
	});

	evaluaTiempoInicial();

	function evaluaTiempoInicial(){
		var inicial=parseFloat($("#producto_tiempo").val());
		if(isNaN(inicial)){
			inicial=0;
		}
		var horas	=parseInt(inicial/3600);
		var minutos	=parseInt((inicial-(horas * 3600))/60);
		$("#horatiempo").attr("value",horas);
		$("#minutotiempo option:selected").removeAttr("selected");
		$('#minutotiempo option[value="'+ minutos +'"]').attr("selected","selected");
		
		
	}

	function setTiempoEnSegundos(horacomponente, minutocomponente, destino){
		var horas	=(horacomponente.attr("value")) * 3600;
		var minutos	=(minutocomponente.attr("value")) * 60;
		destino.attr("value", (horas + minutos));
		
	}

	$("#horatiempo").change(function(){
		setTiempoEnSegundos($(this), $("#minutotiempo"), $("#producto_tiempo"));
	});

	$("#minutotiempo").change(function(){
		setTiempoEnSegundos($("#horatiempo"),$(this), $("#producto_tiempo"));
	});
	
	
	
});
</script>

<script>

var lpT;
$(document).ready(function() {

   lpT = $('#lista-min-productos').dataTable({
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
        "bJQueryUI": true,
        "sPaginationType": "full_numbers"
     });


	//Comprobamos que si es un paquete, todos los ids esten en el arreglo "enTabla" para evitar duplicidad de paquete.
	//Y si es un paquete mostramos la tabla de productos contenidos en paquete.
	if($('#producto_tipo_id').val() == 3)
	{
		var c = 0;
		$("tr[producto_paquete='1']").each(function()
		{
			c++;
			var producto_id = $(this).attr('producto_id');
			enTabla[producto_id] = true;
		});
		if(c > 0)
		{
			$('#en-paquete-actual').show();
		}
	}
	
});

var enTabla = [];
$(document).bind('producto.seleccionado',function(ev,producto_id,tr)
{
	console.debug('Clikc en prod: %s N: %s',producto_id,$(tr).children('.nombre').html());

	if(enTabla[producto_id] != undefined)
	{
		return false;
	}
	else
	{
		enTabla[producto_id] = true;
	}
	
	$('#en-paquete-actual').show();
	
	//Producto base
	var pb = $('#prod-paq-base').clone();
	pb.children('#prod-paq-base-cod').html( $(tr).children('.codigo').html() );
	pb.children('#prod-paq-base-nom').html( $(tr).children('.nombre').html() );
	pb.children('#prod-paq-base-prc').html( $(tr).children('.precio').html() );
	pb.find('#prod-paq-base-prp-inp').val( $(tr).children('.precio').html() );
	pb.find('#prod-paq-base-prp-inp').attr('name','producto_paquete_list['+producto_id+'][precio_paq]');
	pb.find('#prod-paq-base-prp-inp').attr('requerido','1');
	pb.find('#prod-paq-base-id').attr('name','producto_paquete_list['+producto_id+'][id]');
	pb.find('#prod-paq-base-id').val(producto_id);
	pb.attr('id','prod-paq-id-'+producto_id);
	pb.attr('producto_paquete','1');
	pb.attr('producto_id',producto_id);
	pb.insertAfter($('#prod-paq-base'));
	pb.show();
});

function borrar(td)
{
	var fila = $(td).closest("tr");
	console.log('Entra %s',fila);
	var producto_id = fila.attr('producto_id');
	fila.remove();
	delete enTabla[producto_id];
	
}

function comprobarProds()
{
	if(!requeridos())
	{
		return false;
	}
	//Si es un paquete hay que serciorarse de que se hayan seleccionado los paquetes.
	if($('#producto_tipo_id').val() == 3)
	{
		var c = 0;
		$("tr[producto_paquete='1']").each(function(){c++;});
		if(c == 0)
		{
			alert('Debe agregar por lo menos un producto al paquete');
			return false;
		}
	}
	return true;
}


</script>

<?php use_stylesheet('./admin_productos/_form.css')?>

<div class="ui-widget-header ui-corner-top tsss" style="padding: 5px;">
	Producto
	<?php echo (!$form->getObject()->isNew())? ': '.$form->getObject()->getNombre() : '' ?>
</div>
<div class="ui-widget ui-corner-bottom sombra">
	<div class="ui-widget-content ui-corner-bottom clearfix"
		style="padding: 10px;">

		<?php if ($sf_user->hasFlash('ok')): ?>
		<div
			class="flash_ok ui-state-highlight ts sombra-delgada ui-corner-all"
			style="padding: 5px; margin-top: 10px; margin-bottom: 10px;">
			<span style="float: left; margin-right: 0.3em;"
				class="ui-icon ui-icon-info"></span> <strong><?php echo $sf_user->getFlash('ok') ?>
			</strong>
		</div>
		<?php endif; ?>

		<?php if ($sf_user->hasFlash('error')): ?>
		<div
			class="flash_ok ui-state-error tssss sombra-delgada ui-corner-all"
			style="padding: 5px; margin-top: 10px; margin-bottom: 10px;">
			<span style="float: left; margin-right: 0.3em;"
				class="ui-icon ui-icon-info"></span> <strong><?php echo $sf_user->getFlash('error') ?>
			</strong>
		</div>
		<?php endif; ?>

		<form
			action="<?php echo url_for('admin_productos/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>"
			method="post"
			<?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
			<?php if (!$form->getObject()->isNew()): ?>
			<input type="hidden" name="sf_method" value="put" />
			<?php endif; ?>

			<div class="clearfix">
				<div style="float: left;">
					<table>
						<tfoot>
							<tr>
								<td colspan="2">
									<hr /> &nbsp;<a
									href="<?php echo url_for('admin_productos/index') ?>">Ver
										productos</a> <input type="submit" value="Guardar"
									tabindex="1" onclick="return comprobarProds();" />
								</td>
							</tr>
						</tfoot>
						<tbody>
							<!-- Reingeniria Mako
    			Agregamos los tags del formulario a mano debido a que hay que hacer que el campo de tiempo
    			del curso debe ser customizado, y symfony (a menos en esta version) no te permite customizar
    			el html de los formularios definidos.
    			@silvio.bravo
    			02-Abril 2014
    			
    	 -->
							<?= $form['categoria_id']->renderRow() ?>
							<?= $form['tipo_id']->renderRow() 	?>
							<?= $form['unidad_id']->renderRow() ?>
							<?= $form['codigo']->renderRow() ?>
							<?= $form['nombre']->renderRow() ?>
							<?= $form['descripcion']->renderRow() ?>
							<?= $form['precio_lista']->renderRow() ?>
							<tr>
								<th><label style=""> Tiempo de uso en PC (Horas-Minutos) </label>
								</th>
								<td>
									<table>
										<tr>
											<td>Horas:</td>
											<td><input type="text" requerido="1" filtro="entero"
												id="horatiempo" name="horatiempo" maxlength="5" size="5" />
											</td>
											<td>Minutos:</td>
											<td><select id="minutotiempo">
													<?php for($n=0; $n<=59; $n++): ?>
													<option <?= ($n==0)?'selected="selected"':''; ?>
														value="<?= $n; ?>">
														<?= sprintf("%02d",$n) ?>
													</option>
													<?php endfor; ?>

											</select>
											</td>
										</tr>
									</table>
								</td>
							</tr>

							<tr>


							</tr>
							<?= $form['venta_unitaria']->renderRow() ?>
							<?= $form['en_pv']->renderRow() ?>
							<?= $form['activo']->renderRow() ?>
							<?= $form['tiempo']->render() ?>
							<?= $form['_csrf_token']->render() ?>
							<?= $form['id']->render() ?>


							<?php //echo $form ?>
						</tbody>
					</table>
				</div>

				<div style="float: left;" style="border: solid thin black;">
					<table border="0" id="en-paquete-actual" style="display: none;"
						width="400">
						<thead>
							<tr>
								<th width="5%" class="">Código</th>
								<th width="20%">Nombre</th>
								<th width="5%">Precio lista ($)</th>
								<th width="5%">Precio paquete ($)</th>
								<td width="5%"></td>
							</tr>
						</thead>
						<tbody>
							<tr id="prod-paq-base" style="display: none;">
								<td id="prod-paq-base-cod">NUMCOD1</td>
								<td id="prod-paq-base-nom">NUMCOD1</td>
								<td id="prod-paq-base-prc" align="right">NUMCOD1</td>
								<td id="prod-paq-base-prp" align="right"><input type="hidden"
									id="prod-paq-base-id"> <input type="text"
									id="prod-paq-base-prp-inp" filtro="numerico" size="6"
									maxlength="9">
								</td>
								<td><a href="javascript:void(0);" onclick="borrar(this);"> <span
										style="float: left;" class="ui-icon ui-icon-close"></span>
								</a>
								</td>
							</tr>
							<?php include_partial('listaPaqProductos', array('PaqProds' => $prodsEnPaq)) ?>
							</div>
						</tbody>
					</table>
				</div>


				<div style="float: right;">
					<table>
						<tbody>
							<tr>
								<td colspan="2"><h3>Modalidades de pago del producto</h3></td>
							</tr>
							<?php include_partial('listaCobranza', array('TipoCobro' => $TipoCobro,'arMod'=>$arMod)) ?>
						</tbody>
					</table>
				</div>
			</div>
		</form>

		<div id="producto_paquete_list" style="display: none;">
			<?php include_partial('listaProductos', array('Productos' => $ProductosPaq,'arPaq'=>$arPaq)) ?>
		</div>
	</div>

</div>
