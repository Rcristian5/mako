<!-- Lista de productos que componen al paquete actual -->
<?php foreach ($PaqProds as $pp): ?>
<tr id="prod-paq-id-<?php echo $pp->getProductoId() ?>"
	producto_paquete="1" producto_id="<?php echo $pp->getProductoId() ?>">
	<td id="prod-paq-base-cod"><?php echo $pp->getProductoRelatedByProductoId()->getCodigo() ?>
	</td>
	<td id="prod-paq-base-nom"><?php echo $pp->getProductoRelatedByProductoId() ?>
	</td>
	<td align="right" id="prod-paq-base-prc"><?php echo $pp->getProductoRelatedByProductoId()->getPrecioLista() ?>
	</td>
	<td align="right" id="prod-paq-base-prp"><input type="hidden"
		id="prod-paq-base-id"
		name="producto_paquete_list[<?php echo $pp->getProductoId() ?>][id]"
		value="<?php echo $pp->getProductoId() ?>"> <input type="text"
		maxlength="9" size="6" filtro="numerico" id="prod-paq-base-prp-inp"
		name="producto_paquete_list[<?php echo $pp->getProductoId() ?>][precio_paq]"
		value="<?php echo $pp->getPrecioPaquete() ?>" requerido="1">
	</td>
	<td><a onclick="borrar(this);" href="javascript:void(0);"> <span
			class="ui-icon ui-icon-close" style="float: left;"></span>
	</a>
	</td>
</tr>
<?php endforeach; ?>
<!-- Termin Lista de productos que componen al paquete actual -->
