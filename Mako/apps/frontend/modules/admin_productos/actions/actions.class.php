<?php
/*
 * Componentes Bixit
*
* Copyright (c) 2009-2011 Bixit SA de CV
* Bajo Licencia dual MIT (http://www.bixit.mx/legal/MIT-LICENSE.txt)
* y GPL (http://www.bixit.mx/legal/GPL-LICENSE.txt).
*
*/

/**
 * admin_productos actions.
 *
 * @package    bixit
 * @subpackage admin_productos
 * @author     Bixit SA de CV
 * @version    SVN: $Id: actions.class.php,v 1.9 2011/03/04 18:10:40 eorozco Exp $
 */
class admin_productosActions extends sfActions
{
	public function executeIndex(sfWebRequest $request)
	{
		$c = new Criteria();

		//Mostramos en el admin solo los productos que no son subproductos
		$c->add(ProductoPeer::ES_SUBPRODUCTO,false);
		if($request->getParameter('activo')=='t' || $request->getParameter('activo')=='')
			$c->add(ProductoPeer::ACTIVO,true);
		else
			$c->add(ProductoPeer::ACTIVO,false);
		$c->addAscendingOrderByColumn(ProductoPeer::NOMBRE);
		$this->Productos = ProductoPeer::doSelect($c);
	}

	public function executeShow(sfWebRequest $request)
	{
		$this->Producto = ProductoPeer::retrieveByPk($request->getParameter('id'));
		$this->ModalidadesCobranza = $this->Producto->getModalidadCobroProductos();
			
		$c= new Criteria();
		$c->add(PaquetePeer::PAQUETE_ID,$this->Producto->getId());

		$this->ListaProdsPaq = PaquetePeer::doSelect($c);

		$this->forward404Unless($this->Producto);
	}

	public function executeNew(sfWebRequest $request)
	{
		$this->form = new ProductoForm();

		$cp = new Criteria();
		$cp->add(ProductoPeer::ES_SUBPRODUCTO,false);
		$cp->add(ProductoPeer::ACTIVO,true);
		$cp->add(ProductoPeer::TIPO_ID,sfConfig::get('app_tipo_paquete_id'),Criteria::NOT_EQUAL);
		$cp->addAscendingOrderByColumn(ProductoPeer::NOMBRE);
		$this->ProductosPaq = ProductoPeer::doSelect($cp);
			
		$this->TipoCobro = TipoCobroPeer::doSelect(new Criteria());
		$this->arMod = array();
	}

	public function executeCreate(sfWebRequest $request)
	{
		$this->forward404Unless($request->isMethod(sfRequest::POST));

		$this->form = new ProductoForm();

		$this->processForm($request, $this->form);

		$this->setTemplate('new');
	}

	public function executeEdit(sfWebRequest $request)
	{
		$this->forward404Unless($Producto = ProductoPeer::retrieveByPk($request->getParameter('id')), sprintf('Object Producto does not exist (%s).', $request->getParameter('id')));
		$this->form = new ProductoForm($Producto);
		$this->TipoCobro = TipoCobroPeer::doSelect(new Criteria());

		//Obtenemos las opciones de cobranza y las metemos en un array bidimensional para explotarlos en la plantilla
		$this->arMod = array();
		foreach($Producto->getModalidadCobroProductos() as $mod)
		{
			$this->arMod[$mod->getCobroId()] = $mod->getNumeroPagos();
		}

		//Las operaciones de paquete, son los productos que se pueden agregar a un paquete, los que no son subproductos o que no son paquetes.
		$cp = new Criteria();
		$cp->add(ProductoPeer::ES_SUBPRODUCTO,false);
		$cp->add(ProductoPeer::TIPO_ID,sfConfig::get('app_tipo_paquete_id'),Criteria::NOT_EQUAL);
		$cp->add(ProductoPeer::ID,$this->form->getObject()->getId(),Criteria::NOT_EQUAL);
		$cp->addAscendingOrderByColumn(ProductoPeer::NOMBRE);
		$this->ProductosPaq = ProductoPeer::doSelect($cp);

		//si es un paquete obtenemos un arreglo de los productos que lo componen
		$this->arPaq = array();
		$arPa=array();
		$c= new Criteria();
		$c->add(PaquetePeer::PAQUETE_ID,$Producto->getId());
		$arPa=PaquetePeer::doSelect($c);
		if($arPa == null) $arPa=array();
		$this->prodsEnPaq = $arPa;
		foreach($arPa as $pp)
		{
			$this->arPaq[$pp->getProductoId()] = true;
		}

	}

	public function executeUpdate(sfWebRequest $request)
	{
		$this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
		$this->forward404Unless($Producto = ProductoPeer::retrieveByPk($request->getParameter('id')), sprintf('Object Producto does not exist (%s).', $request->getParameter('id')));
		$this->form = new ProductoForm($Producto);

		$this->processForm($request, $this->form);

		$this->setTemplate('edit');
	}

	public function executeDelete(sfWebRequest $request)
	{
		$request->checkCSRFProtection();

		$this->forward404Unless($Producto = ProductoPeer::retrieveByPk($request->getParameter('id')), sprintf('Object Producto does not exist (%s).', $request->getParameter('id')));
		$Producto->delete();

		$this->redirect('admin_productos/index');
	}

	protected function processForm(sfWebRequest $request, sfForm $form)
	{


		//TODO: Validar cuando bajan el número de parcialidades que se borren adecuadamente los numeros de parcialidades
		$form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));


		if ($form->isValid())
		{

			//Tenemos que evitar que den de alta cursos desde el admin de productos, los productos debidos a cursos deben de darse de alta primero en
			//por el admin de control escolar y luego deben ser modelados en el admin de productos.
			if($form->isNew())
			{
				$arProd = $request->getPostParameter('producto');
				$categoria_id = $arProd['categoria_id'];
				$tipo_id = $arProd['tipo_id'];
				if($categoria_id == sfConfig::get('app_categoria_presenciales_id') && $tipo_id == sfConfig::get('app_tipo_servicio_id'))
				{
					$this->getUser()->setFlash('error', "ERROR: Para poder vender un curso, es necesario que primero se cree desde el administrador de control escolar. Una vez creado, aparecerá en el listado de productos para poder modelar su precio de lista, tipo y forma de cobranza.");
					$this->redirect('admin_productos/index');
				}
			}
				
			//Transaccion
			$con = Propel::getConnection();
			$con->beginTransaction();
			try
			{

				//Arreglo de cantidad de pagos por cada modalidad
				$arPagos = $request->getParameter('mcc');
				//Obtenemos la lista de modalidad de pagos del objeto Producto para saber que cambio si ya existe.
				$listaMods = (!is_null($request->getParameter('mc')))?$request->getParameter('mc'):array();

				/* Modalidad cobro productos */
				$arMod=array();
				$arModObj=array();
				foreach($form->getObject()->getModalidadCobroProductos() as $mod)
				{
					//revisamos si un registro existente en base ya no se agrego en en chklist
					//Si ya no existe lo borramos.
					if(!in_array($mod->getCobroId(),$listaMods))
					{
						$c = new Criteria();
						$c->add(SubproductoCobranzaPeer::COBRO_ID,$mod->getCobroId());
						$arSubps = $form->getObject()->getSubproductoCobranzasRelatedBySubProductoDeJoinTipoCobro($c);
						error_log(count($arSubps));
						foreach($arSubps as $sub)
						{
							error_log($sub->getProductoRelatedByProductoId());
							$sub->getProductoRelatedByProductoId()->delete();
						}
						//Borramos tambien los subproductos_cobranza.
						$c = new Criteria();
						$c->add(SubproductoCobranzaPeer::SUB_PRODUCTO_DE,$form->getObject()->getId());
						$c->add(SubproductoCobranzaPeer::COBRO_ID,$mod->getCobroId());
						SubproductoCobranzaPeer::doDelete($c);
						$mod->delete();
					}
					else
					{
						$arMod[$mod->getCobroId()] = $mod->getNumeroPagos();
						$arModObj[$mod->getCobroId()] = $mod;
					}
				}
				$form->getObject()->clearModalidadCobroProductos();
				$arModalidad = array();
				if($request->getParameter('mc'))
				{
					$arModalidad = $request->getParameter('mc');
				}

				//Para cada modalidad de pago:
				foreach($arModalidad as $mod)
				{
					//Si no existia en la base:
					if(!isset($arMod[$mod]))
					{
						$nMod = new ModalidadCobroProducto();
						$nMod->setProducto($form->getObject());
						$nMod->setCobroId($mod);
						$nMod->setNumeroPagos($arPagos[$mod]);
					}
					else //Si ya existia solo verificamos q sean iguales el num de pagos
					{
						if($arMod[$mod] != $arPagos[$mod])
						{
							$arModObj[$mod]->setNumeroPagos($arPagos[$mod]);
							$arModObj[$mod]->save();
						}
					}
				}

				/* Subproducto cobranza */
				if(!$form->getObject()->isNew()){
				 //Borramos los prods relacionados con paquete y los regeneramos en caso de existir
					$c= new Criteria();
					$c->add(PaquetePeer::PAQUETE_ID,$form->getObject()->getId());
					$arPa=PaquetePeer::doSelect($c);
					if($arPa == null) $arPa=array();
					foreach($arPa as $pp)
					{
						$pp->delete();
					}
				}

				/* Paquetes */
				//Si es un paquete obtenemos los productos relacionados
				$listaPaqs = (!is_null($request->getParameter('producto_paquete_list')))?$request->getParameter('producto_paquete_list'):array();

				//Si el producto paquete no es nuevo vemos que no hayan borrado paquetes.
				try{
					if(!$form->isNew())
					{
						$paqsactual = $form->getObject()->getPaquetesRelatedByPaqueteId();
						if(is_array($paqsactual))
						{
							foreach ($paqsactual as $pqa)
							{
								if(!in_array($pqa->getProductoId(), $listaPaqs))
								{
									$pqa->delete();
								}
							}
						}
					}
				}
				catch (Exception $e)
				{
					error_log("Error[Trono:]. ".$e->getMessage());
				}

				//Ahora ingresamos o actualizamos cada paquete
				foreach($listaPaqs as $prod_id => $pp)
				{
					//echo "ProdId: ".$prod_id;
					//print_r($pp); exit;
					if($form->isNew())
					{
						$paq = new Paquete();
						$paq->setProductoRelatedByPaqueteId($form->getObject());
						$paq->setPrecioPaquete($pp['precio_paq']);
						$paq->setProductoId($prod_id);
					}
					else
					{
						//Consultamos el producto en caso de que exista
						$paq = PaquetePeer::retrieveByPK($form->getObject()->getId(), $prod_id);
						if($paq == null)
						{
							$paq = new Paquete();
							$paq->setProductoRelatedByPaqueteId($form->getObject());
							$paq->setPrecioPaquete($pp['precio_paq']);
							$paq->setProductoId($prod_id);
						}
						else
						{
							//Si difiere el precio se actualiza.
							if($paq->getPrecioPaquete() != $pp['precio_paq'])
							{
								$paq->setPrecioPaquete($pp['precio_paq']);
							}
						}
					}
				}


				$es_nuevo = true;
				if(!$form->isNew())
				{
					$es_nuevo = false;
				}
				$Producto = $form->save();
				$con->commit();
				//$this->getUser()->setFlash('ok', 'Se han guardado correctamente los detalles del producto');
				if($es_nuevo)
				{
					//Disparamos evento de producto nuevo
					error_log("Despachamos triger de producto.creado");
					sfProjectConfiguration::getActive()->getEventDispatcher()->notify(new sfEvent($Producto, 'producto.creado',array('producto'=>$Producto)));
				}
				else
				{
					//Disparamos evento de producto modificado
					error_log("Despachamos triger de producto.modificado");
					sfProjectConfiguration::getActive()->getEventDispatcher()->notify(new sfEvent($Producto, 'producto.modificado',array('producto'=>$Producto)));
				}
			}
			catch (Exception $e)
			{
				error_log("Error[processForm]:".$e->getMessage());
				$con->rollBack();
				//$this->getUser()->setFlash('error', 'No es posible eliminar los datos de cobranza porque se ha realizado ya una venta parcial.');
				$this->redirect('admin_productos/edit?id='.$form->getObject()->getId());
			}
				
			$this->redirect('admin_productos/show?id='.$Producto->getId());
		}
	}

	/**
	 * Admin de cobranza
	 * @param sfWebRequest $request
	 * @return unknown_type
	 */
	public function executeCobranza(sfWebRequest $request)
	{
		$this->Productos = ProductoPeer::doSelect(new Criteria());
	}


	/**
	 * Guarda y genera los productos de cobranza
	 * @param sfWebRequest $request
	 * @return void
	 */
	public function executeGuardarCostoCobranza(sfWebRequest $request)
	{
		$preciosCob = $request->getParameter('preciosCob');
		//print_r($preciosCob);

		$producto_padre_id = key($preciosCob);
		$pp = ProductoPeer::retrieveByPK($producto_padre_id);
		$this->Producto = $pp;

		foreach($preciosCob as $prod_id => $cob)
		{

			//Borramos todas las relaciones de cobranza para que se regeneren apartir de los cambios que hubo.
			foreach($pp->getSubproductoCobranzasRelatedBySubProductoDe() as  $spv)
			{
				error_log("borrando spv".$spv);


				$spc_id = $spv->getProductoRelatedByProductoId()->getId();
				$spc = ProductoPeer::retrieveByPK($spc_id);
				$spc->setActivo(false);
				$spc->save();

				$cobro_id = $spv->getCobroid();
				$spvInstancia = SubproductoCobranzaPeer::retrieveByPK($spc_id, $cobro_id);
				$spvInstancia->delete();
			}
				
			//Obtenemos el producto padre
			foreach($cob as $cob_id => $pagos)
			{
				//Obtenemos el tipo de cobro
				$tp = TipoCobroPeer::retrieveByPK($cob_id);
					
				foreach($pagos as $nump => $dp)
				{
					//Primero validamos q no sean negativos los valores
					if($dp['precio'] < 0 || $dp['tiempo']  < 0)
					{
						$this->getUser()->setFlash('error', 'No es posible ingresar valores negativos de monto o tiempo');
						$this->redirect('admin_productos/show?id='.$this->Producto->getId());
					}
						
					//Vemos si el subproducto ya existia si es asi no creamos uno nuevo solo verificamos si cambio
					if($dp['subproducto_id'] != '')
					{
						$spExistente = ProductoPeer::retrieveByPK($dp['subproducto_id']);
						if($spExistente->getPrecioLista() != $dp['precio'])
						{
							$spExistente->setPrecioLista($dp['precio']);
						}
						if($spExistente->getTiempo() != $dp['tiempo'])
						{
							$spExistente->setTiempo($dp['tiempo']);
						}
						$spExistente->setNombre($pp." - $tp ".($nump+1) ." de ".count($pagos) );
						$spExistente->setActivo(true);

						//La relacion de producto y subproducto con su tipo de cobranza
						$spc = new SubproductoCobranza();
						$spc->setProductoRelatedByProductoId($spExistente);
						$spc->setProductoRelatedBySubProductoDe($pp);
						$spc->setTipoCobro($tp);
						$spc->setNumeroPago($nump+1);

						$spExistente->save();
						$spc->save();
					}
					else
					{
						//El subproducto es una copia del producto con diferente precio y nombre
						$sp = $pp->copy();
						$sp->setNombre($pp." - $tp ".($nump+1) ." de ".count($pagos) );
						$sp->setPrecioLista($dp['precio']);
						$sp->setTiempo($dp['tiempo']);
						$sp->setEsSubproducto(true);
						$sp->setVentaUnitaria(true);
						$sp->setEnPv(false);

						//La relacion de producto y subproducto con su tipo de cobranza
						$spc = new SubproductoCobranza();
						$spc->setProductoRelatedByProductoId($sp);
						$spc->setProductoRelatedBySubProductoDe($pp);
						$spc->setTipoCobro($tp);
						$spc->setNumeroPago($nump+1);

						//Obtenemos el siguiente id
						$sp_id = ProductoPeer::getLastId() + 1;
						//Guardamos las nuevas relaciones.
						$sp->setId($sp_id);

						//Este producto se declara como venta unitaria
						$pp->setVentaUnitaria(true);
						$sp->save();
					}

				}
			}
		}

		//Disparamos evento de producto modificado
		sfProjectConfiguration::getActive()->getEventDispatcher()->notify(new sfEvent($this->Producto, 'producto.modificado',array('producto'=>$this->Producto)));

		$this->getUser()->setFlash('ok', 'Se han guardado correctamente los detalles de cobranza del producto');
		$this->redirect('admin_productos/show?id='.$this->Producto->getId());
		//exit;
	}

}
