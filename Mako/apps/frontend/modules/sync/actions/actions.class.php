<?php
/**
 * sync actions.
 *
 * @package    mako
 * @subpackage sync
 * @author     Bixit SA de CV
 * @version    SVN: $Id: actions.class.php,v 1.1 2010/08/31 00:54:25 eorozco Exp $
 */
class syncActions extends sfActions
{
    /**
     * Recibe los datos a sincronizar con la base local
     *
     * @WSMethod(webservice='MakoApi')
     *
     * @param string $datos La cadena YAML que contiene los objetos a sincronizar.
     * @param string $operacion La operación que se realizó remotamente.
     * @param string $clase La clase principal sobre la que se ejecuto la operación.
     * @param string $pk La pk del objeto, para ser usado en operaciones de modificación.
     *
     * @return string El resultado de la operación.
     */
    public function executeRecibeSync($request)
    {
        $datos     = $request->getParameter('datos');
        $operacion = $request->getParameter('operacion');
        $clase     = $request->getParameter('clase');
        $pk        = $request->getParameter('pk');

        error_log("[SYNC] datos: " . (count($datos)) . ", operacion: [ $operacion ], clase: [ $clase ], pk: [ $pk ]");

        $con = Propel::getConnection('propel');
        $con->beginTransaction();
        try {
            $obj = new $clase();
            $res = $obj->getPeer()->cargaSync($datos, $operacion, $clase, $pk);

            $con->commit();
            return sfView::SUCCESS;
        }
        catch(Exception $e) {
            error_log("Desde sync/RecibeSync @ 46: { " . $e->getMessage() . "$res }");
            $con->rollBack();
            $me = $this->isSoapRequest() ? new SoapFault('Server', $e->getMessage() . ": " . $res) : $e->getMessage() . ": " . $res;
            throw $me;
            return sfView::ERROR;
        }

        $me = $this->isSoapRequest() ? new SoapFault('Server', $res) : $res;
        throw $me;
        return "Error default";
    }
}
