<?php

/**
 * admin_cupones actions.
 *
 * @package    mako
 * @subpackage admin_cupones
 * @author     Bixit SA de CV
 * @version    SVN: $Id: actions.class.php,v 1.7 2010/12/10 20:13:26 eorozco Exp $
 */
class admin_cuponesActions extends sfActions
{
	public function executeIndex(sfWebRequest $request)
	{
		$this->PromocionCupons = PromocionCuponPeer::doSelect(new Criteria());
	}

	public function executeShow(sfWebRequest $request)
	{
		$this->PromocionCupon = PromocionCuponPeer::retrieveByPk($request->getParameter('id'));
		$this->forward404Unless($this->PromocionCupon);
	}

	public function executeNew(sfWebRequest $request)
	{
		$this->form = new PromocionCuponForm();
	}

	public function executeCreate(sfWebRequest $request)
	{
		$this->forward404Unless($request->isMethod(sfRequest::POST));

		$this->form = new PromocionCuponForm();

		$this->processForm($request, $this->form);

		$this->setTemplate('new');
	}

	public function executeEdit(sfWebRequest $request)
	{
		$this->forward404Unless($PromocionCupon = PromocionCuponPeer::retrieveByPk($request->getParameter('id')), sprintf('Object PromocionCupon does not exist (%s).', $request->getParameter('id')));
		$this->form = new PromocionCuponForm($PromocionCupon);
	}

	public function executeUpdate(sfWebRequest $request)
	{
		$this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
		$this->forward404Unless($PromocionCupon = PromocionCuponPeer::retrieveByPk($request->getParameter('id')), sprintf('Object PromocionCupon does not exist (%s).', $request->getParameter('id')));
		$this->form = new PromocionCuponForm($PromocionCupon);

		$this->processForm($request, $this->form);

		$this->setTemplate('edit');
	}

	public function executeDelete(sfWebRequest $request)
	{
		$request->checkCSRFProtection();

		$this->forward404Unless($PromocionCupon = PromocionCuponPeer::retrieveByPk($request->getParameter('id')), sprintf('Object PromocionCupon does not exist (%s).', $request->getParameter('id')));
		$PromocionCupon->delete();

		$this->redirect('admin_cupones/index');
	}

	protected function processForm(sfWebRequest $request, sfForm $form)
	{
		$form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
		if ($form->isValid())
		{
			$operador = $this->getUser()->getAttribute('usuario');
			$form->getObject()->setUsuario($operador);
			$PromocionCupon = $form->save();
			if(!$form->isNew())
			{
				sfProjectConfiguration::getActive()->getEventDispatcher()->notify(new sfEvent($PromocionCupon, 'promocioncupon.actualizado',$PromocionCupon));
			}
			else
			{
				sfProjectConfiguration::getActive()->getEventDispatcher()->notify(new sfEvent($PromocionCupon, 'promocioncupon.creado',$PromocionCupon));
			}
			$this->redirect('admin_cupones/show?id='.$PromocionCupon->getId());
		}
	}

	/**
	 * Registra un cupon como otorgado dados cupon_id y socio_id
	 * @param sfWebRequest $request
	 */
	public function executeOtorgarCupon(sfWebRequest $request)
	{
		$this->getResponse()->setContentType('application/json');

		$cupon_id = $request->getParameter('cupon_id');
		$socio_id = $request->getParameter('socio_id');
		$operador_id = $this->getUser()->getAttribute('usuario')->getId();
		$res = PromocionCuponPeer::otorgar($cupon_id,$socio_id,$operador_id);
		return $this->renderText(json_encode(array('resultado'=>'ok','msg'=>'Se ha otorgado el cupón.')));
	}

	/**
	 * Registra un cupon como otorgado dados cupon_id, validador y socio_id
	 * @param sfWebRequest $request
	 */
	public function executeOtorgarCuponManual(sfWebRequest $request)
	{
		$this->getResponse()->setContentType('application/json');

		$codigo = $request->getParameter('codigo_cupon');
		$socio_id = $request->getParameter('socio_id');
		$validador = $request->getParameter('validador');
		$operador_id = $this->getUser()->getAttribute('usuario')->getId();
		$res = PromocionCuponPeer::cuponOtorgable($codigo,$validador,$socio_id,$operador_id);

		return $this->renderText(json_encode($res));
	}

	/**
	 * Regresa la lista de cupones un arhcivo csv.
	 * @param sfWebRequest $request
	 */
	public function executeListaCupones(sfWebRequest $request)
	{
		$promocion_id = $request->getParameter('promocion_id');
		$this->getResponse()->clearHttpHeaders();
		$this->getResponse()->setHttpHeader('Content-Type', 'application/vnd.ms-excel;charset=utf8');
		$this->getResponse()->setHttpHeader('Content-Disposition','attachment; filename='.$promocion_id.'.csv');

		$c = new Criteria();
		$c->add(PromocionCuponPeer::ID,$promocion_id);
		$this->promocion = PromocionCuponPeer::doSelectOne($c);
		$this->setLayout(false);
	}

}
