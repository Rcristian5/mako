<!-- 
/*
 * Componentes Bixit
 *
 * Copyright (c) 2009-2011 Bixit SA de CV
 * Bajo Licencia dual MIT (http://www.bixit.mx/legal/MIT-LICENSE.txt)
 * y GPL (http://www.bixit.mx/legal/GPL-LICENSE.txt).
 *
 */
 -->
<?php use_javascript('jquery.ui.js') ?>
<?php use_javascript('date.format.js') ?>
<?php use_stylesheet('datatables.css') ?>

<script>
var urlAccion = '<?php echo url_for('admin_cupones/otorgarCupon')?>';

$(document).ready(function(){

	//Cuando oprimimos la liga de otorgar ejecutamos la ventana de buscar socio
	$(document).bind('cupon.otorgar',function(ev,cupon_id){
		var coord = oTable.fnGetPosition(document.getElementById('cupon_'+cupon_id));
		$("#cupon_id").val(cupon_id);
		$("#cupon_id").attr('row',coord[0]);
		$("#cupon_id").attr('col',coord[1]);
		$wBuscador.dialog('open');
	});

	//Cuando se selecciona al usuario a otorgar el cupón
	$(document).bind('socio.seleccionado',function(ev, socio_id, socio_nombre){

		$.ajax({
			  url: urlAccion,
			  dataType: 'json',
			  data: {socio_id: socio_id, cupon_id: $("#cupon_id").val()},
			  success: function(req,res){
					var fila = $("#cupon_id").attr('row');
					var col = $("#cupon_id").attr('col');
				  	var dat = oTable.fnGetData( fila );
					//Actualizamos los valores de la tabla
				  	//Columna "Otorgado" 2
				  	oTable.fnUpdate( 'SI', fila, 2);
				  	//Columna "Otorgado a" 4
				  	oTable.fnUpdate( socio_nombre, fila, 4);
				  	//Columna "Fecha otorgado" 5
				  	var ahora = new Date();
				  	oTable.fnUpdate( ahora.format("d-m-Y H:i"), fila, 5);
				  	//Columna 3 "Otorgado por"
				  	oTable.fnUpdate( '<?php echo $sf_user->getAttribute('usuario') ?>', fila, 3);
				  	//Columna controles 8
				  	oTable.fnUpdate( ' ', fila, 8);
			  }
		});
		
		$wBuscador.dialog('close');
	});

	//Cuando termina de buscar el componente	
	$(document).bind('socios.buscador.encontrados',function(){
		$("#buscador-socios a").text("Otorgar cupón").css('color','green');
	});

	//Ventana del buscador de socios
	var $wBuscador = $("#ventana-buscador").dialog({
		modal: true,
		minWidth: 250,
		width: 320,
		maxWidth: 400,
		minHeight: 350,
		height: 600,
		maxHeight: 710,
		autoOpen: false,
		position: ['center','top'],
		closeOnEscape: false,
		title:	'Seleccione socio'
	});

	
});
</script>


<input
	type="hidden" name="cupon_id" id="cupon_id" value="" />

<div class="ui-widget-header ui-corner-top tsss" style="padding: 5px;">Promoción
	de Cupones</div>
<div class="ui-widget ui-corner-bottom sombra">
	<div class="ui-widget-content ui-corner-bottom  clearfix"
		style="padding: 5px;">

		<div id="ui-contenedor" style="min-height: 200px;">

			<table style="float: left; margin-right: 40px;" id="datos-promocion">
				<caption>
					<h3>Datos de la promoción</h3>
				</caption>
				<tbody>
					<tr>
						<th>Nombre:</th>
						<td><?php echo $PromocionCupon->getNombre() ?></td>
					</tr>
					<tr>
						<th>Registró:</th>
						<td><?php echo ($PromocionCupon->getUsuario())?$PromocionCupon->getUsuario() : '' ?>
						</td>
					</tr>

					<tr>
						<th>Descripción:</th>
						<td><?php echo $PromocionCupon->getDescripcion() ?></td>
					</tr>
					<tr>
						<th>Cupones generados:</th>
						<td><?php echo $PromocionCupon->getCantidadCupones() ?></td>
					</tr>
					<tr>
						<th>Cupones otorgados:</th>
						<td><?php 
						$c = new Criteria(); $c->add(CuponPeer::OTORGADO_A,null,Criteria::NOT_EQUAL);
						echo count($PromocionCupon->getCupons($c))
						?>
						</td>
					</tr>
					<tr>
						<th>Cupones utilizados:</th>
						<td><?php 
						$c = new Criteria(); $c->add(CuponPeer::UTILIZADO,false,Criteria::NOT_EQUAL);
						echo count($PromocionCupon->getCupons($c))
						?>
						</td>
					</tr>

					<tr>
						<th>Vigente de:</th>
						<td><?php echo $PromocionCupon->getVigenteDe() ?></td>
					</tr>
					<tr>
						<th>Vigente a:</th>
						<td><?php echo $PromocionCupon->getVigenteA() ?></td>
					</tr>
					<tr>
						<th>Activo:</th>
						<td><?php echo ($PromocionCupon->getActivo())?'SI':'NO' ?></td>
					</tr>
					<tr>
						<th>Descuento:</th>
						<td><?php echo sprintf("%.03f %%",$PromocionCupon->getDescuento()) ?>
						</td>
					</tr>
					<tr>
						<th>Creado:</th>
						<td><?php echo $PromocionCupon->getCreatedAt() ?></td>
					</tr>
					<tr>
						<th>Actualizado:</th>
						<td><?php echo $PromocionCupon->getUpdatedAt() ?></td>
					</tr>
                                        
                                        <?php
                                        //Se le agrega la validación para verificar al super.usuario (No es optimo)
                                        $id=sfContext::getInstance()->getUser()->getAttribute('usuario')->getId();
                                        if ($id == 1){
                                        ?>

					<tr>
						<th>Archivo para <br />cupones imprimibles:
						</th>
						<td><a
							href="<?php echo url_for('admin_cupones/listaCupones?promocion_id='.$PromocionCupon->getId());?>">Descargar
								archivo</a></td>
                                        </tr>
                                        <?php
                                        }
                                        ?>


				</tbody>
			</table>

			<table id="lista-productos">
				<caption>
					<h3>Productos en esta promoción</h3>
				</caption>
				<tbody>
					<?php foreach ($PromocionCupon->getPromocionCuponProductos() as $pp):?>
					<tr>
						<td><li><?php echo $pp->getProducto() ?></li></td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
		<hr />
                <?php
                    //Se le agrega la validación para verificar al super.usuario (No es optimo)
                    $id=sfContext::getInstance()->getUser()->getAttribute('usuario')->getId();
                    if ($id == 1){
                ?>

		<a href="<?php echo url_for('admin_cupones/edit?id='.$PromocionCupon->getId()) ?>">Modificar</a>
                &nbsp;| <?php } ?><a href="<?php echo url_for('admin_cupones/index') ?>">Ver
			Promociones</a> <br></br>
		<?php //include_partial('cupones',array('promocion'=>$PromocionCupon))?>

	</div>
</div>

<div id="ventana-buscador">
	<div id="buscador-socios">
		<?php include_component('registro','buscaSocios')?>
	</div>
</div>
