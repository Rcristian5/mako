<?php
/**
 * Lista los cupones en una promoción de cupones.
 */

$c=1;
?>

<?php use_javascript('plugins/jquery.dataTables.js') ?>
<?php use_javascript('jquery.ui.js') ?>


<script>

var oTable;
$(document).ready(function() {

   oTable = $('#lista-cupones').dataTable({
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
        "bJQueryUI": true,
        "sPaginationType": "full_numbers"
     });
   
});

</script>

<br />
<div class="ui-widget">
	<div class="ui-widget-content ui-corner-all ts">

		<table id="lista-cupones" width="100%">
			<caption>Lista de cupones generados</caption>
			<thead>
				<tr>
					<th>#</th>
					<th>Código cupón</th>
					<th>Otorgado</th>
					<th>Otorgado por</th>
					<th>Otorgado a</th>
					<th>Fecha otorgado</th>
					<th>Utilizado</th>
					<th>Fecha utilizado</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($promocion->getCupons() as $cupon): ?>
				<tr>
					<td><?php echo $c; $c++;?></td>
					<td><?php echo $cupon->getCodigo() ?></td>
					<td><?php echo ($cupon->getOtorgadoA())?'SI':'NO' ?></td>
					<td><?php echo ($cupon->getOtorgadoPor())? $cupon->getUsuario():'--' ?>
					</td>
					<td><?php echo ($cupon->getOtorgadoA())? $cupon->getSocio() : '--' ?>
					</td>
					<td align="center"><?php echo ($cupon->getFechaOtorgado())? $cupon->getFechaOtorgado("%d-%m-%Y %H:%M") : '--' ?>
					</td>
					<td align="center"><?php echo ($cupon->getUtilizado())?'SI':'NO' ?>
					</td>
					<td align="center"><?php echo ($cupon->getFechaUtilizado())? $cupon->getFechaUtilizado("%d-%m-%Y %H:%M") : '--' ?>
					</td>
					<td id="cupon_<?php echo $cupon->getId() ?>"><?php if($cupon->getOtorgadoA() == null): ?>
						<a href="javascript:void(0);"
						onclick="$(this).trigger('cupon.otorgar',['<?php echo $cupon->getId() ?>']);">Otorgar</a>
						<?php endif; ?>
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
