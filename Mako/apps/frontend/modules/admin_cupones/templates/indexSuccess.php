<?php use_javascript('plugins/jquery.dataTables.js') ?>
<?php use_javascript('jquery.ui.js') ?>
<?php use_stylesheet('datatables.css') ?>


<script>
var t;
$(document).ready(function() {

   t = $('#lista-promociones').dataTable({
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
        "bJQueryUI": true,
        "sPaginationType": "full_numbers"
     });
});

</script>
<div class="ui-widget-header ui-corner-top tsss" style="padding: 5px;">Promociones
	con Cupones</div>
<div class="ui-widget ui-corner-bottom sombra">
	<div class="ui-widget-content ui-corner-bottom" style="padding: 5px;">

		<br /> <span style="float: left;" class="ui-icon ui-icon-circle-plus"></span>
		<a href="<?php echo url_for('admin_cupones/new') ?>"> Agregar
			promoción de cupones </a> <br /> <br /> <br />
		<table id="lista-promociones" width="100%">
			<caption>&nbsp;</caption>
			<thead>
				<tr>
					<th>Revisar</th>
					<th>Nombre</th>
					<th>Vigente de</th>
					<th>Vigente a</th>
					<th>Activo</th>
					<th>Descuento</th>
					<th>Cupones</th>
					<th>Otorgados</th>
					<th>Utilizados</th>
					<th>Creación</th>
					<th>Modificación</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($PromocionCupons as $PromocionCupon): ?>
				<tr>
					<td align="center"><a
						href="<?php echo url_for('admin_cupones/show?id='.$PromocionCupon->getId()) ?>">
							<span class="ui-icon  ui-icon-circle-zoomin"></span>
					</a>
					</td>
					<td><?php echo $PromocionCupon->getNombre() ?></td>
					<td align="center"><?php echo $PromocionCupon->getVigenteDe("%d-%m-%Y") ?>
					</td>
					<td align="center"><?php echo $PromocionCupon->getVigenteA("%d-%m-%Y") ?>
					</td>
					<td align="center"><?php echo ($PromocionCupon->getActivo())?'SI':'NO' ?>
					</td>
					<td align="right"><?php echo sprintf("%.03f",$PromocionCupon->getDescuento()) ?>
						(%)</td>
					<td align="right"><?php echo $PromocionCupon->getCantidadCupones() ?>
					</td>
					<td align="right"><?php 
					$c = new Criteria(); $c->add(CuponPeer::OTORGADO_A,null,Criteria::NOT_EQUAL);
					echo count($PromocionCupon->getCupons($c))
					?>
					</td>
					<td align="right"><?php 
					$c = new Criteria(); $c->add(CuponPeer::UTILIZADO,false,Criteria::NOT_EQUAL);
					echo count($PromocionCupon->getCupons($c))
					?>
					</td>
					<td align="center"><?php echo $PromocionCupon->getCreatedAt() ?></td>
					<td align="center"><?php echo $PromocionCupon->getUpdatedAt() ?></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

	</div>
</div>

