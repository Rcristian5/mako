<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<?php use_javascript('jquery.ui.js') ?>
<?php use_javascript('AdminCuponVal.js') ?>

<script>
// Reingenieria Mako C. ID 27. Responsable:  FE. Fecha: 23-01-2014.  Descripción del cambio: Se valido campos de fecha inicial y final.
/*
$(document).ready(function() 
{
	$(".datepicker").datepicker({
				firstDay: 1,
				changeYear: true,
				changeMonth: true,
				dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
				dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
				monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
				minDate: new Date(2010, 1 - 1, 1),
				constrainInput: true,
				dateFormat: 'yy-mm-dd'
	});
});
*/
// Fecha: 23-01-2014 - Fin.
function confirmar()
{
	var cupones = $("#promocion_cupon_cantidad_cupones").val();
	var descto = $("#promocion_cupon_descuento").val();
	if(descto == 0 || descto > 100 || descto =='')
	{
		alert('El descuento debe ser mayor a 0% y menor al 100%. Por favor verifique el descuento');
		return false;
	}
	if(cupones == 0 || cupones == '')
	{
		alert('Debe ingresar cuántos cupones quiere emitir en esta promoción.');
		return false;
	}
	if(confirm('Favor de confirmar que desea crear '+cupones+' cupones para esta promoción, ya que posteriormente no podrá ser modificada dicha cantidad.'))
	{
		return true;
	}
	return false;
}

</script>

<div class="ui-widget-header ui-corner-top tsss" style="padding: 5px;">Generar
	nueva promoción de cupones</div>
<div class="ui-widget ui-corner-bottom sombra">
	<div class="ui-widget-content ui-corner-bottom" style="padding: 15px;">

		<form
			action="<?php echo url_for('admin_cupones/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>"
			method="post"
			<?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
			<?php if (!$form->getObject()->isNew()): ?>
			<input type="hidden" name="sf_method" value="put" />
			<?php endif; ?>
			<table>
				<tfoot>
					<tr>
						<td colspan="2">
							<hr /> &nbsp;<a
							href="<?php echo url_for('admin_cupones/index') ?>">Ver
								Promociones</a> <?php if ($form->getObject()->isNew()): ?> <input
							type="submit" value="Guardar" onclick="return confirmar();" /> <?php endif; ?>
							<?php if (!$form->getObject()->isNew()): ?> <input type="submit"
							value="Guardar" /> <?php endif; ?>


						</td>
					</tr>
				</tfoot>
				<tbody>
					<?php echo $form ?>
				</tbody>
			</table>
		</form>
	</div>
</div>
