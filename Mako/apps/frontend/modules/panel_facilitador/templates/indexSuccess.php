<?php use_stylesheet('control_escolar/control_escolar.css') ?>
<?php use_stylesheet('jquery.windows.css') ?>
<?php use_javascript('jquery.ui.js') ?>
<?php use_javascript('plugins/jquery.windows.js') ?>
<?php use_javascript('control_escolar/panel_facilitador.js') ?>
<?php use_stylesheet('control_escolar/registro_asistencia.css') ?>
<?php use_stylesheet('datatables.css') ?>
<?php use_stylesheet('TableTools.css') ?>
<?php use_javascript('plugins/jquery.dataTables.js') ?>
<?php use_javascript('plugins/dataTables/TableTools.js') ?>
<?php use_javascript('plugins/dataTables/ZeroClipboard.js') ?>
<?php use_javascript('control_escolar/lista_asistencia.js') ?>
<div id="windows-wrapper">
	<div id="registro" window-title="Panel de control" window-state="min">

		<?php include_partial('panel', array('grupos' => $grupos)) ?>

	</div>

	<div id="cursos" window-title="Listas de alumnos" window-state="max">
		<h2>Listas de alumnos</h2>
		<?php if ($sf_user->hasFlash('notice')): ?>
		<div class="flash_notice ui-state-highlight ui-corner-all">
			<span style="float: left; margin-right: 0.3em;"
				class="ui-icon ui-icon-info"></span> <strong><?php echo $sf_user->getFlash('notice') ?>
			</strong>
		</div>
		<?php endif; ?>
		<div id="tabs-panel">
			<ul>
				<li><a
					href="<?php echo url_for('panel_facilitador/asistencia?grupo='.$grupo) ?>">Registro
						de Asistencia</a></li>
				<li><a
					href="<?php echo url_for('panel_facilitador/evaluaciones?grupo='.$grupo) ?>">Registro
						Evalucaiones</a></li>
				<li><a
					href="<?php echo url_for('panel_facilitador/bitacora?grupo='.$grupo) ?>">Bit&aacute;cora
						de socio</a></li>
				<li><a
					href="<?php echo url_for('panel_facilitador/estudiocualitativo?grupo='.$grupo) ?>">Estudio
						cualitativo</a></li>
			</ul>
		</div>
		<script type="text/javascript">
        $(function() {
          $("#tabs-panel").tabs({ selected: <?php echo $tab ?> });
        });
      </script>

	</div>

</div>

