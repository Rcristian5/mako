<?php use_stylesheet('analogclock.css') ?>
<?php use_stylesheet('control_escolar/panel.css') ?>
<?php use_javascript('i18n/ui.datepicker-es.js') ?>
<?php use_javascript('control_escolar/panel.js') ?>
<?php use_javascript('control_escolar/analogclock.js') ?>
<?php use_javascript('plugins/jquery.qtip.js') ?>

<script type="text/javascript">
  var cursos = [
    <?php foreach ($grupos as $grupo): ?>
        {
          curso : '<?php echo $grupo->getGrupo()->getCurso()->getNombre() ?>',
          aula  : '<?php echo $grupo->getGrupo()->getRecursoInfraestructura()->getNombre() ?>',
          inicio : '<?php echo substr($grupo->getGrupo()->getHorario()->getFechaInicio(),0,10) ?>',
          cupo : '<?php echo $grupo->getGrupo()->getCupo() ?>',
          asistencia : '<?php echo '/panel_facilitador/asistencia?grupo='.$grupo->getGrupo()->getId() ?>',
          evaluaciones : '<?php echo '/panel_facilitador/evaluaciones?grupo='.$grupo->getGrupo()->getId() ?>',
          bitacora : '<?php echo '/panel_facilitador/bitacora?grupo='.$grupo->getGrupo()->getId() ?>',
          cualitativo : '<?php echo '/panel_facilitador/estudiocualitativo?grupo='.$grupo->getGrupo()->getId() ?>'
        },
    <?php endforeach; ?>
    ];
</script>

<h2>Panel de control</h2>

<div id="stuffs">

	<div id="dia" class="ui-widget-content ui-corner-all">
		<div id="day-name" class="ui-widget-header ui-corner-all"></div>
		<div id="day-number"
			class="ui-widget-content ui-state-highlight ui-corner-all">
			<span></span>
		</div>
	</div>
	<div id="calendario-linea" class="ui-widget-content ui-corner-all">
		<div id="inline-calendar"></div>
	</div>

	<div id="reloj" class="ui-widget-content ui-corner-all">
		<ul id="clock">
			<li id="sec"></li>
			<li id="hour"></li>
			<li id="min"></li>
		</ul>
	</div>
	<p class="clear">&nbsp;</p>
</div>
<div id="detalle_curso" class="ui-widget-content ui-corner-all">
	<?php if(count($grupos)>0): ?>
	<h3>Grupos asignados</h3>
	<?php foreach ($grupos as $grupo): ?>
	<?php if( ($grupo->getGrupo()->getHorario()->getFechaFin('U') - date('U')) > 0  ): ?>
	<div
		class="ui-widget-content ui-state-default ui-corner-all datos-grupo grupo-<?php echo $grupo->getGrupo()->getId()?>">
		<a
			href="<?php echo url_for('panel_facilitador/index?grupo='.$grupo->getGrupo()->getId()) ?>"
			id="<?php echo $grupo->getGrupo()->getClave() ?>"><?php echo $grupo->getGrupo()->getClave().': '.$grupo->getGrupo()->getCurso()->getNombre() ?>
		</a>
	</div>
	<?php else: ?>
	<div
		class="ui-widget-content ui-state-default ui-corner-all datos-grupo ui-state-disabled grupo-<?php echo $grupo->getGrupo()->getId()?>">
		<a
			href="<?php echo url_for('panel_facilitador/index?grupo='.$grupo->getGrupo()->getId()) ?>"
			id="<?php echo $grupo->getGrupo()->getClave() ?>"><?php echo $grupo->getGrupo()->getClave().': '.$grupo->getGrupo()->getCurso()->getNombre() ?>
		</a>
	</div>
	<?php endif; ?>
	<?php endforeach ?>
	<?php else: ?>
	<div class="ui-state-error no-cursos">No tienes grupos asignados</div>
	<?php endif; ?>
</div>
