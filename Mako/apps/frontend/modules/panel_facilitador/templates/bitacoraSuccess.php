<?php use_stylesheet('control_escolar/registro_asistencia.css') ?>
<?php use_stylesheet('datatables.css') ?>
<?php use_stylesheet('TableTools.css') ?>
<?php use_javascript('plugins/jquery.dataTables.js') ?>
<?php use_javascript('plugins/dataTables/TableTools.js') ?>
<?php use_javascript('plugins/dataTables/ZeroClipboard.js') ?>
<?php use_javascript('control_escolar/bitacora_socio.js') ?>
<script type="text/javascript">
  vTable =  $('#table-bitacora').dataTable({
                    "sDom": '<"H"lfr>t<"F"ip>T<"clear">',
		"bPaginate": false,
		"bLengthChange": true,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
                    "bJQueryUI": true,
                    "aoColumns" : [
                                    null,
                                    null,
                                    null,
                                    {"bVisible":    false},
                                    null,
                               ],
                    "aaSorting": [[0, 'asc']]
                });
 $(function() {

 });
 <?php if($lista['horario']['grupo_id']!=""): ?>
  $('.grupo-<?php echo $lista['horario']['grupo_id']?>').removeClass('ui-state-default');
<?php endif; ?>
</script>

<div id="lista_asistencia" class="ui-widget-content ui-corner-all">
	<?php $sf_user->setAttribute('grupo_bitacora', $lista['horario']['grupo_id']) ?>
	<h3>
		Grupo:
		<?php echo $lista['horario']['clave']?>
	</h3>
	<p class="datos">
		<span>Curso:</span>
		<?php echo $lista['horario']['nombre']?>
	</p>
	<p class="datos">
		<span>Clave:</span>
		<?php echo $lista['horario']['clave_curso']?>
	</p>
	<p class="datos">
		<span>Aula:</span>
		<?php echo $lista['horario']['aula']?>
	</p>
	<?php if(count($lista['alumnos'])>0): ?>
	<form id="form">
		<!--Lista de asistencia -->
		<table width="100%" border="0" class="display" id="table-bitacora">
			<thead>
				<tr>
					<th>Nombre</th>
					<th>Edad</th>
					<th>Sexo</th>
					<th>Correo Electr&oacute;nico</th>
					<th>Adeudo de Pagos</th>
				</tr>
				<tr>

				</tr>
			</thead>
			<tbody>
				<?php if(count($lista['alumnos'])>0): ?>
				<?php foreach ($lista['alumnos'] as $alumno): ?>
				<tr id="bitacora-<?php echo $alumno->getAlumnoId()?>">
					<td><a
						href="<?php echo url_for('panel_facilitador/bitacoraDetalle?id='.$alumno->getAlumnoId()) ?>"
						class="bitacora-link"><?php echo $alumno->getAlumnos()->getSocio()->getApepat().' '.$alumno->getAlumnos()->getSocio()->getApemat().' '.$alumno->getAlumnos()->getSocio()->getNombre() ?>
					</a></td>
					<td><?php echo $alumno->getAlumnos()->getSocio()->getEdad()?></td>
					<td><?php echo $alumno->getAlumnos()->getSocio()->getSexo()?></td>
					<td><?php echo $alumno->getAlumnos()->getSocio()->getEmail()?></td>
					<?php if( ($lista['adeudos'][$alumno->getAlumnoId()]['tiene_adeudo']) && (!$lista['adeudos'][$alumno->getAlumnoId()]['tiene_acuerdo']) ): ?>
					<td>Tiene adeudo de pagos <script type="text/javascript">
                $('#bitacora-<?php echo $alumno->getAlumnoId()?> td').addClass('adeudo');
              </script>
					</td>
					<?php elseif( ($lista['adeudos'][$alumno->getAlumnoId()]['tiene_adeudo']) && ($lista['adeudos'][$alumno->getAlumnoId()]['tiene_acuerdo']) ): ?>
					<td>Tiene un acuerdo de pagos <script type="text/javascript">
                $('#bitacorq-<?php echo $alumno->getAlumnoId()?> td').addClass('acuerdo');
              </script>
					</td>
					<?php else: ?>
					<td>No tiene adeudo de pagos</td>

					<?php endif; ?>
				</tr>
				<?php endforeach; ?>
				<?php endif; ?>
			</tbody>
		</table>

	</form>
	<?php else: ?>
	<div class="ui-state-error no-lista">No hay bitacoras para mostrar</div>
	<?php endif; ?>
</div>
