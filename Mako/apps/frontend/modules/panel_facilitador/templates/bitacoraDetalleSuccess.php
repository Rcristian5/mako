<?php use_stylesheet('control_escolar/control_escolar.css') ?>
<?php use_stylesheet('jquery.windows.css') ?>
<?php use_javascript('jquery.ui.js') ?>
<?php use_javascript('plugins/jquery.windows.js') ?>
<?php use_javascript('control_escolar/panel_facilitador.js') ?>


<div id="windows-wrapper">
	<div id="registro" window-title="Panel de control" window-state="min">

		<?php include_partial('panel', array('grupos' => $grupos ,'titulo'=>'Bitácora de socio')) ?>

	</div>

	<div id="cursos" window-title="Registro de asistencia"
		window-state="max">
		<h2>Bit&aacute;cora de socio</h2>
		<?php if ($sf_user->hasFlash('notice')): ?>
		<div class="flash_notice ui-state-highlight ui-corner-all">
			<span style="float: left; margin-right: 0.3em;"
				class="ui-icon ui-icon-info"></span> <strong><?php echo $sf_user->getFlash('notice') ?>
			</strong>
		</div>
		<?php endif; ?>
		<?php include_partial('bitacoraDetalle', array('form'=> $form, 'bitacora' => $bitacora, 'alumno'=>$alumno, 'grupo'=>$grupo)) ?>

	</div>

</div>

