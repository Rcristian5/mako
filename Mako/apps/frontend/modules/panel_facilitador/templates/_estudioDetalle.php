<?php use_stylesheet('control_escolar/registro_asistencia.css') ?>
<?php use_stylesheet('control_escolar/estudio_cualitativo.css') ?>
<?php use_javascript('control_escolar/bitacora_detalle.js') ?>

<div id="formulario" class="ui-widget-content ui-corner-all">
	<h3>
		<?php echo $alumno->getNombreCompleto() ?>
	</h3>

	<p class="group">
		Grupo:
		<?php echo $grupo->getClave() ?>
	</p>
	<p>Tomando en cuenta el desempeño del alumno a lo largo de curso,
		conteste las siguientes preguntas</p>
	<form action="<?php echo url_for('/panel_facilitador/createEstudio')?>"
		method="post" id="form-estudio">
		<table width="100%">
			<tbody>
				<?php foreach ($cuestionario as $pregunta):?>
				<?php
				$w = new sfWidgetFormChoice(array(
						'expanded' => true,
						'choices'  => $respuestas,
				));
				?>
				<tr>
					<th><?php echo $pregunta->getPregunta() ?> <span
						class="ui-widget-content ui-icon ui-icon-help help"
						q-title="<?php echo $pregunta->getDescripcion() ?>"></span></th>
				</tr>
				<tr>
					<td><?php if(isset($estudio[$pregunta->getId()])):?> <?php echo $w->render('pregunta['.$pregunta->getId().']',$estudio[$pregunta->getId()]['respuesta']);?>
						<?php else: ?> <?php echo $w->render('pregunta['.$pregunta->getId().']',$selected);?>
						<?php endif;?>
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
		<p class="botones_finales">
			<input type="button" value="Cancelar"
				onclick="location.href='/panel_facilitador/index?grupo=<?php echo $sf_user->getAttribute('grupo_estudio') ?>&tab=3'">
			<input type="submit" value="Guardar" />
		</p>
	</form>
</div>
