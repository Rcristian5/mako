<?php use_stylesheet('control_escolar/registro_asistencia.css') ?>
<?php use_stylesheet('datatables.css') ?>
<?php use_stylesheet('TableTools.css') ?>
<?php use_javascript('plugins/jquery.dataTables.js') ?>
<?php use_javascript('plugins/dataTables/TableTools.js') ?>
<?php use_javascript('plugins/dataTables/ZeroClipboard.js') ?>
<?php use_javascript('control_escolar/lista_asistencia.js') ?>
<?php $meses = array(1=>'Enero', 2=>'Febrero', 3=>'Marzo', 4=>'Abril', 5=>'Mayo', 6=>'Junio',
                     7=>'Julio', 8=>'Agosto', 9=>'Septiembre', 10=>'Ocubre', 11=>'Noviembre', 12=>'Diciembre') ?>
<script type="text/javascript">
  oTable =  $('#table-asistencia').dataTable({
                    "sDom": '<"H"lfr>t<"F"ip>T<"clear">',
		"bPaginate": false,
		"bLengthChange": true,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
                    "bJQueryUI": true,
                    "aoColumns" : [
                                    null,
                                    {"bVisible":    false},
                                    {"bVisible":    false},
                                    {"bVisible":    false},
                                    {"bVisible":    false},
                                    <?php foreach ($lista['dias'] as $dia): ?>

                                    {"bSortable":    false},
                                    <?php endforeach; ?>

                               ],
                    "aaSorting": [[0, 'asc']]
                });
</script>

<div id="lista_asistencia" class="ui-widget-content ui-corner-all">

	<?php if(count($lista['dias'])>0): ?>
	<?php $sf_user->setAttribute('grupo_asistencia', $lista['horario']['grupo_id']) ?>
	<h3>
		Grupo:
		<?php echo $lista['horario']['clave']?>
	</h3>
	<p class="datos">
		<span>Curso:</span>
		<?php echo $lista['horario']['nombre']?>
	</p>
	<p class="datos">
		<span>Clave:</span>
		<?php echo $lista['horario']['clave_curso']?>
	</p>
	<p class="datos">
		<span>Aula:</span>
		<?php echo $lista['horario']['aula']?>

	</p>
	<form id="form">
		<!--Lista de asistencia -->
		<table width="100%" border="0" class="display" id="table-asistencia">
			<thead>
				<tr>
					<th rowspan="2">Nombre</th>
					<th rowspan="2">Edad</th>
					<th rowspan="2">Sexo</th>
					<th rowspan="2">Correo Electr&oacute;nico</th>
					<th rowspan="2">Adeudo de Pagos</th>
					<?php $mes_actual = $lista['dias'][0]->getDia('n'); ?>
					<?php $ano_actual = $lista['dias'][0]->getDia('Y'); ?>
					<?php $colspan = 0; ?>
					<?php $lenght = count($lista['dias']); ?>
					<?php $flag = 0; ?>
					<?php foreach ($lista['dias'] as $mes): ?>
					<?php if($mes->getDia('n')==$mes_actual): ?>
					<?php $colspan++ ?>
					<?php else: ?>
					<th colspan="<?php echo $colspan; ?>"
						class="ui-widget-header table-mes"><?php echo $meses[$mes_actual].' de '.$ano_actual; ?>
					</th>
					<?php $mes_actual = $mes->getDia('n'); ?>
					<?php $ano_actual = $mes->getDia('Y'); ?>
					<?php $colspan = 1; ?>
					<?php endif; ?>
					<?php $flag++; ?>
					<?php if($flag == $lenght): ?>
					<?php if($colspan==1): ?>
					<?php $colspan = 2; ?>
					<?php endif; ?>
					<th colspan="<?php echo $colspan; ?>"
						class="ui-widget-header table-mes"><?php echo $meses[$mes_actual].' de '.$ano_actual; ?>
					</th>
					<?php endif; ?>
					<?php endforeach; ?>
				</tr>
				<tr>
					<?php foreach ($lista['dias'] as $dia): ?>
					<th class="centrado"><?php echo $dia->getDia('d'); ?></th>
					<?php endforeach; ?>
				</tr>
			</thead>
			<tbody>
				<?php if(count($lista['alumnos'])>0): ?>
				<?php foreach ($lista['alumnos'] as $alumno): ?>
				<tr>
					<td><?php echo $alumno->getAlumnos()->getSocio()->getApepat().' '.$alumno->getAlumnos()->getSocio()->getApemat().' '.$alumno->getAlumnos()->getSocio()->getNombre() ?>
					</td>
					<td><?php echo $alumno->getAlumnos()->getSocio()->getEdad()?></td>
					<td><?php echo $alumno->getAlumnos()->getSocio()->getSexo()?></td>
					<td><?php echo $alumno->getAlumnos()->getSocio()->getEmail()?></td>
					<td><?php echo ''?></td>
					<?php foreach ($lista['dias'] as $dia): ?>
					<td class="centrado"><?php if($lista['asistencia'][$alumno->getAlumnoId()][$dia->getId()]['asistencia']):?>
						<?php $checked = 'checked="checked"' ?> <?php else: ?> <?php $checked = '' ?>
						<?php endif; ?> <input type="checkbox"
						name="dia_<?php echo $lista['asistencia'][$alumno->getAlumnoId()][$dia->getId()]['id'] ?>"
						value="<?php echo $lista['asistencia'][$alumno->getAlumnoId()][$dia->getId()]['id'] ?>"
						<?php echo $checked ?>>
					</td>
					<?php endforeach; ?>
				</tr>
				<?php endforeach; ?>
				<?php endif; ?>
			</tbody>
		</table>
		<div style="text-align: right;" class="botones_finales">
			<input type="submit" value="Guardar lista de asistencia" />
		</div>
	</form>
	<?php else: ?>
	<div class="ui-state-error no-lista">No hay listas de asistencia para
		mostrar</div>
	<?php endif; ?>
</div>
