
<?php $meses = array(1=>'Enero', 2=>'Febrero', 3=>'Marzo', 4=>'Abril', 5=>'Mayo', 6=>'Junio',
                     7=>'Julio', 8=>'Agosto', 9=>'Septiembre', 10=>'Ocubre', 11=>'Noviembre', 12=>'Diciembre') ?>
<?php $esconder = array();?>
<?php $ver = array();?>
<?php $c = 5;?>
<?php use_stylesheet('./panel_facilitador/asistenciaSuccess.css')?>
<script type="text/javascript">
  aTable =  $('#table-asistencia').dataTable({
                    "sDom": '<"H"lfr>t<"F"ip>T<"clear">',
		"bPaginate": false,
		"bLengthChange": true,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
                    "bJQueryUI": true,
                    "aoColumns" : [
                                    null,
                                    {"bVisible":    false},
                                    {"bVisible":    false},
                                    {"bVisible":    false},
                                    {"bVisible":    false},
                                    <?php foreach ($lista['dias'] as $dia): ?>
                                     {"bSortable":    false},
                                    <?php endforeach; ?>

                               ],
                    "aaSorting": [[0, 'asc']]
                });
$(function() {

  $('#form').submit( function() {
                var obj={};
                $('input:checkbox').each(function(){
                  if(!$(this).is(":checked"))
                   {
                     obj['dis_'+$(this).attr('value')]=$(this).attr('value');
                   }
                })
              var sData = $('input', aTable.fnGetNodes()).serialize();
              if(sData=='')
              {
                alert('Marque los días a los que se han presentado los alumnos');
              }
              else
              {
                location.href='/panel_facilitador/guardarAsistencia?'+sData+'&'+$.param(obj);
              }
              return false;
    } );
});


<?php if($lista['horario']['grupo_id']!=""): ?>
  $('.grupo-<?php echo $lista['horario']['grupo_id']?>').removeClass('ui-state-default');
<?php endif; ?>
</script>
<div id="lista_asistencia" class="ui-widget-content ui-corner-all">
	<?php $sf_user->setAttribute('grupo_asistencia', $lista['horario']['grupo_id']) ?>
	<h3>
		Grupo:
		<?php echo $lista['horario']['clave']?>
	</h3>
	<p class="datos">
		<span>Curso:</span>
		<?php echo $lista['horario']['nombre']?>
	</p>
	<p class="datos">
		<span>Clave:</span>
		<?php echo $lista['horario']['clave_curso']?>
	</p>
	<p class="datos">
		<span>Aula:</span>
		<?php echo $lista['horario']['aula']?>
	</p>
	<?php if(count($lista['alumnos'])>0): ?>
	<form id="form">
		<!--Lista de asistencia -->
		<table width="100%" border="0" class="display" id="table-asistencia">
			<thead>
				<tr>
					<th rowspan="2">Nombre</th>
					<th rowspan="2">Edad</th>
					<th rowspan="2">Sexo</th>
					<th rowspan="2">Correo Electr&oacute;nico</th>
					<th rowspan="2">Adeudo de Pagos</th>
					<?php $mes_actual = $lista['dias'][0]->getDia('n'); ?>
					<?php $ano_actual = $lista['dias'][0]->getDia('Y'); ?>
					<?php $colspan = 0; ?>
					<?php $lenght = count($lista['dias']); ?>
					<?php $flag = 0; ?>
					<?php foreach ($lista['dias'] as $mes): ?>
					<?php if($mes->getDia('n')==$mes_actual): ?>
					<?php if($mes->getDia('Y-m') == date('Y-m')): ?>
					<?php $ok = true ?>
					<?php else: ?>
					<?php $ok = false ?>
					<?php endif; ?>
					<?php $colspan++ ?>
					<?php else: ?>
					<?php if($ok): ?>
					<th colspan="<?php echo $colspan; ?>"
						class="ui-widget-header table-mes"><?php echo $meses[$mes_actual].' de '.$ano_actual; ?>
						<? echo $mes->getDia('Y-m') ?>
					</th>
					<?php else: ?>
					<th colspan="<?php echo $colspan; ?>"
						class="ui-widget-header table-mes no-visibles"><?php echo $meses[$mes_actual].' de '.$ano_actual; ?>
					</th>
					<?php endif; ?>
					<?php $mes_actual = $mes->getDia('n'); ?>
					<?php $ano_actual = $mes->getDia('Y'); ?>
					<?php $colspan = 1; ?>
					<?php endif; ?>
					<?php $flag++; ?>
					<?php if($flag == $lenght): ?>
					<?php if($colspan==1): ?>
					<?php $colspan = 2; ?>
					<?php endif; ?>
					<?php if($ok): ?>
					<th colspan="<?php echo $colspan; ?>"
						class="ui-widget-header table-mes"><?php echo $meses[$mes_actual].' de '.$ano_actual; ?>
					</th>
					<?php else: ?>
					<th colspan="<?php echo $colspan; ?>"
						class="ui-widget-header table-mes no-visibles"><?php echo $meses[$mes_actual].' de '.$ano_actual; ?>
					</th>
					<?php endif; ?>
					<?php endif; ?>
					<?php endforeach; ?>
				</tr>
				<tr>
					<?php foreach ($lista['dias'] as $dia): ?>
					<?php if($dia->getDia('Y-m') == date('Y-m')): ?>
					<th class="centrado"><?php echo $dia->getDia('d'); ?></th>
					<?php else: ?>
					<th class="centrado no-visibles"><?php echo $dia->getDia('d'); ?></th>
					<?php endif; ?>
					<?php endforeach; ?>
				</tr>
			</thead>
			<tbody>
				<?php if(count($lista['alumnos'])>0): ?>
				<?php foreach ($lista['alumnos'] as $alumno): ?>
				<tr id="asistencia-<?php echo $alumno->getAlumnoId()?>">
					<td><?php echo $alumno->getAlumnos()->getSocio()->getApepat().' '.$alumno->getAlumnos()->getSocio()->getApemat().' '.$alumno->getAlumnos()->getSocio()->getNombre() ?>
					</td>
					<td><?php echo $alumno->getAlumnos()->getSocio()->getEdad()?></td>
					<td><?php echo $alumno->getAlumnos()->getSocio()->getSexo()?></td>
					<td><?php echo $alumno->getAlumnos()->getSocio()->getEmail()?></td>
					<?php if( ($lista['adeudos'][$alumno->getAlumnoId()]['tiene_adeudo']) && (!$lista['adeudos'][$alumno->getAlumnoId()]['tiene_acuerdo']) ): ?>
					<td>Tiene adeudo de pagos <script type="text/javascript">
                $('#asistencia-<?php echo $alumno->getAlumnoId()?> td').addClass('adeudo');
              </script>
					</td>
					<?php elseif( ($lista['adeudos'][$alumno->getAlumnoId()]['tiene_adeudo']) && ($lista['adeudos'][$alumno->getAlumnoId()]['tiene_acuerdo']) ): ?>
					<td>Tiene un acuerdo de pagos <script type="text/javascript">
                $('#asistencia-<?php echo $alumno->getAlumnoId()?> td').addClass('acuerdo');
              </script>
					</td>
					<?php else: ?>
					<td>No tiene adeudo de pagos</td>

					<?php endif; ?>
					<?php foreach ($lista['dias'] as $dia): ?>
					<?php if($dia->getDia('Y-m') == date('Y-m')): ?>
					<td class="centrado"><?php if($lista['asistencia'][$alumno->getAlumnoId()][$dia->getId()]['asistencia']):?>
						<?php $checked = 'checked="checked"' ?> <?php else: ?> <?php $checked = '' ?>
						<?php endif; ?> <input type="checkbox"
						name="dia_<?php echo $lista['asistencia'][$alumno->getAlumnoId()][$dia->getId()]['id'] ?>"
						value="<?php echo $lista['asistencia'][$alumno->getAlumnoId()][$dia->getId()]['id'] ?>"
						<?php echo $checked ?>>
					</td>
					<?php else: ?>
					<td class="centrado  no-visibles"><?php if($lista['asistencia'][$alumno->getAlumnoId()][$dia->getId()]['asistencia']):?>
						<?php $checked = 'checked="checked"' ?> <?php else: ?> <?php $checked = '' ?>
						<?php endif; ?> <input type="checkbox"
						name="dia_<?php echo $lista['asistencia'][$alumno->getAlumnoId()][$dia->getId()]['id'] ?>"
						value="<?php echo $lista['asistencia'][$alumno->getAlumnoId()][$dia->getId()]['id'] ?>"
						<?php echo $checked ?>>
					</td>
					<?php endif; ?>
					<?php endforeach; ?>
				</tr>
				<?php endforeach; ?>
				<?php endif; ?>
			</tbody>
		</table>
		<div style="text-align: right;" class="botones_finales">
			<input type="submit" value="Guardar lista de asistencia" />
		</div>
	</form>
	<?php else: ?>
	<div class="ui-state-error no-lista">No hay listas de asistencia para
		mostrar</div>
	<?php endif; ?>
</div>
