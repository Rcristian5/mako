
<script type="text/javascript">
  eTable =  $('#table-evaluaciones').dataTable({
                    "sDom": '<"H"lfr>t<"F"ip>T<"clear">',
		"bPaginate": false,
		"bLengthChange": true,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
                    "bJQueryUI": true,
                    "aoColumns" : [
                                    null,
                                    {"bVisible":    false},
                                    {"bVisible":    false},
                                    {"bVisible":    false},
                                    {"bVisible":    false},
                                    <?php foreach ($lista['evaluaciones'] as $evaluacion): ?>
                                    null,
                                    <?php endforeach; ?>
                               ],
                    "aaSorting": [[0, 'asc']]
                });
$(function() {

  $('#form-evaluacion').submit( function() {
              var sData = $('input:text', eTable.fnGetNodes()).serialize();
              if(sData=='')
              {
                alert('Ingrese al menos una calificación');
              }
              else
              {
                location.href='/panel_facilitador/guardarEvaluaciones?'+sData;
              }
              return false;
    } );

});
<?php if($lista['horario']['grupo_id']!=""): ?>
  $('.grupo-<?php echo $lista['horario']['grupo_id']?>').removeClass('ui-state-default');
<?php endif; ?>
</script>

<div id="lista_asistencia" class="ui-widget-content ui-corner-all">

	<?php $sf_user->setAttribute('grupo_evaluacion', $lista['horario']['grupo_id']) ?>
	<h3>
		Grupo:
		<?php echo $lista['horario']['clave']?>
	</h3>
	<p class="datos">
		<span>Curso:</span>
		<?php echo $lista['horario']['nombre']?>
	</p>
	<p class="datos">
		<span>Clave:</span>
		<?php echo $lista['horario']['clave_curso']?>
	</p>
	<p class="datos">
		<span>Aula:</span>
		<?php echo $lista['horario']['aula']?>
	</p>
	<?php if(count($lista['evaluaciones'])>0): ?>
	<form id="form-evaluacion">
		<!--Lista de asistencia -->
		<table width="100%" border="0" class="display" id="table-evaluaciones">
			<thead>
				<tr>
					<th>Nombre</th>
					<th>Edad</th>
					<th>Sexo</th>
					<th>Correo Electr&oacute;nico</th>
					<th>Adeudo de Pagos</th>
					<?php foreach ($lista['evaluaciones'] as $evaluacion): ?>
					<th class="centrado">Numero <?php echo $evaluacion->getNumero(); ?>:
						<?php echo $evaluacion->getTipoEvaluacion()->getNombre(); ?> <br />
						"<?php echo $evaluacion->getDescripcion(); ?>"
					</th>
					<?php endforeach; ?>
				</tr>
				<tr>

				</tr>
			</thead>
			<tbody>
				<?php if(count($lista['alumnos'])>0): ?>
				<?php foreach ($lista['alumnos'] as $alumno): ?>
				<tr id="evaluacion-<?php echo $alumno->getAlumnoId()?>">
					<td><?php echo $alumno->getAlumnos()->getSocio()->getApepat().' '.$alumno->getAlumnos()->getSocio()->getApemat().' '.$alumno->getAlumnos()->getSocio()->getNombre() ?>
					</td>
					<td><?php echo $alumno->getAlumnos()->getSocio()->getEdad()?></td>
					<td><?php echo $alumno->getAlumnos()->getSocio()->getSexo()?></td>
					<td><?php echo $alumno->getAlumnos()->getSocio()->getEmail()?></td>
					<?php if( ($lista['adeudos'][$alumno->getAlumnoId()]['tiene_adeudo']) && (!$lista['adeudos'][$alumno->getAlumnoId()]['tiene_acuerdo']) ): ?>
					<td>Tiene adeudo de pagos <script type="text/javascript">
                $('#evaluacion-<?php echo $alumno->getAlumnoId()?> td').addClass('adeudo');
              </script>
					</td>
					<?php elseif( ($lista['adeudos'][$alumno->getAlumnoId()]['tiene_adeudo']) && ($lista['adeudos'][$alumno->getAlumnoId()]['tiene_acuerdo']) ): ?>
					<td>Tiene un acuerdo de pagos <script type="text/javascript">
                $('#evaluacion-<?php echo $alumno->getAlumnoId()?> td').addClass('acuerdo');
              </script>
					</td>
					<?php else: ?>
					<td>No tiene adeudo de pagos</td>

					<?php endif; ?>
					<?php foreach ($lista['evaluaciones'] as $evaluacion): ?>
					<td class="centrado"><?php if(isset($lista['calificaciones'][$alumno->getAlumnoId()][$evaluacion->getId()])):?>
						<input class="calificacion" type="text"
						name="si_<?php echo $lista['calificaciones'][$alumno->getAlumnoId()][$evaluacion->getId()]['id'] ?>"
						value="<?php echo  $lista['calificaciones'][$alumno->getAlumnoId()][$evaluacion->getId()]['calificacion']?>"
						size="2" maxlength="4" filtro="numerico" /> <?php else: ?> <input
						class="calificacion" type="text"
						name="no_<?php echo $evaluacion->getId()?>_<?php echo $alumno->getAlumnoId()?>"
						size="2" maxlength="4" filtro="numerico" /> <?php endif; ?>
					</td>
					<?php endforeach; ?>
				</tr>
				<?php endforeach; ?>
				<?php endif; ?>
			</tbody>
		</table>
		<div style="text-align: right;" class="botones_finales">
			<input type="submit" value="Guardar lista de evaluaciones" />
		</div>
	</form>
	<?php else: ?>
	<div class="ui-state-error no-lista">No hay registro de evaluaciones
		para mostrar</div>
	<?php endif; ?>
</div>
