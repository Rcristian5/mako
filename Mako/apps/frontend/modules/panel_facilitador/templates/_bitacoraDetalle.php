<?php use_stylesheet('control_escolar/registro_asistencia.css') ?>
<?php use_javascript('control_escolar/bitacora_detalle.js') ?>
<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<div id="formulario" class="ui-widget-content ui-corner-all">
	<h3>Nueva anotaci&oacute;n</h3>
	<p class="user">
		<?php echo $alumno->getNombreCompleto() ?>
	</p>
	<p class="group">
		Grupo:
		<?php echo $grupo->getClave() ?>
	</p>
	<form action="<?php echo url_for('panel_facilitador/createBitacora')?>"
		method="post">
		<table>
			<tbody>
				<?php echo $form->renderHiddenFields(false) ?>
				<?php foreach ($form->getGlobalErrors() as $nombre => $error): ?>
				<li><?php echo $nombre.': '.$error ?></li>
				<?php endforeach; ?>

				<tr>
					<th><?php echo $form['comentario']->renderLabel('Anotación') ?></th>
					<td><?php echo $form['comentario']->renderError() ?> <?php echo $form['comentario']->render(array('requerido'=>1, 'filtro'=>'alfanumerico', 'cols'=>35, 'rows'=>5)) ?>
					</td>
				</tr>
			</tbody>
		</table>
		<p class="botones_finales">
			<input type="button" value="Cancelar"
				onclick="location.href='/panel_facilitador/index?grupo=<?php echo $sf_user->getAttribute('grupo_bitacora') ?>&tab=2'">
			<input type="submit" value="Guardar"
				onclick="return comparaRequeridos();" />
		</p>
	</form>
</div>
<div id="detalles" class="ui-widget-content ui-corner-all">
	<h3>Anotaciones</h3>
	<?php if(count($bitacora)>0):?>
	<div id="accordion-anotaciones">
		<?php foreach ($bitacora as $anotacion):?>
		<h3>
			<a href="#">Comentario <?php echo $anotacion->getCreatedAt('Y-m-d')?>
				en curso <?php echo $anotacion->getGrupo()->getCurso()->getNombre()?>
			</a>
		</h3>
		<div>
			<?php echo $anotacion->getComentario()?>
		</div>
		<?php endforeach ?>
	</div>
	<?php else: ?>
	<div class="ui-state-error no-lista">No hay anotaciones para mostrar</div>
	<?php endif; ?>
</div>
