<?php

/**
 * panel_facilitador actions.
 *
 * @package    mako
 * @subpackage panel_facilitador
 * @author     Bixit SA de CV
 * @version    SVN: $Id: actions.class.php,v 1.10 2010/05/04 19:43:32 david Exp $
 */
class panel_facilitadorActions extends sfActions
{
	/**
	 * Executes index action
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeIndex(sfWebRequest $request)
	{
		$grupos = null;
		$grupos = GrupoFacilitadorPeer::listaGrupos($this->getUser()->getAttribute('usuario')->getId());
		$this->grupos = $grupos;
		$grupo = $request->getParameter('grupo');
		$tab = $request->getParameter('tab');
		if(isset ($grupo))
		{
			$this->grupo = $grupo;
		}
		else
		{
			$lista = GrupoPeer::listaAsistenciaDefault($this->getUser()->getAttribute('usuario')->getId());
			$this->grupo = $lista['horario']['grupo_id'];
		}
		if(isset ($tab))
		{
			$this->tab = $tab;
		}
		else
		{

			$this->tab = 0;
		}
	}
	public function executeAsistencia(sfWebRequest $request)
	{
		$grupo = $request->getParameter('grupo');
		$this->lista = GrupoPeer::listaAsistencia($this->getUser()->getAttribute('usuario')->getId(),$grupo);
	}
	public function executeEvaluaciones(sfWebRequest $request)
	{
		$grupo = $request->getParameter('grupo');
		$this->lista = GrupoPeer::listaEvaluaciones($this->getUser()->getAttribute('usuario')->getId(),$grupo);
	}

	public function executeBitacora(sfWebRequest $request)
	{
		$grupo = $request->getParameter('grupo');
		$this->lista = GrupoPeer::listaBitacora($this->getUser()->getAttribute('usuario')->getId(),$grupo);
	}

	public function executeEstudiocualitativo(sfWebRequest $request)
	{
		$grupo = $request->getParameter('grupo');
		$this->lista = GrupoPeer::listaBitacora($this->getUser()->getAttribute('usuario')->getId(),$grupo);
	}

	public function executeBitacoraDetalle(sfWebRequest $request)
	{
		$grupos = GrupoFacilitadorPeer::listaGrupos($this->getUser()->getAttribute('usuario')->getId());
		$this->getUser()->setAttribute('bitacora_usuario', $request->getParameter('id'));
		$this->grupos = $grupos;
		$this->bitacora = GrupoPeer::obtenBitacora($request->getParameter('id'), $this->getUser()->getAttribute('grupo_bitacora'));
		$this->form = new BitacoraForm();
		$this->alumno = SocioPeer::retrieveByPK($request->getParameter('id'));
		$this->grupo = GrupoPeer::retrieveByPK($this->getUser()->getAttribute('grupo_bitacora'));
	}

	public function executeEstudioDetalle(sfWebRequest $request)
	{
		$grupos = GrupoFacilitadorPeer::listaGrupos($this->getUser()->getAttribute('usuario')->getId());
		$this->getUser()->setAttribute('bitacora_usuario', $request->getParameter('id'));
		$this->grupos = $grupos;
		$this->cuestionario = GrupoPeer::obtenPreguntas($this->getUser()->getAttribute('grupo_estudio'));
		$respuestas = GrupoPeer::obtenRespuestas($this->getUser()->getAttribute('grupo_estudio'));
		$f=0;
		foreach ($respuestas as $r=>$re)
		{
			if($f==2)
			{
				$select = $r;
			}
			$f++;
		}
		$this->estudio = GrupoPeer::obtenEstudio( $request->getParameter('id'), $this->getUser()->getAttribute('grupo_estudio'));
		$this->respuestas =  $respuestas;
		$this->selected = $select;
		$this->alumno = SocioPeer::retrieveByPK($request->getParameter('id'));
		$this->grupo = GrupoPeer::retrieveByPK($this->getUser()->getAttribute('grupo_estudio'));
	}

	public function executeCreateBitacora(sfWebRequest $request)
	{
		$this->forward404Unless($request->isMethod(sfRequest::POST));

		$grupos = GrupoFacilitadorPeer::listaGrupos($this->getUser()->getAttribute('usuario')->getId());
		$this->grupos = $grupos;
		$this->bitacora = GrupoPeer::obtenBitacora($this->getUser()->getAttribute('bitacora_usuario'), $this->getUser()->getAttribute('grupo_bitacora'));
		$this->form = new BitacoraForm();
		$this->alumno = SocioPeer::retrieveByPK($this->getUser()->getAttribute('bitacora_usuario'));
		$this->grupo = GrupoPeer::retrieveByPK($this->getUser()->getAttribute('grupo_bitacora'));
		$this->processFormBitacora($request, $this->form);

		$this->setTemplate('bitacoraDetalle');
	}

	public function executeCreateEstudio(sfWebRequest $request)
	{
		EstudioCualitativoPeer::guardaEstudio($this->getUser()->getAttribute('bitacora_usuario'), $this->getUser()->getAttribute('grupo_bitacora'), $request->getParameter('pregunta'), $this->getUser()->getAttribute('usuario')->getId());
		$this->getUser()->setFlash('notice', 'Estudio guardado');
		$this->redirect('panel_facilitador/estudioDetalle?id='.$this->getUser()->getAttribute('bitacora_usuario'));
	}

	protected function processFormBitacora(sfWebRequest $request, sfForm $form)
	{
		$form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
		if ($form->isValid())
		{
			$grupo = GrupoPeer::retrieveByPK($this->getUser()->getAttribute('grupo_bitacora'));
			$bitacora = new Bitacora();
			$bitacora->setOperadorId($this->getUser()->getAttribute('usuario')->getId());
			$bitacora->setAlumnoId($this->getUser()->getAttribute('bitacora_usuario'));
			$bitacora->setCursoId($grupo->getCursoId());
			$bitacora->setGrupoId($grupo->getId());
			$bitacora->setComentario($form->getValue('comentario'));
			$bitacora->save();
			$this->getUser()->setFlash('notice', 'Anotación guardada');

			$this->redirect('panel_facilitador/bitacoraDetalle?id='.$this->getUser()->getAttribute('bitacora_usuario'));
		}
	}

	public function executeGuardarAsistencia(sfWebRequest $request)
	{
		foreach($request->getParameterHolder()->getNames() as $dia)
		{
			$nombre = substr($dia, 0, 3);
			if($nombre=='dia')
			{
				$presente = ListaAsistenciaPeer::retrieveByPK($request->getParameter($dia));
				$presente->setAsistencia(true);
				$presente->save();
			}
			if($nombre=='dis')
			{
				$ausente = ListaAsistenciaPeer::retrieveByPK($request->getParameter($dia));
				$ausente->setAsistencia(false);
				$ausente->save();
			}
		}
		$this->getUser()->setFlash('notice', 'Registro de asistencia guardado');
		$this->redirect('panel_facilitador/index?grupo='.$this->getUser()->getAttribute('grupo_asistencia').'&tab=0');
		 
	}
	public function executeGuardarEvaluaciones(sfWebRequest $request)
	{
		foreach($request->getParameterHolder()->getNames() as $evaluacion)
		{
			$nombre = substr($evaluacion, 0, 2);
			$valores = substr($evaluacion, 2);
			if($nombre=='si')
			{
				$val=explode("_",$valores);
				$calificacion = CalificacionesPeer::retrieveByPK($val[1]);
				$calificacion->setCalificacion($request->getParameter($evaluacion));
				$calificacion->setOperadorId($this->getUser()->getAttribute('usuario')->getId());
				$calificacion->save();
			}
			if($nombre=='no')
			{
				$val=explode("_",$valores);
				$grupo = GrupoPeer::retrieveByPK($this->getUser()->getAttribute('grupo_evaluacion'));
				$calificacion = new Calificaciones();
				$calificacion->setAlumnoId($val[2]);
				$calificacion->setCursoId($grupo->getCursoId());
				$calificacion->setEvaluacionId($val[1]);
				$calificacion->setGrupoId($grupo->getId());
				$calificacion->setOperadorId($this->getUser()->getAttribute('usuario')->getId());
				$calificacion->setCalificacion($request->getParameter($evaluacion));
				$calificacion->save();
			}
		}
		$this->getUser()->setFlash('notice', 'Registro de evaluaciones guardado');
		$this->redirect('panel_facilitador/index?grupo='.$this->getUser()->getAttribute('grupo_evaluacion').'&tab=1');

	}
}
