<?php

class webserviceActions extends sfActions
{

  public function preExecute()
  {
    $this->model = $this->getRequest()->getParameter('model');
    sfConfig::set('sf_escaping_strategy', false);

    if(!class_exists($this->model))
      $this->forward('webservice', 'error');
  }

  public function executeList(sfWebRequest $request)
  {
    
    $modelo = $this->model . "Peer";    
    $results = $this->Usuarios = $modelo::doSelect(new Criteria());   
    $objects = array();
    foreach ($results as $result)
    {
      $objects[] = $result->toArray();
    }
    
    $this->objects = $objects;
    
  }  

  public function executeCreate(sfWebRequest $request)
  {

    $formClass = $this->model . "Form";

    $form = new $formClass();

    $valores = $request->getParameter(($form->getName()), $request->getFiles($form->getName()));
        
    if ($this->model = 'horario') {

      error_log('----------CENTRO---------'.$valores['centro_id']);
      error_log('----------CURSO---------'.$valores['curso_id']);

      $this->VerificaHorario($valores['centro_id'], $valores['curso_id']);

    }

    $form = new $formClass(null, $request, false);

    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));

    if(!$form->isValid()){
    //return $this->renderFormError($form);
    return $form->renderGlobalErrors();

    }

    $this->object = $form->save();

    $this->setTemplate('find');


  }

  public function VerificaHorario($cent, $curs)
  {


    error_log('----------CENTRO Ver---------'.$cent);
    error_log('----------CURSO Ver---------'.$curs);

    $centro = CentroPeer::retrieveByPK($cent);    
    $horasInicio = array();
    $horasFin = array();


    for($i = $centro->getHoraApertura('G')+0; $i < $centro->getHoraCierre()+0; $i++)
    {

      if($i < 10)
      {
        $horasInicio[$i] = '0'.$i;
      }
      else
      {
        $horasInicio[$i] = $i;
      }
    }

    for($i = $centro->getHoraApertura('G')+1; $i < $centro->getHoraCierre()+1; $i++)
    {
      if($i < 10)
      {
        $horasFin[$i] = '0'.$i;
      }
      else
      {
        $horasFin[$i] = $i;
      }
    }

    $minutos = array(0=>'00');

    $horasInicioIni = array();
    $horasFinIni = array();

    $horasInicioIni = $horasInicio;
    $horasFinIni = $horasFin;



    /*
     * Defaults
    */
    //$this->setDefault('centro_id', $this->getOption('centro'));
    //$this->setDefault('curso_id', $this->getOption('curso'));
    //$this->setDefault('aula_id', $this->getOption('aula'));

    /*
     * Comprobamos que el curso no tenga restricciones de horario
    */
    $curso = CursoPeer::retrieveByPK($curs);
    if($curso->getRestriccionesHorario())
    {
      $restricciones = $curso->getRestriccionesHorarios();
      foreach ($restricciones as $restriccion)

      {
        /*
         * Si hay restricciones de horario quitamos las horas a las que no se debe dar un curso
        */
        if($restriccion->getHoraInicio())
        {
          foreach ($horasInicio as $key => $hora)
          {
            if($key < $restriccion->getHoraInicio('G'))
            {
              unset ($horasInicio[$key]);
            }
          }
          foreach ($horasFin as $key => $hora)
          {
            if($key <= $restriccion->getHoraInicio('G'))
            {
              unset ($horasFin[$key]);
            }
          }
        }
        if($restriccion->getHoraFin())
        {
          foreach ($horasFin as $key => $hora)
          {
            if($key > $restriccion->getHoraFin('G'))
            {
              unset ($horasFin[$key]);
            }
          }
          foreach ($horasInicio as $key => $hora)
          {
            if($key >= $restriccion->getHoraFin('G'))
            {
              unset ($horasInicio[$key]);
            }
          }
        }
      }
    }

    if ($horasInicio != $horasInicioIni){
      
      error_log('----------------NO SE REALIZO INSERCION DE HORARIO DEBIDO A RESTRICCIONES------------------');
      exit;

    }


  }


  public function executeGet(sfWebRequest $request)
  {

    $id = $request->getParameter('id');
    $modelo = $this->model . "Peer";
    
    $c = new Criteria();
    $c->add($modelo::ID,$id);
    $results = $modelo::doSelectOne($c);   

    $objects = array();
    
    if($results != null) { 

      $objects = $results->toArray();

    }

    $this->object = $objects;

    $this->setTemplate('find');


  }

  public function executeUpdate(sfWebRequest $request)
  {
        
    error_log('----------------INICIO UPDATE------------------');

    $id = $request->getParameter('id');
    $modelo = $this->model . "Peer";    
    $formClass = $this->model . "Form"; 
    error_log($formClass);
    //$request->checkCSRFProtection();
    //$form = new $formClass(null, array(), false); 
    //$nombrequery = $this->model . "Query";  

 /*
    $c = new Criteria();
    $c->add($modelo::ID,$id);
    $results = $modelo::doSelectOne($c); 

    $this->object = $results;

    $content = $request->getContent();
    $content = unserialize($content);

    $this->forward404Unless($this->object);

    //$this->object->fromArray(array($request->getPostParameter($form->getName()))); 
    $this->object->fromArray(array($content)); 
*/
/*
    $content = $request->getPostParameter($form->getName());    
     
    $objeto       = new $this->model();
    $indicador    = $objeto->getPeer()->retrieveByPK($id);    
    $indicador->fromArray($content);
    $indicador->updateObject();
    $indicador->save();
*/

     //$this->forward404Unless($request->isMethod('post')); 
    //$request->checkCSRFProtection();  
    //$this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
    $this->forward404Unless($Forma = $modelo::retrieveByPk($request->getParameter('id')), sprintf('Registro inexistente (%s).', $request->getParameter('id')));    
    //$form = new $formClass($Forma);
    //$form = new $formClass($Forma);
    
    $form = new $formClass($Forma);
    $form->getValidator($form->getCSRFFieldName())->setOption('required', false);
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));

    //error_log( print_r($datos);
    echo var_dump($request->getParameter($form->getName()), $request->getFiles($form->getName()));

    //$form->getValidator($form->getCSRFFieldName())->setOption('required', false);
    //$this->processForm($request, $form);
    //$form->getValidator($form->getCSRFFieldName())->setOption('required', false);

    //$form['id']->render();
    //$form->getValidator($form->getCSRFFieldName())->setOption('required', false);
   
    //$request->checkCSRFProtection();
    //error_log($form['_csrf_token']);
    //error_log($form['nombre']);

  
    //$form = new $formClass($modelo::retrieveByPk($request->getParameter('id')));

    error_log('----------------FORM UPDATE------------------');  



    if(!$form->isValid()){
    //return $this->renderFormError($form);
    return $form->renderGlobalErrors();

    }

    $this->object = $form->save();
    //$form->save(); 

    $this->setTemplate('find');
/*
    $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));

    $this->form = new $formClass($modelo::retrieveByPk($request->getParameter('id')));        

    $this->form->bind($request->getParameter($this->form->getName()), $request->getFiles($this->form->getName()));

$form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));          



    error_log('----------------MITAssssD UPDATE------------------'); 

    if ($this->form->isValid())
     {
      
        $this->form->save(); 
      
    }else{

      return $this->form->renderGlobalErrors();
    }

                        

    echo var_dump($request->getParameter($this->form->getName()), $request->getFiles($this->form->getName()));
    echo "----------------------------------";
    //echo var_dump($this->object);    
    //echo $data['my_token'];
    //echo var_dump($this->form);    
    exit;

    $this->object->save();
    
    $objects = array();
    
    if($this->object != null) { 

      $objects = $this->object->toArray();

    }    

    $this->object = $objects;

    $this->setTemplate('find'); 

     */ 

  }

  public function executeDelete(sfWebRequest $request)
  {

      $id = $request->getParameter('id');

      $modelo = $this->model . "Peer";
      $c = new Criteria();
      $c->add($modelo::ID,$id);
      $results = $modelo::doSelectOne($c); 

      //$this->object = Doctrine_Core::getTable($this->model)->findOneById($id);
      $this->object = $results;
      $this->forward404Unless($this->object);
      $this->object->delete();

      $this->setTemplate('find');    

  }

  public function executeError(sfWebRequest $request)
  {
    $model = $request->getParameter('model');

    $this->getResponse()->setStatusCode(500);

    $this->error = ($model != null) ? "Modelo Invalido: " . $model : "modelo indefinido";
  }



  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));    
    
    if(!$form->isValid()){
    
      $object = $form->save(); 
       

    }

  }

  

}