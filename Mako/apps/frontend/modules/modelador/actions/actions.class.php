<?php

/**
 * modelador actions.
 *
 * @package    mako
 * @subpackage modelador
 * @author     Bixit SA de CV
 * @version    SVN: $Id: actions.class.php,v 1.15 2010/09/12 07:07:53 david Exp $
 */
class modeladorActions extends sfActions
{
	public function executeIndex(sfWebRequest $request)
	{
		$this->detalleCursos = CursoPeer::listaCursosNoAsociados();
	}

	/*
	 * Accion para mostrar los temas
	*
	*/
	public function executeCurso(sfWebRequest $request)
	{
		$this->forward404Unless($curso = CursoPeer::retrieveByPk($request->getParameter('id')), sprintf('El curso no exite(%s).', $request->getParameter('id')));

		$this->curso = $curso;
		$this->detalleCursos = CursoPeer::listaCursosNoAsociados();
		$this->temas = $curso->getTemas();
		$this->evaluaciones = $curso->getEvaluacionParcials();
		$this->formTema= new TemaForm(null, array('curso'=>$curso->getId()));
		$this->formEvaluacion = new EvaluacionParcialForm(null, array('curso'=>$curso->getId()));
		$this->formFinal = new EvaluacionFinalForm( EvaluacionFinalPeer::getEvaluacionFinal($curso->getId()) , array('curso'=>$curso->getId()) );
		$mostrar = false;
		if($curso->getTemas())
		{
			$mostrar = true;
		}
		$this->mostrar = $mostrar;

		$this->setTemplate('index');
	}

	/**
	 * Accion para crear un tema
	 */
	public function executeCreateTema(sfWebRequest $request)
	{
		$this->forward404Unless($request->isMethod(sfRequest::POST));

		$this->forward404Unless($curso = CursoPeer::retrieveByPk($request->getParameter('curso')), sprintf('El tema no exite(%s).', $request->getParameter('id')));

		$this->formTema= new TemaForm(null, array('curso'=>$curso->getId()));

		$this->processFormTema($request, $this->formTema);

		$this->curso = $curso;
		$this->detalleCursos = CursoPeer::listaCursosNoAsociados();
		$this->temas = $curso->getTemas();
		$this->evaluaciones = $curso->getEvaluacionParcials();
		$this->formEvaluacion = new EvaluacionParcialForm(null, array('curso'=>$curso->getId()));
		$this->formFinal = new EvaluacionFinalForm( EvaluacionFinalPeer::getEvaluacionFinal($curso->getId()) , array('curso'=>$curso->getId()) );
		$this->mostrar = true;

		$this->setTemplate('index');
	}

	/**
	 * Accion para crear una evaluaciones
	 */
	public function executeCreateEvaluacion(sfWebRequest $request)
	{
		$this->forward404Unless($request->isMethod(sfRequest::POST));

		$this->forward404Unless($curso = CursoPeer::retrieveByPk($request->getParameter('curso')), sprintf('El tema no exite(%s).', $request->getParameter('id')));

		$this->formEvaluacion = new EvaluacionParcialForm(null, array('curso'=>$curso->getId()));

		$this->processFormEvaluacion($request, $this->formEvaluacion);

		$this->curso = $curso;
		$this->detalleCursos = CursoPeer::listaCursosNoAsociados();
		$this->temas = $curso->getTemas();
		$this->evaluaciones = $curso->getEvaluacionParcials();
		$this->formTema= new TemaForm(null, array('curso'=>$curso->getId()));
		$this->formFinal = new EvaluacionFinalForm( EvaluacionFinalPeer::getEvaluacionFinal($curso->getId()) , array('curso'=>$curso->getId()) );

		$this->mostrar = true;

		$this->setTemplate('index');
	}
	/**
	 * Accion para crear una evaluaciones finales
	 */
	public function executeCreateFinal(sfWebRequest $request)
	{
		$this->forward404Unless($request->isMethod(sfRequest::POST));

		$this->forward404Unless($curso = CursoPeer::retrieveByPk($request->getParameter('curso')), sprintf('El tema no exite(%s).', $request->getParameter('id')));

		$this->formFinal = new EvaluacionFinalForm( null , array('curso'=>$curso->getId()) );

		$this->processFormFinal($request, $this->formFinal);

		$this->curso = $curso;
		$this->detalleCursos = CursoPeer::listaCursosNoAsociados();
		$this->temas = $curso->getTemas();
		$this->evaluaciones = $curso->getEvaluacionParcials();
		$this->formTema= new TemaForm(null, array('curso'=>$curso->getId()));
		$this->formEvaluacion = new EvaluacionParcialForm(null, array('curso'=>$curso->getId()));

		$this->mostrar = true;

		$this->setTemplate('index');
	}
	 
	/*
	 * Función para editar un tema
	*/
	public function executeEditTema(sfWebRequest $request)
	{
		$this->forward404Unless($curso = CursoPeer::retrieveByPk($request->getParameter('curso')), sprintf('El curso no exite(%s).', $request->getParameter('curso')));
		$this->forward404Unless($tema = TemaPeer::retrieveByPk($request->getParameter('tema')), sprintf('El tema no exite(%s).', $request->getParameter('tema')));

		$this->curso = $curso;
		$this->detalleCursos = CursoPeer::listaCursosNoAsociados();
		$this->temas = $curso->getTemas();
		$this->evaluaciones = $curso->getEvaluacionParcials();
		$this->formTema= new TemaForm($tema, array('curso'=>$curso->getId()));
		$this->formEvaluacion = new EvaluacionParcialForm(null, array('curso'=>$curso->getId()));
		$this->formFinal = new EvaluacionFinalForm( EvaluacionFinalPeer::getEvaluacionFinal($curso->getId()) , array('curso'=>$curso->getId()) );
		$this->mostrar = true;

		$this->setTemplate('index');
	}

	/*
	 * Función para editar una evaluacion
	*/
	public function executeEditEvaluacion(sfWebRequest $request)
	{
		$this->forward404Unless($curso = CursoPeer::retrieveByPk($request->getParameter('curso')), sprintf('El curso no exite(%s).', $request->getParameter('curso')));
		$this->forward404Unless($evaluacion = EvaluacionParcialPeer::retrieveByPk($request->getParameter('evaluacion')), sprintf('La evalucion no exite(%s).', $request->getParameter('evalucion')));

		$this->curso = $curso;
		$this->detalleCursos = CursoPeer::listaCursosNoAsociados();
		$this->temas = $curso->getTemas();
		$this->evaluaciones = $curso->getEvaluacionParcials();
		$this->formTema= new TemaForm(null, array('curso'=>$curso->getId()));
		$this->formEvaluacion = new EvaluacionParcialForm($evaluacion, array('curso'=>$curso->getId()));
		$this->formFinal = new EvaluacionFinalForm( EvaluacionFinalPeer::getEvaluacionFinal($curso->getId()) , array('curso'=>$curso->getId()) );
		$this->mostrar = true;

		$this->setTemplate('index');
	}

	/*
	 * Función para  actualizar los datos de un tema
	*/
	public function executeUpdateTema(sfWebRequest $request)
	{
		$this->forward404Unless($curso = CursoPeer::retrieveByPk($request->getParameter('curso')), sprintf('El curso no exite(%s).', $request->getParameter('curso')));
		$this->forward404Unless($tema = TemaPeer::retrieveByPk($request->getParameter('id')), sprintf('El tema no exite(%s).', $request->getParameter('id')));

		$this->formTema= new TemaForm($tema, array('curso'=>$curso->getId()));

		$this->processFormTema($request, $this->formTema);

		$this->curso = $curso;
		$this->detalleCursos = CursoPeer::listaCursosNoAsociados();
		$this->temas = $curso->getTemas();
		$this->evaluaciones = $curso->getEvaluacionParcials();
		$this->formEvaluacion = new EvaluacionParcialForm(null, array('curso'=>$curso->getId()));
		$this->formFinal = new EvaluacionFinalForm( EvaluacionFinalPeer::getEvaluacionFinal($curso->getId()) , array('curso'=>$curso->getId()) );
		$this->mostrar = true;

		$this->setTemplate('index');
	}

	/**
	 * Función para  actualizar los datos de una evaluacion
	 */
	public function executeUpdateEvaluacion(sfWebRequest $request)
	{
		$this->forward404Unless($curso = CursoPeer::retrieveByPk($request->getParameter('curso')), sprintf('El curso no exite(%s).', $request->getParameter('curso')));
		$this->forward404Unless($evaluacion = EvaluacionParcialPeer::retrieveByPk($request->getParameter('id')), sprintf('La evalucion no exite(%s).', $request->getParameter('id')));

		$this->formEvaluacion = new EvaluacionParcialForm( $evaluacion , array('curso'=>$curso->getId()) );

		$this->processFormTema($request, $this->formEvaluacion);

		$this->curso = $curso;
		$this->detalleCursos = CursoPeer::listaCursosNoAsociados();
		$this->temas = $curso->getTemas();
		$this->evaluaciones = $curso->getEvaluacionParcials();
		$this->formTema = new TemaForm(null, array('curso'=>$curso->getId()));
		$this->formFinal = new EvaluacionFinalForm( EvaluacionFinalPeer::getEvaluacionFinal($curso->getId()) , array('curso'=>$curso->getId()) );

		$this->mostrar = true;

		$this->setTemplate('index');
	}

	/**
	 * Función para  actualizar los datos de una evaluacion
	 */
	public function executeUpdateFinal(sfWebRequest $request)
	{
		$this->forward404Unless($curso = CursoPeer::retrieveByPk($request->getParameter('curso')), sprintf('El curso no exite(%s).', $request->getParameter('curso')));
		$this->forward404Unless($evaluacion = EvaluacionFinalPeer::retrieveByPk($request->getParameter('id')), sprintf('La evalucion no exite(%s).', $request->getParameter('id')));

		$this->formFinal = new EvaluacionFinalForm( $evaluacion , array('curso'=>$curso->getId()) );

		$this->processFormTema($request, $this->formFinal);

		$this->curso = $curso;
		$this->detalleCursos = CursoPeer::listaCursosNoAsociados();
		$this->temas = $curso->getTemas();
		$this->evaluaciones = $curso->getEvaluacionParcials();
		$this->formTema = new TemaForm(null, array('curso'=>$curso->getId()));
		$this->formEvaluacion= new EvaluacionParcialForm(null, array('curso'=>$curso->getId()));

		$this->mostrar = true;

		$this->setTemplate('index');
	}

	/**
	 * Ejecuta la acción para desactivar un registro
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeEnabledTema(sfWebRequest $request)
	{
		$this->forward404Unless($curso = CursoPeer::retrieveByPk($request->getParameter('curso')), sprintf('El curso no existe (%s).', $request->getParameter('curso')));
		$this->forward404Unless($tema = TemaPeer::retrieveByPk($request->getParameter('tema')), sprintf('El tema no existe (%s).', $request->getParameter('tema')));
		$tema->setActivo(true);
		$tema->save();

		$this->getUser()->setFlash('notice', 'Tema "'.$tema->getNombre().'" activado');

		$this->redirect('modelador/curso?id='.$curso->getId());
	}

	/**
	 * Ejecuta la acción para desactivar un registro
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeEnabledEvaluacion(sfWebRequest $request)
	{
		$this->forward404Unless($curso = CursoPeer::retrieveByPk($request->getParameter('curso')), sprintf('El curso no existe (%s).', $request->getParameter('curso')));
		$this->forward404Unless($evaluacion = EvaluacionParcialPeer::retrieveByPk($request->getParameter('evaluacion')), sprintf('La evaluación no existe (%s).', $request->getParameter('evalucion')));
		$evaluacion->setActivo(true);
		$evaluacion->save();

		$this->getUser()->setFlash('notice', 'Evaluación "'.$evaluacion->getNombre().'" activada');

		$this->redirect('modelador/curso?id='.$curso->getId());
	}

	/**
	 * Ejecuta la acción para desactivar un registro
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeDisabledTema(sfWebRequest $request)
	{
		$this->forward404Unless($curso = CursoPeer::retrieveByPk($request->getParameter('curso')), sprintf('El curso no existe (%s).', $request->getParameter('curso')));
		$this->forward404Unless($tema = TemaPeer::retrieveByPk($request->getParameter('tema')), sprintf('El tema no existe (%s).', $request->getParameter('tema')));
		$tema->setActivo(false);
		$tema->save();

		$this->getUser()->setFlash('notice', 'Tema "'.$tema->getNombre().'" desactivado');

		$this->redirect('modelador/curso?id='.$curso->getId());
	}

	/**
	 * Ejecuta la acción para desactivar un registro
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeDisabledEvaluacion(sfWebRequest $request)
	{
		$this->forward404Unless($curso = CursoPeer::retrieveByPk($request->getParameter('curso')), sprintf('El curso no existe (%s).', $request->getParameter('curso')));
		$this->forward404Unless($evaluacion = EvaluacionParcialPeer::retrieveByPk($request->getParameter('evaluacion')), sprintf('La evaluación no existe (%s).', $request->getParameter('evaluacion')));
		$evaluacion->setActivo(false);
		$evaluacion->save();

		$this->getUser()->setFlash('notice', 'Evaluación "'.$evaluacion->getNombre().'" desactivada');

		$this->redirect('modelador/curso?id='.$curso->getId());
	}

	/*
	 * Función para  públicar un curso
	*/
	public function executePublicar(sfWebRequest $request)
	{
		$this->forward404Unless($curso = CursoPeer::retrieveByPk($request->getParameter('id')), sprintf('El curso no existe (%s).', $request->getParameter('id')));

		$flag = CursoPeer::publicaCurso($curso->getId());

		if(!$flag)
		{
			$this->dispatcher->notify(new sfEvent($this, 'curso.publicado', array( 'curso' => $curso )));
			$this->getUser()->setFlash('notice', 'Curso '.$curso->getNombre().' publicado');
		}
		else
		{
			$this->dispatcher->notify(new sfEvent($this, 'curso.modificado', array( 'curso' => $curso )));
			$this->getUser()->setFlash('notice', 'Curso '.$curso->getNombre().' editado');
		}


		$this->redirect('modelador/index');
	}

	/**
	 * Función para eliminar tema
	 */
	public function executeDeleteTema(sfWebRequest $request)
	{
		$request->checkCSRFProtection();

		$this->forward404Unless($curso = CursoPeer::retrieveByPk($request->getParameter('curso')), sprintf('El curso no existe (%s).', $request->getParameter('curso')));
		$this->forward404Unless($tema = TemaPeer::retrieveByPk($request->getParameter('tema')), sprintf('El tema no existe (%s).', $request->getParameter('tema')));

		$this->getUser()->setFlash('notice', 'Tema "'.$tema->getNombre().'" borrado');

		$tema->delete();

		$this->redirect('modelador/curso?id='.$curso->getId());
	}

	/**
	 * Función para eliminar evaluacion
	 */
	public function executeDeleteEvaluacion(sfWebRequest $request)
	{
		$request->checkCSRFProtection();

		$this->forward404Unless($curso = CursoPeer::retrieveByPk($request->getParameter('curso')), sprintf('El curso no existe (%s).', $request->getParameter('curso')));
		$this->forward404Unless($evalucion = EvaluacionParcialPeer::retrieveByPk($request->getParameter('evaluacion')), sprintf('La evaluación no existe (%s).', $request->getParameter('evaluacion')));

		$this->getUser()->setFlash('notice', 'Evaluación "'.$evalucion->getNombre().'" borrada');

		$evalucion->delete();

		$this->redirect('modelador/curso?id='.$curso->getId());
	}

	/*
	 * Fucnion para procesar un tema
	*/
	protected function processFormTema(sfWebRequest $request, sfForm $form)
	{
		$form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
		if ($form->isValid())
		{
			$form->getObject()->setOperadorId($this->getUser()->getAttribute('usuario')->getId());

			$mensaje = 'Tema "'.$form->getValue('nombre').'" actualizado';
			if($form->getObject()->isNew())
			{
				$mensaje = 'Tema "'.$form->getValue('nombre').'" creado';
			}

			$tema = $form->save();

			$this->getUser()->setFlash('notice', $mensaje);

			$this->redirect('modelador/curso?id='.$tema->getCursoId());
		}
	}

	/*
	 * Fucnion para procesar una evaluacion
	*/
	protected function processFormEvaluacion(sfWebRequest $request, sfForm $form)
	{
		$form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
		if ($form->isValid())
		{
			$form->getObject()->setOperadorId($this->getUser()->getAttribute('usuario')->getId());

			$mensaje = 'Evaluación parcial "'.$form->getValue('nombre').'" actualizada';
			if($form->getObject()->isNew())
			{
				$mensaje = 'Evaluación parcial "'.$form->getValue('nombre').'" creada';
			}

			$evaluacion = $form->save();

			$this->getUser()->setFlash('notice', $mensaje);

			$this->redirect('modelador/curso?id='.$evaluacion->getCursoId());
		}
	}

	/*
	 * Fucnion para procesar una evaluacion final
	*/
	protected function processFormFinal(sfWebRequest $request, sfForm $form)
	{
		$form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
		if ($form->isValid())
		{
			$form->getObject()->setOperadorId($this->getUser()->getAttribute('usuario')->getId());

			$mensaje = 'Evaluación final"'.$form->getValue('nombre').'" actualizada';
			if($form->getObject()->isNew())
			{
				$mensaje = 'Evaluación final"'.$form->getValue('nombre').'" creada';
			}

			$evaluacion = $form->save();

			$this->getUser()->setFlash('notice', $mensaje);

			$this->redirect('modelador/curso?id='.$evaluacion->getCursoId());
		}
	}

}
