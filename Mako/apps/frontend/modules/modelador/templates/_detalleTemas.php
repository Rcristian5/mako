<div id="temas">

	<?php include_partial('formTema', array('form' => $form, 'curso'=>$curso)) ?>

	<h3>Lista de temas</h3>

	<table id="detalle-temas" class="display" width="100%">
		<thead>
			<tr>
				<th class="acciones-tabla"></th>
				<th>N&uacute;mero</th>
				<th>Nombre</th>
				<th>Descripci&oacute;n</th>
				<th class="acciones-tabla"></th>
				<th class="acciones-tabla"></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($temas as $tema): ?>
			<tr>
				<td><a
					href="<?php echo url_for('modelador/editTema?tema='.$tema->getId().'&curso='.$curso) ?>"><span
						class="ui-icon ui-icon-pencil bx-title" title="Editar tema"></span>
				</a>
				</td>
				<td><?php echo $tema->getNumero() ?>
				</td>
				<td><?php echo $tema->getNombre() ?>
				</td>
				<td><?php echo $tema->getDescripcion() ?>
				</td>
				<td><?php echo ($tema->getActivo() ? link_to('<span class="ui-icon ui-icon-bullet bx-title" title="Desactivar tema"></span>', 'modelador/disabledTema?tema='.$tema->getId().'&curso='.$curso, array('confirm' => '¿Deseas desactivar este tema?')) : link_to('<span class="ui-icon ui-icon-radio-on bx-tittle" title="Activar tema"></span>', 'modelador/enabledTema?tema='.$tema->getId().'&curso='.$curso, array('confirm' => '¿Deseas activar este tema?')) ) ?>
				</td>
				<td><?php echo link_to('<span class="ui-icon ui-icon-trash bx-title" title="Borrar tema"></span>', 'modelador/deleteTema?tema='.$tema->getId().'&curso='.$curso, array('method' => 'delete', 'confirm' => '¿Deseas elimar este tema? \n Todos los datos asociados serán borrados')) ?>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>

</div>
