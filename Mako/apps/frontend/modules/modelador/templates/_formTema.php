<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<h3>
	<?php echo ($form->getObject()->isNew() ? 'Crear tema' : 'Editar tema') ?>
</h3>
<div id="tema-form">
	<form
		action="<?php echo url_for('modelador/'.($form->getObject()->isNew() ? 'createTema' : 'updateTema').(!$form->getObject()->isNew() ? '?curso='.$curso.'&id='.$form->getObject()->getId() : '?curso='.$curso)) ?>"
		method="post"
		<?php $form->isMultipart() and print 'enctype="multipart/form-data" '?>
		onsubmit="return requeridos(getElementById('tema-form'));">
		<?php if (!$form->getObject()->isNew()): ?>
		<input type="hidden" name="sf_method" value="put" />
		<?php endif; ?>
		<?php echo $form->renderGlobalErrors() ?>
		<table>
			<tbody>
				<tr>
					<th><?php echo $form['numero']->renderLabel('Número') ?></th>
					<td><?php echo $form['numero']->renderError() ?> <?php echo $form['numero']->render(array('filtro'=>'numerico', 'size'=>3, 'maxlength'=>10)) ?>
					</td>
				</tr>
				<tr>
					<th><?php echo $form['nombre']->renderLabel() ?></th>
					<td><?php echo $form['nombre']->renderError() ?> <?php echo $form['nombre']->render(array('requerido'=>1, 'filtro'=>'alfanumerico', 'size'=>40, 'maxlength'=>255)) ?>
					</td>
				</tr>
				<tr>
					<th><?php echo $form['descripcion']->renderLabel('Descripción') ?>
					</th>
					<td><?php echo $form['descripcion']->renderError() ?> <?php echo $form['descripcion']->render(array('requerido'=>1, 'filtro'=>'alfanumerico', 'cols'=>35, 'rows'=>4)) ?>
					</td>
				</tr>
			</tbody>
		</table>
		<p class="botones_finales">
			<?php echo $form->renderHiddenFields(false) ?>
			<input type="button" value="Cancelar"
				onclick="location.href='<?php echo url_for('modelador/curso?id='.$curso)?>'">
			<input type="submit" value="Guardar" />
		</p>
	</form>
</div>
