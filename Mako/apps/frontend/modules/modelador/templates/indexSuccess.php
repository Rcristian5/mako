<?php use_stylesheet('control_escolar/control_escolar.css') ?>
<?php use_stylesheet('datatables.css') ?>
<?php use_stylesheet('jquery.windows.css') ?>
<?php use_javascript('jquery.ui.js') ?>
<?php use_javascript('plugins/jquery.windows.js') ?>
<?php use_javascript('plugins/jquery.cookies.js') ?>
<?php use_javascript('plugins/jquery.dataTables.js') ?>
<?php use_javascript('control_escolar/modelador.js') ?>
<?php use_stylesheet('./modelador/indexSuccess.css')?>
<div id="windows-wrapper">
	<div id="columnaIzquierda" window-title="Cursos" window-state="max">
		<h2>
			Temario del curso
			<?php echo ($curso ? $curso->getNombre() : '')  ?>
		</h2>
		<?php if($temas):?>
		<div id="tab" style="width: 99%">
			<ul>
				<li><a href="#temas">Temas del curso</a></li>
				<li><a href="#evaluaciones-parciales">Evaluaciones parciales</a></li>
				<li><a href="#evaluacion-final">Evaluacion final</a></li>
			</ul>

			<?php include_partial('detalleTemas', array('temas' => $temas, 'form' => $formTema, 'curso' => $curso->getId())) ?>
			<?php include_partial('detalleEvaluaciones', array('evaluaciones' => $evaluaciones, 'form' => $formEvaluacion, 'mostrar' => $mostrar, 'curso' => $curso->getId())) ?>
			<?php include_partial('evaluacionFinal', array('form' => $formFinal, 'mostrar' => $mostrar, 'curso' => $curso->getId())) ?>

		</div>
		<?php else:?>
		<p
			style="font-weight: bold; padding: 30px 0; text-align: center; font-size: 14px">
			Selecciona un curso de la lista</p>
		<?php endif; ?>

	</div>

	<div id="columnaDerecha" window-title="Cursos registrados"
		window-state="max">

		<h2>Lista de cursos</h2>

		<?php include_partial('detalleCursos', array('detalle' => $detalleCursos)) ?>

	</div>

</div>

