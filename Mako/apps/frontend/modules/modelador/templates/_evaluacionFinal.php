<div id="evaluacion-final">
	<?php use_stylesheets_for_form($form) ?>
	<?php use_javascripts_for_form($form) ?>

	<h3>
		<?php echo ($form->getObject()->isNew() ? 'Crear evaluación final' : 'Editar evaluación final') ?>
	</h3>
	<div id="final-form">
		<form
			action="<?php echo url_for('modelador/'.($form->getObject()->isNew() ? 'createFinal' : 'updateFinal').(!$form->getObject()->isNew() ? '?curso='.$curso.'&id='.$form->getObject()->getId() : '?curso='.$curso)) ?>"
			method="post"
			<?php $form->isMultipart() and print 'enctype="multipart/form-data" '?>
			onsubmit="return requeridos(getElementById('final-form'));">
			<?php if (!$form->getObject()->isNew()): ?>
			<input type="hidden" name="sf_method" value="put" />
			<?php endif; ?>
			<?php echo $form->renderGlobalErrors() ?>
			<table>
				<tbody>
					<tr>
						<th><?php echo $form['nombre']->renderLabel() ?>
						</th>
						<td><?php echo $form['nombre']->renderError() ?> <?php echo $form['nombre']->render(array('requerido'=>1, 'filtro'=>'alfanumerico', 'size'=>40, 'maxlength'=>255)) ?>
						</td>
					</tr>
					<tr>
						<th><?php echo $form['descripcion']->renderLabel('Descripción') ?>
						</th>
						<td><?php echo $form['descripcion']->renderError() ?> <?php echo $form['descripcion']->render(array('requerido'=>1, 'filtro'=>'alfanumerico', 'cols'=>35, 'rows'=>4)) ?>
						</td>
					</tr>
					<tr>
						<th><?php echo $form['calificacion_minima']->renderLabel('Calificación mínima') ?>
						</th>
						<td><?php echo $form['calificacion_minima']->renderError() ?> <?php echo $form['calificacion_minima']->render(array('requerido'=>0, 'filtro'=>'numerico', 'size'=>5, 'maxlength'=>5)) ?>
						</td>
					</tr>
					<tr>
						<th><?php echo $form['calificacion_maxima']->renderLabel('Calificación máxima') ?>
						</th>
						<td><?php echo $form['calificacion_maxima']->renderError() ?> <?php echo $form['calificacion_maxima']->render(array('requerido'=>0, 'filtro'=>'numerico', 'size'=>5, 'maxlength'=>5)) ?>
						</td>
					</tr>
					<?php if(!$form->getObject()->isNew()): ?>
					<tr>
						<th><?php echo $form['activo']->renderLabel('Activa') ?>
						</th>
						<td><?php echo $form['activo']->renderError() ?> <?php echo $form['activo'] ?>
						</td>
					</tr>
					<?php endif; ?>
				</tbody>
			</table>
			<p class="botones_finales">
				<?php echo $form->renderHiddenFields(false) ?>
				<input type="button" value="Cancelar"
					onclick="location.href='<?php echo url_for('modelador/curso?id='.$curso)?>'">
				<input type="submit" value="Guardar" />
			</p>
		</form>
	</div>
</div>
