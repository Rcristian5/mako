<table id="detalle" class="display" style="width: 100%;">
	<thead>
		<tr>
			<th class="acciones-tabla"></th>
			<th>Categoria</th>
			<th>Clave</th>
			<th>Nombre</th>
			<th>Descripci&oacute;n</th>
			<th>Perfil facilitador</th>
			<th>Duración (Hrs.)</th>
			<th>Seriado</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($detalle as $curso): ?>
		<tr>
			<td><a
				href="<?php echo url_for('modelador/curso?id='.$curso->getId()) ?>"><span
					class="ui-icon ui-icon-note bx-title" title="Modelar curso"></span>
			</a>
			</td>
			<td><?php echo $curso->getCategoriaCurso()->getNombre() ?>
			</td>
			<td><?php echo $curso->getClave() ?>
			</td>
			<td><?php echo $curso->getNombre() ?>
			</td>
			<td><?php echo $curso->getDescripcion() ?>
			</td>
			<td><?php echo $curso->getPerfilFacilitador()->getNombre() ?>
			</td>
			<td><?php echo $curso->getDuracion() ?>
			</td>
			<td><?php echo ($curso->getSeriado() ?'Si': 'No') ?>
			</td>
			<td><?php echo (!$curso->getPublicado() ? link_to('<span class="ui-icon ui-icon-flag bx-title" title="Publicar curso"></span>', 'modelador/publicar?id='.$curso->getId(), array('confirm' => '¿Deseas activar este curso? ')) : '' ) ?>
			</td>
		</tr>
		<?php endforeach ?>
	</tbody>
</table>
