<div id="evaluaciones-parciales">
	<?php if($mostrar): ?>

	<?php include_partial('formEvaluacionParcial', array('form' => $form, 'curso'=>$curso)) ?>

	<h3>Lista de evaluaciones parciales</h3>

	<table id="detalle-evaluaciones" class="display" width="100%">
		<thead>
			<tr>
				<th class="acciones-tabla"></th>
				<th>Tema</th>
				<th>Nombre</th>
				<th>Descripci&oacute;n</th>
				<th>%</th>
				<th>M&iacute;nima</th>
				<th>M&aacute;xima</th>
				<th class="acciones-tabla"></th>
				<th class="acciones-tabla"></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($evaluaciones as $evaluacion): ?>
			<tr>
				<td><a
					href="<?php echo url_for('modelador/editEvaluacion?evaluacion='.$evaluacion->getId().'&curso='.$curso) ?>"><span
						class="ui-icon ui-icon-pencil bx-title" title="Editar evaluación"></span>
				</a>
				</td>
				<td><?php echo $evaluacion->getTema()->getNombre() ?>
				</td>
				<td><?php echo $evaluacion->getNombre() ?>
				</td>
				<td><?php echo $evaluacion->getDescripcion() ?>
				</td>
				<td><?php echo $evaluacion->getPorcentaje() ?>
				</td>
				<td><?php echo ($evaluacion->getCalificacionMinima() ? $evaluacion->getCalificacionMinima() : 'No' )  ?>
				</td>
				<td><?php echo ($evaluacion->getCalificacionMaxima() ? $evaluacion->getCalificacionMaxima() : 'No' )  ?>
				</td>
				<td><?php echo ($evaluacion->getActivo() ? link_to('<span class="ui-icon ui-icon-bullet bx-title" title="Desactivar evaluación"></span>', 'modelador/disabledEvaluacion?evaluacion='.$evaluacion->getId().'&curso='.$curso, array('confirm' => '¿Deseas desactivar esta evaluación?')) : link_to('<span class="ui-icon ui-icon-radio-on bx-tittle" title="Activar evaluación"></span>', 'modelador/enabledEvaluacion?evaluacion='.$evaluacion->getId().'&curso='.$curso, array('confirm' => '¿Deseas activar esta evaluación?')) ) ?>
				</td>
				<td><?php echo link_to('<span class="ui-icon ui-icon-trash bx-title" title="Borrar evaluación"></span>', 'modelador/deleteEvaluacion?evaluacion='.$evaluacion->getId().'&curso='.$curso, array('method' => 'delete', 'confirm' => '¿Deseas elimar esta evaluación?')) ?>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	<?php else: ?>
	<p style="font-weight: bold; padding: 20px 0; text-align: center">Es
		necesario capturar al menos un tema para registrar una
		evaluaci&oacute;n parcial</p>
	<?php endif; ?>
</div>
