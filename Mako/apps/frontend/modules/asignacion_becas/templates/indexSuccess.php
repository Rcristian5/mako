<?php use_javascript('plugins/jquery.dataTables.js') ?>
<?php use_stylesheet('datatables.css') ?>

<script>

var lTAsignacionBecas;
$(document).ready(function() {

   lTAsignacionBecas = $('#lista-AsignacionBecas').dataTable({
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": true,
        "bJQueryUI": true,
        "sPaginationType": "full_numbers",
        "aaSorting": [[0, 'asc']] 
     });
   
});

</script>

<div class="ui-widget-header ui-corner-top tsss" style="padding: 5px;">Lista
	de solicitudes de Beca</div>
<div class="ui-widget ui-corner-bottom sombra">
	<div class="ui-widget-content ui-corner-bottom" style="padding: 5px;">

		<?php if ($sf_user->hasFlash('ok')): ?>
		<div
			class="flash_ok ui-state-highlight ts sombra-delgada ui-corner-all"
			style="padding: 5px; margin-top: 10px; margin-bottom: 10px;">
			<span style="float: left; margin-right: 0.3em;"
				class="ui-icon ui-icon-info"></span> <strong><?php echo $sf_user->getFlash('ok') ?>
			</strong>
		</div>
		<?php endif; ?>


		<br /> <span style="float: left;" class="ui-icon ui-icon-circle-plus"></span>
		<a href="<?php echo url_for('asignacion_becas/new') ?>"> Nueva
			solicitud </a> <br /> <br />


		<table id="lista-AsignacionBecas" style="width: 100%;">
			<thead>
				<tr>
					<th>Beca</th>
					<th>Centro</th>
					<th>Socio</th>
					<th>Fecha solicitud</th>
					<th>Fecha otorgado</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($AsignacionBecas as $AsignacionBeca): ?>
				<tr>
					<td><?php echo $AsignacionBeca->getBeca() ?></td>
					<td><?php echo $AsignacionBeca->getCentro() ?></td>
					<td><?php echo $AsignacionBeca->getSocio()->getUsuario() ?></td>
					<td><?php echo $AsignacionBeca->getFechaSolicitud() ?></td>
					<td><?php echo $AsignacionBeca->getFechaOtorgado() ?></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>


	</div>
</div>

