<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<?php use_javascript('jquery.ui.js') ?>
<?php use_javascript('plugins/jquery.dataTables.js') ?>
<?php use_stylesheet('datatables.css') ?>


<script>
$(document).ready(function(){
	//Escuchamos evento del buscador, socio.seleccionado
	$(document).bind('socio.seleccionado',function(ev, socio_id, socio_nombre){
	
		//Seteamos el hidden de socio_id con el valor seleccionado
		$("#asignacion_beca_socio_id").val(socio_id);
		//Deshabilitamos la liga de seleccionar y ponemos un msg para indicar que ese socio se ha seleccionado
		//$("#liga-sel-"+socio_id).hide();
		$("#liga-sel-"+socio_id).text('Seleccionado').css('color','red');

		//Cuando seleccionamos un usuario escondemos todos los demas resultados
		$(".resultados-buscador-socios").find("fieldset").each(function(){

			if($(this).attr('id') != 'fs_'+socio_id)
			{
				$(this).hide('fast');
			}
		});
	});


	//Escuchamos cuando se ejecuta el evento de búsqueda
	$(document).bind('socios.buscador.buscando',function(){
		console.debug('Buscando...');
		//Seteamos el hidden de socio_id con el valor seleccionado
		$("#asignacion_beca_socio_id").val("");
	});


	//Ventana de detalle del socio
	var $wcm= $("#verDetalleSocio").dialog({
		modal: true,
		autoOpen: false,	    		
		position: ['center','center'],
		title:	'Ver detalle',
		height: 550,
		width: 1100,
		buttons: {
			'Cerrar': function() 
			{
				$(this).dialog('close');
			}							
		}
	});
	
	//Funcion que despliega la ventana de detalles del socio
	function verDetalle(idSocio){
		var UrlVerDetalle = '<?php echo url_for('registro/verDetalle') ?>';
		$("#verDetalleSocio").load(								 
			UrlVerDetalle,
			{
				id: idSocio
			}			
		); 
		$wcm.dialog('open');					
	}

	//Escuchamos evento del buscador, socio.seleccionado
	$(document).bind('socios.ver-detalle',function(ev, socio_id){
		if(socio_id != undefined){
			console.log("Escuchando evento de ver detalle: "+socio_id);
			verDetalle(socio_id);
		}
	});
	
		
});
</script>

<!-- Ver detalle socio -->
<div class="ui-widget ui-corner-all sombra">
	<div id="verDetalleSocio" style="display: none"></div>
</div>
<!-- /Ver detalle socio -->


<div class="ui-widget ui-corner-all sombra">
	<div class="ui-widget-header ui-corner-top tsss" style="padding: 5px;">Solicitud
		Becas</div>
	<div class="ui-widget-content ui-corner-bottom">

		<div style="margin: 15px;">

			<div id="ventana-buscador">
				<div id="buscador-socios">
					<?php include_component('registro','buscaSocios')?>
				</div>
			</div>

			<?php if($form->hasErrors()): ?>
			<div
				class="flash_error ui-state-error ts sombra-delgada ui-corner-all"
				style="padding: 5px; margin-top: 10px; margin-bottom: 10px;">
				<span style="float: left; pading-right: 0.3em;"
					class="ui-icon ui-icon-info"></span>
				<h2>
					<strong>Se han detectado errores al llenar la forma, por favor
						revisa los siguientes mensajes</strong>
				</h2>
				<ul>
					<?php foreach($form->getFormFieldSchema() as $name=>$formField):?>

					<?php if ($formField->hasError()):?>
					<li><?php echo $formField->renderError()?></li>
					<?php endif;?>
					<?php endforeach;?>
				</ul>
			</div>
			<?php endif; ?>


			<form
				action="<?php echo url_for('asignacion_becas/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>"
				method="post"
				<?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
				<?php if (!$form->getObject()->isNew()): ?>
				<input type="hidden" name="sf_method" value="put" />
				<?php endif; ?>
				<table id="">
					<tfoot>
						<tr>
							<td colspan="2">
								<hr /> &nbsp;<a
								href="<?php echo url_for('asignacion_becas/index') ?>">Ver
									Solicitudes</a> <input type="submit" value="Guardar"
								onclick="return requeridos();" />
							</td>
						</tr>
					</tfoot>
					<tbody>
						<?php echo $form ?>
					</tbody>
				</table>
			</form>


		</div>
	</div>
</div>
