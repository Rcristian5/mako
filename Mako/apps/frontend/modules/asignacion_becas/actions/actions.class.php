<?php

/**
 * asignacion_becas actions.
 *
 * @package    mako
 * @subpackage asignacion_becas
 * @author     Bixit SA de CV
 * @version    SVN: $Id: actions.class.php,v 1.2 2011/01/13 03:50:21 eorozco Exp $
 */
class asignacion_becasActions extends sfActions
{
	public function executeIndex(sfWebRequest $request)
	{
		$this->AsignacionBecas = AsignacionBecaPeer::doSelect(new Criteria());
	}

	public function executeNew(sfWebRequest $request)
	{
		$this->form = new AsignacionBecaForm();
	}

	public function executeCreate(sfWebRequest $request)
	{
		$this->forward404Unless($request->isMethod(sfRequest::POST));

		$this->form = new AsignacionBecaForm();

		$this->processForm($request, $this->form);

		$this->setTemplate('new');
	}

	public function executeEdit(sfWebRequest $request)
	{
		$this->forward404Unless($AsignacionBeca = AsignacionBecaPeer::retrieveByPk($request->getParameter('id')), sprintf('Object AsignacionBeca does not exist (%s).', $request->getParameter('id')));
		$this->form = new AsignacionBecaForm($AsignacionBeca);
	}

	public function executeUpdate(sfWebRequest $request)
	{
		$this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
		$this->forward404Unless($AsignacionBeca = AsignacionBecaPeer::retrieveByPk($request->getParameter('id')), sprintf('Object AsignacionBeca does not exist (%s).', $request->getParameter('id')));
		$this->form = new AsignacionBecaForm($AsignacionBeca);

		$this->processForm($request, $this->form);

		$this->setTemplate('edit');
	}

	public function executeDelete(sfWebRequest $request)
	{
		$request->checkCSRFProtection();

		$this->forward404Unless($AsignacionBeca = AsignacionBecaPeer::retrieveByPk($request->getParameter('id')), sprintf('Object AsignacionBeca does not exist (%s).', $request->getParameter('id')));
		$AsignacionBeca->delete();

		$this->redirect('asignacion_becas/index');
	}

	protected function processForm(sfWebRequest $request, sfForm $form)
	{
		$form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
		if ($form->isValid())
		{

			//Seteamos el centro al que pertenece el usuario
			$form->getObject()->setCentroId(sfConfig::get('app_centro_actual_id'));
			 
			//Asociamos el operador que esta dando salida.
			$operador_id = $this->getUser()->getAttribute('usuario')->getId();
			$form->getObject()->setRegistroOperadorId($operador_id);
			$form->getObject()->setFechaSolicitud(date("Y-m-d H:i:s"));


			$AsignacionBeca = $form->save();


	  $this->getUser()->setFlash('ok', sprintf('Se ha guardado correctamente la solicitud de beca.'));



	  $this->redirect('asignacion_becas/index?id='.$AsignacionBeca->getId());
		}
	}


	/**
	 * Regresa todas las solicitudes de becas dado un centro_id
	 *
	 * @WSMethod(webservice='MakoApi')
	 * @param string $centro_id
	 *
	 * @return string Regresa un arreglo asignacion_beca serializado
	 */
	public function executeGetSolicitudesBecas(sfWebRequest $request)
	{
		$centro_id = pg_escape_string($request->getParameter('centro_id'));
		$c = new Criteria();
		$c->add(AsignacionBecaPeer::CENTRO_ID,$centro_id);
		$becas = AsignacionBecaPeer::doSelect($c);
		$asignacion = array();
		foreach ($becas as $beca)
		{
			$asignacion[] = $beca->toArray();
		}

		if(count($asignacion) == 0)
		{
			$me = $this->isSoapRequest() ? new SoapFault('Server', "No existen solicitudes para el centro consultado: $centro_id"):"No existen solicitudes para el centro consultado: $centro_id";
			throw $me;
			return sfView::ERROR;
		}
		else
		{
			$this->result = serialize($asignacion);
			return sfView::SUCCESS;
		}
	}

}
