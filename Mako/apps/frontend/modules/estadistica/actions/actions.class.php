<?php

/**
 * registro actions.
 *
 * @package    mako
 * @subpackage registro
 * @author     Bixit SA de CV
 * @version    SVN: $Id: actions.class.php,v 1.9 2010/06/09 17:25:36 marcela Exp $
 */
class estadisticaActions extends sfActions
{
	public function executeIndex(sfWebRequest $request)
	{
		 
		$periocidad = $request->getParameter('periocidad');
		if($periocidad!='ninguna')
			$this->forward('estadistica', 'temporalidad');

		$ctx = sfContext::getInstance();
		$ctx->getConfiguration()->loadHelpers('Estadistica');

		$inicio = $request->getParameter('inicio');
		$fin = $request->getParameter('fin');

		if($inicio == NULL || $fin==NULL){
			$inicio = NULL;
			$fin = NULL;
			$tiulo = 'Hoy';
		}else
			$titulo = 'Consulta de '. strftime("%d-%m-%Y", strtotime($inicio)) .' al '. strftime("%d-%m-%Y", strtotime($fin));
			
		$this->msg = $titulo;



		//Conservar fechas y opciones de consulta de estadística
		$this->getUser()->setAttribute('vista_estadistica',array('fecha_inicio'=>$inicio,'fecha_fin'=> $fin,'periocidad'=>$periocidad));
		$valsInputs = $this->getUser()->getAttribute('vista_estadistica');
		$this->valInicio = $valsInputs['fecha_inicio'];
		$this->valFin = $valsInputs['fecha_fin'];
		$this->valPeriocidad = $valsInputs['periocidad'];

		/*
		 * POS
		*/

		//Inscripciones Socios
		$estadisticaPOSInscripciones = estadisticaPOSInscripciones($inicio, $fin);

		$inscripciones = array();
		foreach ($estadisticaPOSInscripciones as $row)
		{
			$inscripciones['filas'][] = array('cuenta' => $row['cuenta']);
			$inscripciones['ttl'] += $row['cuenta'];
			$inscripciones['porcentajeTotal'] =  100 * $row['cuenta'] /  $row['cuenta'] ;
		}

		$this->Inscripciones = $inscripciones;

		//Inscripciones Curso
		$estadisticaPOSInscripcionesCurso = estadisticaPOSInscripcionesCurso($inicio, $fin);

		$inscripcionesCurso = array();
		foreach ($estadisticaPOSInscripcionesCurso as $row)
		{
			$inscripcionesCurso['filas'][] = array('cuenta' => $row['cuenta']);
			$inscripcionesCurso['ttl'] += $row['cuenta'];
			$inscripcionesCurso['porcentajeTotal'] =  100 * $row['cuenta'] /  $row['cuenta'] ;
		}

		$this->InscripcionesCurso = $inscripcionesCurso;


		//Ventas por categoria
		$ventasPorCategoria = estadisticaPOSVentasCategoria($inicio, $fin);

		$ventas = array();
		foreach ($ventasPorCategoria as $row)
		{
			$ventas['filas'][] = array('ventas' => $row['label'],'cuenta' => $row['cuenta']);
			$ventas['ttl'] += $row['cuenta'];
			$ventas['porcentajeTotal'] =  100 * $row['cuenta'] /  $row['cuenta'] ;
		}

		$this->Ventas = $ventas;

		/*
		 * Socio
		*/

		//Genero
		$estadisticaSociosGenero = estadisticaSociosGenero($inicio, $fin);

		$genero = array();
		foreach ($estadisticaSociosGenero as $row)
		{
			$genero['filas'][] = array('genero' => $row['genero'],'cuenta' => $row['cuenta']);
			$genero['ttl'] += $row['cuenta'];
			$genero['porcentajeTotal'] =  100 * $row['cuenta'] /  $row['cuenta'] ;
		}

		$this->Genero = $genero;


		//Edad
		$edad = rangoEdad(estadisticaSociosEdad($inicio, $fin));
		$edadV = $edad['edadV'];
		$edadL = $edad['edadL'];
		foreach($edadV as $row => $val){
			$edad['filas'][] = array('edad' => $edadL[$row] ,'cuenta' => $edadV[$row]);
			$edad['ttl'] += $edadV[$row];
			$edad['porcentajeTotal'] =  100 * $edadV[$row] / $edadV[$row] ;
		}

		$this->Edad = $edad;



		//Escolaridad
		$estadisticaSociosEscolaridad = estadisticaSociosEscolaridad($inicio, $fin);

		$escolaridad = array();
		foreach ($estadisticaSociosEscolaridad as $row)
		{
			$escolaridad['filas'][] = array('estudio' => $row['estudio'],'cuenta' => $row['cuenta']);
			$escolaridad['ttl'] += $row['cuenta'];
			$escolaridad['porcentajeTotal'] =  100 * $row['cuenta'] /  $row['cuenta'];
		}

		$this->Escolaridad = $escolaridad;




		//Colonia
		$estadisticaSociosColonia = estadisticaSociosColonia($inicio, $fin);

		$colonia = array();
		foreach ($estadisticaSociosColonia as $row)
		{
			$colonia['filas'][] = array('colonia' => $row['colonia'],'cuenta' => $row['cuenta']);
			$colonia['ttl'] += $row['cuenta'];
			$colonia['porcentajeTotal'] =  100 * $row['cuenta'] /  $row['cuenta'];
		}

		$this->Colonia = $colonia;



		//Empresa
		$estadisticaSociosEmpresa = estadisticaSociosEmpresa($inicio, $fin);

		$empresa = array();
		foreach ($estadisticaSociosEmpresa as $row)
		{
			$empresa['filas'][] = array('empresa' => $row['empresa'],'cuenta' => $row['cuenta']);
			$empresa['ttl'] += $row['cuenta'];
			$empresa['porcentajeTotal'] =  100 * $row['cuenta'] / $row['cuenta'] ;
		}

		$this->Empresa = $empresa;
		 



		/*
		 * Servicio
		*/
		$estadisticaServicio = estadisticaServicio($inicio, $fin);

		$servicio = array();
		foreach ($estadisticaServicio as $row)
		{
			$servicio['filas'][] = array('paquete' => $row['paquete'],'cuenta' => $row['cuenta']);
			$servicio['ttl'] += $row['cuenta'];
			$servicio['porcentajeTotal'] =  100 * $row['cuenta'] /  $row['cuenta'] ;
		}

		$this->Servicio = $servicio;


		//Empresa
		$estadisticaEtapasProceso = estadisticaEtapasProceso($inicio, $fin);

		$proceso = array();
		foreach ($estadisticaEtapasProceso as $row)
		{
			$proceso['filas'][] = array('curso' => $row['curso'],'clave' => $row['clave'],'cuenta' => $row['cuenta'], 'categoria' => $row['categoria'],);
			$proceso['ttl'] += $row['cuenta'];
			$proceso['porcentajeTotal'] +=  100 * $row['cuenta'] / $proceso['ttl'] ;
		}

		$this->Proceso = $proceso;


	}

	 
	public function executeTemporalidad(sfWebRequest $request)
	{
		 
		$periocidad = $request->getParameter('periocidad');
		if($periocidad=='ninguna')
			$this->forward('estadistica', 'index');

		$ctx = sfContext::getInstance();
		$ctx->getConfiguration()->loadHelpers('EstadisticaTemporalidad');

		$inicio = $request->getParameter('inicio');
		$fin = $request->getParameter('fin');
		//$periocidad = $request->getParameter('periocidad');

		$arRangos = Comun::arregloIntervaloFechas($inicio,$fin,$periocidad);
		$arDias = Comun::arregloIntervaloFechas($inicio,$fin,'dias');

		//Conservar fechas y opciones de consulta de estadística
		$this->getUser()->setAttribute('vista_estadistica',array('fecha_inicio'=>$inicio,'fecha_fin'=> $fin,'periocidad'=>$periocidad));
		$valsInputs = $this->getUser()->getAttribute('vista_estadistica');
		$this->valInicio = $valsInputs['fecha_inicio'];
		$this->valFin = $valsInputs['fecha_fin'];
		$this->valPeriocidad = $valsInputs['periocidad'];


		/*
		 * POS
		*/

		//Inscripciones Cant - Global
		$this->InscripcionesXML = generaXML(estadisticaPOSInscripciones($inicio, $fin), 'Inscripciones Cant - Global', $arRangos , $arDias);
		$this->InscripcionesTabla = generaTabla(estadisticaPOSInscripciones($inicio, $fin), 'Inscripciones Cant - Global', $arRangos , $arDias);
		//Inscripciones por Curso / Paquete
		$this->InscripcionesCursoXML = generaXML(estadisticaPOSInscripcionesCurso($inicio, $fin), 'Inscripciones por Curso / Paquete', $arRangos , $arDias);
		$this->InscripcionesCursoTabla = generaTabla(estadisticaPOSInscripcionesCurso($inicio, $fin), 'Inscripciones por Curso / Paquete', $arRangos , $arDias);
		//Ventas por categoria
		//print_r (estadisticaPOSVentasCategoria($inicio, $fin));
		$this->VentasCategoriaXML = generaXML(estadisticaPOSVentasCategoria($inicio, $fin), 'Ventas por Categoría', $arRangos , $arDias);
		$this->VentasCategoriaTabla = generaTabla(estadisticaPOSVentasCategoria($inicio, $fin), 'Ventas por Categoría', $arRangos , $arDias);

		/*
		 * Socio
		*/

		//Genero
		$this->GeneroXML = generaXML(estadisticaSociosGenero($inicio, $fin), 'Género', $arRangos , $arDias);
		$this->GeneroTabla = generaTabla(estadisticaSociosGenero($inicio, $fin), 'Género', $arRangos , $arDias);

		//Edad
		$Edad = rangoEdad(estadisticaSociosEdad($inicio, $fin));
		$i=0;
		foreach($Edad[2] as $llave => $creado){
			foreach($creado as $val){
				$EdadT[$i]['label'] = $llave;
				$EdadT[$i]['created_at'] = $val;
				$i++;
			}
		}
		$this->EdadXML = generaXML($EdadT, 'Edad', $arRangos , $arDias);
		$this->EdadTabla = generaTabla($EdadT, 'Edad', $arRangos , $arDias);



		//Escolaridad
		$this->EscolaridadXML = generaXML(estadisticaSociosEscolaridad($inicio, $fin), 'Escolaridad', $arRangos , $arDias);
		$this->EscolaridadTabla = generaTabla(estadisticaSociosEscolaridad($inicio, $fin), 'Escolaridad', $arRangos , $arDias);
		//Colonia
		$this->ColoniaXML = generaXML(estadisticaSociosColonia($inicio, $fin), 'Colonia', $arRangos , $arDias);
		$this->ColoniaTabla = generaTabla(estadisticaSociosColonia($inicio, $fin), 'Colonia', $arRangos , $arDias);
		//Empresa
		$this->EmpresaXML = generaXML(estadisticaSociosEmpresa($inicio, $fin), 'Empresa', $arRangos , $arDias);
		$this->EmpresaTabla = generaTabla(estadisticaSociosEmpresa($inicio, $fin), 'Empresa', $arRangos , $arDias);

		/*
		 * Servicio
		*/

		//Paquete
		$this->ServicioXML = generaXML(estadisticaServicio($inicio, $fin), 'Paquetes', $arRangos , $arDias);
		$this->ServicioTabla = generaTabla(estadisticaServicio($inicio, $fin), 'Paquetes', $arRangos , $arDias);

		$titulo = 'Consulta de '. strftime("%d-%m-%Y", strtotime($inicio)) .' al '. strftime("%d-%m-%Y", strtotime($fin)) .' por ' .$periocidad;
			
		$this->msg = $titulo;

	}


}