<?php use_javascript('jquery.ui.js') ?>
<?php use_javascript('plugins/jquery.dataTables.js') ?>
<?php use_javascript('plugins/jquery.cookies.js') ?>
<?php use_javascript('marcaDefault.js') ?>
<?php use_stylesheet('datatables.css') ?>
<?php use_stylesheet('tabs.css') ?>

<SCRIPT
	LANGUAGE="Javascript" SRC="/graficas/JSClass/FusionCharts.js"></SCRIPT>

<script>

	//tabs
	$(function() {
		$("#tabs").tabs({ 
			cookie: {
			// store cookie for a day, without, it would be a session cookie
			expires: 1
			}
		});	    		
	});

	
	//datatable
	//var oTable;
	$(document).ready(function() {
		$('.dataTable').dataTable({
			"bPaginate": false,
			"bLengthChange": true,
			"bFilter": false,
			"bSort": true,
			"bInfo": true,
			"bAutoWidth": false,
	        "bJQueryUI": true,
	        "sPaginationType": "full_numbers"
	     });

	});

	$(function() {
		$(".datepicker").datepicker({
					firstDay: 1,
					changeYear: true,
					changeMonth: true,
					dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
					dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
					minDate: new Date(2010, 1 - 1, 1),
					maxDate: '+1d',  
					constrainInput: true,
					dateFormat: 'yy-mm-dd'
		});
	});


	
	
</script>


<form name="forma">

	<div id="page-wrap" class="sombra">
		<div id="tabs">

			<ul id="tabMenu" class="tsss">
				<li><a href="#pos-1">POS</a></li>
				<li><a href="#socios-2">Socios</a></li>
				<li><a href="#servicio-3">Servicio</a></li>
			</ul>


			<div id="rangoFechas">
				Inicio: <input type="text" name='inicio' class="datepicker"
					size="10" value="<?php echo $valInicio ?>"> Fin: <input type="text"
					name='fin' class="datepicker" size="10"
					value="<?php echo $valFin ?>"> Ninguna <input type="radio"
					name="periocidad" id="periocidad" value="ninguna"> Diario <input
					type="radio" name="periocidad" id="periocidad" value="dias">

				Semanal <input type="radio" name="periocidad" id="periocidad"
					value="semanas"> Quincenal <input type="radio" name="periocidad"
					id="periocidad" value="quincenas"> Mensual <input type="radio"
					name="periocidad" id="periocidad" value="meses"> Bimestral <input
					type="radio" name="periocidad" id="periocidad" value="bimestres">

				Trimestral <input type="radio" name="periocidad" id="periocidad"
					value="trimestres"> Semestral <input type="radio" name="periocidad"
					id="periocidad" value="semestres"> Anual <input type="radio"
					name="periocidad" id="periocidad" value="anios">

				<script>marca_radio('periocidad','<?php echo $valPeriocidad ?>');</script>
				<input type="submit" name="button" id="button" value="Enviar">



				<?php if (isset($msg) && $msg!= ''): ?>
				<div
					class="flash_notice ts ui-state-highlight ui-corner-all sombra-delgada">
					<span style="float: left; margin-right: 0.3em;"
						class="ui-icon ui-icon-info"></span> <strong><?php echo $msg ?> </strong>
				</div>
				<?php endif; ?>




			</div>

			<div id="pos-1">

				<h1>Inscripciones socio cant./global</h1>
				<div id="chartInscripciones"></div>
				<script language="JavaScript">					
			var chartInscripciones = new FusionCharts("/graficas/Charts/StackedColumn2D.swf", "chartInscripciones", "950", "600", "0", "1");		   	
			chartInscripciones.setDataXML("<?php echo html_entity_decode($InscripcionesXML, ENT_QUOTES);?>");
			chartInscripciones.addParam('wmode', 'opaque');
			chartInscripciones.render("chartInscripciones");
		</script>
				<div style="overflow-x: scroll; overflow-y: hidden;">
					<table width="100%" border="0" class="display dataTable">
						<?php echo html_entity_decode($InscripcionesTabla, ENT_QUOTES);?>
					</table>
				</div>

				<h1>Inscripciones por curso/paquete</h1>
				<div id="chartInscripcionesCurso"></div>
				<script language="JavaScript">					
			var chartInscripcionesCurso = new FusionCharts("/graficas/Charts/StackedColumn2D.swf", "chartInscripcionesCurso", "950", "600", "0", "1");	
			chartInscripcionesCurso.setDataXML("<?php echo html_entity_decode($InscripcionesCursoXML, ENT_QUOTES);?>	 ");
			chartInscripcionesCurso.render("chartInscripcionesCurso");
		</script>
				<div style="overflow-x: scroll; overflow-y: hidden;">
					<table width="100%" border="0" class="display dataTable">
						<?php echo html_entity_decode($InscripcionesCursoTabla, ENT_QUOTES);?>
					</table>
				</div>

				<h1>Ventas por categoría</h1>
				<div id="chartVentasCategoria"></div>
				<script language="JavaScript">					
			var chartVentasCategoria = new FusionCharts("/graficas/Charts/StackedColumn2D.swf", "chartVentasCategoria", "950", "600", "0", "1");	
			chartVentasCategoria.setDataXML("<?php echo html_entity_decode($VentasCategoriaXML, ENT_QUOTES);?>	 ");
			chartVentasCategoria.render("chartVentasCategoria");
		</script>
				<div style="overflow-x: scroll; overflow-y: hidden;">
					<table width="100%" border="0" class="display dataTable">
						<?php echo html_entity_decode($VentasCategoriaTabla, ENT_QUOTES);?>
					</table>
				</div>
			</div>

			<div id="socios-2">


				<h1>Género</h1>
				<div id="chart1div"></div>
				<script language="JavaScript">					
			var chart1 = new FusionCharts("/graficas/Charts/StackedColumn2D.swf", "chart1Id", "950", "600", "0", "1");	   	
			chart1.setDataXML("<?php echo html_entity_decode($GeneroXML, ENT_QUOTES);?>");
			chart1.addParam('wmode', 'opaque');
			chart1.render("chart1div");
		</script>
				<div style="overflow-x: scroll; overflow-y: hidden;">
					<table width="100%" border="0" class="display dataTable">
						<?php echo html_entity_decode($GeneroTabla, ENT_QUOTES);?>
					</table>
				</div>
				<!-- edad -->
				<h1>Edad</h1>
				<div id="chartEdad" align="left"></div>
				<script language="JavaScript">					
			var chartEdad = new FusionCharts("/graficas/Charts/StackedColumn2D.swf", "chartEdad", "950", "600", "0", "1");  			
			chartEdad.setDataXML("<?php echo html_entity_decode($EdadXML, ENT_QUOTES);?>");
			chartEdad.render("chartEdad");
		</script>
				<div style="overflow-x: scroll; overflow-y: hidden;">
					<table width="100%" border="0" class="display dataTable">
						<?php echo html_entity_decode($EdadTabla, ENT_QUOTES);?>
					</table>
				</div>

				<!-- escolaridad -->
				<h1>Escolaridad</h1>
				<div id="chartEscolaridad"></div>
				<script language="JavaScript">					
			var chartEscolaridad = new FusionCharts("/graficas/Charts/StackedColumn2D.swf", "chartEscolaridad", "950", "600", "0", "1");			  	      			
			chartEscolaridad.setDataXML("<?php echo html_entity_decode($EscolaridadXML, ENT_QUOTES);?>");
			chartEscolaridad.render("chartEscolaridad");
		</script>
				<div style="overflow: scroll">
					<table width="100%" border="0" class="display dataTable">
						<?php echo html_entity_decode($EscolaridadTabla, ENT_QUOTES);?>
					</table>
				</div>

				<!-- colonia -->
				<h1>Colonia</h1>
				<div id="chartColonia"></div>
				<script language="JavaScript">					
			var chartColonia = new FusionCharts("/graficas/Charts/StackedColumn2D.swf", "chartColonia", "950", "600", "0", "1");	     			
			chartColonia.setDataXML("<?php echo html_entity_decode($ColoniaXML, ENT_QUOTES);?>	");
			chartColonia.render("chartColonia");
		</script>
				<div style="overflow: scroll">
					<table width="100%" border="0" class="display dataTable">
						<?php echo html_entity_decode($ColoniaTabla, ENT_QUOTES);?>
					</table>
				</div>

				<!-- empresa referente -->
				<h1>Empresa referente</h1>
				<div id="chartEmpresa"></div>
				<script language="JavaScript">					
			var chartEmpresa = new FusionCharts("/graficas/Charts/StackedColumn2D.swf", "chartEmpresa", "950", "600", "0", "1");			        			
			chartEmpresa.setDataXML("<?php echo html_entity_decode($EmpresaXML, ENT_QUOTES);?>	");
			chartEmpresa.render("chartEmpresa");
		</script>
				<div style="overflow: scroll">
					<table width="100%" border="0" class="display dataTable">
						<?php echo html_entity_decode($EmpresaTabla, ENT_QUOTES);?>
					</table>
				</div>
			</div>




			<div id="servicio-3">
				<h1>Paquetes</h1>
				<div id="chartServicio"></div>
				<script language="JavaScript">					
			var chartServicio = new FusionCharts("/graficas/Charts/StackedColumn2D.swf", "chartServicio", "950", "800", "0", "1");		        			
			chartServicio.setDataXML("<?php echo html_entity_decode($ServicioXML, ENT_QUOTES);?>	");
			chartServicio.addParam('wmode', 'opaque');
			chartServicio.render("chartServicio");
		</script>

				<div style="overflow: scroll">
					<table width="100%" border="0" class="display dataTable">
						<?php echo html_entity_decode($ServicioTabla, ENT_QUOTES);?>
					</table>
				</div>
			</div>





		</div>
	</div>
</form>
