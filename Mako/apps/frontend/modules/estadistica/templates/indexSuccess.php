<?php use_javascript('jquery.ui.js') ?>
<?php use_javascript('plugins/jquery.dataTables.js') ?>
<?php use_javascript('plugins/jquery.cookies.js') ?>
<?php use_javascript('marca_default.js') ?>
<?php use_stylesheet('datatables.css') ?>
<?php use_stylesheet('tabs.css') ?>


<SCRIPT
	LANGUAGE="Javascript" SRC="/graficas/JSClass/FusionCharts.js"></SCRIPT>

<script>

	//tabs
	$(function() {
		$("#tabs").tabs({
			cookie: {expires: 1}
		});
	});

	//datatable
	//var oTable;
	$(document).ready(function() {
		$('.dataTable').dataTable({
			"bPaginate": false,
			"bLengthChange": true,
			"bFilter": false,
			"bSort": true,
			"bInfo": true,
			"bAutoWidth": false,
	        "bJQueryUI": true,
	        "sPaginationType": "full_numbers"
	     });

	});

	$(function() {
		$(".datepicker").datepicker({
					firstDay: 1,
					changeYear: true,
					changeMonth: true,
					dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
					dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
					monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
					minDate: new Date(2010, 1 - 1, 1),
					maxDate: '+1d',  
					constrainInput: true,
					dateFormat: 'yy-mm-dd'
		});
	});


	
	
</script>

<form name="forma">

	<div id="page-wrap" class="sombra">
		<div id="tabs">

			<ul id="tabMenu" class="tsss">
				<li><a href="#pos-1">POS</a></li>
				<li><a href="#socios-2">Socios</a></li>
				<li><a href="#servicio-3">Servicio</a></li>
			</ul>

			<div id="rangoFechas">
				Inicio: <input type="text" name='inicio' class="datepicker"
					size="10" value="<?php echo $valInicio ?>"> Fin: <input type="text"
					name='fin' class="datepicker" size="10"
					value="<?php echo $valFin ?>"> Ninguna <input type="radio"
					name="periocidad" id="periocidad" value="ninguna"> Diario <input
					type="radio" name="periocidad" id="periocidad" value="dias">

				Semanal <input type="radio" name="periocidad" id="periocidad"
					value="semanas"> Quincenal <input type="radio" name="periocidad"
					id="periocidad" value="quincenas"> Mensual <input type="radio"
					name="periocidad" id="periocidad" value="meses"> Bimestral <input
					type="radio" name="periocidad" id="periocidad" value="bimestres">

				Trimestral <input type="radio" name="periocidad" id="periocidad"
					value="trimestres"> Semestral <input type="radio" name="periocidad"
					id="periocidad" value="semestres"> Anual <input type="radio"
					name="periocidad" id="periocidad" value="anios">

				<script>marca_radio('periocidad','<?php echo $valPeriocidad ?>');</script>

				<input type="submit" name="button" id="button" value="Enviar">



				<?php if (isset($msg) && $msg!= ''): ?>
				<div
					class="flash_notice ts ui-state-highlight ui-corner-all sombra-delgada">
					<span style="float: left; margin-right: 0.3em;"
						class="ui-icon ui-icon-info"></span> <strong><?php echo $msg ?> </strong>
				</div>
				<?php endif; ?>




			</div>

			<div id="pos-1">
				<table width="100%" border="0" class="display dataTable">
					<thead>
						<tr>
							<th class="ui-state-default">#</th>
							<th class="ui-state-default">Tipo</th>
							<th class="ui-state-default">TTL</th>
							<th class="ui-state-default">&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>1</td>
							<td>Inscripciones socio cant./global</td>
							<td><?php echo $Inscripciones['ttl']?></td>
							<td><?php echo $Inscripciones['porcentajeTotal'] ?> %</td>
						</tr>


						<tr>
							<td>2</td>
							<td>Inscripciones por curso/paquete</td>
							<td><?php echo $InscripcionesCurso['ttl']?></td>
							<td><?php echo $InscripcionesCurso['porcentajeTotal'] ?> %</td>
						</tr>

						<!-- 
		<tr>
          <td>3</td>
          <td>Inscripciones monto/global</td>
          <td>$<?php echo $InscripcionesMonto['ttl']?></td>
          <td><?php echo $InscripcionesMonto['porcentajeTotal'] ?> % </td>
        </tr>
         -->
					</tbody>
				</table>




				<h1>Ventas por categoría</h1>
				<table width="100%">
					<tr>
						<td>
							<div id="chartVentas"></div> <script language="JavaScript">					
			var chartVentas = new FusionCharts("/graficas/Charts/Pie2D.swf", "chartVentas", "600", "400", "0", "1");	
			 <?php $xml = "<chart caption='Ventas por categoría' showPercentageValues='1' showBorder ='1' >"; 
			 	foreach ($Ventas['filas'] as $row)
				{
					$xml.= "<set label='".$row['ventas']."' value='" .$row['cuenta']."' />";
				}
				$xml.= "</chart>"; 
				?>	   			
			chartVentas.setDataXML("<?php echo $xml ?>");
			chartVentas.render("chartVentas");
		</script>
						</td>
						<td width="80%" valign="top">
							<table width="100%" border="0" class="display dataTable">
								<thead>
									<tr>
										<th class="ui-state-default">Detalle</th>
										<th class="ui-state-default">$</th>
										<th class="ui-state-default">%</th>
									</tr>
								</thead>
								<tbody>
									<!-- Ventas -->
									<?php foreach ($Ventas['filas'] as $row)
									{
										echo "<tr>";
										echo '<td>'.$row['ventas'].'</td>';
										echo '<td align="right">'.number_format($row['cuenta'],2).'</td>';
										echo '<td align="right">'.  round(100 * $row['cuenta'] / $Ventas['ttl'], 2)   .' % </td>';
										echo " </tr>";
									}
									?>
								</tbody>
								<tfoot>
									<tr>
										<td><b>Total</b></td>
										<td align="right"><b><?php echo number_format($Ventas['ttl'],2)?>
										</b></td>
										<td align="right"><b><?php echo $Ventas['porcentajeTotal'] ?>
												% </b></td>
									</tr>
								</tfoot>

							</table>
						</td>
					</tr>
				</table>


			</div>




			<div id="socios-2">

				<h1>Género</h1>
				<table width="100%">
					<tr>
						<td>
							<div id="chart1div">
								<!-- Aqui en este div va a aparecer el grafico -->
							</div> <!-- debe mandarse llamar el codigo despues del div o cargar este codigo en un metodo onload de jquery -->
							<script language="JavaScript">					
			var chart1 = new FusionCharts("/graficas/Charts/Pie2D.swf", "chart1Id", "600", "400", "0", "1");	
			 <?php $xml = "<chart caption='Género' showPercentageValues='1' showBorder ='1' >"; 
			 	foreach ($Genero['filas'] as $row)
				{
					$xml.= "<set label='".$row['genero']."' value='" .$row['cuenta']."' />";
				}
				$xml.= "</chart>"; 
				?>	   			
			chart1.setDataXML("<?php echo $xml ?>");
			chart1.render("chart1div");
		</script>
						</td>
						<td width="80%" valign="top">
							<table width="100%" border="0" class="display dataTable">
								<thead>
									<tr>
										<th class="ui-state-default">Detalle</th>
										<th class="ui-state-default">TTL</th>
										<th class="ui-state-default">%</th>
									</tr>
								</thead>
								<tbody>
									<!-- genero -->
									<?php foreach ($Genero['filas'] as $row)
									{
										echo "<tr>";
										echo '<td>'.$row['genero'].'</td>';
										echo '<td>'.$row['cuenta'].'</td>';
										echo '<td>'.  round(100 * $row['cuenta'] / $Genero['ttl'], 2)   .' % </td>';
										echo " </tr>";
									}
									?>
								</tbody>
								<tfoot>
									<tr>
										<td><b>Total</b></td>
										<td><b><?php echo $Genero['ttl']?> </b></td>
										<td><b><?php echo $Genero['porcentajeTotal'] ?> % </b></td>
									</tr>
								</tfoot>

							</table>
						</td>
					</tr>
				</table>

				<!-- edad -->
				<h1>Edad</h1>
				<table width="100%">
					<tr>
						<td>
							<div id="chartEdad" align="left">
								<!-- Aqui en este div va a aparecer el grafico -->
							</div> <script language="JavaScript">					
			var chartEdad = new FusionCharts("/graficas/Charts/Pie2D.swf", "chartEdad", "600", "400", "0", "1");	
			 <?php $xml = "<chart caption='Edad' showPercentageValues='1' showBorder ='1' >"; 
			 	foreach ($Edad['filas'] as $row)
				{
					$xml.= "<set label='".$row['edad']."' value='" .$row['cuenta']."' />";
				}
				$xml.= "</chart>"; 
				?>	   			
			chartEdad.setDataXML("<?php echo $xml ?>");
			chartEdad.render("chartEdad");
		</script>
						</td>
						<td width="80%" valign="top">
							<table width="100%" border="0" class="display dataTable">
								<thead>
									<tr>
										<th class="ui-state-default">Detalle</th>
										<th class="ui-state-default">TTL</th>
										<th class="ui-state-default">%</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($Edad['filas'] as $row)
									{
										echo "<tr>";
										echo '<td>'.$row['edad'].'</td>';
										echo '<td>'.$row['cuenta'].'</td>';
										echo '<td>'.  round(100 * $row['cuenta'] / $Edad['ttl'], 2)   .' % </td>';
										echo " </tr>";
									}
									?>
								</tbody>
								<tfoot>
									<tr>
										<td><b>Total</b></td>
										<td><b><?php echo $Edad['ttl']?> </b></td>
										<td><b><?php echo $Edad['porcentajeTotal'] ?> % </b></td>
									</tr>
								</tfoot>
							</table>
						</td>
					</tr>
				</table>

				<!-- escolaridad -->
				<h1>Escolaridad</h1>
				<table width="100%">
					<tr>
						<td valign="top">
							<div id="chartEscolaridad" align="left">
								<!-- Aqui en este div va a aparecer el grafico -->
							</div> <script language="JavaScript">					
			var chartEscolaridad = new FusionCharts("/graficas/Charts/Pie2D.swf", "chartEscolaridad", "600", "400", "0", "1");	
			 <?php $xml = "<chart caption='Escolaridad' showPercentageValues='1' showBorder ='1' >"; 
			 	foreach ($Escolaridad['filas'] as $row)
				{
					$xml.= "<set label='".$row['estudio']."' value='" .$row['cuenta']."' />";
				}
				$xml.= "</chart>"; 
				?>	   			
			chartEscolaridad.setDataXML("<?php echo $xml ?>");
			chartEscolaridad.render("chartEscolaridad");
		</script>
						</td>
						<td width="80%" valign="top">
							<table width="100%" border="0" class="display dataTable">
								<thead>
									<tr>
										<th class="ui-state-default">Detalle</th>
										<th class="ui-state-default">TTL</th>
										<th class="ui-state-default">%</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($Escolaridad['filas'] as $row)
									{
										echo "<tr>";
										echo '<td>'.$row['estudio'].'</td>';
										echo '<td>'.$row['cuenta'].'</td>';
										echo '<td>'. round( 100 * $row['cuenta'] / $Escolaridad['ttl'] , 2)  .' % </td>';
										echo " </tr>";
									}
									?>
								</tbody>
								<tfoot>
									<tr>
										<td><b>Total</b></td>
										<td><b><?php echo $Escolaridad['ttl']?> </b></td>
										<td><b><?php echo $Escolaridad['porcentajeTotal'] ?> % </b></td>
									</tr>
								</tfoot>


							</table>
						</td>
					</tr>
				</table>

				<!-- colonia -->
				<h1>Colonia</h1>
				<table width="100%">
					<tr>
						<td valign="top">
							<div id="chartColonia" align="left">
								<!-- Aqui en este div va a aparecer el grafico -->
							</div> <script language="JavaScript">					
			var chartColonia = new FusionCharts("/graficas/Charts/Pie2D.swf", "chartColonia", "600", "400", "0", "1");	
			 <?php $xml = "<chart caption='Colonia' showPercentageValues='1' showBorder ='1' >"; 
			 	foreach ($Colonia['filas'] as $row)
				{
					$xml.= "<set label='". substr_replace($row['colonia'], '...', 10)."' hoverText='". $row['colonia']."' value='" .$row['cuenta']."' />";
				}
				$xml.= "</chart>"; 
				?>	   			
			chartColonia.setDataXML("<?php echo $xml ?>");
			chartColonia.render("chartColonia");
		</script>
						</td>
						<td width="60%" valign="top">
							<table width="100%" border="0" class="display dataTable">
								<thead>
									<tr>
										<th class="ui-state-default">Detalle</th>
										<th class="ui-state-default">TTL</th>
										<th class="ui-state-default">%</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($Colonia['filas'] as $row)
									{
										echo "<tr>";
										echo '<td>'.$row['colonia'].'</td>';
										echo '<td>'.$row['cuenta'].'</td>';
										echo '<td>'.  round(100 * $row['cuenta'] / $Colonia['ttl'], 2)   .' % </td>';
										echo " </tr>";
									}
									?>
								</tbody>
								<tfoot>
									<tr>
										<td><b>Total</b></td>
										<td><b><?php echo $Colonia['ttl']?> </b></td>
										<td><b><?php echo $Colonia['porcentajeTotal'] ?> % </b></td>
									</tr>
								</tfoot>
							</table>
						</td>
					</tr>
				</table>


				<!-- empresa referente -->
				<h1>Empresa referente</h1>
				<table width="100%">
					<tr>
						<td valign="top">

							<div id="chartEmpresa" align="left">
								<!-- Aqui en este div va a aparecer el grafico -->
							</div> <script language="JavaScript">					
			var chartEmpresa = new FusionCharts("/graficas/Charts/Pie2D.swf", "chartEmpresa", "600", "400", "0", "1");	
			 <?php $xml = "<chart caption='Empresa' showPercentageValues='1' showBorder ='1' >"; 
			 	foreach ($Empresa['filas'] as $row)
				{
					$xml.= "<set label='".$row['empresa']."' value='" .$row['cuenta']."' />";
				}
				$xml.= "</chart>"; 
				?>	   			
			chartEmpresa.setDataXML("<?php echo $xml ?>");
			chartEmpresa.render("chartEmpresa");
		</script>
						</td>
						<td width="80%" valign="top">
							<table width="100%" border="0" class="display dataTable">
								<thead>
									<tr>
										<th class="ui-state-default">Detalle</th>
										<th class="ui-state-default">TTL</th>
										<th class="ui-state-default">%</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($Empresa['filas'] as $row)
									{
										echo "<tr>";
										echo '<td>'.$row['empresa'].'</td>';
										echo '<td>'.$row['cuenta'].'</td>';
										echo '<td>'.  round(100 * $row['cuenta'] / $Empresa['ttl'], 2)   .' % </td>';
										echo " </tr>";
									}
									?>
								</tbody>
								<tfoot>
									<tr>
										<td><b>Total</b></td>
										<td><b><?php echo $Empresa['ttl']?> </b></td>
										<td><b><?php echo $Empresa['porcentajeTotal'] ?> % </b></td>
									</tr>
								</tfoot>
							</table>
						</td>
					</tr>
				</table>

			</div>




			<div id="servicio-3">
				<h1>Paquetes</h1>
				<div id="chartServicio" style="float: left;">
					<!-- Aqui en este div va a aparecer el grafico -->
				</div>

				<script language="JavaScript">					
			var chartServicio = new FusionCharts("/graficas/Charts/Column2D.swf", "chartServicio", "650", "600", "0", "1");	
			 <?php $xml = "<chart caption='Paquete' yAxisName='Socios' xAxisName='Paquetes' showPercentageValues='1' showBorder ='1'  useRoundEdges = '1' showLabels='0' >"; 
			 	foreach ($Servicio['filas'] as $row)
				{
					$xml.= "<set label='".$row['paquete']."' value='" .$row['cuenta']."' />";
				}
				$xml.= "</chart>"; 
				?>	   			
			chartServicio.setDataXML("<?php echo $xml ?>");
			chartServicio.render("chartServicio");
		</script>

				<div id="gridDiv" style="float: right;">The grid will appear within
					this DIV.</div>
				<script type="text/javascript">
	      var myGrid = new FusionCharts("/graficas/Charts/SSGrid.swf", "myGrid1", "300", "600", "0", "0");
	      myGrid.setDataXML("<?php echo $xml ?>");
	      myGrid.render("gridDiv");
	   </script>


				<table width="100%" border="0" class="display dataTable">
					<thead>
						<tr>
							<th class="ui-state-default">Detalle</th>
							<th class="ui-state-default">TTL</th>
							<th class="ui-state-default">%</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($Servicio['filas'] as $row)
						{
							echo "<tr>";
				echo '<td>'.$row['paquete'].'</td>';
				echo '<td>'.$row['cuenta'].'</td>';	
				echo '<td>'.  round(100 * $row['cuenta'] / $Servicio['ttl'], 2)   .' % </td>';			
				echo " </tr>";
			}
		?>
					</tbody>
					<tfoot>
						<tr>
							<td><b>Total</b></td>
							<td><b><?php echo $Servicio['ttl']?> </b></td>
							<td><b><?php echo $Servicio['porcentajeTotal'] ?> % </b></td>
						</tr>
					</tfoot>
				</table>
			</div>




		</div>
	</div>
</form>
