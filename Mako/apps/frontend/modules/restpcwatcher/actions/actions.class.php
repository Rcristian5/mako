<?php


class restpcwatcherActions extends sfActions
{
    const LOG = '/var/log/mako/rest-sesiones-pc.log';

    /**
     * Executes index action
     *
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request)
    {
        $this->getResponse()->setContentType('application/json');
        $this->getResponse()->setHttpHeader(
            'Access-Control-Allow-Origin', "*"
        );
        $this->getResponse()->setHttpHeader(
            'Access-Control-Allow-Methods',"GET, POST, OPTIONS, PUT, DELETE"
        );
        $this->getResponse()->setHttpHeader(
            'Access-Control-Allow-Headers',"Origin, X-Requested-With, Content-Type, Accept"
        );

        $usernames = new ArrayObject(
            explode(',', $request->getParameter('username'))
        );
        $ips = new ArrayObject(
            explode(',', $request->getParameter('ip'))
        );
        $so         = $request->getParameter('so');
        $centro_id  = sfConfig::get('app_centro_actual_id');


        $response = $this->createArryResponse();
        $user = $this->validateUser($usernames);
        $ip = $this->getValidIp($ips);

        if (in_array('no_user_found', $usernames->getArrayCopy()) and '' == $user) {
            $this->getResponse()->setStatusCode(404);
            $response->offsetSet('code', 404);
            $response->offsetSet('message', 'Error');
            $response->offsetSet('username', 'no_user_found');

            return $this->renderText(
                json_encode(
                    $response->getArrayCopy()
                )
            );
        }

        if ('' == $ip ) {
            $this->getResponse()->setStatusCode(404);
            $response->offsetSet('code', 404);
            $response->offsetSet('message', 'Error');
            $response->offsetSet('username',$user );

            return $this->renderText(
                json_encode(
                    [
                        $response->getArrayCopy()
                    ]
                )
            );
        }

        $criteria = new Criteria();

        $criteria->add(SesionPeer::IP, $ip);

        $session = SesionPeer::doSelectOne($criteria);

        switch(true){
            case is_null($session):
                $this->pcNoSession($user, $ip);
                break;
            case !is_null($session):
                $this->pcSession($session, $user, $ip);
                break;
        }


        $response->offsetSet('code', 200);
        $response->offsetSet('message', 'Sucess');
        $response->offsetSet('username', $user);
        $response->offsetSet('ip', $ip);

        return $this->renderText(
            json_encode(
                $response->getArrayCopy()
            )
        );
    }

    /**
     * @return ArrayObject
     */
    private function createArryResponse(){
        $response = new ArrayObject();

        $response->offsetSet('code', 200);
        $response->offsetSet('message', '');
        $response->offsetSet('status', true);
        $response->offsetSet('username', '');
        $response->offsetSet('ip', '');

        return $response;
    }

    /**
     * @param ArrayAccess $ips
     * @return string
     */
    private function getValidIp(ArrayAccess $ips){
        $ipValida = '';

        foreach($ips as $ip){
            $criteria = new Criteria();
            $criteria->add(ComputadoraPeer::IP, $ip);
            if(!is_null(ComputadoraPeer::doSelectOne($criteria))){
                $ipValida = $ip;
                break;
            }
            $criteria = null;
        }

        return $ipValida;
    }

    /**
     * @param ArrayObject $users
     */
    private function validateUser(ArrayAccess $users){
        $usernameValid = '';

        foreach($users as $user){
            $criteria = new Criteria();

            $criteria->add(SocioPeer::USUARIO, $user);

            if(!is_null(SocioPeer::doSelectOne($criteria))){
                $usernameValid = $user;
                break;
            }
            $criteria = null;
        }

        return $usernameValid;
    }

    private function pcSession(Persistent $sessionModel, $currenUser, $ip){
        $time = new DateTime();

        if($sessionModel->getUsuario() !== $currenUser){
            $sesionHistoricoModel = static::eliminaSesion_agente(
                $sessionModel, static::obtenerSegundosACompensar($sessionModel)
            );

            OperacionLDAP::socioLogout($sesionHistoricoModel);
            $socio = SocioPeer::porNombreUsuario($currenUser);
            $computadoraModel = ComputadoraPeer::getPcPorIp($ip);

            $newSession = SesionPeer::crearSesion(
                $socio, $computadoraModel, $time->format("Y-m-d H:i:s"),
                sfConfig::get('app_centro_actual_id')
            );

            OperacionLDAP::socioLogin($newSession);
        }else{
            $updateSession = SesionPeer::actualizaSesion($ip);
        }
    }

    private function pcNoSession($currentUser, $currentIp){
        $time = new DateTime();

        $computadoraModel = ComputadoraPeer::getPcPorIp($currentIp);
        $socio = SocioPeer::porNombreUsuario($currentUser);

        OperacionLDAP::socioLogin(
            SesionPeer::crearSesion(
                $socio, $computadoraModel, $time->format("Y-m-d H:i:s"),
                sfConfig::get('app_centro_actual_id')
            )
        );
    }

    /**
     * @deprecated
     *
     * Actualiza sesion en BD basado en tiempo transcurrido desde la ultima peticion.
     * @param $configuration
     */
    private function actualizaSesionesActivas($dispatcher) {

        $datetimeactual = new DateTime('NOW');

        $c = new Criteria();
        $sesiones = SesionPeer::doSelect($c);

        foreach ($sesiones as $s) {

            $datetimebd = new DateTime($s->getUptime());
            $interval = $datetimeactual->diff($datetimebd);

            if ($interval->i >= 5){
                $ajuste = self::obtenerSegundosACompensar($s);

                $h = self::eliminaSesion_agente($s, $ajuste);
                $dispatcher->notify(new sfEvent($this, 'socio.logout', array('sesion_historico' => $h)));
            }
        }

    }

    /**
     * Obtiene segundos compensacion para el usuario.
     * @param $configuration
     */
    private function obtenerSegundosACompensar(Persistent $sesion) {
        $dateahora = new DateTime('NOW');
        $dateuptime = new DateTime($sesion->getUptime());
        $diferenciaensegundos = $dateuptime->getTimestamp() - $dateahora->getTimestamp();

        return $diferenciaensegundos;

    }

    /**
     * @deprecated
     * Obtiene el usuario que actualmente utiliza la pc.
     * @param $configuration
     */
    private function obtenerUsario($usernames) {
        $usuario = false;

        foreach($usernames as $username){
            $criteria = new Criteria();
            $criteria->add(UsuarioPeer::USUARIO, $username);
            $criteria = null;
        }

        foreach ($usernames as $s) {
            $c = new Criteria();
            $c->add(UsuarioPeer::USUARIO, $s);
            $ustaff = UsuarioPeer::doSelectOne($c);
            if ($ustaff != null) {
                exit();
            }

            $o = new Criteria();
            $o->add(SocioPeer::USUARIO, $s);
            $socio = SocioPeer::doSelectOne($o);

            if ($socio != null) {

                $usuario = $socio->getUsuario();

            }
        }
        return $usuario;
    }

    /**
     * @deprecated
     * Obtiene la ip con la cual esta actualmente registrada la pc.
     * @param $configuration
     */
    private function obtenerIp($ips) {
        $ip = false;

        foreach ($ips as $i) {
            $o = new Criteria();
            $o->add(ComputadoraPeer::IP, $i);
            $computadora = ComputadoraPeer::doSelectOne($o);

            if ($computadora != null) {
                $ip = $computadora->getIp();
            }
        }
        return $ip;
    }

    /**
     * Elimina la sesión y genera una entrada para el histórico además de actualizar los tiempos de sesión.
     * @param Sesion $sesion
     * @param int $ajuste Segundos de ajuste para compensar tiempo.
     * @return SesionHistorico Regresa el registro de sesion histórico que corresponde a la sesion eliminada
     */
    private function eliminaSesion_agente($sesion, $ajuste)
    {
        $tlogin = $sesion->getFechaLogin(null);
        //Generamos el historico de sesion
        $h = new SesionHistorico();
        $h->setCentroId($sesion->getCentroId());
        $h->setSocioId($sesion->getSocioId());
        $h->setUsuario($sesion->getUsuario());
        $h->setIp($sesion->getIp());
        $h->setComputadoraId($sesion->getComputadoraId());
        $h->setTipoId($sesion->getTipoId());
        $h->setFechaLogin($sesion->getFechaLogin());
        $ahora = time() + $ajuste;

        $transcurrido = abs($ahora - $tlogin->format('U'));

        $h->setFechaLogout($ahora);
        $h->setOcupados($transcurrido);
        $h->setSaldoInicial($sesion->getSaldo());

        //Vemos el tipo de sesion que se trata para restar o no su saldo
        $tipo_id = $sesion->getTipoId();

        if($tipo_id == sfConfig::get('app_sesion_default_id') || $tipo_id == sfConfig::get('app_sesion_multisesion_id'))
        {
            //actualizamos el saldo de tiempo del socio
            $socio = $sesion->getSocio();
            $nsaldo = ($socio->getSaldo() - $transcurrido);
            if($nsaldo < 0 ) $nsaldo = 0;
            $socio->setSaldo($nsaldo);
            $socio->save();
            $h->setSaldoFinal($nsaldo);
        } else {
            //Si la sesion es de no consumo el saldo final queda igual
            $h->setSaldoFinal($sesion->getSaldo());
        }

        try {
            $h->save();
            $sesion->delete();
        } catch(Exception $ex) {
            $sesion->delete();
        }

        return $h;
    }
}
