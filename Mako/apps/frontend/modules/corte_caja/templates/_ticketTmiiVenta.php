<?php
/**
 * Ticket de venta
 * @version: $Id: _ticketChVenta.php,v 1.1 2010/11/22 07:43:55 eorozco Exp $
 */
use_helper('Facturas');
$productos = $orden->getDetalleOrdens();
//TODO: sacar este código de la plantilla.

$tasa_orden = $tasas[$orden->getId()];

list($iva,$importe,$total,$totalPalabras) = desglosa_iva( $orden->getTotal(), $tasa_orden );
$cadp="";
$caf_prods="";
$tipo="";
foreach($productos as $p)
{
	$cla = array();
	$psplit = array();
	$cant = array();
	$pre = array();

	//Para impresoras de 40 caracteres de ancho.
	$car_nom_prod = sfConfig::get('app_cpl') - 16;
	$car_can = 4;
	$car_pre = 8;
	$tpl_tick = "%-".$car_nom_prod."s %".$car_can."s  %s";

	//usamos este hacksillo para cortar las descripciones largas de los productos en varios renglones
	$cla[] = $p->getProducto()->getCodigo();
	$cant[] = intval($p->getCantidad());
	$pre[] = '$'.sprintf("%".$car_pre."s",money_format('%(#1n',tsi($p->getSubtotal(),$tasa_orden)));
	$psplit=split('\|',wordwrap(fa($p->getProducto()->getNombre()),$car_nom_prod,'|'));
	foreach($psplit as $id=>$d)
	{
		$cantidad_ticket = (isset($cant[$id])) ? intval($cant[$id]) : '';

		$cadp .= sprintf($tpl_tick,$d,$cantidad_ticket,(isset($pre[$id]))? $pre[$id] : '' )."\n";
	}
	if($orden->getTipoId() == sfConfig::get('app_orden_venta_id') && $orden->getEsFactura())
	{
		$tipo = 'Se ha emitido FACTURA de esta venta.';
	}
	elseif($orden->getTipoId() == sfConfig::get('app_orden_venta_id') && $orden->getEstatusId() == sfConfig::get('app_estatus_cancelado_id'))
	{
		$tipo = 'VENTA CANCELADA.';
	}
	elseif($orden->getTipoId() == sfConfig::get('app_orden_venta_id') && !$orden->getEsFactura())
	{
		if(!$tasa_orden) $tipo = 'Venta al publico IVA TASA 0%';
		else $tipo = 'Venta al publico en general.';

	}
}
?>
<?php echo sfConfig::get('app_nombre_empresa')."\n" ?>
Folio:
<?php echo sprintf("%08d",$orden->getFolio()) ?>
-
<?php echo $orden->getCentro()->getNombre()."\n" ?>
Fecha:
<?php echo $orden->getCreatedAt()."\n" ?>
ID TS:
<?php echo $orden->getId()."\n" ?>
Usuario:
<?php echo $orden->getSocio()->getUsuario()."\n" ?>
Cred:
<?php echo $orden->getSocio()->getFolio()."\n" ?>
<?php echo $tipo."\n" ?>
---------------------------------------- Producto Cant. Subtotal
----------------------------------------
<?php echo $cadp."\n" ?>
<?php echo sprintf("%40s","TOTAL: $".sprintf("%8s",$total))."\n" ?>
<?php echo sprintf("%40s","Importe: $".sprintf("%8s",$importe))."\n" ?>
<?php echo sprintf("%40s","IVA: $".sprintf("%8s",$iva))."\n" ?>
<?php echo $totalPalabras."\n" ?>
----------------------------------------