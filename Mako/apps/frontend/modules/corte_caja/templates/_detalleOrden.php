<!-- 
/*
 * Componentes Bixit
 *
 * Copyright (c) 2009-2011 Bixit SA de CV
 * Bajo Licencia dual MIT (http://www.bixit.mx/legal/MIT-LICENSE.txt)
 * y GPL (http://www.bixit.mx/legal/GPL-LICENSE.txt).
 *
 */
 -->
<?php use_javascript('plugins/jquery.dataTables.js') ?>
<?php use_javascript('jquery.ui.js') ?>
<?php use_stylesheet('datatables.css') ?>
<script>
/*var lsoT;
$(document).ready(function() {

   lsoT = $('#detalle').dataTable({
		"bPaginate": false,
		"bLengthChange": false,
		"bFilter": false,
		"bInfo": false,
        "bJQueryUI": true
     });
});
*/
</script>

<?php use_stylesheet('./corte_caja/_detalleOrden.css')?>

<div class="ui-widget-content ui-corner-all sombra"
	style="padding: 5px;">
	<div class="ui-widget-header ui-corner-tl ui-corner-tr tssss"
		style="padding-left: 5px;">
		<h3>
			Orden
			<?php echo $folio ?>
		</h3>
	</div>

	<table id="detalle" width="100%">
		<thead>
			<tr>
				<!-- Reingenieria Mako C. ID 21. Responsable:  FE. Fecha: 21-01-2014.  Descripción del cambio: Se agrega etiqueta para tener un mejor detalle dela información.-->
				<!-- Reingenieria Mako C. ID 41. Responsable:  FE & ES. Fecha: 09-04-2014.  Descripción del cambio: Se agrega etiqueta para tener un mejor detalle dela información-->
				<th width="13%">Id Transacción</th>
				<!-- Fecha: 21-01-2014 - Fin.-->
				<th width="20%">Socio</th>
				<th width="29%">Producto</th>
				<th width="7%">Cantidad</th>
				<th width="11%">Precio unitario</th>
				<th width="11%">Descuento (%)</th>
				<th width="9%">Total ($)</th>
				<!-- Fecha: 09-04-2014 - Fin.-->
			</tr>
		</thead>

		<tbody>

			<?php  foreach ($Orden as $detalle): ?>
			<tr>
				<!-- Reingenieria Mako C. ID 21. Responsable:  FE. Fecha: 21-01-2014.  Descripción del cambio: Se agrega etiqueta para tener un mejor detalle dela información.-->
				<!-- Reingenieria Mako C. ID 41. Responsable:  FE & ES. Fecha: 09-04-2014.  Descripción del cambio: Se agrega etiqueta para tener un mejor detalle dela información-->
				<td><?php echo $detalle->getOrdenId();?></td>
				<!-- Fecha: 21-01-2014 - Fin.-->
				<td><?php echo $detalle->getSocio(); ?></td>
				<td><?php echo $detalle->getProducto();  ?></td>
				<td align="right"><?php echo $detalle->getCantidad();  ?></td>
				<td align="right"><?php echo $detalle->getPrecioLista();  ?></td>
				<td align="right"><?php echo  number_format($detalle->getDescuento(), 2)  ?>
				</td>
				<td align="right"><?php echo  $detalle->getSubtotal()  ?></td>
				<!-- Fecha: 09-04-2014 - Fin.-->
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</div>
