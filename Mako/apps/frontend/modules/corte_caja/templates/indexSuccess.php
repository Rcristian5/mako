<?php use_javascript('plugins/jquery.dataTables.js') ?>
<?php use_javascript('jquery.ui.js') ?>
<?php use_stylesheet('datatables.css') ?>
<script>
var lsC;
$(document).ready(function() {

   lsT = $('#lista-cortes').dataTable({
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": true,
        "bJQueryUI": true,
        "sPaginationType": "full_numbers"
     });
   
});
</script>

<div class="ui-widget-header ui-corner-top tsss" style="padding: 5px;">Historial
	de cortes de caja</div>

<div class="sombra ui-corner-bottom ">

	<div class="ui-widget-content ui-corner-bottom ts"
		style="padding: 5px;">


		<table width="100%" id="lista-cortes">
			<thead>
				<tr>
					<th>Centro</th>
					<th></th>
					<th>Caja inicial ($)</th>
					<th>Ventas ($)</th>
					<th>Salidas ($)</th>
					<th>Cancelado ($)</th>
					<th title="Ventas a público en general">VPG ($)</th>
					<th>Facturado ($)</th>
					<th>Conciliado ($)</th>
					<th>Caja final ($)</th>
					<th>Fecha Corte</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($Cortes as $Corte): ?>
				<tr>
					<td><?php echo $Corte->getCentro() ?></td>
					<td align="center"><a
						href="<?php echo url_for('corte_caja/show?id='.$Corte->getId()) ?>">
							<span class="ui-icon  ui-icon-circle-zoomin"></span>
					</a>
					</td>
					<td align="right"><?php echo $Corte->getFondoInicial() ?></td>
					<td align="right"><?php echo $Corte->getTotalVentas() ?></td>
					<td align="right"><?php echo $Corte->getTotalSalidas() ?></td>
					<td align="right"><?php echo $Corte->getTotalCancelaciones() ?></td>
					<td align="right"><?php echo $Corte->getTotalVpg() ?></td>
					<td align="right"><?php echo $Corte->getTotalFacturas() ?></td>
					<td align="right"><?php echo $Corte->getTotalConciliaciones() ?></td>
					<td align="right"><?php echo $Corte->getFondoCaja() ?></td>
					<td align="right"><?php echo $Corte->getCreatedAt() ?></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

	</div>
</div>
