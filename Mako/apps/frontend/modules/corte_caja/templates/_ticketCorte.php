<?php 
/**
 * Plantilla de ticket largo de corte.
 */
$r = array('á','é','í','ó','ú','Á','É','Í','Ó','Ú'); //Reemplazar
$p = array('a','e','i','o','u','A','E','I','O','U'); //Por
?>
<?php echo sfConfig::get('app_nombre_empresa')."\n" ?>
<?php echo sfConfig::get('app_centro_actual')."\n" ?>
Fecha: <?php echo $cc->getCreatedAt()."\n" ?>
Corte: <?php echo $cc->getId()."\n" ?>
CORTE DE CAJA
-----------------------------------------------
Folio  Tipo   IDC     Fecha   Hora   Total($)   
-----------------------------------------------
<?php foreach ($ords as $ord): ?>
<?php echo sprintf('%06d',$ord->getFolio()) ?> <?php echo str_pad(substr($ord->getTipoOrden()->getNombre(),0,6),6," ",STR_PAD_RIGHT) ?> <?php echo sprintf('%03s',$ord->getOperadorId())?> <?php echo $ord->getCreatedAt("%Y-%m-%d %H:%M")?> <?php echo str_pad('$ '.$ord->getTotal(),12," ",STR_PAD_LEFT)."\n"?>
<?php endforeach; ?>
-----------------------------------------------
<?php echo str_pad("Fondo inicial",35," ",STR_PAD_LEFT) . str_pad(sprintf('$ %01.2f',$cc->getFondoInicial()),12," ",STR_PAD_LEFT)."\n" ?>
<?php echo str_pad("Total ventas",35," ",STR_PAD_LEFT) . str_pad(sprintf('$ %01.2f',$cc->getTotalVentas()),12," ",STR_PAD_LEFT)."\n" ?>
<?php echo str_pad("Total cancelaciones",35," ",STR_PAD_LEFT) . str_pad(sprintf('$ %01.2f',$cc->getTotalCancelaciones()),12," ",STR_PAD_LEFT)."\n" ?>
<?php echo str_pad("Total venta pub. gral.",35," ",STR_PAD_LEFT) . str_pad(sprintf('$%01.2f',$cc->getTotalVpg()),12," ",STR_PAD_LEFT)."\n" ?>
<?php echo str_pad("Total venta pub. gral. I.V.A. 0%",35," ",STR_PAD_LEFT) . str_pad(sprintf('$%01.2f',$total_tasa0),12," ",STR_PAD_LEFT)."\n" ?>
<?php echo str_pad("Total facturas",35," ",STR_PAD_LEFT) . str_pad(sprintf('$ %01.2f',$cc->getTotalFacturas()),12," ",STR_PAD_LEFT)."\n" ?>
<?php echo str_pad("Total salidas",35," ",STR_PAD_LEFT) . str_pad(sprintf('$ %01.2f',$cc->getTotalSalidas()),12," ",STR_PAD_LEFT)."\n" ?>
<?php echo str_pad("Total conciliaciones",35," ",STR_PAD_LEFT) . str_pad(sprintf('$ %01.2f',$cc->getTotalConciliaciones()),12," ",STR_PAD_LEFT)."\n" ?>
<?php echo str_pad("Fondo de caja",35," ",STR_PAD_LEFT) . str_pad(sprintf('$ %01.2f',$cc->getFondoCaja()),12," ",STR_PAD_LEFT)."\n" ?>
-----------------------------------------------
<?php foreach ($ventas_categoria as $venta): ?>
<?php echo str_pad(str_replace($r,$p,$venta['nombre']),35," ",STR_PAD_LEFT) . str_pad(sprintf('$ %01.2f',$venta['sum']),12," ",STR_PAD_LEFT)."\n" ?>
<?php endforeach; ?>
d2
