<?php
/*
 * Componentes Bixit
*
* Copyright (c) 2009-2011 Bixit SA de CV
* Bajo Licencia dual MIT (http://www.bixit.mx/legal/MIT-LICENSE.txt)
* y GPL (http://www.bixit.mx/legal/GPL-LICENSE.txt).
*
*/
?>
<?php use_javascript('plugins/jquery.dataTables.js') ?>
<?php use_javascript('plugins/dataTables/TableTools.js') ?>
<?php use_javascript('plugins/dataTables/ZeroClipboard.js') ?>
<?php use_javascript('jquery.ui.js') ?>
<?php use_stylesheet('datatables.css') ?>
<?php use_stylesheet('TableTools.css') ?>
<script>
var lsoT;
$(document).ready(function() {
   lsoT = $('#lista-ordenes').dataTable({
        "bPaginate": false,
        "bLengthChange": false,
        "bFilter": false,
        "bInfo": false,
        "bJQueryUI": true,
        "sDom": '<"top"T>t'
    });

    $(".TableTools_print").hide();
});
</script>





<!-- Ver detalle socio -->
<div id="verDetalle" style="display: none"></div>
<!-- /Ver detalle socio -->

<script>
    //Ver detalle socio
    var $wcm= $("#verDetalle").dialog({
        modal: true,
        autoOpen: false,
        position: ['center','center'],
        title:  'Ver detalle',
        //setter
        height: 350,
        width: 800,
        buttons: {
            'Cerrar': function() {
                $(this).dialog('close');
            }
        }
    });


    function verDetalle(idOrden){
        var UrlVerDetalle = "<?php echo url_for('corte_caja/verDetalleOrden') ?>";

        $("#verDetalle").load(UrlVerDetalle, {
            idOrden: idOrden
        });

         $wcm.dialog('open');
    };
</script>

<?php use_stylesheet('./corte_caja/showSuccess.css')?>

<div
    class="ui-widget-content ui-corner-all sombra" style="padding: 5px;">
    <div class="ui-widget-header ui-corner-tl ui-corner-tr tssss"
        style="padding-left: 5px;">
        <h3>
            Corte de movimientos al
            <?php echo $cc->getCreatedAt() ?>
        </h3>
    </div>
    <table id="lista-ordenes" width="90%">
        <thead>
            <tr>
                <th></th>
                <th>Comprobante</th>
                <th>Folio</th>
                <th>Operación</th>
                <th>Fecha y hora</th>
                <th>Realizó</th>
                <th>Total</th>
            </tr>
        </thead>

        <tbody>
            <?php $c = 1; foreach ($ords as $ord): ?>

            <tr class="<?php echo ($c%2)?'odd':'even'?>">
                <td><?php echo $c; $c++; ?></td>
                <td><?php echo ($ord->getEsFactura())? 'Factura':'Ticket' ?></td>
                <td nowrap="nowrap">
                    <?php
                        if($ord->getTipoId() != 2) {
                            echo '<a href="javascript:void(0);" onClick="verDetalle('. $ord->getId() .')">';
                                echo sprintf('%06s',$ord->getFolio());
                                //echo '<span class="ui-icon  ui-icon-circle-zoomin"></span>';
                            echo '</a>';
                        }else {
                            echo sprintf('%06s',$ord->getFolio());
                        }
                    ?>
                </td>
                <td class="<?php echo ($ord->getTotal() < 0)? 'negativo':'' ?>"><?php echo $ord->getTipoOrden()->getNombre() ?>
                </td>
                <td><?php echo $ord->getCreatedAt() ?></td>
                <td><?php echo $ord->getUsuario() ?></td>
                <td class="<?php echo ($ord->getTotal() < 0)? 'negativo':'' ?>"
                    align="right"><?php echo sprintf('%01.2f',$ord->getTotal()) ?></td>
            </tr>

            <?php endforeach; ?>
        </tbody>

        <tfoot>
            <tr>
                <td colspan="5"></td>
                <td align="right" class="subrayado">Fondo inicial:</td>
                <td align="right"
                    class="subrayado <?php echo ($cc->getfondoInicial() < 0)? 'negativo':'' ?>"><?php echo sprintf('%01.2f',$cc->getFondoInicial()) ?>
                </td>
            </tr>
            <tr>
                <td colspan="5"></td>
                <td align="right">Total ventas:</td>
                <td align="right"
                    class="<?php echo ($cc->getTotalVentas() < 0)? 'negativo':'' ?>"><?php echo sprintf('%01.2f',$cc->getTotalVentas()) ?>
                </td>
            </tr>
            <tr>
                <td colspan="5"></td>
                <td align="right" class="negativo">Total cancelaciones:</td>
                <td align="right"
                    class="<?php echo ($cc->getTotalCancelaciones() < 0)? 'negativo':'' ?>"><?php echo sprintf('%01.2f',$cc->getTotalCancelaciones()) ?>
                </td>
            </tr>

            <tr>
                <td colspan="5"></td>
                <td align="right">Ventas público en general (I.V.A. <?php echo intval($cc->getCentro()->getIva()) ?>
                    %):
                </td>
                <td align="right"
                    class="<?php echo ($cc->getTotalVpg() < 0)? 'negativo':'' ?>"><?php echo sprintf('%01.2f',$cc->getTotalVpg()) ?>
                </td>
            </tr>
            <tr>
                <td colspan="5"></td>
                <td align="right">Ventas público en general (I.V.A. 0 %):</td>
                <td align="right" class=""><?php echo sprintf('%01.2f',$total_tasa0) ?>
                </td>
            </tr>
            <tr>
                <td colspan="5"></td>
                <td align="right">Total facturas:</td>
                <td align="right"
                    class="<?php echo ($cc->getTotalFacturas() < 0)? 'negativo':'' ?>"><?php echo sprintf('%01.2f',$cc->getTotalFacturas()) ?>
                </td>
            </tr>
            <tr>
                <td colspan="5"></td>
                <td align="right" class="negativo subrayado">Total salidas:</td>
                <td align="right"
                    class="subrayado <?php echo ($cc->getTotalSalidas() < 0)? 'negativo':'' ?>"><?php echo sprintf('%01.2f',$cc->getTotalSalidas()) ?>
                </td>
            </tr>
            <tr>
                <td colspan="5"></td>
                <td align="right">Total conciliaciones:</td>
                <td align="right"
                    class="<?php echo ($cc->getTotalConciliaciones() < 0)? 'negativo':'' ?>"><?php echo sprintf('%01.2f',$cc->getTotalConciliaciones()) ?>
                </td>
            </tr>
            <tr>
                <td colspan="5"></td>
                <td align="right">Fondo de caja:</td>
                <td align="right"
                    class="<?php echo ($cc->getFondoCaja() < 0)? 'negativo':'' ?>"><?php echo sprintf('%01.2f',$cc->getFondoCaja()) ?>
                </td>
            </tr>
            <!-- Reingenieria Mako C. ID 17. Responsable: ES & FE. Fecha: 15-01-2014. Descripción del cambio: Se añaden etiquetas 'faltante | excedente' cuando exista descuadre-->
            <?php
                $texto = '';

                //fe=faltante o excedente
                $fe = $cc->getTotalVentas() - abs($cc->getTotalCancelaciones()) - abs($cc->getTotalSalidas());

                if ($fe > 0) {
                    $texto = 'Excedente:';

                } else {
                    $texto = 'Faltante:';
                }
            ?>
            <tr>
                <td colspan="5"></td>
                <td align="right" class="<?php echo ($fe < 0)? 'negativo':'' ?>"><?php echo $texto;?>
                </td>
                <td align="right" class="<?php echo ($fe < 0)? 'negativo':'' ?>"><?php echo sprintf('%01.2f',$fe) ?>
                </td>
            </tr>
            <!-- Fecha: 15-01-2014. Fin-->
        </tfoot>

    </table>
    <!-- Reingenieria Mako C. ID 42. Responsable:  FE. Fecha: 07-04-2014.  Descripción del cambio: Se agrega funcionalidad para regresar a corte de caja -->
    <!-- Cambio sugerido por Isabel Baxim responsable:IMB 16/10/2014
    <div>
        <hr>
        <a href="<?php echo url_for('corte_caja/index')?>">Regresar a lista de
            cortes de caja</a>
    </div>
    -->
    <!-- Fecha: 07-04-2014 Fin -->
</div>

<?php  if($botonesImpresion == True){
    include_partial('botonesImpresion', array('cc' => $cc));
}
?>
