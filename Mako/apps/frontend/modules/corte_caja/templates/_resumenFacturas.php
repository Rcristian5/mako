<?php 
$facturas	=$datos->getRawValue();
$vttotales	=array("subtotal"=>0, "retencion"=>0, "total"=>0);
$vtdatos	=array();
$vtkeys		=array("corte", "libros", "socio");



foreach ($facturas as $factura){
	$fechahora	=	Comun::getFechaAndHoraIntoArray($factura["fecha"]);
	$key	=	$fechahora["fecha"];
		
	foreach ($vtkeys as $keytype){  				// en caso de que solo venga factura de corte y no de libros inicializamos todo :)
		if(!array_key_exists($keytype,$vtdatos[$key])){
			$vtdatos[$key][$keytype]["folio"]="";
		}
	}
		
	foreach ($vttotales as $totalkey=>$valor){ 		//Seteamos a cero Los totales
		if(!array_key_exists($totalkey, $vtdatos[$key])){
			$vtdatos[$key][$totalkey]=0;
		}
	}
		
	$vtdatos[$key][$factura["socio"]]["folio"].=($vtdatos[$key][$factura["socio"]]["folio"]=="")?"":", ";
	$vtdatos[$key][$factura["socio"]]["folio"].= '<span style="cursor:pointer; font-weight:bold;" alt="'.$factura["id"].'" class="detalleFactura">' .$factura["folio"] . '</span>';
	foreach ($vttotales as $totalkey=>$valor){		//Seteamos totales.
		$vtdatos[$key][$totalkey]	+=$factura[$totalkey];
		$vttotales[$totalkey]		+=$factura[$totalkey];
	}
		
}



?>
<br />
<br />
<h4>Resumen de Facturas</h4>
<table id="lista-facturacion" class="dataTables_wrapper" width="90%">

	<thead>

		<tr>
			<th class="ui-state-default">Fecha</th>
			<th class="ui-state-default">No. Factura</th>
			<th class="ui-state-default">No. Factura Libros</th>
			<th class="ui-state-default">No. Factura(s) Socio</th>
			<th class="derecha ui-state-default">Subtotal</th>
			<th class="derecha ui-state-default">IVA</th>
			<th class="derecha ui-state-default">Total</th>
		</tr>
	</thead>


	<tbody>

		<?php $n=0;	foreach ($vtdatos as $key=>$factura): $n++;	?>
		<tr class="<?= ($n%2==0)?"odd":"even"; ?>">
			<td><?= $key; ?></td>
			<td><?= $factura["corte"]["folio"]; ?></td>
			<td><?= $factura["libros"]["folio"]; ?></td>
			<td><?= $factura["socio"]["folio"]; ?></td>
			<?php foreach ($vttotales as $keytot=>$valor): ?>
			<td class="derecha"><?= number_format( $factura[$keytot],2,".",","); ?>
			</td>
			<?php endforeach; ?>
		</tr>

		<?php endforeach; ?>

	


	
	<tfoot>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td><strong>Totales:</strong></td>
			<?php foreach ($vttotales as $key=>$valor): ?>
			<td class="derecha subrayado"><strong><?= number_format($vttotales[$key],2,".",","); ?>
			</strong></td>
			<?php endforeach; ?>
		</tr>
	</tfoot>
	</tbody>

</table>
<script type="text/javascript">
			  
<!--

//-->

$('#lista-facturacion').dataTable({
	"bPaginate"		: false,
	"bLengthChange"	: false,
	"bFilter"		: false,
	"bInfo"			: false,
    "bJQueryUI"		: true,
    "sDom"			: '<"top"T>t'
 });


$(".TableTools_print").hide();
$(".TableTools_csv").hide();
$(".TableTools_clipboard").hide();

</script>
