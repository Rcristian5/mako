<?php  // Reingenieria Mako C. ID 13. Responsable: FE. Fecha: 10-03-2014. Descripción del cambio: Se mejora la funcionalidad de corte de caja.?>
<script>
function detalle(id){
    $.ajax({ 
        url: "<?php echo url_for('corte_caja/imprimir') ?>/tipo/detalle/id/"+id,
        success: function(){
            $('#detalle').hide();
            $('#imprimeCargando').show();
            $("#imprimeCargando").html("Imprimiendo Detalle de ventas ... <br><img src='/imgs/ajax-loader.gif'>");
            alert ('Esperar que se termine de imprimir.');
        },
        complete: function(){
            $('#imprimeCargando').hide();
            $('#resumen1').show();
        },
        error: function(){
            alert ('Error al imprimir.');
        }
    });
}

function resumen1(id){
    $.ajax({ 
        url: "<?php echo url_for('corte_caja/imprimir') ?>/tipo/resumen/id/"+id, 
        success: function(){
            $('#resumen1').hide();
           $('#imprimeCargando').show();
            $("#imprimeCargando").html("Imprimiendo Resumen de ventas 1...<br><img src='/imgs/ajax-loader.gif'>");
            alert ('Esperar que se termine de imprimir.');
        },
        complete: function(){
            $('#imprimeCargando').hide();
            $('#resumen2').show();
        },
        error: function(){
            alert ('Error al imprimir.');
        }
    });
}

function resumen2(id){
    $.ajax({ 
        url: "<?php echo url_for('corte_caja/imprimir') ?>/tipo/resumen/id/"+id, 
        success: function(){
            $('#resumen2').hide();
            $('#imprimeCargando').show();
            $("#imprimeCargando").html("Imprimiendo Resumen de ventas 2...<br><img src='/imgs/ajax-loader.gif'>");
            alert ('Esperar que se termine de imprimir.');
        },
        complete: function(){
            $('#imprimeCargando').hide();
            $('#factura').show();
        },
        error: function(){
            alert ('Error al imprimir.');
        }
    });
}

function factura(id){
    $.ajax({ 
        url: "<?php echo url_for('corte_caja/imprimir') ?>/tipo/factura/id/"+id, 
        success: function(){
            $('#factura').hide();
            $('#imprimeCargando').show();
            $("#imprimeCargando").html("Imprimiendo Factura de corte...<br><img src='/imgs/ajax-loader.gif'>");
            alert ('Esperar que se termine de imprimir.');
        },
        complete: function(){
            $('#imprimeCargando').hide();
            $('#factura').hide();
        },
        error: function(){
            alert ('Error al imprimir.');
        }
    });
}
</script>

<div id="imprimeCargando" style="display: none;">
	Imprimiendo ...<br> <img src="/imgs/ajax-loader.gif">
</div>

<?php if($cc->getTotalVpg()>0){ ?>
<input
	name="" type="button" onClick="detalle(<?php echo $cc->getId()?>)"
	id="detalle" value="Imprimir Detalle de ventas">
<input
	name="" type="button" onClick="resumen1(<?php echo $cc->getId()?>)"
	id="resumen1" value="Imprimir Resumen de ventas 1"
	style="display: none">
<input
	name="" type="button" onClick="resumen2(<?php echo $cc->getId()?>)"
	id="resumen2" value="Imprimir Resumen de ventas 2"
	style="display: none">
<input
	name="" type="button" onClick="factura(<?php echo $cc->getId()?>)"
	id="factura" value="Imprimir Factura de corte" style="display: none">
<?php }elseif($cc->getTotalVpg()==0){
        error_log("Cambio Isaed");?>

<input
    name="" type="button" onClick="detalle(<?php echo $cc->getId()?>)"
    id="detalle" value="Imprimir Detalle de ventas">

<input
	name="" type="button" onClick="resumen1(<?php echo $cc->getId()?>)"
	id="resumen1" value="Imprimir Resumen de ventas 1"
	style="display: line">
<input
	name="" type="button" onClick="resumen2(<?php echo $cc->getId()?>)"
	id="resumen2" value="Imprimir Resumen de ventas 2"
	style="display: none">
    
<?php } else{?>
<input
	name="" type="button" onClick="detalle(<?php echo $cc->getId()?>)"
	id="detalle" value="Imprimir Detalle de ventas">
<input
	name="" type="button" onClick="resumen1(<?php echo $cc->getId()?>)"
	id="resumen1" value="Imprimir Resumen de ventas 1"
	style="display: none">
<input
	name="" type="button" onClick="resumen2(<?php echo $cc->getId()?>)"
	id="resumen2" value="Imprimir Resumen de ventas 2"
	style="display: none">
<?php } ?>
<?php  // Fecha: 10-03-2014. Fin?>


