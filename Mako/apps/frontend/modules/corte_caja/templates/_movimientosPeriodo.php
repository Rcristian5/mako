<?php use_stylesheet('./corte_caja/_movimientosPeriodo.css')?>

<?php 
$datos			=$datos->getRawValue();
$vordenamiento	="fecha";
$hordenamiento	="nombre";
$vtkeys			=array();
$vttotales		=array("cantidad"=>array(), "subtotal"=>array(), "descuento"=>array());
$vtdatos		=array();

foreach ($datos as $fila){

	foreach ($vttotales as $key=>$valor){
		if(!array_key_exists($key, $vtdatos[$fila[$vordenamiento]][$fila[$hordenamiento]])){
			$vtdatos[$fila[$vordenamiento]][$fila[$hordenamiento]][$key]=0;
		}
		$vtdatos[$fila[$vordenamiento]][$fila[$hordenamiento]][$key]+=$fila[$key];
		if(!array_key_exists($fila[$hordenamiento], $vttotales[$key])){
			$vttotales[$key][$fila[$hordenamiento]]=0;
		}
		$vttotales[$key][$fila[$hordenamiento]]+=$fila[$key];
	}
	$vtkeys[$fila[$hordenamiento]]=0;
}


?>
<h4>Movimientos por Periodo</h4>
<table id="lista-movimientos" style="width: 90%;">
	<thead>
		<tr>
			<th><?= $vordenamiento; ?></th>
			<?php foreach ($vtkeys as $key=>$valor): ?>
			<th class="derecha"><?= $key; ?></th>
			<?php endforeach;?>
		</tr>
	</thead>
	<tbody>
		<?php $n=0; foreach ($vtdatos as $key=>$dato): $n++; ?>
		<tr class="<?= ($n%2==0)?"odd":"even";?>">
			<td><?= $key;  ?></td>
			<?php foreach ($vtkeys as $hkey=>$valor): ?>
			<td class="derecha"><?= number_format((array_key_exists($hkey,$dato))?$dato[$hkey]["subtotal"]:0,2,".", ","); ?>
			</td>
			<?php endforeach; ?>
		</tr>
		<?php endforeach; ?>
	

	<?php  
	$n			=0;
	$subtotal	=0;
	$impuesto	=0;
	$total		=0;
	foreach ($vtkeys as $key=>$valor):
	$tasa		=($key=="Libros")?0:$iva;
	$total		=$vttotales["subtotal"][$key];
	$piva		=$tasa/100+1;
	$subtotal	=$total/$piva;
	$impuesto	=$total-$subtotal;
	$vttotales["subtotal"][$key]=$subtotal;
	$vttotales["iva"][$key]=$impuesto;
	$vttotales["total"][$key]=$total;


	endforeach;

	$vtaux=array("Subtotal"=>array("tipo"=>"subtotal", "valor"=>0), "IVA"=>array("tipo"=> "iva", "valor"=>0), "Total"=>array("tipo"=>"total","valor"=>0));

	?>


		<?php foreach ($vtaux as $title=>$tipo): ?>
		<tr>
			<td><strong><?= $title; ?>:</strong></td> <?php  

			foreach ($vtkeys as $key=>$valor):
			$vtaux[$title]["valor"]+=number_format($vttotales[$tipo["tipo"]][$key],2,".","");
			?>
			
			<td class="derecha subrayado"><strong>$<?= number_format($vttotales[$tipo["tipo"]][$key],2,".", ","); ?>
			</strong></td>
			<?php
			endforeach;
			?>
		</tr>
		<?php endforeach; ?>

	</tbody>
	<tfoot>
		<tr>
			<?php for($n=0; $n<(count($vtkeys)-1); $n++){
				?>
			<td>&nbsp;</td>
			<?php
				
				} ?>
			<td><h3>&nbsp;</h3></td>
			<td><h3>&nbsp;</h3></td>
		</tr>

		<tr>
			<?php for($n=0; $n<(count($vtkeys)-1); $n++){
				?>
			<td>&nbsp;</td>
			<?php
				
				} ?>

			<td><strong>Subtotal:</strong></td>
			<td class="right grantotal"><span class="grantotal">$<?= number_format($vtaux["Subtotal"]["valor"],2,".",","); ?>
			</span></td>
		</tr>

		<tr>
			<?php for($n=0; $n<(count($vtkeys)-1); $n++){
				?>
			<td>&nbsp;</td>
			<?php
				
				} ?>
			<td><strong>Iva:</strong></td>
			<td class="right grantotal"><span class="grantotal">$<?= number_format($vtaux["IVA"]["valor"],2,".",","); ?>
			</span></td>
		</tr>
		<tr>
			<?php for($n=0; $n<(count($vtkeys)-1); $n++){
				?>
			<td>&nbsp;</td>
			<?php
				
				} ?>
			<td><strong>Gran Total:</strong></td>
			<td class="right "><span class="grantotal">$<?= number_format($vtaux["Total"]["valor"],2,".",","); ?>
			</span></td>
		</tr>



	</tfoot>
</table>

<script type="text/javascript">
			  
<!--

//-->

$('#lista-movimientos').dataTable({
	"bPaginate"		: false,
	"bLengthChange"	: false,
	"bFilter"		: false,
	"bInfo"			: false,
    "bJQueryUI"		: true,
    "sDom"			: '<"top"T>t'
 });
$(".TableTools_print").hide();
$(".TableTools_csv").hide();
$(".TableTools_clipboard").hide();
</script>
