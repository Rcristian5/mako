<?php use_javascript('plugins/jquery.dataTables.js') ?>
<?php use_javascript('plugins/dataTables/TableTools.js') ?>
<?php use_javascript('plugins/dataTables/ZeroClipboard.js') ?>
<?php use_javascript('plugins/jquery.windows.js') ?>
<?php use_javascript('jquery.ui.js') ?>
<?php use_javascript('i18n/jquery-ui-i18n.js') ?>
<?php use_javascript('plugins/jquery.qtip.js') ?>
<?php use_javascript('corte_caja/reporte_movimientos.js') ?>
<?php use_stylesheet('datatables.css') ?>
<?php use_stylesheet('TableTools.css') ?>
<!-- Ver detalle socio -->
<div id="verDetalleFactura" style="display: none"></div>
<!-- /Ver detalle socio -->

<?php use_stylesheet('./corte_caja/resumenValijaSuccess.css')?>

<div class="ui-widget-content ui-corner-all sombra"
	style="padding: 5px;">
	<div class="ui-widget-header ui-corner-tl ui-corner-tr tssss"
		style="padding-left: 5px;">
		<h3>
			Movimientos del
			<?= $fecha ?>
			hasta
			<?= $hora ?>
		</h3>
	</div>

	<div window-state="min" style="margin-top: 10px;">

		<div id="left">

			<fieldset>
				<legend>Per&iacute;odo</legend>
				<div>
					<label for="desde">Desde:</label> <input type="text" name="desde"
						id="desde" value="<?= $datestart; ?>" />
				</div>
				<div>
					<label for="hasta">Hasta:</label> <input type="text" name="hasta"
						id="hasta" value="<?= $dateend; ?>" />
				</div>
			</fieldset>
			<fieldset>
				<legend>Consulta</legend>
				<div id="muestra">
					<input type="radio" name="muestra" value="mes" id="valija"
						checked="checked"> <label for="mes">Valija Semanal</label> <br />
					<input type="radio" name="muestra" value="semana" id="movimiento">
					<label for="semana">Movimientos</label>
				</div>
			</fieldset>

			<br /> <input type="button" value="Generar reporte"
				id="generarReporte" />

		</div>
		<div id="right">
			<fieldset>
				<legend>Resultado</legend>
				<div id="contentReporte">
					<?php include_partial('reporteMovimientos'	, array('datos'=>$datos, "tipos"=>$tipos)) ?>
					<br />
					<?php include_partial('resumenFacturas'		, array('datos'=>$facturas)) ?>
				</div>
			</fieldset>
		</div>
		<div class="download"></div>
	</div>
</div>


<script type="text/javascript">
$(document).ready(function () {
	
	$.datepicker.setDefaults( $.datepicker.regional[ "es" ] );
	var dates = $( "#desde, #hasta" ).datepicker({
		showWeek: true,
		dateFormat: 'yy-m-d',
		minDate : new Date(2010, 8, 1),
		maxDate : 0,
		firstDay: 1,
		defaultDate: 0,
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		onSelect: function( selectedDate ) {
			var option = this.id == "desde" ? "minDate" : "maxDate",
				instance = $( this ).data( "datepicker" ),
				date = $.datepicker.parseDate(
					instance.settings.dateFormat ||
					$.datepicker._defaults.dateFormat,
					selectedDate, instance.settings );
			dates.not( this ).datepicker( "option", option, date );
		}
	});
});
</script>
