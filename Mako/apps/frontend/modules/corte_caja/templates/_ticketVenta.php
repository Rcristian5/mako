<?php
/**
 * Ticket de venta
 * @version: $Id: _ticketVenta.php,v 1.3 2010/11/23 09:53:02 eorozco Exp $
 */
use_helper('Facturas');
$productos = $orden->getDetalleOrdens();

$tasa_orden = $tasas[$orden->getId()];
//TODO: sacar este código de la plantilla.

list($iva,$importe,$total,$totalPalabras) = desglosa_iva( $orden->getTotal(), $tasa_orden );

$cadp="";
$caf_prods="";
$tipo="";
foreach($productos as $p)
{
	$cla = array();
	$psplit = array();
	$cant = array();
	$pre = array();
	//usamos este hacksillo para cortar las descripciones largas de los productos en varios renglones
	$cla[] = $p->getProducto()->getCodigo();
	$cant[] = intval($p->getCantidad());
	$pre[] = '$'.sprintf("%9s",money_format('%(#1n',tsi($p->getSubtotal(), $tasa_orden)));
	$psplit=split('\|',wordwrap(fa($p->getProducto()->getNombre()),30,'|'));
	foreach($psplit as $id=>$d)
	{
		$cantidad_ticket = (isset($cant[$id])) ? intval($cant[$id]) : '';
		$cadp .= sprintf("%-30s %4s   %9s ",$d,$cantidad_ticket,(isset($pre[$id]))? $pre[$id] : '' )."\n";
	}
	if($orden->getTipoId() == sfConfig::get('app_orden_venta_id') && $orden->getEsFactura())
	{
		$tipo = 'Se ha proporcionado FACTURA de esta venta.';
	}
	elseif($orden->getTipoId() == sfConfig::get('app_orden_venta_id') && $orden->getEstatusId() == sfConfig::get('app_estatus_cancelado_id'))
	{
		$tipo = 'VENTA CANCELADA.';
	}
	elseif($orden->getTipoId() == sfConfig::get('app_orden_venta_id') && !$orden->getEsFactura())
	{
		if(!$tasa_orden) $tipo = 'Venta al publico I.V.A. TASA 0%.';
		else $tipo = 'Venta al publico en general.';
	}
}
?>
<?php echo sfConfig::get('app_nombre_empresa')."\n" ?>
Folio: <?php echo sprintf("%08d",$orden->getFolio()) ?> - <?php echo $orden->getCentro()->getNombre()."\n" ?>
Fecha: <?php echo $orden->getCreatedAt()."\n" ?>
ID TS: <?php echo $orden->getId()."\n" ?>
Usuario: <?php echo $orden->getSocio()->getUsuario()."\n" ?>
Cred: <?php echo $orden->getSocio()->getFolio()."\n" ?>
<?php echo $tipo."\n" ?>
------------------------------------------------
            Producto           Cant.    Subtotal
------------------------------------------------
<?php echo $cadp."\n" ?>
<?php echo sprintf("%48s","TOTAL: $".sprintf("%9s",$total))."\n" ?>
<?php echo sprintf("%48s","Importe: $".sprintf("%9s",$importe))."\n" ?>
<?php echo sprintf("%48s","IVA: $".sprintf("%9s",$iva))."\n" ?>
<?php echo $totalPalabras."\n" ?>
