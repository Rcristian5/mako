<?php
/**
 * Tickets de ventas en el corte
 * @version: $Id: _ticketsChVentas.php,v 1.1 2010/11/22 07:43:55 eorozco Exp $
 */
?>
-------------DETALLE VENTAS-------------
<?php foreach ($ordenes as $orden): ?>
<?php
if($orden->getTipoId() == sfConfig::get('app_orden_venta_id'))
{
	echo get_partial('corte_caja/ticket'.sfConfig::get('app_prefijo').'Venta',array('orden'=>$orden, 'tasas'=>$tasas));
}
?>
<?php endforeach; 
if(count($ordenes) == 0) { echo ("NO SE REALIZARON VENTAS"); }
?>
-----------FIN DETALLE VENTAS-----------
V

