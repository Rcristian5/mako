<?php
/**
 * Tickets de ventas en el corte
 * @version: $Id: _ticketsVentas.php,v 1.2 2010-09-27 09:14:25 eorozco Exp $
 */
?>
<?php foreach ($ordenes as $orden): ?>

<?php
if($orden->getTipoId() == sfConfig::get('app_orden_venta_id'))
{
	echo get_partial('corte_caja/ticketVenta',array('orden'=>$orden, 'tasas'=>$tasas));
}
?>
<?php endforeach; ?>
d2
