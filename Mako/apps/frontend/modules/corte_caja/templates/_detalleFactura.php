<?php print_r($datos["base"]) ?>
<?php use_stylesheet('./corte_caja/_detalleFactura.css')?>
<h2>
	Factura:
	<?= sprintf("%08d",$folio)  ?>
</h2>
<div class="ui-widget-content ui-corner-all sombra"
	style="padding: 5px;">



	<br />
	<table align="center" class="dataTables_wrapper" width="95%"
		cellspacing="0" cellpadding="0">
		<tr class="even">
			<td><strong>Nombre:</strong></td>
			<td><?= $datos["factura"]["nombre"] ?></td>
			<td><strong>RFC:</strong></td>
			<td><?= $datos["factura"]["rfc"] ?></td>
			<td><strong>Forma de Pago:</strong></td>
			<td><?= $datos["factura"]["formaPago"] ?></td>
		</tr>

		<tr class="even">
			<td><strong>Direcci&oacute;n:</strong></td>
			<td><?= $datos["factura"]["direccion"] ?></td>
			<td><strong>Ciudad:</strong></td>
			<td><?= $datos["factura"]["ciudad"] . ", " . $datos["factura"]["estado"] . ". " . $datos["factura"]["pais"]; ?>
			</td>
			<td><strong>C.P:</strong></td>
			<td><?= $datos["factura"]["cp"] ?></td>
		</tr>




	</table>
	<br /> <br />



	<h3>Detalle de Factura</h3>
	<table align="center" id="detalle" width="90%"
		class="dataTables_wrapper">
		<thead>
			<tr>
				<th>Cantidad</th>
				<th>Concepto</th>
				<th class="right">Precio Unitario</th>
				<th class="right">Subtotal</th>
			</tr>
		</thead>
		<tbody>
			<?php $n=0; foreach ($datos["factura"]["detalle"] as $detalle): ?>
			<tr class="<?= ($n%2 ==0)?"even":"odd"; ?>">
				<td class="labelfactura"><?= number_format($detalle["cantidad"],0,".",","); 			?>
				</td>
				<td class="labelfactura"><?= $detalle["producto"]; 			?></td>
				<td class="right labelfactura"><?= money_format('%#1n',$detalle["preciounitario"]); 	?>
				</td>
				<td class="right labelfactura"><?= money_format('%#1n',$detalle["subtotal"]); 			?>
				</td>
			</tr>
			<?php $n++; endforeach; ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="3" class="right labelfactura"><strong>Subtotal:</strong>
				</td>
				<td class="right labelfacturatotal subrayado"><?= $datos["subtotal"] ?>
				</td>
			</tr>
			<tr>
				<td colspan="3" class="right labelfactura x"><strong>Impuestos:</strong>
				</td>
				<td class="right labelfacturatotal subrayado"><?= $datos["retencion"] ?>
				</td>
			</tr>
			<tr>
				<td colspan="3" class="right labelfactura "><strong>Total:</strong>
				</td>
				<td class="right labelfacturatotal subrayado"><?= $datos["total"] ?>
				</td>
			</tr>
		</tfoot>
	</table>
</div>
