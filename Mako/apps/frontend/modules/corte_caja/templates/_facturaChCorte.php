<?php
/**
 * Factura del corte, o factura de venta al publico en general para impresoras chafas de 40 caracteres por linea
 */

?>
<?php echo sfConfig::get('app_nombre_empresa')."\n" ?>
<?php echo $corte->getCentro()->getRazonSocial()."\n" ?>
<?php echo $corte->getCentro()->getRfc()."\n" ?>
<?php echo $corte->getCentro()->getCalle() ?>
<?php echo $corte->getCentro()->getNumExt() ?>
<?php echo $corte->getCentro()->getNumInt()."\n" ?>
<?php echo $corte->getCentro()->getColonia()."\n" ?>
<?php echo $corte->getCentro()->getMunicipio() ?>
,
<?php echo $corte->getCentro()->getEstado()."\n" ?>
MEXICO, C.P.
<?php echo $corte->getCentro()->getCp()."\n" ?>
PERSONA MORAL CON FINES NO LUCRATIVOS ID:
<?php echo $corte->getId()."\n" ?>
F A C T U R A O R I G I N A L ----------------------------------------
Serie:
<?php echo $ffis->getSerie() ."\n" ?>
Folio:
<?php echo $ffis->getFolioActual() ."\n" ?>
Fecha:
<?php echo $corte->getCreatedAt() ."\n" ?>
---------------------------------------- No. de certificado:
<?php echo $ffis->getNumCertificado() ."\n" ?>
A. de aprobacion:
<?php echo $ffis->getAnoAprobacion() ."\n" ?>
No. de aprobacion:
<?php echo $ffis->getNoAprobacion() ."\n" ?>
---------------------------------------- Expedido a: COMPROBANTE GLOBAL
DE OPERACIONES CON PUBLICO EN GENERAL XAXX010101000 MEXICO
---------------------------------------- 1
<?php echo $conceptos?>
Unidad: NO APLICA Valor unitario: $
<?php echo $importe ."\n"?>
Importe: $
<?php echo $importe ."\n"?>
---------------------------------------- Subtotal: $
<?php echo $subTotal ."\n"?>
IVA
<?php echo $tasa ?>
%: $
<?php echo $iva ."\n"?>
TOTAL: $
<?php echo $total ."\n"?>
Forma de pago: UNA SOLA EXHIBICION Metodo de pago: EFECTIVO
-----------CADENA ORIGINAL--------------
<?php echo $cadena ."\n"?>
----------------SELLO-------------------
<?php echo $sello ."\n"?>
---------------------------------------- Este documento es la impresion
de un Comprobante Fiscal Digital
----------------------------------------
V

