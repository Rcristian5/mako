<?php
/*
 * Componentes Bixit
*
* Copyright (c) 2009-2011 Bixit SA de CV
* Bajo Licencia dual MIT (http://www.bixit.mx/legal/MIT-LICENSE.txt)
* y GPL (http://www.bixit.mx/legal/GPL-LICENSE.txt).
*
*/
/**
 * Pantalla de solicitud de auth para realizar operación de salida
 */
?>

<script>
function valida() {
  
	if(requeridos()) {
		$('#enviar').hide();
		return true;		
	}
	else {
		return false;
  	}

}
</script>


<div class="ui-widget-header ui-corner-top tsss" style="padding: 5px;">Autorizar
	corte de caja</div>
<div class="ui-widget ui-corner-all sombra">
	<div class="ui-widget-content ui-corner-bottom" style="padding: 15px;">

		<form action="<?php echo url_for('corte_caja/auth') ?>" method="post">
			<?php if ($sf_user->hasFlash('error')): ?>
			<div class="flash_ok ui-state-error ts sombra-delgada ui-corner-all"
				style="padding: 5px; margin-top: 10px; margin-bottom: 10px;">
				<span style="float: left; margin-right: 0.3em;"
					class="ui-icon ui-icon-info"></span> <strong><?php echo $sf_user->getFlash('error') ?>
				</strong>
			</div>
			<?php endif; ?>

			<?php if ($sf_user->hasFlash('ok')): ?>
			<div
				class="flash_ok ui-state-highlight ts sombra-delgada ui-corner-all"
				style="padding: 5px; margin-top: 10px; margin-bottom: 10px;">
				<span style="float: left; margin-right: 0.3em;"
					class="ui-icon ui-icon-info"></span> <strong><?php echo $sf_user->getFlash('ok') ?>
				</strong>
			</div>
			<?php endif; ?>

			<table>
				<tr>
					<td>Nombre de usuario</td>
					<td><input type="text" name="usuario" id="usuario" size="30"
						maxlength="50" requerido="1" nouppercase="1" tabindex="1" /></td>
				</tr>
				<tr>
					<td>Clave</td>
					<td><input type="password" name="clave" id="clave" size="30"
						maxlength="50" requerido="1" nouppercase="1" tabindex="1" /></td>
				</tr>

			</table>
			<input type="submit" name="boton" id="enviar" value="Autorizar"
				onclick="return valida();" tabindex="1">
		</form>
	</div>
</div>
