<?php 
$datos		=$datos->getRawValue();
$tipos		=$tipos->getRawValue();
$total		=0;
$subtotal	=0;
$iva		=0;
$vttotales	=array();

?>
<h4>Desgloce de Caja</h4>

<table class="dataTables_wrapper" id="lista-concentrado" width="90%">

	<thead>

		<tr>
			<th class="ui-state-default">Fecha</th>
			<?php 
			foreach ($tipos as $tipo):
			$vttotales[$tipo["Nombre"]]["subtotal"]	=0;
			$vttotales[$tipo["Nombre"]]["iva"]		=0;
			$vttotales[$tipo["Nombre"]]["total"]	=0;
			?>
			<th class="ui-state-default derecha"><?= $tipo["Nombre"]; ?></th>
			<?php endforeach; ?>
			<th class="ui-state-default derecha">Subtotal</th>
			<th class="ui-state-default derecha">I.V.A</th>
			<th class="ui-state-default derecha">Total</th>
		</tr>
	</thead>


	<tbody>
		<?php 
		$vttotales["total"]=0;
		$n=0;
		foreach ($datos as $key=>$fila):
		$n++;
		$fecha=Comun::getFechaAndHoraIntoArray($key);
			
		?>
		<tr class="<?= ($n%2==0)?"odd":"even"; ?>">
			<td><?= $fecha["fecha"]; ?></td>

			<?php
			$total		=0;
			$subtotal	=0;
			$iva		=0;

			foreach ($tipos as $tipo):
			$total									+=$fila[$tipo["Nombre"]]["total"];
			$subtotal								+=$fila[$tipo["Nombre"]]["subtotal"];
			$iva									+=$fila[$tipo["Nombre"]]["iva"];
			$vttotales[$tipo["Nombre"]]["subtotal"]	+=$fila[$tipo["Nombre"]]["subtotal"];
			$vttotales[$tipo["Nombre"]]["total"]	+=$fila[$tipo["Nombre"]]["total"];
			$vttotales[$tipo["Nombre"]]["iva"]		+=$fila[$tipo["Nombre"]]["iva"];

			?>

			<td class="derecha"><?= number_format($fila[$tipo["Nombre"]]["subtotal"], 2, ".", ","); ?>
			</td>
			<?php endforeach; ?>
			<td class="derecha"><?= number_format($subtotal, 2, ".", ","); ?></td>
			<td class="derecha"><?= number_format($iva, 2, ".", ","); ?></td>
			<td class="derecha"><?= number_format($total, 2, ".", ","); ?></td>

		</tr>
		<?php
		$vttotales["total"]		+=$total;
		$vttotales["subtotal"]	+=$subtotal;
		$vttotales["iva"]		+=$iva;
		endforeach;
		?>
	
		<tr>
			<td><strong>Totales:</strong></td>
			<?php foreach ($tipos as $tipo): ?>
			<td class="derecha subrayado"><strong>$<?= number_format($vttotales[$tipo["Nombre"]]["subtotal"],2,".", ",") ; ?>
			</strong></td>
			<?php endforeach; ?>
			<td class="derecha subrayado"><strong>$<?= number_format($vttotales["subtotal"],2, ".", ",") ; ?>
			</strong></td>
			<td class="derecha subrayado"><strong>$<?= number_format($vttotales["iva"],2, ".", ","); ?>
			</strong></td>
			<td class="derecha subrayado"><strong>$<?= number_format($vttotales["total"],2, ".", ","); ?>
			</strong></td>

		</tr>
	</tbody>

</table>
<br /><br /><br />
<script type="text/javascript">
			  
<!--

//-->

$('#lista-concentrado').dataTable({
	"bPaginate"		: false,
	"bLengthChange"	: false,
	"bFilter"		: false,
	"bInfo"			: false,
    "bJQueryUI"		: true,
    "sDom"			: '<"top"T>t'
 });

$(".TableTools_print").hide();
$(".TableTools_csv").hide();
$(".TableTools_clipboard").hide();

</script>

