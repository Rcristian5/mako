<?php
/*
 * Componentes Bixit
*
* Copyright (c) 2009-2011 Bixit SA de CV
* Bajo Licencia dual MIT (http://www.bixit.mx/legal/MIT-LICENSE.txt)
* y GPL (http://www.bixit.mx/legal/GPL-LICENSE.txt).
*
*/


/**
 * *****************************************************************************************************
 * @title Reingenieria Mako
 * @author silvio.bravo@enova.mx
 * @description Agregamos funcionalidad para mostrar el concentrado de ventas del dia hasta el momento.
 * @date 04-Marzo-2014
 * *****************************************************************************************************
 */


/*** Agrupamos la informacion para obtener el concentrado general, el desgloce de venta y las ventas canceladas ***/

$vtGeneral      =array();
$vttotales      =array();
$vtCancelados   =array();
$vtVentas       =array();
foreach ($ordenes as $orden){
    if($orden->getEstatusId()==2){
        $vtCancelados[] =$orden;
    }

        $vtVentas[]     = $orden;
        $fechaHora      = corte_cajaActions::getFechaAndHoraIntoArray($orden->getCreatedAt());

        if(!array_key_exists($orden->getFormaPago()->getNombre(),$vttotales)){
            $vttotales[$orden->getFormaPago()->getNombre()]=0;
        }
        $vttotales[$orden->getFormaPago()->getNombre()]+=$orden->getTotal();

        foreach ($orden->getDetalleOrdens() as $detalle){


            $cantidad = $detalle->getCantidad();
            $total    = $detalle->getSubtotal();
            $concepto = $detalle->getProducto()->getCategoriaProducto()->getNombre();


            if((array_key_exists($concepto, $vtGeneral[$fechaHora["fecha"]]))){

                $vtGeneral[$fechaHora["fecha"]][$concepto]["cantidad"]+=$cantidad;
                $vtGeneral[$fechaHora["fecha"]][$concepto]["total"]+=$total;
            }
            else{
                $vtGeneral[$fechaHora["fecha"]][$concepto]=array("cantidad"=> $cantidad, "total"=>$total);
            }

        }

}

/*** Fin de agrupar informacion ***/



?>
<?php use_javascript('plugins/jquery.dataTables.js') ?>
<?php use_javascript('plugins/dataTables/TableTools.js') ?>
<?php use_javascript('plugins/dataTables/ZeroClipboard.js') ?>
<?php use_javascript('jquery.ui.js') ?>
<?php use_stylesheet('datatables.css') ?>
<?php use_stylesheet('TableTools.css') ?>
<?php use_stylesheet('tabs.css') ?>
<script>

var lsoT;
$(document).ready(function() {

    var $tabs = $('#tabs').tabs();

   lsoT = $('#lista-ordenes').dataTable({
        "bPaginate": false,
        "bLengthChange": false,
        "bFilter": false,
        "bInfo": false,
        "bJQueryUI": true,
        "sDom": '<"top"T>t'
     });

   lsoT2 = $('#lista-concentrado').dataTable({
        "bPaginate": false,
        "bLengthChange": false,
        "bFilter": false,
        "bInfo": false,
       "bJQueryUI": true,
       "sDom": '<"top"T>t'
    });
   $(".TableTools_print").hide();

   lsoT2 = $('#lista-canceladas').dataTable({
        "bPaginate": false,
        "bLengthChange": false,
        "bFilter": false,
        "bInfo": false,
      "bJQueryUI": true,
      "sDom": '<"top"T>t'
   });
  $(".TableTools_print").hide();
});
</script>





<!-- Ver detalle socio -->
<div id="verDetalle" style="display: none">
    <div id="detalleFactura" style="display: none">
        <h2>pregunta nuevamente</h2>
    </div>
</div>
<!-- /Ver detalle socio -->

<script>
    //Ver detalle socio
            var $wcm= $("#verDetalle").dialog(
            {
                        modal: true,
                        autoOpen: false,
                        position: ['center','center'],
                        title:  'Ver detalle',
                        //setter
                        height: 350,
                        width: 800,
                        buttons: {
                            'Cerrar': function()
                            {
                                $(this).dialog('close');
                            }
                        }
            });


            function verDetalle(idOrden)
                    {
                         var UrlVerDetalle = "<?php echo url_for('corte_caja/verDetalleOrden') ?>";
                            $("#verDetalle").load(
                                UrlVerDetalle,
                                 {
                                     idOrden: idOrden
                                 }

                        );
                            $wcm.dialog('open');
                    };
</script>
<?php use_stylesheet('./corte_caja/resumenHoySuccess.css')?>

<div class="ui-widget-content ui-corner-all sombra"
    style="padding: 5px;">
    <div class="ui-widget-header ui-corner-tl ui-corner-tr tssss"
        style="padding-left: 5px;">
        <h3>
            Movimientos del
            <?= $fecha ?>
            hasta
            <?= $hora ?>
        </h3>
    </div>


    <!--
            *********************************************************************************
            * Agregamos tabla de concentrado de ventas ordenadas por fecha y categoria      *
            * aqui se pone la tabla con los datos que ya han sido agrupados anteriormente   *
            * el el codigo que esta mas arriba en este mismo archivo. Los datos estan en un *
            * array: $vtGeneral                                                             *
            * @author: silvio.bravo@enova.mx                                                *
            * @title: Reingenieria Mako                                                     *
            * @date: 04 Marzo 2014                                                          *
            *********************************************************************************
     -->


    <div id="tabs">

        <ul id="tabMenu" class="tsss">
            <li><a href="#concentrado">Concentrado de Ventas</a></li>
            <li><a href="#desgloce">Desgloce de Ventas</a></li>
            <li><a href="#canceladas">Ventas Canceladas</a></li>


        </ul>

        <!-- /* Mostramos concentrado por fecha y Concepto */  -->
        <div id="concentrado" class="ui-tabs-panel">

            <table id="lista-concentrado" width="90%">

                <thead>
                    <tr>
                        <th colspan="4">Concentrado de Ventas por Concepto</th>
                    </tr>
                    <tr>
                        <th>Fecha</th>
                        <th>Concepto</th>
                        <th>Cantidad</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $sumacantidad   =0;
                    $sumatotal      =0;

                    foreach ($vtGeneral as$fecha=>$fila):       //Array por fecha
                    foreach($fila as $concepto=>$datos):    //Array por concepto
                    $sumacantidad   += $datos["cantidad"];
                    $sumatotal      += $datos["total"];

                    ?>
                    <tr>
                        <td><?= $fecha                              ?></td>
                        <td><?= $concepto                           ?></td>
                        <td class="derecha"><?= intval($datos["cantidad"])          ?></td>
                        <td class="subtotal"><?= sprintf('%01.2f',$datos["total"])  ?></td>
                    </tr>

                    <?php
                    endforeach;
                    endforeach;
                    ?>
                </tbody>

                <tfoot>
                    <tr>
                        <td colspan="2" class="derecha">Total:</td>
                        <td class="subtotal subrayado"><strong> <?= $sumacantidad;?>
                        </strong></td>
                        <td class="subtotal subrayado"><strong>$<?= sprintf('%01.2f',$sumatotal) ?>
                        </strong></td>
                    </tr>
                    <!--  Responsable: IMB 23/10/2014 

                    <?php foreach ($vttotales as $concepto=>$valor): ?>

                    <tr>                       
                        <td colspan="3" class="derecha"><strong><?= $concepto; ?></strong>
                        </td>
                        <td colspan="1" class="derecha subrayado subtotal"><strong>$<?= number_format($valor,2,".",""); ?>
                        </strong></td>
                    </tr>

                    <?php endforeach;?>
                    -->
                </tfoot>

            </table>
        </div>

        <!-- Terminamos Conceotrado por concepto  -->

        <div id="desgloce" class="ui-tabs-panel">

            <!--  Iniciamos LIstado detallado de Ventas -->

            <!-- Desgloce de Venta -->
            <table id="lista-ordenes" width="90%">
                <thead>
                    <tr>
                        <th colspan="8">Desgloce de Ventas</th>
                    </tr>
                    <tr>
                        <th></th>
                        <th>Comprobante</th>
                        <th>Folio</th>
                        <th>Operación</th>
                        <th>Fecha</th>
                        <th>Hora</th>
                        <th>Realizó</th>
                        <th>Total</th>
                    </tr>
                </thead>

                <tbody>
                    <?php $total = listaDatosVentas($vtVentas, '', array(2)); ?>
                </tbody>
                <tfoot>
                <!--  Responsable: IMB 03/11/2014 
                    <?php foreach ($vttotales as $concepto => $valor): ?>
                        <tr>
                            <td colspan="7" class="derecha">
                                <strong>
                                    Total <?= $concepto; ?>:
                                </strong>
                            </td>
                            <td class="derecha subtotal subrayado">
                                <strong>
                                    $<?= number_format($valor, 2,".",","); ?>
                                </strong>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    -->
                    <tr>
                        <td colspan="7" class="derecha"><strong>Total:</strong></td>
                        <td class="derecha subtotal subrayado">
                            <strong>
                                $<?= number_format($total, 2,".",","); ?>
                            </strong>
                        </td>
                    </tr>


                </tfoot>

            </table>
        </div>

        <div id="canceladas" class="ui-tabs-panel">
            <!--  Iniciamos LIstado detallado de Ventas -->

            <!-- Desgloce de Venta Canceladas-->
            <table id="lista-canceladas" width="90%">
                <thead>
                    <tr>
                        <th colspan="8">Desgloce de Ventas Canceladas</th>
                    </tr>
                    <tr>
                        <th></th>
                        <th>Comprobante</th>
                        <th>Folio</th>
                        <th>Operación</th>
                        <th>Fecha</th>
                        <th>Hora</th>
                        <!-- Responsable: IMB 23/10/2014 -->
                        <th width="100%">Realizó</th>
                        <th>Total</th>
                    </tr>
                </thead>

                <tbody>
                    <?php $total    = listaDatosVentas($vtCancelados, "negativo"); ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="7" class="derecha"><strong>Total:</strong></td>
                        <td class="derecha subtotal subrayado negativo"><strong>$<?= number_format($total, 2,".",","); ?>
                        </strong></td>
                    </tr>
                </tfoot>

            </table>

        </div>

        <!-- Terminamos detallado de ventas  -->

    </div>

    <?php
        function listaDatosVentas($datos, $totalclass="", $ignorar = array()){

        $c      = 1;
        $total  = 0;
        foreach ($datos as $orden){
            if(!in_array($orden->getTipoId(), $ignorar)) {
                $fechaHora =  corte_cajaActions::getFechaAndHoraIntoArray($orden->getCreatedAt());

                if($orden->getTipoId() == 1) {
                    $link = '<a href="javascript:void(0);" onClick="verDetalle('. $orden->getId() .')">'.
                                sprintf('%06s',$orden->getFolio()).
                            '</a>';

                } else {
                    $link = sprintf('%06s',$orden->getFolio());
                }

                $total += $orden->getTotal();
    ?>
    <tr>
        <td><?= $c; $c++;                                           ?></td>
        <td><?= ($orden->getEsFactura()==true)?"Factura":"Ticket";  ?></td>
        <td><?= $link;                                              ?></td>
        <td><?= $orden->getTipoOrden()->getNombre();                ?></td>
        <td><?= $fechaHora["fecha"]                                 ?></td>
        <td><?= $fechaHora["hora"]                                  ?></td>
        <td><?= $orden->getUsuario()                                ?></td>
        <td class="<?= $totalclass; ?> subtotal"><?= sprintf('%01.2f',$orden->getTotal()) ?>
        </td>
    </tr>
    <?php
            }
        }
        return($total);
    }
    ?>