<?php
/*
 * Componentes Bixit
*
* Copyright (c) 2009-2011 Bixit SA de CV
* Bajo Licencia dual MIT (http://www.bixit.mx/legal/MIT-LICENSE.txt)
* y GPL (http://www.bixit.mx/legal/GPL-LICENSE.txt).
*
*/
/**
 * corte_caja actions.
 *
 * @package    bixit
 * @subpackage corte_caja
 * @author     Bixit SA de CV
 * @version    SVN: $Id: actions.class.php,v 1.13 2011/03/04 18:11:51 eorozco Exp $
 */
class corte_cajaActions extends sfActions
{
    public function executeIndex(sfWebRequest $request)
    {
        //TODO: Hacer forma de auth para promotor o admin q autorice el corte de caja
        $c = new Criteria();
        $c->addDescendingOrderByColumn(CortePeer::CREATED_AT);
        $centro_id = sfConfig::get('app_centro_actual_id');
        $c->add(CortePeer::CREATED_AT,null,Criteria::ISNOTNULL);

        if($centro_id != sfConfig::get('app_central_id'))
        {
            $c->add(CortePeer::CENTRO_ID,$centro_id);
            $c->setLimit(400);
        }

        $this->Cortes = CortePeer::doSelect($c);

    }

    public function executeShow(sfWebRequest $request)
    {
        $corte_id = $request->getParameter('id');
        $this->cc = CortePeer::retrieveByPk($corte_id);
        $this->forward404Unless($this->cc);

        $c = new Criteria();
        $c->add(OrdenPeer::CORTE_ID,$corte_id);
        $c->addAscendingOrderByColumn(OrdenPeer::CREATED_AT);

        $t0 = CortePeer::foliosTasa0($this->cc);
        $this->total_tasa0 = $t0['total_tasa0'];
        $this->ords = OrdenPeer::doSelect($c);

    }

    public function executeNew(sfWebRequest $request)
    {

        //Si no se ha autorizado al autorizador mandamos a la pantalla de auth con msg
        if(!$this->getUser()->hasFlash('autorizado'))
        {
            $this->getUser()->setFlash('error', 'Los datos proporcionados no corresponden a un usuario autorizado. Vuelva a intentar.');
            $this->redirect('corte_caja/guiAuth');
        }


        // Inicializamos un nuevo corte de caja

        // Reingenieria Mako C. ID 15 & 16. Responsable: ES & FE. Fecha: 15-01-2014. Descripción del cambio: Se valida que exista 'Salida de caja' antes de realizar corte.

        $ip_caja = Comun::ipReal();
        $centro_id = sfConfig::get('app_centro_actual_id');
        $s = new Criteria();
        $s->add(SalidaPeer::CENTRO_ID,$centro_id);
        $s->add(SalidaPeer::IP_CAJA,$ip_caja);
        $s->addDescendingOrderByColumn(SalidaPeer::CREATED_AT);
        $s->setLimit(1);
        $salidaAnterior = SalidaPeer::doSelectOne($s);
        if(property_exists($salidaAnterior, "created_at") ){
            $fechaSalida = $salidaAnterior->getCreatedat();
        }
        $fechaActual = date("Y-m-d");
        $fechaActual = date("Y-m-d",strtotime ($fechaActual) );
        $fechaSalidaFormato = date("Y-m-d",strtotime ($fechaSalida) );

        if ($fechaActual!=$fechaSalidaFormato){
            $this->getUser()->setFlash('error', 'Se necesita una salida de caja previa.');
            $this->redirect('corte_caja/guiAuth');
        }
        // Fecha: 15-01-2014. Fin

        // Reingenieria Mako C. ID 15. Responsable: ES & FE. Fecha: 15-01-2014. Descripción del cambio: Se valida que exista un 'Corte de caja' por dia.

        $ct = new Criteria();
        //$ct->add(CortePeer::CREATED_AT, $fechaActual);i
        $ct->add(CortePeer::CREATED_AT, $fechaActual, Criteria::GREATER_THAN);
        $ct->addDescendingOrderByColumn(CortePeer::CREATED_AT);
        $ct->setLimit(1);
		$corte = CortePeer::doSelectOne($ct);
        if( $corte != null && property_exists($corte, "created_at") ){
            $fechaCorte = $corte->getCreatedat();
		    $fechaCorteFormato = date("Y-m-d",strtotime ($fechaCorte) );
	        error_log("Corte: ".$ct->toString());
			error_log("Fecha corte: ".$fechaCorteFormato." Fecha actual: ".$fechaActual);
		    if ($fechaCorteFormato>=$fechaActual){
	            $this->getUser()->setFlash('error', 'No se puede realizar mas de un corte al día.');
				$this->redirect('corte_caja/guiAuth');
			}
        }

        // Fecha: 15-01-2014. Fin

        $cc = new Corte();

        // Consultamos cuál fué el corte de caja más reciente, en esta caja, en este centro.
        $ip_caja = Comun::ipReal();
        $centro_id = sfConfig::get('app_centro_actual_id');
        $c = new Criteria();
        $c->add(CortePeer::CENTRO_ID,$centro_id);
        $c->add(CortePeer::IP_CAJA,$ip_caja);
        $c->addDescendingOrderByColumn(CortePeer::CREATED_AT);
        $c->setLimit(1);
        $corteAnterior = CortePeer::doSelectOne($c);

        $fondo_inicial = 0;

        //Si no hay ningún registro entonces es el primer corte de caja. Por lo tanto se toma el fondo de caja inicial de la conf
        if($corteAnterior == null)
        {
            $fondo_inicial = sfConfig::get('app_fondo_caja_inicial');
        }
        else
        {
            // Reingenieria Mako C. ID 15. Responsable: ES & FE. Fecha: 15-01-2014. Descripción del cambio: Toma el fondo inicial del archivo .

            $fondo_inicial = sfConfig::get('app_fondo_caja_inicial');
            //$fondo_inicial = $corteAnterior->getFondoCaja();

            // Fecha: 15-01-2014. Fin
        }

        //Realizamos las operaciones con los registros que tienen corte_id = null y posteriores a la fecha del ultimo corte
        $c = new Criteria();
        $c->add(OrdenPeer::CENTRO_ID,$centro_id);
        $c->add(OrdenPeer::IP_CAJA,$ip_caja);
        $c->add(OrdenPeer::CORTE_ID,null,Criteria::ISNULL);
        $c->addAscendingOrderByColumn(OrdenPeer::CREATED_AT);

        $this->ords = OrdenPeer::doSelect($c);

        if(!CortePeer::tieneVentas($this->ords))
        {
            $this->getUser()->setFlash('error', 'No hay ninguna operación que reportar en el corte de caja.');
            //$this->redirect('corte_caja/guiAuth');
        }

        $total_ventas = 0;
        $total_facturas = 0;
        $total_vpg = 0;
        $total_salidas = 0;
        $total_cancelaciones = 0;
        $total_cancelaciones_fact = 0;
        $total_cancelaciones_vpg = 0;
        $total_conciliaciones = 0;
        $total_tasa0 = 0;

        $folio_inicial = 0;
        $folio_final = 0;
        foreach($this->ords as $ord)
        {
            $ord->setCorte($cc);
            if($ord->getTipoId() == sfConfig::get('app_orden_venta_id')
                    && $ord->getEstatusId() == sfConfig::get('app_estatus_pagado_id')
                    && !$ord->getEsFactura()
            )
            {
                if($folio_inicial == 0) $folio_inicial = $ord->getFolio();
                $folio_final = $ord->getFolio();
            }

            if($ord->getTipoId() == sfConfig::get('app_orden_venta_id'))
                $total_ventas = $total_ventas + $ord->getTotal();

            //Es orden de venta y no es venta facturada, además no es tasa cero. sumamos al total_vpg
            if($ord->getTipoId() == sfConfig::get('app_orden_venta_id') && !$ord->getEsFactura()) {
                /*
                 * BK02ST19 - Se comenta codigo videjo donde se realizaba calculo
                 * de total_vpg, debido a que posteriormente se realiza el calculo
                 */
                if(DetalleOrdenPeer::tasa_productos($ord)) {
                    $total_tasa0 = $total_tasa0 + $ord->getTotal();
                }
                /*
                if(!DetalleOrdenPeer::tasa_productos($ord)) {
                    $total_vpg = $total_vpg + $ord->getTotal();
                }
                else
                {
                    $total_tasa0 = $total_tasa0 + $ord->getTotal();
                }*/
            }

                /*
                 * BK02ST19 - Se comenta codigo videjo donde se realizaba calculo
                 * de total_vpg, debido a que posteriormente se realiza el calculo
                 */
        /* Se deshabilita la suma de la cancelacion al total de ventas por sugerencia de 
        Luis Gonzales e Isabel Baxim en junta llevada el dia 28/10/2014 con 
        personal de finanza Domitila Lozano, responsable:IMB 
            //si la cancelacion es de ticket y es el mismo dia o extemporanea:
            if($ord->getTipoId() == sfConfig::get('app_orden_cancelacion_id') && !$ord->getEsFactura())
            {
                $total_vpg = $total_vpg + $ord->getTotal();
            }*/

        
            if($ord->getTipoId() == sfConfig::get('app_orden_venta_id') && $ord->getEsFactura())
                $total_facturas = $total_facturas + $ord->getTotal();

            if($ord->getTipoId() == sfConfig::get('app_orden_cancelacion_id') && $ord->getEsFactura())
            {
                $oo = CancelacionPeer::ordenOriginal($ord);
                if($oo->getCreatedAt("Y-m-d") == date("Y-m-d") || $oo->getCorteId()==null)
                {
                    $total_cancelaciones_fact = $total_cancelaciones_fact + $ord->getTotal();
                }
                /*
                 * BK02ST19 - Se comenta codigo videjo donde se realizaba calculo
                 * de total_vpg, debido a que posteriormente se realiza el calculo
                 */
                /*else
                {
                     /* Se deshabilita la suma de la cancelacion al total de ventas por sugerencia de 
                        Luis Gonzales e Isabel Baxim en junta llevada el dia 28/10/2014 con 
                        personal de finanza Domitila Lozano, responsable:IMB 
                    $total_vpg = $total_vpg + $ord->getTotal();
                }*/
            }

            if($ord->getTipoId() == sfConfig::get('app_orden_cancelacion_id'))
                $total_cancelaciones = $total_cancelaciones + $ord->getTotal();

            if($ord->getTipoId() == sfConfig::get('app_orden_conciliacion_administrativa_id'))
                $total_conciliaciones = $total_conciliaciones + $ord->getTotal();

            if($ord->getTipoId() == sfConfig::get('app_orden_salida_id'))
                $total_salidas = $total_salidas + $ord->getTotal();
        }

        $fondo_caja = $fondo_inicial + ($total_ventas + $total_cancelaciones + $total_salidas + $total_conciliaciones);

        /*
         * 
         * BK02ST19 - Quitar IVA 16% a Libros del Total del Ventas
         * El calculo para el Total de ventas al publico en general es
         * El Total de Ventas realizadas
         * + el Total de las cancelaciones
         * + el Total de las facturas
         * - el Total de ventas de Libros (Debido a que no contiene IVA)
         * BGPJ Bet Gader Porcayo Juárez
         * 20/Marzo/2015
         */
        $total_vpg = $total_ventas + $total_cancelaciones - $total_facturas - $total_tasa0;

        $cc->setFondoInicial($fondo_inicial);
        $cc->setFolioVentaInicial($folio_inicial);
        $cc->setFolioVentaFinal($folio_final);
        $cc->setFondoCaja($fondo_caja);
        $cc->setTotalVentas($total_ventas);
        $cc->setTotalVpg($total_vpg);
        $cc->setTotalFacturas($total_facturas);
        $cc->setTotalCancelaciones($total_cancelaciones);
        $cc->setTotalConciliaciones($total_conciliaciones);
        $cc->setTotalSalidas($total_salidas);
        $cc->setIpCaja($ip_caja);
        $cc->setCentroId($centro_id);

        $cc->save();

        $this->cc = $cc;

        //Agregamos al contexto el total de tasa cero para que aparezca en la plantilla
        $this->total_tasa0 = $total_tasa0;

        //Se guarda la informacion en la tabla sincronia sap para su posterior envio
        $this->guardaSincroniaSap_corteCaja($cc);

        $this->botonesImpresion = True;

        $this->setTemplate('show');

        $this->dispatcher->notify(new sfEvent($this, 'corte.realizado', array(
        'ordenes' => $this->ords,
                'corte' => $this->cc
        )));

    }

    public function executeCreate(sfWebRequest $request)
    {
        $this->forward404Unless($request->isMethod(sfRequest::POST));

        $this->form = new CorteForm();

        $this->processForm($request, $this->form);

        $this->setTemplate('new');
    }

    /**
     * Pantalla de autorización de la operación de salida.
     * @param sfWebRequest $request
     * @return void
     */
    public function executeGuiAuth(sfWebRequest $request)
    {
        //esta simplemente lleva a la plantilla guiAuthSuccess.php
    }

    /**
     * Busca los datos ingresados en la forma de autorización en la BD
     * @param sfWebRequest $request
     * @return void
     */
    public function executeAuth(sfWebRequest $request)
    {
        $this->forward404Unless($request->isMethod(sfRequest::POST));
        $usuario = $request->getParameter('usuario');
        $clave = $request->getParameter('clave');

        //Roles autorizadores
        $autorizados = array(
                sfConfig::get('app_rol_super_usuario_id'),
                sfConfig::get('app_rol_supervisor_id'),
                sfConfig::get('app_rol_facilitador_encargado_id'),
                sfConfig::get('app_rol_facilitador_id'),
                sfConfig::get('app_rol_administrador_operaciones_id')
        );
        $c = new Criteria();
        $c->add(UsuarioPeer::USUARIO,$usuario);
        $c->add(UsuarioPeer::CLAVE,$clave);
        $c->add(UsuarioPeer::ACTIVO,true);

        $c->add(UsuarioPeer::ROL_ID,$autorizados,Criteria::IN);

        //TODO: validar centro autorizado para este usuario

        $usuario = UsuarioPeer::doSelectOne($c);
        if($usuario == null)
        {
            // Reingenieria Mako C. ID 56. Responsable:  FE. Fecha: 07-04-2014.  Descripción del cambio: Se corrigió ortografía en el mensaje
            $this->getUser()->setFlash('error', 'Los datos proporcionados no corresponden a un usuario autorizado. Vuelva a intentar.');
            $this->redirect('corte_caja/guiAuth');
            // Fecha: 07-04-2014 Fin
        }
        else
        {
            $this->getUser()->setFlash('autorizado', 'ok');
            $this->redirect('corte_caja/new');
        }
    }



    protected function processForm(sfWebRequest $request, sfForm $form)
    {
        $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
        if ($form->isValid())
        {
            $Corte = $form->save();

            $this->redirect('corte_caja/edit?id='.$Corte->getId());
        }
    }


    public function executeImprimir(sfWebRequest $request)
    {
        $ip = Comun::ipReal();

        $corte_id = $request->getParameter('id');
        $tipo = $request->getParameter('tipo');

        error_log("Tipo impresion: ".$tipo);
        error_log("Corte Id: ".$corte_id);
        error_log("Ip Real: ".$ip);

        //detalle
        if($tipo=='detalle')  {
            $nomtick="/tmp/ticket_detalle".$corte_id. ".txt";
            $res = exec("lp -h ".$ip." -d okipos $nomtick");
        }
        //resumen (1  y 2)
        elseif($tipo=='resumen')  {
            $nomtick="/tmp/ticket_resumen".$corte_id. ".txt";
            $res = exec("lp -h ".$ip." -d okipos $nomtick");
        }
        //factura
        elseif($tipo=='factura')
        {
            $nomtick='/tmp/factura_corte_'.$corte_id.'.txt';
            if(file_exists($nomtick))
            {
                $res = exec("lp -h ".$ip." -d okipos $nomtick");
            }

            $nomtick_t0='/tmp/factura_corte_t0_'.$corte_id.'.txt';
            if(file_exists($nomtick_t0))
            {
                $res = exec("lp -h ".$ip." -d okipos $nomtick_t0");
            }
        }

        $this->getResponse()->setContentType('application/json');
        return $this->renderText(json_encode('Hola Hola'));
    }


    public function executeVerDetalleOrden(sfWebRequest $request)
    {
        //$Orden = DetalleOrdenPeer::retrieveByPk($request->getParameter('idOrden'));

        $c = new Criteria();
        $c->add(DetalleOrdenPeer::ORDEN_ID,$request->getParameter('idOrden'));
        $Orden = DetalleOrdenPeer::doSelect($c);

        $od = OrdenPeer::retrieveByPK($request->getParameter('idOrden'));
        $folio = '';
        if($od) {
            $folio = $od->getFolio();
        }

        return $this->renderPartial('corte_caja/detalleOrden',array('Orden'=>$Orden, 'folio' => $folio));
    }


    /**
     * *******************************************************************************************************************
     *
     * Realizamos un resumen de las ventas del dia realizadas hasta el momento.
     * @author silvio.bravo@enova.mx
     * @title Reingeniera Mako
     * @date  03-Marzo-2014
     * @return view
     *
     * *******************************************************************************************************************
     * */
    public function executeResumenHoy(sfWebRequest $request){

        $fechaInicio    = date("Y-m-d H:s:i",mktime(0, 0, 1     , date("m")  , date("d"), date("Y")));
        $fechaFinal     = date("Y-m-d H:s:i",mktime(23, 59, 59  , date("m")  , date("d"), date("Y")));

        $centro_id      = sfConfig::get('app_centro_actual_id');
        $this->ordenes  = OrdenPeer::getOrdenesAlDia($fechaInicio, $fechaFinal, $centro_id);
        $this->fecha    = $this->nombreDia(date("N")) . " " . date("d") ." de " .  $this->nombreMes(date("m")) . " del " . date("Y");
        $this->hora     = date("h:m A");



    }

    private function nombreMes($mes){
        $mes    =intval($mes);
        $meses  = array(
                1=>'Enero'      , 2=>'Febrero',
                3=>'Marzo'      , 4=>'Abril',
                5=>'Mayo'       , 6=>'Junio',
                7=>'Julio'      , 8=>'Agosto',
                9=>'Septiembre' , 10=>'Octubre',
                11=>'Noviembre' , 12=>'Diciembre',
        );
        if(key_exists($mes, $meses))
            return($meses[$mes]);
        return "";

    }


    private function nombreDia($dia){
        $dia    =intval($dia);
        $dias   = array(
                1=>'Lunes'          , 2=>'Martes',
                3=>'Miercoles'      , 4=>'Jueves',
                5=>'Viernes'        , 6=>'Sabado',
                7=>'Domingo'
        );
        if(key_exists($dia, $dias))
            return($dias[$dia]);
        return "";

    }

    public static function getFechaAndHoraIntoArray($fecha){

        list($fecha, $hora)     = explode(" ",$fecha);
        list($anio,$mes,$dia)   = explode("-",$fecha);
        $stringFecha            = "{$dia} " . self::nombreMes($mes) . " {$anio}";

        return(array("fecha"=>$stringFecha,"hora"=>$hora));

    }



    /**
     * *************************************************************************************************    *
     * Agregamos un reporte para la generacion de la valija y obtener informacion                       *
     * sobre los cortes realizados en un periodo de tiempo                                              *
     * @title Reingenieria Mako.                                                                            *
     * @author silvio.bravo@enova.mx                                                                        *
     * @date 11-MArzo-2014                                                                              *
     * *************************************************************************************************    *
     *
     */

    /*
     * Controller para el reporte de valija
    */

    public function executeResumenValija(sfWebRequest $request){
        $fechainicio= date("Y-m-d");
        $fechafinal = date("Y-m-d");
        $tipo       = 1;
        if($request->hasParameter("tipo")){
            $tipo       =$request->getParameter("tipo");
        }

        switch ($tipo){
            case 1:         //Resume de valija :)
                $this->forward("corte_caja","serviceValijaReport");
                break;
            case 2:         //  Movimientos :D
                $this->forward("corte_caja","serviceMovimientosReport");
        break;
        }

    }



    public function executeServiceMovimientosReport(sfWebRequest $request){

        $fechainicio        = date("Y-m-d");
        $fechafinal         = date("Y-m-d");
        $centro_id          = sfConfig::get('app_centro_actual_id');
        $this->ordenamiento ="fecha";


        if($request->hasParameter("fechainicio") && $request->hasParameter("fechafinal")){

            $fechainicio=$request->getParameter("fechainicio");
            $fechafinal =$request->getParameter("fechafinal");

        }
        $this->datos    =OrdenPeer::getDetalleOrdenPorPeriodo($fechainicio, $fechafinal,$centro_id);
        $this->tipo     =2;
        $centro         = CentroPeer::retrieveByPK($centro_id);
        $this->iva      =$centro->getIva();
        $this->setTemplate("serviceValijaReport", "corte_caja");
        $this->setLayout(false);

    }



    /**
     * Servicio para obtener los datos del reporte de valija
     * @param $request->getParameter("tipo")
     * @param $request->getParameter("fechainicio")
     * @param $request->getParameter("fechafinal")
     * @return array JSON
     * @date 11-Marzo-2014
     * @author silvio.bravo@enova.mx
     *
     */

    public function executeServiceValijaReport(sfWebRequest $request){
        $fechainicio= date("Y-m-d");
        $fechafinal = date("Y-m-d");
        $esprincipal= true;

        if($request->hasParameter("fechainicio") && $request->hasParameter("fechafinal")){

            $fechainicio=$request->getParameter("fechainicio");
            $fechafinal =$request->getParameter("fechafinal");
            $esprincipal=false;
        }

        $this->datestart    = ($esprincipal==true)?Comun::getLunesDeSemana():$fechainicio;
     $this->dateend     = $fechafinal;
     $this->extra       = "";
     $centro_id         = sfConfig::get('app_centro_actual_id');

     $datos             = OrdenPeer::getResumenCorteByPeriodo($centro_id, $this->datestart, $this->dateend, "YYYY-MM-DD");
     $facturas          = FacturaPeer::getResumenFacturasPorPeriodo($centro_id, $this->datestart, $this->dateend);

     $this->tipos       = Comun::serializeObjectArray(TipoProductoPeer::getAll());
     $this->datos       = self::ordenaArrayResumen("tipo",$datos, $this->tipos);
     $this->facturas    = $facturas;
     $this->tipo        =1;

     if($esprincipal==false){
        $this->setTemplate("serviceValijaReport", "corte_caja");
        $this->setLayout(false);
     }
     else{
        $this->setTemplate("resumenValija", "corte_caja");
     }
    }




    /**
     * Agrupamos la informacion dependiendo de una key especifica.
     *
     * @param string $key
     * @param array $datos
     * @return array
     */
    private function ordenaArrayResumen($key, $datos, $keys, $keysToGet){

        $vt=array();
        foreach ($datos as $linea){
            foreach ($keys as $fila){
                foreach (array("cantidad", "descuento","subtotal", "importe", "total","iva") as $valor){
                    if(!array_key_exists($valor,$vt[$linea["fecha"]][$fila["Nombre"]])){
                        $vt[$linea["fecha"]][$fila["Nombre"]][$valor]=0;
                    }
                }
            }

            $importe=$linea["importe"];
            $iva    =$linea["subtotal"] - $importe;
            $
            $vt[$linea["fecha"]][$linea[$key]]["cantidad"]  +=$linea["cantidad"];
            $vt[$linea["fecha"]][$linea[$key]]["descuento"] +=$linea["descuento"];
            $vt[$linea["fecha"]][$linea[$key]]["subtotal"]  +=money_format('%#1n', $importe);
            $vt[$linea["fecha"]][$linea[$key]]["iva"]       +=money_format('%#1n', $iva);
            $vt[$linea["fecha"]][$linea[$key]]["total"]     +=money_format('%#1n', $linea["subtotal"]);

        }

        return($vt);

    }


    /**
     * Servicio para obtener el detalle de una factura.
     * @param sfWebRequest $request
     * @return View con los datos de la factura.
     */

    public function executeDetalleFactura(sfWebRequest $request){
        $this->forward404Unless($request->isMethod(sfRequest::POST));
        $facturaid      =$request->getParameter("facturaid");
        $datos          =FacturaPeer::getDetail($facturaid);
        return $this->renderPartial('corte_caja/detalleFactura',array('datos'=>$datos, 'folio' => $datos["folio"]));
    }


    /**
     * ********************************************************************************************************************************
     * Reporte de ventas diarias. NMP                                                                                                   *
     *                                                                                                                              *
     * @WSMethod(name='reporteVentasDiaria', webservice='MakoApi')                                                                  *
     *                                                                                                                              *
     *                                                                                                                              *
     * @param  string   $centro string of centro_id                                                                                 *
     * @param  string   $fechaIni an string of date start                                                                           *
     * @param  string   $fechaFin an string of date end                                                                             *
     * @return string Regresa cadena en formato JSON con los valores: success, message y data (resultado del reporte)                   *
     * @author silvio.bravo@enova.mx 03-Abril-2014 Reingen                                                                          *
     * @copyright Enova                                                                                                             *
     * @category Reingenieria Mako NMP                                                                                              *
     *                                                                                                                              *
     * ********************************************************************************************************************************
     *
     */


    public function executeReporteVentasDiaria(sfWebRequest $request){

        $result = array("success"=>"false", "message"=>"No se han recibido los parametros necesarios.", "data"=>array());
        $datos  = array();


        if($request->hasParameter("centro") && $request->hasParameter("fechaIni") && $request->hasParameter("fechaFin")){

            $centroId   = pg_escape_string($request->getParameter("centro"));
            $fechaIni   = pg_escape_string($request->getParameter("fechaIni"));
            $fechaFin   = pg_escape_string($request->getParameter("fechaFin"));

            if(Comun::validaFecha($fechaIni) && Comun::validaFecha($fechaFin)){

                $datos              = DetalleOrdenPeer::getReporteVentas($centroId, $fechaIni, $fechaFin);
                $result["success"]  = ($datos["success"])?"true":"false";
                $result["data"]     = $datos["data"];
                $result["message"]  = ($datos["success"])?"Reporte de Ventas para el centro {$centroId} de {$fechaIni} a {$fechaFin}": "Ocurrio un error al consultar con la DB, por vafor verifica los valores enviados";

            }
            else{

                $result["success"]  = "false";
                $result["message"]  = "Formato de Fechas diferente a YYYY-MM-DD o valores incorrectos";

            }

        }
        $this->result   = json_encode($result);
        return sfView::SUCCESS;
    }

     /**
     * Guarda la cancelacion de venta en la tabla sincronia_sap para la sincronizacion con SAP
     * @param sfWebRequest $request
     * @return string json
     */
    public function guardaSincroniaSap_corteCaja($orden)
    {
            $sincronia_sap = new SincroniaSap();
            $sincronia_sap->setOperacion('Pagos');
            $sincronia_sap->setPkReferencia($orden->getId());
            $sincronia_sap->setCompleto(false);
            $sincronia_sap->setCreatedAt(time());
            $sincronia_sap->save();

        return $sincronia_sap;
    }


}
