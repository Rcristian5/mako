<?php
//Time set para mexico

/*
 * ****************************************************************************************************	*
* Reingenieria Mako																					*
* Agregamos un id de formulario para poder encontrarlo posteriormente y consumir la busqueda de socio	*
* deslindad de el comportamiento del componente buscador. 												*
* Esto forma parte de las modificaciones realizadas para resolver el issue de abrir el pv 				*
* desde busqueda de socio y cargar por default los datos del socio y el cargo de credencial.			*
* @author silvio.bravo@enova.mx 24 Marzo 2014															*
* @title Reingenieria Mako																				*
* ****************************************************************************************************	*
*
*/

date_default_timezone_set('America/Mexico_City');
setlocale (LC_TIME, 'es_ES.UTF-8');
?>

<?php if(!isset($id_buscador)) $id_buscador = 'ctx-'.rand(100,999) ?>
<?php use_javascript('registro/buscaSocio.js') ?>

<form action="<?php echo url_for('registro/buscaSocio') ?>"
	id="makoBuscaSocioComponentForm">

	<table>
		<tr>
			<td>
				<fieldset class="forma-buscador-socios"
					id="buscador-socios-<?php echo $id_buscador ?>">
					<legend>Buscar</legend>
					<table>
						<tbody>
							<tr>
								<th>Folio</th>
								<!--Reingeniería PV | Responsable: YD => FE. - Fecha: 26-11-2013. - ID: 1. - Campo: Folio.
                                        Descripción del cambio: Se modifica el archivo ./web/js/registro/buscaSocio.js para que soporte teclas de dirección, se corrigió longitud de campo.-->
								<td><input type="text" name="qFolio" filtro="numerico" size="20"
									maxlength="13" autocomplete="off"
									id="qFolio-<?php echo $id_buscador ?>"
									onkeyup="validaCampo(this,event)" /></td>
								<!--Fecha: 26-11-2013. - Fin-->
							</tr>
							<tr>
								<th>Nombre usuario</th>
								<!--Reingeniería PV | Responsable: FE. - Fecha: 23-10-2013. - ID: 2. - Campo: Nombre usuario.
					Descripción del cambio: Se modifica el archivo ./web/js/filtroVal.js para que soporte el filtro usuario.-->
					<!-- 15/10/2014 responsable:IMB, perimitr copiar en el campo"-->
								<td><input type="text" name="qUsuario" filtro="usuario"
									size="20" autocomplete="off" 
									id="qUsuario-<?php echo $id_buscador ?>"
									onkeyup="validaCampo(this,event)" /></td>
								<!--Fecha: 23-10-2013. - Fin-->
							</tr>
							<tr>
								<!--Reingeniería PV | Responsable: FE. - Fecha: 24-10-2013. - ID: 4. - Campo: Nombre usuario.
					Descripción del cambio: Se crea archivo ./web/js/buscaSocio.js para validar espacios.-->
								<th>Nombre completo</th>
					<!-- 15/10/2014 responsable:IMB, perimitr copiar en el campo" (onPaste="return false")-->
								<td><input type="text" name="qNombre" filtro="alfabetico"
									autocomplete="off" size="20" 
									id="qNombre-<?php echo $id_buscador ?>"
									onkeyup="validaCampo(this,event)" /></td>
								<!--Fecha: 24-10-2013. - Fin-->
							</tr>
							<tr>
								<!--<th>Buscar</th>-->
								<td align="center"><input type="button"
									id="boton-buscar-socios-<?php echo $id_buscador ?>"
									value="Buscar socio"
									onclick="
							$('#buscador-socios-<?php echo $id_buscador ?>').trigger('socios.buscador.buscando');
							$('#resultados-buscador-socios-<?php echo $id_buscador ?>').load(
							 $(this).parents('form').attr('action'),
							 {
								 'qNombre': $('#qNombre-<?php echo $id_buscador ?>').val(),
								 'qUsuario': $('#qUsuario-<?php echo $id_buscador ?>').val(),
								 'qFolio': $('#qFolio-<?php echo $id_buscador ?>').val()
							 },
							 function(v1,v2){$('#buscador-socios-<?php echo $id_buscador ?>').trigger('socios.buscador.encontrados');}
                                                        );
                                                        //Cambios solicitados para el POS - Búsqueda de socio - Limpiar campos
                                                        $('#qNombre-<?php echo $id_buscador ?>').val('');
                                                        $('#qUsuario-<?php echo $id_buscador ?>').val('');
                                                        $('#qFolio-<?php echo $id_buscador ?>').val('');
                                                        /*-Reingeniería PV | Responsable: FE. - Fecha: 24-10-2013. - ID: 6. - Campo: Nombre usuario.
														Descripción del cambio: Se activan campos despues de realizar búsqueda.*/
                                                        $( 'input[name=\'qFolio\']' ).removeAttr('disabled');
														$( 'input[name=\'qUsuario\']' ).removeAttr('disabled');
														$( 'input[name=\'qNombre\']' ).removeAttr('disabled');
														/*Fecha: 24-10-2013. - Fin*/
                                                        //Terminan Cambios - Búsqueda de socio - Limpiar campos
						" />
								</td>

								<!--Id:10, Responsable: ES, Fecha: 22/10/2013-->
								<td align="center"><input type="button" name="btnCancelar"
									value="Cancelar" onclick="location.href='./..'" />
								</td>
							</tr>
						</tbody>
					</table>
				</fieldset>

				<hr />

				<div id="resultados-buscador-socios-<?php echo $id_buscador ?>"
					class="resultados-buscador-socios">
					<?php include_partial('registro/listaSocios', array('Socios' => $Socios,'msg'=>$msg,'id_buscador'=>$id_buscador)) ?>
				</div>
			</td>
		</tr>
	</table>
</form>
