<?php 
/**
 * Plantilla del ticket de socio
 * @version: $Id: _ticketSocio.php,v 1.3 2010/11/23 09:54:09 eorozco Exp $
 */
?>
<?php echo sfConfig::get('app_nombre_empresa')."\n"; ?>
<?php echo $socio->getCentro()->getNombre()."\n"; ?>
<?php echo $socio->getCentro()->getDireccionImpresion()."\n"; ?>
Fecha:<?php echo date("Y-m-d H:i:s")."\n"; ?>
-----------------------------------------------
DATOS DE SOCIO
-----------------------------------------------
Nombre de usuario:
<?php echo $socio->getUsuario()."\n"; ?>
Clave temporal:
<?php echo $socio->getClave()."\n"; ?>
Folio de credencial:
<?php echo $socio->getFolio()."\n"; ?>
-----------------------------------------------

