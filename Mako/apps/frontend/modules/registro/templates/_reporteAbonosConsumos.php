<?php
/**
 * Reporte de abonos de saldo y consumos por sesion
 */
?>

<?php $saldoInicial = sfConfig::get('app_saldo_registro_nuevo') ?>
<!--Reingenieria Mako C. ID 11. Responsable:  FE. Fecha: 16-01-2014.  Descripción del cambio: Se mejoro la forma de como se muestra la información del reporte.-->
<!-- < ?php echo ($saldoInicial/60)/60  ?> horas =  < ?php echo ($saldoInicial/60) ?> minutos por bienvenida de socio.-->
Por bienvenida, se le han otorgado
<?php echo ($saldoInicial/60)/60 . " " ?>
horas de tiempo equivalentes a
<?php echo ($saldoInicial/60) . " " ?>
minutos.
<!-- Fecha: 16-01-2014 - Fin. -->

<h3>Abonos de tiempo por compras:</h3>
<table width="100%" border="0"
	id="registrosAbonos">
	<thead>
		<tr>
			<th>Nombre del producto</th>
			<th>Tiempo_abonado(min)</th>
			<th>Fecha</th>
		</tr>
	</thead>
	<?php foreach($Ordenes as $orden)
	{

		foreach($orden->getDetalleOrdens() as $abono)
		{
			$tiempo = 0;
			if($abono->getProducto()->getEsSubproducto()){
				$cr = new Criteria();
				$cr->add(SocioPagosPeer::DETALLE_ORDEN_ID,$abono->getId());
				$sp = SocioPagosPeer::doSelectOne($cr);
				if($sp != null) $tiempo = $sp->getTiempo();
			}
			else {
				$tiempo = $abono->getTiempo();
			}

			if($abono->getTiempo()>0){
				?>
	<!-- BEGIN abonos -->
	<tr>
		<td><?php echo $abono->getProducto()->getNombre() ?></td>
		<td align="right"><?php
		/* Reingeniería PV | Responsable: GS->FE. - Fecha: 06-12-2013. - ID: 74. - Campo: Reporte de abonos y consumo de tiempo.
		 Descripción del cambio: Se agrego signo negativo y rojo,  cuando se cancela una venta por tiempo.*/
		if($abono->getSubtotal()<0){
			$tiempo=$tiempo*-1;
			echo '<span style="color:#ff0000;">'.($tiempo/60).'</span>';
		}else{
			echo ($tiempo/60);
		}
		/* Fecha: 06-12-2013. - Fin*/
		?>
		</td>
		<td><?php echo $abono->getCreatedAt() ?></td>
	</tr>
	<!-- END abonos -->
	<?php
	$totalComprado = $tiempo + $totalComprado;
			}
		}
	}
	?>
</table>
<br>
<!--Reingenieria Mako C. ID 11. Responsable:  FE. Fecha: 16-01-2014.  Descripción del cambio: Se mejoro la forma de como se muestra la información del reporte.-->
<!--Total < ?php echo ($totalComprado/60)/60 ?> horas = < ?php echo ($totalComprado/60) ?> minutos-->
<!-- Fecha: 16-01-2014 - Fin. -->
Total de tiempo comprado =  
<?php echo ($totalComprado/60)/60 ?>
 horas equivalentes a
<?php echo ($totalComprado/60) ?>
 minutos.
<br>
<?php $totalAbonado = $totalComprado + $saldoInicial ?>
<!--Reingenieria Mako C. ID 11. Responsable:  FE. Fecha: 16-01-2014.  Descripción del cambio: Se mejoro la forma de como se muestra la información del reporte.-->
<!--Total abonado < ?php echo (($totalAbonado/60)/60) ?> horas = < ?php echo ($totalAbonado/60) ?> minutos.-->
<!-- Fecha: 16-01-2014 - Fin. -->

<h3>Detalle de consumo por sesiones:</h3>
<table width="100%"
	border="0" id="registrosConsumo">
	<thead>
		<tr>
			<th>IP</th>
			<th>Fecha Login</th>
			<th>Fecha Logout</th>
			<th>Min. Inicial</th>
			<th>Min. Final</th>
			<th>Min. Ocupados</th>
		</tr>
	</thead>
	<?php foreach($Sesiones as $sesion){ 
		?>
	<!-- BEGIN sesiones -->
	<tr>
		<td><?php echo $sesion->getIp() ?></td>
		<td><?php echo $sesion->getFechaLogin() ?></td>
		<td><?php echo $sesion->getFechaLogout() ?></td>
		<td align="right"><?php echo round(($sesion->getSaldoInicial()/60), 2) ?>
		</td>
		<td align="right"><?php echo round(($sesion->getSaldoFinal()/60), 2) ?>
		</td>
		<td align="right"><?php echo round(($sesion->getOcupados()/60), 2) ?>
		</td>
	</tr>
	<!-- END sesiones -->
	<?php 
	$totalConsumido = $sesion->getOcupados() + $totalConsumido;
			} ?>
</table>

<br>
<!--Reingenieria Mako C. ID 11. Responsable:  FE. Fecha: 16-01-2014.  Descripción del cambio: Se mejoro la forma de como se muestra la información del reporte.-->
<!--Total consumido: < ?php echo round(($totalConsumido/60)/60, 2) ?> hrs.-->
Total de tiempo consumido = 
<?php echo round(($totalConsumido/60)/60, 2) ?>
 horas que equivalentes a
<?php echo round(($totalConsumido/60), 2) ?>
 minutos
<br>
Total Abonado = Total de tiempo comprado + Tiempo de regalo = Tiempo.
<br>
Total Abonado = 
<?php echo (($totalComprado/60)/60) ?>
 + 
<?php echo (($saldoInicial/60)/60) ?>
 = 
<?php echo (($totalAbonado/60)/60) ?>
 hrs equivalente a
<?php echo ($totalAbonado/60) ?>
 minutos.
<br>
Diferencia de consumo = Total abonado - Total consumido = 
<?php echo round((($totalAbonado/60)/60) - (($totalConsumido/60)/60),2) ?>
 hrs equivalentes a 
<?php echo round((($totalAbonado/60)) - (($totalConsumido/60)),2) ?>
 minutos. 
<br>
<!--Saldo actual: < ?php echo round(($Socio->getSaldo()/60),2)?> minutos = < ?php echo round((($Socio->getSaldo()/60)/60),2)?> hrs = < ?php echo Comun::SegsATxt($Socio->getSaldo())?>.-->
<!--Saldo actual = < ?php echo round((($Socio->getSaldo()/60)/60),2)?> hrs equivalente a < ?php echo round(($Socio->getSaldo()/60),0)?> minutos.-->
<!-- Fecha: 16-01-2014 - Fin. -->

<?php $saldo = $totalAbonado - $totalConsumido; 

if($saldo>0){
		
	echo '<h3>El usuario tiene '. Comun::SegsATxt($totalAbonado - $totalConsumido) . ' a favor.</h3>';
}
else
	echo '<h3>El usuario NO tiene saldo a favor.</h3>';
?>

