<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<?php use_javascript('registro/validaSocio.js') ?>
<?php use_javascript('jquery.ui.js') ?>
<?php use_javascript('plugins/jquery.autocompleter.js') ?>
<?php use_stylesheet('tabs.css') ?>


<!-- Busca socio -->
<div id="buscadorSocio" style="display: none">
	<?php include_component('registro', 'buscaSocios') ?>
</div>
<!-- /Busca socio -->

<form name="registro"
	action="<?php echo url_for('registro/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>"
	method="post"
	<?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
	<?php if (!$form->getObject()->isNew()): ?>
	<input type="hidden" name="sf_method" value="put" />
	<?php endif; ?>

	<script type="text/javascript">
	$(function() {
		//$("#tabs").tabs();
		var $tabs = $('#tabs').tabs();
		var next=0;
		var i = 0;

		$(".ui-tabs-panel").each(function(i){

		  var totalSize = $(".ui-tabs-panel").size() - 1;
		  if (i != totalSize) {

		      next = i+1;
	   		  $(this).append("<a href='#' class='next-tab mover' rel='" + next + "'>Siguiente &#187;</a>");
		  }

		  if (i != 0) {
		      prev = i-1;
	   		  $(this).append("<a href='#' class='prev-tab mover' rel='" + prev + "'>&#171; Anterior</a>");
		  }
			i++;
		});

		$('.next-tab, .prev-tab').click(function() {
				//alert($(this).attr("rel"));
			   $tabs.tabs('select', Number($(this).attr("rel")));
	           return false;
	    });

		var $wcm= $("#buscadorSocio").dialog(
			{
			    		modal: true,
						autoOpen: false,
			    		position: ['center','center'],
			    		title:	'Buscador de socio',
			    		//setter
			    		height: 530,
			    		width: 300,
			    		buttons: {
							'Cerrar': function()
				    		{
								$(this).dialog('close');
							},
							'Cancelar': function()
				    		{
								$(this).dialog('close');
							}

						}
			});

			$("#boton-abre-ventana").click(function()
			{
				$wcm.dialog('open');
			});


			$(document).bind('socio.seleccionado',function(ev, socio_ref_id, socio_ref_nombre){

					//Setear los valores a sus campos
					document.getElementById('socio_socio_ref_id').value = socio_ref_id;
					document.getElementById('socio_ref_nombre').innerHTML = socio_ref_nombre;


					$wcm.dialog('close');
			});


	});


	</script>


	<div id="page-wrap">

		<div id="tabs">

			<ul id="tabMenu">
				<li><a href="#personales">Datos personales</a></li>
				<li><a href="#acercamiento">Medio de acercamiento</a></li>
				<li><a href="#escolaridad">Escolaridad</a></li>
				<li><a href="#ocupacion">Ocupacion</a></li>
				<li><a href="#habilidades">Habilidades</a></li>
			</ul>


			<div id="personales" class="ui-tabs-panel">

				<h3>Identificación</h3>
				<div>
					<?php include_partial('registro/capturaFoto') ?>
					<?php include_partial('registro/capturaHuella') ?>
					<p>
						<?php echo $form['nombre']->renderLabel() ?>
						<?php echo $form['nombre']->render(array('requerido'=>'1','filtro'=>'alfabetico','size'=>'50','maxlength'=>'30')) ?>
						<?php echo $form['nombre']->renderError() ?>
						<br />

						<?php echo $form['apepat']->renderLabel("Apellido Paterno") ?>
						<?php echo $form['apepat']->render(array('requerido'=>'1','filtro'=>'alfabetico','size'=>'50','maxlength'=>'30')) ?>
						<?php echo $form['apepat']->renderError() ?>
						<br />

						<?php echo $form['apemat']->renderLabel("Apellido Materno") ?>
						<?php echo $form['apemat']->render(array('requerido'=>'0','filtro'=>'alfabetico','size'=>'50','maxlength'=>'30')) ?>
						<?php echo $form['apemat']->renderError() ?>
						<br />

						<?php echo $form['fecha_nac']->renderLabel("Fecha de Nacimiento") ?>
						<?php echo $form['fecha_nac']->render(array('requerido'=>'1')) ?>
						<?php echo $form['fecha_nac']->renderError() ?>
						<br />

						<?php echo $form['sexo']->renderLabel("Género") ?>
						<?php echo $form['sexo']->render(array('requerido'=>'1')) ?>
						<?php echo $form['sexo']->renderError() ?>
						<br />

					</p>
				</div>
				<h3>Contacto</h3>
				<br>
				<?php echo $form['tel']->renderLabel("Teléfono casa") ?>
				<?php echo $form['tel']->render(array('filtro'=>'numerico', 'maxlength'=>'12')) ?>
				<?php echo $form['tel']->renderError() ?>
				<br />

				<?php echo $form['celular']->renderLabel("Teléfono celular") ?>
				<?php echo $form['celular']->render(array('filtro'=>'numerico', 'maxlength'=>'11')) ?>
				<?php echo $form['celular']->renderError() ?>
				<br />

				<?php echo $form['email']->renderLabel() ?>
				<?php echo $form['email']->render(array('nouppercase'=>'1')) ?>
				<?php echo $form['email']->renderError() ?>

				<h3>Domicilio</h3>
				<br>
				<?php echo $form['id_estado']->renderLabel("Estado") ?>
				<?php echo $form['id_estado']->render() ?>
				<?php echo $form['id_estado']->renderError() ?>
				<br />

				<?php echo $form['municipio']->renderLabel() ?>
				<?php echo $form['municipio']->render() ?>
				<?php echo $form['municipio']->renderError() ?>
				<br />

				<?php echo $form['colonia']->renderLabel() ?>
				<?php echo $form['colonia']->render() ?>
				<?php echo $form['colonia']->renderError() ?>
				<br />

				<?php echo $form['cp']->renderLabel("C.P.") ?>
				<?php echo $form['cp']->render(array('filtro'=>'numerico','size'=>'5','maxlength'=>'5')) ?>
				<?php echo $form['cp']->renderError() ?>
				<br />

				<?php echo $form['calle']->renderLabel() ?>
				<?php echo $form['calle']->render() ?>
				<?php echo $form['calle']->renderError() ?>
				<br />

				<?php echo $form['num_ext']->renderLabel("Número exterior") ?>
				<?php echo $form['num_ext']->render() ?>
				<?php echo $form['num_ext']->renderError() ?>
				<br />

				<?php echo $form['num_int']->renderLabel("Número interior") ?>
				<?php echo $form['num_int']->render() ?>
				<?php echo $form['num_int']->renderError() ?>
				<br />

				<h3>Membresía</h3>
				<br>
				<?php echo $form['folio']->renderLabel("Folio credencial") ?>
				<?php echo $form['folio']->render(array('requerido'=>'1','filtro'=>'numerico','size'=>'15','maxlength'=>'13')) ?>
				<?php echo $form['folio']->renderError() ?>
				<br />

				<?php echo $form['clave']->renderLabel("Clave") ?>
				<?php echo $form['clave']->render(array('value'=>$form->getObject()->getClave(),'requerido'=>'1','size'=>'15','maxlength'=>'30')) ?>
				<?php echo $form['clave']->renderError() ?>
				<br>
				<?php echo $form['clave_confirmacion']->renderLabel("Clave confirmación") ?>
				<?php echo $form['clave_confirmacion']->render(array('value'=>$form->getObject()->getClave(),'requerido'=>'1','size'=>'15','maxlength'=>'30')) ?>
				<?php echo $form['clave_confirmacion']->renderError() ?>
			</div>

			<div id="acercamiento" class="ui-tabs-panel ui-tabs-hide">

				<?php echo $form['canal_contacto_id']->renderLabel("¿Como se enteró del centro?") ?>
				<?php echo $form['canal_contacto_id']->render(array('onChange' => 'acercamiento(this.value, null)')) ?>
				<?php echo $form['canal_contacto_id']->renderError() ?>
				<br />
				<div id="acercamientoCampos">
					<?php echo $form['promotor_ref_id']->renderLabel("Nombre del promotor") ?>
					<?php echo $form['promotor_ref_id']->render(array('value' => $form->getObject()->getUsuarioRelatedByPromotorRefId()))?>
					<?php echo $form['promotor_ref_id']->renderError() ?>
					<br />

					<?php echo $form['liga']->renderLabel() ?>
					<?php echo $form['liga']->render(array('disabled' => 'true')) ?>
					<?php echo $form['liga']->renderError() ?>
					<br />

					<?php echo $form['recomienda_id']->renderLabel("Recomendó") ?>
					<?php echo $form['recomienda_id']->render(array('disabled' => 'true', 'onChange' => 'acercamientoRecomienda(this.value)')) ?>
					<?php echo $form['recomienda_id']->renderError() ?>
					<br />

					<div id="acercamientoCamposRecomendo">
						<?php echo $form['otros_contacto']->renderLabel() ?>
						<?php echo $form['otros_contacto']->render(array('filtro'=>'alfabetico', 'disabled' => 'true')) ?>
						<?php echo $form['otros_contacto']->renderError() ?>
						<br />

						<?php echo $form['socio_ref_id']->renderLabel("Socio que recomienda") ?>
						<?php echo $form['socio_ref_id'] ?>
						<div id="socio_ref_nombre">
							<?php echo $form->getObject()->getSocioRelatedBySocioRefId() ?>
						</div>
						<div id="recomendo_socio" style="display: none">
							<a href="javascript:void(0);" id="boton-abre-ventana">Buscar
								Socio</a>
						</div>
						<?php echo $form['socio_ref_id']->renderError() ?>

						<br />



					</div>
				</div>
			</div>

			<div id="escolaridad" class="ui-tabs-panel ui-tabs-hide">

				<?php echo $form['estudia']->renderLabel() ?>
				<?php echo $form['estudia']->render(array('selected' => 'true',  'onChange' => 'estudiosActual()')) ?>
				<?php echo $form['estudia']->renderError() ?>
				<br />
				<div id="estudiaCamposActual">
					<?php echo $form['nivel_id']->renderLabel("Nivel de estudios en curso") ?>
					<?php echo $form['nivel_id']->render(array('onChange' => 'estudiosNivelActual(this.value)')) ?>
					<?php echo $form['nivel_id']->renderError() ?>
					<br />

					<div id="otrosEstudiosActual">
						<?php echo $form['profesion_actual_id']->renderLabel() ?>
						<?php echo $form['profesion_actual_id']->render(array('disabled' => 'disabled')) ?>
						<?php echo $form['profesion_actual_id']->renderError() ?>
						<br />

						<?php echo $form['estudio_actual_id']->renderLabel("Otros estudios") ?>
						<?php echo $form['estudio_actual_id']->render(array('disabled' => 'disabled', 'onChange' => 'estudiosOtrosActual(this.value)')) ?>
						<?php echo $form['estudio_actual_id']->renderError() ?>
						<br />

						<?php echo $form['otro_estudio_actual']->renderLabel("Otros") ?>
						<?php echo $form['otro_estudio_actual']->render(array('disabled' => 'disabled','filtro'=>'alfabetico',)) ?>
						<?php echo $form['otro_estudio_actual']->renderError() ?>
						<br />
					</div>
				</div>
				<?php echo $form['tipo_escuela_id']->renderLabel("Tipo de escuela") ?>
				<?php echo $form['tipo_escuela_id'] ?>
				<?php echo $form['tipo_escuela_id']->renderError() ?>
				<br />

				<?php echo $form['beca']->renderLabel("¿Cuenta con financiamiento o beca?") ?>
				<?php echo $form['beca'] ?>
				<?php echo $form['beca']->renderError() ?>
				<br /> <br />

				<?php echo $form['tipo_estudio_id']->renderLabel("Tipo de estudios") ?>
				<?php echo $form['tipo_estudio_id'] ?>
				<?php echo $form['tipo_estudio_id']->renderError() ?>
				<br />

				<?php echo $form['grado_id']->renderLabel("Ultimo grado de estudios concluido al 100%") ?>
				<?php echo $form['grado_id']->render(array('onChange' => 'estudiosNivelUltimo(this.value)'))?>
				<?php echo $form['grado_id']->renderError() ?>
				<br />

				<div id="otrosEstudiosUltimo">
					<?php echo $form['profesion_ultimo_id']->renderLabel() ?>
					<?php echo $form['profesion_ultimo_id']->render(array('disabled' => 'disabled'))?>
					<?php echo $form['profesion_ultimo_id']->renderError() ?>
					<br />

					<?php echo $form['estudio_ultimo_id']->renderLabel("Otros estudios") ?>
					<?php echo $form['estudio_ultimo_id']->render(array('disabled' => 'disabled', 'onChange' => 'estudiosOtrosUltimo(this.value)')) ?>
					<?php echo $form['estudio_ultimo_id']->renderError() ?>
					<br />

					<?php echo $form['otro_estudio_ultimo']->renderLabel("Otros") ?>
					<?php echo $form['otro_estudio_ultimo']->render(array('disabled' => 'disabled','filtro'=>'alfabetico',))?>
					<?php echo $form['otro_estudio_ultimo']->renderError() ?>
					<br />
				</div>

			</div>

			<div id="ocupacion" class="ui-tabs-panel ui-tabs-hide">

				<?php echo $form['trabaja']->renderLabel() ?>
				<?php echo $form['trabaja']->render(array('onChange' => 'trabaja(this)')) ?>
				<?php echo $form['trabaja']->renderError() ?>
				<br />
				<div id="ocupacionCampos">
					<?php echo $form['organizacion_id']->renderLabel() ?>
					<?php echo $form['organizacion_id']->render(array('disabled' => 'disabled')) ?>
					<?php echo $form['organizacion_id']->renderError() ?>
					<br />

					<?php echo $form['sector_id']->renderLabel() ?>
					<?php echo $form['sector_id']->render(array('disabled' => 'disabled')) ?>
					<?php echo $form['sector_id']->renderError() ?>
					<br />

					<?php echo $form['posicion_id']->renderLabel() ?>
					<?php echo $form['posicion_id']->render(array('disabled' => 'disabled')) ?>
					<?php echo $form['posicion_id']->renderError() ?>
					<br />
				</div>

				<?php echo $form['ocupacion_id']->renderLabel() ?>
				<?php echo $form['ocupacion_id'] ?>
				<?php echo $form['ocupacion_id']->renderError() ?>
				<br />

			</div>

			<div id="habilidades" class="ui-tabs-panel ui-tabs-hide">
				<?php echo $form['discapacidad_id']->renderLabel() ?>
				<?php echo $form['discapacidad_id'] ?>
				<?php echo $form['discapacidad_id']->renderError() ?>
				<br />

				<?php echo $form['habilidad_informatica_id']->renderLabel() ?>
				<?php echo $form['habilidad_informatica_id'] ?>
				<?php echo $form['habilidad_informatica_id']->renderError() ?>
				<br />


				<?php echo $form['dominio_ingles_id']->renderLabel() ?>
				<?php echo $form['dominio_ingles_id'] ?>
				<?php echo $form['dominio_ingles_id']->renderError() ?>
				<br />

			</div>

			<?php echo $form['foto']->render() ?>
			<?php echo $form['huella']->render() ?>

			<br />
			<?php echo $form['id'] ?>
			<?php echo $form['_csrf_token'] ?>

			<input type="submit" value="Guardar"
				onclick="return comparaRequeridos();" /> <input type="button"
				value="Cancelar"
				onclick="location.href='/frontend_dev.php/registro'">
		</div>
	</div>


	<script>



	acercamiento( <?php echo $form->getObject()->getCanalContactoId(); ?>, <?php echo $form->getObject()->getRecomiendaId(); ?> );
	acercamientoRecomienda( <?php echo $form->getObject()->getRecomiendaId(); ?> );
	estudiosActual( <?php echo $form->getObject()->getEstudia(); ?> );
	estudiosNivelActual( <?php echo $form->getObject()->getNivelId(); ?> );

	estudiosOtrosActual( <?php echo $form->getObject()->getEstudioActualId(); ?> );

	estudiosNivelUltimo( <?php echo $form->getObject()->getGradoId(); ?> );

	estudiosOtrosUltimo( <?php echo $form->getObject()->getEstudioUltimoId(); ?> );

	trabaja();

	$("#socio_promotor_ref_id").val( $("#acercamientoCampos").html() );

</script>

</form>
