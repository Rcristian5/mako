<?php use_javascript('registro/edicionSocio.js') ?>
<?php use_javascript('jquery.ui.js') ?>
<?php use_javascript('plugins/jquery.dataTables.js') ?>
<?php use_javascript('plugins/date.js') ?>
<?php use_javascript('plugins/dataTables/datatables.orden-fecha.js') ?>
<?php use_javascript('plugins/dataTables/datatables.orden-decimal.js') ?>
<?php use_javascript('registro/buscaSocio.js') ?>

<?php use_stylesheet('datatables.css') ?>

<!-- Ver detalle socio -->
<div id="verDetalleSocio" style="display: none"></div>
<!-- /Ver detalle socio -->

<script>
	//Ver detalle socio
			var $wcm= $("#verDetalleSocio").dialog(
			{
			    		modal: true,
						autoOpen: false,	    		
			    		position: ['center','center'],
			    		title:	'Ver detalle',
			    		//setter
			    		height: 550,
			    		width: 1100,
			    		buttons: {
							'Cerrar': function() 
				    		{
								$(this).dialog('close');
							}							
						}
			});


			function verDetalle(idSocio)
					{
						 var UrlVerDetalle = "<?php echo url_for('registro/verDetalle') ?>";
							$("#verDetalleSocio").load(								 
								UrlVerDetalle,
								 {
									 id: idSocio
								 }			
								 
						); 
							$wcm.dialog('open');					
					};
					function removeParam(key, sourceURL) {
				    var rtn = sourceURL.split("?")[0],
				        param,
				        params_arr = [],
				        queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
				    if (queryString !== "") {
				        params_arr = queryString.split("&");
				        for (var i = params_arr.length - 1; i >= 0; i -= 1) {
				            param = params_arr[i].split("=")[0];
				            if (param === key) {
				                params_arr.splice(i, 1);
				            }
				        }
				        rtn = rtn + "?" + params_arr.join("&");
				    }
				    return rtn;
				}
				<?php 
/*
 * Se revierte el cambio de mostrar mensaje de ¿Desea imprimir usuario y clave? despues de registrar un socio
 *
 *
	$(document).ready(function() {
		if (window.location.search.indexOf('nuevoSocio=1') > -1) {
			$('body').append('<div id="modal_dialog"><div class="title" id="TitleModal"></div></div>');
                var warning = '¿Desea imprimir usuario y clave?';
                $('#TitleModal').html(warning);
                var dialog = $('#modal_dialog').dialog({
                    resizable: false,
                    height:150,
                    width:400,
                    buttons: {
                        "Si": function() {
                            console.log('Desea imprimir');
					            $.getJSON('/index.php/registro/imprimirUsuarioClave', 
					            	{ socio_id: 141288848386232}, 
					            	function(res){
   									 alert(res.msg);
   									 window.location = removeParam('nuevoSocio',document.URL);
									}); 
                            $( this ).dialog( "close" );
                            $("#modal_dialog").remove();
                        },
                        "No": function() {
                            console.log('No deseo imprimir usuairo y clave');
                            $( this ).dialog( "close" );
                            $("#modal_dialog").remove();
                        }
                      }
                });
		}
	});
*/
?>
</script>
<div class="ui-widget-content ui-corner-all sombra"
	style="padding: 5px;" id="contenedor-general">

	<?php if (isset($msg) && $msg!= ''): ?>
	<div
		class="flash_notice ts ui-state-highlight ui-corner-all sombra-delgada"
		style="float: left; padding: 15px;">
		<span style="float: left; margin-right: 0.3em;"
			class="ui-icon ui-icon-info"></span> <strong><?php echo $msg ?> </strong>
	</div>
	<?php endif; ?>

	<table width="100%" border="0">
		<tr>
			<td width="75%"><?php foreach ($Socios as $Socio): ?>
				<form>
					<div class="ui-widget">
						<div class="ui-widget-content ui-corner-all">
							<div class="ui-widget-header ui-corner-tl ui-corner-tr tsss"
								style="padding: 5px;">
								<?php echo $Socio->getNombre().' '.$Socio->getApepat().' '.$Socio->getApemat() ?>
							</div>


							<!-- BEGIN datosSocio -->
							<table width="100%" border="0">
								<tr>
									<td width="20%" rowspan="6" valign="top"><div
											class="container fotoGrande">
											<img
												src="<?php echo ($Socio->getFoto() != '')? '/fotos/'.$Socio->getFoto() : '/imgs/sin_foto_sh.jpg' ?>"
												class="main fotoGrande sombra-delgada" /> <img
												class="minor fotoGrande" src="/imgs/frame.png">
										</div></td>
									<th width="10%">Usuario:</th>
									<td><?php echo $Socio->getUsuario() ?></td>
									<th width="10%">Fecha Nac:</th>
									<td colspan="2"><?php echo $Socio->getFechaNac("%d-%m-%Y") ?> <!-- Valor reposicion credencial -->
										<script> var costoRepCred = '<?php echo sfConfig::get('app_costo_reposicion_tarjeta')?>';</script>
										<div id="reposicionCred<?php echo $Socio->getId() ?>"
											style="display: none">
											<label for="folio">Folio:</label> <input type="text"
												name="folio<?php echo $Socio->getId() ?>"
												id="folio<?php echo $Socio->getId() ?>"
												value="<?php echo $Socio->getFolio() ?>" maxlength="50"
												size="50" tabindex="1" filtro='numerico' requerido="1" /> <br />
											<input type="button"
												id="bot-guard-<?php echo $Socio->getId() ?>" value="Guardar"
												onClick="
				             if ( requeridos() ) {
					            $(this).hide();
					            $.getJSON('<?php echo url_for('registro/reposicionTarjeta') ?>', 
					            	{ socio_id: <?php echo $Socio->getId() ?>,
					            	folio: document.getElementById('folio<?php echo $Socio->getId() ?>').value }, 
					            	function(res){
   									 alert(res.msg);
   									 document.getElementById('reposicionCred'+<?php echo $Socio->getId() ?>).style.display ='none';
   									 if(confirm('¿ Deseas realizar otra venta para este Socio ?'))
   									 	redireccionaAPuntoVenta(<?php echo $Socio->getId() ?>);
									}); 
							}" />
										</div> <br /></td>
								</tr>
								<tr>
									<th>Edad:</th>
									<td><?php echo $Socio->getEdad() ?> años</td>
									<th>Género:</th>
									<td colspan="2"><?php echo ($Socio->getSexo()=='M') ? 'Masculino':'Femenino' ?>
									</td>
								</tr>
								<tr>
									<th>Folio:</th>
									<td><?php echo $Socio->getFolio() ?></td>
									<th>Estudia:</th>
									<td colspan="2"><?php echo ($Socio->getEstudia()=='1') ? (($Socio->getNivelEstudio()!=null)?$Socio->getNivelEstudio():'Si'):'No' ?>
									</td>
								</tr>
								<tr>
									<th>Alta en:</th>
									<td><?php echo $Socio->getCentro()->getNombre() ?></td>
									<th>Tiempo PC:</th>
									<td colspan="2"><?php echo Comun::segsATxt($Socio->getSaldo()); ?>
										disponibles.</td>
								</tr>
								<?php /*
								<tr>
									<th>&nbsp;</th>
									<td>&nbsp;</td>
									<th>Trabaja:</th>
									<td colspan="2"><?php echo ($Socio->getTrabaja()=='1') ? 'Si':'No' ?>
									</td>
								</tr>*/?>
								<tr>
									<th>&nbsp;</th>
									<td>&nbsp;</td>
									<th>&nbsp;</th>
									<td colspan="2">&nbsp;</td>
								</tr>

								<tr>
									<td colspan="6"><input type="button" name="botonVerDetalle"
										id="botonVerDetalle" value="Ver detalle"
										onClick="verDetalle(<?php echo $Socio->getId() ?>)" /> <input
										type="button" name="editar" id="editar" value="Modificar"
										onclick="location.href='<?php echo url_for('registro/edit?id='.$Socio->getId()) ?>'" />
										<input type="button" name="imprimir" id="imprimir"
										value="Imprimir usuario y clave"
										onclick="          
					            $.getJSON('<?php echo url_for('registro/imprimirUsuarioClave') ?>', 
					            	{ socio_id: <?php echo $Socio->getId() ?>}, 
					            	function(res){
   									 alert(res.msg);   									
									}); 
							" /> <input type="button" value="Reposición Cred."
										onclick="return reposicionCredencial('<?php echo $Socio->getId() ?>', costoRepCred);" />
										<input type="button" name="button" id="button"
										value="Dar de baja"
										onclick="
				             if ( confirm('¿Está seguro que desea dar de baja este socio?') ) {
					            $.getJSON('<?php echo url_for('registro/baja') ?>', 
					            	{ socio_id: <?php echo $Socio->getId() ?> }, 
					            	function(res){
   									 alert(res.msg);
									}); 
							}" /></td>
								</tr>
							</table>
						</div>
					</div>
				</form> <!-- END datosSocio --> <?php endforeach; ?>
			</td>
			<td width="25%" valign="top">
				<form>
					<div class="ui-widget">
						<div class="ui-widget-content ui-corner-all">
							<div class="ui-widget-header ui-corner-tl ui-corner-tr tsss"
								style="padding: 5px;">Buscar socio</div>

							<table>
								<tbody>
									<tr>
										<th>Folio</th>
										<td><input type="text" name="qFolio" filtro="numerico"
											size="20" maxlength="30" autocomplete="off" id="qFolio" 
											onkeyup="validaCampo(this,event)" /></td>
									</tr>
									<tr>
										<th>Nombre usuario</th>
										<td><input type="text" name="qUsuario" size="20" id="qUsuario"
											autocomplete="off" 
											onkeyup="validaCampo(this,event)" /></td>
									</tr>
									<tr>
										<th>Nombre completo</th>
										<td><input type="text" name="qNombre" filtro="alfabetico"
											size="20" id="qNombre" autocomplete="off" 
											onkeyup="validaCampo(this,event)" /></td>
									</tr>
									<tr>
										<th>Buscar</th>
										<td><input type="submit" id="boton-buscar-socios"
											value="Buscar socio" onClick="return caracteresBusqueda()" />
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</form>
			</td>
		</tr>
	</table>


	<?php  
	/*
	 *****************************************************************************************
	* Agregamos el formulario para enviar datos al punto de venta en caso de ser necesario	*
	* como el caso de repocision de credencial.												*
	* @author: silvio.bravo@enova.mx 21 Marzo 2014											*
	* @copyright Enova																		*
	* @category Reingenieria Mako															*
	*****************************************************************************************
	*/
	?>
	<form id="frmEnvioPV" method="post" action="/pos/new">
		<input type="hidden" id="redirected" name="redirected" value="1" /> 
		<input type="hidden" id="socioId" name="socioId" value="" /> 
			<input type="hidden" id="productoId" name="productoId" value="-1" />
	</form>
</div>
<style>
	#TitleModal{font-size: 16px; font-weight: bold; text-align: center; padding-top: 25px;}
</style>