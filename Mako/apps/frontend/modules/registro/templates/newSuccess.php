<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<?php use_javascript('registro/validaSocio.js') ?>
<?php use_javascript('jquery.ui.js') ?>

<?php use_stylesheet('tabs.css') ?>
<?php use_stylesheet('registro/tooltip.css') ?>
<?php use_javascript('JSON.js') ?>
<?php use_javascript('debug.js') ?>
<?php use_javascript('registro/mapas.js') ?>

<!--**********************************************************************/
/* Fecha: 2013-07-18 12:10
Actividad: Manejar cuadros de ayuda emergente en los campos
ID Actividad: M4F2-11
Responsable: MR. Script para manejar los mensajes.
/**********************************************************************-->
<?php use_javascript('registro/mensajes.js') ?>
<?php use_stylesheet('mensajes.css') ?>


<?php use_stylesheet('./registro/newSuccess.css')?>

<!-- Busca socio -->
<div id="buscadorSocio" style="display: none">
    <div class="contenedor-buscadores-socio">
        <div class="alertaMel"></div>
        <?php include_component('registro', 'buscaSocios') ?>
    </div>
</div>
<!-- /Busca socio -->


<!-- Busca homonimo -->
<div id="buscadorHomonimo" style="display: none">

    <div id="msgHomonimo"
        class="flash_ok ui-state-highlight ts sombra-delgada ui-corner-all"
        style="display: none; padding: 5px; margin-top: 10px; margin-bottom: 10px;">
        <span style="float: left; padding-right: 0.3em;"
            class="ui-icon ui-icon-info"></span> <strong>Se han detectado
            coincidencias con socio(s) registrado(s) anteriormente.<br> Por favor
            valide antes de registrar al socio.
        </strong>
    </div>

    <div id="msgFolioDuplicado"
        class="flash_ok ui-state-highlight ts sombra-delgada ui-corner-all"
        style="display: none; padding: 5px; margin-top: 10px; margin-bottom: 10px;">
        <span style="float: left; pading-right: 0.3em;"
            class="ui-icon ui-icon-info"></span> <strong>Se han detectado
            coincidencias con folio(s) registrado(s) anteriormente.<br> Por favor
            valide antes de registrar al socio.
        </strong>
    </div>

    <div id="buscadorHomonimoLista"></div>
</div>
<!-- /Busca homonimo -->

<!-- Ver detalle socio -->
<div id="verDetalleSocio"
    style="display: none"></div>
<!-- /Ver detalle socio -->


<form name="registro"
    id="registro" 
    action="<?php echo url_for('registro/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>"
    method="post"
    <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
    <?php if (!$form->getObject()->isNew()): ?>
    <input type="hidden" name="sf_method" value="put" />
    <?php endif; ?>

    <script type="text/javascript">

    /**
    *************************************************************************************************************************************
    *
    * @Title: Reingenieria Mako
    * @Author: sivlio.bravo@enova.mx
    * @Date: 25 Feb 2014
    * @Description: Evaluamos el comportamiento de cada uno de los combos que utilizamos para determinar la direcicon del socio.
    * Estado, municipio y colonia. Por cada cambio de ellos ejecutamos una funcion que va por ajax para cargar datos
    *
    *************************************************************************************************************************************
    **/
    $(document).ready(function(){
        InicioDePagina = true;

        tmpmunicipio = $("#tmpmunicipio").attr("value");
        tmpcolonia   = $("#tmpcolonia").attr("value");
        tmpcp        = $("#socio_cp").attr("value");

        if(tmpcp=="     "){
            tmpcp="";
            $("#socio_cp").attr("value","");

        }
        if(tmpmunicipio!=""){
            loadCascadaDatos($("#socio_id_estado"), $("#id_municipio"),"municipio", tmpmunicipio);
        }

        /*Evaluamos los eventos de cada componente (combo, y caja de texto cp) */

        dirAMapa();
        $("#socio_id_estado").live("change",function(){                 //Evaluamos el combo de estados
            loadCascadaDatos($(this), $("#id_municipio"),"municipio",0);
            $("#id_colonia").html("");
            dirAMapa();
        });


        $("#id_municipio").live("change",function(){                    //Evaluamos el combo de Municipios
            setComboTextToInputText($(this),$("#socio_municipio"));
            loadCascadaDatos($(this), $("#id_colonia"),"colonia",0);
            dirAMapa();
        });


        $("#id_colonia").live("change",function(){                      //Evaluamos el combo de colonia
            setComboTextToInputText($(this),$("#socio_colonia"));
            var cp=$(this).find('option:selected').attr("extra");
            $("#socio_cp").attr("value",cp);
            $("#socio_cp").attr("valueinicial",cp);
            dirAMapa();
        });


        $("#socio_cp").focus(function(){                               //Cargamos el codigo postal actual cuando entramos a la caja de cp
                var valor=$(this).attr("value");
                $(this).attr("valueinicial",valor);
            });



        $("#socio_cp").keyup(function(){                                //Evaluamos el cp cuando teclean sobre el

            if($(this).attr("value").length ==5){                           //llenaron un codigo de 5 digitos
                if($(this).attr("valueinicial")!= $(this).attr("value")){   //Lo que habia antes(lo que cargamos en el foco) es diferente a lo..
                    $(this).attr("valueinicial",$(this).attr("value"));     //..que hay ahorita
                    loadCascadaDatos($(this), $("#id_municipio"),"cp",0);
                }
            }
            else if($(this).attr("value").length ==0){
                $("#id_municipio").trigger("change");
            }

        });

        /* Terminamos la evaluacion de los eventos de los componentes para el caso de la direccion  */



        /*************  iniciamos funcionalidades   ************/


        /**
        * Asignamos el texto de una opcion seleccionada de un combo a una variable de tipo input
        * se utiliza para consumir el servicio que va a google maps y actualiza el mapa
        */

        function setComboTextToInputText(comboComponent, inputComponent){
            var etiqueta=comboComponent.find("option:selected").text();
            inputComponent.attr("value", etiqueta);
        }


        /**
        *
        *@description       :Funcion para traer datos por ajax consumiendo un servicio por rest.
        *@return            : JSON
        *@origenComponent   : Componente fuente que sirve como referencia para cargar los datos
        *@destinoComponent  : Componente destino donde se cargaran los nuevos valores
        *@llave             : indicador que se envia al servicio para saber que buscar.
        *@author            : silvio.bravo@enova.mx
        *
        **/


        function loadCascadaDatos(origenComponente, destinoComponente, llave, defaultvalue ){
            var myllave = llave;

            if(llave != "cp"){ //Si no es busqueda por codigo postal
                destinoComponente.html("");
                $("#socio_cp").attr("value",tmpcp);
            }

            destinoComponente.html('<option selected="selected">Buscando ' + llave + 's</option>');
            var id = origenComponente.attr("value");


            /** Vamos por ajax al servidor para obtener coincidencias apartir de una llave y un valor **/

            $.ajax({
                  url       : '<?php echo url_for('registro/buscaDatosDireccion') ?>',
                  type      : "POST",
                  dataType  : "json",
                  data      : {"llave": llave, "valorId": id },
                  success   : function(datos){

                       if(llave!="cp"){                         //funcionamiento normal (NO es codigo postal)
                           if(datos.success === true){
                                llenaCombo(destinoComponente, llave, datos.data,defaultvalue);

                               if(llave=="municipio" && tmpcolonia!=""){
                                    loadCascadaDatos($("#id_municipio"), $("#id_colonia"),"colonia", tmpcolonia);
                                    tmpcolonia="";
                                    tmpmunicipio="";
                                    tmpcp="";

                                }
                                else if(tmpmunicipio!=""){

                                    tmpcolonia="";
                                    tmpmunicipio="";
                                    tmpcp="";
                                    $("#id_municipio").trigger("change");
                                }

                           }
                       }
                       else{                                    //Funcionamiento especifico para Codigo postal
                           //$("#socio_cp").attr("value","");

                            llenaCombo(destinoComponente , "municipio"   , datos.data.municipios,datos.data.municipioId          );
                            llenaCombo($("#id_colonia")  , "colonia"     , datos.data.colonias,$("#id_colonia").attr("value")    );
                            if(datos.data.estadoId==0){
                                seleccionaComboByValue($("#socio_id_estado") ,15);
                                $("#socio_id_estado").trigger("change");
                                $("#socio_cp").attr("value","");
                                $("#socio_id_estado").trigger('change');
                                alert("Codigo Postal no Valido");
                            }
                            else{
                                seleccionaComboByValue($("#socio_id_estado") , datos.data.estadoId);
                            }
                           setComboTextToInputText($(this),$("#socio_municipio"));
                           setComboTextToInputText($(this),$("#socio_colonia"));

                           dirAMapa();


                       }

                  }

            });
        }


        /**
            * Selecciona una opcion de un combo buscando por value
         */

        function seleccionaComboByValue(componente, valor){

            componente.find("option:selected").each(function(){
                $(this).removeAttr("selected");
            });
            componente.find('option[value='+ valor +  ']').attr('selected','selected');
        }



            /*
            * Cargamos los combos de municipio y colonia a partirde un array de datos
            */


        function llenaCombo(destino, llave, datos, selectedValue){

            var nombreId    ="";
            var nombreValor ="";
            var nombreExtra ="";
            var cadena      ='<option value=""></option>';

            switch(llave){
                case "municipio":
                    nombreId    = "McpioId";
                    nombreValor = "McpioNombre";
                    nombreExtra = "McpioNombre";
                break;

                case "colonia":
                    nombreId    = "ClnaId";
                    nombreValor = "ClnaNombre";
                    nombreExtra = "ClnaCp";
                break;

                case 'nivel_estudio':
                    nombreId    = 'Id';
                    nombreValor = 'Nombre';
                    nombreExtra = '';
                    break;
            }

            if(datos.length ==1){
                selectedValue=datos[0][nombreId];
            }

            for(var n=0; n<datos.length; n++){
                cadena+='<option ' + ((selectedValue==datos[n][nombreId])?'selected="selected"':'');
                cadena+='  extra="' + datos[n][nombreExtra]  + '" value="'+ datos[n][nombreId]  +'">' + datos[n][nombreValor] + '</option>';
            }

            destino.html(cadena);
        }


    });

    /***************    fin de funcionalidad    ***************/


/**
 * **************************************************************************************************************************************
 *
 *
 *
 * Fin de funcionamiento para encontrar la direccion
 *
 *
 *
 * **************************************************************************************************************************************
 */


    $(function() {
        //$("#tabs").tabs();
        var $tabs = $('#tabs').tabs();
        var next=0;
        var i = 0;

        $(".ui-tabs-panel").each(function(i){

          var totalSize = $(".ui-tabs-panel").size() - 1;
          if (i != totalSize) {

              next = i+1;
              //$(this).append("<a href='#' class='next-tab mover ui-state-default' rel='" + next + "'>Siguiente &#187;</a>");
          }

          if (i != 0) {
              prev = i-1;
              //$(this).append("<a href='#' class='prev-tab mover ui-state-default' rel='" + prev + "'>&#171; Anterior</a>");
          }
            i++;
        });

        $('.next-tab, .prev-tab').click(function() {
                //alert($(this).attr("rel"));
               $tabs.tabs('select', Number($(this).attr("rel")));
               return false;
        });

        //Buscador Socio
        var $wcm= $("#buscadorSocio").dialog(
            {
                        modal: true,
                        autoOpen: false,
                        position: ['center','center'],
                        title:  'Buscador de socio',
                        //setter
                        height: 530,
                        width: 300,
                        buttons: {
                            'Cerrar': function()
                            {
                                $(this).dialog('close');
                            },
                            'Cancelar': function()
                            {
                                $(this).dialog('close');
                            }

                        }
            });

            //El boton para abrir el buscador de socio referente.
            $("#socio-ref").click(function()
            {
                $(".contenedor-buscadores-socio").attr('id','buscador-socio-ref');

                //El bind de buscador de socio referente
                $("#buscador-socio-ref").bind('socio.seleccionado',function(ev, socio_ref_id, socio_ref_nombre){
                        $('#socio_socio_ref_id').val(socio_ref_id);
                        $('#socio_ref_nombre').html(socio_ref_nombre);
                        $wcm.dialog('close');
                        $("#buscador-socio-ref").unbind('socio.seleccionado');
                });

                $wcm.dialog('open');
			});

			$("#socio_tr_clna").click(function()
		  {
				if(this.checked){
					$("#id_colonia").attr('disabled','disabled').removeAttr('requerido').css('border-color','');
                    $("#id_colonia option[value='']").attr('selected', 'selected');
                    $('#socio_colonia').val('');
				}else{
					$("#id_colonia").removeAttr('disabled').attr('requerido','1');
				}
			});


            //El boton para abrir el buscador de padre del socio.
            $("#boton-buscador-padre").click(function()
            {

                $(".contenedor-buscadores-socio").attr('id','buscador-padre');
                //El bind de buscador de padre del socio
                $("#buscador-padre").bind('socio.seleccionado',function(ev, socio_ref_id, socio_ref_nombre){
                        $('#socio_socio_padre_id').val(socio_ref_id);
                        $('#socio_padre_nombre').html(socio_ref_nombre);
                        $wcm.dialog('close');
                        $("#buscador-padre").unbind('socio.seleccionado');
                });

                $wcm.dialog('open');
            });


            //El boton para abrir el buscador de madre del socio.
            $("#boton-buscador-madre").click(function()
            {
                $(".contenedor-buscadores-socio").attr('id','buscador-madre');
                //El bind de buscador de madre del socio
                $("#buscador-madre").bind('socio.seleccionado',function(ev, socio_ref_id, socio_ref_nombre){
                        $('#socio_socio_madre_id').val(socio_ref_id);
                        $('#socio_madre_nombre').html(socio_ref_nombre);
                        $wcm.dialog('close');
                        $("#buscador-madre").unbind('socio.seleccionado');
                });

                $wcm.dialog('open');
            });


            //
            //Homonimo
            var $wcmHom= $("#buscadorHomonimo").dialog(
            {
                        modal: true,
                        autoOpen: false,
                        position: ['center','center'],
                        //title:    'Homónimos detectados',
                        //setter
                        height: 530,
                        width: 300,
                        buttons: {
                            'Cerrar': function()
                            {
                                $(this).dialog('close');
                            },
                            'Cancelar': function()
                            {
                                $(this).dialog('close');
                            }

                        }
            });

            $('#socio_apemat').blur(function()
            {
                 var UrlListaHomonimos = "<?php echo url_for('registro/buscaSocio') ?>";
                 var nombreCompleto = document.getElementById('socio_nombre').value + ' ' + document.getElementById('socio_apepat').value + ' ' + document.getElementById('socio_apemat').value;
                 var titulo = 'Homonimo detectado';
                 var texto ='Se han detectado coincidencias con socio(s) registrado(s) anteriormente.<br> Por favor valide antes de registrar al socio.';
                    $("#buscadorHomonimoLista").load(

                         UrlListaHomonimos,
                         {
                             qNombre: nombreCompleto
                         },
                         function(v1,v2){$("#buscadorHomonimo").trigger('socio.homonimos.encontrados',[titulo,texto]);}

                );

            });

            //Escuchamos evento socio.homonimos.encontrados y mostramos la alerta en caso de q haya encontrado algo
            $("#buscadorHomonimo").bind('socio.homonimos.encontrados',function(ev, titulo, texto){

                $("#buscadorHomonimo a").text('Ver detalle');
                var encontrados = $("#buscadorHomonimo a").length;
                if(encontrados > 0){
                    $("#msgHomonimo").show();
                    $("#msgFolioDuplicado").hide();
                    $wcmHom.dialog('open');
                    $wcmHom.dialog( 'option' , 'title' , titulo);

                }
            });

            $("#buscadorHomonimo").bind('socio.seleccionado',function(ev, socio_ref_id, socio_ref_nombre){
                verDetalle(socio_ref_id);
            });

            //Ver detalle socio
            var $wcmDetalle= $("#verDetalleSocio").dialog(
            {
                        modal: true,
                        autoOpen: false,
                        position: ['center','center'],
                        title:  'Ver detalle',
                        //setter
                        height: 550,
                        width: 1100,
                        buttons: {
                            'Cerrar': function()
                            {
                                $(this).dialog('close');
                            }
                        }
            });


            function verDetalle(idSocio)
                    {
                         var UrlVerDetalle = "<?php echo url_for('registro/verDetalle') ?>";
                            $("#verDetalleSocio").load(
                                UrlVerDetalle,
                                 {
                                     id: idSocio
                                 }

                        );
                            $wcmDetalle.dialog('open');
                    };
            //

            //Verifica Folio
            $('#socio_folio').blur(function()
            {
             var UrlVerificaFolio = "<?php echo url_for('registro/buscaSocio') ?>";
             var folioV = document.getElementById('socio_folio').value;
                    $("#buscadorHomonimoLista").load(

                         UrlVerificaFolio,
                         {
                             qFolio: folioV
                         },
                         function(v1,v2){$("#buscadorHomonimo").trigger('socio.folio.duplicado');}

                    );

            });

            //Escuchamos evento del folio verificado, socio.seleccionado
            $("#buscadorHomonimo").bind('socio.folio.duplicado',function(ev)
            {
                var encontrados = $("#buscadorHomonimo a").length;
                if(encontrados > 0){
                    document.getElementById('socio_folio').value = '';
                    var titulo = 'Folio duplicado';
                    var texto = '';
                    $("#msgHomonimo").hide();
                    $("#msgFolioDuplicado").show();
                    $wcmHom.dialog('open');
                    $wcmHom.dialog( 'option' , 'title' , titulo);
                }
            });
            //Limpiamos los campos de telefono
            $(".tel").blur(function(){
                $valor = $(this).val();
                $(this).val($valor.replace( /\D+/ , '' ));
            });

            //Habilitamos gui una vez que se ha cargado todo
            $("#loader").hide();
            $("#page-wrap").fadeIn(1500);
			/*******************************************************
			**
			**   @@ LGL   2015-01-05
			**  Por necesidad de negocio se permite el cambio de nombre de usuario en RIA
			**  Se deshabilita modificación para NMP
			**
			********************************************************/
            /*if(window.location.href.indexOf('edit') != -1){
                //Bloqueamos campos de nombre y apellido paterno para evitar que el nombre de usuario (que necesita NMP) no cambie
                document.getElementById('socio_nombre').setAttribute('readonly', true);
                document.getElementById('socio_apepat').setAttribute('readonly', true);
			}*/
    });

    </script>

    <?php if($form->hasErrors()): ?>
    <div class="flash_error ui-state-error ts sombra-delgada ui-corner-all"
        style="padding: 5px; margin-top: 10px; margin-bottom: 10px;">
        <span style="float: left; pading-right: 0.3em;"
            class="ui-icon ui-icon-info"></span>
        <h2>
            <strong>Se han detectado errores al llenar la forma, por favor revisa
                los siguientes mensajes</strong>
        </h2>
        <ul>
            <?php foreach($form->getFormFieldSchema() as $name=>$formField):?>

            <?php if ($formField->hasError()):?>
            <li><?php echo $formField->renderError()?></li>
            <?php endif;?>
            <?php endforeach;?>
        </ul>
    </div>
    <?php endif; ?>
    <div id="loader" align="center">
        <img src="/imgs/ajax-circle.gif" width="64" height="64" />
    </div>
    <div id="page-wrap" class="sombra" style="display: none;">

        <!--Reingenieria Mako AP. ID 8. Responsable: FE. Fecha: 02-04-2014. Descripción del cambio: Se cambio el orden de las pestañas-->
        <!--Reingenieria Mako AP. ID 9. Responsable: FE. Fecha: 02-04-2014. Descripción del cambio: Se mejoro los 'Tool tips'-->
        <div id="tabs">
            <ul id="tabMenu" class="tsss">
                <?php 
                /*
                * Reingenieria  Mako Registro Socios. ID #16
                * Responsable:  (BP) Bet Gader Porcayo Juárez
                * Fecha:        29-08-2014 
                * Descripción:  Nombre de la pestaña a Datos básico del socio
                */
                ?>
                <li><a href="#personales">Datos b&aacutesicos del socio</a></li>
                <?php 
                /*
                * Reingenieria  Mako Registro Socios. ID #16
                * Responsable:  (BP) Bet Gader Porcayo Juárez
                * Fecha:        29-08-2014 
                * Descripción:  Nombre de la pestaña de Escolaridad a Datos complementarios del socio
                */
                ?>
                <li><a href="#escolaridad">Datos complementarios del socio</a></li>
                <?php 
                /*
                * Reingenieria  Mako Registro Socios. ID #42
                * Responsable:  (BP) Bet Gader Porcayo Juárez
                * Fecha:        29-08-2014 
                * Descripción:  Se habilita NSE y se cambia el nombre de la etiqueta a Datos para solicitud de beca
                */
                ?>
                <?php /*<li><a href="#solicitudBeca" >Datos para solicitud de beca</a></li>*/?>
                <?php /*
                <li style="display: none;"><a href="#ocupacion">Ocupación</a></li>
                <li><a href="#acercamiento">Medio de acercamiento</a></li>
                */?>
                <?php
                /************************************************
                 **      @@ LGL     2013-07-08
                **      Se deshabilita pantalla Nivel Socio-Economico
                **      Según Matriz 4M Fase II
                **      Id Actividad : 41


                <li><a href="#nse">NSE</a></li>
                 */?>

            </ul>


            <div id="personales" class="ui-tabs-panel">

                <h3>Identificación</h3>
                <div>

                    <?php include_partial('registro/capturaFoto') ?>
                    <!--
                           Fecha: 12-07-2013. Inicio.
                           Sugerencia/Acciones: Según matriz 4M RegSocios (Ocultar icono de Huella digital.).
                           ID Seguimiento: F2_12.
                           Responsable: YD. Sólo se oculta la opción.
                        -->
                    <?php //include_partial('registro/capturaHuella') ?>
                    <!-- Fin 12-07-2013 -->
                    <p>

            <!--**********************************************************************/
            /* Fecha: 2013-07-18 12:10
            Actividad: Manejar cuadros de ayuda emergente en los campos: Fecha de nacimiento
            ID Actividad: M4F2-11
            Responsable: MR. Se agrego el código para los eventos onblur y onfocus, asi como una etiqueta para
                        mostrar el mensaje.
            /**********************************************************************-->

            <?php 
            /*
            * Reingenieria  Mako Registro Socios. ID #20
            * Responsable:  (BP) Bet Gader Porcayo Juárez
            * Fecha:        29-08-2014 
            * Descripción:  Se hacen obligatorios: Nombre, Apellido paterno, Fecha de nacimiento, 
            *               Género, Télefono fijo o Télefono celular, Estado, Municipio, Colonia, 
            *               Folio credencial, Discapacidad, Habilidad en el manejo de la computadora, 
            *               Dominio de Inglés, Acceso a la tecnología, ¿A que se dedica?
            */
            ?>

                    <table>
                        <tr>
                            <td><?php echo $form['nombre']->renderLabel(null,array('class'=>'RequiredLabel')) ?></td>
                            <td><?php echo $form['nombre']->render(array('requerido'=>'1','filtro'=>'alfabetico','size'=>'50','maxlength'=>'30','tabindex'=>1)) ?>
                            </td>
                            <?php echo $form['nombre']->renderError() ?>
                        </tr>
                        <tr>
                            <td><?php echo $form['apepat']->renderLabel('Apellido Paterno',array('class'=>'RequiredLabel')) ?>
                            </td>
                            <td><?php echo $form['apepat']->render(array('requerido'=>'1','filtro'=>'alfabetico','size'=>'50','maxlength'=>'30','tabindex'=>1)) ?>
                            </td>
                            <?php echo $form['apepat']->renderError() ?>
                        </tr>
                        <tr>
                            <td><?php echo $form['apemat']->renderLabel('Apellido Materno') ?>
                            </td>
                            <td><?php echo $form['apemat']->render(array('requerido'=>'0','filtro'=>'alfabetico','size'=>'50','maxlength'=>'30','tabindex'=>1)) ?>
                            </td>
                            <?php echo $form['apemat']->renderError() ?>
                        </tr>
                        <tr>
                            <td><?php echo $form['fecha_nac']->renderLabel('Fecha de Nacimiento',array('class'=>'RequiredLabel')) ?></td>
                            <!--<td>< ?php echo $form['fecha_nac']->render(array('requerido'=>'1','tabindex'=>1, 'onfocus'=>'mensaje_muestra(this);', 'onblur'=>'mensaje_oculta(this);')) ? ></td>-->
                            <td><?php echo $form['fecha_nac']->render(array('requerido'=>'1','tabindex'=>1)) ?></td>
                            <td>
                                <a href="#" class="tooltip" msg="La edad mínima de un socio es 4 años.">
                                    <div class="ui-icon ui-icon-help" ></div>
                                </a>
                            </td>
                            <?php echo $form['fecha_nac']->renderError() ?>
                        </tr>
                        <?php 
                        /*
                        * Reingenieria  Mako Registro Socios. ID #1
                        * Responsable:  (BP) Bet Gader Porcayo Juárez
                        * Fecha:        29-08-2014 
                        * Descripción:  Ocultar Tutor responsable
                        */
                        ?>
                        <tr id="campo-tutor-responsable" style="display: none;">
                            <td>
                                <?php echo $form['tutor_responsable']->renderLabel("Tutor responsable") ?>
                            </td>
                            <td>
                                <?php echo $form['tutor_responsable']->render(array('tabindex'=>1, 'filtro' => 'alfabetico', 'disabled' => true)); ?>
                            </td>
                            <td>
                                <?php echo $form['tutor_responsable']->renderError() ?>
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo $form['sexo']->renderLabel('Género',array('class'=>'RequiredLabel')) ?></td>
                            <td class="radio-buttons"><?php echo $form['sexo']->render(array('tabindex'=>'2', 'requerido'=>'1')) ?>
                            </td>
                            <?php echo $form['sexo']->renderError() ?>
                        </tr>
                        <tr>
                            <?php // Reingenieria Mako AP. ID 2. Responsable: FE. Fecha: 31-03-2014. Descripción del cambio: Se muestra 'Estado civil' en la pestaña 'Datos personales'?>
                            <td class="verticalTop"><?php echo $form['estado_civil_id']->renderLabel() ?></td>
                            <td><?php echo $form['estado_civil_id']->render(array('tabindex'=>1,'filtro'=>'', 'size'=>'10')) ?>
                            </td>
                            <?php echo $form['estado_civil_id']->renderError() ?>
                            <?php //Fecha: 31-03-2014. Fin?>
                        </tr>
                    </table>
                </div>
                <h3>Contacto</h3>
                <div>
                    <!--**********************************************************************/
            /* Fecha: 2013-07-18 12:10
            Actividad: Manejar cuadros de ayuda emergente en los campos: celular y correo
            ID Actividad: M4F2-11
            Responsable: MR. Se agrego el código para los eventos onblur y onfocus, asi como una etiqueta para
                        mostrar el mensaje.
            /**********************************************************************-->
                    <!--
            Fecha: 25-06-2013. Inicio.
            Sugerencia/Acciones: Según matriz 4M RegSocios (-Verificar el número de dígitos en los números telef
            ónicos y su formato en general. -Definir el tamaño del campo (con o sin 044). Investigar la forma co
            mo se maneja el número celular en cada estado, y países colindantes.)
            ID Seguimiento: 36,67.
            Responsable: FE. Se añadió el evento onChange con la llamada a la función validaTel() y validaCel(),
            de esta forma se obligar al usuario a introducir el teléfono con un formato válido.
            -->
            <?php 
            /*
            * Reingenieria  Mako Registro Socios. ID #5
            * Responsable:  (BP) Bet Gader Porcayo Juárez
            * Fecha:        29-08-2014 
            * Descripción:  Se cambia texto de las etiquetas Telefonos y email, también, se añade texto para que el usuario capture al menos un número telefónico.
            */
            ?>
                    <table>
                        <tr>
                            <td>
                            </td>
                            <td colspan="2">
                            <div class="telefono_requerido">Es necesario que capture al menos un número telefónico.</div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <?php echo $form['tel']->renderLabel("Teléfono fijo (a 10 dígitos)") ?>
                            </td>
                            <td><?php echo $form['tel']->render(array('filtro'=>'numerico', 'nouppercase'=>'1', 'maxlength'=>'10','tabindex'=>1,'autocomplete'=>'off','onBlur'=> 'validaTelefono()', "onkeypress"=>"ValidaSoloNumeros()")) ?>
                            </td>
                            <?php echo $form['tel']->renderError() ?>
                        </tr>
                        <tr>
                            <td><?php echo $form['celular']->renderLabel("Teléfono celular (a 10 dígitos)") ?>
                            </td>
                            <!--<td>< ?php echo $form['celular']->render(array('filtro'=>'numerico', 'nouppercase'=>'1', 'maxlength'=>'10','tabindex'=>1,'autocomplete'=>'off','onBlur'=> 'validaCelular();mensaje_oculta(this);','onFocus'=> 'validaCelular();mensaje_muestra(this);')) ? ></td>-->
                            <td><?php echo $form['celular']->render(array('filtro'=>'numerico', 'nouppercase'=>'1', 'maxlength'=>'10','tabindex'=>1,'autocomplete'=>'off','onBlur'=> 'validaCelular()', "onkeypress"=>"ValidaSoloNumeros()")) ?>
                            </td>
                            <td>
                                <a href="#" class="tooltip" msg="No capture 044 o 045 al inicio.">
                                        <div class="ui-icon ui-icon-help" ></div>
                                </a>
                            </td>
                            <!--< ?php echo '<span id="socio_celular_mensaje" class="msjayu"></span>'; ?>-->
                            <?php echo $form['celular']->renderError() ?>
                            <!--
            Fecha: 25-06-2013. Fin
            -->

                            <!--
            Fecha: 24-06-2013. Inicio.
            Sugerencia/Acciones: Según matriz 4M RegSocios (Revisar librerías para la validación de correo elect
            rónico).
            ID Seguimiento: 38 .
            Responsable: FE. Se añadió el evento onChange con la llamada a la función validaEmail(), de esta for
            ma se obligar al usuario a introducir una dirección de email con un formato válido.
            -->
                        </tr>
                        <tr>
                            <?php 
                            /*
                            * Reingenieria  Mako Registro Socios. ID #6
                            * Responsable:  (BP) Bet Gader Porcayo Juárez
                            * Fecha:        29-08-2014 
                            * Descripción:  Campo de email no es obligatorio
                            */
                            ?>
                            <td><?php echo $form['email']->renderLabel("Correo electrónico") ?></td>
                            <!--<td>< ?php echo $form['email']->render(array('filtro'=>'correo','nouppercase'=>'1','tabindex'=>1,'autocomplete'=>'off','onBlur' => 'validaEmail();mensaje_oculta(this);','onFocus'=> 'mensaje_muestra(this);')) ? ></td>-->
                            <td><?php echo $form['email']->render(array('filtro'=>'correo','nouppercase'=>'1','tabindex'=>1,'autocomplete'=>'off','onBlur' => 'validaEmail()')) ?>
                            </td>
                            <!--< ?php echo '<span id="socio_email_mensaje" class="msjayu"></span>'; ? >-->
                            <?php echo $form['email']->renderError() ?>

                            <!--
            Fecha: 24-06-2013. Fin
            -->
                        </tr>
                    </table>
                </div>
                <?php 
                /*
                * Reingenieria  Mako Registro Socios. ID #8
                * Responsable:  (BP) Bet Gader Porcayo Juárez
                * Fecha:        29-08-2014 
                * Descripción:  Ocultar el campo de Contacto de emergencia,
                * Nota:         No se oculta con comentarios de PHP debido a que hay un error sino es enviado (se envia vacio)
                */
                ?>
                <h3 style="display: none;">Contacto en caso de emergencia</h3>
                <div style="display: none;">
                    <div class="clearfix">
                        <div class="izq">
                            <?php echo $form['contacto_emergencias']->renderLabel("Contacto") ?>
                            <?php echo $form['contacto_emergencias']->render(array('tabindex'=>1, 'maxlength'=>'30')) ?>
                            <?php echo $form['contacto_emergencias']->renderError() ?>
                            <br>
                            <?php echo $form['telefono_emergencia']->renderLabel("Teléfono") ?>
                            <?php echo $form['telefono_emergencia']->render(array('filtro'=>'numerico', 'tabindex'=>1, 'nouppercase'=>'1', 'maxlength'=>'10', facilitador)) ?>
                            <?php echo $form['telefono_emergencia']->renderError() ?>
                        </div>
                    </div>
                </div>
                <h3>Domicilio</h3>
                <div class="clearfix">
                    <div class="izq">
                        <table id="domicilio">
                            <tbody>
                                <tr style="margin-top: 0px;">
                                    <td style="margin-top: 0px; padding-top: 0px;">
                                        <?php echo $form['id_estado']->renderLabel('Estado',array('class'=>'RequiredLabel')) ?>
                                        <?php echo $form['id_estado']->renderError() ?>
                                    </td>
                                    <td style="margin-top: 0px; padding-top: 0px;">
                                        <?php echo $form['id_estado']->render(array('tabindex'=>1, 'requerido'=>1)) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php echo $form['id_municipio']->renderLabel('Municipio',array('class'=>'RequiredLabel')) ?>
                                        <?php echo $form['id_municipio']->renderError() ?>

                                        <?php echo $form['municipio']->render(array('filtro'=>'alfabetico','maxlength'=>'30','tabindex'=>1)) ?>
                                        <?php echo $form['municipio']->renderError() ?>
                                    </td>
                                    <td>
                                    <?php echo $form['id_municipio']->render(array('filtro'=>'alfabetico','maxlength'=>'30','tabindex'=>1, 'id'=>'id_municipio','requerido'=>1)) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="verticalTop">
										<?php echo $form['id_colonia']->renderLabel('Colonia',array('class'=>'RequiredLabel')) ?>
                                        <?php echo $form['id_colonia']->renderError() ?>
                                       <?php echo $form['colonia']->render(array('filtro'=>'alfanumeric','maxlength'=>'30','tabindex'=>1)) ?>
                                        <?php echo $form['colonia']->renderError() ?>
                                    </td>
                                    <td>
										<?php echo $form['id_colonia']->render(array('filtro'=>'alfabetico','maxlength'=>'30','tabindex'=>1, 'id'=>'id_colonia','requerido'=>1)) ?>
										<table>
                                            <tr>
                                                <td>
										          <?php echo $form['tr_clna']->render(); ?>
										        </td>
                                                <td>
                                                  <?php echo '<small style="display: block;width: 70px;">' . $form['tr_clna']->renderLabel('Pendiente', array('class'=>'custom1')) . "</small";  ?>
                                                </td>
                                                <td>
										            <a href="#" class="tooltip" msg="Solo seleccione si la colonia requerida no se encuentra en la lista.">
                                                        <div class="ui-icon ui-icon-help" ></div>
                                                    </a>
										        </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php echo $form['cp']->renderLabel("C.P.") ?>
                                        <?php echo $form['cp']->renderError() ?>
                                    </td>
                                    <td>
                                        <?php echo $form['cp']->render(array('filtro'=>'numerico','size'=>'5','maxlength'=>'5','tabindex'=>1)) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php echo $form['calle']->renderLabel() ?>
                                        <?php echo $form['calle']->renderError() ?>
                                    </td>
                                    <td>
                                        <?php echo $form['calle']->render(array('filtro'=>'alfanumeric','maxlength'=>'50','tabindex'=>1)) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php echo $form['num_ext']->renderLabel("Número exterior") ?>
                                        <?php echo $form['num_ext']->renderError() ?>
                                    </td>
                                    <td>
                                        <?php echo $form['num_ext']->render(array('filtro'=>'alfanumeric','maxlength'=>'25','tabindex'=>1)) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php echo $form['num_int']->renderLabel("Número interior") ?>
                                        <?php echo $form['num_int']->renderError() ?>
                                    </td>
                                    <td>
                                        <?php echo $form['num_int']->render(array('filtro'=>'alfanumeric','maxlength'=>'20','tabindex'=>1)) ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="der" style="width:415px; height:270px; margin-top:6px; float:right">
                        <?php echo $form['lat']->render() ?>
                        <?php echo $form['long']->render() ?>
                        <input type="hidden" name="centro_lat" id="centro_lat"
                            value="<?php echo isset($Centro)? $Centro->getLat():'';  ?>"> <input
                            type="hidden" name="centro_long" id="centro_long"
                            value="<?php echo isset($Centro)? $Centro->getLong():''; ?>">
                        <?php echo $form['dirgmaps']->render() ?>
                        <div id="map_canvas" class="sombra-delgada"></div>
                    </div>
                </div>
                <h3>Membresía</h3>
                <br>
                <?php echo $form['folio']->renderLabel('Folio credencial',array('class'=>'RequiredLabel')) ?>
                <?php echo $form['folio']->render(array('requerido'=>'1','filtro'=>'numerico','size'=>'15','maxlength'=>'13','autocomplete'=>'off','tabindex'=>1)) ?>
                <?php echo $form['folio']->renderError() ?>
                <br /> <br />
                <?php if(!$form->isNew()): ?>
                <?php echo $form['clave']->renderLabel('Clave',array('class'=>'RequiredLabel')) ?>
                <?php echo $form['clave']->render(array('value'=>$form->getObject()->getClave(),'requerido'=>'1','size'=>'15','maxlength'=>'30','tabindex'=>1)) ?>
                <?php echo $form['clave']->renderError() ?>
                <br>
                <?php echo $form['clave_confirmacion']->renderLabel('Clave confirmación',array('class'=>'RequiredLabel')) ?>
                <?php echo $form['clave_confirmacion']->render(array('value'=>$form->getObject()->getClave(),'requerido'=>'1','size'=>'15','maxlength'=>'30','tabindex'=>1)) ?>
                <?php echo $form['clave_confirmacion']->renderError() ?>
                <?php endif; ?>

                <?php 
                /*
                * Reingenieria  Mako Registro Socios. ID #25
                * Responsable:  (BP) Bet Gader Porcayo Juárez
                * Fecha:        29-08-2014 
                * Descripción:  Se agrega escolaridad
                */
                ?>
                <h3>Escolaridad</h3>
                <table>
                    <tr>
                        <td><?php echo $form['estudia']->renderLabel('¿Estudia actualmente? <img src="/css/obligatorio.png"/>') ?></td>
                        <?php if($form->getObject()->isNew()) {?>
                            <td class="radio-buttons"><?php echo $form['estudia']->render(array('onChange' => 'estudiosActual(this)','requerido'=>'1','tabindex'=>2)) ?>
                        <?php } else {?>
                            <td class="radio-buttons">
                                <ul class="radio_list">
                                    <li>
                                        <input name="socio[estudia]" type="radio" value="true" id="socio_estudia_true" onchange="estudiosActual(this)" requerido="1" tabindex="2" <?php echo $form['estudia']->getValue() ? 'checked="checked"' : '' ?>>
                                        &nbsp;<label for="socio_estudia_true">Si</label>
                                    </li>
                                    <li>
                                        <input name="socio[estudia]" type="radio" value="false" id="socio_estudia_false" onchange="estudiosActual(this)" requerido="1" tabindex="2" <?php echo $form['estudia']->getValue() ? '' : 'checked="checked"' ?>>
                                        &nbsp;<label for="socio_estudia_false">No</label>
                                    </li> 
                                </ul>
                            </td>
                        <?php } ?>


                        </td>
                        <?php echo $form['estudia']->renderError() ?>
                    </tr>
                    <tr>
                        <?php 
                        /*
                        * Reingenieria  Mako Registro Socios. ID #40
                        * Responsable:  (BP) Bet Gader Porcayo Juárez
                        * Fecha:        29-08-2014 
                        * Descripción:  Se mueve el campo nivel de estudios en curso y se cambia el nombre de la etiqueta a Nivel de estudios
                        */
                        ?>
                        <td class="verticalTop"><?php echo $form['nivel_id']->renderLabel('Nivel de estudios actual o último <span id="requeridoNivelEstudios"><img src="/css/obligatorio.png"/></span>') ?>
                        </td>
                        <!--<td>< ?php echo $form['nivel_id']->render(array('onChange' => 'estudiosNivelActual(this.value)', 'onfocus'=>'mensaje_muestra(this);', 'onblur'=>'mensaje_oculta(this);')) ? >-->
                        <td><?php echo $form['nivel_id']->render(array('size'=>8, 'requerido'=>1)) ?>
                        </td>
                        <?php 
                        /*
                        * Reingenieria  Mako Registro Socios. ID #25
                        * Responsable:  (BP) Bet Gader Porcayo Juárez
                        * Fecha:        29-08-2014 
                        * Descripción:  Se agrega tooltip
                        */
                        ?>
                        <td>
                            <a href="#" class="tooltip" msg="Posgrado incluye maestría, especialidad y doctorado">
                                <div class="ui-icon ui-icon-help" ></div>
                            </a>
                        </td>
                        <!--< ?php echo '<span id="socio_nivel_id_mensaje" class="msjayu"></span>'; ? >-->
                        <?php echo $form['nivel_id']->renderError() ?>
                    </tr>   
                </table>

                <?php 
                /*
                * Reingenieria  Mako Registro Socios. ID #26 y 41
                * Responsable:  (BP) Bet Gader Porcayo Juárez
                * Fecha:        29-08-2014 
                * Descripción:  Se agrega ocupacion
                */
                ?>
                <h3>Ocupación</h3>
                <table>
                    <?php 
                    /*
                    * Reingenieria  MAKO-ARFS-002 PUNTO 1 2a Rev
                    * Responsable:  (BP) Bet Gader Porcayo Juárez
                    * Fecha:        08/10/2014
                    * Descripción:  Se cambia el icono ! por el ?, también se coloca alieada a la parte superior las etiquetas del formulario
                    */
                    ?>
                    <!--
                    <tr>
                        <?php 
                        /*
                        * Reingenieria  Mako Registro Socios. ID #11
                        * Responsable:  (BP) Bet Gader Porcayo Juárez
                        * Fecha:        29-08-2014 
                        * Descripción:  Se mueve campo trabaja a la pestaña de datos básico
                        */
                        ?>
                        <td class="td-first">
                            <?php echo $form['trabaja']->renderLabel('¿Trabaja? <img src="/css/obligatorio.png"/>') ?>
                        </td>
                        <?php if($form->getObject()->isNew()) {?>
                            <td class="radio-buttons">
                                <?php echo $form['trabaja']->render(array('onChange' => 'trabaja()','requerido'=>'1')) ?>
                            </td>
                        <?php } else { ?>
                            <td class="radio-buttons">
                                <ul class="radio_list">
                                    <li>
                                        <input name="socio[trabaja]" type="radio" value="true" id="socio_trabaja_true" onchange="trabaja()" requerido="1" <?php echo $form['trabaja']->getValue() ? 'checked="checked"' : '' ?>>
                                        &nbsp;<label for="socio_trabaja_true">Si</label>
                                    </li>
                                    <li>
                                        <input name="socio[trabaja]" type="radio" value="false" id="socio_trabaja_false" onchange="trabaja()" requerido="1" <?php echo $form['trabaja']->getValue() ? '' : 'checked="checked"' ?>>
                                        &nbsp;<label for="socio_trabaja_false">No</label>
                                    </li>
                                </ul>
                            </td>
                        <?php } ?>
                        <?php echo $form['trabaja']->renderError() ?>
                    </tr>
                    -->
                    <tr>
                        <?php 
                        /*
                        * Reingenieria  Mako Registro Socios. ID #2
                        * Responsable:  (BP) Bet Gader Porcayo Juárez
                        * Fecha:        29-08-2014 
						* Descripción:  Cambio de texto Ocupación a ¿A qué se dedica?
						* Modificación: Se crea lista con opción multiple para campo OCUPACIÓN
						* Responsable: Luis González
						* Fecha: 09-Oct-2014
                        */
                        ?>
                        <td class="verticalTop">
                            <?php echo $form['ocupacion_id']->renderLabel('¿A qué se dedica?',array('class'=>'RequiredLabel')) ?>
                            <?php /*<label for="noOcupacion">Sin ocupación</label>
                            <input type="checkbox" name="noOcupacion" value="" id="socio_noacceso">*/?>
                        </td>
                        <td id="OcupacionTD">
							<?php //echo $form['ocupacion_id']->render(array('size'=>"10",'requerido'=>1)) ?>
							<?php echo $form['socio_ocupacion']->render(array()); ?>
                        </td>
                        <td>
						<?php echo $form['socio_ocupacion']->renderError() ?>
						
                    </td>
                </tr>
                </table>

                <?php 
                /*
                * Reingenieria  Mako Registro Socios. ID #12
                * Responsable:  (BP) Bet Gader Porcayo Juárez
                * Fecha:        29-08-2014 
                * Descripción:  Se mueve habilidades a la pestaña de datos básico
                */
                ?>
                <h3>Habilidades</h3>
                <table>
			<tr>
 	                       <td class="verticalTop"><?php echo $form['discapacidad_id']->renderLabel(null,array('class'=>'RequiredLabel')) ;?>
				</td>
				<td id="DiscapacidadTD">
                <?php echo $form['socio_discapacidad']->render(array());	?>
                <td>   
                    <?php echo $form['socio_discapacidad']->renderError();?>
                        <a href="#" class="tooltip" msg="Discapacidad del aprendizaje .- Dificultad para aprender procesos fundamentales como leer, escribir o hacer cálculos numéricos">
                            <div class="ui-icon ui-icon-help" ></div>
                        </a>
				</td>
			</tr>
                    <tr>
                        <td class="verticalTop"><?php echo $form['habilidad_informatica_id']->renderLabel('Habilidad en el manejo de la computadora',array('class'=>'RequiredLabel')) ?>
                        </td>
                        <!--<td>< ?php echo $form['habilidad_informatica_id']->render(array('onfocus'=>'mensaje_muestra(this);', 'onblur'=>'mensaje_oculta(this);')) ? > </td>-->
                        <td><?php echo $form['habilidad_informatica_id']->render(array('onfocus'=>'mensaje_muestra(this);', 'onblur'=>'mensaje_oculta(this);','size'=>'10','requerido'=>1)) ?>
                        </td>
                        <td>
                            <a href="#" class="tooltip" msg="NINGUNA: Nunca ha usado una computadora.<?php echo "\n\n"?>
BÁSICO: Puede manejar el teclado y el ratón, crear documentos y consultar información en Internet.<?php echo "\n\n"?>
INTERMEDIO:Puede elaborar presentaciones con diapositivas, consultar y producir información en Internet.<?php echo "\n\n"?>
AVANZADO: Puede trabajar hojas de calculo con facilidad.">
                                <div class="ui-icon ui-icon-help" ></div>
                            </a>
                        </td>
                        <td><?php echo $form['habilidad_informatica_id']->renderError() ?>
                    </tr>
                    <?php 
                        /*
                        * Reingenieria  Mako Registro Socios. ID #15
                        * Responsable:  (BP) Bet Gader Porcayo Juárez
                        * Fecha:        29-08-2014 
                        * Descripción:  Se mueve dominio de inglés a la pestaña de datos básico
                        */
                        ?>
                    <tr>
                        <td class="verticalTop"><?php echo $form['dominio_ingles_id']->renderLabel('Dominio inglés',array('class'=>'RequiredLabel')) ?></td>
                        <!--<td>< ?php echo $form['dominio_ingles_id']->render(array('onfocus'=>'mensaje_muestra(this);', 'onblur'=>'mensaje_oculta(this);')) ? > </td>-->
                        <td><?php echo $form['dominio_ingles_id']->render(array('onfocus'=>'mensaje_muestra(this);', 'onblur'=>'mensaje_oculta(this);',  'requerido'=>"1", 'size'=>'10')) ?>
                        </td>
                        <td>
                            <a href="#" class="tooltip" msg="NINGUNO: Nunca ha tenido acercamiento al idioma.<?php echo "\n\n"?>
BÁSICO: Entiende y elabora frases cortas sobre temas cotidianos.<?php echo "\n\n"?>
INTERMEDIO: Entiende y puede decir oraciones completas sobre diversos temas y sostener conversaciones sencillas.<?php echo "\n\n"?>
AVANZADO: Puede tener una conversación larga sobre cualquier tema.">
                                <div class="ui-icon ui-icon-help" ></div>
                            </a>
                        </td>
                        <td><?php echo $form['dominio_ingles_id']->renderError() ?>

                    </tr>
                </table>
                <?php
                /*************************************
                **      @@ LGL      2013-07-08
                **      Se agrega 3 preguntas de Nivel Socio-Economico en pantalla principal de registro de socios
                **      Según Matriz 4M Fase II
                **      Id Actividad: 42
                        **/?>
                <h3 title="Al menos debe seleccionar una opción">Acceso a la tecnología <span>*</span></h3>                
                <!--
                *************************************************
                Reingenieria Mako. ID #42. 
                Responsable: IMB.
                Fecha: 11-08-2014. 
                Agregar comentario al titulo de la seccion      
                ***************************************************
                 -->
                <div><label>¿Cuenta con alguno de los siguientes aparatos y/o servicios?</label></div>
                <table id="AccesoTecnologia">
                    <tr>
                        <td width="33%">
                             <label>Aparatos</label>
                             <br/>
                             <br/>
                        </td>
                        <td width="33%">
                             <label>Servicios</label>
                             <br/>
                             <br/>
                        </td>
                        <td width="33%">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <!--Fecha: 11-08-2014. Fin-->
                            <?php echo $form['computadora']->renderLabel() ?>
                            <?php echo $form['computadora'] ?>
                            <?php echo $form['computadora']->renderError() ?>
                        </td>
                        <td>
                            <?php 
                            /*
                            * Reingenieria  Mako Registro Socios. ID #19 y 18
                            * Responsable:  (BP) Bet Gader Porcayo Juárez
                            * Fecha:        29-08-2014 
                            * Descripción:  Cambio de texto de Telefono celular por Telefono celular básico e Internet por internet en casa
                            */
                            ?>
                            <?php // Reingenieria Mako AP. ID 7. Responsable: FE. Fecha: 01-04-2014. Descripción del cambio: Se muestra 'Teléfono celular' en la pestaña 'Datos personales?>
                            <?php /*echo $form['linea_telefonica']->renderLabel("Línea telefonica");
                                echo $form['linea_telefonica'];
                                echo $form['linea_telfonica']->rendrError() */?>
                            <?php echo $form['linea_telefonica']->renderLabel("Línea telefónica") ?>
                            <?php echo $form['linea_telefonica'] ?>
                            <?php echo $form['linea_telefonica']->renderError() ?>
                        </td>
                        <td>
                            <label for="noAccesoTecnologia">Ninguno de los anteriores</label>
                            <input type="checkbox" name="noAccesoTecnologia" value="" id="socio_noacceso">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?php echo $form['telefono_celular']->renderLabel("Teléfono celular") ?>
                            <?php echo $form['telefono_celular'] ?>
                            <?php echo $form['telefono_celular']->renderError() ?>
                        </td>
                        <td>
                            <?php echo $form['internet']->renderLabel("Internet en casa") ?>
                            <?php echo $form['internet'] ?>
                            <?php echo $form['internet']->renderError() ?>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?php echo $form['tableta']->renderLabel("Tableta") ?>
                            <?php echo $form['tableta'] ?>
                            <?php echo $form['tableta']->renderError() ?>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
                      <!--
                *************************************************
                Reingenieria Mako. ID #42. 
                Responsable: IMB.
                Fecha: 11-08-2014. 
                Agregar opcion "Televisión"
                ***************************************************
                 -->
                <?php //echo $form['television']->renderLabel("Televisión") ?>
                <?php //echo $form['television'] ?>
                <?php //echo $form['television']->renderError() ?>
                 <!--Fecha: 11-08-2014. Fin-->
                <?php /*
                <?php echo $form['linea_telefonica']->renderLabel() ?>
                <?php echo $form['linea_telefonica'] ?>
                <?php echo $form['linea_telefonica']->renderError() ?>
                <br />
                <br>
                */?>


                <?php 
                /*
                * Reingenieria  MAKO-ARFS-002 PUNTO 14
                * Responsable:  (BP) Bet Gader Porcayo Juárez
                * Fecha:        07/10/2014
                * Descripción:  Se mueve a la paestaña de datos básico del socio
                */
                ?>
                <h3>Medio de acercamiento</h3>
                <table>
                    <tr>
                        <?php 
                        /*
                        * Reingenieria  Mako Registro Socios. ID #32
                        * Responsable:  (BP) Bet Gader Porcayo Juárez
                        * Fecha:        29-08-2014
                        * Descripción:  Se agrega tooltip
                        */
                        ?>
                        <td class="verticalTop">
                        <?php  echo $form['canal_contacto_id']->renderLabel("¿Cómo se enteró del centro?",array('class'=>'RequiredLabel'));?>
                        </td>
                        <td>
                            <?php echo $form['canal_contacto_id']->render(array('onChange' => 'acercamiento(this.value, "")', 'size'=>"10", 'requerido'=>1)); ?>
                        </td>
                        <td>
                            <a href="#" class="tooltip" msg="Publicidad exterior: Nos vio anunciados en algún poster, espectacular o barda en calle.
Vinculación: Viene por algún convenio que tenemos con su empresa, escuela o institución.">
                                <div class="ui-icon ui-icon-help" ></div>
                            </a>
                        </td>
                        <?php echo $form['canal_contacto_id']->renderError() ?>
                    </tr>
                </table>


            </div>

            <?php 
            /*
            * Reingenieria  Mako Registro Socios. ID #9
            * Responsable:  (BP) Bet Gader Porcayo Juárez
            * Fecha:        29-08-2014 
            * Descripción:  Se añade etiqueta escolaridad
            */
            ?>
            <div id="escolaridad" class="ui-tabs-panel ui-tabs-hide">
                <h3>Identificación</h3>
                <table>
                    <tr>
                        <?php 
                        /*
                        * Reingenieria  Mako Registro Socios. ID #4
                        * Responsable:  (BP) Bet Gader Porcayo Juárez
                        * Fecha:        29-08-2014 
                        * Descripción:  Ocultar Tutor responsable
                        */
                        ?>
                        <td><?php echo $form['curp']->renderLabel("CURP") ?></td>
                        <td>
                        <input id="curp_nombre"  size="4" maxlength="4">
                        <input id="curp_fecha"   size="6" maxlength="6">
                        <input id="curp_genero"  size="1" maxlength="1">
                        <input id="curp_estado"  size="2" maxlength="2">
                        <input id="curp_cnombre" size="3" maxlength="3">
                        <input id="curp_codigo"  size="2" maxlength="2">
                        <div id="message_curp" style="displa: none;"></div>

                        <br/>
                        <br/>
                        <div style="display: none;">
                        <?php echo $form['curp']->render(array('size'=>'19','maxlength'=>'18','tabindex'=>1,'autocomplete'=>'off')) ?>
                        </div>

                        </td>
                        <?php echo $form['curp']->renderError() ?>
                    </tr>
                </table>

                <h3>Escolaridad</h3>

                <?php 
                /*
                * Reingenieria  Mako Registro Socios. ID #10
                * Responsable:  (BP) Bet Gader Porcayo Juárez
                * Fecha:        29-08-2014 
                * Descripción:  Se mueve escoradidad a la pestaña de datos básico
                */
                ?>
                <div id="estudiaCamposActual">

                    
                    <div id="otrosEstudiosActual">
                        <!--**********************************************************************/
                        /* Fecha: 2013-07-18 12:10
                        Actividad: Manejar cuadros de ayuda emergente en los campos: Nivel de estudios en curso,
                                Campo de estudio, Otros estudios, Otros, Ultimo grado de estudios concluido al 100%
                        ID Actividad: M4F2-11
                        Responsable: MR. Se agrego el código para los eventos onblur y onfocus, asi como una etiqueta para
                                    mostrar el mensaje.
                        /**********************************************************************-->
                        <!--
                        Fecha: 10-07-2013. Inicio.
                        Sugerencia/Acciones: Según matriz 4M RegSocios (biar leyenda “Profesión actual”
                        y “Profesión último”  por “Campo de estudio”).
                        ID Seguimiento: F2_32.
                        Responsable: FE.  Se cambio la etiqueta de Profeción actual y Profecion ultima.
                        -->
                        <table>
                            <tr style="display:none;">
                                <td><?php echo $form['profesion_actual_id']->renderLabel("Campo de estudio") ?>
                                </td>
                                <!--<td>< ?php echo $form['profesion_actual_id']->render(array('disabled' => 'disabled', 'onfocus'=>'mensaje_muestra(this);', 'onblur'=>'mensaje_oculta(this);')) ? > </td>-->
                                <td><?php echo $form['profesion_actual_id']->render(array('disabled' => 'disabled')) ?>
                                </td>
                                <td>
                            <a href="#" class="tooltip" msg="Seleccione la categoría relacionada a la carrera profesional que estudia el socio.">
                                <div class="ui-icon ui-icon-help" ></div>
                            </a>
                                <!--< ?php echo '<span id="socio_profesion_actual_id_mensaje" class="msjayu"></span>'; ? >-->
                                <?php echo $form['profesion_actual_id']->renderError() ?>
                                <!--Fecha: 10-07-2013. Fin.-->
                            </tr>
                        </table>
                    </div>
                </div>
                <table>
                    <tr>
                        <td class="verticalTop"><?php echo $form['tipo_escuela_id']->renderLabel("Tipo de escuela") ?>
                        </td>
                        <!--Fecha: 08-07-2013. Inicio
                Acciones: Según matriz 4M RegSocios F2.
                ID Seguimiento: 26.
                Responsable: ES.
                -->
                        <td><?php echo $form['tipo_escuela_id']->render(array('size'=>"10")) ?>
                        </td>
                        <?php echo $form['tipo_escuela_id']->renderError() ?>
                        <!--
                                    Fecha: 12-07-2013. Inicio.
                                    Sugerencia/Acciones: Según matriz 4M RegSocios (Quitar la pregunta ¿Cuenta con financiamiento o beca?).
                                    ID Seguimiento: F2_31.
                                    Responsable: YD. Sólo se oculta la opción.
                                <br/>
                                -->
                    </tr>
                </table>
                <!--<div class="clearfix">
                < ?php //echo $form['beca']->renderLabel("¿Cuenta con financiamiento o beca?") ?>
                < ?php //echo $form['beca'] ? >
                < ?php //echo $form['beca']->renderError() ? >
                </div>-->
                <!--br/-->
                <!-- Fin 12-07-2013 -->
                <div class="clearfix">
                    <table>
                        <tr>
                            <?php 
                            /*
                            * Reingenieria  MAKO-ARFS-002 PUNTO 12 2a Rev
                            * Responsable:  (BP) Bet Gader Porcayo Juárez
                            * Fecha:        09/10/2014
                            * Descripción:  Cambio de texto a Modalidad de estudio
                            */
                            ?>
                            <td class="verticalTop"><?php echo $form['tipo_estudio_id']->renderLabel("Modalidad de estudios") ?>
                            </td>
                            <!--FEcha: 08-07-2013. Inicio
                Acciones: Según matriz 4M RegSocios F2.
                Id Seguimiento: 26.
                Responsable: ES.
                -->
                            <td><?php echo $form['tipo_estudio_id']->render(array('size'=>"10")) ?>

                        </tr>
                        <?php echo $form['tipo_estudio_id']->renderError() ?>
                        </tr>
                    </table>
                </div>
                <?php 
                        /*
                        * Reingenieria  Mako Registro Socios. ID #27
                        * Responsable:  (BP) Bet Gader Porcayo Juárez
                        * Fecha:        29-08-2014 
                        * Descripción:  Se oculta el campo de ultimo grado de estudios
                        
                <div class="clearfix">
                    <?php // Reingenieria Mako AP. ID 4. Responsable: FE. Fecha: 31-03-2014. Descripción del cambio: campo requerido''?>
                    <table>
                        <tr> <!--Se cambia la etiqueta a Último nivel de estudios concluido al 100% - ES-->
                            <td><?php echo $form['grado_id']->renderLabel("Ultimo grado de estudios concluido al 100%",array('class'=>'RequiredLabel')) ?>
                            </td>
                            <td><?php echo $form['grado_id']->render(array('onChange' => 'estudiosNivelUltimo(this.value)','requerido'=>'1', 'size'=>"8"))?>
                            </td>
                            <td>
                                <span class="ui-icon ui-icon-help tooltip"
                                      title="Posgrado incluye maestría, especialidad y doctorado">
                                </span>
                            </td>
                            <?php echo $form['grado_id']->renderError() ?>
                            <?php // Fecha: 31-01-2014. Fin''?>
                        </tr>
                    </table>
                </div>
                */
                        ?>
                <div id="otrosEstudiosUltimo">
                    <!--
                    Fecha: 10-07-2013. Inicio.
                    Sugerencia/Acciones: Según matriz 4M RegSocios (cambiar leyenda “Profesión actual”
                    y “Profesión último”  por “Campo de estudio”).
                    ID Seguimiento: F2_32.
                    Responsable: FE.  Se cambio la etiqueta de Profesión actual y Profesión ultima.
                    -->
                    <table>
                        <tr>
                            <td><?php //echo $form['profesion_ultimo_id']->renderLabel("Campo de estudio") ?>
                            </td>
                            <td><?php //echo $form['profesion_ultimo_id']->render(array('disabled' => 'disabled'))?>
                            </td>
                            <?php //echo $form['profesion_ultimo_id']->renderError() ?>
                            <!-- Fecha: 10-07-2013. Fin. -->

                        </tr>
                    </table>
                </div>

                <h3>Ocupación</h3>
                <table>
                    <tr>
                        <?php 
                        /*
                        * Reingenieria  Mako Registro Socios. ID #27
                        * Responsable:  (BP) Bet Gader Porcayo Juárez
                        * Fecha:        29-08-2014 
                        * Descripción:  Se mueve sector 
                        */
                        ?>
                        <td class="verticalTop">
                            <?php echo $form['sector_id']->renderLabel() ?>
                        </td>
                        <td>
                            <?php echo $form['sector_id']->render(array('size'=>"10")) ?>
                        </td>
                        <td>
                            <?php echo $form['sector_id']->renderError() ?>
                        </td>
                    </tr>
                </table>

            </div>


            <?php 
            /*
            <div id="ocupacion" class="ui-tabs-panel ui-tabs-hide">

                
                <br />
                <div id="ocupacionCampos">
                    <?php echo $form['organizacion_id']->renderLabel() ?>
                    <?php echo $form['organizacion_id']->render(array('disabled' => 'disabled')) ?>
                    <?php echo $form['organizacion_id']->renderError() ?>
                    <br />
                    <!--
                                        Fecha: 10-07-2013. Inicio.
                                        Sugerencia/Acciones: Según matriz 4M RegSocios (Corregir redacción de la lista Posición
                    en la opción “Trabajador por cuenta propia”).
                                        ID Seguimiento: F2_39.
                                        Responsable: FE.  Se cambio uno de los registros de posicion que tenia faltas de otografia
                    desde la BD con un sql. que se ejecuta en la terminal.
                                        -->
                    <?php //echo $form['posicion_id']->renderLabel() ?>
                    <?php //echo $form['posicion_id']->render(array('disabled' => 'disabled')) ?>
                    <?php //echo $form['posicion_id']->renderError() ?>
                    <!-- <br /> -->
                    <!--
                    Fecha: 10-07-2013. Fin.
                    -->

                    <?php echo $form['id_tiempo_actividad']->renderLabel('Tiempo actividad') ?>
                    <?php echo $form['id_tiempo_actividad']->render(array('disabled' => 'disabled')) ?>
                    <?php echo $form['id_tiempo_actividad']->renderError() ?>

                    <br>

                    <?php echo $form['id_problema_empresa']->renderLabel('Problemas empresa') ?>
                    <?php echo $form['id_problema_empresa']->render(array('disabled' => 'disabled')) ?>
                    <?php echo $form['id_problema_empresa']->renderError() ?>

                    <br>

                    <?php echo $form['dependientes_economicos']->renderLabel() ?>
                    <?php echo $form['dependientes_economicos']->render(array('filtro'=>'numerico','size'=>'10','maxlength'=>'10')) ?>
                    <?php echo $form['dependientes_economicos']->renderError() ?>
                </div>
                
                <!--
                           Fecha: 12-07-2013. Inicio.
                           Sugerencia/Acciones: Según matriz 4M RegSocios (Quitar la pregunta ¿Es beneficiario del programa oportunidades?).
                           ID Seguimiento: F2_38.
                           Responsable: YD. Sólo se oculta la opción.
                        -->
                <br>
                <?php //echo $form['benef_oportunidades']->renderLabel("¿Es beneficiario del programa Oportunidades?") ?>
                <?php //echo $form['benef_oportunidades']->render() ?>
                <?php //echo $form['benef_oportunidades']->renderError() ?>
                <!--
                Fecha: 12-07-2013. Fin.
            -->

            </div>
            */
            ?>
            <?php /*
            <div id="acercamiento" class="ui-tabs-panel ui-tabs-hide">
                <table>
                    
                </table>
                <div id="acercamientoCampos">
                    <!--**********************************************************************/
            /* Fecha: 2013-07-18 12:10
            Actividad: Manejar cuadros de ayuda emergente en los campos: facilitador, otro medio y recomendó
            ID Actividad: M4F2-11
            Responsable: MR. Se agrego el código para los eventos onblur y onfocus, asi como una etiqueta para
                        mostrar el mensaje.
            /**********************************************************************-->
                    <!--
                Fecha: 20-06-2013. Inicio.
                Sugerencia/Acciones: Según matriz 4M RegSocios (Medio de acercamiento-Nombre del facilitador).
                ID Seguimiento: 66, 67.
                Responsable: FE. Se valido que el campo solo acepte caracetres alfabeticos.
                -->
                        <!--
                        * Reingenieria  Mako Registro Socios. ID #33
                        * Responsable:  (BP) Bet Gader Porcayo Juárez
                        * Fecha:        29-08-2014 
                        * Descripción:  Se ocultan campos: Nombre del promotor, Liga, Recomendó, Otro medio o contacto, Socio que recomienda, Motivo de capacitación, Motivo de acercamiento,
                        -->
                    <table>
                        <tr>
                            <td><?php echo $form['promotor_ref_id']->renderLabel("Nombre del promotor") ?>
                            </td>
                            <!--<td>< ?php echo $form['promotor_ref_id']->render(array('filtro'=>'alfabetico','tabindex'=>1,'disabled' => 'true', 'onfocus'=>'mensaje_muestra(this);', 'onblur'=>'mensaje_oculta(this);')) ? ></td>-->
                            <td><?php echo $form['promotor_ref_id']->render(array('filtro'=>'alfabetico','tabindex'=>1,'disabled' => 'true')) ?>
                            </td>
                            <td><span class="ui-icon ui-icon-help tooltip"
                                title="Capture parte del nombre y el sistema le ayudará a autocompletar."></span>
                            </td>
                            <!--< ?php echo '<span id="autocomplete_socio_promotor_ref_id_mensaje" class="msjayu"></span>'; ? >-->
                            <?php echo $form['promotor_ref_id']->renderError() ?>
                            <!-- Fecha: 20-06-2013. Fin.    -->
                            <!--
                /* Fecha: 2013-07-18 17:43
                Actividad:Definir el tamaño del campo Liga según resulte de investigar un tamaño conveniente
                ID Actividad: M4F2-22
                Responsable: MR. Cambio de 120 a 255 caracteres
                        </tr>
                        <tr>
                            <td><?php echo $form['liga']->renderLabel() ?></td>
                            <td><?php echo $form['liga']->render(array('disabled' => 'true', 'maxlength'=>'255')) ?>
                            </td>
                            <?php echo $form['liga']->renderError() ?>
                        </tr>
                        <tr>
                            <td><?php echo $form['recomienda_id']->renderLabel("Recomendó") ?>
                            </td>
                            <!--<td>< ?php echo $form['recomienda_id']->render(array('disabled' => 'true', 'onChange' => 'acercamientoRecomienda(this.value)', 'onfocus'=>'mensaje_muestra(this);', 'onblur'=>'mensaje_oculta(this);')) ? >-->
                            <td><?php echo $form['recomienda_id']->render(array('disabled' => 'true', 'onChange' => 'acercamientoRecomienda(this.value)')) ?>

                            <td><span class="ui-icon ui-icon-help tooltip"
                                title="Si elige 'Socio', haga clic en la liga 'Buscar socio' que habilita la búsqueda."></span>
                            </td>
                            <!--< ?php echo '<span id="socio_recomienda_id_mensaje" class="msjayu"></span>'; ? >-->
                            <?php echo $form['recomienda_id']->renderError() ?>
                        </tr>
                    </table>

                    <div id="acercamientoCamposRecomendo">
                        <!-- Fecha: 10-07-2013. Inicio.
                                Sugerencia/Acciones: Según matriz 4M RegSocios (Definir el tamaño del campo Otros contac
                                tos a 100 caracteres), (Cambiar el texto Otros contactos por “Otro medio o contacto”).
                                ID Seguimiento: F2_24, F2_25.
                                Responsable: FE.  Se valido que el tamaño máximo para escribir en otros_contacto y se ca
                                mbio la etiqueta de otros contacto.-->
                        <table>
                            <tr>
                                <td>
                                    <?php echo $form['otros_contacto']->renderLabel("Otro medio o contacto") ?>
                                </td>
                                <!--<td>< ?php echo $form['otros_contacto']->render(array('filtro'=>'alfabetico', 'disabled' => 'true','maxlength'=>'100', 'onfocus'=>'mensaje_muestra(this);', 'onblur'=>'mensaje_oculta(this);')) ? > </td>-->
                                <td>
                                    <?php echo $form['otros_contacto']->render(array('filtro'=>'alfabetico', 'disabled' => 'true','maxlength'=>'100', 'onfocus'=>'mensaje_muestra(this);', 'onblur'=>'mensaje_oculta(this);')) ?>
                                    <span class="ui-icon ui-icon-help tooltip"
                                          title="Ejemplo: vecino, vivo cerca, pasaba por aquí."
                                          style="display: inline-block;"></span>
                                </td>
                                <!--< ?php echo '<span id="socio_otros_contacto_mensaje" class="msjayu"></span>'; ? >-->
                                <?php echo $form['otros_contacto']->renderError() ?>
                                <!--Fecha: 10-07-2013. Fin.-->
                            </tr>
                            <tr>
                                <td>
                                    <?php echo $form['socio_ref_id']->renderLabel("Socio que recomienda") ?>
                                </td>
                                <?php echo $form['socio_ref_id'] ?>
                                <td><div id="socio_ref_nombre"></div></td>
                                <td>
                                    <div id="recomendo_socio" style="display: none">
                                        <a href="javascript:void(0);" id="socio-ref">Buscar Socio</a>
                                    </div>
                                </td>
                                <?php echo $form['socio_ref_id']->renderError() ?>
                            </tr>
                            <tr>
                                <td>
                                    <?php echo $form['id_motivo_capacitacion']->renderLabel("Motivo capacitación") ?>
                                </td>
                                <td>
                                    <?php echo $form['id_motivo_capacitacion']->render() ?>
                                </td>
                                <?php echo $form['id_motivo_capacitacion']->renderError() ?>
                            </tr>
                            <tr>
                                <td>
                                    <?php echo $form['id_motivo_acercamiento']->renderLabel("Motivo acercamiento") ?>
                                </td>
                                <td>
                                    <?php echo $form['id_motivo_acercamiento']->render() ?>
                                </td>
                                <?php echo $form['id_motivo_acercamiento']->renderError() ?>
                            </tr>
                        </table>



                    </div>
                </div>
            </div>
            */?>

            <?php /*
            <div id="solicitudBeca" class="ui-tabs-panel ui-tabs-hide">
            <h3>Ocupación</h3>
                <table>
                    <tr>
                        <td>
                            <?php echo $form['servicio_salud_id']->renderLabel("Servicio de salud de la que es beneficiario") ?>
                        </td>
                        <td>
                            <?php echo $form['servicio_salud_id'] ?>
                        </td>
                        <td>
                            <?php echo $form['servicio_salud_id']->renderError() ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?php echo $form['vivienda_tipo_id']->renderLabel("Tipo de vivienda") ?>
                        </td>                        
                        <!--
                        *****************************************************************************
                        Reingenieria Mako. ID #54. 
                        Responsable: IMB.
                        Fecha: 13-08-2014. 
                        Validacion dependiendo tipo de vivienda, deshabilitar objeto 'vivienda_es_id'     
                        *****************************************************************************
                        -->
                             <td><?php echo $form['vivienda_tipo_id']->render(array('onclick' => 'tipoVivienda(this.value)')) ?>                             
                        </td>
                        <td>
                            <?php echo $form['vivienda_tipo_id']->renderError() ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?php echo $form['vivienda_es_id']->renderLabel("La vivienda que habita es") ?>
                        </td>
                        <td>
                            <?php echo $form['vivienda_es_id'] ?>
                        </td>
                        <td>
                            <?php echo $form['vivienda_es_id']->renderError() ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?php echo $form['material_techo_id']->renderLabel("Material del techo de la vivienda") ?>
                        </td>
                        <td>
                            <?php echo $form['material_techo_id'] ?>
                        </td>
                        <td>
                            <?php echo $form['material_techo_id']->renderError() ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?php echo $form['material_piso_id']->renderLabel("Material del piso de la vivienda") ?>
                        </td>
                        <td>
                            <?php echo $form['material_piso_id'] ?>
                        </td>
                        <td>
                            <?php echo $form['material_piso_id']->renderError() ?>
                        </td>
                    </tr>

                </table>

                <h3>Servicios con que cuenta la vivienda</h3>
                <table>
                    <tr>
                        <td>
                            Servicios con que cuenta la vivienda
                        </td>
                        <td>
                            <?php echo $form['energia_electrica']->renderLabel() ?>
                            <?php echo $form['energia_electrica'] ?>
                            <?php echo $form['energia_electrica']->renderError() ?>
                        </td>
                        <td>
                        </td>
                    <tr>
                    </tr>
                        <td>
                        </td>
                        <td>
                            <?php echo $form['agua_entubada']->renderLabel() ?>
                            <?php echo $form['agua_entubada'] ?>
                            <?php echo $form['agua_entubada']->renderError() ?>
                        </td>
                        <td>
                        </td>
                    <tr>
                    </tr>
                        <td>
                        </td>
                        <td>
                            <?php echo $form['drenaje']->renderLabel() ?>
                            <?php echo $form['drenaje'] ?>
                            <?php echo $form['drenaje']->renderError() ?>
                        </td>
                        <td>
                        </td>
                    <tr>
                    </tr>
                        <td>
                        </td>
                        <td>
                            <?php echo $form['linea_telefonica']->renderLabel() ?>
                            <?php echo $form['linea_telefonica'] ?>
                            <?php echo $form['linea_telefonica']->renderError() ?>
                        </td>
                        <td>
                        </td>
                    <tr>
                    </tr>
                        <td>
                        </td>
                        <td>
                            <?php echo $form['television_paga']->renderLabel("Televisión de paga") ?>
                            <?php echo $form['television_paga'] ?>
                            <?php echo $form['television_paga']->renderError() ?>
                        </td>
                        <td>
                        </td>
                    <tr>
                    </tr>                  
                </table>

            <h3>Enseres dom&eacute;sticos</h3>
            <table>
                    <tr>
                        <td>
                            Enseres dom&eacute;sticos
                        </td>
                        <td>
                            <?php echo $form['lavadora']->renderLabel() ?>
                            <?php echo $form['lavadora'] ?>
                            <?php echo $form['lavadora']->renderError() ?>
                        </td>
                        <td>
                        </td>
                    <tr>
                    </tr>
                        <td>
                        </td>
                        <td>
                            <?php echo $form['secadora']->renderLabel() ?>
                            <?php echo $form['secadora'] ?>
                            <?php echo $form['secadora']->renderError() ?>
                        </td>
                        <td>
                        </td>
                    <tr>
                    <!--
                    </tr>
                        <td>
                        </td>
                        <td>
                            <?php echo $form['television']->renderLabel() ?>
                            <?php echo $form['television'] ?>
                            <?php echo $form['television']->renderError() ?>
                        </td>
                        <td>
                        </td>
                    <tr>
                    -->
                    </tr>
                        <td>
                        </td>
                        <td>
                            <?php echo $form['calentador_agua']->renderLabel("Calentador de agua") ?>
                            <?php echo $form['calentador_agua'] ?>
                            <?php echo $form['calentador_agua']->renderError() ?>
                        </td>
                        <td>
                        </td>
                    <tr>
                    </tr>
                        <td>
                        </td>
                        <td>
                            <?php echo $form['horno_microondas']->renderLabel() ?>
                            <?php echo $form['horno_microondas'] ?>
                            <?php echo $form['horno_microondas']->renderError() ?>
                        </td>
                        <td>
                        </td>
                    <tr>
                    </tr>
                        <td>
                        </td>
                        <td>
                            <?php echo $form['tostador']->renderLabel() ?>
                            <?php echo $form['tostador'] ?>
                            <?php echo $form['tostador']->renderError() ?>
                        </td>
                        <td>
                        </td>
                    <tr>
                    </tr>
                        <td>
                        </td>
                        <td>
                            <?php echo $form['reproductor_video']->renderLabel("Reproductor de video") ?>
                            <?php echo $form['reproductor_video'] ?>
                            <?php echo $form['reproductor_video']->renderError() ?>
                        </td>
                        <td>
                        </td>
                    <tr>
                    </tr>
                        <td>
                        </td>
                        <!--
                        <td>
                            <?php echo $form['computadora']->renderLabel() ?>
                            <?php echo $form['computadora'] ?>
                            <?php echo $form['computadora']->renderError() ?>
                        </td>
                        <td>
                        </td>
                    <tr>
                    </tr>
                        <td>
                        </td>
                        <td>
                            <?php echo $form['telefono_celular']->renderLabel("Teléfono celular") ?>
                            <?php echo $form['telefono_celular'] ?>
                            <?php echo $form['telefono_celular']->renderError() ?>
                        </td>
                        <td>
                        </td>
                        -->
                    <tr>
                    </tr>
                        <td>
                        </td>
                        <td>
                            <?php echo $form['reproductor_audio']->renderLabel("Reproductor de audio") ?>
                            <?php echo $form['reproductor_audio'] ?>
                            <?php echo $form['reproductor_audio']->renderError() ?>
                        </td>
                        <td>
                        </td>
                    <tr>
                    </tr>
                        <td>
                        </td>
                        <td>
                            <?php echo $form['fregadero']->renderLabel() ?>
                            <?php echo $form['fregadero'] ?>
                            <?php echo $form['fregadero']->renderError() ?>
                        </td>
                        <td>
                        </td>
                    <tr>
                    </tr>
                        <td>
                        </td>
                        <td>
                            <?php echo $form['estufa']->renderLabel() ?>
                            <?php echo $form['estufa'] ?>
                            <?php echo $form['estufa']->renderError() ?>
                        </td>
                        <td>
                        </td>
                    <tr>
                    </tr>
                        <td>
                        </td>
                        <td>
                            <?php echo $form['refrigerador']->renderLabel() ?>
                            <?php echo $form['refrigerador'] ?>
                            <?php echo $form['refrigerador']->renderError() ?>
                        </td>
                        <td>
                        </td>
                    <tr>
                    </tr>
                        <td>
                        </td>
                        <td>
                            <?php echo $form['licuadora']->renderLabel() ?>
                            <?php echo $form['licuadora'] ?>
                            <?php echo $form['licuadora']->renderError() ?>
                        </td>
                        <td>
                        </td>
                    </tr>
                    </tr>
                        <td>
                            <?php echo $form['automovil_propio']->renderLabel("¿Tiene automóvil propio?") ?>
                        </td>
                        <td class="radio-buttons">
                            <?php echo $form['automovil_propio']->render(array('onChange' => 'nseUsoAutomovil(this.value)')) ?>
                            <?php echo $form['automovil_propio']->renderError() ?>
                        </td>
                        <td>
                        </td>
                    </tr>
                    </tr>
                        <td>
                            <?php echo $form['uso_automovil_id']->renderLabel("Uso que le da al automóvil") ?>
                        </td>
                        <td class="radio-buttons">
                            <?php echo $form['uso_automovil_id']->render(array('disabled' => 'true','size'=>"3"))?>
                            <?php echo $form['uso_automovil_id']->renderError() ?>
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
            </div>*/?>

            <style>
label {
    width: 200px !important;
}
</style>
            <?php
            /******************************************
             **      @@ LGL     2013-07-08
            **      Se deshabilita pantalla de Nivel Socio-Economico
            **      Según Matriz 4M Fase II
            **      Id Actividad: 42
            **
            *****************************************

            <div id="nse" class="ui-tabs-panel ui-tabs-hide">
            <?php echo $form['estado_civil_id']->renderLabel() ?>
            <?php echo $form['estado_civil_id']?>
            <?php echo $form['estado_civil_id']->renderError() ?>
            <br/>

            <?php echo $form['personas_vivienda']->renderLabel("Número de personas con las que comparte vivienda") ?>
            <?php echo $form['personas_vivienda']->render(array('filtro'=>'numerico','size'=>'10','maxlength'=>'10')) ?>
            <?php echo $form['personas_vivienda']->renderError() ?>

            <br/>

            <?php echo $form['habitaciones']->renderLabel("Número de habitaciones") ?>
            <?php echo $form['habitaciones']->render(array('filtro'=>'numerico','size'=>'10','maxlength'=>'10')) ?>
            <?php echo $form['habitaciones']->renderError() ?>
            <br/>

            <?php echo $form['banos']->renderLabel("Número de baños") ?>
            <?php echo $form['banos']->render(array('filtro'=>'numerico','size'=>'10','maxlength'=>'10')) ?>
            <?php echo $form['banos']->renderError() ?>
            <br/>

            <?php echo $form['focos']->renderLabel("Número de focos") ?>
            <?php echo $form['focos']->render(array('filtro'=>'numerico','size'=>'10','maxlength'=>'10')) ?>
            <?php echo $form['focos']->renderError() ?>
            <br/>

            <?php echo $form['anios_residencia']->renderLabel("Años de residir en el domicilio") ?>
            <?php echo $form['anios_residencia']->render(array('filtro'=>'numerico','size'=>'10','maxlength'=>'10')) ?>
            <?php echo $form['anios_residencia']->renderError() ?>
            <br/>

            <?php echo $form['meses_residencia']->renderLabel("Meses de residir en el domicilio") ?>
            <?php echo $form['meses_residencia']->render(array('filtro'=>'numerico','size'=>'10','maxlength'=>'10')) ?>
            <?php echo $form['meses_residencia']->renderError() ?>
            <br/>

            

            <h3></h3>
            <?php echo $form['automoviles']->renderLabel("Cuántos automóviles tienen en casa") ?>
            <?php echo $form['automoviles'] ->render(array('filtro'=>'numerico','size'=>'10','maxlength'=>'10')) ?>
            <?php echo $form['automoviles']->renderError() ?>
            <br/>

            <?php echo $form['socio_padre_id']->renderLabel("Padre o Tutor") ?>
            <?php echo $form['socio_padre_id'] ?>
            <div id="socio_padre_nombre"></div>
            <div id="socio_padre">
            <a href="javascript:void(0);" id="boton-buscador-padre">Buscar Socio</a>
            </div>
            <?php echo $form['socio_padre_id']->renderError() ?>

            <?php echo $form['socio_padre_vive']->renderLabel("Vive (s/n)") ?>
            <?php echo $form['socio_padre_vive']?>
            <?php echo $form['socio_padre_vive']->renderError() ?>
            <br>

            <?php echo $form['socio_madre_id']->renderLabel("Madre o Tutor") ?>
            <?php echo $form['socio_madre_id'] ?>
            <div id="socio_madre_nombre"></div>
            <div id="socio_madre">
            <a href="javascript:void(0);" id="boton-buscador-madre">Buscar Socio</a>
            </div>
            <?php echo $form['socio_madre_id']->renderError() ?>

            <?php echo $form['socio_madre_vive']->renderLabel("Vive (s/n)") ?>
            <?php echo $form['socio_madre_vive']?>
            <?php echo $form['socio_madre_vive']->renderError() ?>
            <br>

            <br/>
            <br/>
            <br/>
            <br/>

            </div>
    */?>


            <?php echo $form['foto']->render() ?>
            <!--
                           Fecha: 12-07-2013. Inicio.
                           Sugerencia/Acciones: Según matriz 4M RegSocios (Ocultar icono de Huella digital.).
                           ID Seguimiento: F2_12.
                           Responsable: YD. Sólo se oculta la opción.
                        -->
            <?php //echo $form['huella']->render() ?>
            <!--Fin 12-07-2013-->
            <br />
            <?php echo $form['id'] ?>
            <?php echo $form['_csrf_token'] ?>

            <input type="submit" value="Guardar"
                onclick="return comparaRequeridos();" id="boton-guardar" /> <input
                type="button" value="Cancelar"
                onclick="location.href='<?php echo url_for('registro/new') ?>'"
                id="boton-cancelar">
        </div>
        <!--Fecha: 02-04-2014 Fin-->
        <!--Fecha: 02-04-2014 Fin-->
    </div>
</form>

<script>

    acercamiento( '<?php echo $form['canal_contacto_id']->getValue(); ?>', '<?php echo $form['recomienda_id']->getValue(); ?>' );
    acercamientoRecomienda( '<?php echo $form['recomienda_id']->getValue(); ?>' );
    estudiosActual();
    //estudiosNivelActual( '<?php echo $form['nivel_id']->getValue(); ?>' );
    estudiosOtrosActual( '<?php echo $form['estudio_actual_id']->getValue() ?>' );
    estudiosNivelUltimo( '<?php echo $form['nivel_id']->getValue(); ?>' );
    estudiosOtrosUltimo( '<?php echo $form['estudio_ultimo_id']->getValue(); ?>' );
    tipoVivienda( '<?php echo $form['vivienda_tipo_id']->getValue() ?>' );  

    $("#socio_ref_nombre").show().html(  '<?php echo $form->getObject()->getSocioRelatedBySocioRefId() ?>' );
    $("#socio_madre_nombre").show().html(  '<?php echo $form->getObject()->getSocioRelatedBySocioMadreId() ?>' );
    $("#socio_padre_nombre").show().html(  '<?php echo $form->getObject()->getSocioRelatedBySocioPadreId() ?>' );

    //trabaja();

    $("#socio_promotor_ref_id").val( '<?php echo $form['promotor_ref_id']->getValue(); ?>' );

    <?php if($form->getObject()->getUsuarioRelatedByPromotorRefId() != '' ): ?>
        $("#autocomplete_socio_promotor_ref_id").val('<?php echo $form->getObject()->getUsuarioRelatedByPromotorRefId() ?>' );
    <?php else:?>
        $("#autocomplete_socio_promotor_ref_id").val('<?php $np = UsuarioPeer::retrieveByPK($form['promotor_ref_id']->getValue()); if($np != null){ echo $np;}else{echo '';} ?>' );
    <?php endif;?>


</script>

<script
    type="text/javascript"
    src="https://maps.google.com/maps/api/js?sensor=true"></script>

<input
    type="hidden" name="appUseMap" id="appUseMap"
    value="<?= $appUsaMap; ?>" />


<input type="hidden" id="tmpmunicipio" name="tmp_municipio" value="<?php echo $form['id_municipio']->getValue(); ?>">
<input type="hidden" id="tmpcolonia" name="tmp_colonia" value="<?php echo $form['id_colonia']->getValue(); ?>">
<style>
input[type="text"], select{ width: 260px; }
select{ width: 268px; }
#socio_fecha_nac_day{ width: 80px;}
#socio_fecha_nac_month{ width: 80px;}
#socio_fecha_nac_year{ width: 90px;}
.td-first{width: 200px;}
#map_canvas{width: 418px; height:265px;}
#domicilio tr{ display: block; margin: 8px 0;}
input[requerido="1"], input[type="password"], textarea[requerido="1"], select[requerido="1"] { background: none !important; }
.telefono_requerido{color: black; font-weight: normal; font-style: italic; font-size: 10px;}
.checkbox_list { list-style-type: None; }
#message_curp{background: #3D3D3D; width: 256px; color: white; padding: 10px; display: none;}
.verticalTop { vertical-align: top; padding-top: 4px;}
#AccesoTecnologia label { width: 150px !important;}
#AccesoTecnologia td { padding-right: 50px;}
#TitleModal{font-size: 13px; font-weight: bold; text-align: center; padding-top: 25px;}
#img_capturar_foto{border: 1px #cc007a solid;}
</style>


<?php 
/*
* Reingenieria  MAKO-ARFS-002 PUNTO 1 2a Rev
* Responsable:  (BP) Bet Gader Porcayo Juárez
* Fecha:        08/10/2014
* Descripción:  Se cambia el icono ! por el ?, también se coloca alieada a la parte superior las etiquetas del formulario
*/
?>
