<?php use_javascript('huella/capturarHuella.js') ?>
<!----------- Comienza componente de captura de huella ------------------>
<div class="applet_container" id="boton_huella" style="float: right;">
	<a href="javascript:void(0);" tabindex="2" id="liga-control-huella"> <img
		border="0" class="sombra-delgada" id="img_huella"
		src="/imgs/iconos/huella_gray.jpg" width="100" height="155" />
	</a>
</div>
<div id="dialogo-control-huella" align="center" style="display: none;">
	<div class="applet_container" style="display: none;"
		id="control_huella">
		<APPLET id="tes"
			code="mx.com.bixit.biometricos.huella.applet.AppletHuella"
			width="220" height="340"
			archive="/js/huella/SAppletCapturaHuella.jar, /js/huella/SFingerprintSDK.jar, /js/huella/Sb64.jar"
			MAYSCRIPT> </APPLET>
	</div>
</div>
<input
	type="hidden" name="tplb64" id="tplb64" value="">
<div id="logdiv"style"display:none"></div>
<div id="res_captura"style"display:none">&nbsp; &nbsp; &nbsp; &nbsp;
	&nbsp; &nbsp;</div>
<!----------- Finaliza componente de captura de huella ------------------>
