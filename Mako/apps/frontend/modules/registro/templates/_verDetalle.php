<script>

    $(function() {
        var tabsvd = $('#tabsVerDetalle').tabs();

        tabsvd.addClass('ui-tabs-vertical ui-helper-clearfix');
        $("#tabsVerDetalle li").removeClass('ui-corner-top').addClass('ui-corner-left');
        $("#tabsVerDetalle .ui-tabs-panel").removeClass('ui-widget-content');

    });


    var oTable;
    $(document).ready(function() {

       oTable = $('.registros').dataTable({
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false,
            "bJQueryUI": true,
            "aaSorting": [],
            "sPaginationType": "full_numbers"
         });

       oTCompras = $('#registrosCompras').dataTable({
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false,
            "bJQueryUI": true,
            "aaSorting": [],
            "aoColumns":[{ "sType": "decimal" },null,null,null,{ "sType": "fecha_dma" } ,{ "sType": "decimal" },null,{ "sType": "decimal" },{ "sType": "decimal" },{ "sType": "decimal" }],
            "sPaginationType": "full_numbers"
         });

       oTSesiones = $('#registrosSesiones').dataTable({
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false,
            "bJQueryUI": true,
            "aaSorting": [],
            "aoColumns":[{ "sType": "timestamp_dma" } ,{ "sType": "timestamp_dma" },null,null,null,null],
            "sPaginationType": "full_numbers"
         });

       oTCob = $('#registrosCobranza').dataTable({
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false,
            "bJQueryUI": true,
            "aaSorting": [],
            "aoColumns":[null,null,null,null,{ "sType": "fecha_dma" } ,{ "sType": "fecha_dma" },null,null],
            "sPaginationType": "full_numbers"
         });

       oTAbono = $('#registrosAbonos').dataTable({
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false,
            "bJQueryUI": true,
            "aaSorting": [],
            "aoColumns":[null,null,{ "sType": "timestamp_amd" }],
            "sPaginationType": "full_numbers"
         });

       oTCons = $('#registrosConsumo').dataTable({
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false,
            "bJQueryUI": true,
            "aaSorting": [],
            "aoColumns":[null,null,{ "sType": "timestamp_amd" },{ "sType": "timestamp_amd" },null,null],
            "sPaginationType": "full_numbers"
         });

    });
</script>
<style>
  .ui-tabs-vertical { width: 1060px; }
  .ui-tabs-vertical .ui-tabs-nav { padding: .2em .1em .2em .2em; float: left; width: 160px; }
  .ui-tabs-vertical .ui-tabs-nav li { clear: left; width: 100%; border-bottom-width: 1px !important; border-right-width: 0 !important; margin: 0 -1px .2em 0; }
  .ui-tabs-vertical .ui-tabs-nav li a { display:block; }
  .ui-tabs-vertical .ui-tabs-nav li.ui-tabs-active { padding-bottom: 0; padding-right: .1em; border-right-width: 1px; border-right-width: 1px; }
  .ui-tabs-vertical .ui-tabs-panel { padding: 1em; float: left; width: 800px;}
  .verticalTop { vertical-align: top; padding-top: 4px;}
  </style>


<?php use_stylesheet('./registro/_verDetalle.css')?>


<div id="contenedorVerDetalle">
    <div id="tabsVerDetalle">

        <ul id="tabMenu-vd">
            <li><a href="#personales-vd">Datos personales</a></li>
            <li><a href="#direccion-vd">Dirección</a></li>
            <?php if($Socio->getRfc() != ''): ?>
            <li><a href="#datosFiscales-vd">Datos fiscales</a></li>
            <?php endif; ?>
            <li><a href="#escolaridad-vd">Escolaridad</a></li>
            <li><a href="#ocupacion-vd">Ocupacion</a></li>
            <li><a href="#habilidades-vd">Habilidades</a></li>
            <li><a href="#acceso-tecnologia-vd">Acceso a la tecnología</a></li>
            <li><a href="#acercamiento-vd">Medio de acercamiento</a></li>
            <li><a href="#historial_sesiones-vd">Historial de sesiones</a></li>
            <li><a href="#historial_compras-vd">Historial de compras</a></li>
            <!--Reingeniería PV | Responsable: YD => FE. - Fecha: 21-11-2013. - ID: 70. - Campo: Historial de cobranza.
      Descripción del cambio: Se modifico etiqueta 'Historial de cobranza'-->
            <li><a href="#historial_cobranza-vd">Historial de pagos</a></li>
            <!-- Fecha: 21-11-2013. - Fin-->
            <li><a href="#reporte-abonos">Reporte de abonos y consumos de tiempo</a>
            </li>
            <?php /*****************************************+
** @@ LGL     2013-07-08
** Se deshabilita visualización de ficha de Nivel Socio Economico
** Según Matriz 4M Fase II
** Id Actividad : 41
****************************************

<li><a href="#nse-vd">NSE</a></li>
                */?>
        </ul>


        <div id="personales-vd">
            <h3>
                <?php echo $Socio->getNombre().' '.$Socio->getApepat().' '.$Socio->getApemat() ?>
            </h3>
            <div class="clearfix">
                <div class="container fotoGrande" style="float: left;">
                    <img src="<?php echo ($Socio->getFoto() != '')? url_for('/fotos/'.$Socio->getFoto()) : url_for('/imgs/sin_foto_sh.jpg') ?>" class="main fotoGrande" /> <img class="minor fotoGrande" src="/imgs/frame.png">
                </div>
                <table width="" border="0" class="dataTables_wrapper">
                    <tr class="odd">
                        <td width="30%" class="sorting_1">Folio:</td>
                        <td><?php echo $Socio->getFolio() ?></td>
                    </tr>
                    <tr class="even">
                        <td class="sorting_1">Usuario:</td>
                        <td><?php echo $Socio->getUsuario() ?></td>
                    </tr>
                    <tr class="odd">
                        <td class="sorting_1">Edad:</td>
                        <td><?php echo $Socio->getEdad() ?> años</td>
                    </tr>
                    <tr class="even">
                        <td class="sorting_1">Genero:</td>
                        <td><?php echo ($Socio->getSexo()=='M') ? 'Masculino':'Femenino' ?></td>
                    </tr>
                    <tr class="odd">
                        <td class="sorting_1">Fecha Nac.:</td>
                        <td><?php echo $Socio->getFechaNac("%d-%m-%Y") ?></td>
                    </tr>
                    <tr class="odd">
                        <td width="40%" class="sorting_1"> Estado civil:</td>
                        <td ><?php echo $Socio->getEstadoCivil() ?></td>
                    </tr>
                    <tr class="even">
                        <td class="sorting_1">CURP:</td>
                        <td><?php echo $Socio->getCurp() ?></td>
                    </tr>
                    <tr class="even">
                        <td class="sorting_1">Teléfono fijo:</td>
                        <td><?php echo $Socio->getTel() ?></td>
                    </tr>
                    <tr class="odd">
                        <td class="sorting_1">Teléfono celular:</td>
                        <td><?php echo $Socio->getCelular() ?></td>
                    </tr>
                    <tr class="odd">
                        <td class="sorting_1">Email:</td>
                        <td><?php echo $Socio->getEmail() ?></td>
                    </tr>
                    <tr class="odd">
                        <td class="sorting_1">Tiempo PC:</td>
                        <td><?php echo Comun::segsATxt($Socio->getSaldo());?></td>
                    </tr>
                    <tr class="odd">
                        <td class="sorting_1">Operador:</td>
                        <td><?php echo $Socio->getUsuarioRelatedByOperadorId() ?></td>
                    </tr>
                    <tr class="odd">
                        <td class="sorting_1">Centro alta:</td>
                        <td><?php echo $Socio->getCentro()->getNombre() ?></td>
                    </tr>
                    <tr class="odd">
                        <td class="sorting_1">Fecha alta:</td>
                        <td><?php echo $Socio->getCreatedAt("%d-%m-%Y") ?></td>
                    </tr>
                    <?php /*
                    <tr class="even">
                        <td class="sorting_1">Rol:</td>
                        <td><?php echo $Socio->getRol() ?></td>
                    </tr>
                    <tr class="even">
                        <td class="sorting_1">Teléfono de emergencia:</td>
                        <td><?php echo $Socio->getTelefonoEmergencia()   ?></td>
                    </tr>
                    <tr class="odd">
                        <td class="sorting_1">Contacto de emergencia:</td>
                        <td><?php echo $Socio->getContactoEmergencias() ?></td>
                    </tr>
                    <tr class="even">
                        <td class="sorting_1">Tutor responsable:</td>
                        <td><?php echo $Socio->getTutorResponsable() ?></td>
                    </tr>
                    <tr class="even">
                        <td class="sorting_1">Vigente:</td>
                        <td><?php echo ($Socio->getVigente()=='1') ? 'Si':'No' ?></td>
                    </tr>
                    <tr class="even">
                        <td class="sorting_1">Activo:</td>
                        <td><?php echo ($Socio->getActivo()=='1') ? 'Si':'No' ?></td>
                    </tr>
                    
                    <tr class="even">
                        <td class="sorting_1">Fecha ultima modificación:</td>
                        <td><?php echo $Socio->getUpdatedAt("%d-%m-%Y") ?></td>
                    </tr>
                    */?>
                </table>
            </div>
        </div>

        <div id="direccion-vd">
            <h3>Dirección</h3>
            <table width="" border="0" class="dataTables_wrapper">
                <tr class="odd">
                    <td width="40%" class="sorting_1">Estado:</td>
                    <td><?php echo $Socio->getEstado() ?></td>
                </tr>
                <tr class="even">
					<td class="sorting_1">Municipio:</td>
					 <td>
                         <?php 
                               if($Socio->getIdMunicipio() > 0)
                                    echo NM01MunicipioPeer::getMunicipioById($Socio->getIdMunicipio())->getMcpioNombre();
                               else
                                    echo "<small>Debe capturar nuevamente la dirección del socio.</small>";
                          ?>  
                    </td>
                </tr>
                <tr class="odd">
                    <td width="30%" class="sorting_1">Colonia:</td>
					<td><?php 
                    if($Socio->getIdColonia())
                        echo NM01ColoniaPeer::getColoniaById($Socio->getIdColonia())->getClnaNombre();
                    else
                        echo '*(Pendiente)';
                    ?></td>
                </tr>
                <tr class="even">
                    <td width="30%" class="sorting_1">Código Postal:</td>
                    <td><?php echo $Socio->getCp() ?></td>
                </tr>
                <tr class="odd">
                    <td class="sorting_1">Calle:</td>
                    <td><?php echo $Socio->getCalle() ?></td>
                </tr>
                <tr class="even">
                    <td width="30%" class="sorting_1">Número exterior:</td>
                    <td><?php echo $Socio->getNumExt() ?></td>
                </tr>
                <tr class="odd">
                    <td class="sorting_1">Número interior:</td>
                    <td><?php echo $Socio->getNumInt() ?></td>
                </tr>
            </table>
        </div>

        <?php if($Socio->getRfc() != ''): ?>
        <div id="datosFiscales-vd">
            <h3>Datos Fiscales</h3>
            <table width="" border="0" class="dataTables_wrapper">
                <tr class="odd">
                    <td width="40%" class="sorting_1">Razon social:</td>
                    <td><?php echo $Socio->getRazonSocial(); ?></td>
                </tr>
                <tr class="even">
                    <td class="sorting_1">RFC:</td>
                    <td><?php echo $Socio->getRfc(); ?></td>
                </tr>
                <tr class="odd">
                    <td class="sorting_1">Calle:</td>
                    <td><?php echo $Socio->getCalleFiscal(); ?></td>
                </tr>
                <tr class="even">
                    <td class="sorting_1">Colonia:</td>
                    <td><?php echo $Socio->getColoniaFiscal(); ?></td>
                </tr>
                <tr class="odd">
                    <td class="sorting_1">Estado:</td>
                    <td><?php echo $Socio->getEstadoFiscal(); ?></td>
                </tr>
                <tr class="even">
                    <td class="sorting_1">Municipio:</td>
                    <td><?php echo $Socio->getMunicipioFiscal(); ?></td>
                </tr>
                <tr class="odd">
                    <td class="sorting_1">Núm ext.:</td>
                    <td><?php echo $Socio->getNumExtFiscal(); ?></td>
                </tr>
                <tr class="even">
                    <td class="sorting_1">Núm int.:</td>
                    <td><?php echo $Socio->getNumIntFiscal(); ?></td>
                </tr>
                <tr class="odd">
                    <td class="sorting_1">País:</td>
                    <td><?php echo $Socio->getPaisFiscal(); ?></td>
                </tr>
            </table>
        </div>
        <?php endif; ?>

        <div id="escolaridad-vd">
            <h3>Escolaridad</h3>
            <table width="" border="0" class="dataTables_wrapper">
                <tr class="odd">
                    <td width="40%" class="sorting_1">¿Estudia actualmente?:</td>
                    <td><?php echo ($Socio->getEstudia()==1)? 'Si':'No' ?></td>
                </tr>
                <?php 
                /*
                * Reingenieria  MAKO-ARFS-002 PUNTO 6 2a Rev
                * Responsable:  (BP) Bet Gader Porcayo Juárez
                * Fecha:        08/10/2014
                * Descripción:  Cambio de etiqueta
                */
                ?>
                <tr class="even">
                    <td class="sorting_1">Nivel de estudios actual o último</td>
                    <td><?php echo (($Socio->getNivelEstudio() !== null) ? $Socio->getNivelEstudio()->getNombre() : '') ?></td>
                </tr>
                <tr class="even">
                    <td width="40%" class="sorting_1">Tipo de escuela:</td>
                    <td><?php echo (($Socio->getTipoEscuela() !== null) ? $Socio->getTipoEscuela()->getNombre() : null) ?></td>
                </tr>
                <?php 
                /*
                * Reingenieria  MAKO-ARFS-002 PUNTO 12 2a Rev
                * Responsable:  (BP) Bet Gader Porcayo Juárez
                * Fecha:        09/10/2014
                * Descripción:  Cambio de texto a Modalidad de estudio
                */
                ?>
                <tr class="odd">
                    <td width="40%" class="sorting_1">Modalidad de estudios:</td>
                    <td><?php echo (($Socio->getTipoEstudio() !== null) ? $Socio->getTipoEstudio()->getNombre() : null) ?></td>
                </tr>
                <?php /*
                <tr class="even">
                    <td width="40%" class="sorting_1">Ultimo grado de estudios concluido al 100%:</td>
                    <td><?php echo (($Socio->getGradoEstudio() !== null) ? $Socio->getGradoEstudio()->getNombre() : '')  ?></td>
                </tr>
                <tr class="odd">
                    <td width="40%" class="sorting_1">Campo de estudio:</td>
                    <td><?php echo (($Socio->getProfesionRelatedByProfesionActualId() !== null) ? $Socio->getProfesionRelatedByProfesionActualId()->getNombre() : '') ?>
                    </td>
                </tr>
                <tr class="odd">
                    <td width="40%" class="sorting_1">Profesión:</td>
                    <td><?php echo (($Socio->getProfesionRelatedByProfesionUltimoId() !== null) ? $Socio->getProfesionRelatedByProfesionUltimoId()->getNombre() : null) ?></td>
                </tr>
                */?>
            </table>
        </div>

        <div id="ocupacion-vd">
            <h3>Ocupación</h3>
            <table width="" border="0" class="dataTables_wrapper">
            <?php 
                /*
                * Reingenieria  MAKO-ARFS-002 PUNTO 7 2a Rev
                * Responsable:  (BP) Bet Gader Porcayo Juárez
                * Fecha:        08/10/2014
                * Descripción:  Se oculta el campo de trabaja?
                */
                ?>  <!--
                <tr class="odd">
                    <td width="40%" class="sorting_1">¿Trabaja?:</td>
                    <td><?php echo ($Socio->getTrabaja()==1)? 'Si':'No' ?></td>
				</tr>-->

				<?php /*
                * Reingenieria  MAKO-ARFS-002 PUNTO 7 2a Rev
                * Responsable:  LG Se despliegan todas las opciones seleccionadas para un socio en OCUPACIÓN
                * Fecha:        08/10/2014
                * Descripción:  Se oculta el campo de trabaja?
				 */?>
                <tr class="odd">
                    <td width="40%" class="sorting_1">¿A qué se dedica?:</td>
                    <td><?php
						$ctr_ocupacion = new Criteria();
						$ctr_ocupacion->add(RelOcupacionSocioPeer::SOCIO_ID,$Socio->getId());
						$ocupacion_socio = RelOcupacionSocioPeer::doSelect($ctr_ocupacion);
							foreach($ocupacion_socio as $ocupa)
							{
								echo OcupacionPeer::retrieveByPK($ocupa->getOcupacionId())->getNombre() . "<br>";
							}
					?></td>
                </tr>
                <tr class="odd">
                    <td width="40%" class="sorting_1">Sector al que pertenece su empresa o negocio:</td>
                    <td><?php echo $Socio->getSector() ?></td>
					</tr>
                <?php /*
                <tr class="even">
                    <td class="sorting_1">Tipo de Organizacion:</td>
                    <td><?php echo $Socio->getTipoOrganizacion() ?>
                    </td>
                </tr>
                <tr class="even" style="display: none;">
                    <td class="sorting_1">Posición:</td>
                    <td><?php echo $Socio->getPosicion() ?>
                    </td>
                </tr>

                <tr class="even">
                    <td width="40%" class="sorting_1">Tiempo actividad:</td>
                    <td><?php echo $Socio->getTiempoActividad() ?></td>
                </tr>

                <tr class="odd">
                    <td width="40%" class="sorting_1">Problemas empresa:</td>
                    <td><?php echo $Socio->getProblemasEmpresa() ?></td>
                </tr>

                <tr class="even">
                    <td width="40%" class="sorting_1">Dependientes economicos:</td>
                    <td><?php echo $Socio->getDependientesEconomicos() ?></td>
                </tr>
                */?>
            </table>
        </div>

        <div id="habilidades-vd">
            <h3>Habilidades</h3>
            <table width="" border="0" class="dataTables_wrapper">
                <tr class="odd">
                    <td width="40%" class="sorting_1" class="verticalTop">Discapacidad:</td>
                    <td><?php
			$ctr_discapacidad = new Criteria();
			$ctr_discapacidad->add(RelDiscapacidadSocioPeer::SOCIO_ID,$Socio->getId());
			$discapacidad_socio = RelDiscapacidadSocioPeer::doSelect($ctr_discapacidad);
			if ( RelDiscapacidadSocioPeer::doCount($ctr_discapacidad) )
			{
	                        foreach($discapacidad_socio as $disc)
        	                {
					echo DiscapacidadPeer::retrieveByPK($disc->getDiscapacidadId())->getNombre() . "<br>";
                        	}
			}
			else
			{
				echo "Sin discapacidad";
			}
			?></td>
                </tr>
                <tr class="even">
                    <td class="sorting_1" class="verticalTop">Habilidad informática:</td>
                    <td><?php echo $Socio->getHabilidadInformatica() ?></td>
                </tr>
                <tr class="odd">
                    <td width="40%" class="sorting_1">Dominio inglés:</td>
                    <td><?php echo $Socio->getDominioIngles() ?></td>
                </tr>
            </table>
        </div>

        <div id="acceso-tecnologia-vd">
            <h3>Acceso a la tecnología</h3>
            <table width="" border="0" class="dataTables_wrapper">
                <tr>
                    <th class="verticalTop" style="padding-top: 25px;"><h3>¿Cuenta con alguno de los siguientes aparatos y/o servicios?</h3></th>
                    <td>
                        <table>
                            <tr class="even">
                                <td class="sorting_1">Computadora</td>
                                <td><?php echo ($Socio->getComputadora()=='1') ? 'Si':'No' ?></td>
                            </tr>
                            <tr class="even">
                                <td class="sorting_1">Teléfono celular</td>
                                <td><?php echo ($Socio->getTelefonoCelular()=='1') ? 'Si':'No' ?></td>
                            </tr>
                            <tr class="even">
                                <td class="sorting_1">Tableta</td>
                                <td><?php echo ($Socio->getTableta()=='1') ? 'Si':'No' ?></td>
                            </tr>
                            <tr class="even">
                                <td class="sorting_1">Línea Telefónica</td>
                                <td><?php echo ($Socio->getLineaTelefonica()=='1') ? 'Si':'No' ?></td>
                            </tr>
                            
                            <?php /* <tr class="even">
                                <td class="sorting_1">Teléfono celular inteligente</td>
                                <td><?php echo ($Socio->getTelefonoInteligente()=='1') ? 'Si':'No' ?></td>
                            </tr> */?>
                            <tr class="even">
                                <td class="sorting_1">Internet en casa</td>
                                <td><?php echo ($Socio->getInternet()=='1') ? 'Si':'No' ?></td>
                            </tr>
                            
				<?php /*
                            <tr class="even">
                                <td class="sorting_1">Internet móvil</td>
                                <td><?php echo ($Socio->getInternetMovil()=='1') ? 'Si':'No' ?></td>
                            </tr>
                            <tr class="even">
                                <td class="sorting_1">Televisión</td>
                                <td><?php echo ($Socio->getTelevision()=='1') ? 'Si':'No' ?></td>
                            </tr>
				*/?>
                        </table>
                    </td>
                </tr>
            </table>
        </div>

        <div id="acercamiento-vd">
            <h3>Medio de acercamiento</h3>
            <table width="" border="0" class="dataTables_wrapper">
                <tr class="odd">
                    <td width="40%" class="sorting_1">¿Como se enteró del centro?:</td>
                    <td><?php echo $Socio->getCanalContacto() ?></td>
                </tr>
                <?php /*
                <tr class="even">
                    <td class="sorting_1">Nombre del Promotor:</td>
                    <td><?php echo $Socio->getUsuarioRelatedByPromotorRefId() ?></td>
                </tr>
                <tr class="odd">
                    <td width="40%" class="sorting_1">Liga:</td>
                    <td><?php echo $Socio->getLiga() ?></td>
                </tr>
                <tr class="even">
                    <td class="sorting_1">Recomendó:</td>
                    <td><?php echo $Socio->getRecomienda() ?></td>
                </tr>
                <tr class="odd">
                    <td width="40%" class="sorting_1">Otro medio o contacto:</td>
                    <td><?php echo $Socio->getOtrosContacto() ?></td>
                </tr>
                <tr class="even">
                    <td class="sorting_1">Socio:</td>
                    <td><?php echo $Socio->getSocioRelatedBySocioRefId()  ?></td>
                </tr>
                <tr class="odd">
                    <td class="sorting_1">Motivo capacitación:</td>
                    <td><?php echo $Socio->getMotivoCapacitacion() ?></td>
                </tr>
                <tr class="even">
                    <td class="sorting_1">Motivo acercamiento:</td>
                    <td><?php echo $Socio->getMotivoAcercamiento() ?></td>
                </tr>
                */ ?>
            </table>
        </div>

        <div id="historial_sesiones-vd">
            <h3>Historial de sesiones</h3>
            <table width="100%" border="0" id="registrosSesiones">
                <thead>
                    <tr>
                        <th>Fecha Inicio</th>
                        <th>Fecha Fin</th>
                        <th>Ocupado</th>
                        <th>Centro</th>
                        <th>Seccion</th>
                        <th>PC</th>
                    </tr>
                </thead>
                <?php foreach($Sesiones as $sesion): ?>
                <!-- BEGIN ocupacion -->

                <tr>
                    <td><?php echo $sesion->getFechaLogin("%d-%m-%Y %H:%M:%S") ?></td>
                    <td><?php echo $sesion->getFechaLogout("%d-%m-%Y %H:%M:%S") ?>
                    </td>
                    <td><?php echo Comun::segsATxt($sesion->getOcupados())?></td>
                    <td><?php echo $sesion->getCentro()->getNombre()?></td>
                    <td><?php echo $sesion->getComputadora()->getSeccion() ?></td>
                    <td><?php echo substr($sesion->getComputadora(),-2) ?></td>
                </tr>
                <!-- END ocupacion -->
                <?php endforeach; ?>
            </table>
        </div>

        <div id="historial_compras-vd">
            <h3>Historial de compras</h3>
            <table width="100%" border="0" id="registrosCompras">
                <thead>
                    <tr>
                        <th>Folio</th>
                        <th>Tipo de orden</th>
                        <th>Comprobante</th>
                        <th>Producto</th>
                        <th>Fecha</th>
                        <th>Cantidad</th>
                        <th>Unidad</th>
                        <th>Precio <br> Unitario
                        </th>
                        <th>Descuento</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <?php foreach($Ordenes as $orden)
                {
                    foreach($orden->getDetalleOrdens() as $item)
                {?>
                <!-- BEGIN carrito -->
                <tr>
                    <td><?php echo sprintf("%010s\n", $orden->getFolio()); ?></td>
                    <td><?php echo $item->getOrden()->getTipoOrden()->getNombre() ?></td>
                    <td><?php echo ($orden->getEsFactura()=='1') ? 'Factura':'Ticket'  ?></td>
                    <td><?php echo $item->getProducto()->getNombre() ?></td>
                    <td><?php echo $orden->getCreatedAt("%d-%m-%Y") ?></td>
                    <td align="right"><?php echo  number_format($item->getCantidad(), 0); ?></td>
                    <td><?php echo $item->getProducto()->getUnidadProducto()->getNombre()?></td>
                    <td align="right">$ <?php echo $item->getPrecioLista() ?></td>
                    <td align="right"><?php  echo  number_format($item->getDescuento(), 2); ?>%</td>
                    <td align="right" nowrap="nowrap">$ <?php echo $item->getSubtotal() ?></td>
                </tr>
                <!-- END carrito -->
                <?php }
                } ?>
            </table>
        </div>

        <div id="historial_cobranza-vd">
            <h3>Historial de pagos</h3>
            <table width="100%" border="0" id="registrosCobranza">
                <thead>
                    <tr>
                        <th>Centro</th>
                        <th>Modalidad</th>
                        <th>Producto</th>
                        <th>Núm pago</th>
                        <th>Fecha a pagar</th>
                        <th>Fecha pago</th>
                        <th>Pagado</th>
                        <th>Acuerdo de pago</th>
                    </tr>
                </thead>
                <?php foreach($Cobranza as $pago): //$pago = new SocioPagos();?>
                <!-- BEGIN cobranza -->
                <tr>
                    <td><?php echo $pago->getCentro() ?></td>
                    <td><?php echo $pago->getTipoCobro() ?></td>
                    <td><?php echo $pago->getProductoRelatedByProductoId()?></td>
                    <td align="right"><?php echo $pago->getNumeroPago() ?></td>
                    <td align="center"><?php echo $pago->getFechaAPagar("%d-%m-%Y") ?></td>
                    <td align="center"><?php echo $pago->getFechaPagado("%d-%m-%Y") ?></td>
                    <td align="center"><?php echo $pago->getPagado() ? 'SI':'NO' ?></td>
                    <td align="center"><?php echo $pago->getAcuerdoPago() ? 'SI':'NO' ?></td>
                </tr>
                <!-- END cobranza -->
                <?php endforeach; ?>
            </table>
        </div>

        <div id="reporte-abonos">
            <h3>Reporte de abonos y consumos de tiempo</h3>
            <?php include_partial('reporteAbonosConsumos', array('Ordenes'=>$Ordenes,'Sesiones'=>$Sesiones,'Socio'=>$Socio))?>
        </div>

        <?php /***********************************
** @@ LGL      2013-07-08
** Se deshabilita pantalla de Nivel Socio-Economico
** Según Matriz 4M Fase II
** Id Actividad: 41
************************************



<div id="nse-vd">
<h3>Nivel socioeconomico</h3>

<table width="" border="0" class="dataTables_wrapper">

<tr class="odd">
<td width="40%" class="sorting_1"> Estado civil:</td>
<td ><?php echo $Socio->getEstadoCivil() ?></td>
</tr>

<tr class="even">
<td class="sorting_1"> Número de personas con las que comparte vivienda: </td>
<td > <?php echo $Socio->getPersonasVivienda() ?> </td>
</tr>

<tr class="odd">
<td class="sorting_1"> Servicio de salud de la que es beneficiario: </td>
<td > <?php echo $Socio->getServicioSalud() ?> </td>
</tr>

<tr class="even">
<td class="sorting_1"> Tipo de vivienda: </td>
<td > <?php echo $Socio->getViviendaTipo() ?> </td>
</tr>

<tr class="odd">
<td class="sorting_1"> La vivienda que habita es: </td>
<td > <?php echo $Socio->getViviendaEs() ?> </td>
</tr>

<tr class="even">
<td class="sorting_1"> Material del techo de la vivienda </td>
<td > <?php echo $Socio->getMaterialTecho() ?> </td>
</tr>

<tr class="odd">
<td class="sorting_1"> Material del piso de la vivienda </td>
<td > <?php echo $Socio->getMaterialPiso() ?> </td>
</tr>

<tr class="even">
<td class="sorting_1"> Número de habitaciones </td>
<td > <?php echo $Socio->getHabitaciones() ?> </td>
</tr>

<tr class="odd">
<td class="sorting_1"> Número de baños </td>
<td > <?php echo $Socio->getBanos() ?> </td>
</tr>

<tr class="even">
<td class="sorting_1"> Número de focos </td>
<td > <?php echo $Socio->getFocos() ?> </td>
</tr>

<tr class="odd">
<td class="sorting_1"> Años de residir en el domicilio </td>
<td > <?php echo $Socio->getAniosResidencia() ?> </td>
</tr>

<tr class="even">
<td class="sorting_1"> Meses de residir en el domicilio </td>
<td > <?php echo $Socio->getMesesResidencia() ?> </td>
</tr>

</table>

<h3> Servicios con que cuenta la vivienda </h3>

<table width="" border="0" class="dataTables_wrapper">

<tr class="odd">
<td class="sorting_1"> Energía eléctrica </td>
<td > <?php echo ($Socio->getEnergiaElectrica()=='1') ? 'Si':'No' ?> </td>
</tr>

<tr class="even">
<td class="sorting_1"> Agua Entubada </td>
<td > <?php echo ($Socio->getAguaEntubada()=='1') ? 'Si':'No' ?> </td>
</tr>

<tr class="odd">
<td class="sorting_1"> Drenaje </td>
<td > <?php echo ($Socio->getDrenaje()=='1') ? 'Si':'No' ?> </td>
</tr>

<tr class="even">
<td class="sorting_1"> Línea teléfonica </td>
<td > <?php echo ($Socio->getLineaTelefonica()=='1') ? 'Si':'No' ?> </td>
</tr>

<tr class="odd">
<td class="sorting_1"> Televisión de paga </td>
<td > <?php echo ($Socio->getTelevisionPaga()=='1') ? 'Si':'No' ?> </td>
</tr>

<tr class="even">
<td class="sorting_1"> Internet </td>
<td > <?php echo ($Socio->getInternet()=='1') ? 'Si':'No' ?> </td>
</tr>

</table>

<h3> Enseres </h3>

<table width="" border="0" class="dataTables_wrapper">

<tr class="odd">
<td class="sorting_1"> Lavadora </td>
<td > <?php echo ($Socio->getLavadora()=='1') ? 'Si':'No' ?> </td>
</tr>

<tr class="even">
<td class="sorting_1"> Secadora </td>
<td > <?php echo ($Socio->getSecadora()=='1') ? 'Si':'No' ?> </td>
</tr>

<tr class="odd">
<td class="sorting_1"> Televisión </td>
<td > <?php echo ($Socio->getTelevision()=='1') ? 'Si':'No' ?> </td>
</tr>

<tr class="even">
<td class="sorting_1"> Calentador de agua </td>
<td > <?php echo ($Socio->getCalentadorAgua()=='1') ? 'Si':'No' ?> </td>
</tr>

<tr class="odd">
<td class="sorting_1"> Horno de microondas </td>
<td > <?php echo ($Socio->getHornoMicroondas()=='1') ? 'Si':'No' ?> </td>
</tr>

<tr class="even">
<td class="sorting_1"> Tostador </td>
<td > <?php echo ($Socio->getTostador()=='1') ? 'Si':'No' ?> </td>
</tr>

<tr class="odd">
<td class="sorting_1"> Reproductor de video </td>
<td > <?php echo ($Socio->getReproductorVideo()=='1') ? 'Si':'No' ?> </td>
</tr>

<tr class="even">
<td class="sorting_1"> Computadora </td>
<td > <?php echo ($Socio->getComputadora()=='1') ? 'Si':'No' ?> </td>
</tr>

<tr class="odd">
<td class="sorting_1"> Teléfono celular </td>
<td > <?php echo ($Socio->getTelefonoCelular()=='1') ? 'Si':'No' ?> </td>
</tr>

<tr class="even">
<td class="sorting_1"> Reproductor de audio </td>
<td > <?php echo ($Socio->getReproductorAudio()=='1') ? 'Si':'No' ?> </td>
</tr>

<tr class="odd">
<td class="sorting_1"> Fregadero </td>
<td > <?php echo ($Socio->getFregadero()=='1') ? 'Si':'No' ?> </td>
</tr>

<tr class="even">
<td class="sorting_1"> Estufa </td>
<td > <?php echo ($Socio->getEstufa()=='1') ? 'Si':'No' ?> </td>
</tr>

<tr class="odd">
<td class="sorting_1"> Refrigerador </td>
<td > <?php echo ($Socio->getRefrigerador()=='1') ? 'Si':'No' ?> </td>
</tr>

<tr class="even">
<td class="sorting_1"> Licuadora </td>
<td > <?php echo ($Socio->getLicuadora() =='1') ? 'Si':'No' ?> </td>
</tr>

</table>

<h3>  </h3>

<table width="" border="0" class="dataTables_wrapper">

<tr class="odd">
<td class="sorting_1"> Cuántos automóviles tienen en casa </td>
<td > <?php echo $Socio->getAutomoviles() ?> </td>
</tr>

<tr class="even">
<td class="sorting_1"> ¿Tiene automóvil propio? </td>
<td > <?php echo ($Socio->getAutomovilPropio()=='1') ? 'Si':'No' ?> </td>
</tr>

<tr class="odd">
<td class="sorting_1"> Uso que le da al automóvil </td>
<td > <?php echo $Socio->getUsoAutomovil() ?> </td>
</tr>

<tr class="even">
<td class="sorting_1"> Padre </td>
<td > <?php echo $Socio->getSocioRelatedBySocioPadreId() ?> </td>
</tr>

<tr class="odd">
<td class="sorting_1"> Vive (s/n) </td>
<td > <?php echo ($Socio->getSocioPadreVive()=='1') ? 'Si':'No'   ?> </td>
</tr>

<tr class="even">
<td class="sorting_1"> Madre </td>
<td > <?php echo $Socio->getSocioRelatedBySocioMadreId() ?> </td>
</tr>

<tr class="odd">
<td class="sorting_1"> Vive (s/n) </td>
<td > <?php echo ($Socio->getSocioMadreVive()=='1') ? 'Si':'No' ?> </td>
</tr>

</table>

     </div>*/?>

    </div>
</div>
