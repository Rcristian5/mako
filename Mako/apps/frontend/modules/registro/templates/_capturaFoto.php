<?php use_javascript('foto/webcam.js') ?>

<!----------- Comienza fragmento de control de fotografías  ----------->

<a href="javascript:void(0);" tabindex="1" id="liga-control-foto"> <img
	border="0" class="sombra-delgada" id="img_capturar_foto"
	src="/imgs/sin_foto_sh.jpg" title="Fotograf&iacute;a"
	style="float: right;" width="100" height="155" />
</a>

<script language="JavaScript">
	        /**
	         * Despliega el control de toma de fotografia
	         */
	         var pathFotos = "<?php echo sfConfig::get('app_dir_fotos_usuarios')?>";
	         var urlAccion = "<?php echo url_for('registro/subirFoto')?>";
	         
	        $(document).ready(function() 
	    	{
	    	console.log('Crea wcm');

			if($("#socio_foto").val() != '')
			{
				$('#img_capturar_foto').attr('src','/'+pathFotos+'/'+$("#socio_foto").val());
			}
		        
	    	$("#fragfotos").append(webcam.get_html(220, 340));
	    	var $wcm = dialogCamera = $("#fragfotos").dialog(
			{
	    		modal: true,
	    		autoOpen: false,
	    		position: ['center','top'],
	    		title:	'Tomar fotografía',
	    		buttons: {
		    		'Tomar foto': function() 
		    		{
						webcam.snap();
					},
					'Cerrar': function() 
		    		{
						$(this).dialog('close');
					},
					'Cancelar': function() 
		    		{
						$(this).dialog('close');
					}
					
				}
   			});
	    	$('#liga-control-foto').click(function() 
	    	{
   	    		$wcm.dialog('open');
   	    		$("#cont_foto").show();
	    	});
	    			
	        webcam.set_api_url(urlAccion);
            webcam.set_quality(100); // Calidad de la imagen
            webcam.set_shutter_sound(false); // Sonido del "click"

            $(document).bind('foto.tomada',function(ev,msg){
                var foto = '/'+pathFotos+'/'+msg;
                $('#socio_foto').val(msg);
                $('#img_capturar_foto').attr('src',foto);
                $('#cont_foto').hide();
                $wcm.dialog('close');
            });
	    });

	</script>

<div id="fragfotos" style="display: none; text-align: 'center';"
	align="center"></div>

<!----------- Termina fragmento de control de fotografías  ----------->
