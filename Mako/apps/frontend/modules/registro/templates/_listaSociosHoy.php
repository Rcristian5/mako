<?php
//Time set para mexico
date_default_timezone_set('America/Mexico_City');
setlocale (LC_TIME, 'es_ES.UTF-8');
?>

<?php if ($msg!=''): ?>
<div class="flash_notice">
    <?php echo $msg ?>
</div>
<?php endif; ?>

<?php foreach ($Socios as $Socio): ?>

<fieldset id="fs_<?php echo $Socio->getId() ?>" class="socio-encontrado">
    <legend>
        Folio: <a href="javascript:void(0);"
            onclick="$(this).trigger('socios.ver-detalle',['<?php echo $Socio->getId(); ?>']);">
            <?php echo $Socio->getFolio() ?>
        </a>
    </legend>
    <table width="100%" border="0">

        <tr>
            <td width="10%">
                <div class="container fotoChica">
                    <img
                        src="<?php echo ($Socio->getFoto() != '')? '/fotos/'.$Socio->getFoto() : '/imgs/sin_foto_sh.jpg' ?>"
                        class="main fotoChica" /> <img class="minor fotoChica"
                        src="/imgs/frame.png">
                </div>
            </td>
            <td width="90%"><?php echo $Socio ?><br /> <?php echo $Socio->getUsuario() ?><br />
                <?php echo $Socio->getEdad() ?> años<br />
            </td>
        </tr>
        <tr>
            <td colspan="2">Alta en: <?php echo $Socio->getCentro()->getNombre() ?>
                <br /> Fecha Nac: <?php echo $Socio->getFechaNac("%d-%m-%Y") ?><br />
                Género: <?php echo ($Socio->getSexo()=='M') ? 'Masculino':'Femenino' ?><br />
                Estudia: <?php echo ($Socio->getEstudia()=='1') ? (($Socio->getNivelEstudio()!=null)?$Socio->getNivelEstudio():'Si'):'No' ?><br />
                <!--Trabaja: <?php echo ($Socio->getTrabaja()=='1') ? 'Si':'No' ?><br />-->
                Tiempo PC: <?php echo Comun::segsATxt($Socio->getSaldo())   ?> <br>
                <p>
                    <a href="javascript:void(0);"
                        id="liga-sel-<?php echo $Socio->getId() ?>"
                        onclick="$(this).trigger('socio.seleccionado',['<?php echo $Socio->getId(); ?>','<?php echo $Socio; ?>']);"
                        socio_id="<?php echo $Socio->getId(); ?>"
                        socio_usuario="<?php echo $Socio->getUsuario(); ?>"
                        socio_nombre="<?php echo $Socio->getNombre(); ?>"
                        socio_apepat="<?php echo $Socio->getApepat(); ?>"
                        socio_apemat="<?php echo $Socio->getApepat(); ?>"> Seleccionar </a>
                </p>
            </td>
        </tr>
    </table>
</fieldset>

<?php endforeach; ?>
