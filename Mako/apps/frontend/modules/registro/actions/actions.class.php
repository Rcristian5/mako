<?php
/**
 * registro actions.
 *
 * @package    mako
 * @subpackage registro
 * @author     Bixit SA de CV
 * @version    SVN: $Id: actions.class.php,v 1.37 2012-02-15 00:42:34 eorozco Exp $
 */
class registroActions extends sfActions
{
    public function executeIndex(sfWebRequest $request)
    {

        $this->Socios = SocioPeer::listaSocios($request->getParameter('idSocio'), $request->getParameter('qNombre'), $request->getParameter('qUsuario'), $request->getParameter('qFolio'), $request->getParameter('limit'));

        if (empty($request['idSocio']) && empty($request['qNombre']) && empty($request['qUsuario']) && empty($request['qFolio'])) $this->msg    = '';
        else {
            if (!count($this->Socios)) {
                $this->msg    = 'No se han encontrado socios coincidentes con los datos proporcionados.';
            }
        }
    }

    public function executeShow(sfWebRequest $request)
    {
        $this->Socio  = SocioPeer::retrieveByPk($request->getParameter('id'));
        $this->forward404Unless($this->Socio);
    }

    public function executeNew(sfWebRequest $request)
    {

        $Socio           = SocioPeer::retrieveByPk($request->getParameter('id'));
        //$this->form = new SocioForm($Socio);

        /**
         * Agregamos el uso de la variable de cofiguracion registro_uso_map que indica si se hara uso o no del servicio de google para generar la ubicacion.
         * @author silvio.bravo@enova.mc
         * @title Reingenieria Mako
         * @date 3 Marzo 2014
         */
        $this->appUsaMap = (sfConfig::get('app_registro_uso_map') == false) ? 0 : 1;
        $this->Centro    = CentroPeer::retrieveByPK(sfConfig::get('app_centro_actual_id'));
        $this->form      = new SocioForm($Socio, array('url' => $this->getController()->genUrl('registro/consultaPromotores')));
    }

    public function executeCreate(sfWebRequest $request)
    {
        $this->forward404Unless($request->isMethod(sfRequest::POST));
        $this->form = new SocioForm(null, array('url' => $this->getController()->genUrl('registro/consultaPromotores?isNew=1')));

        $this->processForm($request, $this->form);

        $this->setTemplate('new');
    }

    public function executeEdit(sfWebRequest $request)
    {
        $Socio      = SocioPeer::retrieveByPk($request->getParameter('id'));
        $this->forward404Unless($Socio, sprintf('Object Socio does not exist (%s).', $request->getParameter('id')));
        $this->form = new SocioForm($Socio, array('url' => $this->getController()->genUrl('registro/consultaPromotores')));
        $this->setTemplate('new');
    }

    public function executeUpdate(sfWebRequest $request)
    {
        $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
        $this->forward404Unless($Socio      = SocioPeer::retrieveByPk($request->getParameter('id')), sprintf('Object Socio does not exist (%s).', $request->getParameter('id')));
        $this->form = new SocioForm($Socio, array('url' => $this->getController()->genUrl('registro/consultaPromotores')));

        $this->processForm($request, $this->form);

        $this->setTemplate('new');
    }

    public function executeDelete(sfWebRequest $request)
    {
        $request->checkCSRFProtection();

        $this->forward404Unless($Socio = SocioPeer::retrieveByPk($request->getParameter('id')), sprintf('Object Socio does not exist (%s).', $request->getParameter('id')));
        $Socio->delete();

        $this->redirect('registro/index');
    }

    protected function processForm(sfWebRequest $request, sfForm $form)
    {

        $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));

        if ($form->isValid()) {
            //Asociamos el operador que esta dando salida.
            $operador = $this->getUser()->getAttribute('usuario')->getId();
            $form->getObject()->setOperadorId($operador);

		
                $post   = $request->getPostParameters();
			$discapacidades = $post['socio']['socio_discapacidad'];
			$ocupaciones = $post['socio']['socio_ocupacion'];
		//error_log("DISCAPACIDADES_SELECC : " . print_r($discapacidades,true));

            if ($form->isNew()) {
                //Seteamos el centro al que pertenece el usuario
                $form->getObject()->setCentroId(sfConfig::get('app_centro_actual_id'));
                //Agregamos el rol default del socio
                $criteria = new Criteria();
                $criteria->add(RolPeer::NOMBRE, sfConfig::get('app_rol_socios'), Criteria::EQUAL);
                $form->getObject()->setRol(RolPeer::doSelectOne($criteria));
                //Construimos un nombre de usuario válido
                $post   = $request->getPostParameters();
                $nombre = $post['socio']['nombre'];
                $apepat = $post['socio']['apepat'];

                $form->getObject()->setUsuario($form->getObject()->generaUsuario($nombre, $apepat));
                //Generamos la clave.
                $form->getObject()->setClave(Comun::generaClave());
            }

            $Socio = $form->save();

		//Una vez que se actualizó la información del socio, guardamos sus discapacidades
		$criteria_discapacidad = new Criteria();
		$criteria_discapacidad->add(RelDiscapacidadSocioPeer::SOCIO_ID,$Socio->getId());
		//$discapacidad_socio = RelDiscapacidadSocioPeer::doSelect($criteria_discapacidad);
		RelDiscapacidadSocioPeer::doDelete($criteria_discapacidad);
		foreach($discapacidades as $discapacidad)
		{
			$r_disc_socio = new RelDiscapacidadSocio();
			$r_disc_socio->setSocioId($Socio->getId());
			$r_disc_socio->setDiscapacidadId($discapacidad);
			$r_disc_socio->save();
		}

		//Una vez que se actualizó la información del socio, guardamos sus ocupaciones
		$criteria_ocupacion = new Criteria();
		$criteria_ocupacion->add(RelOcupacionSocioPeer::SOCIO_ID,$Socio->getId());
		//$discapacidad_socio = RelDiscapacidadSocioPeer::doSelect($criteria_discapacidad);
		RelOcupacionSocioPeer::doDelete($criteria_ocupacion);
		foreach($ocupaciones as $ocupacion)
		{
			$r_ocupa_socio = new RelOcupacionSocio();
			$r_ocupa_socio->setSocioId($Socio->getId());
			$r_ocupa_socio->setOcupacionId($ocupacion);
			$r_ocupa_socio->save();
		}


            if ($form->isNew()) {
                sfProjectConfiguration::getActive()->getEventDispatcher()->notify(new sfEvent($Socio, 'socio.registrado', array('socio'                  => $Socio)));
            } else {
                $usuario_anterior = null;
                if ($form->cambioNombreUsuario()) {
                    error_log("Detecta cambio desde action. Anterior: " . $form->getSocioAnterior());
                    //Si ha cambiado el nombre o el apellido, recreamos el nombre de usuario
                    //Pasamos al evento del ldap el nombre anterior para modificar el cn
                    $socioAnterior    = $form->getSocioAnterior();
                    $usuario_anterior = $socioAnterior->getUsuario();
                    //Construimos un nombre de usuario válido

                    //Ahora el nombre de usuario en modificacion se construye en preUpdateObject de la forma SocioForm.class.php


                }

                sfProjectConfiguration::getActive()->getEventDispatcher()->notify(new sfEvent($Socio, 'socio.actualizado', array('socio'           => $Socio, 'modificaUsername'           => $form->cambioNombreUsuario(), 'usuario_anterior'           => $usuario_anterior)));
            }
            //$this->redirect('registro/new?id='.$Socio->getId());
            $this->msg = "Datos guardados correctamente.";
            if ($form->isNew()) {
                $this->redirect('registro/index?idSocio=' . $Socio->getId() . '&nuevoSocio=1');
            }else{
                $this->redirect('registro/index?idSocio=' . $Socio->getId());
            }
        }
    }

    protected function executeLista(sfWebRequest $request)
    {
        $this->Socios = SocioPeer::doSelect(new Criteria());
    }
    /**
     * Ejecuta la consulta de lista promotores y la regresa en formato json.
     * @param sfWebRequest $request
     * @return string
     */
    public function executeConsultaPromotores(sfWebRequest $request)
    {
        $this->getResponse()->setContentType('application/json');
        $promotores = UsuarioPeer::listaPromotores($request->getParameter('q'), $request->getParameter('limit'));
        return $this->renderText(json_encode($promotores));
    }
    /**
     * Ejecuta la busqueda de socios
     * @param sfWebRequest $request
     * @return unknown_type
     */

    public function executeBuscaSocio(sfWebRequest $request)
    {
        $this->Socios = SocioPeer::listaSocios(null, $request->getParameter('qNombre'), $request->getParameter('qUsuario'), $request->getParameter('qFolio'), $request->getParameter('limit'));
        if (!count($this->Socios)) {
            $this->msg    = 'No se han encontrado socios coincidentes con los datos proporcionados.';
        }
        return $this->renderPartial('registro/listaSocios', array('Socios'           => $this->Socios, 'msg'           => $this->msg));
    }
    /**
     * Lista socios registrados en el rango seleccionado. Si no se ingresa rango entonces en el dia de hoy.
     * @param sfWebRequest $request
     */
    public function executeRegistrosRango(sfWebRequest $request)
    {
        $fecha_ini = pg_escape_string($request->getParameter('fecha_ini'));
        $fecha_fin = pg_escape_string($request->getParameter('fecha_fin'));
        if ($fecha_ini == '' or $fecha_fin == '') {
            $fecha_ini = date("Y-m-d") . " 00:00:00";
            $fecha_fin = date("Y-m-d") . " 23:59:59";
        }
        $c         = new Criteria();
        $c->add(SocioPeer::CREATED_AT, $fecha_ini, Criteria::GREATER_EQUAL);
        $c->addAnd(SocioPeer::CREATED_AT, $fecha_fin, Criteria::LESS_EQUAL);

        $this->Socios = SocioPeer::doSelect($c);
        if (!count($this->Socios)) {
            $this->msg    = 'No se han encontrado socios para el rango de fechas dado.';
        }
        return $this->renderPartial('registro/listaSociosHoy', array('Socios' => $this->Socios, 'msg' => $this->msg));
    }
    /**
     * Acción que recibe los datos de la webcam y los almacena en el filesystem como una fotografía jpg.
     * retornando el nombre de la fotografía.
     * @param sfWebRequest $request
     * @return string
     */
    public function executeSubirFoto(sfWebRequest $request)
    {
        $this->getResponse()->setContentType('text/javascript');

        $path       = sfConfig::get('sf_root_dir') . DIRECTORY_SEPARATOR . sfConfig::get('sf_web_dir_name') . 'web' . DIRECTORY_SEPARATOR . sfConfig::get('app_dir_fotos_usuarios');
        $nombre_img = "";
        $nombre_img = date('YmdHis') . '.jpg';
        $filename   = $path . DIRECTORY_SEPARATOR . $nombre_img;
        $result     = file_put_contents($filename, file_get_contents('php://input'));
        if (!$result) {
            $msgerr     = "ERROR[TomaFoto]: No se pudo escribir el archivo $filename, checar permisos de escritura.";
            error_log($msgerr);
            return $this->renderText("ERROR");
        }
        return $this->renderText($nombre_img);
    }
    /**
     * Baja de Socio
     * @param sfWebRequest $request
     * @return array
     */

    public function executeBaja(sfWebRequest $request)
    {

        $this->getResponse()->setContentType('application/json');

        $Socio = SocioPeer::retrieveByPk($request->getParameter('socio_id'));
        $Socio->setActivo(0);
        $Socio->save();

        return $this->renderText(json_encode(array('msg' => "El socio ha sido dado de baja correctamente.")));
    }
    /**
     * Ejecuta la reposicion de la tarjeta, guarda el nuevo folio y levanta el cargo.
     * @param sfWebRequest $request
     * @return array
     */
    public function executeReposicionTarjeta(sfWebRequest $request)
    {
        error_log("Entramos a reposicion de credencial::::::::::");
        $this->getResponse()->setContentType('application/json');
        //Necesitamos estar seguros q la operacion se ejecuta desde caja
        $pc      = ComputadoraPeer::getPcPorIp(Comun::ipReal());
        if ($pc != null) {
            $tipo_pc = $pc->getTipoId();
        } else {
            return $this->renderText(json_encode(array('msg' => "Esta operación debe hacerse desde la PC de Caja.")));
            exit;
        }
        if ($tipo_pc != sfConfig::get('app_caja_tipo_pc_id')) {
            return $this->renderText(json_encode(array('msg' => "Esta operación debe hacerse desde la PC de Caja.")));
            exit;
        }

        error_log("SOmos una computadora de caja::::::::::");

        $Socio = SocioPeer::retrieveByPk($request->getParameter('socio_id'));
        $Socio->setFolio($request->getParameter('folio'));
        $Socio->save();
        error_log("Se guardaron los datos.::::::::::::::::::::::::::::::::");
        


        $cajero = $this->getUser()->getAttribute('usuario');
        $this->dispatcher->notify(new sfEvent($this, 'socio.reposicion_credencial', array(
        'socio' => $Socio,
        'cajero_id' => $cajero->getId()
        )));

        error_log("Enviamos la notificacion::::::::::::::::::::");
        
        return $this->renderText(json_encode(array('msg' => "El folio ha sido cambiado correctamente.")));
    }
    /**
     * Ejecuta la impresion de los datos del usuario.
     * @param sfWebRequest $request
     * @return array
     */
    public function executeImprimirUsuarioClave(sfWebRequest $request)
    {
        $this->getResponse()->setContentType('application/json');
        $Socio = SocioPeer::retrieveByPk($request->getParameter('socio_id'));

        $this->dispatcher->notify(new sfEvent($this, 'socio.ticket.solicitado', array('socio' => $Socio)));

        return $this->renderText(json_encode(array('msg'             => "Datos impresos correctamente.")));
    }
    /**
     * Regresa el partial con los detalles del socio y su historial de compras y de sesiones.
     * @param sfWebRequest $request
     * @return unknown_type
     */
    public function executeVerDetalle(sfWebRequest $request)
    {
        $this->Socio = SocioPeer::retrieveByPk($request->getParameter('id'));
        $Ordenes     = OrdenPeer::historialComprasSocios($request->getParameter('id'));
        $Sesiones    = SesionHistoricoPeer::historialSesionesSocios($request->getParameter('id'));
        $Cursos      = GrupoAlumnosPeer::cursosTomadosDetalle($request->getParameter('id'));
        $Cobranza    = SocioPagosPeer::historialCobranza($request->getParameter('id'));
        return $this->renderPartial('registro/verDetalle', array('Socio' => $this->Socio, 'Ordenes' => $Ordenes, 'Sesiones' => $Sesiones, 'Cursos' => $Cursos, 'Cobranza' => $Cobranza));
    }
    /**
     * Ejecuta la consulta de lista promotores y la regresa en formato json.
     * @param sfWebRequest $request
     * @return string
     */
    public function executeConsultaEmpresas(sfWebRequest $request)
    {
        $this->getResponse()->setContentType('application/json');
        $empresas = EmpresaPeer::listaEmpresas($request->getParameter('q'), $request->getParameter('limit'));
        return $this->renderText(json_encode($empresas));
    }
    /**
     * Dado un pk de socio regresa una cadena que corresponde al arreglo de datos serializado del socio
     *
     * @WSMethod(webservice='MakoApi')
     * @param string $socio_id
     *
     * @return string Regresa un arreglo socio serializado
     */
    public function executeGetSocio(sfWebRequest $request)
    {
        $socio_id = pg_escape_string($request->getParameter('socio_id'));

        $socio    = SocioPeer::retrieveByPK($socio_id);

        if ($socio == null) {
            $me       = $this->isSoapRequest() ? new SoapFault('Server', 'No existe el usuario solicitado') : 'No existe el usuario solicitado';
            throw $me;
            return sfView::ERROR;
        } else {
            $this->result = serialize($socio->toArray());
            return sfView::SUCCESS;
        }
    }
    /**
     * Dado un username de socio regresa una cadena que corresponde al arreglo de datos serializado del socio
     *
     * @WSMethod(webservice='MakoApi')
     * @param string $usuario
     *
     * @return string Regresa un arreglo socio serializado
     */
    public function executeGetSocioPorUsuario(sfWebRequest $request)
    {
        $usuario = trim(strtolower(pg_escape_string($request->getParameter('usuario'))));
        $c       = new Criteria();
        $c->add(SocioPeer::USUARIO, $usuario);
        $socio = SocioPeer::doSelectOne($c);

        if ($socio == null) {
            $me    = $this->isSoapRequest() ? new SoapFault('Server', 'No existe el usuario solicitado') : 'No existe el usuario solicitado';
            throw $me;
            return sfView::ERROR;
        } else {
            $this->result = serialize($socio->toArray());
            return sfView::SUCCESS;
        }
    }
    /**
     * Dada una fecha devuelve un arreglo serializado con los registros de socios inactivos.
     * La definición de socios inactivos son aquellos que llvean mas de 4 semanas sin ir a un centro. Aunque se reportan
     * sólo los socios entre las 4 y las 8 semanas.
     *
     * @WSMethod(webservice='MakoApi')
     * @param string $fecha
     *
     * @return string Regresa un arreglo socio serializado
     */
    public function executeListaInactivos(sfWebRequest $request)
    {

        $configuration  = ProjectConfiguration::getApplicationConfiguration('frontend', 'tolu2', true);
        $context        = sfContext::createInstance($configuration);

        $centro_id      = sfConfig::get('app_centro_actual_id');

        $fecha          = pg_escape_string($request->getParameter('fecha'));
        if ($fecha == null or $fecha == '') {
            $fecha          = date("Y-m-d");
        }

        $centro_id      = sfConfig::get('app_centro_actual_id');

        $listaInactivos = array();

        $listaInactivos = SocioPeer::sociosInactivos($centro_id, $fecha);

        if (count(listaInactivos) == 0) {
            $me             = $this->isSoapRequest() ? new SoapFault('Server', 'No hay registros') : 'No no hay registros';
            throw $me;
            return sfView::ERROR;
        } else {
            $this->result = serialize($listaInactivos);
            return sfView::SUCCESS;
        }
    }
    /**
     *
     * Servicio que recibe datos para retornar municipios o colonias en formato JSON dependiendo del tipo de busqueda que se requiera realizar
     *
     * @param sfWebRequest $request
     * @return array JSON
     * @author silvio.bravo@enova.mx 20-Marzo-2014 Reingen
     * @copyright Enova
     * @category Reingenieria Mako
     */

    public function executeBuscaDatosDireccion(sfWebRequest $request)
    {
        error_log(sfConfig::get('sf_environment'));
        $resultado = array("success" => true,
                           "data"    => array(),
                           "key"     => "");
        $valorId   = $request->getParameter('valorId');

        $this->getResponse()->setContentType('application/json');

        switch ($request->getParameter('llave')) {
            case "municipio":
                $resultado["data"] = NM01MunicipioPeer::getMunicipioByEstadoId($valorId);
                error_log('municipio');
                break;

            case "colonia":
                $resultado["data"] = NM01ColoniaPeer::getColoniaByMunicipioId($valorId);
                error_log('colonia');
                break;

            case "cp":
                $resultado["data"] = NM01ColoniaPeer::getAllResultByCp($valorId);
                error_log('cp');
                break;

            default:
                $resultado["success"] = false;
                error_log('default');
                break;
        }

        return $this->renderText(json_encode($resultado));
    }

    public function executeBuscaGradosEstudio($request)
    {
        $resultado = array('success'           => true, 'data'           => array());
        $nivel     = $request->getParameter('nivel');

        $this->getResponse()->setContentType('application/json');

        $resultado['data'] = GradoEstudioPeer::getGradosEstudioByIdNivelEstudio($nivel);

        return $this->renderText(json_encode($resultado));
    }
    /*
     * **********************************************************************************************************
     * 					INICIO DE METODOS PARA LA CREACION DE WEB SERVICE
     * **********************************************************************************************************
    */
    /*
     * **********************************************************************************************************
     * Implementacion de web services  en symfony a traves del plugin ckWebServicePlugin que es el se ocupo		*
     * para desarrollar el sistema																				*
     * @author silvio.bravo@gmail.com																			*
     * @date 18 MArzo 2014																						*
     * @title Reingenieria Mako.																				*
     * **********************************************************************************************************
     * */
    /**
     * **********************************************************************************************************
     * Registramos un socio exponiendo este metodo mediante un web services, 									*
     * basicamente utilizamos las funcionalidades que ya estan definidas anteriormente							*
     * @WSMethod(webservice='MakoApi') 																			*
     * @param string $centroId an Array of centro_id															*
     * @param string $datos cadena json con los datos del nuevo socio											*
     * @return string Regresa cadena en formato JSON															*
     * @author silvio.bravo@enova.mx 20-Marzo-2014 Reingen														*
     * @copyright Enova																							*
     * @category Reingenieria Mako																				*
     * **********************************************************************************************************
     */

    public function executeAgregaSocio(sfWebRequest $request)
    {

        $result       = array("success"              => "false", "message"              => "No se han recibido los parametros necesarios.", "data"              => array());
        $datos        = array();
        $centroId     = pg_escape_string($request->getParameter("centroId"));
        $aux          = json_decode($request->getParameter("datos"));
        $datos        = Comun::stdClassToArray($aux);
        $datos["CentroId"]              = $centroId;

        if (!$request->hasParameter("centroId") || !$request->hasParameter("datos")) {

            $result["success"]              = "false";
            $result["message"]              = "Se esperaban los campos centroId y datos(array de datos)";
        } else {

            $tmp          = $this->setNewSocioFromHash($datos); //Servicio que realiza el regstro del Socio.
            if ($tmp["success"] == true) { //El registro se ha realizado correctamente

                $socio        = $tmp["data"];
                $result["success"]              = true;
                $result["message"]              = "El Socio " . $socio->getNombreCompleto() . " se ha registrado Correctamente";
                $result["data"]              = array("usuario"              => $socio->getUsuario(), "clave"              => $socio->getClave());
            } else {

                $result["success"]              = "false";
                $result["message"]              = $tmp["message"];
                $result["data"]              = $tmp["data"];
            }
        }

        $this->result = json_encode($result);
        return sfView::SUCCESS;
    }
    /**
     * **********************************************************************************************************
     * Realizamos el registro de un socio a partir de un array de datos.										*
     * @param array $datos //hash que contempla los datos necesarios para registrar un nuevo socio.				*
     * @return array con la sig estructura: success, data si se ha podido registrar success es = true 			*
     * y data contiene el objeto de tipo socio.De lo contrario success=false y data=null						*
     * @author silvio.bravo@enova.mx 20-Marzo-2014 Reingen														*
     * @copyright Enova																							*
     * @category Reingenieria Mako																				*
     * **********************************************************************************************************
     */

    private function setNewSocioFromHash(array $datos)
    {

        $requiredValues = array("Nombre"                => 0, "Apepat"                => 0, "IdEstado"                => 0, "Folio"                => 0, "FechaNac"                => 0, "Sexo"                => 0);
        $result         = array("success"                => false, "message"                => "", "data"                => null);
        $oqbjectOk      = true;

        if (Comun::keysArrayExistInArray($requiredValues, $datos) == false) { //Validamos si estan los datos obligatorios

            $result["message"]                = "Faltaron algunos de los siguientes valores requeridos: " . implode(", ", array_keys($requiredValues));
            $result["success"]                = false;
            return ($result);
        }

        $nombre = $datos["Nombre"];
        $apepat = $datos["Apepat"];
        $datos["IdEstado"]        = 15;
        $socio  = new Socio();
        $datos["Usuario"]        = $socio->generaUsuario($nombre, $apepat);
        $datos["Clave"]        = Comun::generaClave();

        try {

            $socio->fromArray($datos);
            $socio->save();
            $result["success"] = true;
            $result["data"] = $socio;
            //Notificamos para ldap y Moodle.
            sfProjectConfiguration::getActive()->getEventDispatcher()->notify(new sfEvent($socio, 'socio.registrado', array('socio' => $socio)));
        }
        catch(Exception $e) {

            $result["success"] = false;
            $result["message"] = "Ocurrio un error al momento de registrar el Nuevo Socio, por favor verifique el nombre de las variables ";
            $result["message"].= "y los tipos de datos enviados de acuerdo a lo establecido en la variable data ";
            $result["data"] = $this->getSocioColumDataType();
        }

        return ($result);
    }
    /**
     * **********************************************************************************************************
     * Obtenemos el nombre y el tipo de datos de las propiedades de la clase Socio.								*
     * @return array de datos que contiene el nombre de la propiedad y el tipo de dato de la misma.				*
     * @author silvio.bravo@enova.mx 21 Marzo 2014																*
     * @copyright Enova																							*
     * @category Reingenieria Mako																				*
     * **********************************************************************************************************
     *
     */

    private function getSocioColumDataType()
    {

        $keys         = SocioPeer::getFieldNames();
        $vtdatos      = array();
        $socioTable   = SocioPeer::getTableMap();
        $socioColumns = $socioTable->getColumns();
        $n            = 0;
        foreach ($socioColumns as $column) {
            $vtdatos[$keys[$n]]              = $column->getType();
            $n++;
        }
        return $vtdatos;
    }
    /*
     * **********************************************************************************************************
     * 					FIN DE METODOS PARA LA CREACION DE WEB SERVICE
     * **********************************************************************************************************
    */
}
