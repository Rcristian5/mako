<?php

class RegistroComponents extends sfComponents
{
	public function executeBuscaSocios()
	{
		if(!isset($this->Socios))
		{
			$this->Socios = array();
		}
		if(isset($this->socio_id) ||  isset($this->qNombre) || isset($this->qUsuario) || isset($this->qFolio))
		{
			$this->Socios = SocioPeer::listaSocios(
					($this->socio_id)?$this->socio_id:null,
					($this->qNombre)?$this->qNombre:null,
					($this->qUsuario)?$this->qUsuario:null,
					($this->qFolio)?$this->qFolio:null,
					50);
		}

		if(!isset($this->msg))
			$this->msg = "";
			
	}


	public function executeVerDetalle()
	{
		if(!isset($this->Socio))
			$this->Socio = array();
	}

}

?>