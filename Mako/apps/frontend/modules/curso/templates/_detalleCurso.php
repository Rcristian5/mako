<table id="detalle_curso" class="display" style="width: 100%;">
	<thead>
		<tr>
			<th class="acciones-tabla"></th>
			<th>Categoria</th>
			<th>Clave</th>
			<th>Nombre</th>
			<th>Nombre corto</th>
			<th>Descripci&oacute;n</th>
			<th>Perfil facilitador</th>
			<th>Duración (Hrs.)</th>
			<th>Seriado</th>
			<th>Asociado Moodle</th>
			<th>Asociado RS</th>
			<th class="acciones-tabla"></th>
			<th class="acciones-tabla"></th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($detalle as $curso): ?>
		<tr>
			<td><a href="<?php echo url_for('curso/edit?id='.$curso->getId()) ?>"><span
					class="ui-icon ui-icon-pencil bx-title" title="Editar curso"></span>
			</a>
			</td>
			<td><?php echo $curso->getCategoriaCurso()->getNombre() ?>
			</td>
			<td><?php echo $curso->getClave() ?>
			</td>
			<td><?php echo $curso->getNombre() ?>
			</td>
			<td><?php echo $curso->getNombreCorto() ?>
			</td>
			<td><?php echo $curso->getDescripcion() ?>
			</td>
			<td><?php echo $curso->getPerfilFacilitador()->getNombre() ?>
			</td>
			<td><?php echo $curso->getDuracion() ?>
			</td>
			<td><?php echo ($curso->getSeriado() ?'Si': 'No') ?>
			</td>
			<td><?php echo ($curso->getAsociadoMoodle() ?'Si': 'No') ?>
			</td>
			<td><?php echo ($curso->getAsociadoRs() ?'Si': 'No') ?>
			</td>
			<td><?php echo ($curso->getActivo() ? link_to('<span class="ui-icon ui-icon-bullet bx-title" title="Desactivar curso"></span>', 'curso/disabled?id='.$curso->getId(), array('confirm' => '¿Deseas desactivar este curso?')) : link_to('<span class="ui-icon ui-icon-radio-on bx-tittle" title="Activar curso"></span>', 'curso/enabled?id='.$curso->getId(), array('confirm' => '¿Deseas activar este curso?')) ) ?>
			</td>
			<td><?php echo link_to('<span class="ui-icon ui-icon-trash bx-title" title="Borrar curso"></span>', 'curso/delete?id='.$curso->getId(), array('method' => 'delete', 'confirm' => '¿Deseas elimar este curso? \n Todos los datos asociados serán borrados')) ?>
			</td>
		</tr>
		<?php endforeach ?>
	</tbody>
</table>
