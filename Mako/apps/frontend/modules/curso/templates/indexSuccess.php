<?php use_stylesheet('control_escolar/control_escolar.css') ?>
<?php use_stylesheet('control_escolar/curso.css') ?>
<?php use_stylesheet('datatables.css') ?>
<?php use_stylesheet('jquery.windows.css') ?>
<?php use_javascript('jquery.ui.js') ?>
<?php use_javascript('plugins/jquery.windows.js') ?>
<?php use_javascript('plugins/jquery.dataTables.js') ?>
<?php use_javascript('control_escolar/curso.js') ?>

<div id="windows-wrapper">
	<div id="columnaIzquierda" window-title="Cursos" window-state="min">

		<?php include_partial('form', array('form' => $form)) ?>

	</div>

	<div id="columnaDerecha" window-title="Cursos registrados"
		window-state="max">

		<h2>Cursos Registrados</h2>

		<?php include_partial('detalleCurso', array('detalle' => $detalle)) ?>
	</div>

</div>

