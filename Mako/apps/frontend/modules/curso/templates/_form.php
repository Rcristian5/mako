<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<?php use_stylesheet('./curso/_form.css')?>

<?php if ($form->getObject()->isNew()): ?>
<h2>Crear curso</h2>
<?php else: ?>
<h2>Editar curso</h2>
<?php endif; ?>
<form
	action="<?php echo url_for('curso/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>"
	method="post"
	<?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>
	onsubmit="return requeridos();">
	<?php if (!$form->getObject()->isNew()): ?>
	<input type="hidden" name="sf_method" value="put" />
	<?php endif; ?>
	<table width="100%">
		<tfoot>
			<tr>
				<td colspan="2"><?php echo $form->renderHiddenFields(true) ?>
				</td>
			</tr>
		</tfoot>
		<tbody>
			<tr>
				<td colspan="2">
					<h3>Datos del curso</h3>
				</td>
			</tr>
			<tr>
				<th><?php echo $form['categoria_curso_id']->renderLabel('Categoria del curso') ?>
				</th>
				<td><?php echo $form['categoria_curso_id']->renderError() ?> <?php echo $form['categoria_curso_id'] ?>
				</td>
			</tr>
			<tr>
				<th><?php echo $form['clave']->renderLabel() ?></th>
				<td><?php echo $form['clave']->renderError() ?> <?php echo $form['clave']->render(array('requerido'=>1, 'filtro'=>'alfanumerico', 'size'=>16, 'maxlength'=>18)) ?>
				</td>
			</tr>
			<tr>
				<th><?php echo $form['nombre']->renderLabel() ?></th>
				<td><?php echo $form['nombre']->renderError() ?> <?php echo $form['nombre']->render(array('nouppercase'=>1, 'requerido'=>1,'filtro'=>'alfanumerico', 'size'=>40, 'maxlength'=>255)) ?>
				</td>
			</tr>
			<tr>
				<th><?php echo $form['nombre_corto']->renderLabel('Nombre corto') ?>
				</th>
				<td><?php echo $form['nombre_corto']->renderError() ?> <?php echo $form['nombre_corto']->render(array('nouppercase'=>1, 'requerido'=>1,'filtro'=>'alfanumerico', 'size'=>40, 'maxlength'=>100)) ?>
				</td>
			</tr>
			<tr>
				<th><?php echo $form['descripcion']->renderLabel('Descripción') ?></th>
				<td><?php echo $form['descripcion']->renderError() ?> <?php echo $form['descripcion']->render(array('nouppercase'=>1, 'requerido'=>1, 'cols'=>38, 'rows'=>4)) ?>
				</td>
			</tr>
			<tr>
				<th><?php echo $form['perfil_id']->renderLabel('Perfil del facilitador') ?>
				</th>
				<td><?php echo $form['perfil_id']->renderError() ?> <?php echo $form['perfil_id'] ?>
				</td>
			</tr>
			<tr>
			
			
			<tr>
				<th><?php echo $form['duracion']->renderLabel('Duración total') ?></th>
				<td><?php echo $form['duracion']->renderError() ?> <?php echo $form['duracion']->render(array('requerido'=>1,'filtro'=>'numerico','size'=>3, 'maxlength'=>4)) ?>
					<b>Hrs.</b>
				</td>
			</tr>
			<tr>
				<th><?php echo $form['seriado']->renderLabel('Curso seriado (ruta de aprendizaje)') ?>
				</th>
				<td><?php echo $form['seriado']->renderError() ?> <?php echo $form['seriado'] ?>
				</td>
			</tr>
			<tr class="ruta-aprendizaje">
				<th colspan="2"><?php echo $form['curso_padre_list']->renderLabel('Cursos con los que tiene relacion') ?>
				</th>
			</tr>
			<tr class="ruta-aprendizaje">
				<td colspan="2"><?php echo $form['curso_padre_list']->renderError() ?>
					<?php echo $form['curso_padre_list'] ?>
				</td>
			</tr>
			<tr>
				<th><?php echo $form['asociado_moodle']->renderLabel('Asociado a Moodle') ?>
				</th>
				<td><?php echo $form['asociado_moodle']->renderError() ?> <?php echo $form['asociado_moodle'] ?>
				</td>
			</tr>
			<tr>
				<th><?php echo $form['asociado_rs']->renderLabel('Asociado a Rosetta Stone') ?>
				</th>
				<td><?php echo $form['asociado_rs']->renderError() ?> <?php echo $form['asociado_rs'] ?>
				</td>
			</tr>
			<tr>
				<th><?php echo $form['calificacion_minima']->renderLabel('Calificación mínima de ingreso') ?>
				</th>
				<td><?php echo $form['calificacion_minima']->renderError() ?> <?php echo $form['calificacion_minima']->render(array('filtro'=>'numerico','size'=>2, 'maxlength'=>2)) ?>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<h3>Restricciones por perfil de socio</h3>
				</td>
			</tr>
			<tr>
				<th><?php echo $form['rs']['edad_minima']->renderLabel('Edad mínima') ?>
				</th>
				<td><?php echo $form['rs']['edad_minima']->renderError() ?> <?php echo $form['rs']['edad_minima']->render(array('filtro'=>'numerico','size'=>2, 'maxlength'=>2)) ?>
					años</td>
			</tr>
			<tr>
				<th><?php echo $form['rs']['edad_maxima']->renderLabel('Edad máxima') ?>
				</th>
				<td><?php echo $form['rs']['edad_maxima']->renderError() ?> <?php echo $form['rs']['edad_maxima']->render(array('filtro'=>'numerico','size'=>2, 'maxlength'=>2)) ?>
					años</td>
			</tr>
			<tr>
				<th><?php echo $form['rs']['sexo']->renderLabel('Sexo') ?></th>
				<td><?php echo $form['rs']['sexo']->renderError() ?> <?php echo $form['rs']['sexo'] ?>
				</td>
			</tr>
			<tr>
				<th><?php echo $form['rs']['ocupacion_id']->renderLabel('Ocupación') ?>
				</th>
				<td><?php echo $form['rs']['ocupacion_id']->renderError() ?> <?php echo $form['rs']['ocupacion_id'] ?>
				</td>
			</tr>
			<tr>
				<th><?php echo $form['rs']['estudia']->renderLabel('Estudia') ?></th>
				<td><?php echo $form['rs']['estudia']->renderError() ?> <?php echo $form['rs']['estudia'] ?>
				</td>
			</tr>
			<tr>
				<th><?php echo $form['rs']['nivel_estudio_id']->renderLabel('Último nivel de estudios') ?>
				</th>
				<td><?php echo $form['rs']['nivel_estudio_id']->renderError() ?> <?php echo $form['rs']['nivel_estudio_id'] ?>
				</td>
			</tr>
			<tr>
				<th><?php echo $form['rs']['habilidad_informatica_id']->renderLabel('Habilidad informática') ?>
				</th>
				<td><?php echo $form['rs']['habilidad_informatica_id']->renderError() ?>
					<?php echo $form['rs']['habilidad_informatica_id'] ?>
				</td>
			</tr>
			<tr>
				<th><?php echo $form['rs']['dominio_ingles_id']->renderLabel('Dominio de inglés') ?>
				</th>
				<td><?php echo $form['rs']['dominio_ingles_id']->renderError() ?> <?php echo $form['rs']['dominio_ingles_id'] ?>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<h3>Restricciones de horario</h3>
				</td>
			</tr>
			<tr>
				<th><?php echo $form['rh']['hora_inicio']->renderLabel('Hora mínima de inicio') ?>
				</th>
				<td><?php echo $form['rh']['hora_inicio']->renderError() ?> <?php echo $form['rh']['hora_inicio'] ?>
				</td>
			</tr>
			<tr>
				<th><?php echo $form['rh']['hora_fin']->renderLabel('Hora mínima de fin') ?>
				</th>
				<td><?php echo $form['rh']['hora_fin']->renderError() ?> <?php echo $form['rh']['hora_fin'] ?>
				</td>
			</tr>
		</tbody>
	</table>
	<p class="botones_finales">
		<input type="button" value="Cancelar"
			onclick="location.href='<?php echo url_for('curso/index')?>'"> <input
			type="submit" value="Guardar" />
	</p>
</form>
