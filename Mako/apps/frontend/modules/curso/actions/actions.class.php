<?php

/**
 * curso actions.
 *
 * @package    mako
 * @subpackage curso
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php,v 1.18 2010/09/24 23:42:32 david Exp $
 */
class cursoActions extends sfActions
{
	public function executeIndex(sfWebRequest $request)
	{
		$this->detalle = CursoPeer::doSelect(new Criteria());

		$this->form = new cursoForm();
	}

	public function executeCreate(sfWebRequest $request)
	{
		$this->forward404Unless($request->isMethod(sfRequest::POST));

		$this->form = new cursoForm();

		$this->processForm($request, $this->form);
		 
		$this->detalle = CursoPeer::doSelect(new Criteria());

		$this->setTemplate('index');
	}

	public function executeEdit(sfWebRequest $request)
	{
		$this->forward404Unless($curso = CursoPeer::retrieveByPk($request->getParameter('id')), sprintf('El curso no existe (%s).', $request->getParameter('id')));

		$this->form = new cursoForm($curso);

		$this->detalle = CursoPeer::doSelect(new Criteria());

		$this->setTemplate('index');
	}

	public function executeUpdate(sfWebRequest $request)
	{
		$this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
		$this->forward404Unless($curso = CursoPeer::retrieveByPk($request->getParameter('id')), sprintf('El curso no existe (%s).', $request->getParameter('id')));

		$this->form = new cursoForm($curso);

		$this->processForm($request, $this->form);

		$this->detalle = CursoPeer::doSelect(new Criteria());

		$this->setTemplate('index');
	}

	public function executeDelete(sfWebRequest $request)
	{
		$request->checkCSRFProtection();

		$this->forward404Unless($curso = CursoPeer::retrieveByPk($request->getParameter('id')), sprintf('El curos no existe (%s).', $request->getParameter('id')));
		foreach ($curso->getProductoCursos() as $productoCurso)
		{
			$producto = ProductoPeer::retrieveByPK($productoCurso->getProducto()->getId());
			$productoCurso->delete();
		}
		$this->getUser()->setFlash('notice', sprintf('Curso %s borrado', $curso->getNombre()));
		$curso->delete();

		$this->redirect('curso/index');
	}

	/**
	 * Ejecuta la acción para desactivar un registro
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeEnabled(sfWebRequest $request)
	{
		$this->forward404Unless($curso = CursoPeer::retrieveByPk($request->getParameter('id')), sprintf('El curso no existe (%s).', $request->getParameter('id')));
		$curso->setActivo(true);
		$curso->save();
		$this->getUser()->setFlash('notice', 'Curso "'.$curso->getNombre().'" activado');

		$this->redirect('curso/index');
	}

	/**
	 * Ejecuta la acción para desactivar un registro
	 *
	 * @param sfRequest $request A request object
	 */
	public function executeDisabled(sfWebRequest $request)
	{
		$this->forward404Unless($curso = CursoPeer::retrieveByPk($request->getParameter('id')), sprintf('El curso no existe (%s).', $request->getParameter('id')));
		$curso->setActivo(false);
		$curso->save();
		$this->getUser()->setFlash('notice', 'Curso "'.$curso->getNombre().'" desactivado');

		$this->redirect('curso/index');
	}

	protected function processForm(sfWebRequest $request, sfForm $form)
	{
		$form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
		if ($form->isValid())
		{
			$flag = false;
			if($form->getObject()->isNew())
			{
				$flag = true;
			}
			$form->getObject()->setOperadorId($this->getUser()->getAttribute('usuario')->getId());
			$curso = $form->save();
			//Si el curso esta asociado a moodel se publica
			if($curso->getAsociadoMoodle())
			{
				$asociado = CursoPeer::publicarMoodle($curso->getId());
				if(!$flag)
				{
					if(CursoPeer::esCursoPublicado($curso->getId()))
					{
						$this->dispatcher->notify(new sfEvent($this, 'curso.modificado', array( 'curso' => $curso )));
						$mensaje = 'Curso '.$curso->getNombre().' editado. '.$asociado;
					}
					//Disparamos evento de curso actualizado
					$this->dispatcher->notify(new sfEvent($this, 'curso.actualizado', array( 'curso' => $curso )));
				}
				else
				{
					//Disparamos evento de cursos creado
					$this->dispatcher->notify(new sfEvent($this, 'curso.creado', array( 'curso' => $curso )));
				}

				if(!CursoPeer::esCursoPublicado($curso->getId()))
				{
					CursoPeer::publicaCurso($curso->getId());
					$this->dispatcher->notify(new sfEvent($this, 'curso.publicado', array( 'curso' => $curso )));
					$mensaje = 'Curso '.$curso->getNombre().' creado. '.$asociado;
				}
			}
			else
			{
				if($flag)
				{
					//Disparamos evento de cursos creado
					$this->dispatcher->notify(new sfEvent($this, 'curso.creado', array( 'curso' => $curso )));
					$mensaje = 'Curso '.$curso->getNombre().' creado';
				}
				else
				{
					$mensaje = 'Curso '.$curso->getNombre().' editado';
					if(CursoPeer::esCursoPublicado($curso->getId()))
					{
						error_log('Se modifico un curso');
						$this->dispatcher->notify(new sfEvent($this, 'curso.modificado', array( 'curso' => $curso )));
					}
					//Disparamos evento de curso actualizado
					$this->dispatcher->notify(new sfEvent($this, 'curso.actualizado', array( 'curso' => $curso )));
				}
			}
			$this->getUser()->setFlash('notice', $mensaje);
			$this->redirect('curso/index');
		}
	}

}
