<?php

/**
 * admin_pc actions.
 *
 * @package    mako
 * @subpackage admin_pc
 * @author     Bixit SA de CV
 * @version    SVN: $Id: actions.class.php,v 1.2 2011/01/13 03:47:28 eorozco Exp $
 */
class admin_pcActions extends sfActions
{
	public function executeIndex(sfWebRequest $request)
	{
		$c = new Criteria();
		$centro_id = sfConfig::get('app_centro_actual_id');
		if($centro_id != sfConfig::get('app_central_id'))
		{
			$c->add(ComputadoraPeer::CENTRO_ID,$centro_id);
		}
		 
		$this->computadoras = ComputadoraPeer::doSelect($c);
	}

	public function executeNew(sfWebRequest $request)
	{
		$this->form = new computadoraForm();
	}

	public function executeCreate(sfWebRequest $request)
	{
		$this->forward404Unless($request->isMethod(sfRequest::POST));

		$this->form = new computadoraForm();

		$this->processForm($request, $this->form);

		$this->setTemplate('new');
	}

	public function executeEdit(sfWebRequest $request)
	{
		$this->forward404Unless($computadora = ComputadoraPeer::retrieveByPk($request->getParameter('id')), sprintf('Object computadora does not exist (%s).', $request->getParameter('id')));
		$this->form = new computadoraForm($computadora);
	}

	public function executeUpdate(sfWebRequest $request)
	{
		$this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
		$this->forward404Unless($computadora = ComputadoraPeer::retrieveByPk($request->getParameter('id')), sprintf('Object computadora does not exist (%s).', $request->getParameter('id')));
		$this->form = new computadoraForm($computadora);

		$this->processForm($request, $this->form);

		$this->setTemplate('edit');
	}

	public function executeDelete(sfWebRequest $request)
	{
		$request->checkCSRFProtection();

		$this->forward404Unless($computadora = ComputadoraPeer::retrieveByPk($request->getParameter('id')), sprintf('Object computadora does not exist (%s).', $request->getParameter('id')));
		$computadora->delete();

		$this->redirect('admin_pc/index');
	}

	protected function processForm(sfWebRequest $request, sfForm $form)
	{
		$form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
		if ($form->isValid())
		{
			$computadora = $form->save();


	  $this->getUser()->setFlash('ok', sprintf('Se ha guardado correctamente el registro: %s',$form->getObject()));



	  $this->redirect('admin_pc/index?id='.$computadora->getId());
		}
	}
}
