<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>


<div class="ui-widget ui-corner-all sombra">
	<div class="ui-widget-header ui-corner-top tsss" style="padding: 5px;">Administrar
		computadoras</div>
	<div class="ui-widget-content ui-corner-bottom">

		<div style="margin: 15px;">


			<form
				action="<?php echo url_for('admin_pc/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>"
				method="post"
				<?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
				<?php if (!$form->getObject()->isNew()): ?>
				<input type="hidden" name="sf_method" value="put" />
				<?php endif; ?>
				<table>
					<tfoot>
						<tr>
							<td colspan="2"><?php echo $form->renderHiddenFields(false) ?>
								&nbsp;<a href="<?php echo url_for('admin_pc/index') ?>">Regresar
									al índice</a> <?php if (!$form->getObject()->isNew()): ?>
								&nbsp;<?php echo link_to('Borrar registro', 'admin_pc/delete?id='.$form->getObject()->getId(), array('method' => 'delete', 'confirm' => '¿Está seguro? Esta acción no se puede deshacer.')) ?>
								<?php endif; ?> <input type="submit" value="Guardar registro" />
								<input type="button" value="Cancelar"
								onclick="location.href='<?php echo url_for('admin_pc/new') ?>'" />
							</td>
						</tr>
					</tfoot>
					<tbody>
						<?php echo $form->renderGlobalErrors() ?>
						<tr>
							<th><?php echo $form['centro_id']->renderLabel() ?></th>
							<td><?php echo $form['centro_id']->renderError() ?> <?php echo $form['centro_id'] ?>
							</td>
						</tr>
						<tr>
							<th><?php echo $form['seccion_id']->renderLabel() ?></th>
							<td><?php echo $form['seccion_id']->renderError() ?> <?php echo $form['seccion_id'] ?>
							</td>
						</tr>
						<tr>
							<th><?php echo $form['ip']->renderLabel() ?></th>
							<td><?php echo $form['ip']->renderError() ?> <?php echo $form['ip'] ?>
							</td>
						</tr>
						<tr>
							<th><?php echo $form['hostname']->renderLabel() ?></th>
							<td><?php echo $form['hostname']->renderError() ?> <?php echo $form['hostname'] ?>
							</td>
						</tr>
						<tr>
							<th><?php echo $form['mac_adress']->renderLabel() ?></th>
							<td><?php echo $form['mac_adress']->renderError() ?> <?php echo $form['mac_adress'] ?>
							</td>
						</tr>
						<tr>
							<th><?php echo $form['alias']->renderLabel() ?></th>
							<td><?php echo $form['alias']->renderError() ?> <?php echo $form['alias'] ?>
							</td>
						</tr>
						<tr>
							<th><?php echo $form['tipo_id']->renderLabel() ?></th>
							<td><?php echo $form['tipo_id']->renderError() ?> <?php echo $form['tipo_id'] ?>
							</td>
						</tr>
						<tr>
							<th><?php echo $form['activo']->renderLabel() ?></th>
							<td><?php echo $form['activo']->renderError() ?> <?php echo $form['activo'] ?>
							</td>
						</tr>
					</tbody>
				</table>
			</form>


		</div>
	</div>
</div>
