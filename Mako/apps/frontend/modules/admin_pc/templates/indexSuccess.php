<?php use_javascript('plugins/jquery.dataTables.js') ?>
<?php use_stylesheet('datatables.css') ?>

<script>

var pTl;
$(document).ready(function() {

   pTl = $('#lista-computadoras').dataTable({
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": true,
        "bJQueryUI": true,
        "sPaginationType": "full_numbers",
        "aaSorting": [[3, 'asc']]  
     });
   
});

</script>

<div class="ui-widget-header ui-corner-top tsss" style="padding: 5px;">Administrador
	de computadoras</div>
<div class="ui-widget ui-corner-bottom sombra">
	<div class="ui-widget-content ui-corner-bottom" style="padding: 5px;">

		<?php if ($sf_user->hasFlash('ok')): ?>
		<div
			class="flash_ok ui-state-highlight ts sombra-delgada ui-corner-all"
			style="padding: 5px; margin-top: 10px; margin-bottom: 10px;">
			<span style="float: left; margin-right: 0.3em;"
				class="ui-icon ui-icon-info"></span> <strong><?php echo $sf_user->getFlash('ok') ?>
			</strong>
		</div>
		<?php endif; ?>

		<br /> <span style="float: left;" class="ui-icon ui-icon-circle-plus"></span>
		<a href="<?php echo url_for('admin_pc/new') ?>"> Agregar registro </a>
		<br /> <br />


		<table id="lista-computadoras" style="width: 100%;">
			<thead>
				<tr>
					<th><span class="ui-icon  ui-icon-pencil"></span></th>
					<th>Centro</th>
					<th>Sección</th>
					<th>IP</th>
					<th>Hostname</th>
					<th>Mac adress</th>
					<th>Alias</th>
					<th>Tipo</th>
					<th>Activo</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($computadoras as $computadora): ?>
				<tr>
					<td><a
						href="<?php echo url_for('admin_pc/edit?id='.$computadora->getId()) ?>">
							<span class="ui-icon  ui-icon-pencil"></span>
					</a>
					</td>
					<td><?php echo $computadora->getCentro() ?></td>
					<td><?php echo $computadora->getSeccion()->getNombre() ?></td>
					<td><?php echo $computadora->getIp() ?></td>
					<td><?php echo $computadora->getHostname() ?></td>
					<td><?php echo $computadora->getMacAdress() ?></td>
					<td><?php echo $computadora->getAlias() ?></td>
					<td><?php echo $computadora->getTipoPc() ?></td>
					<td><?php echo ($computadora->getActivo())?'SI':'NO' ?></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>


	</div>
</div>

