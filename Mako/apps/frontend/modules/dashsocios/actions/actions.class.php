<?php
/**
 * dashsocios actions.
 *
 * @package    mako
 * @subpackage dashsocios
 * @author     Bixit SA de CV
 * @version    SVN: $Id: actions.class.php,v 1.9 2012-01-24 02:16:19 eorozco Exp $
 */
class dashsociosActions extends sfActions
{

    public function executeIndex(sfWebRequest $request)
    {
        $this->socio = $this->getUser()->getAttribute('usuario');
        $this->Socio = $this->socio;
        if ($this->socio == null) {
            $this->traeSesionSocio();
            $this->socio     = $this->getUser()->getAttribute('usuario');
            $this->Socio     = $this->socio;
        }

        // PARA EJECUTAR PRUEBAS LOCALES @@ LGL
        //$this->socio =  SocioPeer::retrieveByPK(128812980693491);
        //$this->Socio = $this->socio;

        //Revisamos si hay aceptación de términos: (solo si el socio ha sido dado de alta en el centro)
        $centro_id_socio = $this->socio->getCentroId();
        $centro_id       = sfConfig::get('app_centro_actual_id');
        $at              = AceptacionTerminos2016Peer::validaAceptacion($this->socio->getId());

        if (is_null($at) && $centro_id_socio == $centro_id) {
            $this->setTemplate('politicas');

        } else {
            //Esto para mitigar la solicitud de mois ticket mantis (5143)

            //Este para aplicar el ticket mantis (15885)
            $c            = CentroPeer::retrieveByPK(sfConfig::get('app_centro_actual_id'));
            $this->centro = strtolower($c->getAlias());

            $this->setTemplate('misdatos');
        }
    }

    public function executeEducativo(sfWebRequest $request)
    {
        $this->socio = $this->getUser()->getAttribute('usuario');
        if ($this->$socio == null) {
            $this->traeSesionSocio();
            $this->socio  = $this->getUser()->getAttribute('usuario');
        }

        $c            = CentroPeer::retrieveByPK(sfConfig::get('app_centro_actual_id'));
        $this->centro = strtolower($c->getAlias());
    }

    public function executeLibre(sfWebRequest $request)
    {
        $this->socio  = $this->getUser()->getAttribute('usuario');
        if ($this->$socio == null) {
            $this->traeSesionSocio();
            $this->socio = $this->getUser()->getAttribute('usuario');
        }
    }

    public function executeMicuenta(sfWebRequest $request)
    {
        $this->socio = $this->getUser()->getAttribute('usuario');
        if ($this->$socio == null) {
            $this->traeSesionSocio();
            $this->socio = $this->getUser()->getAttribute('usuario');
        }
    }

    public function executeMisdatos(sfWebRequest $request)
    {
        $this->Socio = $this->getUser()->getAttribute('usuario');
        if ($this->$Socio == null) {
            $this->traeSesionSocio();
            $this->Socio = $this->getUser()->getAttribute('usuario');
        }
    }

    public function executeHistorial(sfWebRequest $request)
    {
        $this->Socio = $this->getUser()->getAttribute('usuario');
        if ($this->$Socio == null) {
            $this->traeSesionSocio();
            $this->Socio       = $this->getUser()->getAttribute('usuario');
        }

        $this->Ordenes     = OrdenPeer::historialComprasSocios($this->Socio->getId());
        $this->Sesiones    = SesionHistoricoPeer::historialSesionesSocios($this->Socio->getId());
        //$this->Cursos = GrupoAlumnosPeer::cursosTomadosDetalle($this->Socio->getId());
        $this->Cobranza    = SocioPagosPeer::historialCobranza($this->Socio->getId());
    }

    public function executeAceptacion(sfWebRequest $request)
    {
        $socio             = $this->getUser()->getAttribute('usuario');
        //$socio =  SocioPeer::retrieveByPK(128812980693491);
        $this->primera_vez = true;
        try {
            $ap                = new AceptacionTerminos2016();
            $ap->setCentroId(sfConfig::get('app_centro_actual_id'));
            $ap->setIp(Comun::ipReal());
            $ap->setSocio($socio);
            $ap->save();
        }
        catch(Exception $e) {
            error_log("Error[dashSocio/aceptacion]: " . $e->getMessage());
        }
        $this->form = new SocioClaveForm($socio);
        $this->setTemplate('cambiarClave');
    }

    public function executeCambiarClave(sfWebRequest $request)
    {
        $socio      = $this->getUser()->getAttribute('usuario');
        $this->form = new SocioClaveForm($socio);
    }

    public function executeCambiarcv(sfWebRequest $request)
    {

        $aPp        = $request->getPostParameters();
        $id         = pg_escape_string($aPp['socio']['id']);
        $this->forward404Unless($request->isMethod(sfRequest::POST));
        $this->forward404Unless($Socio      = SocioPeer::retrieveByPk($id), sprintf('El socio no existe dice (%s).', $id));
        $this->form = new SocioClaveForm($Socio);

        $this->processForm($request, $this->form);
        $this->primera_vez = false;
        $this->setTemplate('cambiarClave');
    }

    protected function processForm(sfWebRequest $request, sfForm $form)
    {
        $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
        if ($form->isValid()) {

            $Socio   = $form->save();
            $mensaje = "Se ha modificado la clave con éxito";
            $this->getUser()->setFlash('notice', $mensaje);

            sfProjectConfiguration::getActive()->getEventDispatcher()->notify(new sfEvent($Socio, 'socio.cambioclave', array('socio' => $Socio, 'modificaUsername' => false, 'usuario_anterior' => null)));

            $this->redirect('dashsocios/index');
        }
    }

    public function executeSsoMoodle(sfWebRequest $request)
    {

        if ($this->$Socio == null) {
            $this->traeSesionSocio();
            $this->Socio = $this->getUser()->getAttribute('usuario');

            if (!method_exists($this->Socio, 'getLdapUid')) {
                error_log("No hay metodo getLdapUid. Es staff? redirect a moodle");
                $this->redirect(sfConfig::get('app_moodle'));
            }
        }

        $ldap_uid = $this->Socio->getLdapUid();
        error_log("Datos para SSO: " . $this->Socio . ": ldapuid:" . $ldap_uid);
        if (intval($ldap_uid) == 0) {
            $this->redirect(sfConfig::get('app_moodle'));
        }
        $res = OperacionMoodle::sso($ldap_uid);
        error_log("Respuesta serv SSO moodle." . $this->Socio . ": " . print_r($res, true));
        if ($res->status == 'ACK') {
            $this->redirect(sfConfig::get('app_moodle') . '/course/view.php?id=1&wssid=' . $res->info);
            //$this->redirect(sfConfig::get('app_moodle'));

        } else {
            $this->redirect(sfConfig::get('app_moodle'));
        }
    }

    /**
     * Liga dinamica para acceso estatico a brainpop
     * Esto debido a una solicitud de borrar los accesos anteriores del dashboard y la necesidad de tener
     * en el browser o en el desktop accesos directos en todo momento
     * ver mantis 15885 y
     * @param $request
     */
    public function executeBrainPopEsp(sfWebRequest $request)
    {
        $centro       = CentroPeer::retrieveByPK(sfConfig::get('app_centro_actual_id'));
        if ($centro != null) {

            $url_brainpop = "http://esp.brainpop.com/user/loginDo.weml?user=ria-" . strtolower($centro->getAlias()) . "&password=ria";
            $this->redirect($url_brainpop);
        } else {
            $url_brainpop = "http://esp.brainpop.com/user/loginDo.weml?user=ria-&password=ria";
            $this->redirect($url_brainpop);
        }
    }

    public function executeBrainPopIng(sfWebRequest $request)
    {
        $centro       = CentroPeer::retrieveByPK(sfConfig::get('app_centro_actual_id'));
        if ($centro != null) {

            $url_brainpop = "http://www.brainpopesl.com/user/loginDo.weml?user=ria-" . strtolower($centro->getAlias()) . "&password=ria";
            $this->redirect($url_brainpop);
        } else {
            $url_brainpop = "http://www.brainpopesl.com/user/loginDo.weml?user=ria-&password=ria";
            $this->redirect($url_brainpop);
        }
    }

    private function traeSesionSocio()
    {
        $ip       = Comun::ipReal();
        $criteria = new Criteria();
        $criteria->add(SesionPeer::IP, $ip);
        $sesion = SesionPeer::doSelectOne($criteria);

        if ($sesion != null) {
            $socio  = $sesion->getSocio();
            $this->getUser()->setAuthenticated(true);
            $this->getUser()->setAttribute('usuario', $socio);
        }
    }
}
