<?php use_javascript('jquery.ui.js') ?>
<?php use_javascript('plugins/jquery.dataTables.js') ?>
<?php use_stylesheet('datatables.css') ?>
<?php use_javascript('plugins/date.js') ?>
<?php use_javascript('plugins/dataTables/datatables.orden-fecha.js') ?>
<?php use_javascript('plugins/dataTables/datatables.orden-decimal.js') ?>

<div align="center">
	<img src="/imgs/dashboard/menu_mi-cuenta.png" alt="" width="503"
		height="126" border="0" usemap="#Map" />
	<map name="Map" id="Map">
		<area shape="circle" coords="111,71,52"
			href="<?php echo url_for('dashsocios/misdatos') ?>" />
		<area shape="circle" coords="253,69,56"
			href="<?php echo url_for('dashsocios/cambiarClave') ?>" />
		<area shape="circle" coords="390,68,52"
			href="<?php echo url_for('dashsocios/historial') ?>" />
	</map>
</div>
<br>

<script>

	$(function() {
		var $tabsvd = $('#tabsVerDetalle').tabs();

		$("#tabsVerDetalle").tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
		$("#tabsVerDetalle li").removeClass('ui-corner-top').addClass('ui-corner-left');

	});


	var oTable;
	$(document).ready(function() {
	
	   /*oTable = $('.registros').dataTable({
			"bPaginate": true,
			"bLengthChange": true,
			"bFilter": true,
			"bSort": true,
			"bInfo": true,
			"bAutoWidth": false,
	        "bJQueryUI": true,
	        "sPaginationType": "full_numbers"
	     });
	     */
	   oTCompras = $('#registrosCompras').dataTable({
			"bPaginate": true,
			"bLengthChange": true,
			"bFilter": true,
			"bSort": true,
			"bInfo": true,
			"bAutoWidth": false,
	        "bJQueryUI": true,
	        "aaSorting": [],
			"aoColumns":[{ "sType": "decimal" },null,null,null,{ "sType": "fecha_dma" } ,{ "sType": "decimal" },null,{ "sType": "decimal" },{ "sType": "decimal" },{ "sType": "decimal" }],
	        "sPaginationType": "full_numbers"
	     });

	   oTSesiones = $('#registrosSesiones').dataTable({
			"bPaginate": true,
			"bLengthChange": true,
			"bFilter": true,
			"bSort": true,
			"bInfo": true,
			"bAutoWidth": false,
	        "bJQueryUI": true,
	        "aaSorting": [],
			"aoColumns":[{ "sType": "timestamp_dma" } ,{ "sType": "timestamp_dma" },null,null,null,null],
	        "sPaginationType": "full_numbers"
	     });

	   oTCob = $('#registrosCobranza').dataTable({
			"bPaginate": true,
			"bLengthChange": true,
			"bFilter": true,
			"bSort": true,
			"bInfo": true,
			"bAutoWidth": false,
	        "bJQueryUI": true,
	        "aaSorting": [],
			"aoColumns":[null,null,null,null,{ "sType": "fecha_dma" } ,{ "sType": "fecha_dma" },null,null],
	        "sPaginationType": "full_numbers"
	     });
	   
	});
</script>


<?php use_stylesheet('./dashsocios/historialSuccess.css')?>


<div id="contenedorVerDetalle">
	<div id="tabsVerDetalle">

		<ul id="tabMenu-vd">
			<li><a href="#historial_cobranza-vd">Historial de pagos</a></li>
			<li><a href="#historial_compras-vd">Historial de compras</a></li>
			<li><a href="#historial_sesiones-vd">Historial de sesiones</a></li>
		</ul>


		<div id="historial_compras-vd">
			<h3>Historial de compras</h3>
			<table width="100%" border="0" id="registrosCompras"
				class="registros">
				<thead>
					<tr>
						<th>Folio</th>
						<th>Tipo de orden</th>
						<th>Comprobante</th>
						<th>Producto</th>
						<th>Fecha</th>
						<th>Cantidad</th>
						<th>Unidad</th>
						<th>Precio <br> Unitario
						</th>
						<th>Descuento</th>
						<th>Total</th>
					</tr>
				</thead>
				<?php foreach($Ordenes as $orden)
				{
					foreach($orden->getDetalleOrdens() as $item)
		{?>
				<!-- BEGIN carrito -->
				<tr>
					<td><?php echo sprintf("%010s\n", $orden->getFolio()); ?></td>
					<td><?php echo $item->getOrden()->getTipoOrden()->getNombre() ?></td>
					<td><?php echo ($orden->getEsFactura()=='1') ? 'Factura':'Ticket'  ?>
					</td>
					<td><?php echo $item->getProducto()->getNombre() ?></td>
					<td><?php echo $orden->getCreatedAt("%d-%m-%Y") ?></td>
					<td align="right"><?php echo  number_format($item->getCantidad(), 0); ?>
					</td>
					<td><?php echo $item->getProducto()->getUnidadProducto()->getNombre()?>
					</td>
					<td align="right">$ <?php echo $item->getPrecioLista() ?>
					</td>
					<td align="right"><?php  echo  number_format($item->getDescuento(), 2); ?>
						%</td>
					<td align="right" nowrap="nowrap">$ <?php echo $item->getSubtotal() ?>
					</td>
				</tr>
				<!-- END carrito -->

				<?php } 
} ?>
			</table>
		</div>

		<div id="historial_sesiones-vd">
			<h3>Historial de sesiones</h3>
			<table width="100%" border="0" id="registrosSesiones"
				class="registros">
				<thead>
					<tr>
						<th>Fecha Inicio</th>
						<th>Fecha Fin</th>
						<th>Ocupado</th>
						<th>Centro</th>
						<th>Seccion</th>
						<th>PC</th>
					</tr>
				</thead>
				<?php foreach($Sesiones as $sesion): //$sesion = new SesionHistorico();?>
				<!-- BEGIN ocupacion -->

				<tr>
					<td><?php echo $sesion->getFechaLogin("%d-%m-%Y %H:%M") ?></td>
					<td><?php echo $sesion->getFechaLogout("%d-%m-%Y %H:%M") ?>
					</td>
					<td><?php echo Comun::segsATxt($sesion->getOcupados())?></td>
					<td><?php echo $sesion->getCentro()->getNombre()?></td>
					<td><?php echo $sesion->getComputadora()->getSeccion() ?></td>
					<td><?php echo substr($sesion->getComputadora(),-2) ?></td>
				</tr>
				<!-- END ocupacion -->
				<?php endforeach; ?>
			</table>
		</div>

		<div id="historial_cobranza-vd">
			<h3>Historial de pagos</h3>
			<table width="100%" border="0" id="registrosCobranza"
				class="registros">
				<thead>
					<tr>
						<th>Centro</th>
						<th>Modalidad</th>
						<th>Producto</th>
						<th>Núm pago</th>
						<th>Fecha a pagar</th>
						<th>Fecha pago</th>
						<th>Pagado</th>
						<th>Acuerdo de pago</th>
					</tr>
				</thead>
				<?php foreach($Cobranza as $pago): //$pago = new SocioPagos();?>
				<!-- BEGIN cobranza -->

				<tr>
					<td><?php echo $pago->getCentro() ?></td>
					<td><?php echo $pago->getTipoCobro() ?>
					</td>
					<td><?php echo $pago->getProductoRelatedByProductoId()?></td>
					<td align="right"><?php echo $pago->getNumeroPago() ?></td>
					<td align="center"><?php echo $pago->getFechaAPagar("%d-%m-%Y") ?>
					</td>
					<td align="center"><?php echo $pago->getFechaPagado("%d-%m-%Y") ?>
					</td>
					<td align="center"><?php echo $pago->getPagado() ? 'SI':'NO' ?></td>
					<td align="center"><?php echo $pago->getAcuerdoPago() ? 'SI':'NO' ?>
					</td>
				</tr>
				<!-- END cobranza -->
				<?php endforeach; ?>
			</table>


		</div>


	</div>
</div>
