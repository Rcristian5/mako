<?php use_javascript('jquery.ui.js') ?>
<?php use_javascript('plugins/jquery.dataTables.js') ?>
<?php use_stylesheet('datatables.css') ?>

<div align="center">
    <img src="/imgs/dashboard/menu_mi-cuenta.png" alt="" width="503"
        height="126" border="0" usemap="#Map" />
    <map name="Map" id="Map">
        <area shape="circle"
              coords="111,71,52"
              href="<?php echo url_for('dashsocios/misdatos') ?>" />
        <area shape="circle"
              coords="253,69,56"
              href="<?php echo url_for('dashsocios/cambiarClave') ?>" />
        <area shape="circle"
              coords="390,68,52"
              href="<?php echo url_for('dashsocios/historial') ?>" />
    </map>
</div>
<br>


<script>

    $(function() {
        var $tabsvd = $('#tabsVerDetalle').tabs();

        $("#tabsVerDetalle").tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
        $("#tabsVerDetalle li").removeClass('ui-corner-top').addClass('ui-corner-left');

    });


    var oTable;
    $(document).ready(function() {

       oTable = $('.registros').dataTable({
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false,
            "bJQueryUI": true,
            "sPaginationType": "full_numbers"
         });

    });
</script>


<?php use_stylesheet('./dashsocios/misdatosSuccess.css')?>

<div id="contenedorVerDetalle">
    <div id="tabsVerDetalle">

        <ul id="tabMenu-vd">
            <li><a href="#personales-vd">Datos personales</a></li>
            <li><a href="#direccion-vd">Dirección</a></li>
            <?php if($Socio->getRfc() != ''): ?>
                <li><a href="#datosFiscales-vd">Datos fiscales</a></li>
            <?php endif; ?>
            <li><a href="#escolaridad-vd">Escolaridad</a></li>
            <li><a href="#ocupacion-vd">Ocupacion</a></li>
            <li><a href="#acercamiento-vd">Medio de acercamiento</a></li>
        </ul>

        <div id="personales-vd" style="width: 80%;">
            <h3>
                <?php echo $Socio->getNombre().' '.$Socio->getApepat().' '.$Socio->getApemat() ?>
            </h3>
            <div class="clearfix">
                <div class="container fotoGrande" style="float: left;">
                    <img src="<?php echo ($Socio->getFoto() != '')? url_for('/fotos/'.$Socio->getFoto()) : url_for('/imgs/sin_foto_sh.jpg') ?>"
                         class="main fotoGrande" />
                    <img class="minor fotoGrande"
                         src="/imgs/frame.png">
                </div>

                <table width="" border="0" class="dataTables_wrapper">
                    <tr class="odd">
                        <td width="30%" class="sorting_1">Folio:</td>
                        <td>
                            <?php echo $Socio->getFolio() ?>
                        </td>
                    </tr>

                    <tr class="even">
                        <td class="sorting_1">Usuario:</td>
                        <td>
                            <?php echo $Socio->getUsuario() ?>
                        </td>
                    </tr>

                    <tr class="odd">
                        <td class="sorting_1">Edad:</td>
                        <td>
                            <?php echo $Socio->getEdad() ?> años
                        </td>
                    </tr>

                    <tr class="even">
                        <td class="sorting_1">Sexo:</td>
                        <td>
                            <?php echo ($Socio->getSexo()=='M') ? 'Masculino':'Femenino' ?>
                        </td>
                    </tr>

                    <tr class="odd">
                        <td class="sorting_1">Fecha Nac.:</td>
                        <td>
                            <?php echo $Socio->getFechaNac("%d-%m-%Y") ?>
                        </td>
                    </tr>

                    <tr class="even">
                        <td class="sorting_1">Centro alta:</td>
                        <td>
                            <?php echo $Socio->getCentro()->getNombre() ?>
                        </td>
                    </tr>

                    <tr class="odd">
                        <td class="sorting_1">Rol:</td>
                        <td>
                            <?php echo $Socio->getRol() ?>
                        </td>
                    </tr>

                    <tr class="even">
                        <td class="sorting_1">Email:</td>
                        <td>
                            <?php echo $Socio->getEmail() ?>
                        </td>
                    </tr>

                    <tr class="odd">
                        <td class="sorting_1">Teléfono casa:</td>
                        <td>
                            <?php echo $Socio->getTel() ?>
                        </td>
                    </tr>

                    <tr class="even">
                        <td class="sorting_1">Celular:</td>
                        <td>
                            <?php echo $Socio->getCelular() ?>
                        </td>
                    </tr>

                    <tr class="even">
                        <td class="sorting_1">Tiempo PC:</td>
                        <td>
                            <?php echo Comun::segsATxt($Socio->getSaldo())   ?>
                        </td>
                    </tr>

                    <tr class="odd">
                        <td class="sorting_1">Vigente:</td>
                        <td>
                            <?php echo ($Socio->getVigente()=='1') ? 'Si':'No' ?>
                        </td>
                    </tr>

                    <tr class="even">
                        <td class="sorting_1">Operador:</td>
                        <td>
                            <?php echo $Socio->getUsuarioRelatedByOperadorId() ?>
                        </td>
                    </tr>

                    <tr class="odd">
                        <td class="sorting_1">Activo:</td>
                        <td>
                            <?php echo ($Socio->getActivo()=='1') ? 'Si':'No' ?>
                        </td>
                    </tr>

                    <tr class="even">
                        <td class="sorting_1">Fecha alta:</td>
                        <td>
                            <?php echo $Socio->getCreatedAt("%d-%m-%Y") ?>
                        </td>
                    </tr>

                    <tr class="odd">
                        <td class="sorting_1">Fecha ultima modificación:</td>
                        <td>
                            <?php echo $Socio->getUpdatedAt("%d-%m-%Y") ?>
                        </td>
                    </tr>
                </table>

            </div>
        </div>

        <div id="direccion-vd">

            <h3>Dirección</h3>
            <table width="" border="0" class="dataTables_wrapper">

                <tr class="odd">
                    <td width="40%" class="sorting_1">Estado:</td>
                    <td>
                        <?php echo $Socio->getEstado() ?>
                    </td>
                </tr>

                <tr class="even">
                    <td class="sorting_1">Municipio:</td>
                    <td>
                        <?php echo $Socio->getMunicipio() ?>
                    </td>
                </tr>

                <tr class="odd">
                    <td width="30%" class="sorting_1">Colonia:</td>
                    <td>
                        <?php echo $Socio->getColonia() ?>
                    </td>
                </tr>

                <tr class="even">
                    <td width="30%" class="sorting_1">CP:</td>
                    <td>
                        <?php echo $Socio->getCp() ?>
                    </td>
                </tr>

                <tr class="odd">
                    <td class="sorting_1">Calle:</td>
                    <td>
                        <?php echo $Socio->getCalle() ?>
                    </td>
                </tr>

                <tr class="even">
                    <td width="30%" class="sorting_1">Núm ext.:</td>
                    <td>
                        <?php echo $Socio->getNumExt() ?>
                    </td>
                </tr>

                <tr class="odd">
                    <td class="sorting_1">Núm int.:</td>
                    <td>
                        <?php echo $Socio->getNumInt() ?>
                    </td>
                </tr>

            </table>
        </div>

        <?php if($Socio->getRfc() != ''): ?>
        <div id="datosFiscales-vd">
            <h3>Datos Fiscales</h3>

            <table width="" border="0" class="dataTables_wrapper">

                <tr class="odd">
                    <td width="40%" class="sorting_1">Razon social:</td>
                    <td>
                        <?php echo $Socio->getRazonSocial() ?>
                    </td>
                </tr>

                <tr class="even">
                    <td class="sorting_1">RFC:</td>
                    <td>
                        <?php echo $Socio->getRfc() ?>
                    </td>
                </tr>

                <tr class="odd">
                    <td class="sorting_1">Calle:</td>
                    <td>
                        <?php echo $Socio->getCalleFiscal() ?>
                    </td>
                </tr>

                <tr class="even">
                    <td class="sorting_1">Colonia:</td>
                    <td>
                        <?php echo $Socio->getColoniaFiscal() ?>
                    </td>
                </tr>

                <tr class="odd">
                    <td class="sorting_1">Estado:</td>
                    <td>
                        <?php echo $Socio->getEstadoFiscal() ?>
                    </td>
                </tr>

                <tr class="even">
                    <td class="sorting_1">Municipio:</td>
                    <td>
                        <?php echo $Socio->getMunicipioFiscal() ?>
                    </td>
                </tr>

                <tr class="odd">
                    <td class="sorting_1">Núm ext.:</td>
                    <td>
                        <?php echo $Socio->getNumExtFiscal() ?>
                    </td>
                </tr>

                <tr class="even">
                    <td class="sorting_1">Núm int.:</td>
                    <td>
                        <?php echo $Socio->getNumIntFiscal() ?>
                    </td>
                </tr>

                <tr class="odd">
                    <td class="sorting_1">País:</td>
                    <td>
                        <?php echo $Socio->getPaisFiscal() ?>
                    </td>
                </tr>

            </table>

        </div>
        <?php endif; ?>

        <div id="acercamiento-vd">

            <h3>Medio de acercamiento</h3>

            <table width="" border="0" class="dataTables_wrapper">

                <tr class="odd">
                    <td width="40%" class="sorting_1">¿Como se enteró del centro?:</td>
                    <td>
                        <?php echo $Socio->getCanalContacto() ?>
                    </td>
                </tr>

                <tr class="even">
                    <td class="sorting_1">Nombre del Promotor:</td>
                    <td>
                        <?php echo $Socio->getUsuarioRelatedByPromotorRefId() ?>
                    </td>
                </tr>

                <tr class="odd">
                    <td width="40%" class="sorting_1">Liga:</td>
                    <td>
                        <?php echo $Socio->getLiga() ?>
                    </td>
                </tr>

                <tr class="even">
                    <td class="sorting_1">Recomendó:</td>
                    <td>
                        <?php echo $Socio->getRecomienda()  ?>
                    </td>
                </tr>

                <tr class="odd">
                    <td width="40%" class="sorting_1">Otro medio o contacto:</td>
                    <td>
                        <?php echo $Socio->getOtrosContacto() ?>
                    </td>
                </tr>

                <tr class="even">
                    <td class="sorting_1">Socio:</td>
                    <td>
                        <?php echo $Socio->getSocioRelatedBySocioRefId()  ?>
                    </td>
                </tr>


                <tr class="odd">
                    <td class="sorting_1">Motivo capacitación:</td>
                    <td>
                        <?php echo $Socio->getMotivoCapacitacion() ?>
                    </td>
                </tr>

                <tr class="even">
                    <td class="sorting_1">Motivo acercamiento:</td>
                    <td>
                        <?php echo $Socio->getMotivoAcercamiento() ?>
                    </td>
                </tr>

            </table>
        </div>

        <div id="escolaridad-vd">
            <h3>Escolaridad</h3>

            <table width="" border="0" class="dataTables_wrapper">

                <tr class="odd">
                    <td width="40%" class="sorting_1">¿Estudia?:</td>
                    <td>
                        <?php echo ($Socio->getEstudia()==1)? 'Si':'No' ?>
                    </td>
                </tr>

                <tr class="even">
                    <td class="sorting_1">Nivel de estudios en curso:</td>
                    <td>
                        <?php echo (($Socio->getNivelEstudio() !== null) ? $Socio->getNivelEstudio()->getNombre() : '') ?>
                    </td>
                </tr>

                <tr class="odd">
                    <td width="40%" class="sorting_1">Campo de estudio:</td>
                    <td>
                        <?php echo (($Socio->getProfesionRelatedByProfesionActualId() !== null) ? $Socio->getProfesionRelatedByProfesionActualId()->getNombre() : '') ?>
                    </td>
                </tr>

                <tr class="even">
                    <td width="40%" class="sorting_1">Tipo de escuela:</td>
                    <td>
                        <?php echo (($Socio->getTipoEscuela() !== null) ? $Socio->getTipoEscuela()->getNombre() : null) ?>
                    </td>
                </tr>

                <tr class="odd">
                    <td width="40%" class="sorting_1">Tipo de estudios:</td>
                    <td>
                        <?php echo (($Socio->getTipoEstudio() !== null) ? $Socio->getTipoEstudio()->getNombre() : null) ?>
                    </td>
                </tr>

                <tr class="even">
                    <td width="40%" class="sorting_1">
                        Ultimo grado de estudios concluido al 100%:
                    </td>
                    <td>
                        <?php echo (($Socio->getGradoEstudio() !== null) ? $Socio->getGradoEstudio()->getNombre() : '')  ?>
                    </td>
                </tr>

                <tr class="odd">
                    <td width="40%" class="sorting_1">Profesión:</td>
                    <td>
                        <?php echo (($Socio->getProfesionRelatedByProfesionUltimoId() !== null) ? $Socio->getProfesionRelatedByProfesionUltimoId()->getNombre() : null) ?>
                    </td>
                </tr>

            </table>
        </div>

        <div id="ocupacion-vd">
            <h3>Ocupación</h3>
            <table width="" border="0" class="dataTables_wrapper">

                <tr class="odd">
                    <td width="40%" class="sorting_1">¿Trabaja?:</td>
                    <td>
                        <?php echo ($Socio->getTrabaja()==1)? 'Si':'No' ?>
                    </td>
                </tr>

                <tr class="even">
                    <td class="sorting_1">Tipo de Organizacion:</td>
                    <td>
                        <?php echo $Socio->getTipoOrganizacion() ?>
                    </td>
                </tr>

                <tr class="odd">
                    <td width="40%" class="sorting_1">Sector:</td>
                    <td>
                        <?php echo $Socio->getSector() ?>
                    </td>
                </tr>

                <tr class="even" style="display: none;">
                    <td class="sorting_1">Posición:</td>
                    <td>
                        <?php echo $Socio->getPosicion() ?>
                    </td>
                </tr>

                <tr class="even">
                    <td width="40%" class="sorting_1">Tiempo actividad:</td>
                    <td>
                        <?php echo $Socio->getTiempoActividad() ?>
                    </td>
                </tr>

                <tr class="odd">
                    <td width="40%" class="sorting_1">Problemas empresa:</td>
                    <td>
                        <?php echo $Socio->getProblemasEmpresa() ?>
                    </td>
                </tr>

                <tr class="even">
                    <td width="40%" class="sorting_1">Dependientes economicos:</td>
                    <td>
                        <?php echo $Socio->getDependientesEconomicos() ?>
                    </td>
                </tr>

                <tr class="odd">
                    <td width="40%" class="sorting_1">Ocupación:</td>
                    <td>
                        <?php echo $Socio->getOcupacion() ?>
                    </td>
                </tr>

            </table>

        </div>

    </div>
</div>
