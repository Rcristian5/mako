<br>
<br>
<br>
<br>
<br>
<!-- Cuerpo de la pantalla parametros $Id: cambiarClaveSuccess.php,v 1.1 2010/09/06 15:12:32 eorozco Exp $  -->

<?php if($primera_vez == true ): ?>
<h1>
	<div align="center">Se ha detectado que es la primera vez que nos
		visitas. Por favor cambia tu clave temporal.</div>
</h1>
<br>
<?php endif; ?>

<script>jQuery.noConflict();</script>
</script>
<script type="text/javascript">
					

        
        function comparaRequeridos(){
        
            if (requeridos()) {
				var cadenaClave = document.getElementById('clave');
				var cadenaClaveCompara = document.getElementById('claveCompara');
				//regexp
				var re = new RegExp(/(\w)(\1){7}/);
				
				//checa si la clave tiene como minimo 8 caracteres
				if (cadenaClave.value.length < 8) {
					alert ('La clave debe ser de mínimo 8 caracteres y sólo se han capturado ' + document.getElementById('clave').value.length);
					return false;
				}
                  
				 //checa q no tenga espacios en blanco, acentos ni eñes.
				 if (cadenaClave.value.search(/ñ|á|é|í|ó|ú|\s+/i) >= 1){
				 	alert ('La clave no puede contener acentos, espacios en blanco ni eñes.');
				 	return false;
				 }
				
				//checa q no tenga repeticion de todos los caracteres, como 1111111, aaaaa etc.
				 if (cadenaClave.value.match(re)){
				 	alert ('La clave no puede ser el mismo caracter todas las veces.');
				 	return false;
				 }
				//checa q no sea 12345678 ni 87654321 ni abcdefgh.
				 if (cadenaClave.value == '12345678' || cadenaClave == '87654321' || cadenaClave == 'abcdefgh'){
				 	alert ('Esta clave es muy sencilla de adivinar. Por favor selecciona otra.');
				 	return false;
				 }				  
				 				  
				 //checa si las claves coinciden  
                if (comparaCampos(cadenaClave.value, cadenaClaveCompara.value)) 
                    return true;
                else 
                    return false;
                
            }
            else 
                return false;
        	}
            </script>

<form action="<?php echo url_for('dashsocios/cambiarcv')?>"
	method="post">
	<div style="width: 100%" align="center">
		<div class="ui-widget ui-widget-content ui-corner-all sombra"
			style="width: 400px; minheight: 150px;">
			<div class="ui-widget ui-widget-header ui-corner-top tssss"
				style="padding: 5px;">Cambio de clave</div>
			<div style="margin-top: 20px">
				<?php echo $form['clave']->renderLabel("Clave") ?>
				<?php echo $form['clave']->render(array('requerido'=>'1','size'=>'15','maxlength'=>'30')) ?>
				<?php echo $form['clave']->renderError() ?>
				<br>
				<?php echo $form['clave_confirmacion']->renderLabel("Clave confirmación") ?>
				<?php echo $form['clave_confirmacion']->render(array('requerido'=>'1','size'=>'15','maxlength'=>'30')) ?>
				<?php echo $form['clave_confirmacion']->renderError() ?>
				<br>
				<?php echo $form['id']->render() ?>
				<?php echo $form['_csrf_token'] ?>
				<input type="submit" name="Cambiar clave" value="Cambiar clave"
					id="enviar" onclick="return requeridos();" /> <br>
			</div>
		</div>
	</div>
	<br>
</form>
<br>
<!-- Fin Cuerpo de la pantalla parametros administrador -->
<?php if($primera_vez == false): ?>
<div align="center">
	<a href="<?php echo url_for('dashsocios/micuenta')?>">Cancelar cambio
		de clave</a>
</div>
<?php endif; ?>
<br>
<br>
<br>
<br>
<br>
