<!DOCTYPE html>
<!-- saved from url=(0035)http://bd.org.mx/msjs/terminos.html -->
<html class="ui-mobile"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><!--<base href="http://bd.org.mx/msjs/terminos.html">--><base href=".">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
    <title>Red de Innovación y Aprendizaje</title>
    
    <link rel="stylesheet" href="/css/dashsocios/style.css">
    <link rel="shortcut icon" href="http://bd.org.mx/msjs/favicon.ico">
  

</head>
<body>
    <section>
        <header>
            <img src="/css/dashsocios/logo-ria.png" alt="Logo Red de Innovación y Aprendizaje"/>
        </header>
        <div id="main">
            <p>Mediante la aceptación por vía electrónica de los presentes términos y Código de conducta de uso (los “Términos”), cada usuario de la Red de Innovación y Aprendizaje(RIA) reconoce, acepta y conviene con la Fundación Proacceso ECO, A.C. (“ECO”)</p>
            <a class="botonacuerdos" href="javascript:void(0)" onclick = "document.getElementById('terminos').style.display='block';document.getElementById('fade').style.display='block'">Aceptar</a>

        </div>
        <div id="terminos" class="white_content">

            <div class="respoderheader"></div>
                <h3 class="title">Términos de Uso de la Red de Innovación y Aprendizaje</h3>
                <p>Mediante la aceptación por vía electrónica de los presentes términos y Código de conducta de uso (los “Términos”), cada usuario de los centros Red de Innovación y Aprendizaje” (la RIA reconoce, acepta y conviene con la Fundación Proacceso ECO, A.C. (“ECO")</p>
                <h4>Uso de los equipos</h4>
                <p>Los equipos de cómputo deberán ser utilizados en todo momento conforme al reglamento publicado en cada uno de los centros RIA. En todo momento los socios harán un uso cuidadoso de los equipos de cómputo y las demás instalaciones de la RIA.</p>
                <h4>Prohibiciones</h4>
                <p>Los equipos de los centros RIA no podrán ser utilizados, enunciativa y no limitativamente, para:</p>
                <ul>
                    <li>Llevar a cabo cualquier actividad prohibida por el reglamento de los centros RIA.</li>
                    <li>Llevar a cabo cualquier conducta ilícita.</li>
                    <li>Enviar correos indeseados.</li>
                    <li>Procesar, utilizar o distribuir cualquier material pornográfico.</li>
                    <li>Participar en campañas políticas o actividades religiosas, o involucrarse en actividades de propaganda, destinadas a influir en la legislación.</li>
                    <li>Realizar actividades lucrativas, salvo por aquéllas vinculadas a programas de capacitación y soporte a microempresarios </li>
                </ul>
            <h4>Información</h4>
            <p>La Fundación Proacceso no tendrá responsabilidad alguna respecto de la información guardada en la Red de Innovación y Aprendizaje. Asimismo, cada usuario reconoce que la Red de Innovación y Aprendizaje no cuenta con mecanismos internos para guardar la información. Por lo tanto, los socios deberán guardar su información en mecanismos físicos o virtuales externos.</p>
            <h4>Liberación de responsabilidad</h4>
            <p>Los usuarios liberan plenamente de responsabilidad a la Fundación Proacceso, sus empleados, funcionarios, afiliadas y socios comerciales por cualquier pérdida de información, daño o cualquier otra circunstancia que se pueda suscitar en los centros RIA. </p>
            <h4>Uso de información</h4>
            <p>Los usuarios reconocen que, consistentemente con los estándares de proveedores de correo electrónico y otros, la Fundación Proacceso podrá analizar su desempeño de aprendizaje, patrones de uso y otras conductas en los equipos de cómputo. El uso que la Fundación Proacceso dará a dicha información incluye enunciativa y no limitativamente: personalizar la publicidad y el contenido presentado en los centros RIA, cumplir con solicitudes de productos o servicios, mejorar nuestros servicios y contenido, contactar a los usuarios, realizar investigaciones, y proveer reportes anónimos para socios y clientes internos y externos. </p>
            <h4>Modificaciones</h4>
            <p>Los presentes Términos podrán ser modificados de tiempo en tiempo por la Fundación Proacceso y serán publicados en la página de Internet ria.org.mx. Dichas modificaciones se entienden aceptadas por el usuario desde este acto y es responsabilidad de éste consultar los Términos periódicamente.</p>
            <br />
            <br />
            <h3 class="title">Código de conducta para usuarios de la Red de Innovación y Aprendizaje</h3>
            <p>La Red de Innovación y Aprendizaje se esfuerza por brindarte un espacio educativo, de fomento a la lectura y a la cultura, en el que puedas disfrutar de sus servicios en las condiciones más favorables; por ello, te pedimos que leas con atención y agradecemos tu compromiso de respetar los siguientes lineamientos de conducta. De esta manera haremos de la Red de Innovación y Aprendizaje un lugar agradable para ti y toda la comunidad.</p>
            <p>Si tienes dudas, puedes comunicarte en cualquier momento con tu supervisor.</p>
            <h4>Durante mi estancia en la Red de Innovación y Aprendizaje:</h4>
            <p>Los equipos de cómputo deberán ser utilizados en todo momento conforme al reglamento publicado en cada RIA. En todo momento los socios harán un uso cuidadoso de los equipos de cómputo y las demás instalaciones de los centros RIA.</p>
            <h4>Prohibiciones</h4>
            <p><b>1.</b> Siempre seguiré las indicaciones de los encargados del centro.</p>
            <ul>
                <li>Colaboraré para crear un ambiente cordial y de respeto con los asesores y otros usuarios.</li>
                <li>En caso de problemas, acudiré con el supervisor para solicitar su apoyo.</li>
            </ul>
            <p><b>2.</b> Evitaré interrumpir, desalentar, sabotear o distraer a otros usuarios en las actividades programadas que se lleven acabo dentro de la Red de Innovación y Aprendizaje.</p>
            <ul>
                <li>Al escuchar música, lo haré siempre de manera que no interrumpa a nadie.</li>
                <li>Al entrar a Internet consultaré actividades educativas, laborales, culturales o lúdicas. Está estrictamente prohibido consultar en las instalaciones de la Red de Innovación y Aprendizaje material como: pornografía, violencia o abusos, drogas, descargas ilegales o piratería.</li>
            </ul>

            <p><b>3.</b> Nunca realizaré actividades que pongan en riesgo la integridad de los demás usuarios; del personal; de la Red de Innovación y Aprendizaje, o de su infraestructura física o tecnológica.</p>
            <ul>
                <li>Ninguna situación justifica actitudes o agresiones físicas o verbales hacia los usuarios o asesores de la Red de Innovación y Aprendizaje.</li>
                <li>Se prohíben el lenguaje o chistes ofensivos, comentarios inadecuados sobre raza, etnia, género o religión, comentarios degradantes, comportamientos intimidantes o amenazantes.</li>
                <li>Se prohíbe la posesión de armas dentro de la Red de Innovación y Aprendizaje o en su perímetro cercano.</li>
                <li>No robaré ni destruiré los equipos tecnológicos o los materiales físicos o digitales de los centros RIA.</li>
            </ul>
            <p><b>4.</b> Me apegaré a las indicaciones del supervisor, incluyendo en situaciones no contempladas en este <b>Código de conducta para usuarios de la Red de Innovación y Aprendizaje.</b></p>
            <p><b>5.</b> Si tú o alguna otra persona es objeto de agresión, discriminación o acoso por algún otro usuario dentro de la Red de Innovación y Aprendizaje, repórtalo con el supervisor.</p>
            <p>Todos los reportes de supuestos incumplimientos a este Código o a la ley, se tomarán con seriedad, objetividad y se revisarán de forma inmediata. Según corresponda, el funcionario de la Red de Innovación y Aprendizaje y sus supervisores analizarán el caso con apego a lo siguiente:</p>
            <p><b>En caso de incumplimiento de este código de conducta, acepto sujetarme a cualquiera de las siguientes medidas:</b></p>

            <p><b>a)</b> Una infracción menor relacionada con lo mencionado en el punto 2, conllevará la suspensión temporal de mi asistencia a la Red de Innovación y Aprendizaje.</p>
            <p><b>b)</b> Una infracción mayor relacionada con lo mencionado en el punto 2 en forma recurrente, o en el punto 3, conllevará la suspensión definitiva del acceso a la Red de Innovación y Aprendizaje.</p>
            <p><b>c)</b> Una infracción crítica relacionada con lo mencionado en el punto 3, conllevará la suspensión definitiva del acceso a cualquier RIA en el Estado de México.</p>

            <p>Además, acepto que si participo en actos de violencia o en actividades ilegales dentro o fuera de la Red de Innovación y Aprendizaje, podré ser sujeto a perder mi afiliación y a las sanciones que determine la autoridad competente.</p>
            <br />
            <br />
            <h3 class="title">Política preventiva: bullying y acoso</h3>
            <p>La Red de Innovación y Aprendizaje está compuesta por varios centros ubicados en diferentes municipios del Estado de México. El objetivo de estos centros es brindar a la comunidad acceso a tecnología, herramientas educativas y contenidos multimedia para atender necesidades académicas, personales, sociales y profesionales. Se distinguen por contar con instalaciones seguras y están abiertas a toda la comunidad.</p>
            <h4>Política para evitar violencia, bullying y acoso en los centros de la Red de Innovación y Aprendizaje</h4>
            <h4>Justificación</h4>
            <p>La convivencia con respeto es un derecho y un deber que tienen todos los integrantes de la Red de Innovación y Aprendizaje (RIA).</p>
            <p>Respetar esta política asegurará que cada centro de la RIA preste un servicio adecuado y sea un espacio agradable para toda la comunidad.</p>
            <h4>Objetivo</h4>
            <p>La siguiente política tiene la finalidad de promover entre los usuarios y colaboradores de los centros de la RIA una sana convivencia, haciendo énfasis en que éstos aprendan y trabajen en un espacio que favorezca la prevención de toda clase de violencia o agresión. Adicionalmente, establece lineamientos y protocolos de acción para casos de maltrato y acoso. </p>
            <h4>1. Derechos y deberes de los usuarios y colaboradores</h4>
            <ul class="list_number">
                <li>1.1 Promover y asegurar una sana convivencia.</li>
                <li>1.2 Hacer uso de las instalaciones y los espacios bajo la premisa del respeto mutuo y la tolerancia.</li>
                <li>1.3 Denunciar, reclamar, ser escuchados y solicitar que sus solicitudes sean atendidas.</li>
                <li>1.4 Colaborar en el tratamiento de situaciones de conflicto o maltrato entre cualquiera de los integrantes de los centros de la RIA y en el esclarecimiento de los hechos denunciados.</li>
            </ul>
            <h4>2. Definiciones y consideraciones generales</h4>
            <p class="note">2.1 Violencia: La Organización Mundial de la Salud define la violencia como “el uso deliberado de la fuerza física o el poder, ya sea en grado de amenaza o efectivo, contra uno mismo, otra persona o un grupo o comunidad, que cause o tenga muchas probabilidades de causar lesiones, muerte, daños psicológicos, trastornos del desarrollo o privaciones”.</p>
            <p>2.2 Se considera “bullying” o acoso, cualquier acción intencional, ya sea física o psicológica, realizada en forma escrita o verbal en contra de uno o más usuarios o colaboradores de los centros de la RIA, siempre que:</p>
            <ul class="list_letters">
                <li>Produzca temor y amenazar contra la integridad física, psíquica y emocional,</li>
                <li>Cree un ambiente intimidatorio, humillante o abusivo; o</li>
                <li>Dificulte o impida de cualquier manera el desempeño académico, moral, intelectual, afectivo o físico.</li>
            </ul>
            <p>2.3 Se entenderá como “Ciberbullying” el acoso a través de medios de comunicación como: correo electrónico, redes sociales, blogs, mensajería instantánea, mensajes de texto, teléfonos móviles y sitios web para acosar a una o varias personas. </p>
            <p>2.4 Está estrictamente prohibido cualquier acto de violencia, acoso, bullying o ciberbullying en los centros de la RIA o en un perímetro de dos metros a la redonda de las instalaciones o en actividades organizadas y coordinadas por la misma y en contra de un usuario o colaborador de los centros de la RIA. Se considerarán faltas por violencia y/o acoso las siguientes conductas realizadas por un:</p>
            <ul>
                <li>Colaborador(es) a usuario(s);</li>
                <li>Usuario(s) a colaborador(es);</li>
                <li>Usuario(s) a otro(s) usuario(s); y</li>
                <li>Colaborador(es) a otro(s) colaborador(es).</li>
            </ul>
            <ul class="list_letters">
                <li>Insultar, amenazar y ofender reiteradamente;</li>
                <li>Agredir física, verbal o psicológicamente;</li>
                <li>Intimidar, acosar o burlarse ;</li>
                <li>Discriminar por condición social, situación económica, religión, etnia, nombre, nacionalidad, orientación sexual, discapacidad o cualquier otra circunstancia;</li>
                <li>Atacar o desprestigiar a un usuario o colaborador a través de medios tecnológicos y de comunicación: mensajería instantánea, foros, redes sociales, mensajes de texto, correos electrónicos, sitios web y teléfono;</li>
                <li>Exhibir, transmitir o difundir por cualquier medio de comunicación o tecnológico alguna conducta de maltrato;</li>
                <li>Hacer gestos, insinuaciones y/o tener contacto físico que constituya acoso u hostigamiento sexual;</li>
                <li>Portar cualquier tipo de armas, instrumentos, utensilios u objetos punzocortantes, aun cuando no se haga uso de ellos dentro de los centros de la RIA, en un metro a la redonda de las instalaciones o en actividades organizadas y coordinadas por la misma; o</li>
                <li>Portar, vender, comprar, distribuir o consumir bebidas alcohólicas, drogas o sustancias ilícitas, y/o encontrarse bajo sus efectos, al interior de los centros de la RIA.</li>
            </ul>
            <p class="note_bottom">Organización Mundial de la Salud (2002). Informe mundial sobre la violencia y la salud. Washington, D.C.: Organización Panamericana de la Salud para la Organización Mundial de la Salud. Disponible en: http://www.who.int/violence_injury_prevention/violence/world_report/es/summary_es.pdf</p>
            <h4>3. Sobre protección a menores</h4>
            <p>Con la finalidad de evitar cualquier situación de riesgo para los usuarios menores, a continuación se detallan los siguientes lineamientos:</p>
            <p>Relación entre colaboradores de los centros de la RIA y los usuarios menores de 18 años.</p>
            <ul class="list_letters">
                <li>Queda estrictamente prohibido cualquier tipo de contacto físico o muestra afectiva entre colaboradores de los centros de la RIA y los usuarios. Este tipo de actividad será considerada como acoso, por lo que se aplicará la sanción de acuerdo a lo estipulado en el punto 6 de esta Política y se notificará la situación a la autoridad correspondiente.</li>
                <li>Los colaboradores que requieran conversar con usuarios en forma individual para tratar temas relacionados con sus actividades, deberán hacerlo en espacios abiertos y visibles.</li>
                <li>e prohíbe estrictamente a los colaboradores de los centros de la RIA, tener a los usuarios como amigos en Facebook u otras redes sociales. Se exceptúa de este lineamiento los casos en que se deban crear grupos con fines educativos o de coordinación de actividades propias de  los centros de la RIA.</li>
                <li>Queda prohibido establecer comunicación desde medios personales con los usuarios, ya sea vía telefónica, correo electrónico o cualquier otro medio de comunicación. La comunicación con los usuarios debe restringirse al call center o al número local de los centros de la RIA.</li>
                <li>Los colaboradores podrán participar en eventos sociales con los usuarios, cuando éstos se encuentren dentro del marco de actividades propias de los centros de la RIA: convivencias, activaciones, aniversario de los centros, ferias, presentaciones, exposiciones y conciertos, entre otras; salvo relación de parentesco del usuario con algún colaborador de los centros de la RIA.</li>
                <li>Los usuarios no pueden permanecer en ningún lugar de uso exclusivo de los colaboradores.</li>
                <li>El padre, madre o tutor son responsables de su (s) hijo/a (s), asegurándose de que salgan de los centros de la RIA una vez finalizada la actividad, únicamente en su compañía. Por lo anterior, el (los) padre(s) de familia o tutor, deberán permanecer en los centros de la RIA durante la actividad de su hijo (a).</li>
            </ul>
            <h4>4. Denuncia de casos</h4>
            <p>4.1 Todo reclamo por conductas contrarias a la sana convivencia dentro de los centros de la RIA o en eventos de la misma, podrá ser presentado por escrito al Líder de la RIA, quien deberá dar aviso al Coordinador y al Líder Académico dentro de un plazo de 24 horas, a fin de que se dé inicio al debido proceso. En caso de que el Líder RIA esté involucrado en el incidente, el escrito deberá ser enviado al siguiente correo electrónico:  <a href="mailto:politicasRIA@proacceso.org.mx">politicasRIA@proacceso.org.mx</a></p>
            <p>4.2 Todo escrito debe estar acompañado por una copia de cualquiera de los siguientes documentos vigentes: </p>
            <ul class="list_letters">
                <li>Credencial de elector</li>
                <li>Licencia de conducir</li>
                <li>Pasaporte</li>
                <li>Credencial escolar</li>
                <li>Credencial del INAPAM</li>
            </ul>
            <h4>Protocolo de actuación</h4>
            <p>5.1 El Líder Académico, el Coordinador y el Asesor Encargado asegurarán a todas las partes la mayor confidencialidad, privacidad y respeto, mientras se estén llevando a cabo las indagaciones.</p>
            <p>5.2 El Líder RIA llevará un registro individual de cada reclamo. En caso de que éste se encuentre involucrado en el incidente a investigar, será el Coordinador quien llevará el registro. Ningún tercero ajeno a la investigación podrá tener acceso a los registros, a excepción de que sea solicitado por alguna autoridad.</p>
            <p>5.3 El Líder Académico, el Coordinador, el Líder RIA y todos los colaboradores de los centros de la RIA, garantizarán la protección del afectado y de todos los involucrados, el derecho de todas las partes a ser oídas y la fundamentación de las decisiones, durante todo el procedimiento.</p>
            <p>5.4 El Líder RIA y el Coordinador deberán dirigir la investigación de las denuncias, entrevistando a las partes, solicitando información a terceros o disponiendo cualquier otra medida que estime necesaria para su esclarecimiento.</p>
            <p>5.5 Una vez recopilada la información o agotada la investigación, el Líder RIA y el Coordinador deberán presentar un informe al Líder Académico, para que éste indique la medida o sanción que corresponda.</p>
            <p>5.6 Una vez que el Líder Académico indique la sanción, el Líder RIA y/o el Coordinador deberán  citar a las partes y, en su caso, a los padres o tutores del socio(s) involucrado(s), a una reunión que tendrá como principal finalidad buscar un acuerdo entre las partes. Para esta entrevista, se considerará el tipo de tópicos que convenga tratar en presencia de los socios o sólo entre adultos.</p>
            <p>5.7 En caso de existir acuerdo entre las partes se podrá suspender el curso de la indagación, exigiendo a cambio el cumplimiento de determinadas condiciones por un período de tiempo convenido. Si se cumplen íntegramente las condiciones impuestas se dará por cerrado el reclamo, dejándose constancia de esta circunstancia.</p>
            <p>5.8 Si no hubiere acuerdo, se deberá oír a las partes involucradas, quienes deberán presentar todos los antecedentes que estimen necesarios. También se podrá citar a un profesional en la materia, quien podrá aconsejar o pronunciarse al respecto. De ser necesario, se dará aviso a la autoridad competente.</p>
            <p>5.9 De cada caso, el Líder Académico y el Coordinador deberán dejar constancia de los fundamentos que justifiquen la decisión adoptada. El Coordinador y/o el Líder RIA deberán notificar la resolución a todas las partes.</p>
            <h4>Medidas y sanciones sobre el incumplimiento de la política.</h4>
            <p>6.1 Todos los reportes de supuestos incumplimientos a esta  política o a las leyes estatales y/o federales vigentes al momento de la falta, se tomarán con seriedad, objetividad y se revisarán de forma inmediata. Las medidas y sanciones se podrán aplicar a quien incurra en conductas contrarias a la sana convivencia, y especialmente en los casos de violencia, maltrato y acoso.</p>
            <p>6.2 Según corresponda, la Fundación Proacceso ECO, A.C.; el Líder Académico, los Coordinadores y/o el Líder RIA analizarán los casos reportados de violencia, maltrato o acoso, con apego a lo expresado a continuación: </p>
            <ul class="list_letters">
                <li>Una infracción menor relacionada con lo mencionado en el punto 2.4 inciso a, b y d y 3.1 inciso f;  conllevará la suspensión temporal para asistir a los centros de la RIA.</li>
                <li>Una infracción mayor relacionada con lo mencionado en el punto 2.4 inciso a, b y d y 3.1 inciso f en forma recurrente; y en el punto 2.4 inciso c y 3.1 inciso b y d, conllevará la suspensión definitiva de la de los centros de la RIA.</li>
                <li>Una infracción crítica relacionada con lo mencionado en el punto 2.4 inciso e, f, g, h, i; y con el punto 3.1 inciso a; podrá derivar la suspensión definitiva del acceso a uno o cualquiera de los centros de la RIA operada por la Fundación Proacceso ECO, A.C.</li>
            </ul>
            <h4>Vigencia</h4>
            <p>Esta política entra en vigor partir del 1° de abril de 2016.</p>
            <p>En caso de dudas o sugerencias, agradecemos establecer comunicación con el Líder RIA o el Coordinador de los centros de la RIA. Con gusto podremos atenderle de manera gratuita en la siguiente línea: 01 800 SOLO RIA (7656 742).</p>
            <img src="/css/dashsocios/sign.png">
            <p sytle="text-align:center">
                Revisado</p>
                <p>Fundación Proacceso ECO, A.C.
            </p>
            <div class="responder">
                <a class="botonacuerdos" href="/dashsocios/aceptacion" onclick = "document.getElementById('terminos').style.display='none';document.getElementById('fade').style.display='none'">He leído y acepto términos, políticas y código de conducta</a>
                <a class="botonacuerdos" href="javascript:void(0)" onclick = "document.getElementById('terminos').style.display='none';document.getElementById('fade').style.display='none'">Cancelar</a>
            </div>
        </div>
        <div id="fade" class="black_overlay"></div>
    </section>
</body>
<footer>
        <p>2015 Todos los derechos reservados Fundación Proacceso ECO A.C. - Gobierno del Estado - Red de Innovación y Aprendizaje (RIA)</p>
</footer>
</html>