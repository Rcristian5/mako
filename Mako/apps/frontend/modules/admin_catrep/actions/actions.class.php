<?php

/**
 * admin_catrep actions.
 *
 * @package    mako
 * @subpackage admin_catrep
 * @author     Bixit SA de CV
 * @version    SVN: $Id: actions.class.php,v 1.1 2011/01/03 21:26:45 eorozco Exp $
 */
class admin_catrepActions extends sfActions
{
	public function executeIndex(sfWebRequest $request)
	{
		$this->CategoriaReportes = CategoriaReportePeer::doSelect(new Criteria());
	}

	public function executeShow(sfWebRequest $request)
	{
		$this->CategoriaReporte = CategoriaReportePeer::retrieveByPk($request->getParameter('id'));
		$this->forward404Unless($this->CategoriaReporte);
	}

	public function executeNew(sfWebRequest $request)
	{
		$this->form = new CategoriaReporteForm();
	}

	public function executeCreate(sfWebRequest $request)
	{
		$this->forward404Unless($request->isMethod(sfRequest::POST));

		$this->form = new CategoriaReporteForm();

		$this->processForm($request, $this->form);

		$this->setTemplate('new');
	}

	public function executeEdit(sfWebRequest $request)
	{
		$this->forward404Unless($CategoriaReporte = CategoriaReportePeer::retrieveByPk($request->getParameter('id')), sprintf('Object CategoriaReporte does not exist (%s).', $request->getParameter('id')));
		$this->form = new CategoriaReporteForm($CategoriaReporte);
	}

	public function executeUpdate(sfWebRequest $request)
	{
		$this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
		$this->forward404Unless($CategoriaReporte = CategoriaReportePeer::retrieveByPk($request->getParameter('id')), sprintf('Object CategoriaReporte does not exist (%s).', $request->getParameter('id')));
		$this->form = new CategoriaReporteForm($CategoriaReporte);

		$this->processForm($request, $this->form);

		$this->setTemplate('edit');
	}

	public function executeDelete(sfWebRequest $request)
	{
		$request->checkCSRFProtection();

		$this->forward404Unless($CategoriaReporte = CategoriaReportePeer::retrieveByPk($request->getParameter('id')), sprintf('Object CategoriaReporte does not exist (%s).', $request->getParameter('id')));
		$CategoriaReporte->delete();

		$this->redirect('admin_catrep/index');
	}

	protected function processForm(sfWebRequest $request, sfForm $form)
	{
		$form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
		if ($form->isValid())
		{
			$CategoriaReporte = $form->save();


	  $this->getUser()->setFlash('ok', sprintf('Se ha guardado correctamente el registro: %s',$form->getObject()));

	  if($form->isNew())
	  {
	  	sfProjectConfiguration::getActive()->getEventDispatcher()->notify(new sfEvent($CategoriaReporte, 'categoriareporte.creada',$CategoriaReporte));
	  }
	  else
	  {
	  	sfProjectConfiguration::getActive()->getEventDispatcher()->notify(new sfEvent($CategoriaReporte, 'categoriareporte.actualizada',$CategoriaReporte));
	  }
	   
	  $this->redirect('admin_catrep/index?id='.$CategoriaReporte->getId());
		}
	}
}
