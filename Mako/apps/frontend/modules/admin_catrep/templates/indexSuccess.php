<?php use_javascript('plugins/jquery.dataTables.js') ?>
<?php use_stylesheet('datatables.css') ?>

<script>

var lTCategoriaReportes;
$(document).ready(function() {

   lTCategoriaReportes = $('#lista-CategoriaReportes').dataTable({
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": true,
        "bJQueryUI": true,
        "sPaginationType": "full_numbers",
        "aaSorting": [[1, 'asc']] 
     });
   
});

</script>

<div class="ui-widget-header ui-corner-top tsss" style="padding: 5px;">Administrador
	de Categorías de Productos</div>
<div class="ui-widget ui-corner-bottom sombra">
	<div class="ui-widget-content ui-corner-bottom" style="padding: 5px;">

		<?php if ($sf_user->hasFlash('ok')): ?>
		<div
			class="flash_ok ui-state-highlight ts sombra-delgada ui-corner-all"
			style="padding: 5px; margin-top: 10px; margin-bottom: 10px;">
			<span style="float: left; margin-right: 0.3em;"
				class="ui-icon ui-icon-info"></span> <strong><?php echo $sf_user->getFlash('ok') ?>
			</strong>
		</div>
		<?php endif; ?>

		<br /> <span style="float: left;" class="ui-icon ui-icon-circle-plus"></span>
		<a href="<?php echo url_for('admin_catrep/new') ?>"> Agregar registro
		</a> <br /> <br />


		<table id="lista-CategoriaReportes" style="width: 100%;">
			<thead>
				<tr>
					<th>Editar</th>
					<th>Nombre</th>
					<th>Activo</th>
					<th>Creado</th>
					<th>Modificado</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($CategoriaReportes as $CategoriaReporte): ?>
				<tr>
					<td><a
						href="<?php echo url_for('admin_catrep/edit?id='.$CategoriaReporte->getId()) ?>">
							<span class="ui-icon  ui-icon-pencil"></span>
					</a>
					</td>
					<td><?php echo $CategoriaReporte->getNombre() ?></td>
					<td><?php echo $CategoriaReporte->getActivo()?'SI':'NO' ?></td>
					<td><?php echo $CategoriaReporte->getCreatedAt() ?></td>
					<td><?php echo $CategoriaReporte->getUpdatedAt() ?></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>


	</div>
</div>

