<?php

/**
 * restpreinsc actions.
 *
 * @package    mako NMP
 * @subpackage restpreinsc
 * @author     Fabrica de Software Enova
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class restpreinscActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */

  public function executeIndex(sfWebRequest $request)
  {
  		$arch_log    = "/tmp/log_restpreinsc.log";
  		$cadena_recibida = $request->getContent();
  		$this->getResponse()->setContentType('application/json');
        $this->getResponse()->setStatusCode(200);

   		// Validamos que sea un JSON valido
  		if ( !Comun::EsJson($cadena_recibida) )
  		{
  			error_log("\n[REST_PREINSC] La cadena enviada no es un JSON, favor de validar",3,$arch_log);
            $this->getResponse()->setStatusCode(404);
  			return $this->renderText(json_encode(array("code" => 404, "message" => "ERROR => " . json_last_error() . " :: Json no valido, por favor verifique.", "status" => true, "data" => array())));
  		}
        if ( !$request->hasParameter('grupo_id') || !$request->hasParameter('usuario') || !$request->hasParameter('centro_id') || !$request->hasParameter('socio_id') ) {
            error_log("\n[REST_PREINSC] Algún parametro obligatorio no fue proveido, favor de validar",3,$arch_log);
            $this->getResponse()->setStatusCode(404);
            return $this->renderText(json_encode(array("code" => 404, "message" => "ERROR => Algún parametro obligatorio no fue proveido, por favor verifique.", "status" => true, "data" => array())));
        }
  		switch($request->getMethod())
  		{
  			case 'POST':
  				$grupo_id     = $request->getParameter('grupo_id');
    			$usuario      = $request->getParameter('usuario');
                $centro_id    = $request->getParameter('centro_id');
                $socio_id     = $request->getParameter('socio_id');
    			$ObjSocio = SocioPeer::porNombreUsuario($usuario);
	    		error_log("\n[REST_PREINSC] PARAMS =>      GRUPO_ID = " . $grupo_id . "   ||    USUARIO = " . $usuario . "  ||  CENTRO_ID = " . $centro_id . "  ||  SOCIO_ID " . $socio_id,3,$arch_log);
				if ( !Comun::EsBigInt($grupo_id)) {
                    $this->getResponse()->setStatusCode(404);
    				return $this->renderText(json_encode(array("code" => 404, "message" => "ERROR => Parametro GRUPO_ID incorrecto, por favor verifique.", "status" => true, "data" => array())));
    			}
	    		if (  !is_string($usuario) ) {
                    $this->getResponse()->setStatusCode(404);
    				return $this->renderText(json_encode(array("code" => 404, "message" => "ERROR => Parametro USUARIO incorrecto, por favor verifique.", "status" => true, "data" => array())));
	    		}

    			if ( GrupoPeer::retrieveByPK($grupo_id) === null ) {
                    $this->getResponse()->setStatusCode(404);
    				error_log("\n\n\t[REST_PREINSC] No existe el grupo_id => " . $grupo_id,3,$arch_log);
    				return $this->renderText(json_encode(array("code" => 404, "message" => "ERROR => No existe el grupo, por favor verifique.", "status" => true, "data" => array())));
		    	}
    			if ( $ObjSocio === null ) {
    				// Aquí es donde se importa al socio 
                    //error_log("\nConsultando información de SIE...",3,$arch_log);       //
        			/*try {
                        $curl = curl_init($uri);
                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                        $curl_response = curl_exec($curl);
                        //error_log("\n[REST_PREINSC] CURL_RESPONSE => " . print_r($curl_response, true),3,$arch_log);
                        if ($curl_response === false) {
                            $info = curl_getinfo($curl);
                            curl_close($curl);
                            error_log("\n[CURL_CALL] INFO => " . print_r($info, true),3,$arch_log);
                            $this->getResponse()->setStatusCode(404);
                            return $this->renderText(json_encode(array("code" => 404, "message" => "ERROR [CURL_EXEC] => Ocurrio un error al ejecutar CURL_EXEC().", "status" => true, "data" => array())));
                        }
                        curl_close($curl);
                        $decoded = json_decode($curl_response);
                        error_log("\n[REST_PREINSC] DECODED => " . print_r($decoded, true),3,$arch_log);
                        if (isset($decoded->response->status) && $decoded->response->status == 'ERROR') {
                            error_log("\n[CURL_CALL::DECODE] STATUS => " . print_r($decoded->response->errormessage, true),3,$arch_log);
                            $this->getResponse()->setStatusCode(404);
                            return $this->renderText(json_encode(array("code" => 404, "message" => "ERROR [DECODE] => Ocurrio un error " . $decoded->response->errormessage, "status" => true, "data" => array())));
                        }
                        $res = array();
                        $res[] = $decoded->data[0]->centro_id;
                        $res[] = $decoded->data[0]->socio_id;*/
                        $res = array();
                        $res[] = $centro_id;
                        $res[] = $socio_id;
                        error_log("\n[REST_PREINSC] RES => " . print_r($res, true),3,$arch_log);
        			/*} catch (Exception $e) {
                        $this->getResponse()->setStatusCode(404);
                        error_log("\nError al consultar usuario en SIE.",3,$arch_log);       //
            			return $this->renderText(json_encode(array("code" => 404, "message" => "ERROR [SIE_CURL] => No se pudo consultar la información del socio.", "status" => true, "data" => array())));
        			}*/

        			if (is_array($res)) {
            			list($centro_id, $socio_id) = $res;
            			error_log("\n[REST_PREINSC] Tratando de importar el socio con datos: $centro_id => $socio_id",3,$arch_log);

            			$centro = CentroPeer::retrieveByPK($centro_id);
            			if ( $centro === null ) {
                            $this->getResponse()->setStatusCode(404);
            				error_log("\n[REST_PREINSC] No existe el centro en la base de datos local. Por favor, verifique.",3,$arch_log);
                            return $this->renderText(json_encode(array("code" => 404, "message" => "ERROR [REST_PREINSC] => No existe centro en base de datos local.", "status" => true, "data" => array())));
                        }

            			try {
                			$cliente = new SoapClient("http://" . $centro->getIp() . "/MakoApi.wsdl");
                			error_log("\n[REST_PREINSC] Conectando a : " . $centro->getIp(),3,$arch_log);
            			} catch (Exception $e) {
                            $this->getResponse()->setStatusCode(404);
                			error_log("\n[REST_PREINSC] No se pudo conectar con CENTRO_ORIGEN => " . $e->getMessage(),3,$arch_log);
                			return $this->renderText(json_encode(array("code" => 404, "message" => "ERROR [CTRO_CNX] => No se pudo consultar la información del socio.", "status" => true, "data" => array())));
            			}

            			try {
                			$sres = $cliente->registro_getSocio($socio_id);
			                error_log("\n[REST_PREINSC] Se han importado los datos del socio correctamente. ",3,$arch_log);
            			} catch (Exception $e) {
                            $this->getResponse()->setStatusCode(404);
                			error_log("\n[REST_PREINSC] Error al ejecutar el metodo REGISTRO_GETSOCIO(). " . $e->getMessage(),3,$arch_log);
                			return $this->renderText(json_encode(array("code" => 404, "message" => "ERROR [METHOD_API] => No se pudo consultar la información del socio.", "status" => true, "data" => array())));
            			}
            			try {
            				error_log("\n[REST_PREINSC] Creando registro de socio IMPORTADO. ",3,$arch_log);
            				$asoc = unserialize($sres);
                			$socio = new Socio();
			            	$socio->fromArray($asoc);
            				$socio->save();
            				error_log("\n[REST_PREINSC] Registro de socio IMPORTADO creado exitosamente. SID => " . $socio_id,3,$arch_log);
            				$Alumno = new Alumnos();
            				$Alumno->setMatricula($socio->getId());
            				$Alumno->setSocioId($socio->getId());
            				$Alumno->save();
            			} catch (Exception $e) {
                            $this->getResponse()->setStatusCode(404);
                			error_log("\n[REST_PREINSC] Error a crear socio en la base de datos LOCAL. Revise LOG. " . $e->getMessage(),3,$arch_log);
                			return $this->renderText(json_encode(array("code" => 404, "message" => "ERROR [METHOD_API] => No se pudo consultar la información del socio.", "status" => true, "data" => array())));
            			}
        			} else {
            			error_log("\n[REST_PREINSC] No es posible determinar de que centro proviene el socio",3,$arch_log);
                        $this->getResponse()->setStatusCode(404);
            			return $this->renderText(json_encode(array("code" => 404, "message" => "ERROR [SOCIO_UNKNOW] => No es posible determinar de que centro proviene el socio.", "status" => true, "data" => array())));
        			}
    				/******************************************************************/
    				// Reintentando obtener datos ya importados
    				try {
    					error_log("\n[REST_PREINSC] Buscando registro importando en base de datos LOCAL." ,3,$arch_log);
    					$ObjSocio = SocioPeer::porNombreUsuario($usuario);
    				}catch (Exception $e) {
                        $this->getResponse()->setStatusCode(404);
    					error_log("\n[REST_PREINSC] Error al obtener socio importado de base de datos local. Revise LOG. "  . $e->getMessage() ,3,$arch_log);
                        return $this->renderText(json_encode(array("code" => 404, "message" => "ERROR [DB_ERR] => Error al obtener socio importado de base de datos local. Revise LOG.", "status" => true, "data" => array())));
    				}
    			}
    			$c = new Criteria();
    			$c->clearSelectColumns();
    			$c->addSelectColumn(AlumnosPeer::MATRICULA);
    			$c->add(AlumnosPeer::SOCIO_ID,$ObjSocio->getId());

    			//error_log("\n[REST_PREINSC] Qry => " . $c->toString(),3,$arch_log);

    			$ObjAlumnos = AlumnosPeer::doSelectOne($c);

                if ( $ObjAlumnos === null )
                {
                    error_log("\n[REST_PREINSC] Error ObjAlumnos no existe. Creando matricula.",3,$arch_log);
                    $Alumno = new Alumnos();
                    $Alumno->setMatricula($ObjSocio->getId());
                    $Alumno->setSocioId($ObjSocio->getId());
                    $Alumno->save();
                    $ObjAlumnos = $Alumno;
                }
    			//error_log("\n[REST_PREINSC] ObjAlumnos => " . print_r($ObjAlumnos,true),3,$arch_log);

    			$respuesta = GrupoAlumnosPeer::preincribe($ObjAlumnos->getMatricula(),$grupo_id);
    			error_log("\nRESP_PREINSCRIBE => " . print_r($respuesta,true),3,$arch_log);
    			if ( isset($respuesta['estatus']) && $respuesta['estatus'] === 0)
    			{
    				return $this->renderText(json_encode(array("code" => 200, "message" => "El SOCIO ya se encuentra enrolado a este curso.", "status" => true, "data" => array())));
    			}
    			return $this->renderText(json_encode(array("code" => 200, "message" => "Socio preinscrito a grupo " . $respuesta['clave'] . " del curso " . $respuesta['nombre'] . ".     IDTS => " . $respuesta['id'], "status" => true, "data" => array('idts' => $respuesta['id']))));
    			break;
    		default:
                $this->getResponse()->setStatusCode(404);
    			return $this->renderText(json_encode(array("code" => 404, "message" => "ERROR :: No existe una operación valida. Por favor, verifique.", "status" => true, "data" => array())));
                break;
    	}
        $this->getResponse()->setStatusCode(404);
    	return $this->renderText(json_encode(array("code" => 404, "message" => "No se realizó ninguna operación.", "status" => true, "data" => array())));
	}
}
