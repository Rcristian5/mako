<?php

/**
 * empresa actions.
 *
 * @package    mako
 * @subpackage empresa
 * @author     Bixit SA de CV
 * @version    SVN: $Id: actions.class.php,v 1.2 2010/03/06 10:13:50 marcela Exp $
 */
class empresaActions extends sfActions
{
	public function executeIndex(sfWebRequest $request)
	{
		$this->Empresas = EmpresaPeer::doSelect(new Criteria());
	}

	public function executeShow(sfWebRequest $request)
	{
		$this->Empresa = EmpresaPeer::retrieveByPk($request->getParameter('id'));
		$this->forward404Unless($this->Empresa);
	}

	public function executeNew(sfWebRequest $request)
	{
		$this->form = new EmpresaForm();
	}

	public function executeCreate(sfWebRequest $request)
	{
		$this->forward404Unless($request->isMethod(sfRequest::POST));

		$this->form = new EmpresaForm();

		$this->processForm($request, $this->form);

		$this->setTemplate('new');
	}

	public function executeEdit(sfWebRequest $request)
	{
		$this->forward404Unless($Empresa = EmpresaPeer::retrieveByPk($request->getParameter('id')), sprintf('Object Empresa does not exist (%s).', $request->getParameter('id')));
		$this->form = new EmpresaForm($Empresa);
	}

	public function executeUpdate(sfWebRequest $request)
	{
		$this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
		$this->forward404Unless($Empresa = EmpresaPeer::retrieveByPk($request->getParameter('id')), sprintf('Object Empresa does not exist (%s).', $request->getParameter('id')));
		$this->form = new EmpresaForm($Empresa);

		$this->processForm($request, $this->form);

		$this->setTemplate('edit');
	}

	public function executeDelete(sfWebRequest $request)
	{
		$request->checkCSRFProtection();

		$this->forward404Unless($Empresa = EmpresaPeer::retrieveByPk($request->getParameter('id')), sprintf('Object Empresa does not exist (%s).', $request->getParameter('id')));
		$Empresa->delete();

		$this->redirect('empresa/index');
	}

	protected function processForm(sfWebRequest $request, sfForm $form)
	{
		$form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
		if ($form->isValid())
		{
			$Empresa = $form->save();

			$this->redirect('empresa/edit?id='.$Empresa->getId());
		}
	}

	/**
	 * Regresa el partial para dar de alta una empresa
	 * @param sfWebRequest $request
	 * @return
	 */
	public function executeAltaEmpresa(sfWebRequest $request)
	{
		$this->form = new EmpresaForm();
		return $this->renderPartial('empresa/altaEmpresa');
	}

}
