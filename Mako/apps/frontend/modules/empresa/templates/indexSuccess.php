<h1>Empresas List</h1>

<table>
	<thead>
		<tr>
			<th>Id</th>
			<th>Nombre</th>
			<th>Razon social</th>
			<th>Tipo</th>
			<th>Id estado</th>
			<th>Estado</th>
			<th>Ciudad</th>
			<th>Id del</th>
			<th>Municipio</th>
			<th>Id municipio</th>
			<th>Colonia</th>
			<th>Id asentamiento</th>
			<th>Cp</th>
			<th>Calle</th>
			<th>Num ext</th>
			<th>Num int</th>
			<th>Activo</th>
			<th>Created at</th>
			<th>Updated at</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($Empresas as $Empresa): ?>
		<tr>
			<td><a
				href="<?php echo url_for('empresa/show?id='.$Empresa->getId()) ?>"><?php echo $Empresa->getId() ?>
			</a></td>
			<td><?php echo $Empresa->getNombre() ?></td>
			<td><?php echo $Empresa->getRazonSocial() ?></td>
			<td><?php echo $Empresa->getTipoId() ?></td>
			<td><?php echo $Empresa->getIdEstado() ?></td>
			<td><?php echo $Empresa->getEstado() ?></td>
			<td><?php echo $Empresa->getCiudad() ?></td>
			<td><?php echo $Empresa->getIdDel() ?></td>
			<td><?php echo $Empresa->getMunicipio() ?></td>
			<td><?php echo $Empresa->getIdMunicipio() ?></td>
			<td><?php echo $Empresa->getColonia() ?></td>
			<td><?php echo $Empresa->getIdAsentamiento() ?></td>
			<td><?php echo $Empresa->getCp() ?></td>
			<td><?php echo $Empresa->getCalle() ?></td>
			<td><?php echo $Empresa->getNumExt() ?></td>
			<td><?php echo $Empresa->getNumInt() ?></td>
			<td><?php echo $Empresa->getActivo() ?></td>
			<td><?php echo $Empresa->getCreatedAt() ?></td>
			<td><?php echo $Empresa->getUpdatedAt() ?></td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<a href="<?php echo url_for('empresa/new') ?>">New</a>
