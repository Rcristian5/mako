<table>
	<tbody>
		<tr>
			<th>Id:</th>
			<td><?php echo $Empresa->getId() ?>
			</td>
		</tr>
		<tr>
			<th>Nombre:</th>
			<td><?php echo $Empresa->getNombre() ?>
			</td>
		</tr>
		<tr>
			<th>Razon social:</th>
			<td><?php echo $Empresa->getRazonSocial() ?>
			</td>
		</tr>
		<tr>
			<th>Tipo:</th>
			<td><?php echo $Empresa->getTipoId() ?>
			</td>
		</tr>
		<tr>
			<th>Id estado:</th>
			<td><?php echo $Empresa->getIdEstado() ?>
			</td>
		</tr>
		<tr>
			<th>Estado:</th>
			<td><?php echo $Empresa->getEstado() ?>
			</td>
		</tr>
		<tr>
			<th>Ciudad:</th>
			<td><?php echo $Empresa->getCiudad() ?>
			</td>
		</tr>
		<tr>
			<th>Id del:</th>
			<td><?php echo $Empresa->getIdDel() ?>
			</td>
		</tr>
		<tr>
			<th>Municipio:</th>
			<td><?php echo $Empresa->getMunicipio() ?>
			</td>
		</tr>
		<tr>
			<th>Id municipio:</th>
			<td><?php echo $Empresa->getIdMunicipio() ?>
			</td>
		</tr>
		<tr>
			<th>Colonia:</th>
			<td><?php echo $Empresa->getColonia() ?>
			</td>
		</tr>
		<tr>
			<th>Id asentamiento:</th>
			<td><?php echo $Empresa->getIdAsentamiento() ?>
			</td>
		</tr>
		<tr>
			<th>Cp:</th>
			<td><?php echo $Empresa->getCp() ?>
			</td>
		</tr>
		<tr>
			<th>Calle:</th>
			<td><?php echo $Empresa->getCalle() ?>
			</td>
		</tr>
		<tr>
			<th>Num ext:</th>
			<td><?php echo $Empresa->getNumExt() ?>
			</td>
		</tr>
		<tr>
			<th>Num int:</th>
			<td><?php echo $Empresa->getNumInt() ?>
			</td>
		</tr>
		<tr>
			<th>Activo:</th>
			<td><?php echo $Empresa->getActivo() ?>
			</td>
		</tr>
		<tr>
			<th>Created at:</th>
			<td><?php echo $Empresa->getCreatedAt() ?>
			</td>
		</tr>
		<tr>
			<th>Updated at:</th>
			<td><?php echo $Empresa->getUpdatedAt() ?>
			</td>
		</tr>
	</tbody>
</table>

<hr />

<a href="<?php echo url_for('empresa/edit?id='.$Empresa->getId()) ?>">Edit</a>
&nbsp;
<a href="<?php echo url_for('empresa/index') ?>">List</a>
