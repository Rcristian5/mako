<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<?php use_javascript('jquery.ui.js') ?>
<?php use_stylesheet('tabs.css') ?>

<form name="registro"
	action="<?php echo url_for('empresa/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>"
	method="post"
	<?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
	<?php if (!$form->getObject()->isNew()): ?>
	<input type="hidden" name="sf_method" value="put" />
	<?php endif; ?>

	<script type="text/javascript">
	$(function() {
		$("#tabsEmpresa").tabs();
	});
	</script>

	<div id="page-wrap" class="sombra">

		<div id="tabsEmpresa">

			<div id="generales">

				<h3>Datos generales</h3>

				<p>
					<?php echo $form['nombre']->renderLabel() ?>
					<?php echo $form['nombre']->render(array('requerido'=>'1','filtro'=>'alfanumerico','size'=>'50','maxlength'=>'30')) ?>
					<?php echo $form['nombre']->renderError() ?>
					<br />

					<?php echo $form['razon_social']->renderLabel() ?>
					<?php echo $form['razon_social']->render(array('requerido'=>'1','filtro'=>'alfanumerico','size'=>'50','maxlength'=>'30')) ?>
					<?php echo $form['razon_social']->renderError() ?>
					<br />

					<?php echo $form['tipo_id']->renderLabel() ?>
					<?php echo $form['tipo_id']->render(array('requerido'=>'1')) ?>
					<?php echo $form['tipo_id']->renderError() ?>
					<br />

				</p>

				<h3>Domicilio</h3>
				<br>
				<?php echo $form['id_estado']->renderLabel("Estado") ?>
				<?php echo $form['id_estado']->render() ?>
				<?php echo $form['id_estado']->renderError() ?>
				<br />

				<?php echo $form['municipio']->renderLabel() ?>
				<?php echo $form['municipio']->render() ?>
				<?php echo $form['municipio']->renderError() ?>
				<br />

				<?php echo $form['colonia']->renderLabel() ?>
				<?php echo $form['colonia']->render() ?>
				<?php echo $form['colonia']->renderError() ?>
				<br />

				<?php echo $form['cp']->renderLabel("C.P.") ?>
				<?php echo $form['cp']->render(array('filtro'=>'numerico','size'=>'5','maxlength'=>'5')) ?>
				<?php echo $form['cp']->renderError() ?>
				<br />

				<?php echo $form['calle']->renderLabel() ?>
				<?php echo $form['calle']->render() ?>
				<?php echo $form['calle']->renderError() ?>
				<br />

				<?php echo $form['num_ext']->renderLabel("Número exterior") ?>
				<?php echo $form['num_ext']->render() ?>
				<?php echo $form['num_ext']->renderError() ?>
				<br />

				<?php echo $form['num_int']->renderLabel("Número interior") ?>
				<?php echo $form['num_int']->render() ?>
				<?php echo $form['num_int']->renderError() ?>
				<br />


			</div>

			<br />
			<?php echo $form['id'] ?>
			<?php echo $form['_csrf_token'] ?>

			<input type="submit" value="Guardar"
				onclick="return requeridos(document.getElementById('generales')); $(this).trigger('empresa.seleccionada')" />


		</div>
	</div>


</form>
