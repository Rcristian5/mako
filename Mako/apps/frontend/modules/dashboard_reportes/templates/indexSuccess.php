
<?php use_javascript('plugins/jquery.windows.js') ?>
<?php use_javascript('jquery.ui.js') ?>
<?php use_javascript('i18n/jquery-ui-i18n.js') ?>
<?php use_javascript('plugins/jquery.qtip.js') ?>

<?php use_stylesheet('./dashboard_reportes/indexSuccess.css')?>

<div id="windows-wrapper">
	<div window-title="Seguimiento Indicadores" window-state="min">
		<h1 class="nombre-reporte">Seguimiento Indicadores</h1>
		<form action="<?php url_for('dashboard_reportes/index') ?>">

			<div id="left">
				<fieldset>
					<legend>Opciones</legend>
					<div>
						<div>
							<label for="filtro">Filtro:</label> <select name="filtro"
								id="filtro">
								<option value="centro">Centro</option>
								<option value="grupo">Curso/Grupo</option>
								<option value="facilitador">Facilitador</option>
								<option value="ria" selected="selected">RIA</option>
								<option value="zona">Zona</option>
							</select>
						</div>
					</div>
				</fieldset>

				<fieldset>
					<legend>Per&iacute;odo</legend>
					<div>
						<label for="desde">Desde:</label> <input type="text" name="desde"
							id="desde" />
					</div>
					<div>
						<label for="hasta">Hasta:</label> <input type="text" name="hasta"
							id="hasta" />
					</div>
				</fieldset>
				<fieldset>
					<legend>Tama&ntilde;o de muestra</legend>
					<div id="muestra">
						<input type="radio" name="muestra" value="mes" id="mes"
							checked="checked"> <label for="mes">Mes</label> <input
							type="radio" name="muestra" value="semana" id="semana"> <label
							for="semana">Semana</label>
					</div>
				</fieldset>
			</div>
			<div id="right">
				<fieldset>
					<legend>Indicadores</legend>
					<table class="indicadores">
						<tr>
							<td>
								<div>
									<input type="checkbox" name="registrados" value="1"
										id="registrados" class="off"> <span
										class="ui-icon ui-icon-info info"
										title="Total de socios registrados  hasta a la fecha seleccionada."></span>
									<label for="registrados">Socios Registrados(#)</label>
								</div>
								<div>
									<input type="checkbox" name="activos" value="1" id="activos"
										class="off"> <span class="ui-icon ui-icon-info info"
										title="Número de socios activos. Un socio activos es aquel que realizó alguna operación de compra (no importando el tipo de producto que se adquiere) o asistieron a alguna sesión programada de un grupo."></span>
									<label for="activos">Socios Activos(#)</label>
								</div>
								<div>
									<input type="checkbox" name="activos_nuevos" value="1"
										id="activos_nuevos" class="off"> <span
										class="ui-icon ui-icon-info info"
										title="Cantidad de socios nuevos que realizaron alguna operación de compra o asistieron a alguna sesión programada de un grupo."></span>
									<label for="activos">Socios Activos Nuevos(#)</label>
								</div>
								<div>
									<input type="checkbox" name="nuevos" value="1" id="nuevos"
										class="off"> <span class="ui-icon ui-icon-info info"
										title="Cantidad de registros de socios nuevos."></span> <label
										for="nuevos">Registros Nuevos(#)</label>
								</div>
								<div>
									<input type="checkbox" name="cobranza_internet" value="1"
										id="cobranza_internet" class="off"> <span
										class="ui-icon ui-icon-info info"
										title="Monto recaudado por la venta de los productos relacionados con el producto Internet."></span>
									<label for="cobranza_internet">Cobranza Internet($)</label>
								</div>
								<div>
									<input type="checkbox" name="cobranza_educacion" value="1"
										id="cobranza_educacion" class="off"> <span
										class="ui-icon ui-icon-info info"
										title="Monto recaudado por la venta de cursos."></span> <label
										for="cobranza_educacion">Cobranza Educaci&oacute;n($)</label>
								</div>
								<div>
									<input type="checkbox" name="cobranza_otros" value="1"
										id="cobranza_otros" class="off"> <span
										class="ui-icon ui-icon-info info"
										title="Monto recaudado por la venta de cualquier producto que no es Internet o Cursos."></span>
									<label for="cobranza_otros">Cobranza Otros($)</label>
								</div>
							</td>
							<td>
								<div>
									<input type="checkbox" name="cobranza_total" value="1"
										id="cobranza_total" class="off"> <span
										class="ui-icon ui-icon-info info"
										title="Monto recaudado por la venta de cualquier producto en un periodo de tiempo dado."></span>
									<label for="cobranza_total">Cobranza Total($)</label>
								</div>
								<div>
									<input type="checkbox" name="inscripcion_esperada" value="1"
										id="inscripcion_esperada"> <span
										class="ui-icon ui-icon-info info"
										title="Total de alumnos que se espera se inscriban a los grupos que inician en un mes o semana dados. Esta cantidad es el cupo total que tienen las aulas en el momento en que se calendariza un grupo."></span>
									<label for="inscripcion_esperada">Incripci&oacute;n Esperada(#)</label>
								</div>
								<div>
									<input type="checkbox" name="preinscritos" value="1"
										id="preinscritos"> <span class="ui-icon ui-icon-info info"
										title="Total de alumnos que permanecen con estado Preinscrito a los cursos programados que inician dentro del periodo seleccionado."></span>
									<label for="preinscritos">Alumnos Preinscritos(#)</label>
								</div>
								<div>
									<input type="checkbox" name="inscritos" value="1"
										id="inscritos"> <span class="ui-icon ui-icon-info info"
										title="Total de alumnos que tiene estado Inscrito en los cursos programados que inician dentro del periodo seleccionado."></span>
									<label for="inscritos">Alumnos Inscritos(#)</label>
								</div>
								<div>
									<input type="checkbox" name="inscritos_nuevos" value="1"
										id="inscritos_nuevos"> <span class="ui-icon ui-icon-info info"
										title="Cantidad de Socios Nuevos que además se inscribieron a un curso."></span>
									<label for="inscritos_nuevos">Alumnos Inscritos Nuevos(#)</label>
								</div>
								<div>
									<input type="checkbox" name="inscritos_recurrentes" value="1"
										id="inscritos_recurrentes"> <span
										class="ui-icon ui-icon-info info"
										title="Total de alumnos inscritos que han finalizado al menos un curso en el pasado."></span>
									<label for="inscritos_recurrentes">Alumnos Inscritos
										Recurrentes(#)</label>
								</div>
							</td>
							<td>
								<div>
									<input type="checkbox" name="actividad_programada" value="1"
										id="alumnos_actividad_programada"> <span
										class="ui-icon ui-icon-info info"
										title="El total de alumnos inscritos a grupos activos de un mes o semana que se espera acudan al centro"></span>
									<label for="alumnos_activos">Alumnos Programados(#)</label>
								</div>
								<div>
									<input type="checkbox" name="alumnos_activos" value="1"
										id="alumnos_activos"> <span class="ui-icon ui-icon-info info"
										title="Total de alumnos que asistieron al menos a una clase o realizaron al menos un pago en el periodo."></span>
									<label for="alumnos_activos">Alumnos Activos(#)</label>
								</div>
								<div>
									<input type="checkbox" name="alumnos_esperados" value="1"
										id="alumnos_esperados"> <span
										class="ui-icon ui-icon-info info"
										title="Total de lugares programados de todos los grupos activos durante el lapso de tiempo proporcionado."></span>
									<label for="alumnos_esperados">Alumnos Potenciales(#)</label>
								</div>
								<div>
									<input type="checkbox" name="graduacion_esperada" value="1"
										id="graduacion_esperada"> <span
										class="ui-icon ui-icon-info info"
										title="Cantidad alumnos inscritos a los grupo que finalizan en un periodo de tiempo dado y que se espera se gradúen."></span>
									<label for="graduados">Graduaci&oacute;n Esperada(#)</label>
								</div>
								<div>
									<input type="checkbox" name="graduados" value="1"
										id="graduados"> <span class="ui-icon ui-icon-info info"
										title="Total de alumnos que obtuvieron una calificación mínima de 80 puntos o más en la evaluación final. Las únicas evaluaciones que son analizadas por el sistema son: Evaluación final, Evaluación Final y Examen final MateRIA.  Este valor aplica únicamente para grupos que han finalizado en el periodo."></span>
									<label for="graduados">Alumnos Graduados(#)</label>
								</div>
								<div>
									<input type="checkbox" name="reprobados" value="1"
										id="reprobados"> <span class="ui-icon ui-icon-info info"
										title="Total de alumnos que obtuvieron una calificación menor a 80 puntos en la evaluación final. Las únicas evaluaciones que son analizadas por el sistema son: Evaluación final, Evaluación Final y Examen final MateRIA.  Este valor aplica únicamente para grupos que han finalizado en el periodo."></span>
									<label for="reprobados">Alumnos Reprobados(#)</label>
								</div>
							</td>
							<td>
								<div>
									<input type="checkbox" name="sin_evaluacion" value="1"
										id="sin_evaluacion"> <span class="ui-icon ui-icon-info info"
										title="Total de alumnos que no realizaron la evaluación final al término de las sesiones programadas de un grupo."></span>
									<label for="sin_evaluacion">Alumnos Sin Evaluaci&oacute;n(#)</label>
								</div>
								<div>
									<input type="checkbox" name="desertores" value="1"
										id="desertores"> <span class="ui-icon ui-icon-info info"
										title="Total de alumnos que no cubrieron los últimos 2 pagos del curso y no asistieron a las últimas 2 sesiones. Este valor aplica únicamente para los grupos que han finalizado en el periodo."></span>
									<label for="desertores">Alumnos Desertores(#)</label>
								</div>
								<div>
									<input type="checkbox" name="pagos_programados" value="1"
										id="pagos_programados"> <span
										class="ui-icon ui-icon-info info"
										title="Cantidad de pagos programados para el periodo."></span>
									<label for="pagos_programdos">Pagos Programados(#)</label>
								</div>
								<div>
									<input type="checkbox" name="pagos_cobrados" value="1"
										id="pagos_cobrados"> <span class="ui-icon ui-icon-info info"
										title="Cantidad de pagos que fueron cubiertos."></span> <label
										for="pagos_cobrados">Pagos Cobrados(#)</label>
								</div>
								<div>
									<input type="checkbox" name="cobranza_programada" value="1"
										id="cobranza_programada"> <span
										class="ui-icon ui-icon-info info"
										title="Monto total de pagos programados."></span> <label
										for="cobranza_programada">Cobranza Programada($)</label>
								</div>
								<div>
									<input type="checkbox" name="cobranza_cobrada" value="1"
										id="cobranza_cobrada"> <span class="ui-icon ui-icon-info info"
										title="Monto total de pagos cubiertos."></span> <label
										for="cobranza_cobrada">Cobranza Cobrada($)</label>
								</div>

							</td>
						</tr>
						<tr>
							<td colspan="4">
								<div class="all">
									<input type="checkbox" name="todos" value="1" id="todos"> <label
										for="todos">Marcar/Desmarcar Todos</label>
								</div>
							</td>
						</tr>
					</table>
				</fieldset>
			</div>
			<div class="download">
				<input type="submit" value="Descargar reporte" id="submit-button">
			</div>
		</form>
	</div>
</div>


<script type="text/javascript">
$(document).ready(function () {
	$('#windows-wrapper').windows({
		resizable : true,
	    minimizable: false,
	    minheight : 300
	});
	$('span.info').qtip({
		   show: 'mouseover',
		   hide: 'mouseout',
		   style: { 
			   	  name: 'blue',
			      tip: true,
			      border: {
			          radius: 6,
			       },
			       fontSize: 15,
			       backgroundColor : '#ffffff',
			       lineHeight: 1.4
			   }
		})
	$('#todos').click(function(){
		if($(this).attr('checked'))
		{
			$('input[type="checkbox"]').attr('checked', true);
		}
		else
		{
			$('input[type="checkbox"]').attr('checked', false);
		}
	});
	$('#filtro').change(function(){
		if($(this).val() == 'grupo' || $(this).val() == 'facilitador')
		{
			$('.off').attr('disabled', true);
		}
		else
		{
			$('.off').attr('disabled', false);
		}
	});
	$('form').submit(function(){
		if($('#desde').val() == '' && $('#hasta').val() == '')
		{
			alert('Ingresa un rango de fechas');
			return false;
		}
		var flag = 0;
		$('input[type="checkbox"]').each(function(){
			if($(this).attr('checked'))
			{
				flag ++;
			}
		});
		if(flag ==0)
		{
			alert('Selecciona al menos un indicador');
			return false;
		}
		
	});
	$.datepicker.setDefaults( $.datepicker.regional[ "es" ] );
	var dates = $( "#desde, #hasta" ).datepicker({
		showWeek: true,
		dateFormat: 'yy-m-d',
		minDate : new Date(2010, 8, 1),
		maxDate : 0,
		firstDay: 1,
		defaultDate: 0,
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		onSelect: function( selectedDate ) {
			var option = this.id == "desde" ? "minDate" : "maxDate",
				instance = $( this ).data( "datepicker" ),
				date = $.datepicker.parseDate(
					instance.settings.dateFormat ||
					$.datepicker._defaults.dateFormat,
					selectedDate, instance.settings );
			dates.not( this ).datepicker( "option", option, date );
		}
	});
});
</script>
