<h1>Recursos List</h1>

<table>
	<thead>
		<tr>
			<th>Id</th>
			<th>Categoria recursos</th>
			<th>Unidad</th>
			<th>Codigo</th>
			<th>Nombre</th>
			<th>Activo</th>
			<th>Operador</th>
			<th>Created at</th>
			<th>Updated at</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($recursos as $recurso): ?>
		<tr>
			<td><a
				href="<?php echo url_for('recursos/edit?id='.$recurso->getId()) ?>"><?php echo $recurso->getId() ?>
			</a></td>
			<td><?php echo $recurso->getCategoriaRecursosId() ?></td>
			<td><?php echo $recurso->getUnidadId() ?></td>
			<td><?php echo $recurso->getCodigo() ?></td>
			<td><?php echo $recurso->getNombre() ?></td>
			<td><?php echo $recurso->getActivo() ?></td>
			<td><?php echo $recurso->getOperadorId() ?></td>
			<td><?php echo $recurso->getCreatedAt() ?></td>
			<td><?php echo $recurso->getUpdatedAt() ?></td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<a href="<?php echo url_for('recursos/new') ?>">New</a>
