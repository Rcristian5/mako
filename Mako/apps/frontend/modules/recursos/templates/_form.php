<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<form
	action="<?php echo url_for('recursos/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>"
	method="post"
	<?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
	<?php if (!$form->getObject()->isNew()): ?>
	<input type="hidden" name="sf_method" value="put" />
	<?php endif; ?>
	<table>
		<tbody>
			<?php echo $form->renderGlobalErrors() ?>
			<tr>
				<th><?php echo $form['codigo']->renderLabel('Código') ?></th>
				<td><?php echo $form['codigo']->renderError() ?> <?php echo $form['codigo']->render(array('requerido'=>1, 'filtro'=>'alfanumerico', 'size'=>10, 'maxlength'=>50)) ?>
				</td>
			</tr>
			<tr>
				<th><?php echo $form['nombre']->renderLabel() ?></th>
				<td><?php echo $form['nombre']->renderError() ?> <?php echo $form['nombre']->render(array('requerido'=>1, 'filtro'=>'alfanumerico', 'size'=>25, 'maxlength'=>50)) ?>
				</td>
			</tr>
			<tr>
				<th><?php echo $form['categoria_recursos_id']->renderLabel('Categoría') ?>
				</th>
				<td><?php echo $form['categoria_recursos_id']->renderError() ?> <?php echo $form['categoria_recursos_id'] ?>
				</td>
			</tr>
			<tr>
				<th><?php echo $form['unidad_id']->renderLabel() ?></th>
				<td><?php echo $form['unidad_id']->renderError() ?> <?php echo $form['unidad_id'] ?>
				</td>
			</tr>
			<tr>
				<?php if ($form->getObject()->isNew()): ?>
				<th><?php echo $form['cantidad']->renderLabel('Cantidad Inical') ?>
				</th>
				<td><?php echo $form['cantidad']->renderError() ?> <?php echo $form['cantidad']->render(array('requerido'=>1, 'filtro'=>'numerico', 'size'=>10, 'maxlength'=>50)) ?>
				</td>
				<?php else : ?>
				<th><?php echo $form['cantidad']->renderLabel('Cantidad') ?></th>
				<td><?php echo $form['cantidad']->renderError() ?> <?php echo $form['cantidad']->render(array('requerido'=>1, 'filtro'=>'numerico', 'size'=>10, 'maxlength'=>50, 'value'=>$cantidad)) ?>
				</td>
				<?php endif; ?>
			</tr>
		</tbody>
	</table>
	<?php echo $form->renderHiddenFields(false) ?>
	<p class="botones_finales">
		<?php if (!$form->getObject()->isNew()): ?>
		<!--&nbsp;<?php echo link_to('Delete', 'curso/delete?id='.$form->getObject()->getId(), array('method' => 'delete', 'confirm' => 'Are you sure?')) ?>-->
		<input type="button" value="Cancelar"
			onclick="location.href='/recursos/new'">
		<?php endif; ?>
		<input type="submit" value="Guardar"
			onclick="return comparaRequeridos();" />
	</p>
</form>
