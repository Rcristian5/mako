<?php slot('title', "Carga de Recursos"); ?>
<?php use_stylesheet('control_escolar/control_escolar.css') ?>
<?php use_stylesheet('jquery.windows.css') ?>
<?php use_stylesheet('datatables.css') ?>
<?php use_javascript('jquery.ui.js') ?>
<?php use_javascript('plugins/jquery.windows.js') ?>
<?php use_javascript('plugins/jquery.dataTables.js') ?>
<?php use_javascript('control_escolar/carga_recursos.js') ?>

<div id="windows-wrapper">
	<div id="columnaIzquierda" window-title="Añadir recurso"
		window-state="min">
		<?php if ($sf_user->hasFlash('notice')): ?>
		<div class="flash_notice ui-state-highlight ui-corner-all">
			<span style="float: left; margin-right: 0.3em;"
				class="ui-icon ui-icon-info"></span> <strong><?php echo $sf_user->getFlash('notice') ?>
			</strong>
		</div>
		<?php endif; ?>
		<h2>Añadir Recursos</h2>

		<?php include_partial('form', array('form' => $form)) ?>

	</div>

	<div id="columnaDerecha" window-title="Recursos registrados"
		window-state="max">

		<h2>Lista de Recursos</h2>
		<?php include_partial('detalleRecursos', array('detalle' => $detalle)) ?>

	</div>

</div>

