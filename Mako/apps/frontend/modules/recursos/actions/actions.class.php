<?php
/**
 * recursos actions.
 *
 * @package    mako
 * @subpackage recursos
 * @author     Bixit SA de CV
 * @version    SVN: $Id: actions.class.php,v 1.5 2010/03/17 20:06:51 david Exp $
 */
class recursosActions extends sfActions
{
    public function executeIndex(sfWebRequest $request) {
        $this->recursos = RecursoPeer::doSelect(new Criteria());
    }

    public function executeNew(sfWebRequest $request) {
        $this->form     = new RecursoInfraestructuraForm();
        $this->detalle  = RecursoPeer::listaRecursos();
    }

    public function executeCreate(sfWebRequest $request) {
        $this->forward404Unless($request->isMethod(sfRequest::POST));

        $this->form    = new recursoForm();
        $this->detalle = RecursoPeer::listaRecursos();

        $this->processForm($request, $this->form);

        $this->setTemplate('new');
    }

    public function executeEdit(sfWebRequest $request) {
        $this->forward404Unless($recurso       = RecursoPeer::retrieveByPk($request->getParameter('id')) , sprintf('Object recurso does not exist (%s).', $request->getParameter('id')));
        $this->form    = new recursoForm($recurso);
        $this->detalle = RecursoPeer::listaRecursos();
    }

    public function executeUpdate(sfWebRequest $request) {
        $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
        $this->forward404Unless($recurso       = RecursoPeer::retrieveByPk($request->getParameter('id')) , sprintf('Object recurso does not exist (%s).', $request->getParameter('id')));
        $this->form    = new recursoForm($recurso);
        $this->detalle = RecursoPeer::listaRecursos();
        $this->processForm($request, $this->form);

        $this->setTemplate('edit');
    }

    public function executeDelete(sfWebRequest $request) {
        $request->checkCSRFProtection();

        $this->forward404Unless($recurso = RecursoPeer::retrieveByPk($request->getParameter('id')) , sprintf('Object recurso does not exist (%s).', $request->getParameter('id')));
        $recurso->delete();

        $this->redirect('recursos/index');
    }

    protected function processForm(sfWebRequest $request, sfForm $form) {
        $form->bind($request->getParameter($form->getName()) , $request->getFiles($form->getName()));

        if ($form->isValid()) {
            $cantidad = $form->getValue('cantidad');

            if ($form->getObject()->isNew()) {
                $flag     = true;
            } else {
                $flag     = false;
            }
            $form->getObject()->setOperadorId($this->getUser()->getAttribute('usuario')->getId());
            $recurso = $form->save();

            if ($flag) {
                $this->getUser()->setFlash('notice', sprintf('Recurso %s guardado', $recurso->getNombre()));
                RecursoPeer::guardaStock($recurso->getId() , $cantidad);
            } else {
                $this->getUser()->setFlash('notice', sprintf('Recurso %s editado', $recurso->getNombre()));
                RecursoPeer::actualizaStock($recurso->getId() , $cantidad);
            }

            $this->redirect('recursos/new');
        }
    }
}
