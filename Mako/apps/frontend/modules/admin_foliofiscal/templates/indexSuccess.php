<?php use_javascript('plugins/jquery.dataTables.js') ?>
<?php use_stylesheet('datatables.css') ?>

<script>

var lTFolioFiscals;
$(document).ready(function() {

   lTFolioFiscals = $('#lista-FolioFiscals').dataTable({
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": true,
        "bJQueryUI": true,
        "sPaginationType": "full_numbers",
        "aaSorting": [[1, 'asc']] 
     });
   
});

</script>

<div class="ui-widget-header ui-corner-top tsss" style="padding: 5px;">Administrador
	de folios fiscales</div>
<div class="ui-widget ui-corner-bottom sombra">
	<div class="ui-widget-content ui-corner-bottom" style="padding: 5px;">

		<?php if ($sf_user->hasFlash('ok')): ?>
		<div
			class="flash_ok ui-state-highlight ts sombra-delgada ui-corner-all"
			style="padding: 5px; margin-top: 10px; margin-bottom: 10px;">
			<span style="float: left; margin-right: 0.3em;"
				class="ui-icon ui-icon-info"></span> <strong><?php echo $sf_user->getFlash('ok') ?>
			</strong>
		</div>
		<?php endif; ?>

		<br /> <span style="float: left;" class="ui-icon ui-icon-circle-plus"></span>
		<a href="<?php echo url_for('admin_foliofiscal/new') ?>"> Agregar
			registro </a> <br /> <br />


		<table id="lista-FolioFiscals" style="width: 100%;">
			<thead>
				<tr>
					<th><span class="ui-icon  ui-icon-pencil"></span>
					</th>
					<th>Centro</th>
					<th>Serie</th>
					<th>Núm. aprobación</th>
					<th>Año aprobación</th>
					<th>Serie certificado</th>
					<th>Folio inicial</th>
					<th>Folio final</th>
					<th>Folio actual</th>
					<th>Activo</th>
					<th>Num certificado</th>
					<th>Creado</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($FolioFiscals as $FolioFiscal): ?>
				<tr>
					<td><a
						href="<?php echo url_for('admin_foliofiscal/edit?id='.$FolioFiscal->getId()) ?>">
							<span class="ui-icon  ui-icon-pencil"></span>
					</a>
					</td>
					<td><?php echo $FolioFiscal->getCentro()->getAlias() ?></td>
					<td><?php echo $FolioFiscal->getSerie() ?></td>
					<td><?php echo $FolioFiscal->getNoAprobacion() ?></td>
					<td><?php echo $FolioFiscal->getAnoAprobacion() ?></td>
					<td><?php echo $FolioFiscal->getSerieCertificado() ?></td>
					<td><?php echo $FolioFiscal->getFolioInicial() ?></td>
					<td><?php echo $FolioFiscal->getFolioFinal() ?></td>
					<td><?php echo $FolioFiscal->getFolioActual() ?></td>
					<td><?php echo $FolioFiscal->getActivo() ? 'SI':'NO' ?></td>
					<td><?php echo $FolioFiscal->getNumCertificado() ?></td>
					<td><?php echo $FolioFiscal->getCreatedAt() ?></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>


	</div>
</div>

