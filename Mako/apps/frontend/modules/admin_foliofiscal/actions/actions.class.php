<?php

/**
 * admin_foliofiscal actions.
 *
 * @package    mako
 * @subpackage admin_foliofiscal
 * @author     Bixit SA de CV
 * @version    SVN: $Id: actions.class.php,v 1.1 2010/08/31 00:54:24 eorozco Exp $
 */
class admin_foliofiscalActions extends sfActions
{
	public function executeIndex(sfWebRequest $request)
	{
		$this->FolioFiscals = FolioFiscalPeer::doSelect(new Criteria());
	}

	public function executeNew(sfWebRequest $request)
	{
		$this->form = new FolioFiscalForm();
	}

	public function executeCreate(sfWebRequest $request)
	{
		$this->forward404Unless($request->isMethod(sfRequest::POST));

		$this->form = new FolioFiscalForm();

		$this->processForm($request, $this->form);

		$this->setTemplate('new');
	}

	public function executeEdit(sfWebRequest $request)
	{
		$this->forward404Unless($FolioFiscal = FolioFiscalPeer::retrieveByPk($request->getParameter('id')), sprintf('Object FolioFiscal does not exist (%s).', $request->getParameter('id')));
		$this->form = new FolioFiscalForm($FolioFiscal);
	}

	public function executeUpdate(sfWebRequest $request)
	{
		$this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
		$this->forward404Unless($FolioFiscal = FolioFiscalPeer::retrieveByPk($request->getParameter('id')), sprintf('Object FolioFiscal does not exist (%s).', $request->getParameter('id')));
		$this->form = new FolioFiscalForm($FolioFiscal);

		$this->processForm($request, $this->form);

		$this->setTemplate('edit');
	}

	public function executeDelete(sfWebRequest $request)
	{
		$request->checkCSRFProtection();

		$this->forward404Unless($FolioFiscal = FolioFiscalPeer::retrieveByPk($request->getParameter('id')), sprintf('Object FolioFiscal does not exist (%s).', $request->getParameter('id')));
		$FolioFiscal->delete();

		$this->redirect('admin_foliofiscal/index');
	}

	protected function processForm(sfWebRequest $request, sfForm $form)
	{
		$form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
		if ($form->isValid())
		{
			$FolioFiscal = $form->save();


	  $this->getUser()->setFlash('ok', 'Se ha guardado correctamente el registro');



	  $this->redirect('admin_foliofiscal/index?id='.$FolioFiscal->getId());
		}
	}
}
