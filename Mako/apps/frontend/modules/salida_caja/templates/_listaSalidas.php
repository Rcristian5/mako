<?php use_javascript('plugins/jquery.dataTables.js') ?>
<?php use_javascript('jquery.ui.js') ?>
<?php use_stylesheet('datatables.css') ?>
<script>
var lsT;
$(document).ready(function() {

   lsT = $('#lista-salidas').dataTable({
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
        "bJQueryUI": true,
        "sPaginationType": "full_numbers"
     });
   
});
</script>

<div class="ui-widget sombra">
	<div class="ui-widget-content ui-corner-all ts" style="padding: 5px;">

		<table width="100%" id="lista-salidas">
			<thead>
				<tr>
					<th>Centro</th>
					<th>Operador</th>
					<th>Monto($)</th>
					<th>Tipo</th>
					<th>Folio docto aval</th>
					<th>Folio</th>
					<th>Ip caja</th>
					<th>Fecha salida</th>
					<th>Observaciones</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($Salidas as $Salida): 
					$centro = (array) $Salida->getCentro();
					if ($centro){
				?>
				<tr id="salida_<?php echo $Salida->getId() ?>">
					<td><?php echo $Salida->getCentro()->getNombre() ?></td>
					<td><?php echo $Salida->getUsuario() ?></td>
					<td align="right"><?php echo $Salida->getMonto() ?></td>
					<td><?php echo $Salida->getTipoSalida() ?></td>
					<td><?php echo $Salida->getFolioDoctoAval() ?></td>
					<td align="right"><?php echo sprintf('%05d',$Salida->getFolio()) ?>
					</td>
					<td align="center"><?php echo $Salida->getIpCaja() ?></td>
					<td align="center"><?php echo $Salida->getCreatedAt() ?></td>
					<td><?php echo $Salida->getObservaciones() ?></td>
				</tr>
				<?php }
				endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
