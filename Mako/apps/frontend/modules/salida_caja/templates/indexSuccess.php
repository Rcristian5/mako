<?php 
/*
 * Componentes Bixit
*
* Copyright (c) 2009-2011 Bixit SA de CV
* Bajo Licencia dual MIT (http://www.bixit.mx/legal/MIT-LICENSE.txt)
* y GPL (http://www.bixit.mx/legal/GPL-LICENSE.txt).
*
*/
?>
<?php use_javascript('jquery.ui.js') ?>
<?php use_javascript('plugins/jquery.dataTables.js') ?>
<?php use_javascript('plugins/dataTables/TableTools.js') ?>
<?php use_javascript('plugins/dataTables/ZeroClipboard.js') ?>
<?php use_javascript('plugins/dataTables/datatables.orden-decimal.js') ?>
<?php use_stylesheet('TableTools.css') ?>
<?php use_stylesheet('datatables.css') ?>

<script>
var lsC;
$(document).ready(function() {


	TableToolsInit.oFeatures = {
			"bCsv": true,
			"bXls": true,
			"bCopy": true,
			"bPrint": false
		};
	
   lsT = $('#lista-cortes').dataTable({
	   "bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": true,
       "bJQueryUI": true,
       "sPaginationType": "full_numbers",
       "aaSorting": [],
       "sDom": 'T<"fg-toolbar ui-widget-header ui-corner-tl ui-corner-tr ui-helper-clearfix"lfr>t<"fg-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix"ip>'

     });

   
});
</script>

<?php use_stylesheet('./salida_caja/indexSuccess.css')?>

<div class="ui-widget-header ui-corner-top tsss" style="padding: 5px;">Salidas
	Caja</div>

<div class="sombra ui-corner-bottom ">

	<div class="ui-widget-content ui-corner-bottom ts"
		style="padding: 5px;">

		<table width="100%" id="lista-cortes">
			<thead>
				<tr>
					<th>Centro</th>
					<th>Operador</th>
					<th>Monto</th>
					<th>Tipo</th>
					<th>Folio</th>
					<th>Ip caja</th>
					<th>Fecha</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($Salidas as $Salida): ?>
				<tr>
					<td><?php echo $Salida->getCentro()->getAliasReporte() ?></td>
					<td><?php echo $Salida->getUsuario()?></td>
					<td align="right"><?php echo $Salida->getMonto() ?></td>
					<td><?php echo $Salida->getTipoSalida() ?></td>
					<td><?php echo $Salida->getFolio() ?></td>
					<td><?php echo substr(strrchr($Salida->getIpCaja(), "."), 1); ?></td>
					<td><?php echo $Salida->getCreatedAt() ?></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

	</div>
</div>
