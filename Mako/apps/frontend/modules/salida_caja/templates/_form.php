<?php
/*
 * Componentes Bixit
*
* Copyright (c) 2009-2011 Bixit SA de CV
* Bajo Licencia dual MIT (http://www.bixit.mx/legal/MIT-LICENSE.txt)
* y GPL (http://www.bixit.mx/legal/GPL-LICENSE.txt).
*
*/
?>
<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<?php use_javascript('jquery.ui.js') ?>

<script type="text/javascript">
    // Reingenieria Mako C. ID 45. Responsable: FE. Fecha: 10-04-2014. Descripción del cambio: Se pone por default 'Deposito a cofre'
    $(document).ready(function(){

      $('#salida_tipo_id option[value="2"]').attr("selected","selected");

    });
    // Fecha: 10-04-2014 Fin

// Reingenieria Mako C. ID 13. Responsable: FE. Fecha: 15-01-2014. Descripción del cambio: Se añade funcionalidad 'Salida de caja' en cero
function valida() {
	if(requeridos()) {
		if($("#salida_monto").val()==0){
                                                if($("#salida_tipo_id").val()!=4){
                                                    alert("Opción 'tipo', no valido");
                                                }else{
                                                    var confirma = confirm("¿Desea realizar una salida en ceros?")
                                                    if (confirma){
                                                    	    ValidarSalidas();                                                           
                                                            //$('#enviar').hide();
                                                            //return true;
                                                    }else{
                                                            return false;
                                                    }
                                                }
		}else{
                                                if($("#salida_tipo_id").val()==4){
                                                    alert("Opción 'tipo', no valido");
                                                }else{
                                                    var confirma = confirm("Confirmar salida por $"+$("#salida_monto").val())
                                                    if (confirma){
                                                            submitForm()
                                                            //$('#enviar').hide();
                                                            //return true;
                                                    }else{
                                                            return false;
                                                    }
                                                }
		}
	}
	else {
		return false;
	}
}

/* Validacion salidas de caja anteriores
	10/11/2014
	Responsable: IMB
*/
function ValidarSalidas(){	
           $.ajax({
                    url : "<?php echo url_for('salida_caja/validarsalidascajas') ?>",                    
            		dataType : 'json',                  
                    success: function(data)
                    {                	   
                	   	if (typeof data.folio != 'undefined') {
                	   	 console.log('data: ' + data.folio);                 		
                	   	   alert("Existen ventas pendientes por procesar");                	   	     
                	   	  	 //window.location.href= 'new';
                	   	  	 $(location).attr('href','./guiAuth');                	   	  	 
            		   }else{

            		   	     submitForm();
            		   	     
            		   }
                    },
                    error: function(data)
                    {
                        console.error(data);
                    }
                });
}


function submitForm(){
	
	var values = $('#formSalida').serialize();
	$.ajax({
		url : "<?php echo url_for('salida_caja/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>",
		data: values,
		type: "POST",
		beforeSend : function(){

		},
		success: function(data) {
		//	$(location).attr('href','./guiAuth');
			$('#enviar').hide();
			$('.enviar').append('REGISTRADNO SALIDA<img src="/imgs/ajax-circle.gif" alt="cargando…"/>');
			$('.carga').html('REGISTRADNO SALIDA<img src="/imgs/ajax-circle.gif" alt="cargando…"/>');
		},
		complete: function(data){
			$(location).attr('href','./guiAuth');
		}
	});
}




// Fecha: 15-01-2014. Fin
</script>

<div class="ui-widget-header ui-corner-top tsss" style="padding: 5px;">Salida
	de caja</div>
<div class="ui-widget ui-corner-all sombra">
	<div class="ui-widget-content ui-corner-bottom" style="padding: 15px;">


		<?php if ($sf_user->hasFlash('ok')): ?>
		<div
			class="flash_ok ui-state-highlight ts sombra-delgada ui-corner-all"
			style="padding: 5px; margin-top: 10px; margin-bottom: 10px;">
			<span style="float: left; margin-right: 0.3em;"
				class="ui-icon ui-icon-info"></span> <strong><?php echo $sf_user->getFlash('ok') ?>
			</strong>
		</div>
		<?php endif; ?>

		<!-- Reingenieria Mako C. ID 13. Responsable: FE. Fecha: 15-01-2014. Descripción del cambio: Se añade funcionalidad 'Salida de caja' en cero. -->
		<form id='formSalida' action="" method="post"
		<?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
			<?php if (!$form->getObject()->isNew()): ?>
			<input type="hidden" name="sf_method" value="put" />
			<?php endif; ?>
			<table>
				<tfoot>
					<tr>
						<td colspan="2"><input type="button" id="enviar" value="Guardar"
							onclick="return valida();" />
						</td>
						<td class="carga"></td>
					</tr>
				</tfoot>
				<tbody>
					<?php echo $form ?>
				</tbody>
			</table>
		</form>
		<!-- Fecha: 15-01-2014. Fin-->
	</div>
</div>
