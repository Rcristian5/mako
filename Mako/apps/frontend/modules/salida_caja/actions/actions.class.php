<?php
/*
 * Componentes Bixit
*
* Copyright (c) 2009-2011 Bixit SA de CV
* Bajo Licencia dual MIT (http://www.bixit.mx/legal/MIT-LICENSE.txt)
* y GPL (http://www.bixit.mx/legal/GPL-LICENSE.txt).
*
*/
/**
 * salida_caja actions.
 *
 * @package    bixit
 * @subpackage salida_caja
 * @author     Bixit SA de CV
 * @version    SVN: $Id: actions.class.php,v 1.9 2011/03/04 18:14:26 eorozco Exp $
 */
class salida_cajaActions extends sfActions
{

	public function executeIndex(sfWebRequest $request)
	{
		$c = new Criteria();
		$c->addDescendingOrderByColumn(SalidaPeer::CREATED_AT);
		 
		//local
		if(sfConfig::get('app_centro_actual_id') != sfConfig::get('app_central_id'))
		{
			$c->add(SalidaPeer::CENTRO_ID,sfConfig::get('app_centro_actual_id'),Criteria::EQUAL);
		}
	 //central
	 else
	 {
	 	//$c->add(SocioPeer::CENTRO_ID,$fecha_ini,Criteria::GREATER_EQUAL);
	 }

	 $this->Salidas = SalidaPeer::doSelect($c);
	}

	public function executeNew(sfWebRequest $request)
	{

		if(!$this->getUser()->hasFlash('autorizado'))
		{
			$this->getUser()->setFlash('error', 'Los datos proporcionados no corresponden a un usuario autorizado. Vuelva a intentar.');
			$this->redirect('salida_caja/guiAuth');
		}
		 
		$this->form = new SalidaForm();
		$c = new Criteria();
		$c->addDescendingOrderByColumn(SalidaPeer::ID);
		if(sfConfig::get('app_centro_actual_id') != sfConfig::get('app_central_id'))
		{
			$c->add(SalidaPeer::CENTRO_ID,sfConfig::get('app_centro_actual_id'));
			$ip_actual = Comun::ipReal();
			$c->add(SalidaPeer::IP_CAJA, $ip_actual);
			 
			//TODO: también se debe meter un rango de fecha a traer y traer más con un paginador
		}

		$this->Salidas = SalidaPeer::doSelect($c);
	}

	public function executeCreate(sfWebRequest $request)
	{

		$this->forward404Unless($request->isMethod(sfRequest::POST));

		$this->form = new SalidaForm();

		$this->processForm($request, $this->form);

		$this->setTemplate('new');
	}

	/**
	 * Pantalla de autorización de la operación de salida.
	 * @param sfWebRequest $request
	 * @return void
	 */
	public function executeGuiAuth(sfWebRequest $request)
	{
		//esta simplemente lleva a la plantilla guiAuthSuccess.php
	}

	/**
	 * Busca los datos ingresados en la forma de autorización en la BD
	 * @param sfWebRequest $request
	 * @return void
	 */
	public function executeAuth(sfWebRequest $request)
	{
		$this->forward404Unless($request->isMethod(sfRequest::POST));
		$usuario = $request->getParameter('usuario');
		$clave = $request->getParameter('clave');
		 
		//Roles autorizadores
		$autorizados = array(
				sfConfig::get('app_rol_super_usuario_id'),
				sfConfig::get('app_rol_supervisor_id'),
				sfConfig::get('app_rol_facilitador_encargado_id'),
				sfConfig::get('app_rol_facilitador_id'),
				sfConfig::get('app_rol_administrador_operaciones_id')
		);
		$c = new Criteria();
		$c->add(UsuarioPeer::USUARIO,$usuario);
		$c->add(UsuarioPeer::CLAVE,$clave);
		$c->add(UsuarioPeer::ACTIVO,true);
		 
		$c->add(UsuarioPeer::ROL_ID,$autorizados,Criteria::IN);
		 
		//TODO: validar centro autorizado para este usuario
		$usuario = UsuarioPeer::doSelectOne($c);
		if($usuario == null)
		{
			// Reingenieria Mako C. ID 58. Responsable:  FE. Fecha: 07-04-2014.  Descripción del cambio: Se corrigió ortografía en el mensaje
			$this->getUser()->setFlash('error', 'Los datos proporcionados no corresponden a un usuario autorizado. Vuelva a intentar.');
			$this->redirect('salida_caja/guiAuth');
			// Fecha: 07-04-2014 Fin
		}
		else
		{
			$this->getUser()->setFlash('autorizado', 'ok');
			$this->redirect('salida_caja/new');
		}
	}

	/**
	 * Accion para validar salidas de caja anteriores
	 * @param sfWebRequest $request
	 */
	public function executeValidarsalidascajas(sfWebRequest $request){
		/* Validacion salidas de caja anteriores
			10/11/2014
		Responsable: IMB
		*/
      if ($request->isXmlHttpRequest())
		{

			//$hm1 = time()-(24*60*60);
			$hoy = date("Y-m-d H:i:s");
  			//$ayer = date("Y-m-d",$hm1);
  			$conn = Propel::getConnection();
  			//$fI = $ayer." 00:00:00";
  			//$fII = $ayer." 23:59:59";
  			
  			//error_log('AYER ===> '.$ayer);			
  			error_log('HOY ===> '.$hoy);	

        	$s = new Criteria();        	 
			//$s->add(SalidaPeer::CREATED_AT,$ayer,Criteria::EQUAL);        	
			//$s->add(SalidaPeer::CREATED_AT, SalidaPeer::CREATED_AT." BETWEEN '".$ayer." 00:00:00' AND '".$ayer." 23:59:59'", Criteria::CUSTOM); 
			//$s->add(CortePeer::CREATED_AT, $hoy, Criteria::LESS_EQUAL);
			//$s->add(CortePeer::CREATED_AT, '(SELECT MAX('.CortePeer::CREATED_AT.') FROM corte)', Criteria::CUSTOM);
			$s->addDescendingOrderByColumn(CortePeer::CREATED_AT);	
			$s->add(CortePeer::CREATED_AT,null,Criteria::ISNOTNULL);					
			//$c->add(SalidaPeer::CREATED_AT,$fI,Criteria::GREATER_THAN);
			//$c->add(SalidaPeer::CREATED_AT,$fII,Criteria::LESS_THAN);
        	$cp = CortePeer::doSelectOne($s);
        	error_log('========================================================SALIDA]=======================================================');
        	error_log( print_r( $cp, true ) );

        	 if (isset($cp)) {

        			$fechabusqueda = $cp->getCreatedAt();

        			error_log('========================================================FECHA BUSQUEDA=======================================================');

        			error_log('FECHA BUSQUEDA ===> '.$fechabusqueda );
				
				}

        	$c = new Criteria();        	 
			//$c->add(OrdenPeer::CREATED_AT,$ayer,Criteria::EQUAL);
			//$c->add(OrdenPeer::CREATED_AT, OrdenPeer::CREATED_AT." BETWEEN '".$ayer." 00:00:00' AND '".$ayer." 23:59:59'", Criteria::CUSTOM);       				
			//$c->add(OrdenPeer::CREATED_AT,$fechabusqueda,Criteria::GREATER_EQUAL);
			$c->add(OrdenPeer::CREATED_AT,$fechabusqueda,Criteria::GREATER_THAN);			
			//$c->add(OrdenPeer::CREATED_AT,$fI,Criteria::GREATER_THAN);
			//$c->add(OrdenPeer::CREATED_AT,$fII,Criteria::LESS_THAN);
        	$sp = OrdenPeer::doSelectOne($c);
        	error_log('========================================================ORDEN]=======================================================');
        	error_log( print_r( $sp, true ) );


        	$arreglo=array();

            //if(!isset($cp)) {
               if (isset($sp)) {

        			$arreglo = array('folio'=> $sp->getId());

        			error_log('========================================================ARREGLO DENTRO]=======================================================');

        			error_log( print_r( $arreglo, true ) );
				
				}

			//}

			error_log('========================================================ARREGLO AFUERA]=======================================================');

        			error_log( print_r( $arreglo, true ) );
        			

			$this->getResponse()->setHttpHeader('Content-Type', 'application/json; charset=utf-8');
			return $this->renderText(json_encode($arreglo));
		}

	}

	protected function processForm(sfWebRequest $request, sfForm $form)
	{
		$form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
		if ($form->isValid())
		{


			//Asociamos el operador que esta dando salida.
			$cajero = $this->getUser()->getAttribute('usuario');
			$form->getObject()->setUsuario($cajero);

			//Si es una salida nueva
			$nuevo = false;
			if($form->getObject()->isNew())
			{
				$nuevo = true;
				if($form->getObject()->getCentroId() == null)
					$form->getObject()->setCentroId(sfConfig::get('app_centro_actual_id'));
				$form->getObject()->setFolio(FolioSalidaPeer::folio($form->getObject()->getCentroId() ));
			}
			 
			$form->getObject()->setIpCaja(Comun::ipReal());

			$aSalida = $request->getPostParameter('salida');
			$monto = $aSalida['monto']*(-1);


					//Generamos la orden de salida
			$orden = new Orden();

			$orden->setTipoId(sfConfig::get('app_orden_salida_id'));
			$orden->setCentroId($form->getObject()->getCentroId());
			$orden->setSocioId(null);
			$orden->setIpCaja($form->getObject()->getIpCaja());
			$orden->setTotal($monto);
			$orden->setUsuario($form->getObject()->getUsuario());
			$orden->setFormaPagoId(sfConfig::get('app_pago_efectivo_id'));
			//Folio
			$orden->setFolio($form->getObject()->getFolio());

			$form->getObject()->setOrden($orden);
			$Salida = $form->save();
			 
			if($nuevo)
			{
				//se incrementa el folio de salida
				FolioSalidaPeer::folioSiguiente($form->getObject()->getCentroId());
			}

			$this->getUser()->setFlash('ok', sprintf('Se ha reportado la salida por $ %s con folio %05s correctamente',$Salida->getMonto(),$Salida->getFolio()));
			return  sprintf('Se ha reportado la salida por $ %s con folio %05s correctamente',$Salida->getMonto(),$Salida->getFolio());
			$this->redirect('salida_caja/guiAuth');
		}
	}
}
