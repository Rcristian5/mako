<?php

/**
 * registro actions.
 *
 * @package    mako
 * @subpackage registro
 * @author     Bixit SA de CV
 * @version    SVN: $Id: actions.class.php,v 1.20 2011-10-06 16:59:15 eorozco Exp $
 */
class reporteActions extends sfActions
{
	public function executeIndex(sfWebRequest $request)
	{

		$ctx = sfContext::getInstance();
		$ctx->getConfiguration()->loadHelpers('Reportes');

		$semana = date('W', strtotime($request->getParameter('fecha')));
		$anio = date('Y', strtotime($request->getParameter('fecha')));
		$fecha = $request->getParameter('fecha');

		$rango = $request->getParameter('rango');

		if($rango=='semana' || $rango==''){
			if($fecha == NULL){
				$semana = date('W', strtotime("last monday"));
				$anio = date('Y', strtotime("last monday"));
				//Find out the date of weeknumber where day is Monday
				$fecha =  date('d-m-Y', strtotime("last monday"));

			}

			$fechaTime=  mktime(0, 0, 0, $fecha{3}.$fecha{4},$fecha{0}.$fecha{1}, $anio);
			$fechaFin =  date('d-m-Y', strtotime('+6 days', $fechaTime));

			$anterior =  date('d-m-Y' ,strtotime('-1 week', $fechaTime));
			$siguiente = date('d-m-Y' , strtotime('+1 week', $fechaTime));
				
			//si la proxima fecha es mayor al ultimo lunes, se queda en la fecha de hoy.
			if(date('U',$siguiente) > date('U', strtotime('-1 week',strtotime("last monday")))) $siguiente = $fecha;

			$this->chkSemana = 'checked = "checked"';
			$tituloReporte = 'Semana';
			//mes
		}else{
			if($fecha == NULL){
				$semana = date('W');
				$anio = date('Y');
				$fecha =  date('01-m-Y');
			}
			$fechaTime = mktime(0, 0, 0, $fecha{3}.$fecha{4},$fecha{0}.$fecha{1}, $anio);
			if($rango=='mes'){
				$fechaFin =  date('t-m-Y', $fechaTime);

				$anterior =  date('d-m-Y' ,strtotime('-1 month', $fechaTime));
				$siguiente = date('d-m-Y' , strtotime('+1 month', $fechaTime));
				$this->chkMes = 'checked = "checked"';
				$tituloReporte = 'Mensual';
				//bimestre
			}elseif($rango=='bimestre'){
				$fechaFin =  date('t-m-Y',strtotime('+1 month', $fechaTime));

				$anterior =  date('d-m-Y' ,strtotime('-2 month', $fechaTime));
				$siguiente = date('d-m-Y' , strtotime('+2 month', $fechaTime));

				$this->chkBimestre = 'checked = "checked"';
				$tituloReporte = 'Bimestral';
				//Trimestre
			}elseif($rango=='trimestre'){
				$fechaFin =  date('t-m-Y',strtotime('+2 month', $fechaTime));
					
				$anterior =  date('d-m-Y' ,strtotime('-3 month', $fechaTime));
				$siguiente = date('d-m-Y' , strtotime('+3 month', $fechaTime));
				$this->chkTrimestre = 'checked = "checked"';
				$tituloReporte = 'Trimestral';
				//semestre
			}elseif($rango=='semestre'){
				$fechaFin =  date('t-m-Y',strtotime('+5 month', $fechaTime));

				$anterior =  date('d-m-Y' ,strtotime('-6 month', $fechaTime));
				$siguiente = date('d-m-Y' , strtotime('+6 month', $fechaTime));
				$this->chkSemestre = 'checked = "checked"';
				$tituloReporte = 'Semestral';
				//Año
			}elseif($rango=='anio'){
				$fechaFin =  date('t-m-Y',strtotime('+11 month', $fechaTime));

				$anterior =  date('d-m-Y' ,strtotime('-1 year', $fechaTime));
				$siguiente = date('d-m-Y' , strtotime('+1 year', $fechaTime));
				$this->chkAnio = 'checked = "checked"';
				$tituloReporte = 'Anual';
			}
		}
			
		//Ranking RIA
		$this->anterior = $anterior;
		$this->siguiente = $siguiente;
		$this->rango = $rango;
		$this->msg = 'Reporte '.$tituloReporte.' del ' . $fecha .' al '. $fechaFin;

		if(sfConfig::get('app_centro_actual_id') != sfConfig::get('app_central_id'))
		{
				
			$r = ReporteRankingLocal($semana, $anio, null, $fecha, $fechaFin);
			$this->reporteRanking = tablaReporteRankinLocal($r);
		}
		else
		{
			$r = ReporteRankingCentral($semana, $anio, $fecha, $fechaFin);
			$this->reporteRanking = tablaReporteRankingCentral($r);
		}

		$inicio = date('Y-m-d', strtotime($request->getParameter('inicio')));
		$fin = date('Y-m-d', strtotime($request->getParameter('fin')));

		if($inicio == NULL){
			$inicio = date('Y-m-d', strtotime("today"));
			$fin = date('Y-m-d', strtotime("tomorrow"));
				
		}

		/*
		 //Control Laboral
		$c = new Criteria();
		$c->add(ControlLaboralPeer::CREATED_AT,$inicio,Criteria::GREATER_EQUAL);
		$c->addAnd(ControlLaboralPeer::CREATED_AT,$fin,Criteria::LESS_EQUAL);
		$c->addAnd(ControlLaboralPeer::FIN,null,Criteria::NOT_EQUAL);
		$c->addAscendingOrderByColumn(ControlLaboralPeer::INICIO);
		$controlLaboral = ControlLaboralPeer::doSelect($c);

		$this->controlLaboral=$controlLaboral;
		*/
	}


	/**
	 * Regresa el partial con los detalles del socio y su historial de compras y de sesiones.
	 * @param sfWebRequest $request
	 * @return unknown_type
	 */
	public function executeVerDetalleRankingCentro(sfWebRequest $request)
	{
		$ctx = sfContext::getInstance();
		$ctx->getConfiguration()->loadHelpers('Reportes');

		$centroId = $request->getParameter('centroId');
		$fechaInicio = $request->getParameter('fechaInicio');
		$fechaFin = $request->getParameter('fechaFin');

		$r = ReporteRankingLocal($request->getParameter('semana'),$request->getParameter('anio'),$centroId,$fechaInicio,$fechaFin);
		$detalle = tablaReporteRankinLocal($r);

		$centro = CentroPeer::retrieveByPK($centroId);
		$centroNombre=$centro->getNombre();

		return $this->renderPartial('reporte/verDetalleCentro',array('reporteRanking' => $detalle,'msg' => 'Reporte para el centro '. $centroNombre));
	}



	/**
	 * Regresa el partial con los detalles del socio y su historial de compras y de sesiones.
	 * @param sfWebRequest $request
	 * @return unknown_type
	 */
	public function executeVerDetalle(sfWebRequest $request)
	{
		$ctx = sfContext::getInstance();
		$ctx->getConfiguration()->loadHelpers('Reportes');

		$Detalle =  estadisticaDetalle($request->getParameter('tipoDetalle'), $request->getParameter('idProducto'));

		return $this->renderPartial('reporte/verDetalle',array('Detalle'=>$Detalle));
	}



	/**
	 * Regresa el partial con los detalles del socio y su historial de compras y de sesiones.
	 * @param sfWebRequest $request
	 * @return unknown_type
	 */
	public function executeVerDetalleCurso(sfWebRequest $request)
	{
		$ctx = sfContext::getInstance();
		$ctx->getConfiguration()->loadHelpers('Reportes');

		$Detalle =  estadisticaDetalleCurso($request->getParameter('tipoDetalle'));

		return $this->renderPartial('reporte/verDetalleCurso',array('Detalle'=>$Detalle));
	}



	/**
	 * Regresa el partial con los detalles del socio y su historial de compras y de sesiones.
	 * @param sfWebRequest $request
	 * @return unknown_type
	 */
	public function executeVerDetalleCafeteria(sfWebRequest $request)
	{
		$ctx = sfContext::getInstance();
		$ctx->getConfiguration()->loadHelpers('Reportes');

		$Detalle =  estadisticaDetalleCafeteria($request->getParameter('tipoDetalle'));

		return $this->renderPartial('reporte/verDetalleCafeteria',array('Detalle'=>$Detalle));
	}

	/**
	 * Regresa el partial con los detalles del socio y su historial de compras y de sesiones.
	 * @param sfWebRequest $request
	 * @return unknown_type
	 */
	public function executeVerDetalleDetalleCafeteria(sfWebRequest $request)
	{
		$ctx = sfContext::getInstance();
		$ctx->getConfiguration()->loadHelpers('Reportes');

		$Detalle =  estadisticaDetalleDetalleCafeteria($request->getParameter('tipoDetalle'), $request->getParameter('idProducto'));

		return $this->renderPartial('reporte/verDetalleDetalleCafeteria',array('RangoFechas'=>$Detalle['rango'],'Productos'=>$Detalle[getEnv('prod')]));
	}


	/**
	 * Dado un rango de fechas regresa el resumen de ventas por categoria de producto que se ha realizado en el rango solicitado.
	 *
	 * @WSMethod(webservice='MakoApi')
	 * @param string $fecha_inicio Fecha de inicio de la consulta
	 * @param string $fecha_fin Fecha de fin de la consulta
	 *
	 * @return string Regresa un arreglo serializado del tipo $res[centro_id][categoria_id]=$total
	 */
	public function executeGetResumenVentas(sfWebRequest $request)
	{
		$configuration = ProjectConfiguration::getApplicationConfiguration( 'frontend', 'tolu2', true);
		$context = sfContext::createInstance($configuration);

		$centro_id = sfConfig::get('app_centro_actual_id');

		$fecha_inicio = pg_escape_string($request->getParameter('fecha_inicio'));
		$fecha_fin = pg_escape_string($request->getParameter('fecha_fin'));

		$res = OrdenPeer::resumenVentasPorCategoria($fecha_inicio,$fecha_fin);

		$this->result = serialize($res);

		if($res == null)
		{
			$me = $this->isSoapRequest() ? new SoapFault('Server', 'No hubo ventas para el rango solicitado.'):'Error al obtener el resumen del lado del servidor';
			throw $me;
			return sfView::ERROR;
				
		}
		else
		{
			return sfView::SUCCESS;
		}
	}

	/**
	 * Dado un rango de fechas regresa las ordenes detalle realizadas en el rango.
	 *
	 * @WSMethod(webservice='MakoApi')
	 * @param string $fecha_inicio Fecha de inicio de la consulta
	 * @param string $fecha_fin Fecha de fin de la consulta
	 *
	 * @return string Regresa un arreglo serializado del tipo $res[centro_id][]=$detalle_orden
	 */
	public function executeGetReporteDetalleOrdenes(sfWebRequest $request)
	{
		$configuration = ProjectConfiguration::getApplicationConfiguration( 'frontend', 'tolu2', true);
		$context = sfContext::createInstance($configuration);

		$centro_id = sfConfig::get('app_centro_actual_id');

		$fecha_inicio = pg_escape_string($request->getParameter('fecha_inicio'));
		$fecha_fin = pg_escape_string($request->getParameter('fecha_fin'));

		$res = OrdenPeer::reporteDetalleOrdenes($fecha_inicio,$fecha_fin);

		$this->result = serialize($res);

		if($res == null)
		{
			$me = $this->isSoapRequest() ? new SoapFault('Server', 'No existen ordenes de venta o cancelacion para el rango solicitado.'):'Error al obtener el resumen del lado del servidor';
			throw $me;
			return sfView::ERROR;
				
		}
		else
		{
			return sfView::SUCCESS;
		}
	}

	/**
	 * Regresa un arreglo con los registros de socios con movimientos el día de hoy.
	 *
	 * @WSMethod(webservice='MakoApi')
	 *
	 * @return string Regresa un arreglo serializado del tipo $res[]=$socio
	 */
	public function executeGetMovimientosSocios(sfWebRequest $request)
	{
		$configuration = ProjectConfiguration::getApplicationConfiguration( 'frontend', 'tolu2', true);
		$context = sfContext::createInstance($configuration);

		$fecha_ini = date("Y-m-d 00:00:00");
		$fecha_fin = date("Y-m-d 23:59:59");
		$c = new Criteria();
		$c->add(SocioPeer::UPDATED_AT,$fecha_ini,Criteria::GREATER_EQUAL);
		$c->addAnd(SocioPeer::CREATED_AT,$fecha_fin,Criteria::LESS_EQUAL);

		$socios = SocioPeer::doSelect($c);
		$res = array();
		foreach($socios as $socio)
		{
			$res[] = $socio->toArray();
		}
		$this->result = serialize($res);

		if(count($res) == 0)
		{
			$me = $this->isSoapRequest() ? new SoapFault('Server', 'No existen movimientos de socio para hoy.'):'No existen movimientos de socio para hoy';
			throw $me;
			return sfView::ERROR;
				
		}
		else
		{
			return sfView::SUCCESS;
		}

	}

	/**
	 * Dado un rango de fechas regresa un arreglo con los registros de socios registrados en esos dias.
	 *
	 * @WSMethod(webservice='MakoApi')
	 * @param string $fecha_ini Fecha de inicio de la consulta
	 * @param string $fecha_fin Fecha de fin de la consulta
	 *
	 * @return string Regresa un arreglo serializado del tipo $res[]=$socio
	 */
	public function executeGetRegistroSociosRango(sfWebRequest $request)
	{
		$configuration = ProjectConfiguration::getApplicationConfiguration( 'frontend', 'tolu2', true);
		$context = sfContext::createInstance($configuration);

		$centro_id = sfConfig::get('app_centro_actual_id');

		$fecha_ini = pg_escape_string($request->getParameter('fecha_ini'));
		$fecha_fin = pg_escape_string($request->getParameter('fecha_fin'));

		$c = new Criteria();

		$c->add(SocioPeer::CENTRO_ID,$centro_id);
		$c->add(SocioPeer::CREATED_AT,$fecha_ini,Criteria::GREATER_EQUAL);
		$c->addAnd(SocioPeer::CREATED_AT,$fecha_fin,Criteria::LESS_EQUAL);

		$socios = SocioPeer::doSelect($c);

		$res = array();
		foreach($socios as $socio)
		{
			$res[] = $socio->toArray();
		}
		$this->result = serialize($res);

		if(count($res) == 0)
		{
			$me = $this->isSoapRequest() ? new SoapFault('Server', 'No existen registros de socio para el rango de fechas dado.'):'No existen registros de socio para el rango de fechas dado';
			throw $me;
			return sfView::ERROR;
				
		}
		else
		{
			return sfView::SUCCESS;
		}

	}

	/**
	 * Regresa un arreglo serializado con el detalle de las sesiones del dia.
	 *
	 * @WSMethod(webservice='MakoApi')
	 *
	 * @return string Regresa un arreglo serializado del tipo $res[]=$sesion
	 */
	public function executeGetSesiones(sfWebRequest $request)
	{
		$configuration = ProjectConfiguration::getApplicationConfiguration( 'frontend', 'tolu2', true);
		$context = sfContext::createInstance($configuration);

		$centro_id = sfConfig::get('app_centro_actual_id');

		$fecha_ini = date("Y-m-d 00:00:00");
		$fecha_fin = date("Y-m-d 23:59:59");

		$sesiones = SesionHistoricoPeer::historialPorFecha($fecha_ini, $fecha_fin, $centro_id);

		$this->result = serialize($sesiones);

		if(count($sesiones) == 0)
		{
			$me = $this->isSoapRequest() ? new SoapFault('Server', 'No existen registros de sesion para este centro el dia de hoy.'):'No existen registros de sesion para este centro el dia de hoy';
			throw $me;
			return sfView::ERROR;
		}
		else
		{
			return sfView::SUCCESS;
		}
	}

	/**
	 * Regresa un arreglo serializado con el detalle de las sesiones del dia.
	 *
	 * @WSMethod(webservice='MakoApi')
	 * @param string $fecha_ini Fecha de inicio de la consulta
	 * @param string $fecha_fin Fecha de fin de la consulta
	 * @return string Regresa un arreglo serializado del tipo $res[]=$sesion
	 */
	public function executeGetSesionesRango(sfWebRequest $request)
	{
		$configuration = ProjectConfiguration::getApplicationConfiguration( 'frontend', 'tolu2', true);
		$context = sfContext::createInstance($configuration);

		$centro_id = sfConfig::get('app_centro_actual_id');
		$fecha_ini = pg_escape_string($request->getParameter('fecha_ini'));
		$fecha_fin = pg_escape_string($request->getParameter('fecha_fin'));

		$sesiones = SesionHistoricoPeer::historialPorFecha($fecha_ini, $fecha_fin, $centro_id);

		$this->result = serialize($sesiones);

		if(count($sesiones) == 0)
		{
			$me = $this->isSoapRequest() ? new SoapFault('Server', 'No existen registros de sesion para este centro el dia de hoy.'):'No existen registros de sesion para este centro el dia de hoy';
			throw $me;
			return sfView::ERROR;
		}
		else
		{
			return sfView::SUCCESS;
		}
	}

	/**
	 * Regresa un arreglo serializado con el detalle de las salidas en el rango de fechas.
	 *
	 * @WSMethod(webservice='MakoApi')
	 * @param string $fecha_ini Fecha de inicio de la consulta
	 * @param string $fecha_fin Fecha de fin de la consulta
	 * @return string Regresa un arreglo serializado del tipo $res[]=$sesion
	 */
	public function executeGetSalidasRango(sfWebRequest $request)
	{
		$configuration = ProjectConfiguration::getApplicationConfiguration( 'frontend', 'tolu2', true);
		$context = sfContext::createInstance($configuration);

		$centro_id = sfConfig::get('app_centro_actual_id');
		$fecha_ini = pg_escape_string($request->getParameter('fecha_ini'));
		$fecha_fin = pg_escape_string($request->getParameter('fecha_fin'));

		$salidas = SalidaPeer::getPorRango($fecha_ini, $fecha_fin, $centro_id);

		$this->result = serialize($salidas);

		if(count($salidas) == 0)
		{
			$me = $this->isSoapRequest() ? new SoapFault('Server', 'No existen registros para el rango solicitado.'):'No existen registros para el rango solicitado';
			throw $me;
			return sfView::ERROR;
		}
		else
		{
			return sfView::SUCCESS;
		}
	}

	/**
	 * Regresa un arreglo serializado con el detalle de las conciliaciones en el rango de fechas.
	 *
	 * @WSMethod(webservice='MakoApi')
	 * @param string $fecha_ini Fecha de inicio de la consulta
	 * @param string $fecha_fin Fecha de fin de la consulta
	 * @return string Regresa un arreglo serializado del tipo $res[]=$sesion
	 */
	public function executeGetConciliacionesRango(sfWebRequest $request)
	{
		$configuration = ProjectConfiguration::getApplicationConfiguration( 'frontend', 'tolu2', true);
		$context = sfContext::createInstance($configuration);

		$centro_id = sfConfig::get('app_centro_actual_id');
		$fecha_ini = pg_escape_string($request->getParameter('fecha_ini'));
		$fecha_fin = pg_escape_string($request->getParameter('fecha_fin'));

		$conciliaciones = ConciliacionPeer::getPorRango($fecha_ini, $fecha_fin, $centro_id);

		$this->result = serialize($conciliaciones);

		if(count($conciliaciones) == 0)
		{
			$me = $this->isSoapRequest() ? new SoapFault('Server', 'No existen registros para el rango solicitado.'):'No existen registros para el rango solicitado.';
			throw $me;
			return sfView::ERROR;
		}
		else
		{
			return sfView::SUCCESS;
		}
	}

	/**
	 * Regresa un arreglo serializado con las ordenes en el rango de fechas.
	 *
	 * @WSMethod(webservice='MakoApi')
	 * @param string $fecha_ini Fecha de inicio de la consulta
	 * @param string $fecha_fin Fecha de fin de la consulta
	 * @return string Regresa un arreglo serializado del tipo $res[]=$sesion
	 */
	public function executeGetOrdenesRango(sfWebRequest $request)
	{
		$configuration = ProjectConfiguration::getApplicationConfiguration( 'frontend', 'tolu2', true);
		$context = sfContext::createInstance($configuration);

		$centro_id = sfConfig::get('app_centro_actual_id');
		$fecha_ini = pg_escape_string($request->getParameter('fecha_ini'));
		$fecha_fin = pg_escape_string($request->getParameter('fecha_fin'));

		$ordenes = OrdenPeer::getReporteOrdenes($fecha_ini, $fecha_fin, $centro_id);

		$this->result = serialize($ordenes);

		if(count($ordenes) == 0)
		{
			$me = $this->isSoapRequest() ? new SoapFault('Server', 'No existen registros para el rango solicitado.'):'No existen registros para el rango solicitado.';
			throw $me;
			return sfView::ERROR;
		}
		else
		{
			return sfView::SUCCESS;
		}
	}

	/**
	 * Regresa un arreglo serializado con las registros de productos.
	 *
	 * @WSMethod(webservice='MakoApi')
	 * @param int $id Producto id, si es cero o nulo regresa todo
	 * @return string Regresa un arreglo serializado del tipo $res[]=$producto
	 */
	public function executeGetProductos(sfWebRequest $request)
	{
		$id = intval(pg_escape_string($request->getParameter('id')));
		$c = new Criteria();

		if($id > 0)
		{
			$c->add(ProductoPeer::ID,$id);
		}

		$Aproductos = array();
		$productos = ProductoPeer::doSelect($c);
		foreach($productos as $p)
		{
			$Aproductos[] = $p->toArray();
		}

		$this->result = serialize($Aproductos);

		if(count($Aproductos) == 0)
		{
			$me = $this->isSoapRequest() ? new SoapFault('Server', 'No existen registros para la consula de producto enviada.'):'No existen registros.';
			throw $me;
			return sfView::ERROR;
		}
		else
		{
			return sfView::SUCCESS;
		}
	}

	/**
	 * Regresa arreglo serializado con con los registros de la tabla reporte_metas para el rango de fechas dado.
	 * @WSMethod(webservice='MakoApi')
	 * @param string $fecha_ini Fecha de inicio de la consulta
	 * @param string $fecha_fin Fecha de fin de la consulta
	 * @return string Regresa un arreglo serializado del tipo $res[]=$reporte_metas
	 */
	public function executeGetReporteMetas($request)
	{
		$configuration = ProjectConfiguration::getApplicationConfiguration( 'frontend', 'tolu2', true);
		$context = sfContext::createInstance($configuration);

		$centro_id = sfConfig::get('app_centro_actual_id');
		$fecha_ini = pg_escape_string($request->getParameter('fecha_ini'));
		$fecha_fin = pg_escape_string($request->getParameter('fecha_fin'));

		$reporteMetas = ReporteMetasPeer::arregloReporteMetasPorRango($fecha_ini, $fecha_fin, $centro_id);

		$this->result = serialize($reporteMetas);

		if(count($reporteMetas) == 0)
		{
			$me = new SoapFault('Server', "No existen registros de metas para el rango de fechas consultadas $fecha_ini a $fecha_fin en el centro_id $centro_id.");
			throw $me;
			return sfView::ERROR;
		}
		else
		{
			return sfView::SUCCESS;
		}

	}

	/**
	 * Regresa arreglo serializado con con los registros de la tabla reporte_metas para el rango de fechas dado.
	 * @WSMethod(webservice='MakoApi')
	 * @param string $fecha_ini Fecha de inicio de la consulta
	 * @param string $fecha_fin Fecha de fin de la consulta
	 * @return string Regresa un arreglo serializado del tipo $res[]=$reporte_metas
	 */
	public function executeGetReporteVentasCategorias($request)
	{
		$configuration = ProjectConfiguration::getApplicationConfiguration( 'frontend', 'tolu2', true);
		$context = sfContext::createInstance($configuration);

		$centro_id = sfConfig::get('app_centro_actual_id');
		$fecha_ini = pg_escape_string($request->getParameter('fecha_ini'));
		$fecha_fin = pg_escape_string($request->getParameter('fecha_fin'));

		$reporte = ReporteVentasCategoriasPeer::arregloReporte($fecha_ini, $fecha_fin, $centro_id);

		$this->result = serialize($reporte);

		if(count($reporte) == 0)
		{
			$me = new SoapFault('Server', "[GetReporteVentasCategorias]: No existen registros para el rango de fechas consultadas $fecha_ini a $fecha_fin en el centro_id $centro_id.");
			throw $me;
			return sfView::ERROR;
		}
		else
		{
			return sfView::SUCCESS;
		}
	}

	/**
	 * Regresa arreglo serializado con con los registros de la tabla dias_operacion para el rango de fechas dado.
	 * @WSMethod(webservice='MakoApi')
	 * @param string $fecha_ini Fecha de inicio de la consulta
	 * @param string $fecha_fin Fecha de fin de la consulta
	 * @return string Regresa un arreglo serializado del tipo $res[]=$reporte_metas
	 */
	public function executeGetReporteDiasOperacion($request)
	{
		$configuration = ProjectConfiguration::getApplicationConfiguration( 'frontend', 'tolu2', true);
		$context = sfContext::createInstance($configuration);

		$centro_id = sfConfig::get('app_centro_actual_id');
		$fecha_ini = pg_escape_string($request->getParameter('fecha_ini'));
		$fecha_fin = pg_escape_string($request->getParameter('fecha_fin'));

		$reporte = DiasOperacionPeer::arregloReporte($fecha_ini, $fecha_fin, $centro_id);

		$this->result = serialize($reporte);

		if(count($reporte) == 0)
		{
			$me = new SoapFault('Server', "[GetReporteDiasOperacion]: No existen registros para el rango de fechas consultadas $fecha_ini a $fecha_fin en el centro_id $centro_id.");
			throw $me;
			return sfView::ERROR;
		}
		else
		{
			return sfView::SUCCESS;
		}
	}

	/**
	 * Regresa arreglo serializado con con los registros de la tabla ocupacion_horas para el rango de fechas dado.
	 * @WSMethod(webservice='MakoApi')
	 * @param string $fecha_ini Fecha de inicio de la consulta
	 * @param string $fecha_fin Fecha de fin de la consulta
	 * @return string Regresa un arreglo serializado del tipo $res[]=$reporte_metas
	 */
	public function executeGetReporteOcupacionHoras($request)
	{
		$configuration = ProjectConfiguration::getApplicationConfiguration( 'frontend', 'tolu2', true);
		$context = sfContext::createInstance($configuration);

		$centro_id = sfConfig::get('app_centro_actual_id');
		$fecha_ini = pg_escape_string($request->getParameter('fecha_ini'));
		$fecha_fin = pg_escape_string($request->getParameter('fecha_fin'));

		$reporte = OcupacionHorasPeer::arregloReporte($fecha_ini, $fecha_fin, $centro_id);

		$this->result = serialize($reporte);

		if(count($reporte) == 0)
		{
			$me = new SoapFault('Server', "[GetReporteOcupacionHoras]: No existen registros para el rango de fechas consultadas $fecha_ini a $fecha_fin en el centro_id $centro_id.");
			throw $me;
			return sfView::ERROR;
		}
		else
		{
			return sfView::SUCCESS;
		}
	}

	/**
	 * Regresa arreglo serializado con con los registros de la tabla ocupacion_horas para el rango de fechas dado.
	 * @WSMethod(webservice='MakoApi')
	 * @param string $fecha_ini Fecha de inicio de la consulta
	 * @param string $fecha_fin Fecha de fin de la consulta
	 * @return string Regresa un arreglo serializado del tipo $res[]=$reporte_metas
	 */
	public function executeGetReporteEstatusSocios($request)
	{
		$configuration = ProjectConfiguration::getApplicationConfiguration( 'frontend', 'tolu2', true);
		$context = sfContext::createInstance($configuration);

		$centro_id = sfConfig::get('app_centro_actual_id');
		$fecha_ini = pg_escape_string($request->getParameter('fecha_ini'));
		$fecha_fin = pg_escape_string($request->getParameter('fecha_fin'));

		$reporte = EstatusSociosPeer::arregloReporte($fecha_ini, $fecha_fin, $centro_id);

		$this->result = serialize($reporte);

		if(count($reporte) == 0)
		{
			$me = new SoapFault('Server', "[GetReporteEstatusSocios]: No existen registros para el rango de fechas consultadas $fecha_ini a $fecha_fin en el centro_id $centro_id.");
			throw $me;
			return sfView::ERROR;
		}
		else
		{
			return sfView::SUCCESS;
		}
	}

	/**
	 * Regresa arreglo serializado con con los registros de la tabla reporte_activos para el rango de fechas dado.
	 * @WSMethod(webservice='MakoApi')
	 * @param string $fecha_ini Fecha de inicio de la consulta
	 * @param string $fecha_fin Fecha de fin de la consulta
	 * @return string Regresa un arreglo serializado del tipo $res[]=$reporte_metas
	 */
	public function executeGetReporteSociosActivos($request)
	{
		$configuration = ProjectConfiguration::getApplicationConfiguration( 'frontend', 'tolu2', true);
		$context = sfContext::createInstance($configuration);

		$centro_id = sfConfig::get('app_centro_actual_id');
		$fecha_ini = pg_escape_string($request->getParameter('fecha_ini'));
		$fecha_fin = pg_escape_string($request->getParameter('fecha_fin'));

		$reporte = ReporteActivosPeer::arregloReporte($fecha_ini, $fecha_fin, $centro_id);

		$this->result = serialize($reporte);

		if(count($reporte) == 0)
		{
			$me = new SoapFault('Server', "[GetReporteActivos]: No existen registros para el rango de fechas consultadas $fecha_ini a $fecha_fin en el centro_id $centro_id.");
			throw $me;
			return sfView::ERROR;
		}
		else
		{
			return sfView::SUCCESS;
		}
	}

	/**
	 * Regresa arreglo serializado con con los registros de la tabla reporte_categoria_cursos para el rango de fechas dado.
	 * @WSMethod(webservice='MakoApi')
	 * @param string $fecha_ini Fecha de inicio de la consulta
	 * @param string $fecha_fin Fecha de fin de la consulta
	 * @return string Regresa un arreglo serializado del tipo $res[]=$reporte_metas
	 */
	public function executeGetReporteCategoriaCursos($request)
	{
		$configuration = ProjectConfiguration::getApplicationConfiguration( 'frontend', 'tolu2', true);
		$context = sfContext::createInstance($configuration);

		$centro_id = sfConfig::get('app_centro_actual_id');
		$fecha_ini = pg_escape_string($request->getParameter('fecha_ini'));
		$fecha_fin = pg_escape_string($request->getParameter('fecha_fin'));

		$reporte = ReporteCategoriaCursosPeer::arregloReporte($fecha_ini, $fecha_fin, $centro_id);

		$this->result = serialize($reporte);

		if(count($reporte) == 0)
		{
			$me = new SoapFault('Server', "[GetReporteCategoriaCursos]: No existen registros para el rango de fechas consultadas $fecha_ini a $fecha_fin en el centro_id $centro_id.");
			throw $me;
			return sfView::ERROR;
		}
		else
		{
			return sfView::SUCCESS;
		}
	}

	/**
	 * Regresa arreglo serializado con con los registros de la tabla reporte_venta_cursos para el rango de fechas dado.
	 * @WSMethod(webservice='MakoApi')
	 * @param string $fecha_ini Fecha de inicio de la consulta
	 * @param string $fecha_fin Fecha de fin de la consulta
	 * @return string Regresa un arreglo serializado del tipo $res[]=$reporte_metas
	 */
	public function executeGetReporteVentaCursos($request)
	{
		$configuration = ProjectConfiguration::getApplicationConfiguration( 'frontend', 'tolu2', true);
		$context = sfContext::createInstance($configuration);

		$centro_id = sfConfig::get('app_centro_actual_id');
		$fecha_ini = pg_escape_string($request->getParameter('fecha_ini'));
		$fecha_fin = pg_escape_string($request->getParameter('fecha_fin'));

		$reporte = ReporteVentaCursosPeer::arregloReporte($fecha_ini, $fecha_fin, $centro_id);

		$this->result = serialize($reporte);

		if(count($reporte) == 0)
		{
			$me = new SoapFault('Server', "[GetReporteVentaCursos]: No existen registros para el rango de fechas consultadas $fecha_ini a $fecha_fin en el centro_id $centro_id.");
			throw $me;
			return sfView::ERROR;
		}
		else
		{
			return sfView::SUCCESS;
		}
	}

	/**
	 * Regresa arreglo serializado con con los registros de la tabla reporte_metas para el rango de fechas dado.
	 * @WSMethod(webservice='MakoApi')
	 * @param string $semana Semana de la consulta
	 * @param string $anio Anio de la consulta
	 * @param string $fechaInicio Fecha de inicio de la consulta
	 * @param string $fechaFin Fecha de fin de la consulta
	 * @return string Regresa un arreglo serializado del tipo $res[]=$reporteRankingCentral
	 */
	public function executeGetReporteRankingCentralWS($request)
	{
		$configuration = ProjectConfiguration::getApplicationConfiguration( 'frontend', 'tolu2', true);
		$context = sfContext::createInstance($configuration);

		$semana = pg_escape_string($request->getParameter('semana'));
		$anio = pg_escape_string($request->getParameter('anio'));
		$fecha = pg_escape_string($request->getParameter('fechaInicio'));
		$fechaFin = pg_escape_string($request->getParameter('fechaFin'));

		//$this->reporteRanking = ReporteRankingLocal($semana, $anio, null, $fecha, $fechaFin);
		$reporteRankingCentral = ReporteMetasPeer::reporteRankingCentral($semana, $anio, $fecha, $fechaFin);

		$this->result = serialize($reporteRankingCentral);

		if(count($reporteRankingCentral) == 0)
		{
			$me = new SoapFault('Server', "No existen registros de metas para el rango de fechas consultadas $semana - $fecha a $fechaFin en el centro_id $centro_id.");
			throw $me;
			return sfView::ERROR;
		}
		else
		{
			return sfView::SUCCESS;
		}

	}



	/**
	 * Regresa arreglo serializado con con los registros de la tabla reporte_metas para el rango de fechas dado.
	 * @WSMethod(webservice='MakoApi')
	 * @param string $centroId Identificador del centro
	 * @param string $semana Semana de la consulta
	 * @param string $anio Año de la consulta
	 * @param string $fechaInicio Fecha de inicio de la consulta
	 * @param string $fechaFin Fecha de fin de la consulta
	 * @return string Regresa un arreglo serializado del tipo $res[]=$reporteRankingCentral
	 */
	public function executeGetReporteRankingLocalWS($request)
	{
		$configuration = ProjectConfiguration::getApplicationConfiguration( 'frontend', 'tolu2', true);
		$context = sfContext::createInstance($configuration);

		$centroId = pg_escape_string($request->getParameter('centroId'));
		$semana = pg_escape_string($request->getParameter('semana'));
		$anio = pg_escape_string($request->getParameter('anio'));
		$fecha = pg_escape_string($request->getParameter('fechaInicio'));
		$fechaFin = pg_escape_string($request->getParameter('fechaFin'));

		$reporteRankingLocal = ReporteMetasPeer::reporteRankingLocal($semana, $anio, $centroId, $fecha, $fechaFin);

		$this->result = serialize($reporteRankingLocal);

		if(count($reporteRankingLocal) == 0)
		{
			$me = new SoapFault('Server', "No existen registros de metas para el rango de fechas consultadas $semana - $fecha a $fechaFin en el centro_id $centro_id.");
			throw $me;
			return sfView::ERROR;
		}
		else
		{
			return sfView::SUCCESS;
		}

	}
	/**
	 * Accion para mostrar el reporte de socios
	 * @param sfWebRequest $request
	 */
	public function executeMapa(sfWebRequest $request){
	}
	/**
	 * Accion que regresa la vista parcial del detalle de un socio
	 * @param sfWebRequest $request
	 * @return Vista parcial del detalle  de socio
	 */
	public function executeDetalleSocio(sfWebRequest $request)
	{
		$this->forward404unless($request->isXmlHttpRequest());
		$this->forward404Unless($socio = SocioPeer::retrieveByPk($request->getParameter('id')), sprintf('El socio no existe (%s).', $request->getParameter('id')));

		return $this->renderPartial('detalleSocio', array('socio' => $socio ));
	}


	/**
	 * Servicio que regresa en formato JSON la consulta de domicilio de socios cercanos a un radio de x kilometros
	 *
	 * @param sfWebRequest $request
	 * @return string JSON
	 */
	public function socioGeoref(sfWebRequest $request)
	{
		$centro_lat = pg_escape_string($request->getParameter('centro_lat'));
		$centro_long = pg_escape_string($request->getParameter('centro_long'));
		$radio = pg_escape_string($request->getParameter('radio'));

		$this->getResponse()->setContentType('application/json');

		$socios = SocioPeer::georef();
		return $this->renderText(json_encode($socios));
	}

	/**
	 * Registros totales , acumulado y meta
	 *
	 * @param sfWebRequest $request
	 */
	public function executeRegistrosSociosTotales(sfWebRequest $request)
	{
		$ctx = sfContext::getInstance();
		$ctx->getConfiguration()->loadHelpers('Reportes');

		$fI = pg_escape_string($request->getParameter('fecha_ini'));
		$fF = pg_escape_string($request->getParameter('fecha_fin'));

		$zona_id = intval(pg_escape_string($request->getParameter('zona')));
		$centro_id = intval(pg_escape_string($request->getParameter('centro_id')));

		if($zona_id != 0)
		{
			$zona = ZonaPeer::retrieveByPK($zona_id);
		}
		if($centro_id != 0)
		{
			$centro = CentroPeer::retrieveByPK($centro_id);
		}


		//Grafica
		$graf =  $request->getParameter('grafica');

		$nivel =  intval($request->getParameter('nivel'));
		if($nivel == 0) {
			$sqNivel = "date_trunc('month',fecha )::date as mes,";
		}
		if($nivel == 1) {
			$sqNivel = "date_trunc('week',fecha )::date as mes,";
		}
		if($nivel == 2) {
			$sqNivel = "date_trunc('day',fecha )::date as mes,";
		}

		if($graf == '' or $graf == null) $graf = 'soctot';

		if($fI == '' or $fI == null) $fI = '2009-05-11';
		if($fF == '' or $fF == null) $fF = date("Y-m-d");

		error_log("FI: $fI FF: $fF nivel $nivel centro_id: $centro_id zona_id: $zona_id");

		if($graf == 'soctot')
		{

			if($zona_id == 0 && $centro_id == 0)
			{

				$sql = "
				SELECT
				$sqNivel
				sum(cantidad) as cantidad,
				sum(meta_diaria) as meta
				FROM reporte_metas AS r
				WHERE categoria_fija_id = 1
				AND fecha BETWEEN '$fI' AND '$fF'
				GROUP BY mes
				ORDER BY mes;
				";
				$ns1 = 'Personas';
				$ns2 = 'Meta';
				$ns3 = 'Acumulado';
				$ns4 = 'Meta Acum';

			}
			elseif($zona_id != 0 && $centro_id == 0)
			{
				$sql = "
				SELECT
				$sqNivel
				sum(cantidad) as cantidad,
				sum(meta_diaria) as meta
				FROM reporte_metas AS r
				LEFT JOIN centro AS c ON (r.centro_id = c.id)
				WHERE categoria_fija_id = 1
				AND fecha BETWEEN '$fI' AND '$fF'
				AND c.zona_id = $zona_id
				GROUP BY mes
				ORDER BY mes;
				";
				$ns1 = $zona."";
				$ns2 = $zona.' Meta';
				$ns3 = $zona.' Acumulado';
				$ns4 = $zona.' Meta Acum';

			}
			elseif($zona_id == 0 && $centro_id != 0)
			{
				$sql = "
				SELECT
				$sqNivel
				sum(cantidad) as cantidad,
				sum(meta_diaria) as meta
				FROM reporte_metas AS r
				LEFT JOIN centro AS c ON (r.centro_id = c.id)
				WHERE categoria_fija_id = 1
				AND fecha BETWEEN '$fI' AND '$fF'
				AND c.id = $centro_id
				GROUP BY mes
				ORDER BY mes;
				";
				$ns1 = $centro->getAliasReporte();
				$ns2 = $centro->getAliasReporte().' Meta';
				$ns3 = $centro->getAliasReporte().' Acumulado';
				$ns4 = $centro->getAliasReporte().' Meta Acum';
			}
				
			error_log($sql);
				
			$conn = Propel::getConnection();
			$d = array();
			$acumulado=0;
			foreach($conn->query($sql) as $r)
			{
				$unixtime = strtotime($r['mes']);
				$jsunixtime=$unixtime*1000;
				$cantidad = floatval($r['cantidad']);
				$acumulado = $acumulado + $cantidad;
				$meta = floatval($r['meta']);
				$macumulado = $macumulado + $meta;

				$d[$ns1][] = array(
						$jsunixtime,
						$cantidad
				);
				$d[$ns2][] = array(
						$jsunixtime,
						$meta
				);
				$d[$ns3][] = array(
						$jsunixtime,
						$acumulado
				);

				$d[$ns4][] = array(
						$jsunixtime,
						$macumulado
				);

			}
			$this->getResponse()->setContentType('application/json');
			return $this->renderText(json_encode($d));
		}
	}

	/**
	 * Registros por pc y meta acumulada
	 *
	 * @param sfWebRequest $request
	 */
	public function executeRegistrosSociosPC(sfWebRequest $request)
	{
		$ctx = sfContext::getInstance();
		$ctx->getConfiguration()->loadHelpers('Reportes');

		$fI = pg_escape_string($request->getParameter('fecha_ini'));
		$fF = pg_escape_string($request->getParameter('fecha_fin'));

		$zona_id = intval(pg_escape_string($request->getParameter('zona')));
		$centro_id = intval(pg_escape_string($request->getParameter('centro_id')));

		if($zona_id != 0)
		{
			$zona = ZonaPeer::retrieveByPK($zona_id);
		}
		if($centro_id != 0)
		{
			$centro = CentroPeer::retrieveByPK($centro_id);
		}

		$nivel =  intval($request->getParameter('nivel'));
		if($nivel == 0) {
			$sqNivel = "date_trunc('month',fecha )::date as mes,";
		}
		if($nivel == 1) {
			$sqNivel = "date_trunc('week',fecha )::date as mes,";
		}
		if($nivel == 2) {
			$sqNivel = "date_trunc('day',fecha )::date as mes,";
		}

		//Grafica
		$graf =  $request->getParameter('grafica');
		if($graf == '' or $graf == null) $graf = 'soctotpc';

		if($fI == '' or $fI == null) $fI = '2010-09-01';
		if($fF == '' or $fF == null) $fF = date("Y-m-d");

		if($graf == 'soctotpc')
		{

			if($zona_id == 0 && $centro_id == 0)
			{
				$sql = "
				SELECT
				$sqNivel
				sum(cantidad) as cantidad,
				sum(meta_diaria) as meta
				FROM reporte_metas AS r
				WHERE categoria_fija_id = 1
				AND fecha BETWEEN '$fI' AND '$fF'
				GROUP BY mes
				ORDER BY mes;
				";
				$ns1 = 'Personas / PC';
				$ns2 = 'Meta / PC';
			}
			else if($centro_id == 0 && $zona_id != 0)
			{
				$sql = "
				SELECT
				$sqNivel
				sum(cantidad) as cantidad,
				sum(meta_diaria) as meta
				FROM reporte_metas AS r
				LEFT JOIN centro AS c ON (r.centro_id = c.id)
				WHERE categoria_fija_id = 1
				AND fecha BETWEEN '$fI' AND '$fF'
				AND c.zona_id = $zona_id
				GROUP BY mes
				ORDER BY mes;
				";

				$ns1 = $zona.' Personas / PC';
				$ns2 = $zona.' Meta / PC';

			}
			else if($centro_id != 0 && $zona_id == 0)
			{
				$sql = "
				SELECT
				$sqNivel
				sum(cantidad) as cantidad,
				sum(meta_diaria) as meta
				FROM reporte_metas AS r
				LEFT JOIN centro AS c ON (r.centro_id = c.id)
				WHERE categoria_fija_id = 1
				AND fecha BETWEEN '$fI' AND '$fF'
				AND c.id = $centro_id
				GROUP BY mes
				ORDER BY mes;
				";

				$ns1 = $centro->getAliasReporte().' Personas / PC';
				$ns2 = $centro->getAliasReporte().' Meta / PC';

			}
				
			error_log($sql);
				
			$conn = Propel::getConnection();
			$d = array();
			$acumulado=0;
			foreach($conn->query($sql) as $r)
			{

				$pcs = intval(self::numPcsEnFecha($r['mes']));
				$unixtime = strtotime($r['mes']);
				$jsunixtime=$unixtime*1000;

				$personas = floatval($r['cantidad']);
				$acumulado = $acumulado + $personas;

				$meta = floatval($r['meta']);
				$macumulado = $macumulado + $meta;

				$d[$ns1][] = array(
						$jsunixtime,
						floatval(number_format($personas/$pcs,2))
				);
				$d[$ns2][] = array(
						$jsunixtime,
						floatval(number_format($meta/$pcs,2))
				);


			}
			$this->getResponse()->setContentType('application/json');
			return $this->renderText(json_encode($d));
		}
	}

	/**
	 * Inscripciones, acumulado y meta
	 *
	 * @param sfWebRequest $request
	 */
	public function executeInscripcionesTotales(sfWebRequest $request)
	{
		$ctx = sfContext::getInstance();
		$ctx->getConfiguration()->loadHelpers('Reportes');

		$fI = pg_escape_string($request->getParameter('fecha_ini'));
		$fF = pg_escape_string($request->getParameter('fecha_fin'));

		$zona_id = intval(pg_escape_string($request->getParameter('zona')));
		$centro_id = intval(pg_escape_string($request->getParameter('centro_id')));

		if($zona_id != 0)
		{
			$zona = ZonaPeer::retrieveByPK($zona_id);
		}
		if($centro_id != 0)
		{
			$centro = CentroPeer::retrieveByPK($centro_id);
		}

		$nivel =  intval($request->getParameter('nivel'));
		if($nivel == 0) {
			$sqNivel = "date_trunc('month',fecha )::date as mes,";
		}
		if($nivel == 1) {
			$sqNivel = "date_trunc('week',fecha )::date as mes,";
		}
		if($nivel == 2) {
			$sqNivel = "date_trunc('day',fecha )::date as mes,";
		}

		//Grafica
		$graf =  $request->getParameter('grafica');
		if($graf == '' or $graf == null) $graf = 'soctot';

		if($fI == '' or $fI == null) $fI = '2010-09-01';
		if($fF == '' or $fF == null) $fF = date("Y-m-d");

		if($graf == 'soctot')
		{

			if($zona_id == 0 && $centro_id == 0)
			{
				$sql = "
				SELECT
				$sqNivel
				SUM(cantidad) AS cantidad,
				SUM(meta_diaria) AS meta
				FROM reporte_metas AS r
				WHERE categoria_fija_id = 2
				AND fecha BETWEEN '$fI' AND '$fF'
				GROUP BY mes
				ORDER BY mes;			";

				$ns1 = 'Inscripciones';
				$ns2 = 'Meta';
				$ns3 = 'Acumulado';
				$ns4 = 'Meta Acum';

			}
			else if($zona_id != 0 && $centro_id == 0)
			{
				$sql = "
				SELECT
				$sqNivel
				SUM(cantidad) AS cantidad,
				SUM(meta_diaria) AS meta
				FROM reporte_metas AS r
				LEFT JOIN centro AS c ON (r.centro_id = c.id)
				WHERE categoria_fija_id = 2
				AND fecha BETWEEN '$fI' AND '$fF'
				AND c.zona_id = $zona_id
				GROUP BY mes
				ORDER BY mes;			";

				$ns1 = $zona.'';
				$ns2 = $zona.' Meta';
				$ns3 = $zona.' Acumulado';
				$ns4 = $zona.' Meta Acum';


			}
			else if($zona_id == 0 && $centro_id != 0)
			{
				$sql = "
				SELECT
				$sqNivel
				SUM(cantidad) AS cantidad,
				SUM(meta_diaria) AS meta
				FROM reporte_metas AS r
				LEFT JOIN centro AS c ON (r.centro_id = c.id)
				WHERE categoria_fija_id = 2
				AND fecha BETWEEN '$fI' AND '$fF'
				AND c.id = $centro_id
				GROUP BY mes
				ORDER BY mes;			";

				$ns1 = $centro->getAliasReporte();
				$ns2 = $centro->getAliasReporte().' Meta';
				$ns3 = $centro->getAliasReporte().' Acumulado';
				$ns4 = $centro->getAliasReporte().' Meta Acum';

			}
				
			error_log($sql);
				
			$conn = Propel::getConnection();
			$d = array();
			$acumulado=0;
			foreach($conn->query($sql) as $r)
			{
				$unixtime = strtotime($r['mes']);
				$jsunixtime=$unixtime*1000;
				$cantidad = floatval($r['cantidad']);
				$acumulado = $acumulado + $cantidad;
				$meta = floatval($r['meta']);
				$macumulado = $macumulado + $meta;

				$d[$ns1][] = array(
						$jsunixtime,
						$cantidad
				);
				$d[$ns2][] = array(
						$jsunixtime,
						$meta
				);
				$d[$ns3][] = array(
						$jsunixtime,
						$acumulado
				);
				$d[$ns4][] = array(
						$jsunixtime,
						$macumulado
				);

			}
			$this->getResponse()->setContentType('application/json');
			return $this->renderText(json_encode($d));
		}
	}

	/**
	 * Inscripciones por curso y meta curso
	 *
	 * @param sfWebRequest $request
	 */
	public function executeInscripcionesCurso(sfWebRequest $request)
	{
		$ctx = sfContext::getInstance();
		$ctx->getConfiguration()->loadHelpers('Reportes');
		$ctx->getConfiguration()->loadHelpers('ReportesIndicadores');

		$fI = pg_escape_string($request->getParameter('fecha_ini'));
		$fF = pg_escape_string($request->getParameter('fecha_fin'));

		$zona_id = intval(pg_escape_string($request->getParameter('zona')));
		$centro_id = intval(pg_escape_string($request->getParameter('centro_id')));

		if($zona_id != 0)
		{
			$zona = ZonaPeer::retrieveByPK($zona_id);
		}
		if($centro_id != 0)
		{
			$centro = CentroPeer::retrieveByPK($centro_id);
		}

		$nivel =  intval($request->getParameter('nivel'));
		if($nivel == 0) {
			$sqNivel = "date_trunc('month',fecha )::date as mes,";
		}
		if($nivel == 1) {
			$sqNivel = "date_trunc('week',fecha )::date as mes,";
		}
		if($nivel == 2) {
			$sqNivel = "date_trunc('day',fecha )::date as mes,";
		}

		//Grafica
		$graf =  $request->getParameter('grafica');
		if($graf == '' or $graf == null) $graf = 'soctotpc';

		if($fI == '' or $fI == null) $fI = '2010-09-01';
		if($fF == '' or $fF == null) $fF = date("Y-m-d");

		if($graf == 'soctotpc')
		{
			if($zona_id == 0 && $centro_id == 0)
			{
				$sql = "
				SELECT
				$sqNivel
				sum(cantidad) as cantidad,
				sum(meta_diaria) as meta
				FROM reporte_metas AS r
				WHERE categoria_fija_id = 2
				AND fecha BETWEEN '$fI' AND '$fF'
				GROUP BY mes
				ORDER BY mes;
				";
				$ns1 = "Inscripciones / Curso";
				$ns2 = "Meta / Curso";
			}
			if($zona_id != 0 && $centro_id == 0)
			{
				$sql = "
				SELECT
				$sqNivel
				sum(cantidad) as cantidad,
				sum(meta_diaria) as meta
				FROM reporte_metas AS r
				LEFT JOIN centro AS c ON (c.id = r.centro_id)
				WHERE categoria_fija_id = 2
				AND fecha BETWEEN '$fI' AND '$fF'
				AND c.zona_id = $zona_id
				GROUP BY mes
				ORDER BY mes;
				";
				$ns1 = $zona."";
				$ns2 = $zona." Meta";
			}
			if($zona_id == 0 && $centro_id != 0)
			{
				$sql = "
				SELECT
				$sqNivel
				sum(cantidad) as cantidad,
				sum(meta_diaria) as meta
				FROM reporte_metas AS r
				LEFT JOIN centro AS c ON (c.id = r.centro_id)
				WHERE categoria_fija_id = 2
				AND fecha BETWEEN '$fI' AND '$fF'
				AND c.id = $centro_id
				GROUP BY mes
				ORDER BY mes;
				";
				$ns1 = $centro->getAliasReporte();
				$ns2 = $centro->getAliasReporte()." Meta";
			}
				
			$aCursos = numCursosperiodo($fI,$fF,$nivel,$centro_id,$zona_id);
				
			$conn = Propel::getConnection();
			$d = array();
			$cca = array();
			$acumulado=0;
			foreach($conn->query($sql) as $r)
			{

				$cursos = $aCursos[$r['mes']];
				$unixtime = strtotime($r['mes']);
				$jsunixtime=$unixtime*1000;

				$cantidad = floatval($r['cantidad']);
				$cca[$r['mes']]=$cantidad;
				$meta = floatval($r['meta']);

				$d[$ns1][] = array(
						$jsunixtime,
						floatval(number_format($cantidad/$cursos['cantidad'],2))
				);
				$d[$ns2][] = array(
						$jsunixtime,
						floatval(number_format($meta/$cursos['cantidad'],2))
				);
			}
				
			//TODO:Revisar la forma de sacar los cursos unicos.
			//DEBE HACERSE COMO CON LOS USUARIOS UNICOS.
			error_log(print_r($cca,true),3,'/tmp/salida.txt');
			$this->getResponse()->setContentType('application/json');
			return $this->renderText(json_encode($d));
		}
	}


	/**
	 * Cobranza, acumulado y meta
	 *
	 * @param sfWebRequest $request
	 */
	public function executeCobranzaTotales(sfWebRequest $request)
	{
		$ctx = sfContext::getInstance();
		$ctx->getConfiguration()->loadHelpers('Reportes');

		$fI = pg_escape_string($request->getParameter('fecha_ini'));
		$fF = pg_escape_string($request->getParameter('fecha_fin'));

		$zona_id = intval(pg_escape_string($request->getParameter('zona')));
		$centro_id = intval(pg_escape_string($request->getParameter('centro_id')));

		if($zona_id != 0)
		{
			$zona = ZonaPeer::retrieveByPK($zona_id);
		}
		if($centro_id != 0)
		{
			$centro = CentroPeer::retrieveByPK($centro_id);
		}

		$nivel =  intval($request->getParameter('nivel'));
		if($nivel == 0) {
			$sqNivel = "date_trunc('month',fecha )::date as mes,";
		}
		if($nivel == 1) {
			$sqNivel = "date_trunc('week',fecha )::date as mes,";
		}
		if($nivel == 2) {
			$sqNivel = "date_trunc('day',fecha )::date as mes,";
		}

		//Grafica
		$graf =  $request->getParameter('grafica');
		if($graf == '' or $graf == null) $graf = 'soctot';

		if($fI == '' or $fI == null) $fI = '2010-09-01';
		if($fF == '' or $fF == null) $fF = date("Y-m-d");

		if($graf == 'soctot')
		{
			if($zona_id == 0 && $centro_id == 0)
			{
				$sql = "
				SELECT
				$sqNivel
				SUM(cantidad) AS cantidad,
				SUM(meta_diaria) AS meta
				FROM reporte_metas AS r
				WHERE categoria_fija_id = 3
				AND fecha BETWEEN '$fI' AND '$fF'
				GROUP BY mes
				ORDER BY mes;			";

				$ns1 = 'Cobranza';
				$ns2 = 'Meta';
				$ns3 = 'Acumulado';
				$ns4 = 'Meta Acum';
			}
			elseif($zona_id != 0 && $centro_id == 0)
			{
				$sql = "
				SELECT
				$sqNivel
				SUM(cantidad) AS cantidad,
				SUM(meta_diaria) AS meta
				FROM reporte_metas AS r
				LEFT JOIN centro AS c ON (c.id = r.centro_id)
				WHERE categoria_fija_id = 3
				AND fecha BETWEEN '$fI' AND '$fF'
				AND c.zona_id = $zona_id
				GROUP BY mes
				ORDER BY mes;			";
				$ns1 = $zona.'';
				$ns2 = $zona.' Meta';
				$ns3 = $zona.' Acumulado';
				$ns4 = $zona.' Meta Acum';
			}
			elseif($zona_id == 0 && $centro_id != 0)
			{
				$sql = "
				SELECT
				$sqNivel
				SUM(cantidad) AS cantidad,
				SUM(meta_diaria) AS meta
				FROM reporte_metas AS r
				LEFT JOIN centro AS c ON (c.id = r.centro_id)
				WHERE categoria_fija_id = 3
				AND fecha BETWEEN '$fI' AND '$fF'
				AND c.id = $centro_id
				GROUP BY mes
				ORDER BY mes;			";

				$ns1 = $centro->getAliasReporte().'';
				$ns2 = $centro->getAliasReporte().' Meta';
				$ns3 = $centro->getAliasReporte().' Acumulado';
				$ns4 = $centro->getAliasReporte().' Meta Acum';
			}
				
			$conn = Propel::getConnection();
			$d = array();
			$acumulado=0;
			foreach($conn->query($sql) as $r)
			{
				$unixtime = strtotime($r['mes']);
				$jsunixtime=$unixtime*1000;
				$cantidad = floatval($r['cantidad']);
				$acumulado = $acumulado + $cantidad;
				$meta = floatval($r['meta']);
				$macumulado = $macumulado + $meta;

				$d[$ns1][] = array(
						$jsunixtime,
						$cantidad
				);
				$d[$ns2][] = array(
						$jsunixtime,
						$meta
				);
				$d[$ns3][] = array(
						$jsunixtime,
						$acumulado
				);

				$d[$ns4][] = array(
						$jsunixtime,
						$macumulado
				);

			}
			$this->getResponse()->setContentType('application/json');
			return $this->renderText(json_encode($d));
		}
	}

	/**
	 * Cobranza por curso y meta curso
	 *
	 * @param sfWebRequest $request
	 */
	public function executeCobranzaCurso(sfWebRequest $request)
	{
		$ctx = sfContext::getInstance();
		$ctx->getConfiguration()->loadHelpers('Reportes');
		$ctx->getConfiguration()->loadHelpers('ReportesIndicadores');

		$fI = pg_escape_string($request->getParameter('fecha_ini'));
		$fF = pg_escape_string($request->getParameter('fecha_fin'));

		$zona_id = intval(pg_escape_string($request->getParameter('zona')));
		$centro_id = intval(pg_escape_string($request->getParameter('centro_id')));

		if($zona_id != 0)
		{
			$zona = ZonaPeer::retrieveByPK($zona_id);
		}
		if($centro_id != 0)
		{
			$centro = CentroPeer::retrieveByPK($centro_id);
		}

		$nivel =  intval($request->getParameter('nivel'));
		if($nivel == 0) {
			$sqNivel = "date_trunc('month',fecha )::date as mes,";
		}
		if($nivel == 1) {
			$sqNivel = "date_trunc('week',fecha )::date as mes,";
		}
		if($nivel == 2) {
			$sqNivel = "date_trunc('day',fecha )::date as mes,";
		}

		//Grafica
		$graf =  $request->getParameter('grafica');
		if($graf == '' or $graf == null) $graf = 'soctotpc';

		if($fI == '' or $fI == null) $fI = '2010-09-01';
		if($fF == '' or $fF == null) $fF = date("Y-m-d");

		if($graf == 'soctotpc')
		{
			if($zona_id == 0 && $centro_id == 0)
			{
				$sql = "
				SELECT
				$sqNivel
				SUM(cantidad) AS cantidad,
				SUM(meta_diaria) AS meta
				FROM reporte_metas AS r
				WHERE categoria_fija_id = 3
				AND fecha BETWEEN '$fI' AND '$fF'
				GROUP BY mes
				ORDER BY mes;			";

				$ns1 = 'Cobranza';
				$ns2 = 'Meta';
			}
			elseif($zona_id != 0 && $centro_id == 0)
			{
				$sql = "
				SELECT
				$sqNivel
				SUM(cantidad) AS cantidad,
				SUM(meta_diaria) AS meta
				FROM reporte_metas AS r
				LEFT JOIN centro AS c ON (c.id = r.centro_id)
				WHERE categoria_fija_id = 3
				AND fecha BETWEEN '$fI' AND '$fF'
				AND c.zona_id = $zona_id
				GROUP BY mes
				ORDER BY mes;			";

				$ns1 = $zona.'';
				$ns2 = $zona.' Meta';
			}
			elseif($zona_id == 0 && $centro_id != 0)
			{
				$sql = "
				SELECT
				$sqNivel
				SUM(cantidad) AS cantidad,
				SUM(meta_diaria) AS meta
				FROM reporte_metas AS r
				LEFT JOIN centro AS c ON (c.id = r.centro_id)
				WHERE categoria_fija_id = 3
				AND fecha BETWEEN '$fI' AND '$fF'
				AND c.id = $centro_id
				GROUP BY mes
				ORDER BY mes;			";

				$ns1 = $centro->getAliasReporte().'';
				$ns2 = $centro->getAliasReporte().' Meta';
			}
			error_log($sql);
				
			$aCursos = numCursosperiodo($fI,$fF,$nivel,$centro_id,$zona_id);

			$conn = Propel::getConnection();
			$d = array();
			$acumulado=0;
			foreach($conn->query($sql) as $r)
			{

				$cursos = $aCursos[$r['mes']];

				$unixtime = strtotime($r['mes']);
				$jsunixtime=$unixtime*1000;

				$cantidad = floatval($r['cantidad']);

				$meta = floatval($r['meta']);

				$d[$ns1][] = array(
						$jsunixtime,
						floatval(number_format($cantidad/$cursos['cantidad'],2))
				);
				$d[$ns2][] = array(
						$jsunixtime,
						floatval(number_format($meta/$cursos['cantidad'],2))
				);
			}
				
			$this->getResponse()->setContentType('application/json');
			return $this->renderText(json_encode($d));
		}
	}

	public function executeIndicadores($request)
	{

	}

	public function executeIndicadoresCapacidad($request)
	{

	}

	public function executeRanking($request)
	{

		// echo date("Y-m-d",strtotime('last day',strtotime($f)));

		$fecha_inicio = '2011-01-01';
		$fecha_fin = '2011-05-30';
		$conn = Propel::getConnection();

		$sql ="
		select alias_reporte as centro,categoria_fija_id,
		sum(cantidad) as cantidad,
		sum(meta_diaria) as meta
		from reporte_metas as r
		left join centro as c on (c.id = r.centro_id)
		where fecha between '$fecha_inicio' and '$fecha_fin'
		group by 1,2
		order by 1,2;";

		$Rank = array();
		foreach($conn->query($sql) as $r)
		{
			$Rank[$r['centro']][$r['categoria_fija_id']] = array("cantidad"=>$r['cantidad'],"meta"=>$r['meta']);
		}

		//Acomodamos los registros para que aparezcan como tabla
		$Rankings = array();
		foreach($Rank as $r => $a)
		{
			$regs = ($a['1']['meta']>0)? number_format(($a['1']['cantidad']/$a['1']['meta'])*100,2) : 100;
			$curs = ($a['2']['meta']>0)? number_format(($a['2']['cantidad']/$a['2']['meta'])*100,2) : 100;
			$cobr = ($a['3']['meta']>0)? number_format(($a['3']['cantidad']/$a['3']['meta'])*100,2) : 100;
				
			$regso = $a['1']['cantidad'];
			$curso = $a['2']['cantidad'];
			$cobro = $a['3']['cantidad'];
				
			$regsm = $a['1']['meta'];
			$cursm = $a['2']['meta'];
			$cobrm = $a['3']['meta'];
				
			$Rankings[] = array(
					'centro'=>$r,
					'registros'=> $regs,
					'cursos'=>$curs,
					'cobranza'=>$cobr,

					'registroso'=> $regso,
					'cursoso'=>$curso,
					'cobranzao'=>$cobro,

					'registrosm'=> $regsm,
					'cursosm'=>$cursm,
					'cobranzam'=>$cobrm,

					'promedioo' => number_format(($regso + $curso + $cobro)/3,2),
						
					'promedio' => number_format(($regs + $curs + $cobr)/3,2)
			);
		}

		//Ordenamos por el mayor promedio
		$this->Rankings = $this->ordInvArray($Rankings, 'promedio');

		//Ordenamos por el mayor de registros
		$raRegs = $this->ordInvArray($Rankings, 'registros');
		$raCursos = $this->ordInvArray($Rankings, 'cursos');
		$raCobranza = $this->ordInvArray($Rankings, 'cobranza');

		//print_r($raRegs);
		//print_r($raCursos);
		//print_r($raCobranza);


		//Calculamos los rankings de registro
		$rRegs = array();
		$ant=0;
		$cntrnk = 1;
		foreach($raRegs as $idx => $raReg)
		{
			$centro = $raReg['centro'];
			$actual = $raReg['registros'];
			if($ant == $actual)
			{
				$rRegs[$centro] = $cntrnk;
				continue;
			}
			else
			{
				$rRegs[$centro] = $cntrnk;
				$cntrnk++;
				continue;
			}

		}

		//Calculamos los rankings de cursos
		$rCurs = array();
		$ant=0;
		$cntrnk = 1;
		foreach($raCursos as $idx => $raCur)
		{
			$centro = $raCur['centro'];
			$actual = $raCur['cursos'];
			if($ant == $actual)
			{
				$rCurs[$centro] = $cntrnk;
				continue;
			}
			else
			{
				$rCurs[$centro] = $cntrnk;
				$cntrnk++;
				continue;
			}

		}

		//Calculamos los rankings de cobranza
		$rCobs = array();
		$ant=0;
		$cntrnk = 1;
		foreach($raCobranza as $idx => $raCob)
		{
			$centro = $raCob['centro'];
			$actual = $raCob['cobranza'];
			if($ant == $actual)
			{
				$rCobs[$centro] = $cntrnk;
				continue;
			}
			else
			{
				$rCobs[$centro] = $cntrnk;
				$cntrnk++;
				continue;
			}

		}


		//Calculamos el ranking general:
		$rnkGeneral = array();
		foreach($rRegs as $centro => $val)
		{
			$rnkGeneral[$centro] = number_format(($rRegs[$centro] + $rCurs[$centro] + $rCobs[$centro]) / 3, 2);
		}
		asort($rnkGeneral);
		$this->rnkGeneral = $rnkGeneral;


		//************************ F1 ******************************

		$sql ="
		SELECT
		c.alias_reporte AS centro,
		SUM(r.cantidad)/c.pcs_usuario AS cantidad
		FROM reporte_metas AS r
		LEFT JOIN centro AS c ON (c.id = r.centro_id)
		WHERE categoria_fija_id = 1
		AND fecha BETWEEN '$fecha_inicio' AND '$fecha_fin'
		GROUP BY c.alias_reporte,c.pcs_usuario
		ORDER BY 2 DESC";

		$RankF1 = array();
		$sth = $conn->prepare($sql);
		$sth->execute();
		$this->RankF1 = $this->ordInvArray($sth->fetchAll(PDO::FETCH_ASSOC),'cantidad');

		//************************ F2 ******************************

		$sql ="
		SELECT
		c.alias_reporte AS centro,
		SUM(r.monto)/c.pcs_usuario AS cantidad
		FROM reporte_ventas_categorias AS r
		LEFT JOIN centro AS c ON (c.id = r.centro_id)
		WHERE categoria_id = 2
		AND fecha BETWEEN '$fecha_inicio' AND '$fecha_fin'
		GROUP BY c.alias_reporte,c.pcs_usuario
		ORDER BY 2 DESC";

		$RankF2 = array();
		$sth = $conn->prepare($sql);
		$sth->execute();
		$this->RankF2 = $this->ordInvArray($sth->fetchAll(PDO::FETCH_ASSOC),'cantidad');

		//************************ F3 ******************************

		$sql ="
		SELECT
		c.alias_reporte AS centro,
		sum(r.monto)/c.pcs_usuario AS cantidad
		FROM reporte_ventas_categorias AS r
		LEFT JOIN centro AS c ON (c.id = r.centro_id)
		WHERE
		fecha BETWEEN '$fecha_inicio' AND '$fecha_fin'
		GROUP BY c.alias_reporte,c.pcs_usuario
		ORDER BY 2 desc";

		$RankF3 = array();
		$sth = $conn->prepare($sql);
		$sth->execute();
		$this->RankF3 = $this->ordInvArray($sth->fetchAll(PDO::FETCH_ASSOC),'cantidad');

		//************************ F4 ******************************

		$sql ="
		SELECT
		c.alias_reporte AS centro, c.id,
		SUM(r.cantidad) AS cantidad
		FROM reporte_metas AS r
		LEFT JOIN centro AS c ON (c.id = r.centro_id)
		WHERE categoria_fija_id = 1
		AND fecha BETWEEN '$fecha_inicio' AND '$fecha_fin'
		GROUP BY c.alias_reporte, c.id";

		$RankF4 = array();
		$sth = $conn->prepare($sql);
		$sth->execute();
		foreach ($sth->fetchAll(PDO::FETCH_ASSOC) as $d)
		{
			$aulas = $this->numAulas($d['id']);
			error_log("AULAS: ".$aulas);
			$RankF4[] = array('centro'=>$d['centro'],'cantidad'=> number_format($d['cantidad']/$aulas,2));
		}

		$this->RankF4 = $this->ordInvArray($RankF4,'cantidad');

		//************************ F5 ******************************

		$sql ="
		SELECT
		c.alias_reporte AS centro,
		SUM(r.cantidad)/r.aulas AS cantidad
		FROM reporte_ventas_categorias AS r
		LEFT JOIN centro AS c ON (c.id = r.centro_id)
		WHERE
		categoria_id = 2
		AND
		fecha BETWEEN '$fecha_inicio' AND '$fecha_fin'
		GROUP BY c.alias_reporte, r.aulas";

		$RankF5 = array();
		$sth = $conn->prepare($sql);
		$sth->execute();
		$this->RankF5 = $this->ordInvArray($sth->fetchAll(PDO::FETCH_ASSOC),'cantidad');

		//************************ F6 ******************************

		$sql ="
		SELECT
		c.alias_reporte AS centro,
		SUM(r.monto)/r.aulas AS cantidad
		FROM reporte_ventas_categorias AS r
		LEFT JOIN centro AS c ON (c.id = r.centro_id)
		WHERE
		fecha BETWEEN '$fecha_inicio' AND '$fecha_fin'
		GROUP BY c.alias_reporte, r.aulas";

		$RankF6 = array();
		$sth = $conn->prepare($sql);
		$sth->execute();
		$this->RankF6 = $this->ordInvArray($sth->fetchAll(PDO::FETCH_ASSOC),'cantidad');

		//************************ F7 ******************************

		$sql ="
		SELECT
		c.alias_reporte AS centro,
		count(*) as cantidad
		FROM dias_operacion AS d
		LEFT JOIN centro AS c ON (c.id = d.centro_id)
		WHERE
		fecha BETWEEN '$fecha_inicio' AND '$fecha_fin'
		AND opera = 't'
		GROUP BY 1
		";

		$RankF7 = array();
		$sth = $conn->prepare($sql);
		$sth->execute();
		$this->RankF7 = $this->ordInvArray($sth->fetchAll(PDO::FETCH_ASSOC),'cantidad');

	}

	/**
	 * Devuelve el numero total de PCs de socio existentes a una fecha dada.
	 * @param string $fecha
	 * @return integer
	 */
	static public function numPcsEnFecha($fecha)
	{
		$conn = Propel::getConnection();
		$cantidad = 0;
		//TODO: Contemplar filtro por centro
		//TODO: Contemplar filtro por rango de fecha.
		$sql = "SELECT sum(pcs_usuario) AS cantidad FROM centro WHERE desde <= '$fecha'";
		foreach($conn->query($sql) as $r)
		{
			$cantidad = $r['cantidad'];
		}
		return $cantidad;
	}

	/**
	 * Devuelve el numero total de aulas efectivas de un centro
	 * @param integer $centro_id El id del centro
	 * @return integer
	 */
	static public function numAulas($centro_id)
	{
		$c = new Criteria(); $c->add(SeccionPeer::ACTIVO,true); $c->add(SeccionPeer::EN_DASHBOARD,true);
		$aulas = 0;
		$centro = CentroPeer::retrieveByPK($centro_id);
		foreach($centro->getSeccions($c) as $aula)
		{
			if(preg_match("/aula (\d){1}/i",$aula->getNombre()))
				$aulas++;
		}
		return $aulas;
	}


	/**
	 * Ordena arreglo multidimensional por alguna llave
	 * de forma inversa
	 *
	 * @param Array $a Arreglo a ordenar
	 * @param string $subkey Llave por la cual ordenar
	 */
	public static function ordInvArray($a,$subkey) {
		foreach($a as $k=>$v) {
			$b[$k] = strtolower($v[$subkey]);
		}
		arsort($b);
		foreach($b as $key=>$val) {
			$c[] = $a[$key];
		}
		error_log($c);
		return $c;
	}


	public function executeIndicadoresDetalle($request)
	{

		$this->Zonas = ZonaPeer::listaZonas();
		$this->Centros = CentroPeer::listaCentrosObj();

		$fI = pg_escape_string($request->getParameter('fecha_ini'));
		$fF = pg_escape_string($request->getParameter('fecha_fin'));
		$caso = pg_escape_string($request->getParameter('caso'));

		if($caso == 'registros_total')
		{
			$this->modulo = 'RegistrosSociosTotales';
			$this->opciones_titulo = "Registros";
			$this->opciones_subtitulo = "Registros Totales y Metas";
			$this->opciones_yaxis = "Personas";
			$this->hayAcumulados = true;
		}
		elseif($caso == 'registros_total_pcs')
		{
			$this->modulo = 'RegistrosSociosPC';
			$this->opciones_titulo = "Registros por PC";
			$this->opciones_subtitulo = "Registros Totales y Metas por PC";
			$this->opciones_yaxis = "Personas";
			$this->hayAcumulados = false;
		}
		if($caso == 'inscripciones_total')
		{
			$this->modulo = 'InscripcionesTotales';
			$this->opciones_titulo = "Inscripciones";
			$this->opciones_subtitulo = "Inscripciones Totales y Metas";
			$this->opciones_yaxis = "Personas";
			$this->hayAcumulados = true;
		}
		elseif($caso == 'inscripciones_total_cursos')
		{
			$this->modulo = 'InscripcionesCurso';
			$this->opciones_titulo = "Inscripciones por Curso";
			$this->opciones_subtitulo = "Inscripciones Totales y Metas por Curso";
			$this->opciones_yaxis = "Personas";
			$this->hayAcumulados = false;
		}
		if($caso == 'cobranza_total')
		{
			$this->modulo = 'CobranzaTotales';
			$this->opciones_titulo = "Cobranza";
			$this->opciones_subtitulo = "Cobranza Total y Metas";
			$this->opciones_yaxis = "Pesos ($)";
			$this->hayAcumulados = true;
		}
		elseif($caso == 'cobranza_total_cursos')
		{
			$this->modulo = 'CobranzaCurso';
			$this->opciones_titulo = "Cobranza y Metas por Curso";
			$this->opciones_subtitulo = "Cobranza Total y Metas por Curso";
			$this->opciones_yaxis = "Pesos ($)";
			$this->hayAcumulados = false;
		}

	}


	public function executeIndicadoresDetalleZoom($request)
	{

		$this->Zonas = ZonaPeer::listaZonas();
		$this->Centros = CentroPeer::listaCentrosObj();

		$fI = pg_escape_string($request->getParameter('fecha_ini'));
		$fF = pg_escape_string($request->getParameter('fecha_fin'));
		$caso = pg_escape_string($request->getParameter('caso'));

		$this->moduloIndicadores = 'indicadores';

		if($caso == 'registros_total')
		{
			$this->modulo = 'RegistrosSociosTotales';
			$this->opciones_titulo = "Registros";
			$this->opciones_subtitulo = "Registros Totales y Metas";
			$this->opciones_yaxis = "Personas";
			$this->hayAcumulados = true;
			$this->hayMetas = true;

		}
		elseif($caso == 'registros_total_pcs')
		{
			$this->modulo = 'RegistrosSociosPC';
			$this->opciones_titulo = "Registros por PC";
			$this->opciones_subtitulo = "Registros Totales y Metas por PC";
			$this->opciones_yaxis = "Personas";
			$this->hayAcumulados = false;
			$this->hayMetas = true;
		}
		elseif($caso == 'inscripciones_total')
		{
			$this->modulo = 'InscripcionesTotales';
			$this->opciones_titulo = "Inscripciones";
			$this->opciones_subtitulo = "Inscripciones Totales y Metas";
			$this->opciones_yaxis = "Personas";
			$this->hayAcumulados = true;
			$this->hayMetas = true;
		}
		elseif($caso == 'inscripciones_total_cursos')
		{
			$this->modulo = 'InscripcionesCurso';
			$this->opciones_titulo = "Inscripciones por Curso";
			$this->opciones_subtitulo = "Inscripciones Totales y Metas por Curso";
			$this->opciones_yaxis = "Personas";
			$this->hayAcumulados = false;
			$this->hayMetas = true;
		}
		if($caso == 'cobranza_total')
		{
			$this->modulo = 'CobranzaTotales';
			$this->opciones_titulo = "Cobranza";
			$this->opciones_subtitulo = "Cobranza Total y Metas";
			$this->opciones_yaxis = "Pesos ($)";
			$this->hayAcumulados = true;
			$this->hayMetas = true;
		}
		elseif($caso == 'cobranza_total_cursos')
		{
			$this->modulo = 'CobranzaCurso';
			$this->opciones_titulo = "Cobranza y Metas por Curso";
			$this->opciones_subtitulo = "Cobranza Total y Metas por Curso";
			$this->opciones_yaxis = "Pesos ($)";
			$this->hayAcumulados = false;
			$this->hayMetas = true;
		}
		if($caso == 'minutos_utilizados')
		{
			$this->modulo = 'Ocupacion';
			$this->opciones_titulo = "Minutos Utilizados";
			$this->opciones_subtitulo = "Total de Minutos Utilizados";
			$this->opciones_yaxis = "Minutos";
			$this->hayAcumulados = true;
			$this->moduloIndicadores = 'indicadoresCapacidad';
		}
		elseif($caso == 'tasa_minutos_utilizados')
		{
			$this->modulo = 'TasaOcupacion';
			$this->opciones_titulo = "Tasa Minutos Utilizados";
			$this->opciones_subtitulo = "Tasa de Minutos Utilizados Totales";
			$this->opciones_yaxis = "Minutos Utilizados / Minutos Disponibles ";
			$this->hayAcumulados = false;
			$this->hayMetas = false;
			$this->ponderados = true;
			$this->moduloIndicadores = 'indicadoresCapacidad';
			//$this->setTemplate('indicadoresDetalleCapacidadHorasDias');
		}
		elseif($caso == 'tasa_ocupacion_60')
		{
			$this->modulo = 'TasaOcupacion60';
			$this->opciones_titulo = "Tasa Minutos Utilizados";
			$this->opciones_subtitulo = "Tasa de Minutos Utilizados Totales Ponderado al 60%";
			$this->opciones_yaxis = "Minutos Utilizados / Minutos Disponibles ";
			$this->hayAcumulados = false;
			$this->hayMetas = false;
			$this->ponderados = true;
			$this->moduloIndicadores = 'indicadoresCapacidad';
			//$this->setTemplate('indicadoresDetalleCapacidadHorasDias');
		}
		elseif($caso == 'tasa_ocupacion_40')
		{
			$this->modulo = 'TasaOcupacion40';
			$this->opciones_titulo = "Tasa Minutos Utilizados";
			$this->opciones_subtitulo = "Tasa de Minutos Utilizados Totales Ponderado al 40%";
			$this->opciones_yaxis = "Minutos Utilizados / Minutos Disponibles ";
			$this->hayAcumulados = false;
			$this->hayMetas = false;
			$this->ponderados = true;
			$this->moduloIndicadores = 'indicadoresCapacidad';
			//$this->setTemplate('indicadoresDetalleCapacidadHorasDias');
		}

		elseif($caso == 'ocupacion_horas')
		{
			$this->modulo = 'OcupacionPorHora';
			$this->opciones_titulo = "Minutos Utilizados por Hora";
			$this->opciones_subtitulo = "Total de Minutos Utilizados por Hora";
			$this->opciones_yaxis = "Minutos (miles)";
			$this->hayAcumulados = true;
			$this->moduloIndicadores = 'indicadoresCapacidad';
			//$this->setTemplate('indicadoresDetalleCapacidadHorasDias');
		}
		elseif($caso == 'tasa_ocupacion_horas')
		{
			$this->modulo = 'TasaOcupacionPorHora';
			$this->opciones_titulo = "Tasa Minutos Utilizados por Hora";
			$this->opciones_subtitulo = "Tasa de Minutos Utilizados por Hora";
			$this->opciones_yaxis = "Minutos Utilizados / Minutos Disponibles";
			$this->hayAcumulados = true;
			$this->moduloIndicadores = 'indicadoresCapacidad';
			//$this->setTemplate('indicadoresDetalleCapacidadHorasDias');
		}
		elseif($caso == 'ocupacion_dias')
		{
			$this->modulo = 'OcupacionPorDia';
			$this->opciones_titulo = "Minutos Utilizados por Día";
			$this->opciones_subtitulo = "Total de Minutos Utilizados por Día";
			$this->opciones_yaxis = "Minutos Utilizados / Minutos Disponibles";
			$this->hayAcumulados = true;
			$this->moduloIndicadores = 'indicadoresCapacidad';
			//$this->setTemplate('indicadoresDetalleCapacidadHorasDias');
		}
		elseif($caso == 'tasa_ocupacion_dias')
		{
			$this->modulo = 'TasaOcupacionPorDia';
			$this->opciones_titulo = "Tasa Minutos Utilizados por Hora";
			$this->opciones_subtitulo = "Tasa de Minutos Utilizados por Día";
			$this->opciones_yaxis = "Minutos Utilizados / Minutos Disponibles";
			$this->hayAcumulados = true;
			$this->moduloIndicadores = 'indicadoresCapacidad';
			//$this->setTemplate('indicadoresDetalleCapacidadHorasDias');
		}
		elseif($caso == 'estatus_socios')
		{
			$this->modulo = 'EstatusSocios';
			$this->opciones_titulo = "Estatus de Socios";
			$this->opciones_subtitulo = "Socios Nuevos, Activos y Totales";
			$this->opciones_yaxis = "Socios";
			$this->controlesEstatusSocios = true;
			$this->moduloIndicadores = 'indicadoresDetalleZoom?caso=estatus_socios';
		}
	}

	/**
	 * Muestra el partial de subdetalle de cobranza
	 *
	 * @param Object $request
	 */
	public function executeSubDetalleCobranza($request)
	{

		$fI = pg_escape_string($request->getParameter('fecha_ini'));
		$fF = pg_escape_string($request->getParameter('fecha_fin'));

		if($fI == '' or $fI == null) $fI = '2010-09-01';
		if($fF == '' or $fF == null) $fF = date("Y-m-d");
		//error_log('Tipo:'.$request->getParameter('tipo'));

		$nivel =  intval($request->getParameter('nivel'));
		if($nivel == 0) {
			$sqNivel = "date_trunc('month',fecha )::date as mes,";
		}
		if($nivel == 1) {
			$sqNivel = "date_trunc('week',fecha )::date as mes,";
		}
		if($nivel == 2) {
			$sqNivel = "date_trunc('day',fecha )::date as mes,";
		}

		if($request->getParameter('tipo') == 'zona')
		{
			$zona_id = intval(pg_escape_string($request->getParameter('id')));
			if($zona_id != 0)
			{
				$zona = ZonaPeer::retrieveByPK($zona_id);
			}
				
			$sql = "
			SELECT
				
			$sqNivel
			SUM(monto) AS cantidad,
			categoria
			FROM reporte_ventas_categorias AS r
			JOIN centro AS c ON (c.id = r.centro_id AND c.zona_id = $zona_id)
			AND fecha BETWEEN '$fI' AND '$fF'
			GROUP BY mes,categoria
			ORDER BY mes;			";
			$ns1 = $zona.'';
				
		}
		elseif($request->getParameter('tipo') == 'centro')
		{
			$centro_id = intval(pg_escape_string($request->getParameter('id')));
				
			if($centro_id != 0)
			{
				$centro = CentroPeer::retrieveByPK($centro_id);
			}
			$sql = "
			SELECT

			$sqNivel
			SUM(monto) AS cantidad,
			categoria
			FROM reporte_ventas_categorias AS r
			JOIN centro AS c ON (c.id = r.centro_id AND c.id = $centro_id)
			WHERE
			fecha BETWEEN '$fI' AND '$fF'
			AND c.id = $centro_id
			GROUP BY mes,categoria
			ORDER BY mes
			";
			$ns1 = 'Cobranza';
			$ns2 = 'Acumulado';
		}
		else
		{
			$sql = "
			SELECT
			$sqNivel
			SUM(monto) AS cantidad,
			categoria
			FROM reporte_ventas_categorias AS r
			WHERE
			fecha BETWEEN '$fI' AND '$fF'
			GROUP BY mes,categoria
			ORDER BY mes
			";
			$ns1 = 'Cobranza';
			$ns2 = 'Acumulado';
		}
			
		$conn = Propel::getConnection();
		$d = array();
		$acumulado=0;
		//error_log(str_replace("\t", "", str_replace("\n", " ", $sql)));
		foreach($conn->query($sql) as $r)
		{

			$unixtime = strtotime($r['mes']);
			$jsunixtime=$unixtime*1000;

			$cantidad = $r['cantidad'];
			$acumulado = $acumulado + $cantidad;
			$categoria = $r['categoria'];

			/*$d['Acumulado'][] = array(
				$jsunixtime,
					floatval(number_format($acumulado,2)),
			);
			*/
			//error_log("Cat:".$categoria." fecha:".$r['mes']." = ".$cantidad);
			$d[$categoria][] = array(
					$jsunixtime,
					floatval($cantidad)
						
			);
		}

		//error_log(print_r($d,true),3,'/tmp/salida.log');

		$this->getResponse()->setContentType('application/json');
		return $this->renderText(json_encode($d));
	}

	/**
	 * Muestra el partial de subSUBdetalle de cobranza
	 *
	 * @param Object $request
	 */
	public function executeSubSubDetalleCobranza($request)
	{

		$fI = pg_escape_string($request->getParameter('fecha_ini'));
		$fF = pg_escape_string($request->getParameter('fecha_fin'));

		if($fI == '' or $fI == null) $fI = '2010-09-01';
		if($fF == '' or $fF == null) $fF = date("Y-m-d");

		$tipo = $request->getParameter('tipo');

		$nivel =  intval($request->getParameter('nivel'));
		if($nivel == 0) {
			$sqNivel = "date_trunc('month',fecha )::date as mes,";
		}
		if($nivel == 1) {
			$sqNivel = "date_trunc('week',fecha )::date as mes,";
		}
		if($nivel == 2) {
			$sqNivel = "date_trunc('day',fecha )::date as mes,";
		}

		$id = intval(pg_escape_string($request->getParameter('id')));

		if($tipo == 'zona')
		{
			$zona_id = $id;
			if($zona_id != 0)
			{
				$zona = ZonaPeer::retrieveByPK($zona_id);
			}
				
			$sql = "
			SELECT
			$sqNivel
			sum(monto) AS monto,
			sum(cantidad) AS cantidad,
			siglas,
			categoria_nombre
			FROM reporte_categoria_cursos AS r
			JOIN centro AS c ON (c.id = r.centro_id)
			WHERE
			fecha BETWEEN '$fI' AND '$fF'
			AND zona_id = $zona_id
			GROUP BY
			mes,siglas,categoria_nombre
			ORDER BY
			mes,categoria_nombre";
			$ns1 = $zona.'';
				
		}
		elseif($tipo == 'centro')
		{
			$centro_id = $id;
				
			if($centro_id != 0)
			{
				$centro = CentroPeer::retrieveByPK($centro_id);
			}
			$sql = "
			SELECT
			$sqNivel
			sum(monto) AS monto,
			sum(cantidad) AS cantidad,
			siglas,
			categoria_nombre
			FROM reporte_categoria_cursos AS r
			JOIN centro AS c ON (c.id = r.centro_id)
			WHERE
			fecha BETWEEN '$fI' AND '$fF'
			AND centro_id = $centro_id
			GROUP BY
			mes,siglas,categoria_nombre
			ORDER BY
			mes,categoria_nombre";

			$ns1 = 'Cobranza';
			$ns2 = 'Acumulado';
		}
		else
		{
			$sql = "
			SELECT
			$sqNivel
			sum(monto) AS monto,
			sum(cantidad) AS cantidad,
			siglas,
			categoria_nombre
			FROM reporte_categoria_cursos AS r
			JOIN centro AS c ON (c.id = r.centro_id)
			WHERE
			fecha BETWEEN '$fI' AND '$fF'
			GROUP BY
			mes,siglas,categoria_nombre
			ORDER BY
			mes,categoria_nombre
			";
			$ns1 = 'Cobranza';
			$ns2 = 'Acumulado';
		}
			
		$conn = Propel::getConnection();
		$d = array();
		$acumulado=0;
		foreach($conn->query($sql) as $r)
		{

			$unixtime = strtotime($r['mes']);
			$jsunixtime=$unixtime*1000;

			$cantidad = $r['monto'];
			$acumulado = $acumulado + $cantidad;
			$categoria = $r['siglas']." ".$r['categoria_nombre'];

			/*$d['Acumulado'][] = array(
				$jsunixtime,
					floatval(number_format($acumulado,2)),
			);
			*/
			//error_log("Cat:".$categoria." fecha:".$r['mes']." = ".$cantidad);
			$d[$categoria][] = array(
					$jsunixtime,
					floatval($cantidad)
						
			);
		}

		$this->getResponse()->setContentType('application/json');
		return $this->renderText(json_encode($d));
	}

	public function executeSubDetalle($request)
	{

		$tipo = $request->getParameter('tipo');
		$id = $request->getParameter('id');
		$fecha = $request->getParameter('fecha');
		$modulo = $request->getParameter('modulo');
		return $this->renderPartial('reporte/subDetalle',array(
				'modulo'=>$modulo,
				'fecha'=>$fecha,
				'id'=>$id,
				'tipo'=>$tipo,
		));
	}

	public function executeSubSubDetalle($request)
	{

		$tipo = $request->getParameter('tipo');
		$id = $request->getParameter('id');
		$fecha = $request->getParameter('fecha');
		$modulo = $request->getParameter('modulo');
		return $this->renderPartial('reporte/subSubDetalle',array(
				'modulo'=>$modulo,
				'fecha'=>$fecha,
				'id'=>$id,
				'tipo'=>$tipo,
		));
	}

	public function executeListaZonas($request)
	{
		$zonas = array();
		foreach(ZonaPeer::listaZonas() as $zona)
		{
			//$zona = new Zona();
			foreach($zona->getCentros() as $centro)
			{
				$zonas[$zona->getId()][]=$centro->getId();
			}
		}
		$this->getResponse()->setContentType('application/json');
		return $this->renderText(json_encode($zonas));
	}



	public function executeOcupacion($request)
	{
		$ctx = sfContext::getInstance();
		$ctx->getConfiguration()->loadHelpers('ReportesIndicadores');

		$fI = pg_escape_string($request->getParameter('fecha_ini'));
		$fF = pg_escape_string($request->getParameter('fecha_fin'));

		$zona_id = intval(pg_escape_string($request->getParameter('zona')));
		$centro_id = intval(pg_escape_string($request->getParameter('centro_id')));

		$nivel =  intval($request->getParameter('nivel'));

		$this->getResponse()->setContentType('application/json');
		$d = array();
		$d = ocupacion($fI, $fF, $centro_id, $zona_id, $nivel);
		return $this->renderText(json_encode($d));

	}

	public function executeTasaOcupacion($request)
	{
		$ctx = sfContext::getInstance();
		$ctx->getConfiguration()->loadHelpers('ReportesIndicadores');

		$fI = pg_escape_string($request->getParameter('fecha_ini'));
		$fF = pg_escape_string($request->getParameter('fecha_fin'));

		$zona_id = intval(pg_escape_string($request->getParameter('zona')));
		$centro_id = intval(pg_escape_string($request->getParameter('centro_id')));
		$nivel =  intval($request->getParameter('nivel'));

		$hora_ini = intval(pg_escape_string($request->getParameter('hora_ini')));
		$hora_fin = intval(pg_escape_string($request->getParameter('hora_fin')));

		$this->getResponse()->setContentType('application/json');

		$d = array();
		$d = tasaOcupacion($fI, $fF, $centro_id, $zona_id, $nivel, 0, $hora_ini, $hora_fin);
		return $this->renderText(json_encode($d));
	}

	public function executeTasaOcupacion60($request)
	{
		$ctx = sfContext::getInstance();
		$ctx->getConfiguration()->loadHelpers('ReportesIndicadores');

		$fI = pg_escape_string($request->getParameter('fecha_ini'));
		$fF = pg_escape_string($request->getParameter('fecha_fin'));

		$ponderacion = 0.6;

		$zona_id = intval(pg_escape_string($request->getParameter('zona')));
		$centro_id = intval(pg_escape_string($request->getParameter('centro_id')));
		$nivel =  intval($request->getParameter('nivel'));

		$hora_ini = intval(pg_escape_string($request->getParameter('hora_ini')));
		$hora_fin = intval(pg_escape_string($request->getParameter('hora_fin')));

		$this->getResponse()->setContentType('application/json');

		$d = array();
		$d = tasaOcupacion($fI, $fF, $centro_id, $zona_id, $nivel, $ponderacion, $hora_ini, $hora_fin);
		return $this->renderText(json_encode($d));
	}

	public function executeTasaOcupacion40($request)
	{
		$ctx = sfContext::getInstance();
		$ctx->getConfiguration()->loadHelpers('ReportesIndicadores');

		$fI = pg_escape_string($request->getParameter('fecha_ini'));
		$fF = pg_escape_string($request->getParameter('fecha_fin'));

		$ponderacion = 0.4;

		$zona_id = intval(pg_escape_string($request->getParameter('zona')));
		$centro_id = intval(pg_escape_string($request->getParameter('centro_id')));
		$nivel =  intval($request->getParameter('nivel'));

		$hora_ini = intval(pg_escape_string($request->getParameter('hora_ini')));
		$hora_fin = intval(pg_escape_string($request->getParameter('hora_fin')));

		$this->getResponse()->setContentType('application/json');

		$d = array();
		$d = tasaOcupacion($fI, $fF, $centro_id, $zona_id, $nivel, $ponderacion, $hora_ini, $hora_fin);
		return $this->renderText(json_encode($d));
	}


	public function executeOcupacionPorHora($request)
	{
		$ctx = sfContext::getInstance();
		$ctx->getConfiguration()->loadHelpers('ReportesIndicadores');

		$fI = pg_escape_string($request->getParameter('fecha_ini'));
		$fF = pg_escape_string($request->getParameter('fecha_fin'));

		$zona_id = intval(pg_escape_string($request->getParameter('zona')));
		$centro_id = intval(pg_escape_string($request->getParameter('centro_id')));
		$nivel =  intval($request->getParameter('nivel'));

		$this->getResponse()->setContentType('application/json');

		$d = array();
		$d = ocupacionPorHora($fI, $fF, $centro_id, $zona_id, $nivel);
		return $this->renderText(json_encode($d));
	}

	public function executeTasaOcupacionPorHora($request)
	{
		$ctx = sfContext::getInstance();
		$ctx->getConfiguration()->loadHelpers('ReportesIndicadores');

		$fI = pg_escape_string($request->getParameter('fecha_ini'));
		$fF = pg_escape_string($request->getParameter('fecha_fin'));

		$zona_id = intval(pg_escape_string($request->getParameter('zona')));
		$centro_id = intval(pg_escape_string($request->getParameter('centro_id')));
		$nivel =  intval($request->getParameter('nivel'));

		$this->getResponse()->setContentType('application/json');

		$d = array();
		$d = tasaOcupacionPorHora($fI, $fF, $centro_id, $zona_id, $nivel);
		return $this->renderText(json_encode($d));
	}


	public function executeOcupacionPorDia($request)
	{
		$ctx = sfContext::getInstance();
		$ctx->getConfiguration()->loadHelpers('ReportesIndicadores');

		$fI = pg_escape_string($request->getParameter('fecha_ini'));
		$fF = pg_escape_string($request->getParameter('fecha_fin'));

		$zona_id = intval(pg_escape_string($request->getParameter('zona')));
		$centro_id = intval(pg_escape_string($request->getParameter('centro_id')));
		$nivel =  intval($request->getParameter('nivel'));

		$this->getResponse()->setContentType('application/json');

		$d = array();
		$d = ocupacionPorDia($fI, $fF, $centro_id, $zona_id, $nivel);
		return $this->renderText(json_encode($d));
	}

	public function executeTasaOcupacionPorDia($request)
	{
		$ctx = sfContext::getInstance();
		$ctx->getConfiguration()->loadHelpers('ReportesIndicadores');

		$fI = pg_escape_string($request->getParameter('fecha_ini'));
		$fF = pg_escape_string($request->getParameter('fecha_fin'));

		$zona_id = intval(pg_escape_string($request->getParameter('zona')));
		$centro_id = intval(pg_escape_string($request->getParameter('centro_id')));
		$nivel =  intval($request->getParameter('nivel'));

		$this->getResponse()->setContentType('application/json');

		$d = array();
		$d = tasaOcupacionPorDia($fI, $fF, $centro_id, $zona_id, $nivel);
		return $this->renderText(json_encode($d));
	}

	public function executeEstatusSocios($request)
	{
		$ctx = sfContext::getInstance();
		$ctx->getConfiguration()->loadHelpers('ReportesIndicadores');

		$fI = pg_escape_string($request->getParameter('fecha_ini'));
		$fF = pg_escape_string($request->getParameter('fecha_fin'));

		$zona_id = intval(pg_escape_string($request->getParameter('zona')));
		$centro_id = intval(pg_escape_string($request->getParameter('centro_id')));
		$nivel =  intval($request->getParameter('nivel'));

		$this->getResponse()->setContentType('application/json');

		$d = array();
		$d = sociosActivos($fI, $fF, $centro_id, $zona_id, $nivel);
		return $this->renderText(json_encode($d));
	}

	public function executeIndicadoresEstatusSocios($request)
	{
		/*$this->Zonas = ZonaPeer::listaZonas();
		 $this->Centros = CentroPeer::listaCentrosObj();

		$this->modulo = 'EstatusSocios';
		$this->opciones_titulo = "Estatus de Socios";
		$this->opciones_subtitulo = "Socios Nuevos, Activos y Totales";
		$this->opciones_yaxis = "Socios";
		$this->hayAcumulados = true;
		*/
		$this->redirect('reporte/indicadoresDetalleZoom?caso=estatus_socios');
	}

	public function executeIndicadoresDetalleCapacidad($request)
	{

		$this->Zonas = ZonaPeer::listaZonas();
		$this->Centros = CentroPeer::listaCentrosObj();

		$fI = pg_escape_string($request->getParameter('fecha_ini'));
		$fF = pg_escape_string($request->getParameter('fecha_fin'));
		$caso = pg_escape_string($request->getParameter('caso'));

		if($caso == 'minutos_utilizados')
		{
			$this->modulo = 'Ocupacion';
			$this->opciones_titulo = "Minutos Utilizados";
			$this->opciones_subtitulo = "Total de Minutos Utilizados";
			$this->opciones_yaxis = "Minutos";
			$this->hayAcumulados = false;
		}
		elseif($caso == 'tasa_minutos_utilizados')
		{
			$this->modulo = 'TasaOcupacion';
			$this->opciones_titulo = "Tasa Minutos Utilizados";
			$this->opciones_subtitulo = "Tasa de Minutos Utilizados Totales";
			$this->opciones_yaxis = "Minutos Utilizados / Minutos Disponibles ";
			$this->hayAcumulados = false;
			$this->setTemplate('indicadoresDetalleCapacidadHorasDias');
		}
		elseif($caso == 'ocupacion_horas')
		{
			$this->modulo = 'OcupacionPorHora';
			$this->opciones_titulo = "Minutos Utilizados por Hora";
			$this->opciones_subtitulo = "Total de Minutos Utilizados por Hora";
			$this->opciones_yaxis = "Minutos (miles)";
			$this->hayAcumulados = true;
			$this->setTemplate('indicadoresDetalleCapacidadHorasDias');
		}
		elseif($caso == 'tasa_ocupacion_horas')
		{
			$this->modulo = 'TasaOcupacionPorHora';
			$this->opciones_titulo = "Tasa Minutos Utilizados por Hora";
			$this->opciones_subtitulo = "Tasa de Minutos Utilizados por Hora";
			$this->opciones_yaxis = "Minutos Utilizados / Minutos Disponibles";
			$this->hayAcumulados = true;
			$this->setTemplate('indicadoresDetalleCapacidadHorasDias');
		}
		elseif($caso == 'ocupacion_dias')
		{
			$this->modulo = 'OcupacionPorDia';
			$this->opciones_titulo = "Minutos Utilizados por Día";
			$this->opciones_subtitulo = "Total de Minutos Utilizados por Día";
			$this->opciones_yaxis = "Minutos Utilizados / Minutos Disponibles";
			$this->hayAcumulados = true;
			$this->setTemplate('indicadoresDetalleCapacidadHorasDias');
		}
		elseif($caso == 'tasa_ocupacion_dias')
		{
			$this->modulo = 'TasaOcupacionPorDia';
			$this->opciones_titulo = "Tasa Minutos Utilizados por Hora";
			$this->opciones_subtitulo = "Tasa de Minutos Utilizados por Día";
			$this->opciones_yaxis = "Minutos Utilizados / Minutos Disponibles";
			$this->hayAcumulados = true;
			$this->setTemplate('indicadoresDetalleCapacidadHorasDias');
		}

	}

	/**
	 * Dado un rango de fechas regresa el los reportes de asistencia
	 *
	 * @WSMethod(webservice='MakoApi')
	 * @param string $desde Fecha de inicio de la consulta
	 * @param string $hasta Fecha de fin de la consulta
	 * @return string Regresa un arreglo serializado del tipo $res[]=$reporte_metas
	 */
	public function executeGetReporteAsistencias(sfWebRequest $request)
	{
		$configuration = ProjectConfiguration::getApplicationConfiguration( 'frontend', 'tolu2', true);
		$context = sfContext::createInstance($configuration);
		$centro_id = sfConfig::get('app_centro_actual_id');
		$desde = pg_escape_string($request->getParameter('desde'));
		$hasta = pg_escape_string($request->getParameter('hasta'));
		$reporte = ReporteAsistenciasPeer::consolidaReporteAsistencias($desde, $hasta, $centro_id);

		$this->result = serialize($reporte);

		if(count($reporte) == 0)
		{
			$me = new SoapFault('Server', "[GetReporteAsistencias]: No existen registros para el rango de fechas consultadas $desde a $hasta en el centro_id $centro_id.");
			throw $me;
			return sfView::ERROR;
		}
		else
		{
			return sfView::SUCCESS;
		}
	}
	/**
	 * Muestra la pantalla de consulta de inactivos
	 *
	 * @param Request $request
	 */
	public function executeListaInactivos($request)
	{
		$this->Zonas = ZonaPeer::listaZonas();
		$this->Centros = CentroPeer::listaCentrosObj();

	}

	/**
	 * Muestra la lista de inactivos mediante el web service de consulta a todos los centros.
	 *
	 * @param unknown_type $request
	 */
	public function executeDatosSociosInactivos($request)
	{

		$centro_id = intval(pg_escape_string($request->getParameter('centro_id')));
		$zona_id = intval(pg_escape_string($request->getParameter('zona_id')));

		$ctx = sfContext::getInstance();
		$ctx->getConfiguration()->loadHelpers('ReportesIndicadores');

		$fecha = date("Y-m-d");
		$tabla_id = "C-$centro_id-Z-$zona_id";
		$d = SocioPeer::sociosInactivos($centro_id, $zona_id, $fecha);

		return $this->renderPartial('listaSociosInactivos',array('socios' => $d,'tabla_id'=>$tabla_id));

	}


	public function executeServidoresSinRespuesta($request)
	{
		$d = ReporteServidoresPeer::sinRespuesta();
		$this->getResponse()->setContentType('application/json');
		return $this->renderText(json_encode($d));
	}

}
