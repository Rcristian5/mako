<?php

/**
 * components components.
 *
 * @package    mako
 * @subpackage components
 * @author     David
 * @version    SVN: $Id: components.class.php,v 1.2 2011-06-24 03:00:24 david Exp $
 */

class ReporteComponents extends sfComponents
{
	/**
	 * Componente para mostrar en un mapa los socios registrados
	 *
	 */
	public function executeReporteMapaSocios()
	{
		ini_set("memory_limit","512M");
		$this->socios = json_encode(SocioPeer::sociosRegistradosCentro(sfConfig::get('app_centro_actual_id')));
		$centro = CentroPeer::retrieveByPK(sfConfig::get('app_centro_actual_id'));
		$this->lat = $centro->getLat();
		$this->long = $centro->getLong();
		$this->centro = $centro->getNombre();
	}
}