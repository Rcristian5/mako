<fieldset id="fs_<?php echo $socio->getId() ?>" class="socio-encontrado">
	<legend>
		Folio:
		<?php echo $socio->getFolio() ?>
	</legend>

	<table width="100%" border="0">
		<tr>
			<td width="10%">
				<div class="container fotoChica">
					<img
						src="<?php echo ($socio->getFoto() != '')? '/fotos/'.$socio->getFoto() : '/imgs/sin_foto_sh.jpg' ?>"
						class="main fotoChica" /> <img class="minor fotoChica"
						src="/imgs/frame.png">
				</div>
			</td>
			<td width="90%"><?php echo $socio->getNombreCompleto() ?><br /> <?php echo $socio->getUsuario() ?><br />
				<?php echo $socio->getEdad() ?> años<br /></td>
		</tr>
		<tr>
			<td colspan="2">Alta en: <?php echo $socio->getCentro()->getNombre() ?>
				<br /> Fecha Nac: <?php echo $socio->getFechaNac("%d-%m-%Y") ?><br />
				Género: <?php echo ($socio->getSexo()=='M') ? 'Masculino':'Femenino' ?><br />
				Estudia: <?php echo ($socio->getEstudia()=='1') ? (($socio->getNivelEstudio()!=null)?$socio->getNivelEstudio():'Si'):'No' ?><br />
				Trabaja: <?php echo ($socio->getTrabaja()=='1') ? 'Si':'No' ?><br />
				Tiempo PC: <?php echo Comun::segsATxt($socio->getSaldo())   ?> <br>
		
		</tr>
	</table>
</fieldset>
