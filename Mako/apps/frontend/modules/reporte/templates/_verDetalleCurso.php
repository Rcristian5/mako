<script>

	$(function() {
		var $tabsvd = $('#tabsVerDetalle').tabs();
	
	});
	
	
	var oTable;
	$(document).ready(function() {
	
	   oTable = $('.registros').dataTable({
			"bPaginate": true,
			"bLengthChange": true,
			"bFilter": true,
			"bSort": true,
			"bInfo": true,
			"bAutoWidth": false,
	        "bJQueryUI": true,
	        "sPaginationType": "full_numbers"
	     });
	   
	});
</script>


<?php use_stylesheet('./reporte/_verDetalleCurso.css')?>


<div id="contenedorVerDetalle">
	<div id="tabsVerDetalle">

		<ul id="tabMenu-vd">
			<li><a href="#personales-vd">Detalle</a></li>
		</ul>


		<div id="personales-vd" style="width: 80%;">

			<h3>Detalle</h3>
			<table width="850" border="0" id="registrosCompras" class="registros">
				<thead>
					<tr>
						<th>Producto</th>
						<th>$</th>
						<th>Detalle</th>
					</tr>
				</thead>
				<tbody>

					<?php 
					foreach($Detalle as $row)
					{
						?>
					<!-- BEGIN carrito -->
					<tr>
						<td><?php echo $row['nombre']; ?></td>
						<td>$<?php echo $row['precio_lista']; ?>
						</td>
						<td><a href="#"
							onClick="verDetalle('', '<?php echo $row['curso']; ?>', 'Detalle',  <?php echo $row['id']; ?>)">
								Ver detalle</a></td>
					</tr>
					<!-- END carrito -->

					<?php  } ?>
				</tbody>
			</table>
		</div>


	</div>



</div>
</div>


