<div class="ui-widget">
	<div class="ui-widget-content ui-corner-all" style="padding: 5px;">

		<table id="lista-ranking" width="400" class="datos">
			<caption>Ranking Total</caption>
			<thead>
				<tr>
					<th>Lugar</th>
					<th>Centro</th>
					<th>Ranking Total</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($rnkGeneral as $centro => $val):?>
				<?php $lugar++; ?>
				<tr>
					<td class="" align="center"><?php echo $lugar ?>
					</td>
					<td class="" align="right"><?php echo $centro ?>
					</td>
					<td class="" align="right"><?php echo $val ?>
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

	</div>
</div>
