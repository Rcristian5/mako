<div class="ui-widget">
	<div class="ui-widget-content ui-corner-all" style="padding: 5px;">

		<table id="lista-ranking" width="400" class="datos">
			<caption>Factor 6 (F6): Venta Total entre Aulas disponibles (en $)</caption>
			<thead>
				<tr>
					<th class="ui-state-default">Lugar</th>
					<th class="ui-state-default">Centro</th>
					<th class="ui-state-default">Vta TTL /Aula ($)</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($RankF6 as $Ranking): ?>
				<?php 

				$lugar++;
				 
				?>
				<tr>
					<td class="" align="center"><?php echo $lugar ?>
					</td>
					<td class="" align="right"><?php echo $Ranking['centro'] ?>
					</td>
					<td class="" align="right"><?php echo number_format($Ranking['cantidad'],2) ?>
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

	</div>
</div>
