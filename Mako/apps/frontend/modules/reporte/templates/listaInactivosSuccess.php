<?php use_stylesheet('TableTools.css') ?>
<?php use_stylesheet('datatables.css') ?>

<script
	type="text/javascript" src="/js/jquery.1.6.min.js"></script>
<script
	type="text/javascript" src="/js/jquery.ui.js"></script>

<script
	type="text/javascript" src="/js/charts/highcharts.src.js"></script>
<script
	type="text/javascript" src="/js/plugins/jquery.addcommas.js"></script>
<script
	type="text/javascript" src="/js/plugins/jquery.dataTables.js"></script>
<script
	type="text/javascript" src="/js/plugins/dataTables/TableTools.js"></script>
<script
	type="text/javascript" src="/js/plugins/dataTables/ZeroClipboard.js"></script>

<?php use_stylesheet('./reporte/listaInactivosSuccess.css') ?>
<?php use_stylesheet('datatables.css') ?>

<script type="text/javascript">
		var tOpc = {
				"bPaginate": true,
				"bLengthChange": false,
				"bFilter": true,
				"bSort": true,
				"bInfo": true, 
				"bAutoWidth": false,
			    "bJQueryUI": true,
			    "aaSorting": [],
			    "sPaginationType": "full_numbers"
				
			};

		var tXopc = {
				"bPaginate": true,
				"bLengthChange": true,
				"bFilter": true,
				"bSort": true,
				"bInfo": true,
				"bAutoWidth": false,
		        "bJQueryUI": true,
		        "sPaginationType": "full_numbers",
		        "aaSorting": [],
		        "sDom": 'T<"fg-toolbar ui-widget-header ui-corner-tl ui-corner-tr ui-helper-clearfix"lfr>t<"fg-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix"ip>'
	    
		     };
		
		$(document).ready(function() {

			TableToolsInit.oFeatures = {
					"bCsv": true,
					"bXls": true,
					"bCopy": true,
					"bPrint": false
				};

		var arZonas = new Object();
		jQuery.get('<?php echo url_for('reporte/listaZonas')?>', null, function(datos, state, xhr) {
			arZonas = datos;	
		});
		var tCentros = 	$('#lista_centros').dataTable(tOpc);	
			
	
//Controles
/* BIND POR CENTROS */
				$('#centros').bind('centro',function(e){

					
					var idx = $(e.target).val();
					
					if($(e.target).attr('checked')) 
					{ 
						$(e.target).attr('disabled','disabled');
						var params = {centro_id:idx};
						jQuery.get('<?php echo url_for('reporte/datosSociosInactivos')?>', params, function(datos, state, xhr) {
							$('#tablas').append(datos);
							$('#lista-C-'+idx+'-Z-0').dataTable(tXopc);
							$(e.target).removeAttr('disabled');
						});	
					}
					else
					{
						$('#lista-C-'+idx+'-Z-0').remove();
						$('#lista-C-'+idx+'-Z-0_wrapper').remove();
					}
				});


				/* BIND POR ZONAS */
				$('#zonas').bind('zona',function(e){

					var idx = $(e.target).val();
					
					if($(e.target).attr('checked')) 
					{ 
						$(e.target).attr('disabled','disabled');
						var params = {zona_id:idx};
						jQuery.get('<?php echo url_for('reporte/datosSociosInactivos')?>', params, function(datos, state, xhr) {
							$('#tablas').append(datos);
							$('#lista-C-0-Z-'+idx).dataTable(tXopc);
							$(e.target).removeAttr('disabled');
						});	
					}
					else
					{
						$('#lista-C-0-Z-'+idx).remove();
						$('#lista-C-0-Z-'+idx+'_wrapper').remove();
					}
				});



				$('#carga').hide();
				$('#carga').ajaxStart(function() {
			        $(this).fadeIn('slow');
			    });
				$('#carga').ajaxStop(function() {
			        $(this).fadeOut('slow');
			    });
				
		});				


	
		
		</script>




<div id="cont_general">

	<div class="clearfix">

		<div id="cont_grafica" class="cg ui-corner-all ui-widget"
			style="border: thin solid silver; min-height: 400px;">
			<div class="ui-widget-header ui-corner-top tsss"
				style="padding: 5px;">Listas de socios inactivos</div>
			<div id="tablas"></div>

		</div>

		<div style="width: 20%; margin-left: 10px; float: right;"
			class="clearfix">

			<div align="center">
				<div style="height: 20px;">
					<img id="carga" src="/imgs/ajax-loader.gif" />
				</div>
				<div id="zonas">
					<?php include_partial('listaZonas', array('Zonas'=>$Zonas))?>
				</div>
				<hr>

				<div id="centros">
					<?php include_partial('listaCentros', array('Centros'=>$Centros))?>
				</div>
			</div>
		</div>
	</div>



	<div id="sub_detalle_ventana">
		<div id="sub_detalle"></div>
	</div>

</div>
