<script type="text/javascript" src="/js/jquery.1.6.min.js"></script>
<script
	type="text/javascript" src="/js/charts/highcharts.src.js"></script>

<script
	type="text/javascript" src="/js/plugins/jquery.addcommas.js"></script>
<script type="text/javascript">
			var chartc;
			var chartc2;
			$(document).ready(function() {

				
				chartc = new Highcharts.Chart({
					chart: {
						renderTo: 'cobranza_tot',
						shadow: true,
				        events: {
					        click: function(){
						        window.location = '<?php echo url_for('reporte/indicadoresDetalleZoom')."?caso=cobranza_total"?>';
					        },
				            selection: function(event) {
				                if (event.xAxis) {
				                	console.log(
				                			Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', event.xAxis[0].min),
				                			Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', event.xAxis[0].max)
				                		);
				                } else {
				                    console.log('Selection reset');
				                }
				            }				            
				        },
						
						type: 'spline',
						animation: false,
						height: 300
					},
					credits: {
						enabled: false
					},
					title: {
						text: 'Cobranza'
					},
					subtitle: {
						text: 'Cobranza Total y Metas'	
					},
					xAxis: {
						type: 'datetime',
						dateTimeLabelFormats: {
							month: '%b %Y',
							year: '%Y'
						}
					},
					yAxis: {
						title: {
							text: 'Pesos ($)'
						},
					    labels: {
					    	formatter: function() {
					        	return $.fn.addCommas(this.value);
							}
					    },
						min: 0
					},
					tooltip: {
						formatter: function() {
				                return '<b>'+ this.series.name +'</b><br/>'+
								Highcharts.dateFormat('%b %Y', this.x) +': $ '+ $.fn.addCommas(this.y);
						}
					}
				});
				
				var datos;
				jQuery.get('<?php echo url_for('reporte/CobranzaTotales')?>', null, function(datos, state, xhr) {
					
					jQuery.each(datos, function(i, val) {
						chartc.addSeries({
							id:i,
							name:i,
							data:val
						});
						
					});
				});


				chartc2 = new Highcharts.Chart({
					chart: {
						renderTo: 'cobranza_meta',
						shadow: true,
				        events: {
					        click: function(){
						        window.location = '<?php echo url_for('reporte/indicadoresDetalleZoom')."?caso=cobranza_total_cursos"?>';
					        },
				            selection: function(event) {
				                if (event.xAxis) {
				                	console.log(
				                			Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', event.xAxis[0].min),
				                			Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', event.xAxis[0].max)
				                		);
				                } else {
				                    console.log('Selection reset');
				                }
				            }				            
				        },
						
						type: 'spline',
						animation: false,
						height: 300
					},
					credits: {
						enabled: false
					},
					title: {
						text: 'Cobranza y Meta por Curso'
					},
					subtitle: {
						text: 'Cobrabza Total y Meta por Curso'	
					},
					xAxis: {
						type: 'datetime',
						dateTimeLabelFormats: {
							month: '%b %Y',
							year: '%Y'
						}
					},
					yAxis: {
						title: {
							text: 'Pesos ($)/Curso'
						},
					    labels: {
					    	formatter: function() {
					        	return $.fn.addCommas(this.value);
							}
					    },
						min: 0
					},
					tooltip: {
						formatter: function() {
				                return '<b>'+ this.series.name +'</b><br/>'+
								Highcharts.dateFormat('%b %Y', this.x) +': $ '+ $.fn.addCommas(this.y);
						}
					}
				});
				
				var datos;
				jQuery.get('<?php echo url_for('reporte/CobranzaCurso')?>', null, function(datos, state, xhr) {
					
					jQuery.each(datos, function(i, val) {
						chartc2.addSeries({
							id:i,
							name:i,
							data:val
						});
						
					});
				});


				
			});
				
		</script>

<div
	id="cobranza_tot" style="width: 400px; height: 320px; margin: 0 auto"></div>
<div
	id="cobranza_meta" style="width: 400px; height: 320px; margin: 0 auto"></div>

