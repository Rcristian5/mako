<script>
	//datatable
	$(document).ready(function() {		
		$('.dataTableDetalle').dataTable({
			"bPaginate": false,
			"bLengthChange": true,
			"bFilter": false,
			"bSort": true,
			"bInfo": false,
			"bAutoWidth": false,
	        "bJQueryUI": true,
	        "sPaginationType": "full_numbers",
	        "aaSorting": [],
	        "sDom": 'T<"clear">lfrtip'     
	     });
		$('#ttoolsDetalle')[0].appendChild($('.TableTools')[1]);
		
	});
	
</script>

<form>

	<div id="rankingDetalle" style="overflow-x: scroll;">

		<div id="ttoolsDetalle"
			class="flash_notice ts ui-state-highlight ui-corner-all sombra-delgada"
			style="height: 40px;">
			<b><?php echo $msg ?> </b>
		</div>

		<table width="100%" border="0" class="display dataTableDetalle">
			<?php echo html_entity_decode($reporteRanking) ?>
		</table>
	</div>

</form>
