<?php use_javascript('jquery.ui.js') ?>
<?php use_javascript('i18n/jquery-ui-i18n.js') ?>
<?php use_javascript('plugins/jquery.dataTables.js') ?>
<?php use_javascript('plugins/dataTables/TableTools.js') ?>
<?php use_javascript('plugins/dataTables/ZeroClipboard.js') ?>
<?php use_javascript('plugins/dataTables/datatables.orden-decimal.js') ?>
<?php use_javascript('plugins/jquery.cookies.js') ?>
<?php use_javascript('plugins/jquery.selectboxes.js') ?>
<?php use_stylesheet('TableTools.css') ?>
<?php use_stylesheet('datatables.css') ?>
<?php use_stylesheet('tabs.css') ?>

<?php setlocale(LC_TIME, "es_MX.UTF-8");?>

<!-- Ver detalle  -->
<div id="verDetalle" style="display: none"></div>
<!-- /Ver detalle  -->

<?php use_stylesheet('./reporte/indexSuccess.css') ?>

<script>


    //tabs
    $(function() {
        $("#tabs").tabs({
            cookie: {expires: 1}
        });
    });

    //datatable
    $(document).ready(function() {

        $.datepicker.setDefaults($.datepicker.regional['es']);

        $(".calendario").datepicker({
            "dateFormat": 'dd-mm-yy',
            "firstDay": 1,
            "maxDate": '+1w',
            "showOn": "button",
            "buttonImage": "imgs/calendar.gif",
            "buttonImageOnly": true,
            "changeMonth": true,
            "changeYear": true,
            "minDate": new Date(2010, 8, 15)
        });

        $(".semana").datepicker({
            "dateFormat": 'dd-mm-yy',
            "showWeek": true,
            "firstDay": 1,
            "maxDate": '-1w',
            //"showOn": "button",
            //"buttonImage": "imgs/calendar.gif",
            //"buttonImageOnly": true,
            "changeMonth": true,
            "changeYear": true,
            "minDate": new Date(2010, 8, 15),
             "beforeShowDay": function(date)
             { return [(date.getDay() == 1), ""]; }

        });
        TableToolsInit.oFeatures = {
                "bCsv": true,
                "bXls": true,
                "bCopy": true,
                "bPrint": false
            };

        var cols = new Array();
        var tds = document.getElementById("ranking").getElementsByTagName('tr')[2].getElementsByTagName('th').length;
        for (i=0;i<tds;i++)
        {
            cols.push({ "sType": "decimal" });
        }

        $('.dataTable').dataTable({
            "bPaginate": false,
            "bLengthChange": false,
            "bFilter": true,
            "bSort": true,
            "bInfo": false,
            "bAutoWidth": false,
            "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "aaSorting": [],
            "aoColumns": cols,
            "sDom": 'T<"fg-toolbar ui-widget-header ui-corner-tl ui-corner-tr ui-helper-clearfix"lfr>t<"fg-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix"ip>'

         });

        $('.dataTableControlLaboral').dataTable({
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bSort": true,
            "bInfo": false,
            "bAutoWidth": false,
            "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "aaSorting": [],
            "sDom": 'T<"fg-toolbar ui-widget-header ui-corner-tl ui-corner-tr ui-helper-clearfix"lfr>t<"fg-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix"ip>'

         });

        //$('#ttools')[0].appendChild($('.TableTools')[0]);




        function selectsIntervalo(){
            rango = $('input:radio[name=rango]:checked').val();
            if(rango=='semana'){
                $('#divIntervalo').css('display', 'none');
                $('#divSemana').css('display', 'block');
            }else{
                if (rango=='mes'){
                    var optionArray = {
                            "" : "Elegir",
                            "01" : "Enero",
                            "02" : "Febrero",
                            "03" : "Marzo",
                            "04" : "Abril",
                            "05" : "Mayo",
                            "06" : "Junio",
                            "07" : "Julio",
                            "08" : "Agosto",
                            "09" : "Septiembre",
                            "10" : "Octubre",
                            "11" : "Noviembre",
                            "12" : "Diciembre"

                        };
                }else if (rango=='bimestre'){
                    var optionArray = {
                            "" : "Elegir",
                            "01" : "1. Enero-Febrero",
                            "03" : "2. Marzo-Abril",
                            "05" : "3. Mayo-Junio",
                            "07" : "4. Julio-Agosto",
                            "09" : "5. Septiembre-Octubre",
                            "11" : "6. Noviembre-Diciembre"

                        };

                }else if (rango=='trimestre'){
                    var optionArray = {
                            "" : "Elegir",
                            "01" : "1. Enero-Febrero-Marzo",
                            "04" : "2. Abril-Mayo-Junio",
                            "07" : "3. Julio-Agosto-Septiembre",
                            "10" : "4. Octubre-Noviembre-Diciembre"
                        };

                }else if (rango=='semestre'){
                    var optionArray = {
                            "" : "Elegir",
                            "01" : "1. Enero a Junio",
                            "07" : "2. Julio a Diciembre"
                        };

                }else if (rango=='anio'){
                    var optionArray = {
                            "" : "Elegir",
                            "01" : "1. Enero a Diciembre"
                        };

                }
                $('#divIntervalo').css('display', 'block');
                $('#divSemana').css('display', 'none');

                $("#intervaloMeses").removeOption(/./);
                // Use true if you want to select added options
                $("#intervaloMeses").addOption(optionArray, false);
            }
        };

        $('.rango').click(selectsIntervalo);

        $('#intervaloMeses').click(function() {
            mes = $('select.intervaloMeses').val();
            anio = $('select.intervaloAnio').val();
            $('#semana').val('01-'+mes+'-'+anio);

        });
        $('#intervaloAnio').click(function() {
            mes = $('select.intervaloMeses').val();
            anio = $('select.intervaloAnio').val();
            $('#semana').val('01-'+mes+'-'+anio);

        });


        selectsIntervalo();
    });


    //Ver detalle Ranking Centro
            var $wcm= $("#verDetalle").dialog(
            {
                        modal: true,
                        autoOpen: false,
                        position: ['center','center'],
                        //title:    'Ver detalle',
                        //setter
                        height: 400,
                        width: 1100,
                        buttons: {
                            'Cerrar': function()
                            {
                                $(this).dialog('close');
                            }
                        }
            });


            function verDetalle(semana, anio, centroId, fechaInicio, fechaFin)
                    {
                         var UrlVerDetalle = "<?php echo url_for('reporte/verDetalleRankingCentro') ?>" ;
                            $("#verDetalle").load(
                                UrlVerDetalle,
                                 {
                                    semana: semana,
                                    anio: anio,
                                    centroId:centroId,
                                    fechaInicio:fechaInicio,
                                    fechaFin: fechaFin
                                 }

                        );

                            //$wcm.dialog( "option", "title" );
                            $wcm.dialog('open');

                    };




</script>

<form>

    <div id="page-wrap" class="sombra">
        <div id="tabs">

            <ul id="tabMenu" class="tsss">
                <li><a href="#ranking">Ranking Cre&iacute;ble</a></li>
                <!--
    <li><a href="#controlLaboral">Control Laboral</a></li>
     -->
            </ul>


            <div id="ranking" style="overflow-x: scroll;">

                <label><input type="radio" class="rango" name="rango"
                    id="rangoSemana" value="semana" <?php echo $chkSemana ?> />Semana</label>
                <label><input type="radio" class="rango" name="rango" id="rangoMes"
                    value="mes" <?php echo $chkMes ?> />Mes</label> <label><input
                    type="radio" class="rango" name="rango" id="rangoBimestre"
                    value="bimestre" <?php echo $chkBimestre ?> />Bimestre</label> <label><input
                    type="radio" class="rango" name="rango" id="rangoTrimestre"
                    value="trimestre" <?php echo $chkTrimestre ?> />Trimestre</label> <label><input
                    type="radio" class="rango" name="rango" id="rangoSemestre"
                    value="semestre" <?php echo $chkSemestre ?> />Semestre</label> <label><input
                    type="radio" class="rango" name="rango" id="rangoAnio" value="anio"
                    <?php echo $chkAnio ?> />Año</label> <br> <br> Fecha:
                <div id="divSemana">
                    <input type="text" id="semana" name="fecha" readonly class="semana">
                </div>
                <div id="divIntervalo" style="display: none">
                    <select name="intervaloMeses" id="intervaloMeses"
                        class="intervaloMeses"></select> <select name="intervaloAnio"
                        id="intervaloAnio" class="intervaloAnio">
                        <?php for($i = date("Y"); $i >= 2010;  $i--):?>
                        <option value="<?php echo $i; ?>">
                            <?php echo $i; ?>
                        </option>
                        <?php endfor;?>
                    </select>
                </div>
                <input type="submit"> <br>


                <!-- botones para avanzar o retroceder semana  -->
                <table width="100%">
                    <tr>
                        <td><a
                            href="reporte?fecha=<?php echo $anterior?>&rango=<?php echo $rango?>"><span
                                class="ui-icon  ui-icon-circle-arrow-w"></span> </a> Anterior <?php echo $anterior?>
                        </td>
                        <td align="right"><a
                            href="reporte?fecha=<?php echo $siguiente?>&rango=<?php echo $rango?>"><span
                                class="ui-icon  ui-icon-circle-arrow-e"></span> </a> Siguiente <?php echo $siguiente?>
                        </td>
                    </tr>
                </table>

                <div id="ttools"
                    class="flash_notice ts ui-state-highlight ui-corner-all sombra-delgada">
                    <b><?php echo $msg ?> </b>
                </div>

                <table width="100%" border="0" class="display dataTable">
                    <?php echo html_entity_decode($reporteRanking) ?>
                </table>
            </div>

            <!--

<div id="controlLaboral" style="overflow-x: scroll;">Inicio: <input
    type="text" id="inicio" name="inicio" readonly class="calendario"> Fin:
<input type="text" id="fin" name="fin" readonly class="calendario"> <input
    type="submit">


<table width="100%" class="display dataTableControlLaboral">
    <thead>
        <tr>
            <th>Centro</th>
            <th>Usuario</th>
            <th>Evento</th>
            <th>Día</th>
            <th>Fecha</th>
            <th>Inicio</th>
            <th>Fin</th>
            <th>Total (min)</th>
            <th>Observaciones</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($controlLaboral as $control) {?>
        <tr>
            <td><?php echo $control->getCentro();?></td>
            <td><?php echo $control->getUsuario();?></td>
            <td><?php echo $control->getTipoEventoLaboral();?></td>
            <td><?php echo $control->getInicio("%A");?></td>
            <td><?php echo $control->getInicio("%d-%m-%Y");?></td>
            <td><?php echo $control->getInicio("%H:%M");?></td>
            <td><?php echo $control->getFin("%H:%M");?></td>
            <td align="right"><?php echo abs(round($control->getTotal()/60, 0));?></td>
            <td><?php echo $control->getObservaciones();?></td>
        </tr>
        <?php }?>
    </tbody>
</table>

</div>
 -->
        </div>
    </div>
</form>
