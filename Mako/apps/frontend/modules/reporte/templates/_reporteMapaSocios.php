<script src="http://www.google.com/jsapi"></script> 
<script src="http://google-maps-utility-library-v3.googlecode.com/svn/tags/markerclusterer/1.0/src/markerclusterer.js"></script> 
<script type="text/javascript">
// Carda del API de Maps
google.load('maps', '3', {
    other_params: 'sensor=false'
});
google.setOnLoadCallback(initialize);
var socios = <?php echo html_entity_decode($socios) ?>;
// Agregamos la venta de informacion del usuario
function initialize() {
    var center = new google.maps.LatLng(<?php echo $lat ?> , <?php echo $long ?>);
    var options = {
      'zoom': 13,
      'center': center,
      'mapTypeId': google.maps.MapTypeId.ROADMAP
    };
    
    var map = new google.maps.Map(document.getElementById("reporte-map"), options);
    // Se coloca un marcador para el centro
    var riaMarker = new google.maps.Marker({
          position: new google.maps.LatLng(<?php echo $lat ?> , <?php echo $long ?>),
          map: map,
          title: '<?php echo $centro ?>',
          icon: '/imgs/ria_marker.png'
    });    
    var markers = [];
    var infowindow = new google.maps.InfoWindow({
    content: '<p style="text-align: center"><img src="/imgs/ajax-circle.gif" alt="cargando..."/></p>'
    });
    
    for (var i = 0, socio; socio = socios[i]; i++) 
    {
        var latLng = new google.maps.LatLng(socio.latitude,
            socio.longitude);
        var marker = new google.maps.Marker({
            position: latLng,
            map: map,
            title: socio.nombre,
            icon: '/imgs/man_icon-1.gif'
        });
        marker.setValues({ id : socio.iden });
        google.maps.event.addListener(marker, 'click', function() {
            infowindow.setContent('<p style="text-align: center"><img src="/imgs/ajax-circle.gif" alt="cargando..."/></p>');
            infowindow.open(map, this);
            $.get('<?php echo url_for('reporte/detalleSocio') ?>', { id : this.get('id') }, function(data){
                infowindow.setContent(data);
            });
        });
        markers.push(marker);
    }
    var markerCluster = new MarkerClusterer(map, markers,{
            maxZoom: 18
    } );
}
function socioInfo(iden)
{
    
}
</script>
<style type="text/css">
#reporte-map{
    width: 100%;
    height: 700px;
}
</style>
<div id="reporte-map"></div>