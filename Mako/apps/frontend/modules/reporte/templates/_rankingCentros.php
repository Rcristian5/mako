<div class="ui-widget">
	<div class="ui-widget-content ui-corner-all" style="padding: 5px;">

		<table id="lista-ranking" width="100%" class="datos">
			<caption>
				Ranking
				<?php echo $titulo ?>
			</caption>
			<thead>
				<tr>
					<th>Lugar</th>
					<th>Centro</th>
					<th>Alcance Registros (%)</th>
					<th>Alcance Cursos (%)</th>
					<th>Alcance Cobranza (%)</th>
					<th>Promedio de metas (%)</th>
					<th>Diferencial tras quitar el 80%</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($Rankings as $Ranking): ?>
				<?php 

				$lugar++;
				 
				?>
				<tr>
					<td class="" align="center"><?php echo $lugar ?>
					</td>
					<td class="" align="right"><?php echo $Ranking['centro'] ?>
					</td>
					<td class="" align="right"><?php echo $Ranking['registros'] ?>
					</td>
					<td class="" align="right"><?php echo $Ranking['cursos'] ?>
					</td>
					<td class="" align="right"><?php echo $Ranking['cobranza'] ?>
					</td>
					<td class="" align="right"><?php echo $Ranking['promedio'] ?>
					</td>
					<td class="" align="right"><?php echo number_format($Ranking['promedio'] - 80,2) ?>
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

	</div>
</div>
