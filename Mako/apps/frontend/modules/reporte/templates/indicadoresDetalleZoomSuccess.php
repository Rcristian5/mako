
<script
	type="text/javascript" src="/js/jquery.1.6.min.js"></script>
<script
	type="text/javascript" src="/js/jquery.ui.js"></script>

<script
	type="text/javascript" src="/js/charts/highcharts.src.js"></script>
<script
	type="text/javascript" src="/js/plugins/jquery.addcommas.js"></script>
<script
	type="text/javascript" src="/js/plugins/jquery.dataTables.js"></script>
<script
	type="text/javascript" src="/js/charts/control.js"></script>
<script
	type="text/javascript" src="/js/indicadores/controlIndicadores.js"></script>

<?php use_stylesheet('./reporte/indicadoresDetalleZoomSuccess.css') ?>

<?php use_stylesheet('datatables.css') ?>

<script type="text/javascript">

//Opciones para tablas de datos y de control
var tOpc = {
	"bPaginate": true,
	"bLengthChange": false,
	"bFilter": true,
	"bSort": true,
	"bInfo": true, 
	"bAutoWidth": false,
	"bJQueryUI": true,
	"aaSorting": [],
	"sPaginationType": "full_numbers"
};

var tXopc = {"bPaginate": false,
	"bLengthChange": false,
	"bFilter": false,
	"bSort": false,
	"bInfo": false,
	"bAutoWidth": false,
	"bJQueryUI": true,
	"sPaginationType": "full_numbers",
	"aaSorting": []
};

//Variables para el control de graficos

		var urlModulo = '<?php echo url_for('reporte/'.$modulo)?>';
		var modulo = '<?php echo $modulo ?>';
		var opcionesTitulo = '<?php echo $opciones_titulo ?>';
		var opcionesSubTitulo = '<?php echo $opciones_subtitulo ?>';	
		var opcionesYAxis = '<?php echo $opciones_yaxis ?>';
		
		/**
		 * Hace llamada al sub detalle y lo abre en el div de subdetalle.
		 */
		function subDetalle(tipo, id, fecha, fecha_ini, fecha_fin, valor, nombre, nivel)
		{
			var params = {};
			params.tipo = tipo;
			params.id = id;
			params.fecha = fecha;
			params.fecha_ini = fecha_ini;
			params.fecha_fin = fecha_fin;
			params.valor = valor;
			params.subdetalle = true;
			params.nivel = nivel;
			
			$('#sub_detalle').load('<?php echo url_for("reporte/subDetalle")?>', params, function() {
				
				//Inicializamos la ventana de subdetalle //SubLlamada existe en 
				subLlamada('<?php echo url_for("reporte/subDetalleCobranza")?>',params);
				$('#sub_detalle_ventana').dialog('open');
				$('#sub_detalle_ventana').dialog( "option", "width", 1024 );
				$('#sub_detalle_ventana').dialog( "option", "height", 600 );
				$('#sub_detalle_ventana').dialog( "option", "position", ['center','center'] );
				$('#sub_detalle_ventana').dialog( "option", "title", nombre);
									
			});
		}	

		
		/**
		 * Hace llamada al sub sub detalle y lo abre en el div de subsubdetalle.
		 */
		function subSubDetalle(params,nombre, titulo, fecha, valor,i)
		{
			console.log('en subsubdetalle: ',params);
			var nombre,titulo;
			var pos = $('#sub_detalle_ventana').position();
			$('#sub_sub_detalle').load('<?php echo url_for("reporte/subSubDetalle")?>', params, function() {
				
				//Inicializamos la ventana de subdetalle
				subSubLlamada('<?php echo url_for("reporte/subSubDetalleCobranza")?>',params);
				$('#sub_sub_detalle_ventana').dialog('open');
				$('#sub_sub_detalle_ventana').dialog( "option", "width", 1024 );
				$('#sub_sub_detalle_ventana').dialog( "option", "height", 600 );
				$('#sub_sub_detalle_ventana').dialog( "option", "position", [pos.left+100,pos.top+100] );
				$('#sub_sub_detalle_ventana').dialog( "option", "title", nombre + ' - '+ titulo);
									
			});
		}	

$(document).ready(function() {
	jQuery.get('<?php echo url_for("reporte/servidoresSinRespuesta")?>', {}, function(datos, state, xhr) {
		console.log(datos);
		if(datos.length > 0) $('#panel-servidores').fadeIn('slow'); 
		jQuery.each(datos,function(id,servidor)
		{
			$('#panel-servidores').find('span').append($('<div>').text(servidor.centro+' desde '+servidor.ultima_fecha));
			
		});
	});

	$("#handler-servidores").click(function(){$("#lista-servidores").toggle('blind','fast');});
});
			
 
		</script>



<div id="cont_general">

	<div class="clearfix">
		<?php if (!($sf_user->getAttribute('auth_externo') && sfConfig::get('app_centro_actual_id') == sfConfig::get('app_central_id'))):?>
		<div id="cont_grafica" style="margin-bottom: 10px;">
			<?php else: ?>
			<div id="cont_grafica" class="cg" style="margin-bottom: 10px;">
				<?php endif;?>
				<div id="grafica-detalle-0"
					style="width: 100%; height: 450px; margin: 0 auto;"></div>
				<div id="grafica-detalle-1"
					style="display: none; width: 100%; height: 450px; margin: 0 auto;"></div>
				<div id="grafica-detalle-2"
					style="display: none; width: 100%; height: 450px; margin: 0 auto;"></div>
				<div id="grafica-detalle-3"
					style="display: none; width: 100%; height: 450px; margin: 0 auto;"></div>

				<?php if($ponderados): ?>
				<div style="margin: 10px;" align="center">
					<span id="rango-horas">Entre las 8:00 y las 19:59 Hrs.</span>
					<div id="control-horas"></div>
				</div>
				<?php endif; ?>

			</div>


			<?php if (!($sf_user->getAttribute('auth_externo') && sfConfig::get('app_centro_actual_id') == sfConfig::get('app_central_id'))):?>
			<div class="clearfix" style="position: absolute; right: 10px;">
				<?php else: ?>
				<div style="width: 20%; margin-left: 10px; float: right;"
					class="clearfix">
					<?php endif;?>

					<div>
						<div>
							<span id="cont_regresar" style="display: none;"> <a
								href="javascript:void(0);" id="regresar"><span
									class="ui-icon ui-icon-arrowreturnthick-1-w"
									style="whidt: 20px; height: 20px; float: left;"></span>Anterior</a>
								|
							</span> <a
								href="<?php echo url_for('reporte/'.$moduloIndicadores)?>"
								onclick="window.location.reload();">Indicadores</a> | <a
								href="javascript:void(0);" onclick="window.location.reload();">Reset</a>
						</div>
						<?php if (!($sf_user->getAttribute('auth_externo') && sfConfig::get('app_centro_actual_id') == sfConfig::get('app_central_id'))):?>
						<div style="display: none;">
							<?php endif;?>
							<hr>
							<?php if($ponderados): ?>
							<div>
								<a id="p100"
									href="<?php echo url_for('reporte/indicadoresDetalleZoom').'?caso=tasa_minutos_utilizados'?>">Ponderado
									al 100%</a> | <a id="p60"
									href="<?php echo url_for('reporte/indicadoresDetalleZoom').'?caso=tasa_ocupacion_60'?>">Ponderado
									al 60%</a> | <a id="p40"
									href="<?php echo url_for('reporte/indicadoresDetalleZoom').'?caso=tasa_ocupacion_40'?>">Ponderado
									al 40%</a>
							</div>

							<?php endif; ?>
							<div>
								<?php if($hayMetas):?>
								<input type="checkbox" value="vismetas" id="vismetas" /> <label
									for="vismetas">Mostrar metas</label>
								<?php endif;?>
								<?php if($hayAcumulados):?>
								<input type="checkbox" value="visacum" id="visacum" /> <label
									for="visacum">Mostrar acumulados</label>
								<?php endif;?>

								<?php if($controlesEstatusSocios):?>
								<input type="checkbox" value="visactivos" id="visactivos"
									checked="checked" /> <label for="visactivos">Activos</label> <input
									type="checkbox" value="visnuevos" id="visnuevos"
									checked="checked" /> <label for="visnuevos">Nuevos</label> <input
									type="checkbox" value="visacum" id="visacum" /> <label
									for="visacum">Acumulados</label>
								<?php endif;?>

							</div>
							<hr>
							<div id="zonas">
								<div id="zonas-0">
									<?php include_partial('listaZonas', array('Zonas'=>$Zonas))?>
								</div>
							</div>
							<hr>
							<div id="centros">
								<div id="centros-0">
									<?php include_partial('listaCentros', array('Centros'=>$Centros))?>
								</div>
								<div id="centros-1" style="display: none;">
									<?php include_partial('listaCentros', array('Centros'=>$Centros))?>
								</div>
								<div id="centros-2" style="display: none;">
									<?php include_partial('listaCentros', array('Centros'=>$Centros))?>
								</div>
								<div id="centros-3" style="display: none;">
									<?php include_partial('listaCentros', array('Centros'=>$Centros))?>
								</div>

							</div>
							<?php if (!($sf_user->getAttribute('auth_externo') && sfConfig::get('app_centro_actual_id') == sfConfig::get('app_central_id'))):?>
						</div>
						<?php endif;?>
					</div>
				</div>
			</div>


			<div id="tablas-0"></div>
			<div id="tablas-1" style="display: none;"></div>
			<div id="tablas-2" style="display: none;"></div>
			<div id="tablas-3" style="display: none;"></div>

			<div id="sub_detalle_ventana">
				<div id="sub_detalle"></div>
			</div>

			<div id="sub_sub_detalle_ventana">
				<div id="sub_sub_detalle"></div>
			</div>

		</div>



		<div id="top_tabs"
			style="position: absolute; outline 0px none; top: 0px; left: auto; right: 0; z-index: 1000;">

			<div class="sombra ui-corner-bottom" id="panel-servidores"
				style="display: none; float: right; margin-left: 8px;">
				<div class="ui-tabs-panel ui-widget-content" id="lista-servidores"
					style="display: none; padding: 5px;" align="center">
					<span><b>No ha sido posible extraer información de <br />los
							siguiente servidores debido a que se encontraron fuera de línea
							al extraer los datos:
					</b> </span>
				</div>
				<div class="ui-state-error ui-corner-bottom"
					style="padding: 5px; z-index: 1002; cursor: pointer;"
					align="center" id="handler-servidores">
					<span style="float: left; margin-right: 0.3em;"
						class="ui-icon ui-icon-info"></span> <strong>Servidores no
						sincronizados !</strong>
				</div>
			</div>
		</div>