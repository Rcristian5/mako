<script>
function subSubLlamada(url,params){

	console.log('ensubsub',params);
	jQuery.get(url, params, function(datos, state, xhr) {
	
	var chart = new Highcharts.Chart({
		chart: {
			renderTo: 'grafica-detalle-sub-sub',
			shadow: true,
	        events: {
	            selection: function(event) {
	                if (event.xAxis) {
		                	console.log(
	                			Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', event.xAxis[0].min),
	                			Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', event.xAxis[0].max)
	                		);
	                } else {
	                    console.log('Selection reset');
	                }
	            }				            
	        },
	        //type: 'spline',
	        defaultSeriesType: 'bar',
			animation: false
		},
		legend: {
			margin: 25
		},
		credits: {
				enabled: false
		},
		title: {
			text: 'Cobranza por Categoría de Cursos'
		},
		subtitle: {
			text: 'Cobranza por Categorías de Cursos'	
		},
		xAxis: {
			type: 'datetime',
			maxZoom: 86400000,
			dateTimeLabelFormats: {
				month: '%b %Y',
				week: 'S%W - %b %Y',
				day: '%a %d %b %Y',
				hour: '%a %d %b %Y',
				second: '%a %d %b %Y',
				minute: '%a %d %b %Y',
				year: '%Y'
			},
			labels: {
	            rotation: -90,
	            align: 'right'
	         }				
		},
		yAxis: {
			title: {
				text: 'Cobranza ($)'
			},
		    labels: {
		    	formatter: function() {
		        	return $.fn.addCommas(this.value);
				}
			},
			min: 0
		},
		tooltip: {
			formatter: function() {

	            var total = 0;
				var s ='<b>';
				if(params.nivel == 0) {   
					s += Highcharts.dateFormat('%b %Y', this.x);
	            }
	            else if(params.nivel == 1) {
					s += Highcharts.dateFormat('S%W - %b %Y', this.x);
	            }
	            else if(params.nivel == 2) {
					s += Highcharts.dateFormat('%a %d %b %Y', this.x);
	            }
	            s += '</b>';
	            $.each(this.points, function(i, point) {
	            	//console.log(point.series);
	            	var idPs = point.series.name;
	            	idPs = idPs.replace(/\W/,'-');
	            	idPs = idPs.replace(/\//,'-');
	            	idPs += '-'+point.x+'-'+point.y;
	                s += '<br/><span id="'+idPs+'" style="color:'+ point.series.color +';">'+ point.series.name +': </span>'+
	                Highcharts.numberFormat(point.y,2,'.',',') ;
	                total += point.y;
	            });
	            s += '<br/><span style="text-decoration:underline; font-weight: bold;">TOTAL:</span>'+Highcharts.numberFormat(total,2,'.',',');
	            return s;
			},
			shared: true,
			textAlign: 'right'
		},
		plotOptions: {
			column: {
				stacking: 'normal'
			}
		}
		
	});

	///Datos
	var m = 0;
	jQuery.each(datos, function(i, val) 
	{
		var tipo = 'column';
	
		//console.log(i+' '+val);
		var evClick = {};
		var serCursor = null;
		
		if(i=='Cursos')
		{
			evClick = {
				click: function(event) {
					console.log([params, this.options.id, event.point.x, event.point.y]);
					//subSubDetalle(params, this.options.id, event.point.x, event.point.y,aDatos.nombre);
            	}
			};
			serCursor = 'pointer'
		}

		chart.addSeries({
			id:i,
			name:i,
			data:val,
			type:tipo,
			shadow: false,
            cursor: serCursor,
        	events: {
	                click: evClick.click
            }

			//stack:'c'+m
		});
		m++;

	});
	
});


	
}




</script>


<div id="cont_general_sub_sub">
	<div class="clearfix">
		<div id="cont_grafica_sub_sub">
			<div id="grafica-detalle-sub-sub"
				style="width: 100%; height: 550px; margin: 0 auto"></div>
		</div>
	</div>
</div>
