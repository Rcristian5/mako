<div class="ui-widget">
	<div class="ui-widget-content ui-corner-all" style="padding: 5px;">

		<table id="lista-ranking" width="400" class="datos">
			<caption>Factor 1 (F1): Registros realizados dividos por el número de
				computadoras del centro</caption>
			<thead>
				<tr>
					<th>Lugar</th>
					<th>Centro</th>
					<th>Registros /PCs</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($RankF1 as $Ranking): ?>
				<?php 

				$lugar++;
				 
				?>
				<tr>
					<td class="" align="center"><?php echo $lugar ?>
					</td>
					<td class="" align="right"><?php echo $Ranking['centro'] ?>
					</td>
					<td class="" align="right"><?php echo number_format($Ranking['cantidad'],2) ?>
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

	</div>
</div>
