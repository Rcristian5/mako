

<table width="100%" class="lista-usuarios"
	id="lista-<?php echo $tabla_id?>">
	<thead>
		<tr>
			<th>Centro</th>
			<th>Usuario</th>
			<th>Folio</th>
			<th>Nombre</th>
			<th>Fecha Nac.</th>
			<th>Sexo</th>
			<th>Email</th>
			<th>Tel</th>
			<th>Cel</th>
			<th>Ultima visita</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($socios as $socio): ?>
		<tr>
			<td><?php echo $socio['centro']?></td>
			<td><?php echo $socio['usuario'] ?></td>
			<td><?php echo $socio['folio'] ?></td>
			<td><?php echo $socio['nombre_completo'] ?></td>
			<td><?php echo $socio['fecha_nac'] ?></td>

			<td><?php echo $socio['sexo'] ?></td>
			<td><?php echo $socio['email'] ?></td>
			<td><?php echo $socio['tel'] ?></td>
			<td><?php echo $socio['celular'] ?></td>
			<td><?php echo $socio['ultima_visita'] ?></td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>
