
<script
	type="text/javascript" src="/js/jquery.1.6.min.js"></script>
<script
	type="text/javascript" src="/js/jquery.ui.js"></script>

<script
	type="text/javascript" src="/js/charts/highcharts.src.js"></script>
<script
	type="text/javascript" src="/js/plugins/jquery.addcommas.js"></script>
<script
	type="text/javascript" src="/js/plugins/jquery.dataTables.js"></script>

<?php use_stylesheet('./reporte/indicadoresDetalleSuccess.css') ?>

<?php use_stylesheet('datatables.css') ?>

<script type="text/javascript">
		var tOpc = {
				"bPaginate": true,
				"bLengthChange": false,
				"bFilter": true,
				"bSort": true,
				"bInfo": true, 
				"bAutoWidth": false,
			    "bJQueryUI": true,
			    "aaSorting": [],
			    "sPaginationType": "full_numbers"
				
			};

		var tXopc = {"bPaginate": false,
			"bLengthChange": false,
			"bFilter": false,
			"bSort": false,
			"bInfo": false,
			"bAutoWidth": false,
		    "bJQueryUI": true,
		    "sPaginationType": "full_numbers",
		    "aaSorting": []
		};
		
		$(document).ready(function() {


			$('#sub_detalle_ventana').dialog({ 
				autoOpen: false,
				modal:true
			});

			$('#sub_sub_detalle_ventana').dialog({ 
				autoOpen: false,
				modal:true 
			});
			
			
		var arZonas = new Object();
		jQuery.get('<?php echo url_for('reporte/listaZonas')?>', null, function(datos, state, xhr) {
			arZonas = datos;	
		});
		
		var tCentros = 	$('#lista_centros').dataTable(tOpc);	
			
			/*$('body').click(function(){
				window.location = '<?php echo url_for('reporte/indicadores')?>';
			});
			*/
			
	var aadata = new Object();

	var aacolumns = new Array();
	aacolumns.push({"sTitle":"Periodo"});							
	var aSeries = new Object();
	var aSerMetas = new Array();
	var aSerAcum = new Array();
	var aSerPrinc = new Array();
	var reMetas = /Meta/gi;
	var reAcum = /Acum/gi;
	var aDatos={};
	
				jQuery.get('<?php echo url_for('reporte/'.$modulo)?>', null, function(datos, state, xhr) {
					
					chart = new Highcharts.Chart({
						chart: {
							renderTo: 'grafica-detalle',
							shadow: true,
					        events: {
					            selection: function(event) {
					                if (event.xAxis) {
					                	console.log(
					                			Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', event.xAxis[0].min),
					                			Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', event.xAxis[0].max)
					                		);
					                } else {
					                    console.log('Selection reset');
					                }
					            }				            
					        },
					        //type: 'spline',
					        defaultSeriesType: 'bar',
							animation: false,
							zoomType:'x'
						},
						credits: {
							enabled: false
						},
						title: {
							text: '<?php echo $opciones_titulo ?>'
						},
						subtitle: {
							text: '<?php echo $opciones_subtitulo ?>'	
						},
						xAxis: {
							type: 'datetime',
							dateTimeLabelFormats: {
								month: '%b %Y',
								year: '%Y'
							}
						},
						yAxis: {
							title: {
								text: '<?php echo $opciones_yaxis ?>'
							},
						    labels: {
						    	formatter: function() {
						        	return $.fn.addCommas(this.value);
								}
						    },
							min: 0
						},
						tooltip: {
							formatter: function() {
					                return '<b>'+ this.series.name +'</b><br/>'+
									Highcharts.dateFormat('%b %Y', this.x) +': '+ $.fn.addCommas(this.y) + ' <?php echo $opciones_yaxis ?>'
							}
						},
					    plotOptions: {
					         column: {
					            stacking: 'normal'
					          }
					    }
					});

					var pred = new Array();
					var adatax =  new Array();
					var ctSp = 0; //contador de las series principales
					jQuery.each(datos, function(i, val) 
					{
						var vis = false;
						if(ctSp==0)
						{ 
							vis = true;
						}
						else if(i.match(reMetas) && !i.match(reAcum) && $('#vismetas').attr('checked'))
						{ 
							vis = true;
						} 
						else if(!i.match(reMetas) && i.match(reAcum) && $('#visacum').attr('checked'))
						{
							vis = true;
						}
						var evClick = {};
						var serCursor = null;
						var modulo = '<?php echo $modulo ?>';
						if(modulo=='CobranzaTotales' && ctSp == 0)
						{
							evClick = {
								click: function(event) {
									subDetalle('global', this.options.id, event.point.x, event.point.y,'Global');
			                	}
							};
							serCursor = 'pointer';
							
						}

						var sp = chart.addSeries({
							id:i,
							name:i,
							data:val,
							type:'spline',
							visible: vis,
							events: {
				                click: evClick.click
				            },
				            cursor: serCursor							
						});
						ctSp++;

						//almacenamos la serie en algun contenedor
						if(i.match(reMetas) && !i.match(reAcum))
						{
							aSerMetas[i] = sp;
						}
						else if(i.match(reAcum) && !i.match(reMetas))
						{
							aSerAcum[i] = sp;
						}
						else if(i.match(reAcum) && i.match(reMetas))
						{
							aSerAcum[i] = sp;
						}
						else
						{
							aSerPrinc[i] = sp;
						}

						//Empieza la tabla
						//Metemos la columna del periodo
						aacolumns.push({"sTitle":i});

						jQuery.each(val,function(j,a)
						{
							//console.log('a0 '+a[0]);
							if(typeof pred[a[0]] == 'undefined')
							{
								pred[a[0]] = 0;
								aadata[a[0]] = [];
								aadata[a[0]][pred[a[0]]] = $.fn.addCommas(a[1]);	
							}
							else if (pred[a[0]] >= 0)
							{
								pred[a[0]]++;
								aadata[a[0]][pred[a[0]]] = $.fn.addCommas(a[1]);
							}
							
						});
						
					});

					jQuery.each(aadata,function(k,b)
					{
						if(typeof b[0] != 'undefined')
						{
							if(b.length > 2){
								var row = [];
								row.push(Highcharts.dateFormat('%b %Y',k));
								for(var v in b)
								{
									row.push(b[v]);
								}
								//adatax.push([Highcharts.dateFormat('%b %Y',k),b[0],b[1],b[2],b[3]]);
								adatax.push(row);
							}
							else
							{
								adatax.push([Highcharts.dateFormat('%b %Y',k),b[0],b[1]]);
							}
						}
					});											

					$('#tabla_datos').dataTable({
						"bPaginate": false,
						"bLengthChange": false,
						"bFilter": false,
						"bSort": false,
						"bInfo": false,
						"bAutoWidth": false,
					    "bJQueryUI": true,
					    "sPaginationType": "full_numbers",
					    "aaSorting": [],
						"aaData":adatax,
						"aoColumns":aacolumns
						
					});	

					$("#tabla_datos").find('th').each(function(){
						$(this).addClass("ui-state-default");
						$(this).css("text-align","center");
					});

					$('#tabla_datos tbody tr').each( function() {
						var idx = 0;
						$(this).find('td').each(function(){
							if(idx > 0)
							{	
								$(this).attr('align','right');
							}
							idx++;
						});
					});				
					
				});

//Controles
/* BIND POR CENTROS */
				$('#centros').bind('centro',function(e){

					
					var idx = $(e.target).val();
					
					if($(e.target).attr('checked')) 
					{ 
						$(e.target).attr('disabled','disabled');
						var params = {centro_id:idx};
						aDatos.nombre = '';
						aDatos.id = 'centro_'+idx;
						var mino = [];
						$("#lista_centros label[for="+aDatos.id+"]").each(function(){
							mino.push($(this).text());
						});
						aDatos.nombre = mino.join(" | ");
						aDatos.series = [];
						
						//if(typeof aSeries['centro_'+idx] == "undefined")
						//{
							aSeries['centro_'+idx]= new Array();
							var modulo = '<?php echo $modulo ?>';
							jQuery.get('<?php echo url_for('reporte/'.$modulo)?>', params, function(datos, state, xhr) {
								var m = 0;
								jQuery.each(datos, function(i, val) 
								{
									var vis = false;
									if(m==0)
									{ 
										vis = true;
									}
									else if(i.match(reMetas) && !i.match(reAcum) && $('#vismetas').attr('checked'))
									{ 
										vis = true;
									} 
									else if(!i.match(reMetas) && i.match(reAcum) && $('#visacum').attr('checked'))
									{
										vis = true;
									}
									var evClick = {};
									var serCursor = null;
									if(modulo=='CobranzaTotales' && m==0)
									{
										evClick = {click: function(event) {
											console.log(this.name +' clicked\n'+
						                    		'id: '+ idx +'\n'+
						                    		'valorx: '+ event.point.x +'\n'+
						                    		'valory: '+ event.point.y +'\n'+
							                          'Alt: '+ event.altKey +'\n'+
						                          'Control: '+ event.ctrlKey +'\n'+
						                          'Shift: '+ event.shiftKey +'\n');
					                          
											subDetalle('centro', idx, event.point.x, event.point.y,aDatos.nombre);
						                	}
										};

										serCursor = 'pointer'
									}

									aDatos.series[m]={'nombre':i,valores:val};
									aSeries['centro_'+idx][m] = chart.addSeries({
										id:i,
										name:i,
										data:val,
										type:'column',
										stack:'c'+m,
										visible: vis,
							            cursor: serCursor,
						            	events: {
							                click: evClick.click
							            }
									});

									//almacenamos la serie en algun contenedor
									//console.log(i+'centro_id => '+idx+' valor: '+val);
									//console.log(i+'reAcum => '+i.match(reAcum));
									
									if(i.match(reMetas) && !i.match(reAcum))
									{
										aSerMetas[i] = aSeries['centro_'+idx][m];
									}
									else if(i.match(reAcum) && !i.match(reMetas))
									{
										aSerAcum[i] = aSeries['centro_'+idx][m];
									}
									else if(i.match(reAcum) && i.match(reMetas))
									{
										aSerAcum[i] = aSeries['centro_'+idx][m];
									}
									else
									{
										aSerPrinc[i] = aSeries['centro_'+idx][m];
									}

									m++;
									
								});
								$(e.target).removeAttr('disabled');
								lasTablas(aDatos);
							});
					/*	}
						else
						{
							for(s in aSeries['centro_'+idx])
							{
								aSeries['centro_'+idx][s].show();
							}
						}
					*/
					}
					else
					{
						for(s in aSeries['centro_'+idx])
						{
							var idSer = aSeries['centro_'+idx][s].options.id;
							if(typeof aSerMetas[idSer] != "undefined") delete aSerMetas[idSer];
							if(typeof aSerAcum[idSer] != "undefined") delete aSerAcum[idSer];
							if(typeof aSerPrinc[idSer] != "undefined") delete aSerPrinc[idSer];

							aSeries['centro_'+idx][s].remove();
							$('#tabla_centro_'+idx+'_wrapper').remove();
						}
					}
				});


				/* BIND POR ZONAS */
				$('#zonas').bind('zona',function(e){
					
					
					var idx = $(e.target).val();
					var modulo = '<?php echo $modulo;?>';
					if($(e.target).attr('checked')) 
					{ 
						var params = {zona:idx};
						$(e.target).attr('disabled','disabled');
						//if(typeof aSeries['zona_'+idx] == "undefined")
						//{
							
							aDatos.id = 'zona_'+idx;
							aDatos.nombre = $(e.target).next('label').text(); 
							aDatos.series = [];
							
						
							aSeries['zona_'+idx]= new Array();
							jQuery.get('<?php echo url_for('reporte/'.$modulo)?>', params, function(datos, state, xhr) {
								var m = 0;
								jQuery.each(datos, function(i, val) 
								{
									var vis = false;
									if(m==0)
									{ 
										vis = true;
									}
									else if(i.match(reMetas) && !i.match(reAcum) && $('#vismetas').attr('checked'))
									{ 
										vis = true;
									} 
									else if(!i.match(reMetas) && i.match(reAcum) && $('#visacum').attr('checked'))
									{
										vis = true;
									}
									var evClick = {};
									var serCursor = null;
									if(modulo=='CobranzaTotales' && m==0)
									{
										evClick = {
											click: function(event) {
												subDetalle('zona', this.options.id, event.point.x, event.point.y,aDatos.nombre);
						                	}
										};
										serCursor = 'pointer'
									}
									
									
									
									aDatos.series[m]={'nombre':i,valores:val};
									aSeries['zona_'+idx][m] = chart.addSeries({
										id:idx,
										name:i,
										data:val,
										type:'column',
										stack:'z'+m,
										visible: vis,
							            cursor: serCursor,
						            	events: {
								                click: evClick.click
							            },
							            point:{
							            	events: {
								                click: evClick.click
								            }
					            		}
										
									});
									

									//almacenamos la serie en algun contenedor
									if(i.match(reMetas) && !i.match(reAcum))
									{
										aSerMetas[i] = aSeries['zona_'+idx][m];
									}
									else if(i.match(reAcum) && !i.match(reMetas))
									{
										aSerAcum[i] = aSeries['zona_'+idx][m];
									}
									else if(i.match(reAcum) && i.match(reMetas))
									{
										aSerAcum[i] = aSeries['zona_'+idx][m];
									}
									else
									{
										aSerPrinc[i] = aSeries['zona_'+idx][m];
									}

									m++;
								});

								$(e.target).removeAttr('disabled');
								lasTablas(aDatos);
							});
						/*}
						else
						{
							for(s in aSeries['zona_'+idx])
							{
								aSeries['zona_'+idx][s].show();
							}
						}
						*/
					}
					else
					{
						for(s in aSeries['zona_'+idx])
						{
							var idSer = aSeries['zona_'+idx][s].options.id;
							if(typeof aSerMetas[idSer] != "undefined") delete aSerMetas[idSer];
							if(typeof aSerAcum[idSer] != "undefined") delete aSerAcum[idSer];
							if(typeof aSerPrinc[idSer] != "undefined") delete aSerPrinc[idSer];
							aSeries['zona_'+idx][s].remove();
							//$('#tabla_zona_'+idx).remove();
							$('#tabla_zona_'+idx+'_wrapper').remove();
						}
					}
				});


		/* Bind controles visibilidad metas y acumulados */
				$('#vismetas').click(function(e){
					if($(e.target).attr('checked')) 
					{
						 for(var meta in aSerMetas)
						 {
							 if(typeof aSerMetas[meta] != "undefined") aSerMetas[meta].show();
						 }
					}
					else
					{
						 for(meta in aSerMetas)
						 {
							 if(typeof aSerMetas[meta] != "undefined") aSerMetas[meta].hide();
						 }
					}
				});

				$('#visacum').click(function(e){
					if($(e.target).attr('checked')) 
					{
						 for(var acum in aSerAcum)
						 {
							 if(typeof aSerAcum[acum] != "undefined") aSerAcum[acum].show();
						 }
					}
					else
					{
						 for(acum in aSerAcum)
						 {
							 if(typeof aSerAcum[acum] != "undefined") aSerAcum[acum].hide();
						 }
					}
				});

				
			});

	
		
		///Tratamos de hacer las tablas
		function lasTablas(oDatos)
		{

			var nombreTabla = oDatos.nombre;
			var id = oDatos.id;
			var t = {};
			var html = '<table id="tabla_'+id+'" class="datos"><caption class="ui-state-default">'+nombreTabla+'</caption>';
			
			var aFilas = {};
			var dt = [];
			var headCols = ['Periodo'];
			for(nval in oDatos.series){			
				//aFilas={};
				var s = oDatos.series[nval];
				
				var nSer = s.nombre;
				headCols.push(nSer);
				var val = s.valores;

				//Convertimos de filas a columnas				
				jQuery.each(val,function(j,a)
				{
					if(typeof dt[a[0]] == 'undefined')
					{
						dt[a[0]] = 0; //Indice del segundo nivel, la llave es el num de columna
						aFilas[ a[0] ] = [];
						aFilas[ a[0] ][ dt[ a[0] ] ] = $.fn.addCommas(a[1]);
					}
					else if (dt[a[0]] >= 0)
					{
						dt[a[0]]++;
						aFilas[a[0]][dt[a[0]]] = $.fn.addCommas(a[1]);
					}
					else
					{
						console.log('Nodeberia: dt[a[0]]:'+dt[a[0]]);
					}
					
				});
			}

			//Construimos las cabezas de la tabla
			html += '<thead><tr>';
			jQuery.each(headCols,function(j,a)
			{
				html += '<th class="ui-state-default">'+a+'</th>';
			});
			html += '</tr></thead>';
			html += '<tbody>';

			//Construimos las filas de la tabla
			jQuery.each(aFilas,function(j,a)
			{
				html += '<tr>';
				html += '<td>'+Highcharts.dateFormat('%b %Y',j)+'</td>';
				jQuery.each(a,function(fe,b)
				{
						html += '<td align="right">'+b+'</td>';
				});
				html += '</tr>';
			});
			html += '</tbody></table>';
			
			t = $(html);
			$('#tablas').append(t);
			$('#tabla_'+id).dataTable(tXopc);
			$('#tabla_'+id).css('border','thin solid silver');
			$('#tabla_'+id+'_wrapper').css("float","left");

		}

		/**
		 * Hace llamada al sub detalle y lo abre en el div de subdetalle.
		 */
		function subDetalle(tipo, id, fecha, valor, nombre)
		{
			var params = {};
			params.tipo = tipo;
			params.id = id;
			params.fecha = fecha;
			params.valor = valor;
			params.subdetalle = true;
			
			$('#sub_detalle').load('<?php echo url_for("reporte/subDetalle")?>', params, function() {
				
				//Inicializamos la ventana de subdetalle
				subLlamada('<?php echo url_for("reporte/subDetalleCobranza")?>',params);
				$('#sub_detalle_ventana').dialog('open');
				$('#sub_detalle_ventana').dialog( "option", "width", 1024 );
				$('#sub_detalle_ventana').dialog( "option", "height", 600 );
				$('#sub_detalle_ventana').dialog( "option", "position", ['center','center'] );
				$('#sub_detalle_ventana').dialog( "option", "title", nombre);
									
			});
		}	

		
		/**
		 * Hace llamada al sub sub detalle y lo abre en el div de subsubdetalle.
		 */
		function subSubDetalle(tipo, id, titulo, fecha, valor, nombre)
		{
			var params = {};
			params.tipo = tipo;
			params.id = id;
			params.fecha = fecha;
			params.valor = valor;
			params.subdetalle = true;

			var pos = $('#sub_detalle_ventana').position();
			$('#sub_sub_detalle').load('<?php echo url_for("reporte/subSubDetalle")?>', params, function() {
				
				//Inicializamos la ventana de subdetalle
				subSubLlamada('<?php echo url_for("reporte/subSubDetalleCobranza")?>',params);
				$('#sub_sub_detalle_ventana').dialog('open');
				$('#sub_sub_detalle_ventana').dialog( "option", "width", 1024 );
				$('#sub_sub_detalle_ventana').dialog( "option", "height", 600 );
				$('#sub_sub_detalle_ventana').dialog( "option", "position", [pos.left+100,pos.top+100] );
				$('#sub_sub_detalle_ventana').dialog( "option", "title", nombre + ' - '+ titulo);
									
			});
		}	

		
		</script>

<div id="cont_general">

	<div class="clearfix">
		<div id="cont_grafica" class="cg">
			<div id="grafica-detalle"
				style="width: 100%; height: 450px; margin: 0 auto;"></div>
		</div>

		<div style="width: 20%; margin-left: 10px; float: right;"
			class="clearfix">
			<div>
				<div>
					<a href="<?php echo url_for('reporte/indicadores')?>"
						onclick="window.location.reload();">Indicadores</a> | <a
						href="javascript:void(0);" onclick="window.location.reload();">Reset</a>
				</div>
				<hr>
				<div>
					<input type="checkbox" value="vismetas" id="vismetas" /> <label
						for="vismetas">Mostrar metas</label>
					<?php if($hayAcumulados):?>
					<input type="checkbox" value="visacum" id="visacum" /> <label
						for="visacum">Mostrar acumulados</label>
					<?php endif;?>
				</div>
				<hr>
				<div id="zonas">
					<?php include_partial('listaZonas', array('Zonas'=>$Zonas))?>
				</div>
				<hr>
				<div id="centros">
					<?php include_partial('listaCentros', array('Centros'=>$Centros))?>
				</div>
			</div>
		</div>
	</div>

	<table id="tabla_datos" width="100%" class="datos">
		<thead>
			<tr>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>

	<div id="tablas"></div>

	<div id="sub_detalle_ventana">
		<div id="sub_detalle"></div>
	</div>

	<div id="sub_sub_detalle_ventana">
		<div id="sub_sub_detalle"></div>
	</div>

</div>
