<script type="text/javascript" src="/js/jquery.1.6.min.js"></script>
<script
	type="text/javascript" src="/js/charts/highcharts.src.js"></script>

<script type="text/javascript">
			$(document).ready(function() {

				chart5 = new Highcharts.Chart({
					chart: {
						renderTo: 'ocupacion_dia',
						shadow: true,
				        events: {
					        click: function(){
						        window.location = '<?php echo url_for('reporte/indicadoresDetalleZoom')."?caso=ocupacion_dias"?>';
					        },
				            selection: function(event) {
				                if (event.xAxis) {
				                	console.log(
				                			Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', event.xAxis[0].min),
				                			Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', event.xAxis[0].max)
				                		);
				                } else {
				                    console.log('Selection reset');
				                }
				            }				            
				        },
				        defaultSeriesType: 'bar',
						animation: false,
						height: 300,
						zoomType:'x'
					},
					credits: {
						enabled: false
					},
					title: {
						text: 'Minutos Utilizados por Día'
					},
					subtitle: {
						text: 'Total de Minutos Utilizados por Día'	
					},
					xAxis: {
						type: 'datetime',
						dateTimeLabelFormats: {
							month: '%b %Y',
							year: '%Y'
						}
					},
					yAxis: {
						title: {
							text: 'Minutos Utilizados (miles)'
						},
					    labels: {
					    	formatter: function() {
					        	return Highcharts.numberFormat(this.value/1000,0);
							}
					    },
						min: 0
					},
					tooltip: {
						formatter: function() {
				                return '<b>'+ this.series.name +'</b><br/>'+
								Highcharts.dateFormat('%b %Y', this.x) +': '+ Highcharts.numberFormat(this.y,0) +' minutos';
						}
					},
				    plotOptions: {
				         column: {
				            stacking: 'normal'
				          }
				    }					
				});
				
				
				jQuery.get('<?php echo url_for('reporte/OcupacionPorDia')?>', null, function(datos, state, xhr) {
					
					socTot = datos;
					var m = 0;
					jQuery.each(datos, function(i, val) {
						//console.log(val);
						chart5.addSeries({
							id:i,
							name:i,
							data:val,
							type:'column',
							shadow:false
						});
						m++;
						
					});
				});


				chart6 = new Highcharts.Chart({
					chart: {
						renderTo: 'tasa_ocupacion_dia',
						shadow: true,
				        events: {
					        click: function(){
						        window.location = '<?php echo url_for('reporte/indicadoresDetalleZoom')."?caso=tasa_ocupacion_dias"?>';
					        },
				            selection: function(event) {
				                if (event.xAxis) {
				                	console.log(
				                			Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', event.xAxis[0].min),
				                			Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', event.xAxis[0].max)
				                		);
				                } else {
				                    console.log('Selection reset');
				                }
				            }				            
				        },
						
				        defaultSeriesType: 'bar',
						animation: false,
						height: 300
					},
					credits: {
						enabled: false
					},
					title: {
						text: 'Tasa de Ocupación por Días'
					},
					subtitle: {
						text: 'Minutos Utilizados por Minutos Disponibles'	
					},
					xAxis: {
						type: 'datetime',
						dateTimeLabelFormats: {
							month: '%b %Y',
							year: '%Y'
						}
					},
					yAxis: {
						title: {
							text: 'Minutos Utilizados/Disponibles'
						},
					    labels: {
					    	formatter: function() {
					        	return Highcharts.numberFormat(this.value);
							}
					    },
						min: 0
					},
					tooltip: {
						formatter: function() {
				                return '<b>'+ this.series.name +'</b><br/>'+
								Highcharts.dateFormat('%b %Y', this.x) +': '+ Highcharts.numberFormat(this.y,2,'.',','); +' ';
						}
					},
				    plotOptions: {
				         column: {
				            stacking: 'normal'
				          }
				    }
				});
				
				var datos;
				jQuery.get('<?php echo url_for('reporte/TasaOcupacionPorDia')?>', null, function(datos, state, xhr) {
					jQuery.each(datos, function(i, val) {
						chart6.addSeries({
							id:i,
							name:i,
							data:val,
							type:'column',
							shadow:false
						});
						
					});
				});


				
			});
				
		</script>
<div style="position: relative; z-index: 1010;" id="cont1">
	<div id="ocupacion_dia"
		style="width: 400px; height: 320px; margin: 0 auto"></div>
</div>
<div
	id="tasa_ocupacion_dia"
	style="width: 400px; height: 320px; margin: 0 auto"></div>

