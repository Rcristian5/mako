<div class="ui-widget">
	<div class="ui-widget-content ui-corner-all" style="padding: 5px;">

		<table id="lista-ranking" width="400" class="datos">
			<caption>Factor 7 (F7): Días de operación del centro (???Como se
				calcula??)</caption>
			<thead>
				<tr>
					<th>Lugar</th>
					<th>Centro</th>
					<th>Ranking Ponderado a Días Operación</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($RankF7 as $Ranking): ?>
				<?php 

				$lugar++;
				 
				?>
				<tr>
					<td class="" align="center"><?php echo $lugar ?>
					</td>
					<td class="" align="right"><?php echo $Ranking['centro'] ?>
					</td>
					<td class="" align="right"><?php echo $Ranking['cantidad'] ?>
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

	</div>
</div>
