
<script
	type="text/javascript" src="/js/jquery.1.6.min.js"></script>
<script
	type="text/javascript" src="/js/jquery.ui.js"></script>

<script
	type="text/javascript" src="/js/charts/highcharts.src.js"></script>
<script
	type="text/javascript" src="/js/plugins/jquery.addcommas.js"></script>
<script
	type="text/javascript" src="/js/plugins/jquery.dataTables.js"></script>

<?php use_stylesheet('./reporte/indicadoresDetalleCapacidadHorasDiasSuccess.css') ?>

<?php use_stylesheet('datatables.css') ?>

<script type="text/javascript">
		var tOpc = {
				"bPaginate": true,
				"bLengthChange": false,
				"bFilter": true,
				"bSort": true,
				"bInfo": true, 
				"bAutoWidth": false,
			    "bJQueryUI": true,
			    "aaSorting": [],
			    "sPaginationType": "full_numbers"
				
			};

		var tXopc = {"bPaginate": false,
			"bLengthChange": false,
			"bFilter": false,
			"bSort": false,
			"bInfo": false,
			"bAutoWidth": false,
		    "bJQueryUI": true,
		    "sPaginationType": "full_numbers",
		    "aaSorting": []
		};
		
		$(document).ready(function() {


			$('#sub_detalle_ventana').dialog({ 
				autoOpen: false,
				modal:true 
			});
			
		var arZonas = new Object();
		jQuery.get('<?php echo url_for('reporte/listaZonas')?>', null, function(datos, state, xhr) {
			arZonas = datos;	
		});
		
		var tCentros = 	$('#lista_centros').dataTable(tOpc);	
			
			/*$('body').click(function(){
				window.location = '<?php echo url_for('reporte/indicadores')?>';
			});
			*/
			
	var aadata = new Object();

	var aacolumns = new Array();
	aacolumns.push({"sTitle":"Periodo"});							
	var aSeries = new Object();
	var aSerMetas = new Array();
	var aSerGlobal = new Array();
	var aSerPrinc = new Array();
	var reMetas = /Meta/gi;
	var reAcum = /Acum/gi;
	var aDatos={};
	
				jQuery.get('<?php echo url_for('reporte/'.$modulo)?>', null, function(datos, state, xhr) {
					
					chart = new Highcharts.Chart({
						chart: {
							renderTo: 'grafica-detalle',
							shadow: true,
					        events: {
					            selection: function(event) {
					                if (event.xAxis) {
					                	console.log(
					                			Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', event.xAxis[0].min),
					                			Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', event.xAxis[0].max)
					                		);
					                } else {
					                    console.log('Selection reset');
					                }
					            }				            
					        },
					        //type: 'spline',
					        defaultSeriesType: 'bar',
							animation: false,
							zoomType:'x'
						},
						credits: {
							enabled: false
						},
						title: {
							text: '<?php echo $opciones_titulo ?>'
						},
						subtitle: {
							text: '<?php echo $opciones_subtitulo ?>'	
						},
						xAxis: {
							type: 'datetime',
							dateTimeLabelFormats: {
								month: '%b %Y',
								year: '%Y'
							}
						},
						yAxis: {
							title: {
								text: '<?php echo $opciones_yaxis ?>'
							},
						    labels: {
						    	formatter: function() {
						        	return Highcharts.numberFormat(this.value,'2','.',',');
								}
						    },
							min: 0
						},
						tooltip: {
							formatter: function() {
					                return '<b>'+ this.series.name +'</b><br/>'+
									Highcharts.dateFormat('%b %Y', this.x) +': '+ Highcharts.numberFormat(this.y,'2','.',',') + ' <?php echo $opciones_yaxis ?>'
							}
						},
					    plotOptions: {
					         column: {
					            stacking: 'normal'
					          }
					    }
					});

					var pred = new Array();
					var adatax =  new Array();
					var ctSp = 0; //contador de las series principales

					aDatos.id = 'global';
					aDatos.nombre = 'Global'; 
					aDatos.series = [];
			
					jQuery.each(datos, function(i, val) 
					{
						var evClick = {};
						var serCursor = null;
						var modulo = '<?php echo $modulo ?>';
						if(modulo=='CobranzaTotales' && ctSp == 0)
						{
							evClick = {
								click: function(event) {
									subDetalle('global', this.options.id, event.point.x, event.point.y,'Global');
			                	}
							};
							serCursor = 'pointer';
							
						}
						aDatos.series[ctSp]={'nombre':i,valores:val};
						var sp = chart.addSeries({
							id:i,
							name:i,
							data:val,
							type:'column',
							shadow:false,
							visible: true,
							events: {
				                click: evClick.click
				            },
				            cursor: serCursor							
						});
						ctSp++;

						//almacenamos las series en el contenedor global
						aSerGlobal[i] = sp;
						
					});
					lasTablas(aDatos);
					
				});

//Controles
/* BIND POR CENTROS */
				$('#centros').bind('centro',function(e){

					
					var idx = $(e.target).val();
					
					if($(e.target).attr('checked')) 
					{ 
						$(e.target).attr('disabled','disabled');
						var params = {centro_id:idx};
						aDatos.nombre = '';
						aDatos.id = 'centro_'+idx;
						var mino = [];
						$("#lista_centros label[for="+aDatos.id+"]").each(function(){
							mino.push($(this).text());
						});
						aDatos.nombre = mino.join(" | ");
						aDatos.series = [];
						
						//if(typeof aSeries['centro_'+idx] == "undefined")
						//{
							aSeries['centro_'+idx]= new Array();
							var modulo = '<?php echo $modulo ?>';
							jQuery.get('<?php echo url_for('reporte/'.$modulo)?>', params, function(datos, state, xhr) {
								var m = 0;
								jQuery.each(datos, function(i, val) 
								{
									var evClick = {};
									var serCursor = null;
									if(modulo=='CobranzaTotales' && m==0)
									{
										evClick = {click: function(event) {
											console.log(this.name +' clicked\n'+
						                    		'id: '+ idx +'\n'+
						                    		'valorx: '+ event.point.x +'\n'+
						                    		'valory: '+ event.point.y +'\n'+
							                          'Alt: '+ event.altKey +'\n'+
						                          'Control: '+ event.ctrlKey +'\n'+
						                          'Shift: '+ event.shiftKey +'\n');
					                          
											subDetalle('centro', idx, event.point.x, event.point.y,aDatos.nombre);
						                	}
										};

										serCursor = 'pointer'
									}

									aDatos.series[m]={'nombre':i,valores:val};
									aSeries['centro_'+idx][m] = chart.addSeries({
										id:i,
										name:i,
										data:val,
										type:'column',
										stack:'c'+idx,
										visible: true,
										shadow:false,
							            cursor: serCursor,
						            	events: {
							                click: evClick.click
							            }
									});

									m++;
									
								});
								$(e.target).removeAttr('disabled');
								lasTablas(aDatos);
							});
					/*	}
						else
						{
							for(s in aSeries['centro_'+idx])
							{
								aSeries['centro_'+idx][s].show();
							}
						}
					*/
					}
					else
					{
						for(s in aSeries['centro_'+idx])
						{
							var idSer = aSeries['centro_'+idx][s].options.id;
							aSeries['centro_'+idx][s].remove();
							$('#tabla_centro_'+idx+'_wrapper').remove();
						}
					}
				});


				/* BIND POR ZONAS */
				$('#zonas').bind('zona',function(e){
					
					
					var idx = $(e.target).val();
					var modulo = '<?php echo $modulo;?>';
					if($(e.target).attr('checked')) 
					{ 
						var params = {zona:idx};
						$(e.target).attr('disabled','disabled');
						//if(typeof aSeries['zona_'+idx] == "undefined")
						//{
							
							aDatos.id = 'zona_'+idx;
							aDatos.nombre = $(e.target).next('label').text(); 
							aDatos.series = [];
							
						
							aSeries['zona_'+idx]= new Array();
							jQuery.get('<?php echo url_for('reporte/'.$modulo)?>', params, function(datos, state, xhr) {
								var m = 0;
								jQuery.each(datos, function(i, val) 
								{
									var evClick = {};
									var serCursor = null;
									if(modulo=='CobranzaTotales' && m==0)
									{
										evClick = {
											click: function(event) {
												subDetalle('zona', this.options.id, event.point.x, event.point.y,aDatos.nombre);
						                	}
										};
										serCursor = 'pointer'
									}
									
									aDatos.series[m]={'nombre':i,valores:val};
									aSeries['zona_'+idx][m] = chart.addSeries({
										id:idx,
										name:i,
										data:val,
										type:'column',
										stack:'z'+idx,
										visible: true,
										shadow: false,
							            cursor: serCursor,
						            	events: {
								                click: evClick.click
							            },
							            point:{
							            	events: {
								                click: evClick.click
								            }
					            		}
										
									});

									m++;
								});

								$(e.target).removeAttr('disabled');
								lasTablas(aDatos);
							});
						/*}
						else
						{
							for(s in aSeries['zona_'+idx])
							{
								aSeries['zona_'+idx][s].show();
							}
						}
						*/
					}
					else
					{
						for(s in aSeries['zona_'+idx])
						{
							var idSer = aSeries['zona_'+idx][s].options.id;
							aSeries['zona_'+idx][s].remove();
							//$('#tabla_zona_'+idx).remove();
							$('#tabla_zona_'+idx+'_wrapper').remove();
						}
					}
				});


		/* Bind controles visibilidad metas y acumulados */
				$('#visglobal').click(function(e){
					if($(e.target).attr('checked')) 
					{
						 for(var id in aSerGlobal)
						 {
							 if(typeof aSerGlobal[id] != "undefined") aSerGlobal[id].show();
						 }
					}
					else
					{
						 for(var id in aSerGlobal)
						 {
							 if(typeof aSerGlobal[id] != "undefined") aSerGlobal[id].hide();
						 }
					}
				});


				
			});

	
		
		///Tratamos de hacer las tablas
		function lasTablas(oDatos)
		{

			var nombreTabla = oDatos.nombre;
			var id = oDatos.id;
			var t = {};
			var html = '<table id="tabla_'+id+'" class="datos"><caption class="ui-state-default">'+nombreTabla+'</caption>';
			
			var aFilas = {};
			var dt = [];
			var headCols = ['Periodo'];
			for(nval in oDatos.series){			
				//aFilas={};
				var s = oDatos.series[nval];
				
				var nSer = s.nombre;
				headCols.push(nSer);
				var val = s.valores;

				//Convertimos de filas a columnas				
				jQuery.each(val,function(j,a)
				{
					if(typeof dt[a[0]] == 'undefined')
					{
						dt[a[0]] = 0; //Indice del segundo nivel, la llave es el num de columna
						aFilas[ a[0] ] = [];
						aFilas[ a[0] ][ dt[ a[0] ] ] = Highcharts.numberFormat(a[1],'2','.',',');
					}
					else if (dt[a[0]] >= 0)
					{
						dt[a[0]]++;
						aFilas[a[0]][dt[a[0]]] = Highcharts.numberFormat(a[1],'2','.',',');
					}
					else
					{
						console.log('Nodeberia: dt[a[0]]:'+dt[a[0]]);
					}
					
				});
			}

			//Construimos las cabezas de la tabla
			html += '<thead><tr>';
			jQuery.each(headCols,function(j,a)
			{
				html += '<th class="ui-state-default">'+a+'</th>';
			});
			html += '</tr></thead>';
			html += '<tbody>';

			//Construimos las filas de la tabla
			jQuery.each(aFilas,function(j,a)
			{
				html += '<tr>';
				html += '<td>'+Highcharts.dateFormat('%b %Y',j)+'</td>';
				jQuery.each(a,function(fe,b)
				{
						html += '<td align="right">'+b+'</td>';
				});
				html += '</tr>';
			});
			html += '</tbody></table>';
			
			t = $(html);
			$('#tablas').append(t);
			$('#tabla_'+id).dataTable(tXopc);
			$('#tabla_'+id).css('border','thin solid silver');
			$('#tabla_'+id+'_wrapper').css("float","left");

		}

		/**
		 * Hace llamada al sub detalle y lo abre en el div de subdetalle.
		 */
		function subDetalle(tipo, id, fecha, valor, nombre)
		{
			console.log('en subdetalle');
			var params = {};
			params.tipo = tipo;
			params.id = id;
			params.fecha = fecha;
			params.valor = valor;
			params.subdetalle = true;
			
			$('#sub_detalle').load('<?php echo url_for("reporte/subDetalle")?>', params, function() {
				
				//Inicializamos la ventana de subdetalle
				subLlamada('<?php echo url_for("reporte/subDetalleCobranza")?>',params);
				$('#sub_detalle_ventana').dialog('open');
				$('#sub_detalle_ventana').dialog( "option", "width", 1024 );
				$('#sub_detalle_ventana').dialog( "option", "height", 600 );
				$('#sub_detalle_ventana').dialog( "option", "position", ['center','center'] );
				$('#sub_detalle_ventana').dialog( "option", "title", nombre);
									
			});


		
			
		}	
		
		</script>

<div id="cont_general">

	<div class="clearfix">
		<div id="cont_grafica" class="cg">
			<div id="grafica-detalle"
				style="width: 100%; height: 450px; margin: 0 auto;"></div>
		</div>

		<div style="width: 20%; margin-left: 10px; float: right;"
			class="clearfix">
			<div>
				<div>
					<a href="<?php echo url_for('reporte/indicadoresCapacidad')?>">Indicadores</a>
					| <a href="javascript:void(0);" onclick="window.location.reload();">Reset</a>
				</div>
				<hr>
				<div>
					<input type="checkbox" value="visglobal" id="visglobal" checked />
					<label for="visglobal">Mostrar Global</label>
				</div>
				<hr>
				<div id="zonas">
					<?php include_partial('listaZonas', array('Zonas'=>$Zonas))?>
				</div>
				<hr>
				<div id="centros">
					<?php include_partial('listaCentros', array('Centros'=>$Centros))?>
				</div>
			</div>
		</div>
	</div>

	<table id="tabla_datos" width="100%" class="datos">
		<thead>
			<tr>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>

	<div id="tablas"></div>

	<div id="sub_detalle_ventana">
		<div id="sub_detalle"></div>
	</div>

</div>
