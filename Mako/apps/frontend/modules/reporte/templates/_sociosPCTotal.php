<script type="text/javascript" src="/js/jquery.1.6.min.js"></script>
<script
	type="text/javascript" src="/js/charts/highcharts.src.js"></script>

<script type="text/javascript">
			var chart;
			var chart2;
			var socTot;
			$(document).ready(function() {

				chart = new Highcharts.Chart({
					chart: {
						renderTo: 'socios_tot',
						shadow: true,
				        events: {
					        click: function(){
						        window.location = '<?php echo url_for('reporte/indicadoresDetalleZoom')."?caso=registros_total"?>';
					        },
				            selection: function(event) {
				                if (event.xAxis) {
				                	console.log(
				                			Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', event.xAxis[0].min),
				                			Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', event.xAxis[0].max)
				                		);
				                } else {
				                    console.log('Selection reset');
				                }
				            }				            
				        },
				        type: 'spline',
						animation: false,
						height: 300,
						zoomType:'x'
					},
					credits: {
						enabled: false
					},
					title: {
						text: 'Registros'
					},
					subtitle: {
						text: 'Registros Totales y Metas'	
					},
					xAxis: {
						type: 'datetime',
						dateTimeLabelFormats: {
							month: '%b %Y',
							year: '%Y'
						}
					},
					yAxis: {
						title: {
							text: 'Personas'
						},
					    labels: {
					    	formatter: function() {
					        	return $.fn.addCommas(this.value);
							}
					    },
						min: 0
					},
					tooltip: {
						formatter: function() {
				                return '<b>'+ this.series.name +'</b><br/>'+
								Highcharts.dateFormat('%b %Y', this.x) +': '+ $.fn.addCommas(this.y) +' personas';
						}
					}
				});
				
				
				jQuery.get('<?php echo url_for('reporte/RegistrosSociosTotales')?>', null, function(datos, state, xhr) {
					
					socTot = datos;
					jQuery.each(datos, function(i, val) {
						chart.addSeries({
							id:i,
							name:i,
							data:val
						});
						
					});
				});


				chart2 = new Highcharts.Chart({
					chart: {
						renderTo: 'socios_meta',
						shadow: true,
				        events: {
					        click: function(){
						        window.location = '<?php echo url_for('reporte/indicadoresDetalleZoom')."?caso=registros_total_pcs"?>';
					        },
				            selection: function(event) {
				                if (event.xAxis) {
				                	console.log(
				                			Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', event.xAxis[0].min),
				                			Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', event.xAxis[0].max)
				                		);
				                } else {
				                    console.log('Selection reset');
				                }
				            }				            
				        },
						
						type: 'spline',
						animation: false,
						height: 300
					},
					credits: {
						enabled: false
					},
					title: {
						text: 'Registros por PC'
					},
					subtitle: {
						text: 'Registros Totales y Meta por PC'	
					},
					xAxis: {
						type: 'datetime',
						dateTimeLabelFormats: {
							month: '%b %Y',
							year: '%Y'
						}
					},
					yAxis: {
						title: {
							text: 'Personas/PC'
						},
					    labels: {
					    	formatter: function() {
					        	return $.fn.addCommas(this.value);
							}
					    },
						min: 0
					},
					tooltip: {
						formatter: function() {
				                return '<b>'+ this.series.name +'</b><br/>'+
								Highcharts.dateFormat('%b %Y', this.x) +': '+ $.fn.addCommas(this.y) +' personas';
						}
					}
				});
				
				var datos;
				jQuery.get('<?php echo url_for('reporte/RegistrosSociosPC')?>', null, function(datos, state, xhr) {
					jQuery.each(datos, function(i, val) {
						chart2.addSeries({
							id:i,
							name:i,
							data:val
						});
						
					});
				});


				
			});
				
		</script>
<div style="position: relative; z-index: 1010;" id="cont1">
	<div id="socios_tot"
		style="width: 400px; height: 320px; margin: 0 auto"></div>
</div>
<div
	id="socios_meta" style="width: 400px; height: 320px; margin: 0 auto"></div>

