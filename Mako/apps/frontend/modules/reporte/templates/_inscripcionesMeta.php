<script type="text/javascript" src="/js/jquery.1.6.min.js"></script>
<script
	type="text/javascript" src="/js/charts/highcharts.src.js"></script>

<script type="text/javascript">
			var charti;
			var charti2;
			$(document).ready(function() {

				charti = new Highcharts.Chart({
					chart: {
						renderTo: 'inscripciones_tot',
						shadow: true,
				        events: {
					        click: function(){
						        window.location = '<?php echo url_for('reporte/indicadoresDetalleZoom')."?caso=inscripciones_total"?>';
					        },
				            selection: function(event) {
				                if (event.xAxis) {
				                	console.log(
				                			Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', event.xAxis[0].min),
				                			Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', event.xAxis[0].max)
				                		);
				                } else {
				                    console.log('Selection reset');
				                }
				            }				            
				        },
						
						type: 'spline',
						animation: false,
						height: 300
					},
					credits: {
						enabled: false
					},
					title: {
						text: 'Inscripciones'
					},
					subtitle: {
						text: 'Inscripciones Totales y Metas'	
					},
					xAxis: {
						type: 'datetime',
						dateTimeLabelFormats: {
							month: '%b %Y',
							year: '%Y'
						}
					},
					yAxis: {
						title: {
							text: 'Personas'
						},
					    labels: {
					    	formatter: function() {
					        	return $.fn.addCommas(this.value);
							}
					    },
						min: 0
					},
					tooltip: {
						formatter: function() {
				                return '<b>'+ this.series.name +'</b><br/>'+
								Highcharts.dateFormat('%b %Y', this.x) +': '+ $.fn.addCommas(this.y) +' personas';
						}
					}
				});
				
				var datos;
				jQuery.get('<?php echo url_for('reporte/InscripcionesTotales')?>', null, function(datos, state, xhr) {
					
					jQuery.each(datos, function(i, val) {
						charti.addSeries({
							id:i,
							name:i,
							data:val
						});
						
					});
				});


				charti2 = new Highcharts.Chart({
					chart: {
						renderTo: 'inscripciones_meta',
						shadow: true,
				        events: {
					        click: function(){
						        window.location = '<?php echo url_for('reporte/indicadoresDetalleZoom')."?caso=inscripciones_total_cursos"?>';
					        },
				            selection: function(event) {
				                if (event.xAxis) {
				                	console.log(
				                			Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', event.xAxis[0].min),
				                			Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', event.xAxis[0].max)
				                		);
				                } else {
				                    console.log('Selection reset');
				                }
				            }				            
				        },
						
						type: 'spline',
						animation: false,
						height: 300
					},
					credits: {
						enabled: false
					},
					title: {
						text: 'Inscripciones por Curso'
					},
					subtitle: {
						text: 'Inscripciones Totales y Meta por Curso'	
					},
					xAxis: {
						type: 'datetime',
						dateTimeLabelFormats: {
							month: '%b %Y',
							year: '%Y'
						}
					},
					yAxis: {
						title: {
							text: 'Personas/Curso'
						},
					    labels: {
					    	formatter: function() {
					        	return $.fn.addCommas(this.value);
							}
					    },
						min: 0
					},
					tooltip: {
						formatter: function() {
				                return '<b>'+ this.series.name +'</b><br/>'+
								Highcharts.dateFormat('%b %Y', this.x) +': '+ $.fn.addCommas(this.y) +' personas';
						}
					}
				});
				
				var datos;
				jQuery.get('<?php echo url_for('reporte/InscripcionesCurso')?>', null, function(datos, state, xhr) {
					
					jQuery.each(datos, function(i, val) {
						charti2.addSeries({
							id:i,
							name:i,
							data:val
						});
						
					});
				});


				
			});
				
		</script>

<div
	id="inscripciones_tot"
	style="width: 400px; height: 320px; margin: 0 auto"></div>
<div
	id="inscripciones_meta"
	style="width: 400px; height: 320px; margin: 0 auto"></div>

