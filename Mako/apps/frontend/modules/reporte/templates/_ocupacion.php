<script type="text/javascript" src="/js/jquery.1.6.min.js"></script>
<script
	type="text/javascript" src="/js/charts/highcharts.src.js"></script>

<script type="text/javascript">
			$(document).ready(function() {

				chart3 = new Highcharts.Chart({
					chart: {
						renderTo: 'ocupacion',
						shadow: true,
				        events: {
					        click: function(){
						        window.location = '<?php echo url_for('reporte/indicadoresDetalleZoom')."?caso=minutos_utilizados"?>';
					        },
				            selection: function(event) {
				                if (event.xAxis) {
				                	console.log(
				                			Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', event.xAxis[0].min),
				                			Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', event.xAxis[0].max)
				                		);
				                } else {
				                    console.log('Selection reset');
				                }
				            }				            
				        },
				        type: 'spline',
						animation: false,
						height: 300,
						zoomType:'x'
					},
					credits: {
						enabled: false
					},
					title: {
						text: 'Minutos Utilizados'
					},
					subtitle: {
						text: 'Total de Minutos Utilizados'	
					},
					xAxis: {
						type: 'datetime',
						dateTimeLabelFormats: {
							month: '%b %Y',
							year: '%Y'
						}
					},
					yAxis: {
						title: {
							text: 'Minutos Utilizados (miles)'
						},
					    labels: {
					    	formatter: function() {
					        	return Highcharts.numberFormat(this.value/1000,0);
							}
					    },
						min: 0
					},
					tooltip: {
						formatter: function() {
				                return '<b>'+ this.series.name +'</b><br/>'+
								Highcharts.dateFormat('%b %Y', this.x) +': '+ Highcharts.numberFormat(this.y,0) +' minutos';
						}
					}
				});
				
				
				jQuery.get('<?php echo url_for('reporte/Ocupacion')?>', null, function(datos, state, xhr) {
					
					socTot = datos;
					jQuery.each(datos, function(i, val) {
						chart3.addSeries({
							id:i,
							name:i,
							data:val
						});
						
					});
				});


				chart4 = new Highcharts.Chart({
					chart: {
						renderTo: 'tasa_ocupacion',
						shadow: true,
				        events: {
					        click: function(){
						        window.location = '<?php echo url_for('reporte/indicadoresDetalleZoom')."?caso=tasa_minutos_utilizados"?>';
					        },
				            selection: function(event) {
				                if (event.xAxis) {
				                	console.log(
				                			Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', event.xAxis[0].min),
				                			Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', event.xAxis[0].max)
				                		);
				                } else {
				                    console.log('Selection reset');
				                }
				            }				            
				        },
						
						type: 'spline',
						animation: false,
						height: 300
					},
					credits: {
						enabled: false
					},
					title: {
						text: 'Tasa de Ocupación'
					},
					subtitle: {
						text: 'Minutos Utilizados por Minutos Disponibles'	
					},
					xAxis: {
						type: 'datetime',
						dateTimeLabelFormats: {
							month: '%b %Y',
							year: '%Y'
						}
					},
					yAxis: {
						title: {
							text: 'Minutos Utilizados/Disponibles'
						},
					    labels: {
					    	formatter: function() {
					        	return Highcharts.numberFormat(this.value);
							}
					    },
						min: 0
					},
					tooltip: {
						formatter: function() {
				                return '<b>'+ this.series.name +'</b><br/>'+
								Highcharts.dateFormat('%b %Y', this.x) +': '+ Highcharts.numberFormat(this.y,2,'.',','); +' ';
						}
					}
				});
				
				var datos;
				jQuery.get('<?php echo url_for('reporte/TasaOcupacion')?>', null, function(datos, state, xhr) {
					jQuery.each(datos, function(i, val) {
						chart4.addSeries({
							id:i,
							name:i,
							data:val
						});
						
					});
				});


				
			});
				
		</script>
<div style="position: relative; z-index: 1010;" id="cont1">
	<div id="ocupacion" style="width: 400px; height: 320px; margin: 0 auto"></div>
</div>
<div
	id="tasa_ocupacion" style="width: 400px; height: 320px; margin: 0 auto"></div>

