<script>

	$(function() {
		var $tabsvd = $('#tabsVerDetalle').tabs();
	
	});
	
	
	var oTable;
	$(document).ready(function() {
	
	   oTable = $('.registros').dataTable({
			"bPaginate": true,
			"bLengthChange": true,
			"bFilter": true,
			"bSort": true,
			"bInfo": true,
			"bAutoWidth": false,
	        "bJQueryUI": true,
	        "sPaginationType": "full_numbers"
	     });
	   
	});
</script>

<?php use_stylesheet('./reporte/_verDetalleDetalleCafeteria.css')?>


<div id="contenedorVerDetalle">
	<div id="tabsVerDetalle">

		<ul id="tabMenu-vd">
			<li><a href="#personales-vd">Detalle</a></li>
		</ul>


		<div id="personales-vd" style="width: 80%;">

			<h3>Detalle</h3>
			<table width="850" border="0" id="registrosCompras" class="registros">
				<thead>
					<tr>
						<th>Código</th>
						<th>Producto</th>
						<?php  
          foreach($RangoFechas as $fecha){	?>
						<!-- BEGIN carrito -->
						<th><?php echo strftime("%A", strtotime($fecha)); ?></th>
						<!-- END carrito -->
						<?php  } ?>
					</tr>
				</thead>
				<tbody>

					<?php

					$Producto = $Productos->getRawValue();
					foreach($Producto as $Codigo)
					{
						?>
					<!-- BEGIN carrito -->
					<tr>
						<td><?php echo key($Codigo) ?></td>
						<td><?php echo key($Producto); ?></td>
						<?php  foreach($Codigo as $fecha => $valor) {        	
							foreach($valor as $cuenta) {
								?>
						<td align="right">$<?php echo number_format($cuenta,2); ?>
						</td>
						<?php 	
							}
						}
						?>
					</tr>
					<!-- END carrito -->

					<?php  } ?>
				</tbody>
			</table>
		</div>


	</div>



</div>
</div>


