
<script
	type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
<script
	type="text/javascript" src="/js/charts/highcharts.src.js"></script>

<script type="text/javascript">
			var chart;
			$(document).ready(function() {

				chart = new Highcharts.Chart({
					chart: {
						renderTo: 'container',
						type: 'spline',
						height: 300
					},
					title: {
						text: 'Registros'
					},
					subtitle: {
						text: 'Registros totales'	
					},
					xAxis: {
						type: 'datetime',
						dateTimeLabelFormats: {
							month: '%b %Y',
							year: '%Y'
						}
					},
					yAxis: {
						title: {
							text: 'Personas'
						},
						min: 0
					},
					tooltip: {
						formatter: function() {
				                return '<b>'+ this.series.name +'</b><br/>'+
								Highcharts.dateFormat('%e. %b %Y', this.x) +': '+ this.y +' personas';
						}
					}
				});
				
				var datos;
				jQuery.get('<?php echo url_for('reporte/RegistrosSociosPC')?>', null, function(datos, state, xhr) {
					console.log(datos);
					jQuery.each(datos, function(i, val) {
						chart.addSeries({
							id:i,
							name:i,
							data:val
						});
						
					});
				});
			});
				
		</script>

<div
	id="container" style="width: 800px; height: 400px; margin: 0 auto"></div>

