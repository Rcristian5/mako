<?php use_javascript('jquery.ui.js') ?>
<?php use_javascript('plugins/jquery.dataTables.js') ?>
<?php use_stylesheet('datatables.css') ?>

<script type="text/javascript">
<!--
$(document).ready(function() {


	
	$('.datos').dataTable({
		"bPaginate": false,
		"bLengthChange": false,
		"bFilter": false,
		"bSort": false,
		"bInfo": false,
		"bAutoWidth": false,
	    "bJQueryUI": true,
	    "sPaginationType": "full_numbers",
	    "aaSorting": [],
	    "sDom": 'T<"fg-toolbar ui-widget-header ui-corner-tl ui-corner-tr ui-helper-clearfix"lfr>t<"fg-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix"ip>'
	
	 });

	$(".datos").find('th').each(function(){$(this).addClass("ui-state-default");});

});
//-->
</script>


<?php use_stylesheet('./reporte/rankingSuccess.css')?>

<div>
	<?php include_partial('rankingCentros', array("Rankings"=>$Rankings)) ?>
</div>
<div class="clearfix">
	<div class="renglones">
		<?php include_partial('rankingF1', array("RankF1"=>$RankF1)) ?>
	</div>
	<div class="renglones">
		<?php include_partial('rankingF2', array("RankF2"=>$RankF2)) ?>
	</div>
</div>
<div class="clearfix">
	<div class="renglones">
		<?php include_partial('rankingF3', array("RankF3"=>$RankF3)) ?>
	</div>
	<div class="renglones">
		<?php include_partial('rankingF4', array("RankF4"=>$RankF4)) ?>
	</div>
</div>
<div class="clearfix">
	<div class="renglones">
		<?php include_partial('rankingF5', array("RankF5"=>$RankF5)) ?>
	</div>
	<div class="renglones">
		<?php include_partial('rankingF6', array("RankF6"=>$RankF6)) ?>
	</div>
</div>

<div class="clearfix">

	<div class="renglones">
		<?php include_partial('rankingF7', array("RankF7"=>$RankF7)) ?>
	</div>

	<div class="renglones">
		<?php include_partial('rankingTotal', array("rnkGeneral"=>$rnkGeneral)) ?>

	</div>
</div>
