<?php use_stylesheet('./reporte/indicadoresCapacidadSuccess.css')?>

<div id="graficas">
	<div align="center" id="cont" class="clearfix">
		<div class="caja">
			<?php include_partial('ocupacion', array()) ?>
		</div>
		<div class="caja">
			<?php include_partial('ocupacionHoras', array()) ?>
		</div>
		<div class="caja">
			<?php include_partial('ocupacionDias', array()) ?>
		</div>
	</div>
</div>
