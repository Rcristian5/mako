<?php use_stylesheet('./reporte/indicadoresSuccess.css')?>

<div id="graficas">
	<div align="center" id="cont" class="clearfix">
		<div class="caja">
			<?php include_partial('sociosPCTotal', array()) ?>
		</div>
		<div class="caja">
			<?php include_partial('inscripcionesMeta', array()) ?>
		</div>
		<div class="caja">
			<?php include_partial('cobranzaMeta', array()) ?>
		</div>
	</div>
</div>
