<?php use_javascript('jquery.ui.js') ?>

<?php use_javascript('plugins/jquery.dataTables.js') ?>
<?php use_stylesheet('datatables.css') ?>


<script type="text/javascript">
	$(function() {
		
			$("#tabs").tabs({
				select: function(event, ui) {
				
					var seccion_id = $('#'+ui.panel.id).attr('seccion_id');
					if(seccion_id == undefined) return;
					$.getJSON('<?php echo url_for('switch_pcs/estatusPcs') ?>',{'id':seccion_id}, function(data) {
						for(pc in data)
						{
							console.log(data[pc]+ ' '+ pc);
							if(data[pc] == 0)
							{
								$('#pc-'+pc).removeClass('encendida').addClass('apagada');
								$('#pc-'+pc).attr('estado','apagado');
							}
							else
							{
								$('#pc-'+pc).attr('estado','encendido');
								$('#pc-'+pc).removeClass('apagada').addClass('encendida');
							}
						}
						  
					});					
				}
			});


			$('#tabs').bind('tabsshow',function(ev,ui){
				$('.pc').removeClass('apagada').removeClass('encendida');
			});
			
			$(document).ajaxSend(function() {
				  
				  $('.ajax-loader').show();
			});

			$(document).ajaxComplete(function() {
				  $('.ajax-loader').hide();
			});


			$('.liga-pc').bind('click',function(){
				console.log($(this).closest('div').attr('estado'));
				var estado = $(this).closest('div').attr('estado');
				var pc_id = $(this).closest('div').attr('pc_id');
				if(estado == undefined) return;
				$.getJSON('<?php echo url_for('switch_pcs/controlPc') ?>',{'id':pc_id,'estado':estado}, function(data) {
					console.log(data);
					alert('Se ha enviado orden.');					
				});
				
			});
			
			$('.liga-seccion').bind('click',function(){

				if($(this).closest('div').attr('estado') =='apagado'){
					if(!confirm('Acaba de oprimir el botón de apagado de todas las máquinas de ésta sección.\n¿Desea continuar con esta acción?'))
						return false;
				}
				
				console.log($(this).closest('div').attr('estado'));
				
				var estado = $(this).closest('div').attr('estado');
				var seccion_id = $(this).closest('div').attr('seccion_id');
				console.log("id_seccion :"+seccion_id);
				if(estado == undefined) return;
				$.getJSON('<?php echo url_for('switch_pcs/controlSeccion') ?>',{'id':seccion_id,'estado':estado}, function(data) {
					console.log(data);
					alert('Se ha enviado orden.');					
				});
				
			});

			$('.liga-centro').bind('click',function(){

				if($(this).closest('div').attr('estado') =='apagado'){
					if(!confirm('Acaba de oprimir el botón de apagado de todas las máquinas de éste centro.\n¿Desea continuar con esta acción?'))
						return false;
				}
				
				console.log($(this).closest('div').attr('estado'));
				var estado = $(this).closest('div').attr('estado');
				if(estado == undefined) return;
				$.getJSON('<?php echo url_for('switch_pcs/controlCentro') ?>',{'estado':estado}, function(data) {
					console.log(data);
					alert('Se ha enviado orden.');					
				});
				
			});
			
	});


	
	</script>

<?php use_stylesheet('./switch_pcs/indexSuccess.css')?>


<div class="ui-widget-header ui-corner-top tsss" style="padding: 5px;">Administrar
	energía de PCs</div>
<div class="ui-widget ui-corner-bottom sombra">
	<div class="ui-widget-content ui-corner-bottom" style="padding: 5px;">

		<?php if ($sf_user->hasFlash('ok')): ?>
		<div
			class="flash_ok ui-state-highlight ts sombra-delgada ui-corner-all"
			style="padding: 5px; margin-top: 10px; margin-bottom: 10px;">
			<span style="float: left; margin-right: 0.3em;"
				class="ui-icon ui-icon-info"></span> <strong><?php echo $sf_user->getFlash('ok') ?>
			</strong>
		</div>
		<?php endif; ?>


		<div id="tabs">
			<ul>
				<li><span style="float: left;" class="ui-icon ui-icon-power"></span><a
					href="#tabs-1">CENTRO</a></li>
				<?php foreach($Secciones as $seccion): //$seccion = new Seccion(); ?>
				<li><span style="float: left;" class="ui-icon ui-icon-power"></span>
					<a href="#tabs-<?php echo $seccion->getId() ?>"
					seccion_id="<?php echo $seccion->getId() ?>"><?php echo $seccion->getNombre() ?>
				</a>
				</li>
				<?php endforeach; ?>
			</ul>


			<div id="tabs-1">
				<div class="clearfix">
					<p>Encender o apagar máquinas de todo el centro.</p>
					<div class="switch liga-centro" estado="encendido">
						<a href="javascript:void(0);"
							title="Enviar señal de encendido para todo el centro"><img
							border="0" src="/imgs/iconos/switch_on.png" /> </a>
					</div>
					<div class="switch liga-centro" estado="apagado">
						<a href="javascript:void(0);"
							title="Enviar señal de apagado para todo el centro"><img
							border="0" src="/imgs/iconos/switch_off.png" /> </a>
					</div>

				</div>
			</div>

			<?php foreach($Secciones as $seccion): //$seccion = new Seccion(); ?>
			<div id="tabs-<?php echo $seccion->getId() ?>" class="clearfix"
				seccion_id="<?php echo $seccion->getId() ?>">

				<div class="clearfix">
					<div class="switch liga-seccion" estado="encendido"
						seccion_id="<?php echo $seccion->getId() ?>">
						<a href="javascript:void(0);"
							title="Enviar señal de encendido para todas las máquinas de ésta sección"><img
							border="0" src="/imgs/iconos/switch_on.png" /> </a>
					</div>
					<div class="switch liga-seccion" estado="apagado"
						seccion_id="<?php echo $seccion->getId() ?>">
						<a href="javascript:void(0);"
							title="Enviar señal de apagado para todas las máquinas de ésta sección"><img
							border="0" src="/imgs/iconos/switch_off.png" /> </a>
					</div>
					<div class="switch ajax-loader" style="display: none;">
						<img src="/imgs/ajax-circle.gif">
					</div>
				</div>

				<div>
					<?php foreach($seccion->getComputadoras() as $pc): //$pc = new Computadora(); ?>

					<div
						class="pc ui-corner-all ui-widget ui-widget-content sombra-delgada ts"
						id="pc-<?php echo $pc->getId() ?>"
						pc_id="<?php echo $pc->getId() ?>">
						<a href="" onclick="return false;" class="liga-pc">
							<div class="numero-pc">
								<?php echo $pc->getAlias();?>
							</div> <img src="/imgs/iconos/libre.png" border="0"
							title="Oprima para encender o apagar" width="64" height="64" />
						</a>
					</div>

					<?php endforeach;?>

				</div>
			</div>
			<?php endforeach;?>
		</div>

	</div>
</div>

