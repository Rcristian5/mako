<?php

/**
 * switch_pcs actions.
 * Realiza las funciones de encendido y apagado de las pcs.
 *
 * @package    mako
 * @subpackage admin_pc
 * @author     Bixit SA de CV
 * @version    SVN: $Id: actions.class.php,v 1.2 2010/10/27 06:18:53 eorozco Exp $
 */
class switch_pcsActions extends sfActions
{
	public function executeIndex(sfWebRequest $request)
	{
		//Funciona solo para las pcs de este centro.
		$centro_id = sfConfig::get('app_centro_actual_id');
		$c = new Criteria();
		$c->addAscendingOrderByColumn(SeccionPeer::ID);
		$c->add(SeccionPeer::CENTRO_ID,$centro_id);
		$this->Secciones = SeccionPeer::doSelect($c);
		 
		//$this->computadoras = ComputadoraPeer::doSelect($c);
	}

	public function executeEstatusPcs(sfWebRequest $request)
	{
		 
		$seccion_id = $request->getParameter('id');
		//error_log("Llega aqui: $seccion_id");
		$this->getResponse()->setContentType('application/json');
		$seccion = SeccionPeer::retrieveByPK($seccion_id);
		$ar = array();
		foreach($seccion->getComputadoras() as $pc)
		{
			$res = exec("/usr/bin/perl /opt/MakoScripts/ping.pl ".$pc->getIp());
			$ar[$pc->getId()] =	$res;
		}
		return $this->renderText(json_encode($ar));
		 
	}


	public function executeControlCentro(sfWebRequest $request)
	{
		$ar = array();
		$this->getResponse()->setContentType('application/json');
		$centro_id = sfConfig::get('app_centro_actual_id');
		$cmd = $request->getParameter('estado');
		$centro = CentroPeer::retrieveByPK($centro_id);
		$alias = $centro->getAlias();
		if($cmd == 'apagado')
		{
			$res = exec("mako soporte:apagar-pcs --centro=$alias");
		}
		else
		{
			$res = exec("mako soporte:encender-pcs --centro=$alias");
		}

		return $this->renderText(json_encode($res));
	}

	public function executeControlSeccion(sfWebRequest $request)
	{
		$ar = array();
		$this->getResponse()->setContentType('application/json');
		$centro = CentroPeer::retrieveByPK(sfConfig::get('app_centro_actual_id'));
		$alias = $centro->getAlias();
		$seccion_id = $request->getParameter('id');
		$cmd = $request->getParameter('estado');
		$seccion = SeccionPeer::retrieveByPK($seccion_id);
		$aula = $seccion->getNombre();
		if($cmd == 'apagado')
		{
			error_log("mako soporte:apagar-pcs --centro=$alias --aula=\"$aula\"");
			$res = exec("mako soporte:apagar-pcs --centro=$alias --aula=\"$aula\"");
		}
		else
		{
			$res = exec("mako soporte:encender-pcs --centro=$alias --aula=\"$aula\"");
		}
		return $this->renderText(json_encode($res));
	}

	public function executeControlPc(sfWebRequest $request)
	{
		$ar = array();
		$this->getResponse()->setContentType('application/json');
		$computadora_id = $request->getParameter('id');
		$estado = $request->getParameter('estado');
		$pc = ComputadoraPeer::retrieveByPK($computadora_id);
		$cmd=$estado;
		$res = $this->switchpc($pc, $cmd);
		return $res;
	}

	private function switchpc(Computadora $pc,$cmd)
	{
		error_log("Cmd: $cmd ip: ".$pc->getIp());
		$centro = CentroPeer::retrieveByPK(sfConfig::get('app_centro_actual_id'));
		$alias = $centro->getAlias();
		if($cmd == 'apagado')
		{
			error_log("Entramos en encender :" ."mako soporte:encender-pcs --ip=".$pc->getIp());
			$res = exec("mako soporte:encender-pcs --ip=".$pc->getIp());
		}
		else
		{
			error_log("Entramos en apagar: " ."mako soporte:apagar-pcs --ip=".$pc->getIp());
			$res = exec("mako soporte:apagar-pcs --ip=".$pc->getIp());
		}
		return $this->renderText(json_encode($res));

		 
	}


}
