<?php

/**
 * cancelacion_venta actions.
 *
 * @package    mako
 * @subpackage cancelacion_venta
 * @author     Bixit SA de CV
 * @version    SVN: $Id: actions.class.php,v 1.10 2010/10/11 07:09:14 eorozco Exp $
 */
class cancelacion_ventaActions extends sfActions
{
	public function executeIndex(sfWebRequest $request)
	{
		$this->Cancelacions = CancelacionPeer::doSelect(new Criteria());
	}

	public function executeShow(sfWebRequest $request)
	{
		$this->Cancelacion = CancelacionPeer::retrieveByPk($request->getParameter('id'));
		$this->forward404Unless($this->Cancelacion);
	}

	public function executeNew(sfWebRequest $request)
	{
		$this->form = new CancelacionForm();
	}

	public function executeCreate(sfWebRequest $request)
	{
		$this->forward404Unless($request->isMethod(sfRequest::POST));

		$this->form = new CancelacionForm();

		$this->processForm($request, $this->form);

		$this->setTemplate('new');
	}

	/**
	 * Pantalla de autorización de la operación de cancelación de venta.
	 * @param sfWebRequest $request
	 * @return void
	 */
	public function executeGuiAuth(sfWebRequest $request)
	{
		 
	}

	/**
	 * Busca los datos ingresados en la forma de autorización en la BD
	 * @param sfWebRequest $request
	 * @return void
	 */
	public function executeAuth(sfWebRequest $request)
	{
		$this->forward404Unless($request->isMethod(sfRequest::POST));
		$usuario = $request->getParameter('usuario');
		$clave = $request->getParameter('clave');
		 
		//Roles autorizadores
		$autorizados = array(
				sfConfig::get('app_rol_super_usuario_id'),
				sfConfig::get('app_rol_supervisor_id'),
				sfConfig::get('app_rol_facilitador_encargado_id'),
				sfConfig::get('app_rol_facilitador_id'),
				sfConfig::get('app_rol_administrador_operaciones_id')
		);
		$c = new Criteria();
		$c->add(UsuarioPeer::USUARIO,$usuario);
		$c->add(UsuarioPeer::CLAVE,$clave);
		$c->add(UsuarioPeer::ACTIVO,true);
		 
		$c->add(UsuarioPeer::ROL_ID,$autorizados,Criteria::IN);
		 
		//TODO: validar centro autorizado para este usuario
		 
		$usuario = UsuarioPeer::doSelectOne($c);
		if($usuario == null)
		{
			// Reingenieria Mako C. ID 55. Responsable:  FE. Fecha: 07-04-2014.  Descripción del cambio: Se corrigió ortografía en el mensaje
			$this->getUser()->setFlash('error', 'Los datos proporcionados no corresponden a un usuario autorizado. Vuelva a intentar.');
			$this->redirect('cancelacion_venta/guiAuth');
			// Fecha: 07-04-2014 Fin
		}
		else
		{
			$this->getUser()->setFlash('autorizado', 'ok');
			$this->redirect('cancelacion_venta/new');
		}
	}


	protected function processForm(sfWebRequest $request, sfForm $form)
	{

		 
		$folio = intval($request->getParameter('folio_a_cancelar'));
		$id_transaccion = $request->getParameter('id_transaccion');
		$observaciones = $request->getParameter('observaciones');

		if(!($folio == '' || $id_transaccion == ''))
		{
			//Obtenemos las orden original a cancelar.
			$c = new Criteria();
			$c->add(OrdenPeer::FOLIO, $folio);
			$c->add(OrdenPeer::ID, $id_transaccion);
			$c->add(OrdenPeer::TIPO_ID, sfConfig::get('app_orden_venta_id'));
			$orden_a_cancelar = OrdenPeer::doSelectOne($c);
			if($orden_a_cancelar == null)
			{
				$this->getUser()->setFlash('error', 'No se ha encontrado la venta ingresada, no se ha podido cancelar, verifique los datos proporcionados');
				$this->redirect('cancelacion_venta/new');
			}
			//SI ya esta cancelada la venta ponemos mensaje
			elseif($orden_a_cancelar->getEstatusId() == sfConfig::get('app_estatus_cancelado_id'))
			{
				$this->getUser()->setFlash('error', sprintf('La venta ya ha sido cancelada anteriormente ( %s ).',$orden_a_cancelar->getUpdatedAt()));
				$this->redirect('cancelacion_venta/new');
			}
			else
			{
				//Asociamos la orden con el operador:
				$operador = $this->getUser()->getAttribute('usuario');
				$form->getObject()->setUsuario($operador);

				//Relacionamos la orden a cancelar con la cancelación
				$form->getObject()->setOrdenRelatedByOrdenId($orden_a_cancelar);

				//Asociamos los comentarios al respecto de la cancelacion
				$form->getObject()->setObservaciones($observaciones);

				//Generamos la orden de cancelación
				$orden_canc = $orden_a_cancelar->copy();

				//Reseteamos valores de la orden de cancelacion
				$orden_canc->setCorteId(null);
				$orden_canc->setCreatedAt(time());
				$orden_canc->setUpdatedAt(time());
				$orden_canc->setId(Comun::generaId());

				//La orden de cancelación es idéntica sólo que con total negativo.
				$orden_canc->setTotal($orden_a_cancelar->getTotal()*(-1));

				//El tipo de la orden de cancelacion
				$orden_canc->setTipoId(sfConfig::get('app_orden_cancelacion_id'));

				//Ponemos el estatus de la orden original como cancelada:
				$orden_a_cancelar->setEstatusId(sfConfig::get('app_estatus_cancelado_id'));

				//Se guarda la sincronizacion en SAP de la cancelacion
				$this->guardaSincroniaSap_cancelacion($orden_a_cancelar);

				//Asociamos la orden de cancelacion a la cancelacion
				$form->getObject()->setOrdenRelatedByOrdenCancelacionId($orden_canc);

				//Generamos la devolución de los productos en la orden.
				DetalleOrdenPeer::generaCancelacion($form->getObject());

				$form->getObject()->save();

				$this->dispatcher->notify(new sfEvent($this, 'venta.cancelada', array(
						'orden' => $orden_a_cancelar,
						'operador' => $operador
				)));

				$this->getUser()->setFlash('ok', 'Se ha cancelado la venta correctamente.');
				$this->redirect('cancelacion_venta/new');
			}
		}
		else
		{
			$this->getUser()->setFlash('error', 'Debe ingresar los datos solicitados a fin de cancelar la venta.');
			$this->redirect('cancelacion_venta/new');
		}
	}

	/**
	 * Guarda la cancelacion de venta en la tabla sincronia_sap para la sincronizacion con SAP
	 * @param sfWebRequest $request
	 * @return string json
	 */
	public function guardaSincroniaSap_cancelacion($orden)
	{
			$sincronia_sap = new SincroniaSap();
			$sincronia_sap->setOperacion('Devoluciones');
			$sincronia_sap->setPkReferencia($orden->getId());
			$sincronia_sap->setCompleto(false);
			$sincronia_sap->setCreatedAt(time());
			$sincronia_sap->save();

		return $sincronia_sap;
	}
}
