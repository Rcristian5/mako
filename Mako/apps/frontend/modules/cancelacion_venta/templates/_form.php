<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<div class="ui-widget-header ui-corner-top tsss" style="padding: 5px;">Cancelación
	de venta</div>
<div class="ui-widget ui-corner-all sombra">
	<div class="ui-widget-content ui-corner-bottom" style="padding: 15px;">


		<?php if ($sf_user->hasFlash('ok')): ?>
		<div
			class="flash_ok ui-state-highlight ts sombra-delgada ui-corner-all"
			style="padding: 5px; margin-top: 10px; margin-bottom: 10px;">
			<span style="float: left; margin-right: 0.3em;"
				class="ui-icon ui-icon-info"></span> <strong><?php echo $sf_user->getFlash('ok') ?>
			</strong>
		</div>
		<?php endif; ?>

		<?php if ($sf_user->hasFlash('error')): ?>
		<div
			class="flash_ok ui-state-error tssss sombra-delgada ui-corner-all"
			style="padding: 5px; margin-top: 10px; margin-bottom: 10px;">
			<span style="float: left; margin-right: 0.3em;"
				class="ui-icon ui-icon-info"></span> <strong><?php echo $sf_user->getFlash('error') ?>
			</strong>
		</div>
		<?php endif; ?>

		<form
			action="<?php echo url_for('cancelacion_venta/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?id='.$form->getObject()->getId() : '')) ?>"
			method="post"
			<?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
			<?php if (!$form->getObject()->isNew()): ?>
			<input type="hidden" name="sf_method" value="put" />
			<?php endif; ?>
			<table>
				<tfoot>
					<tr>
						<td colspan="2"><input type="submit" value="Cancelar venta"
							onclick="return requeridos();" />
						</td>
					</tr>
				</tfoot>
				<tbody>
					<?php echo $form ?>
				</tbody>
			</table>
		</form>

	</div>
</div>
