<h1>Cancelacions List</h1>

<table>
	<thead>
		<tr>
			<th>Id</th>
			<th>Centro</th>
			<th>Operador</th>
			<th>Orden</th>
			<th>Orden cancelacion</th>
			<th>Created at</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($Cancelacions as $Cancelacion): ?>
		<tr>
			<td><a
				href="<?php echo url_for('cancelacion_venta/show?id='.$Cancelacion->getId()) ?>"><?php echo $Cancelacion->getId() ?>
			</a></td>
			<td><?php echo $Cancelacion->getCentroId() ?></td>
			<td><?php echo $Cancelacion->getOperadorId() ?></td>
			<td><?php echo $Cancelacion->getOrdenId() ?></td>
			<td><?php echo $Cancelacion->getOrdenCancelacionId() ?></td>
			<td><?php echo $Cancelacion->getCreatedAt() ?></td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<a href="<?php echo url_for('cancelacion_venta/new') ?>">New</a>
