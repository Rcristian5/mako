<?php

class frontendConfiguration extends sfApplicationConfiguration
{
	public function configure()
	{
		//Registramos listeners para nuestros eventos:

		//Ejecutamos guardar fiscales de socio si es que los hay
		$this->dispatcher->connect('orden.venta.realizada', array('SocioPeer', 'setDatosFiscales'));

		//Ejecutamos ticketVenta si fue realizada una orden de venta
		$this->dispatcher->connect('orden.venta.realizada', array('ImpresoraTickets', 'ticketVenta'));

		//Ejecutamos facturaVenta si fue realizada una orden de venta
		$this->dispatcher->connect('orden.venta.realizada', array('ImpresoraTickets', 'facturaVenta'));

		//escuchamos ventas para saber si hay que redimir cupones
		$this->dispatcher->connect('orden.venta.realizada', array('CuponPeer', 'redimirCupon'));

		//escuchamos ventas para saber si hay que generar la cobranza
		$this->dispatcher->connect('orden.venta.realizada', array('SocioPagosPeer', 'setupPagos'));

		//Escuchamos el evento de venta para saber si se agregea tiempo de venta o no.
		$this->dispatcher->connect('orden.venta.realizada', array('OrdenPeer', 'asignaTiempos'));

		//escuchamos ventas para saber si es un pago de curso y hay q dar de alta al alumno en curso
		$this->dispatcher->connect('orden.venta.realizada', array('AlumnoPagosPeer', 'pagosCurso'));

		//Ejecutamos realizar venta cuando se da una reposicion de credencial.
		$this->dispatcher->connect('socio.reposicion_credencial', array('OrdenPeer', 'venderReposicionCredencial'));

		//Ejecutamos ticketSocio cuando se dispara socio.ticket.solicitado
		$this->dispatcher->connect('socio.ticket.solicitado',array('ImpresoraTickets', 'ticketSocio'));

		//Ejecutamos tickets corte cuando se dispara corte.realizado
		$this->dispatcher->connect('corte.realizado',array('ImpresoraTickets', 'ticketCorte'));

		//Ejecutamos factura corte cuando se dispara corte.realizado
		$this->dispatcher->connect('corte.realizado',array('ImpresoraTickets', 'facturaCorte'));

		//Ejecutamos facturaCorteTasa0 por si hay productos tasa0 que deban reportarse como venta al publico
		$this->dispatcher->connect('corte.realizado',array('ImpresoraTickets', 'facturaCorteTasa0'));

		//Ejecutamos agregar a LDAP cuando se haya ingresado un nuevo socio
		$this->dispatcher->connect('socio.registrado',array('OperacionLDAP', 'registrarSocio'));

		//Ejecutamos modificar LDAP cuando se haya modificado un socio
		$this->dispatcher->connect('socio.actualizado',array('OperacionLDAP', 'actualizarSocio'));

		//Ejecutamos agregar a LDAP cuando se haya ingresado un nuevo usuario
		$this->dispatcher->connect('usuario.registrado',array('OperacionLDAP', 'registrarUsuario'));

		//Ejecutamos modificar LDAP cuando se haya modificado un usuario
		$this->dispatcher->connect('usuario.actualizado',array('OperacionLDAP', 'actualizarUsuario'));

		//Generamos un nuevo producto cuando se haya publicado un curso
		$this->dispatcher->connect('curso.publicado',array('ProductoPeer', 'productoCursoBind'));

		//Generamos un nuevo curso en el servidor LDAP para auth con moodle
		$this->dispatcher->connect('curso.publicado',array('OperacionLDAP', 'registrarCurso'));

		//Modificamos el producto cuando se haya modificado el curso
		$this->dispatcher->connect('curso.modificado',array('ProductoPeer', 'productoCursoModificadoBind'));

		//Modificamos el registro en LDAP correspondiente al curso modificado
		$this->dispatcher->connect('curso.modificado',array('OperacionLDAP', 'modificarCurso'));

		//Escuchamos el evento de alumno preinscrito para incluirlo en la tabla Alumno Pagos
		$this->dispatcher->connect('alumno.preinscrito',array('AlumnoPagosPeer', 'preinscripcion'));

		//Escuchamos el evento de alumno inscrito para incluirlo en el grupo LDAP
		$this->dispatcher->connect('alumno.inscrito',array('operacionLDAP', 'inscribirAlumno'));

		//Escuchamos el evento de cancelación de inscripcion
		$this->dispatcher->connect('alumno.preinscripcion.cancelada',array('AlumnoPagosPeer', 'cancelarPagos'));

		//Escuchamos el evento de modificación de grupo_alumnos
		$this->dispatcher->connect('grupo.actualizado',array('AlumnoPagosPeer', 'actualizarGrupo'));

		//Escuchamos evento de venta.cancelada
		$this->dispatcher->connect('venta.cancelada',array('AlumnoPagosPeer', 'cancelarPagoVenta'));
		//Escuchamos evento de venta.cancelada
		$this->dispatcher->connect('venta.cancelada',array('SocioPagosPeer', 'cancelarPagoVenta'));

		//Este evento se dispara en el login de la pc mediante GDM, sesiones/executeLogin
		//$this->dispatcher->connect('usuario.login',array('operacionLDAP', 'usuarioLoginBind'));

		//Este evento se dispara en el logout de la pc mediante GDM, sesiones/executeLogout
		//$this->dispatcher->connect('usuario.logout',array('operacionLDAP', 'usuarioLogoutBind'));

		//Este evento se dispara en el login de la pc mediante GDM, sesiones/executeLogin
		$this->dispatcher->connect('socio.login',array('operacionLDAP', 'socioLoginBind'));

		//Este evento se dispara en el logout de la pc mediante GDM, sesiones/executeLogout
		$this->dispatcher->connect('socio.logout',array('operacionLDAP', 'socioLogoutBind'));


		//Este evento se dispara cuando un socio cambia su clave desde el dash de socios
		$this->dispatcher->connect('socio.cambioclave',array('OperacionLDAP', 'modificarClaveSocioBind'));


		//**************************Estos eventos generan consumo de web services de moodle*************************//

		//Ejecutamos agregar a Moodle cuando se haya ingresado un nuevo socio
		$this->dispatcher->connect('socio.registrado',array('OperacionMoodle', 'registrarSocioBind'));

		//Ejecutamos modificar a Moodle cuando se haya modificado un socio
		$this->dispatcher->connect('socio.actualizado',array('OperacionMoodle', 'actualizarSocioBind'));

		//Ejecutamos modificar a Moodle cuando se haya modificado la clave del socio desde el dash de socios
		$this->dispatcher->connect('socio.cambioclave',array('OperacionMoodle', 'actualizarSocioBind'));

		//Escuchamos el evento de alumno inscrito para incluirlo en el grupo Moodle
		$this->dispatcher->connect('alumno.inscrito',array('OperacionMoodle', 'inscribirAlumnoBind'));





		/**
		 * **************************************************************************************************
		 * Agregamos listeners para el caso de registro de socio e inscripcion para NMP						*
		 * @author silvio.bravo 																			*
		 * **************************************************************************************************
		 */




		/************************EVENTOS PARA GENERAR CONSUMO DE WSDL DE NMP ********************************************/

		//Registramos socio en micrositio NMP
		//$this->dispatcher->connect('socio.registrado'        , array('NMPServicesWSDL', 'agregarSocio'));
		//$this->dispatcher->connect('syncNMP.socio.registrado', array('NMPServicesWSDL', 'agregarSocio'));

		//Actualizamos socio en micrositio NMP
		//$this->dispatcher->connect('socio.actualizado'        , array('NMPServicesWSDL', 'actualizarSocio'));
		//$this->dispatcher->connect('syncNMP.socio.actualizado', array('NMPServicesWSDL', 'actualizarSocio'));

		//Inscribimos socio a curso en Micrositio NMP
		//$this->dispatcher->connect('alumno.inscrito'        , array('NMPServicesWSDL', 'inscribirSocio'));
		//$this->dispatcher->connect('syncNMP.alumno.inscrito', array('NMPServicesWSDL', 'inscribirSocio'));

		//Cancelamos la inscripcion de un socio a un curso
		//$this->dispatcher->connect('alumno.cancelar.inscripcion'        , array('NMPServicesWSDL' => 'cancelarInscripcionSocio'));
		//$this->dispatcher->connect('syncNMP.alumno.cancelar.inscripcion', array('NMPServicesWSDL' => 'cancelarInscripcionSocio'));

		/****************************************************************************************************************/
		/****************************************************************************************************************/




















		//**************************Bindings para el manejo de sincronía central***********************************//

		$this->dispatcher->connect('usuario.actualizado',array('UsuarioPeer', 'syncUsuarioActualizarBind'));
		$this->dispatcher->connect('usuario.registrado',array('UsuarioPeer', 'syncUsuarioRegistrarBind'));

		//Escuchamos los eventos para productos creados y modificados, asi como curso-producto creado y modificado
		$this->dispatcher->connect('producto.creado',array('ProductoPeer', 'syncProductoCreadoBind'));
		$this->dispatcher->connect('producto.modificado',array('ProductoPeer', 'syncProductoModificadoBind'));

		//Escuchamos los eventos para productos creados y modificados que deben realizarse solo en un centro
		$this->dispatcher->connect('producto.creado-centro',array('ProductoPeer', 'syncACentroProductoCreadoBind'));
		$this->dispatcher->connect('producto.modificado-centro',array('ProductoPeer', 'syncACentroProductoModificadoBind'));

		//Escuchamos los eventos para cursos creados y actualizados
		$this->dispatcher->connect('curso.creado',array('CursoPeer', 'syncCursoCreadoBind'));
		$this->dispatcher->connect('curso.actualizado',array('CursoPeer', 'syncCursoActualizadoBind'));
		$this->dispatcher->connect('productocurso.creado',array('ProductoCursoPeer', 'syncProductoCursoCrearBind'));
		//Escuchamos los eventos para la extension de cobro en punto de venta
		$this->dispatcher->connect('plazo.creado',array('PlazoFechaCobroPeer', 'syncPlazoCreadoBind'));
		$this->dispatcher->connect('plazo.modificado',array('PlazoFechaCobroPeer', 'syncPlazoModificadoBind'));
		//Escuchamos los eventos para aulas creadas y actualizadas
		$this->dispatcher->connect('aula.creada',array('AulaPeer', 'syncAulaCreadaBind'));
		$this->dispatcher->connect('aula.modificada',array('AulaPeer', 'syncAulaModificadaBind'));

		//Escuchamos los eventos para dias no laborables creados y modificados
		$this->dispatcher->connect('nolaborable.creado',array('DiasNoLaborablesPeer', 'syncNoLaborableCreadoBind'));
		$this->dispatcher->connect('nolaborable.modificado',array('DiasNoLaborablesPeer', 'syncNoLaborableModificadoBind'));

		//Escuchamos los eventos para grupos creados y modificados
		$this->dispatcher->connect('grupo.creado',array('GrupoPeer', 'syncGrupoCreadoBind'));
		$this->dispatcher->connect('grupo.modificado',array('GrupoPeer', 'syncGrupoModificadoBind'));

		//Escuchamos los eventos para los horarios creados y modificados
		$this->dispatcher->connect('horario.creado',array('HorarioPeer', 'syncHorarioCreadoBind'));
		$this->dispatcher->connect('horario.modificado',array('HorarioPeer', 'syncHorarioModificadoBind'));

		//Escuchamos los eventos para las promociones de cupones creados y modificados
		$this->dispatcher->connect('promocioncupon.creado',array('PromocionCuponPeer', 'syncPromocionCuponCrearBind'));
		$this->dispatcher->connect('promocioncupon.actualizado',array('PromocionCuponPeer', 'syncPromocionCuponActualizarBind'));

		//Escuchamos los eventos para las promociones de descuento creadas y modificadas
		$this->dispatcher->connect('promocion.creado',array('PromocionPeer', 'syncPromocionCrearBind'));
		$this->dispatcher->connect('promocion.actualizado',array('PromocionPeer', 'syncPromocionActualizarBind'));
		$this->dispatcher->connect('promocionregla.creada',array('PromocionReglaPeer', 'syncPromocionReglaCrearBind'));
		$this->dispatcher->connect('promocionregla.borrada',array('PromocionReglaPeer', 'syncPromocionReglaBorrarBind'));

		//Escuchamos los eventos cuando se crea o modifica una categoria de reporte de producto
		$this->dispatcher->connect('categoriareporte.creada',array('CategoriaReportePeer', 'syncBind'));
		$this->dispatcher->connect('categoriareporte.actualizada',array('CategoriaReportePeer', 'syncBind'));

		//Escuchamos la creacion de metas
		$this->dispatcher->connect('meta.creada',array('MetasPeer', 'syncBind'));
		$this->dispatcher->connect('meta.actualizada',array('MetasPeer', 'syncBind'));
		$this->dispatcher->connect('meta.borrada',array('MetasPeer', 'syncBind'));

	}
}
