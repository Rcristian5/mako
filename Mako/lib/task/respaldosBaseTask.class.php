<?php
/**
 * Realiza los respaldos de las bases de datos.
 * @author edgar
 *
 */
class respaldosBaseTask extends sfBaseTask
{
    /**
     * Path donde se encuentran los archivos de respaldo de bases
     * @var string
     */
    static $pbacks = '/backups/bases';

    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'Aplicacion', 'frontend'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'Ambiente',  'tolu2' ),
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'La conexion a la bd', 'propel')
        ));

        $this->namespace           = 'respaldos';
        $this->name                = 'base';
        $this->briefDescription    = 'Extrae los dumps de las bases de datos de cada centro.';
        $this->detailedDescription = <<<EOF
[respaldos:base|INFO] Genera los respaldos de las bases de cada centro, lo copia a central, borra la instancia vieja y realiza la nueva.
  [php symfony respaldos:base --centro=tolu1 |INFO]
  Si se llama sin la opción centro, entonces realiza los respaldos de todos los centros.
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        // Inicializamos la base de datos central
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'] ? $options['connection'] : null)->getConnection();

        //Inicializamos los contextos
        $configuration = ProjectConfiguration::getApplicationConfiguration($options['application'], $options['env'], true);
        $context       = sfContext::createInstance($configuration);
        $dispatcher    = $configuration->getEventDispatcher();
        //Si estamos en central...
        if (sfConfig::get('app_centro_actual_id') == sfConfig::get('app_central_id')) {
            $c                         = new Criteria();
            $c->add(CentroPeer::ACTIVO, true);
            $c->addAscendingOrderByColumn(CentroPeer::ALIAS_REPORTE);
            //Existe la opcion centro?
            if ($options['centro'] != null or trim($options['centro']) != '') {
                $c->add(CentroPeer::ALIAS, $options['centro']);
                $centro = CentroPeer::doSelectOne($c);
                if ($centro != null) {
                    $res    = $this->respaldar($centro);
                    if ($res) $this->restauraInstancia($centro);
                }
            } else {
                if ($this->inicializa()) {
                    foreach (CentroPeer::doSelect($c) as $centro) {
                        $res = $this->respaldar($centro);
                        if ($res) $this->restauraInstancia($centro);
                    }
                }
            }

            $ts = date("Y-m-d H:i:s");
            echo "========== FINALIZANDO PROCESO DE RESPALDO $ts ===========\n";
            //Eliminamos los respaldos viejos
            $this->eliminaRespaldosViejos();
        }
    }
    /**
     * Genera archivos tar comprimidos en bz2 de los archivos en el dir backups
     */
    private function inicializa() {

        $ts = date("Y-m-d H:i:s");
        echo "========== INICIANDO PROCESO DE RESPALDO $ts ===========\n";

        $path = self::$pbacks;
        chdir($path);
        $fecha   = date("Ymd");
        $archivo = "respaldos_" . $fecha . ".tar.bz";

        $cmd     = "tar cvjf $archivo *.sql 2>&1";
        $out     = array();
        exec($cmd, $out, $ret);
        //print_r($out);
        $ts = date("Y-m-d H:i:s");
        echo " $ts >> Finaliza compresion de los archivos sql existentes. \n $archivo \n";
        return ($ret == 0) ? true : false;
    }
    /**
     * Ejecuta la funcion primaria del respaldo.
     */
    private function respaldar(Centro $centro) {
        $ts   = date("Y-m-d H:i:s");
        $path = self::$pbacks;
        chdir($path);
        echo "$ts >> Inicia respaldo de centro: " . $centro->getAlias() . "\n";
        $cmd = "pg_dump -h " . $centro->getIp() . " mako > backup_" . $centro->getAlias() . ".sql -U postgres";
        $out = array();
        exec($cmd, $out, $ret);
        $this->analizaRes($centro, $out, $ret);
        $ts = date("Y-m-d H:i:s");
        echo "$ts >> Finaliza respaldo de centro: " . $centro->getAlias() . "\n";
        return ($ret == 0) ? true : false;
    }
    /**
     * Analiza el resultado del backup y guarda en log.
     *
     * @param Centro $centro
     * @param array $out
     * @param integer $ret
     */
    private function analizaRes(Centro $centro, $out, $ret) {
        $ts        = date("Y-m-d H:i:s");
        $centro_id = $centro->getId();
        $salida    = implode("\n", $out);
        if ($ret > 0) {
            echo $ts . " " . $centro->getAlias() . ": ERROR: No ha sido posible concluir el proceso. " . $salida . "\n";
        } else {
            echo $ts . " " . $centro->getAlias() . ": respaldo OK\n";
        }
    }
    /**
     * Borra la instancia previa y restaura la nueva.
     * @param Centro $centro
     */
    private function restauraInstancia(Centro $centro) {
        $ts = date("Y-m-d H:i:s");
        echo "$ts >> Inicia restore de centro: " . $centro->getAlias() . "\n";

        $path = self::$pbacks;
        chdir($path);
        //Primero hacemos drop
        $cmd = "dropdb backup-" . $centro->getAlias() . " -U postgres";
        $out = array();
        exec($cmd, $out, $ret);
        //Luego hacemos createdb
        $cmd = "createdb backup-" . $centro->getAlias() . " -U postgres";
        $out = array();
        exec($cmd, $out, $ret);
        //Y por ultimo hacemos el restore
        //Luego hacemos createdb
        $cmd = "psql backup-" . $centro->getAlias() . " -U postgres -f backup_" . $centro->getAlias() . ".sql";
        $out = array();
        exec($cmd, $out, $ret);
        $this->analizaRes($centro, $out, $ret);

        $ts = date("Y-m-d H:i:s");
        echo "$ts >> Finaliza restore de centro: " . $centro->getAlias() . "\n";
    }
    /**
     * Elimina respaldos viejos para que se llene la particion del disco.
     *
     */
    private function eliminaRespaldosViejos() {

        if ($handle = opendir(self::$pbacks)) {
            while (false !== ($file   = readdir($handle))) {
                if ($file != "." && $file != ".." && preg_match("/respaldos_/", $file)) {
                    //Obtenemos la fecha de hoy menos 15 dias para borrar todos los archivos mas viejos que 15 dias.
                    $t      = mktime(0, 0, 0, date("m"), date("d") - 15, date("Y"));
                    //echo "Coincide $file\n";
                    //Preguntamos cuanto tiempo tiene el archivo.
                    $st     = stat(self::$pbacks . "/" . $file);
                    //echo $st['ctime']." < $t \n";
                    if ($st['ctime'] < $t) {
                        echo " Se borra $file por tener mas de 15 días de viejo\n";
                        unlink(self::$pbacks . "/" . $file);
                    }
                }
            }
            closedir($handle);
        }
    }
}
