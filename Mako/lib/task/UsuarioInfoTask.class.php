<?php
class UsuarioInfoTask extends sfBaseTask
{
    public $log = array();

    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'                                                                         ,  'tolu2' ),
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección'                                                           , 'propel'),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación'                                                          , 'frontend'),
            new sfCommandOption('usuario'    , null, sfCommandOption::PARAMETER_REQUIRED, 'Nombre de usuario del operador de staff a consultar'                                 , null),
            new sfCommandOption('accion'     , null, sfCommandOption::PARAMETER_REQUIRED, 'Accion a realizar, "consultar", "sincronizar", "crear", "actualizar", "crear-batch" ', 'consultar')
        ));

        $this->namespace           = 'soporte';
        $this->name                = 'staff-info';
        $this->briefDescription    = 'Muestra información de registro del operador de staff, así como sincronía con LDAP.';
        $this->detailedDescription = <<<EOF

  [mako soporte:staff-info|INFO] Muestra información de registro del operador de staff, así como sincronía con LDAP.,

  --accion=sincronizar : Sincroniza la clave en BD con la clave en LDAP.
  --accion=crear:  Si el usuario no existe en LDAP lo creo apartir de los datos de la BD local.
  --accion=actualizar: Si los datos del usuario no están actualizados en LDAP los actualiza.
  --accion=crear-batch: Ejecuta la acción crear para todos los usuarios dados de alta en la base central de staff de Mako.

  [mako soporte:staff-info --usuario="nombre.usuario" --accion="consultar" |INFO]  Muestra la información de registro del operador de staff.
  [mako soporte:staff-info --usuario="nombre.usuario" --accion="sincronizar" |INFO]  Sincroniza la clave existente en Mako y clave LDAP
  [mako soporte:staff-info --usuario="nombre.usuario" --accion="crear" |INFO]  Crea los datos de un usuario existente en base local en LDAP
  [mako soporte:staff-info --usuario="nombre.usuario" --accion="actualizar" |INFO]  Actualiza la información en LDAP con los datos del operador
  [mako soporte:staff-info --accion="crear-batch" |INFO]  Ejecuta la acción crear para todos los usuarios dados de alta en la base central de staff de Mako

EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'] ? $options['connection'] : null)->getConnection();
        $configuration   = ProjectConfiguration::getApplicationConfiguration('frontend', $options['env'], true);
        $context         = sfContext::createInstance($configuration);

        $configuration->loadHelpers('Ldap');

        if (($options['usuario'] == null or $options['usuario'] == '') && ($options['accion'] != 'crear-batch')) {
            $this->log[] = $this->formatter->format("Porfavor teclea:", 'ERROR') .
                           $this->formatter->format(" mako ayuda soporte:staff-info", 'COMMENT');
            $this->log($this->log);

            throw new Exception("El nombre de usuario del operador de staff es un dato requerido!", "0011");
        } elseif ($options['accion'] == 'crear-batch') {
            $c = new Criteria();
            $c->add(UsuarioPeer::ACTIVO, true);
            $c->add(UsuarioPeer::USUARIO, 'super.usuario', Criteria::NOT_EQUAL);
            $c->addAscendingOrderByColumn(UsuarioPeer::ID);

            $as = UsuarioPeer::doSelect($c);
        } else {
            $c  = new Criteria();
            $c->add(UsuarioPeer::USUARIO, $options['usuario']);

            $as = UsuarioPeer::doSelect($c);

            if (count($as) < 1) $this->log[] = $this->formatter->format("\n===No existe el usuario en la base local de este centro.===\n", 'ERROR');
        }

        foreach ($as as $usuario) {
            if ($options['accion'] == 'consultar') {
                $this->info($usuario);
            } elseif ($options['accion'] == 'sincronizar') {
                try {
                    modificarClaveSocioLDAP($usuario->getUsuario(), $usuario->getClave());
                    $this->log[] = $this->formatter->format("****Se ha enviado orden de sincronía de claves****", 'COMMENT');
                }
                catch(Exception $e) {
                    $this->log[] = $this->formatter->format("Error al sincronizar claves: " . $e->getMessage(), 'ERROR');
                }
            } elseif ($options['accion'] == 'crear' || $options['accion'] == 'crear-batch') {
                error_log("Procesando $usuario:", 3, '/tmp/staff-ldap.log');
                if (!verificaUsuarioLDAP($usuario->getUsuario())) {
                    try {
                        $this->crearUsuario($usuario);
                        $this->log[] = $this->formatter->format("Se ha creado: $usuario", 'INFO');
                    }
                    catch(Exception $e) {
                        $this->log[] = $this->formatter->format("Error al crear al usuario: $usuario", 'ERROR');
                    }
                } else {
                    $this->log[] = $this->formatter->format("El usuario $usuario ya existe en LDAP, no es posible crearlo!:", 'ERROR');
                    error_log("El usuario $usuario ya existe en LDAP, no es posible crearlo!:", 3, '/tmp/staff-ldap.log');
                }
            } elseif ($options['accion'] == 'actualizar') {
                if (verificaUsuarioLDAP($usuario->getUsuario())) {
                    try {
                        $this->actualizarUsuario($usuario);
                        $this->log[] = $this->formatter->format("Se ha actualizado: $usuario", 'INFO');
                    }
                    catch(Exception $e) {
                        $this->log[] = $this->formatter->format("Error al actualizar al socio: $usuario", 'ERROR');
                    }
                } else {
                    $this->log[] = $this->formatter->format("El usuario no existe en LDAP no es posible actualizarlo!:", 'ERROR');
                }
            }
        }
        $this->log($this->log);
    }

    function info(Usuario $usuario) {
        $compara = comparaClavesLdap($usuario->getUsuario(), $usuario->getClave());
        if ($compara === 0) {
            $this->log[] = $this->formatter->format("\n===No existe el usuario en el servidor LDAP.===\n", 'ERROR');
            return;
        }

        $this->log[] = $this->formatter->format("\n====Consulta de información de usuario operador de staff====\n", 'COMMENT');
        $this->log[] = $this->formatter->format("Usuario: \t", 'COMMENT') . $usuario->getUsuario();
        $this->log[] = $this->formatter->format("Nombre: \t", 'COMMENT') . $usuario;
        //$this->log[] = $this->formatter->format("Alta en: \t", 'COMMENT').$usuario->getCentro();

        $this->log[] = $this->formatter->format("Fecha Alta: \t", 'COMMENT') . $usuario->getCreatedAt();
        $this->log[] = $this->formatter->format("Estatus: \t", 'COMMENT') . ($usuario->getActivo() ? 'ACTIVO' : 'INACTIVO');
        $this->log[] = $this->formatter->format("UID Mako: \t", 'COMMENT') . $usuario->getId();

        if ($compara == true) {
            $this->log[] = $this->formatter->format("Estatus clave: \t", 'COMMENT') . $this->formatter->format("Claves coincidentes", 'INFO');
        } else {
            $this->log[] = $this->formatter->format("Estatus clave: \t", 'COMMENT') . $this->formatter->format("Claves NO coinciden", 'ERROR');
        }
    }

    function crearUsuario(Usuario $usuario) {
        $username     = $usuario->getUsuario();
        $nombre       = $usuario->getNombre();
        $apepat       = $usuario->getApepat();
        $apemat       = $usuario->getApemat();
        $clave        = $usuario->getClave();
        $email        = $usuario->getEmail();
        $alias_centro = 'mako';

        agregarUsuarioLDAP($username, $nombre, $apepat, $clave, $email, $alias_centro);
    }

    function actualizarUsuario(Usuario $usuario) {
        modificarUsuarioLDAP(
            $usuario->getUsuario(),
            $usuario->getNombre(),
            $usuario->getApepat(),
            $usuario->getApemat(),
            $usuario->getClave(),
            $usuario->getEmail(),
            false,
            null);
    }
}
?>