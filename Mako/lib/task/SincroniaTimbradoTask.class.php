<?php 
    define("PATH_PROYECTO",dirname(__FILE__).'/../..');
    define("PATH_TIMBRADO",PATH_PROYECTO."/web/timbrado");
    define("CLAVE_EDICOM_PUBLISH"       , "publishWS");
    define("CLAVE_EDICOM_SUSCRIPTION"   , "subscriptionWS");
class SincroniaTimbradoTask extends sfBaseTask
{

	public $path_log = '/var/log/mako/sincronia_timbrado.log';
	
  protected function configure()
  {
    $this->addOptions(array(
      new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'Entorno de ambiente', 'dev'),
      new sfCommandOption('accion', null, sfCommandOption::PARAMETER_OPTIONAL, 'Accion que se ejcutara', ''),
    ));
 
    $this->namespace = 'mako';
    $this->name = 'sincronia_timbrado';
    $this->briefDescription = 'Sincronia Timbrado. Envia informacion a Timbrado de facturas.';
 
    $this->detailedDescription = <<<EOF
The [mako:pcs-watcher|INFO] Sincronia Timbrado:
 
  [./symfony mako:sincronia_timbrado --env=prod --accion=|INFO]
EOF;
  }
 
  protected function execute($arguments = array(), $options = array())
  {

    $path_log = '/var/log/mako/sincronia_timbrado.log';
    $databaseManager = new sfDatabaseManager($this->configuration);
    $configuration = ProjectConfiguration::getApplicationConfiguration('frontend', 'tolu2' , true); 
	$context = sfContext::createInstance($configuration);
    $configuration->loadHelpers('Facturas');
    $dispatcher = $configuration->getEventDispatcher();
    $conn = Propel::getConnection();

	$sinc_sap = new Criteria();
	$sinc_sap->add(SincroniaTimbradoPeer::COMPLETO,false);
	$sinc_pendientes = SincroniaTimbradoPeer::doSelect($sinc_sap);

    foreach($sinc_pendientes as $sinc)
    {

      $factura          = $sinc->getFactura();
      $id_sinc_timbrado = $sinc->getId();
      $modulo_timbrado  = $sinc->getOperacion();
      $origen_peticion  = 'timbrado';
      $cadena_cfdi      = $sinc->getCadenaCfdi();

      switch ($modulo_timbrado) {

          case CLAVE_EDICOM_PUBLISH:

            $respuesta = $this->solicitaTimbradoFactura($factura);
            if ($respuesta['status']){

                error_log('Factura registrada correctamente en web service '.$modulo_timbrado,3,$path_log);
                error_log('[SINCRONIA]: '.print_r($respuesta['data'], true),3,$path_log);
                $completo=false;
                $estatus = 'en proceso de obtención de sellos';

                self::guardarSincroniaTimbrado($factura, $respuesta['data'], CLAVE_EDICOM_SUSCRIPTION, $id_sinc_timbrado, $completo, $estatus, $origen_peticion);

            }else{

                error_log('Ocurrio un error al registrar la factura en el web service '.$respuesta['data']. "\n",3,$path_log);
                $completo=false;
                $estatus = 'en proceso de publicación';
                self::guardarSincroniaTimbrado($factura, $respuesta['data'], CLAVE_EDICOM_PUBLISH, $id_sinc_timbrado, $completo, $estatus, $origen_peticion);

            }

            break;
          
          case CLAVE_EDICOM_SUSCRIPTION:

            error_log("\nConsultando \n".$factura,3,$path_log);

            $datetimeactual = new DateTime('NOW');
            $datetimebd = new DateTime($sinc->getUpdatedAt());
            $interval = $datetimeactual->diff($datetimebd);

            error_log("\nMinutos transcurrido desde publicación \n".$interval->i,3,$path_log);

            //if ($interval->i >= 5){

                if ($cadena_cfdi == null ){

                    $resultado = self::obtenerFacturaTimbradaProveedor($modulo_sap, $factura, $id_sinc_timbrado, $origen_peticion );

                }else{

                    $resultado = self::obtenerFacturaTimbradaBd($modulo_sap, $id_sinc_timbrado, $origen_peticion );

                }
      
            //}

            break;

      }

    }

  }


  public function solicitaTimbradoFactura($nombreArchivoFactura){

    $respuesta      = array("status"=>false, "data"=>null);

    $respuestaWsdl  = $this->enviar_wsdl_timbrado($nombreArchivoFactura,CLAVE_EDICOM_PUBLISH);
    if (empty($respuestaWsdl)) {

        $respuesta['status']  = false;
        $respuesta['data']    = $respuestaWsdl;


    }else{

        $respuesta['status']  = true;
        $respuesta['data']    = $respuestaWsdl;

    }
    return $respuesta;
  }

  public function obtieneFacturaTimbrada($nombreArchivoFactura){

    $respuesta    = array('status'=>false, 'data'=>null);
    $datos_orden  = array('timbre_fiscal'=>null, 'comprobante'=>null );

    $respuestaWsdl      = self::enviar_wsdl_timbrado($nombreArchivoFactura,CLAVE_EDICOM_SUSCRIPTION);

    if (empty($respuestaWsdl)) {

        $respuesta['status']  = false;
        $respuesta['data']    = $datos_orden;


    }else{

        $simple = $respuestaWsdl;
        $p = xml_parser_create();
        xml_parse_into_struct($p, $simple, $vals, $index);
        xml_parser_free($p);
    /*    print_r($vals);
        print_r('-------------------------------------'.$nombreArchivoFactura);
        print_r($index);
        exit();*/

        $datos_orden['timbre_fiscal'] = $vals[$index['TFD:TIMBREFISCALDIGITAL'][0]]['attributes'];
        $datos_orden['comprobante'] = $vals[$index['CFDI:COMPROBANTE'][0]]['attributes'];

        $respuesta['status']  = true;
        $respuesta['data']    = $datos_orden;

        self::guardarFisicoFacturaTimbrada($nombreArchivoFactura, $respuestaWsdl);


    }

    return $respuesta;

  }

      public function guardarFisicoFacturaTimbrada($nombreArchivoFactura, $respuestaWsdl){
     
        $dir_timbrado=PATH_TIMBRADO;
        
        $nombreArchivoFacturaFisico = substr($nombreArchivoFactura, 0, -4);

        $nombre_archivo_timbrado = $nombreArchivoFacturaFisico.'_respuesta'.'.xml';

        $direccion_archivo_timbrado = $dir_timbrado."/".$nombre_archivo_timbrado;

        $fp = fopen($direccion_archivo_timbrado,"wb");
        fwrite($fp,$respuestaWsdl);
        fclose($fp);

        return true;

    }

   public function obtenerFacturaTimbradaBd($modulo_sap, $id_sinc_timbrado, $origen_peticion ){

    $path_log = '/var/log/mako/sincronia_timbrado.log';

    $respuesta = self::obtieneDatosFacturaBd($id_sinc_timbrado);

        if ($respuesta['status'] == true){

            $factura = $respuesta['data']['factura'];

            $facturacion_sap = FacturaPeer::sincronia_sap_factura($respuesta['data'], $id_sinc_timbrado, $origen_peticion);

             if ($facturacion_sap == true){
                $completo = true;
                $estatus  = 'factura registrada en Sap';
             }else{
                $completo = false;
                $estatus  = 'factura sin registrar en Sap';
             }

            self::guardarSincroniaTimbrado($factura, $respuesta['data'], CLAVE_EDICOM_SUSCRIPTION, $id_sinc_timbrado, $completo, $estatus, $origen_peticion);

        }else{

            error_log('Ocurrio un error al obtener datos de factura en base datos '.$repuesta['data']. "\n",3,$path_log);
            $completo = false;
            self::guardarSincroniaTimbrado($factura, $respuesta['data'], CLAVE_EDICOM_SUSCRIPTION, $id_sinc_timbrado, $completo, 'en proceso de ser enviado a Sap', 'sap');

        }
    }

    private function obtenerFacturaTimbradaProveedor($modulo_sap, $factura, $id_sinc_timbrado, $origen_peticion){

            $respuesta = $this->obtieneFacturaTimbrada($factura);

                error_log("\nStatus \n".$respuesta['status'],3,$path_log);


                if ($respuesta['status'] === true){

                    error_log('Descarga realizada adecuadamente '.$modulo_sap,3,$path_log);
                    error_log('[SINCRONIA]: '.print_r($respuesta['data'], true),3,$path_log);

                    $guardar_datos = self::guardarSincroniaTimbradoDatosFactura($id_sinc_timbrado, $respuesta['data']);

                    if ($guardar_datos){
                        
                        $facturacion_sap = FacturaPeer::sincronia_sap_factura($respuesta['data'], $id_sinc_timbrado, $origen_peticion);

                        if ($facturacion_sap){
                            $completo =true;
                            $estatus  = 'factura registrada en Sap';
                        }else{
                            $completo=false;
                            $estatus  = 'factura sin registrar en Sap';
                        }

                        self::guardarSincroniaTimbrado($factura, $respuesta['data'], CLAVE_EDICOM_SUSCRIPTION, $id_sinc_timbrado, $completo, $estatus, $origen_peticion);

                    }else{

                        self::guardarSincroniaTimbrado($factura, 'NO SE LOGRO GUARDAR DATOS DE FACTURA EN BASE DE DATOS', CLAVE_EDICOM_SUSCRIPTION, $id_sinc_timbrado, $completo, 'NO SE LOGRO GUARDAR DATOS DE FACTURA EN BASE DE DATOS', $origen_peticion);

                    }

                }else{

                    error_log('Ocurrio un error al obtener archivo en el service '.$repuesta['data']. "\n",3,$path_log);
                    $completo=false;
                    self::guardarSincroniaTimbrado($factura, $respuesta['data'], CLAVE_EDICOM_SUSCRIPTION, $id_sinc_timbrado, $completo, $estatus, $origen_peticion);

                }

        return $completo;
    }

	/**
    * Envia informacion al WSDL de Timbrado
    * @return $params parametros
    */
   private function enviar_wsdl_timbrado($factura, $modulo_timbrado) {

        $path_log = '/var/log/mako/sincronia_timbrado.log';
        
         try{

            $url    = sfConfig::get('app_timbrado_wsdl');
            $cliente = new SoapClient($url, array('trace' => 1));

            switch ($modulo_timbrado){

            case 'publishWS':
            
            $dir_timbrado=PATH_TIMBRADO;
            $nombre_archivo_timbrado = $dir_timbrado."/".$factura;
            $ffis = FolioFiscalPeer::fiscalesCentroActual();
            $serie = $ffis->getSerie();
            $folio = trim(str_replace(".txt", "", str_replace($serie, "", str_replace( sfConfig::get('app_fiscales_rfc'), "", $factura))));

                if ($folio) {

                    $reg_arch = self::regenerar_archivo_timbrado($folio);

                    if ($reg_arch) {

                        $contenido_archivo_timbrado = file_get_contents($nombre_archivo_timbrado);

                        $archivo = $contenido_archivo_timbrado;

                        $params = self::contruir_parametros_publish($archivo,$factura);

                    }else{

                        error_log("\nNo se logro regenera el archivo de manera correcta, se finaliza el proceso \n",3,$path_log);
                        exit();

                    }

                }else{

                    error_log("\nNo se logro obtener folio de manera correcta para regenerar archivo, se finaliza el proceso \n",3,$path_log);
                    exit();

                }
            
            break;

            case 'subscriptionWS':

                $params = self::contruir_parametros_subscription($factura);
            
            break;
        
            }


        $respuesta = $cliente->__soapCall($modulo_timbrado, array($params), NULL);

        }catch(Exception $e){

          error_log("\nException => \n".$e->getMessage(),3,$path_log);

        }catch(SoapFault $sp){

          error_log("\nSoapFault => \n".$sp->getMessage(),3,$path_log);

        }

        $response = $cliente->__getLastResponse();
        //error_log("\nREQUEST => \n".$cliente->__getLastRequest(),3,$path_log);
        //error_log("\nRESPONSE => \n".$response,3,$path_log);


        switch ($modulo_timbrado) {
          case CLAVE_EDICOM_PUBLISH:
            $data = self::get_string_between($response, '<return>', '</return>');
            break;
          
          case CLAVE_EDICOM_SUSCRIPTION:
            $data = self::get_string_between($response, '<?xml version="1.0" encoding="UTF-8"?>', '</cfdi:Comprobante>');
            break;
        }

    return $data;

    }

    private function regenerar_archivo_timbrado($folio) {

       $path_log = '/var/log/mako/sincronia_timbrado.log';

       if ($folio) {

         $criteria_fact = new Criteria();
         $criteria_fact->add(FacturaPeer::FOLIO, $folio);
         $factura = FacturaPeer::doSelectOne($criteria_fact);

            if ($factura->getId()){

                $criteria = new Criteria();
                $criteria->add(TrCorteFacturaPeer::FACTURA_ID, $factura->getId());
                $trcorte = TrCorteFacturaPeer::doSelectOne($criteria);
                

                if ($trcorte){

                    $corte = CortePeer::retrieveByPk($trcorte->getCorteId());

                    list($confFactura, $conceptos, $ffis)  = self::facturaCorte($corte, $folio, $factura);
                    FacturaPeer::creacionFacturaTimbradoResellar($confFactura, $conceptos);
                    $respuesta_regenera = true;

                }else{

                    $criteria_orden = new Criteria();
                    $criteria_orden->add(TrFacturaOrdenPeer::FACTURA_ID, $factura->getId());
                    $trorden = TrFacturaOrdenPeer::doSelectOne($criteria_orden);

                    if ($trorden){

                        $orden = OrdenPeer::retrieveByPk($trorden->getOrdenId());

                        list($confFactura, $conceptos, $cadenaconceptos, $totalPalabras, $ffis, $caf_prods)  = self::facturaVenta($orden);
                        FacturaPeer::creacionFacturaTimbradoResellar($confFactura, $conceptos);
                        $respuesta_regenera = true;


                    }else{

                        error_log("\nLA FACTURA NO ES CORTE NI ORDEN\n".$folio,3,$path_log);
                        print("\nLA FACTURA NO ES CORTE NI ORDEN\n".$folio);
                        $respuesta_regenera = false;

                    }

                }
            }else{

                error_log("\nNO EXISTE FACTURA CON FOLIO\n".$folio,3,$path_log);
                print("\nNO EXISTE FACTURA CON FOLIO\n".$folio);
                $respuesta_regenera = false;

            }

           
            }else{

                error_log("\nNO SE RECIBIO NINGUN FOLIO PARA LA REGENERACION DEL ARCHIVO\n".$folio,3,$path_log);
                print("\nNO SE RECIBIO NINGUN FOLIO PARA LA REGENERACION DEL ARCHIVO\n".$folio);
                $respuesta_regenera = false;

            }

        return $respuesta_regenera;

    }

    private function contruir_parametros_publish($archivo,$factura) {

        $otro = new StdClass();
        $base = new StdClass();
        $base->clientId      = sfConfig::get('app_timbrado_clientid');
        $base->user          = sfConfig::get('app_timbrado_usuario');
        $base->password      = sfConfig::get('app_timbrado_contrasena');
        $base->domain        = sfConfig::get('app_timbrado_domain');
        $base->application   = sfConfig::get('app_timbrado_application');
        $base->schema        = sfConfig::get('app_timbrado_schema');
        $base->destination   = sfConfig::get('app_timbrado_destination');
        $base->reference     = 1;
        $otro->message       = $archivo;
        $otro->name          = $factura;
        $base->messages      = $otro;
        $base->duplicates    = 0;

        $params = $base;

        return $params;

    }


    private function contruir_parametros_subscription($factura) {

        //        $referencia = substr ( $factura,12,10);

        $referencia = trim(str_replace(".txt", "", str_replace( sfConfig::get('app_fiscales_rfc'), "", $factura)));

        if ($referencia != '') {

            $base = new StdClass();
            $base->clientId         = sfConfig::get('app_timbrado_clientid');
            $base->user             = sfConfig::get('app_timbrado_usuario');
            $base->password         = sfConfig::get('app_timbrado_contrasena');
            $base->domain           = sfConfig::get('app_timbrado_domain');
            $base->application      = sfConfig::get('app_timbrado_application');
            $base->schema           = sfConfig::get('app_timbrado_schema_subscriptionws');
            $base->transformation   = '';
            $base->reference        = $referencia;

            $params = $base;

            return $params;

        }else{

            error_log("\nNo se logro obtener referencia correcta para pedir documento timbrado a proveedor, se finaliza el proceso \n",3,$path_log);
            exit();

        }

    }

    /**
    * Guarda datos de sincronizacion con Timbrado
    * @return 
    */
   public static function guardarSincroniaTimbrado($factura, $result, $operacion, $id_sinc_timbrado, $completo, $estatus, $origen_peticion) {

     $path_log = '/var/log/mako/sincronia_timbrado.log';
        $st = new Criteria();
		$st->add(SincroniaTimbradoPeer::ID,$id_sinc_timbrado);
		$sincronia_timbrado = SincroniaTimbradoPeer::doSelectOne($st);

		if ($sincronia_timbrado){

            if($origen_peticion == 'timbrado'){

    			$sincronia_timbrado->setRespuesta($result);
    			$sincronia_timbrado->setCompleto($completo);
                $sincronia_timbrado->setEstatus($estatus);
                $sincronia_timbrado->setOperacion($operacion);
                $sincronia_timbrado->setFactura($factura);
    			$intentos = $sincronia_timbrado->getIntentos() + 1;
    			$sincronia_timbrado->setIntentos($intentos);
    			$sincronia_timbrado->setFechaSincronia(time());
    			$sincronia_timbrado->save();
                
            }else{

                if($origen_peticion == 'sap'){

                    $sincronia_timbrado->setEstatus($estatus);
                    $sincronia_timbrado->setCompleto($completo);
                    $sincronia_timbrado->setFechaSincronia(time());
                    $sincronia_timbrado->save();

                }


            }
		}

	return $sincronia_timbrado;

   }

    /**
    * Guarda datos de factura sincronizacion con Timbrado
    * @return 
    */
   public static function guardarSincroniaTimbradoDatosFactura($id_sinc_timbrado, $datos_orden) {
   
     $path_log = '/var/log/mako/sincronia_timbrado.log';
        $st = new Criteria();
        $st->add(SincroniaTimbradoPeer::ID,$id_sinc_timbrado);
        $sincronia_timbrado = SincroniaTimbradoPeer::doSelectOne($st);

        if ($sincronia_timbrado){

            $sincronia_timbrado->setFolio($datos_orden['comprobante']['FOLIO']);
            $sincronia_timbrado->setCadenaCfdi($datos_orden['comprobante']['NOCERTIFICADO']);
            $sincronia_timbrado->setFechaCfdi($datos_orden['timbre_fiscal']['FECHATIMBRADO']);
            $sincronia_timbrado->setSelloCfdi($datos_orden['timbre_fiscal']['SELLOCFD']);
            $sincronia_timbrado->setSelloSat($datos_orden['timbre_fiscal']['SELLOSAT']);
            $sincronia_timbrado->setCertificadoSat($datos_orden['timbre_fiscal']['NOCERTIFICADOSAT']);
            $sincronia_timbrado->setUuid($datos_orden['timbre_fiscal']['UUID']);
            $sincronia_timbrado->save();
            $guardar = true;

        }else{

            error_log("\nNO EXISTE ID DE SINCRONIA PARA FACTURA \n".$id_sinc_timbrado,3,$path_log);
            $guardar = false;

        }
        

    return $guardar;

   }

    /**
    * Obtiene datos de factura sincronizacion con Timbrado
    * @return 
    */
   private function obtieneDatosFacturaBd($id_sinc_timbrado) {
   
        $path_log = '/var/log/mako/sincronia_timbrado.log';

        $respuesta    = array('status'=>false, 'data'=>null);
        $datos_orden  = array('timbre_fiscal'=>null, 'comprobante'=>null, 'factura' =>null );

        error_log("\nSe realizo consulta en base datos id => \n".$id_sinc_timbrado,3,$path_log);

        $st = new Criteria();
        $st->add(SincroniaTimbradoPeer::ID,$id_sinc_timbrado);
        $sincronia_timbrado = SincroniaTimbradoPeer::doSelectOne($st);

        if ($sincronia_timbrado){

            $datos_orden['comprobante']['NOCERTIFICADO']      = $sincronia_timbrado->getCadenaCfdi();
            $datos_orden['comprobante']['FOLIO']              = $sincronia_timbrado->getFolio();
            $datos_orden['timbre_fiscal']['FECHATIMBRADO']    = $sincronia_timbrado->getFechaCfdi();
            $datos_orden['timbre_fiscal']['SELLOCFD']         = $sincronia_timbrado->getSelloCfdi();
            $datos_orden['timbre_fiscal']['SELLOSAT']         = $sincronia_timbrado->getSelloSat();
            $datos_orden['timbre_fiscal']['NOCERTIFICADOSAT'] = $sincronia_timbrado->getCertificadoSat();
            $datos_orden['timbre_fiscal']['UUID']             = $sincronia_timbrado->getUuid();
            $datos_orden['factura']                           = $sincronia_timbrado->getFactura();


            $respuesta['status']  = true;
            $respuesta['data']    = $datos_orden;

        }else{

            error_log("\nNO EXISTE ID DE SINCRONIA EN BD PARA FACTURA \n".$id_sinc_timbrado,3,$path_log);

        }


    return $respuesta;

   }


   public static function get_string_between($string, $start, $end){
    $string = ' ' . $string;
    $ini = strpos($string, $start);
    if ($ini == 0) return '';
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
    }


    static public function facturaCorte(Corte $corte, $folio, Factura $factura) {
        //Datos de folios fiscales para el centro
        $ffis = FolioFiscalPeer::fiscalesCentroActual();

        $centro = CentroPeer::retrieveByPK($corte->getCentroId());

        $retencion = $factura->getRetencion();

        if ($retencion == 0){

            $tasa = 0;
            $total = $factura->getImporte();
            
        }else{

            $tasa = $corte->getCentro()->getIva();
            $total = $corte->getTotalVpg();
            
        }

        //Total a facturar
        //$total = $corte->getTotalVpg();

        //Desglose de iva del total de la venta al publico en general
        list($iva, $importe, $subtotal, $en_palabras) = desglosa_iva($total, $tasa);

        //Procesamos la fecha del corte
        $fecha_factura = str_replace(" ", "T", $corte->getCreatedAt());

        //Generamos el formato para el domicilio del emisor.
        $domEmisor = array();

        if (sfConfig::get('app_fiscales_calle')        != '') $domEmisor['calle']        = sfConfig::get('app_fiscales_calle');
        if (sfConfig::get('app_fiscales_noExterior')   != '') $domEmisor['noExterior']   = sfConfig::get('app_fiscales_noExterior');
        if (sfConfig::get('app_fiscales_noInterior')   != '') $domEmisor['noInterior']   = sfConfig::get('app_fiscales_noInterior');
        if (sfConfig::get('app_fiscales_colonia')      != '') $domEmisor['colonia']      = sfConfig::get('app_fiscales_colonia');
        if (sfConfig::get('app_fiscales_municipio')    != '') $domEmisor['municipio']    = sfConfig::get('app_fiscales_municipio');
        if (sfConfig::get('app_fiscales_estado')       != '') $domEmisor['estado']       = sfConfig::get('app_fiscales_estado');
        if (sfConfig::get('app_fiscales_pais')         != '') $domEmisor['pais']         = sfConfig::get('app_fiscales_pais');
        if (sfConfig::get('app_fiscales_codigoPostal') != '') $domEmisor['codigoPostal'] = sfConfig::get('app_fiscales_codigoPostal');

        $descuento = '0.00';

        $confFactura = array(
            //Agregue xmlns, xmlns:xsi, xsi:schemaLocation y version
            'xmlns'              => "http://www.sat.gob.mx/cfd/2",
            'xmlns:xsi'          => "http://www.w3.org/2001/XMLSchema-instance",
            'xsi:schemaLocation' => "http://www.sat.gob.mx/cfd/2 cfdv2.xsd",
            'version'            => "2.0",
            'serie'              => $ffis->getSerie(),
            'folio'              => $folio,
            'fecha'              => $fecha_factura,
            'sello'              => "",
            'noAprobacion'       => $ffis->getNoAprobacion(),
            'anoAprobacion'      => $ffis->getAnoAprobacion(),
            'tipoDeComprobante'  => "ingreso",
            'formaDePago'        => "EL PAGO DE ESTA FACTURA (CONTRAPRESTACION) SE EFECTUARA EN UNA SOLA EXHIBICION, SI POR ALGUNA RAZON NO FUERA ASI, EMITIREMOS LOS COMPROBANTES DE LAS PARCIALIDADES RESPECTIVAS",
            'subTotal'           => number_format(trim($importe), 2, '.', ''),
            'total'              => number_format(trim($total), 2, '.', ''),
            'descuento'          => number_format($descuento, 2, '.', ''),
            'noCertificado'      => $ffis->getNumCertificado(),
            'metodoDePago'       => 'EFECTIVO',
            'LugarExpedicion'    => $centro->getMunicipio().', '.$centro->getEstado(), 

            'certificado'        => '',
            'Emisor'             => array(
                'rfc'             => $corte->getCentro()->getRfc(),
                'nombre'          => $corte->getCentro()->getRazonSocial(),
                'DomicilioFiscal' => $domEmisor,
                'RegimenFiscal' => array(
                'Regimen' => 'PERSONA MORAL CON FINES NO LUCRATIVOS'
                )
                ),
            'Receptor'           => array(
                'rfc'       => "XAXX010101000",
                'nombre'    => "COMPROBANTE GLOBAL DE OPERACIONES CON PUBLICO EN GENERAL",
                'Domicilio' => array(
                    //'calle'        => "",
                    //'noExterior'   => "",
                    //'noInterior'   => "",
                    //'colonia'      => "",
                    //'municipio'    => "",
                    //'estado'       => "",
                    'pais'         => "MEXICO",
                    //'codigoPostal' => ""
                )
            ),
            //Agregue en arreglo para los impuestos
            'Impuestos'          => array(
                'Traslados' => array(
                    'Traslado' => array(
                        'impuesto' => "IVA",
                        'tasa'     => number_format($tasa, 2, '.', ''),
                        'importe'  => number_format(trim($iva), 2, '.', '')
                    )
                )
            )
        );

        //El arreglo de conceptos
        $conceptos   = array();

        if ($retencion == 0){

            $ft0 = CortePeer::foliosTasa0($corte);

            $descripcion= "VENTA AL PUBLICO EN GENERAL, NOTAS DE VENTA FOLIOS:". implode(', ',$ft0['folios']);
        
            $conceptos[] = array(
                'cantidad' => "1.00",
                'unidad'   => "NO APLICA",
                'descripcion' => $descripcion,
                'valorUnitario' => number_format(trim($importe),2,'.',''),
                'importe' => number_format(trim($importe),2,'.','')
            );

        }else{

            $descripcion = "VENTA AL PUBLICO EN GENERAL, NOTAS DE VENTA FOLIOS:" .
                           $corte->getFolioVentaInicial() .
                           " A " .
                           $corte->getFolioVentaFinal();

            //Si hay folios tasa cero se indican:
            $ft0 = CortePeer::foliosTasa0($corte);
            if (count($ft0['folios'])) {
                $descripcion .= ", EXCEPTO NOTAS DE VENTA CON FOLIOS: " .
                                implode(', ', $ft0['folios']) .
                                " DEBIDAS A VENTAS CON IVA TASA 0";
            }

            $conceptos[] = array(
                'cantidad'      => "1.00",
                'unidad'        => "NO APLICA",
                'descripcion'   => $descripcion,
                'valorUnitario' => number_format(trim($importe), 2, '.', ''),
                'importe'       => number_format(trim($importe), 2, '.', '')
            );

        }

        return array($confFactura, $conceptos, $ffis);
    }


    static public function facturaVenta(Orden $orden, $tasa0=false){
        
        // Datos del centro
        $centro = CentroPeer::retrieveByPK($orden->getCentroId());
        
        //Datos de folios fiscales para el centro
        if($orden->getFactura())
        {
            $serie = $orden->getFactura()->getFolioFiscal()->getSerie();
            $noAprovacion =  $orden->getFactura()->getFolioFiscal()->getNoAprobacion();
            $anioAprovacion = $orden->getFactura()->getFolioFiscal()->getAnoAprobacion();
            $folio = $orden->getFactura()->getFolio();
            $certificado = $orden->getFactura()->getFolioFiscal()->getNumCertificado();
            $ffis = $orden->getFactura()->getFolioFiscal();
        }
        else 
        {
            $ffis = FolioFiscalPeer::fiscalesCentroActual();
            $serie = $ffis->getSerie();
            $folio = $ffis->getFolioActual();
            $anioAprovacion = $ffis->getAnoAprobacion();
            $noAprovacion = $ffis->getNoAprobacion();
            $certificado = $ffis->getNumCertificado();
        }       
        
        //Domicilio receptor
        $domReceptor = array();
        if($orden->getCalleFiscal() != '') $domReceptor['calle'] = $orden->getCalleFiscal();
        if($orden->getNumExtFiscal() !='') $domReceptor['noExterior'] = $orden->getNumExtFiscal();
        if($orden->getNumIntFiscal() !='') $domReceptor['noInterior'] = $orden->getNumIntFiscal();
        if($orden->getColoniaFiscal() !='') $domReceptor['colonia'] = $orden->getColoniaFiscal();
        if($orden->getMunicipioFiscal() !='') $domReceptor['municipio'] = $orden->getMunicipioFiscal();
        if($orden->getEstadoFiscal() !='') $domReceptor['estado'] = $orden->getEstadoFiscal();
        $domReceptor['pais'] = "MEXICO";
        if($orden->getCpFiscal() !='') $domReceptor['codigoPostal'] = $orden->getCpFiscal();
        
        
        list($cadconceptos,$conceptos,$caf_prods) = cadenaProductosFactura($orden, $tasa0);
                
        //Total a facturar
        $total = $orden->getTotal();
        
        //Tasa iva
        $tasa = intval($orden->getCentro()->getIva());
        if($tasa0) $tasa = 0;
        
        //Desglose de iva del total de la venta al publico en general
        list($iva,$importe,$subtotal,$en_palabras) = desglosa_iva($total,$tasa);

        //Procesamos la fecha de la venta
        $fecha_factura = str_replace(" ","T",$orden->getCreatedAt());
        
        //Generamos el formato para el domicilio del emisor.
         $domEmisor = array();
                 if(sfConfig::get('app_fiscales_calle') != '') $domEmisor['calle'] = sfConfig::get('app_fiscales_calle');
                 if(sfConfig::get('app_fiscales_noExterior') != '') $domEmisor['noExterior'] = sfConfig::get('app_fiscales_noExterior');
                 if(sfConfig::get('app_fiscales_noInterior') !='') $domEmisor['noInterior'] = sfConfig::get('app_fiscales_noInterior');
                 if(sfConfig::get('app_fiscales_colonia') !='') $domEmisor['colonia'] = sfConfig::get('app_fiscales_colonia');
                 if(sfConfig::get('app_fiscales_municipio') !='') $domEmisor['municipio'] = sfConfig::get('app_fiscales_municipio');
                 if(sfConfig::get('app_fiscales_estado') !='') $domEmisor['estado'] = sfConfig::get('app_fiscales_estado');
                 if(sfConfig::get('app_fiscales_pais') !='') $domEmisor['pais'] = sfConfig::get('app_fiscales_pais');
                 if(sfConfig::get('app_fiscales_codigoPostal') !='') $domEmisor['codigoPostal'] = sfConfig::get('app_fiscales_codigoPostal');

        // Domicilio del lugar en donde se expide
        $expedidoEn = array();
                 if($centro->getCalle() != '') $expedidoEn['calle'] = $centro->getCalle();
                 if($centro->getNumExt() !='') $expedidoEn['noExterior'] = $centro->getNumExt();
                 if($centro->getNumInt() !='') $expedidoEn['noInterior'] = $centro->getNumInt();
                 if($centro->getColonia() !='') $expedidoEn['colonia'] = $centro->getColonia();
                 if($centro->getMunicipio() !='') $expedidoEn['municipio'] = $centro->getMunicipio();
                 if($centro->getEstado() !='') $expedidoEn['estado'] = $centro->getEstado();
                 $expedidoEn['pais'] = 'MEXICO';
                 if($centro->getCp() !='') $expedidoEn['codigoPostal'] = $centro->getCp();
                
                //Obtenemos el subtotal y descuento de la factura en base al detalle de la orden
                $subtotal = 0;
                $descuento = 0;
                foreach ($conceptos as $productosOrden) 
                {
                    $subtotal += $productosOrden['importe'];
                }
        
        $confFactura = array(
            //Agregue xmlns, xmlns:xsi, xsi:schemaLocation y version
            'xmlns' => "http://www.sat.gob.mx/cfd/2",
            'xmlns:xsi' =>"http://www.w3.org/2001/XMLSchema-instance",
            'xsi:schemaLocation' => "http://www.sat.gob.mx/cfd/2 http://www.sat.gob.mx/sitio_internet/cfd/2/cfdv22.xsd",
            'version'=>"2.2",
            'serie' => $serie,
            'folio' => $folio, 
            'fecha' => $fecha_factura, 
            'sello' => "",
            'noAprobacion' => $noAprovacion,
            'anoAprobacion' => $anioAprovacion, 
            'tipoDeComprobante' => "ingreso",
            'formaDePago' => "EL PAGO DE ESTA FACTURA (CONTRAPRESTACION) SE EFECTUARA EN UNA SOLA EXHIBICION, SI POR ALGUNA RAZON NO FUERA ASI, EMITIREMOS LOS COMPROBANTES DE LAS PARCIALIDADES RESPECTIVAS",
            'subTotal' => number_format($subtotal,6,'.',''),
            'total' =>  number_format(trim($total),6,'.',''),
            'descuento'=> '0.00',
            'noCertificado' => $certificado,    
            'certificado' => '',
            'metodoDePago' => 'EFECTIVO',
            'LugarExpedicion' => $centro->getMunicipio().', '.$centro->getEstado(), 
            'Emisor' => array(
                'rfc' => sfConfig::get('app_fiscales_rfc'),
                'nombre'=> sfConfig::get('app_fiscales_nombre'),
                'DomicilioFiscal' => $domEmisor ,
                'ExpedidoEn' => $expedidoEn,
                'RegimenFiscal' => array(
                'Regimen' => 'PERSONA MORAL CON FINES NO LUCRATIVOS'
                )
            ),
            'Receptor' => array(
                'rfc' => $orden->getRfc(),
                'nombre'=> $orden->getRazonSocial(),
                'Domicilio' => $domReceptor
            ),  
            //Agregue en arreglo para los impuestos
            'Impuestos' => array(
                'Traslados' => array(
                    'Traslado' => array(
                        'impuesto' => "IVA",
                        'tasa' => number_format($tasa,2,'.','') ,
                        'importe' => number_format( ($subtotal-$descuento) * ($tasa/100),6,'.','')
                    )
                )
            )
        );
        
        return array($confFactura,$conceptos,$cadconceptos,$en_palabras,$ffis,$caf_prods);
    }

}
?>