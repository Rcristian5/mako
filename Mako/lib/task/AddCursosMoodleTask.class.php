<?php
class AddCursosMoodleTask extends sfPropelBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección' , 'propel'),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación', 'frontend'),
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'               ,  'tolu2' )
        ));

        $this->namespace           = 'mako';
        $this->name                = 'add-cursos-moodle';
        $this->briefDescription    = 'Crea cursos a moodle';
        $this->detailedDescription = <<<EOF
 [mako:add-cursos-moodlle|INFO] Crea los cursos a moodle que no fue posible crear
     durante el proceso de creación

  [./symfony mako:ejecuta-sync --env=prod |INFO]
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'])->getConnection();
        $c               = new Criteria();

        $c->add(CursoPeer::ASOCIADO_MOODLE, true);
        $c->add(CursoPeer::REGISTRADO_MOODLE, false);

        $this->logSection('Creando lista de cursos asociados y que no fue posible crear en moodle', '');

        foreach (CursoPeer::doSelect($c) as $curso) {
            $this->logSection('Creando ' . $curso->getNombre() . ' en Moodle:', CursoPeer::publicarMoodle($curso->getId()));
        }

        $this->logSection('Se termino de cargar la lista de cursos asociados', '');
    }
}
?>
