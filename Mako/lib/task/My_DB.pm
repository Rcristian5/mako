package My_DB;

use strict;
use warnings;

use POSIX qw(locale_h);
setlocale (LC_ALL, 'es_MX');

use base 'Exporter';

our $VERSION = 1.0;
our @EXPORT  = qw(
  mi_dbh query query_sin_respuesta trans_sin_respuesta
  );


#  Funciones de Bases de Datos.
use DBI;

sub mi_dbh {
  my $config = shift;
  my $dbh;

  eval {
    local $SIG{ALRM} =
      sub { die 'El tiempo de la conexión a la base de datos se agotó...' };
    alarm 60;
          
    my $dsn;
    if ( $config->{servicio} =~ m/^PostgreSQL$/ ) {
      $dsn = "dbi:Pg:dbname=$config->{DB};host=$config->{host};port=$config->{port}";
    } elsif ( $config->{servicio} =~ m/^MySQL$/ ) {
      $dsn = "DBI:mysql:database=$config->{DB};host=$config->{host};"
              . "port=$config->{port}";
    }

    $dbh = DBI->connect($dsn, $config->{user}, $config->{passwd},
             { AutoCommit => $config->{autocommit} });
    alarm 0;
  };
  alarm 0;
    
  if ( $@ or not defined $dbh ) {
    my $error = $DBI::errstr ? $DBI::errstr : '';
    die "Can't connect to DB: $error\n";
  }

  return $dbh;
}

sub query {
  my $dbh   = shift;
  my $query = shift;
  my $args  = shift;
  my $sth   = $dbh->prepare($query) or return $dbh->errstr;
  my $rv    = $sth->execute(@$args) or return $sth->errstr;

  return undef unless $rv;

  my @rows;
  while ( my $row = $sth->fetchrow_hashref ) {
    push @rows, $row;
  }

  #warn "QUERY: ($query) VALUES: (@$args)\n";
  return undef if ( $sth->rows == 0 );
  return \@rows;
}

sub query_sin_respuesta {
  my $dbh   = shift;
  my $query = shift;
  my $args  = shift;
  my $sth   = $dbh->prepare($query) or return $dbh->errstr;
  my $rv    = $sth->execute(@$args) or return $sth->errstr;

  $dbh->commit or return $dbh->errstr;
  $sth->finish;

  #warn "QUERY: ($query) VALUES: (@$args)\n";
  return $rv;
}

sub trans_sin_respuesta {
  my $dbh   = shift;
  my $query = shift;
  my $exito = 1;
  my $cont  = 0;

  #  Entre más queries tenga, más memoria voy a necesitar!!!
  foreach my $q ( @{$query} ) {
    ${$query}[$cont][2] = $dbh->prepare_cached(${$q}[0]);
    $exito &&= ${$query}[$cont][2]->execute(@{${$q}[1]});
    $cont++;
    print "QUERY (${$q}[0]), exito ($exito)\n";
  }

  print "EXITO ($exito)\n";
  my $resultado = ( $exito ? $dbh->commit : $dbh->rollback );

  return undef unless $resultado;
  return $exito;
}
#  Fin de funciones de Bases de Datos.


1;
