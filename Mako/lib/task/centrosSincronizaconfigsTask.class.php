<?php
/**
 * Sincroniza los datos de configuración de los centros.
 *
 * @author edgar
 *
 */
class centrosSincronizaconfigsTask extends sfBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'Nombre de la app', 'frontend'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'Contexto',  'tolu2' ),
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'Conexion', 'propel'),
            new sfCommandOption('en', null, sfCommandOption::PARAMETER_REQUIRED, 'Alias del centro en donde se pretende sincronizar la informacion', null),
            new sfCommandOption('de', null, sfCommandOption::PARAMETER_REQUIRED, 'Alias del centro en donde se pretende sincronizar la informacion', null)
        ));

        $this->namespace           = 'centros';
        $this->name                = 'sincroniza-configs';
        $this->briefDescription    = 'Sincroniza los datos de configuración de los centros, de central a todos los centros, o a un solo centro.';
        $this->detailedDescription = <<<EOF
[centros:sincroniza-configs|INFO] Sincroniza los datos de configuración de los centros, como nombre, id, alias, ip, dirección, etc.
  [php symfony centros:sincroniza-configs|INFO] Sincroniza toda la información de centros en central.
  [php symfony centros:sincroniza-configs --centro="tolu1"|INFO] Sincroniza la información para el centro seleccionado.
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        // Se inicializa la conexion a la base
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'] ? $options['connection'] : null)->getConnection();

        if ($options['en'] == null and $options['de'] == null) {
            $c = new Criteria();
            $c->add(CentroPeer::ID, 9999, Criteria::LESS_THAN);
            $c->addAscendingOrderByColumn(CentroPeer::ID);

            $centros = CentroPeer::doSelect($c);

            foreach ($centros as $centro) {
                error_log("======= Sincronizando INFO de: " . $centro->getAliasReporte() . " IP " . $centro->getIp() . " En todos centros");
                $this->sincroniza($centro);
            }
        } elseif ($options['en'] != null and $options['de'] == null) {
            //Se toma un solo centro para sincronizar todos los valores del catalogo central
            $en = pg_escape_string($options['en']);
            $c  = new Criteria();
            $c->add(CentroPeer::ALIAS, $en);
            $centro = CentroPeer::doSelectOne($c);

            if ($centro != null) {
                $this->sincroniza($centro);
            } else {
                error_log("No se encuentra el centro $en");
            }
        } elseif ($options['en'] != null and $options['de'] != null) {
            //Se toma un solo centro para sincronizar todos los valores del catalogo central
            $en = pg_escape_string($options['en']);
            $de = pg_escape_string($options['de']);
            $c  = new Criteria();
            $c->add(CentroPeer::ALIAS, $en);
            $centro = CentroPeer::doSelectOne($c);

            if ($centro != null) {
                $this->sincroniza($centro, $de);
            } else {
                error_log("No se encuentra el centro $en");
            }
        } else { //Se toma un solo centro para sincronizar todos los valores del catalogo central
            $c = new Criteria();
            $c->add(CentroPeer::ID, 9999, Criteria::LESS_THAN);
            $c->addAscendingOrderByColumn(CentroPeer::ID);
            $centros = CentroPeer::doSelect($c);
            $de      = pg_escape_string($options['de']);
            foreach ($centros as $centro) {
                error_log("======= Sincronizando INFO de: " . $de . " IP " . $centro->getIp() . " En todos centros");
                $this->sincroniza($centro, $de);
            }
        }
    }

    private function sincroniza(Centro $info, $de      = null) {

        try {
            $cliente = new SoapClient("http://" . $info->getIp() . "/MakoApi.wsdl", array("connection_timeout" => 5));
        }
        catch(Exception $e) {
            error_log("Error al conectar cliente de ws " . $e->getMessage());
            return 1;
        }

        $c = new Criteria();
        $c->add(CentroPeer::ID, 9999, Criteria::LESS_THAN);
        if ($de != null) $c->add(CentroPeer::ALIAS, $de);
        $c->addAscendingOrderByColumn(CentroPeer::ID);
        $centros = CentroPeer::doSelect($c);

        foreach ($centros as $centro) {
            $this->log($this->formatter->format("\tCentro: " . $centro->getAliasReporte() . " IP " . $centro->getIp(), 'COMMENT'));

            try {

                $cArr = $centro->toArray();
                $cStr = sfYaml::dump($cArr);

                $res  = $cliente->sync_recibeSync($cStr, 'update', 'Centro', $centro->getId());

                $this->log("Sync OK: $chost operacion: " . $centro->getId() . " Alias " . $centro->getAliasReporte(), 'INFO');
            }
            catch(Exception $e) {
                error_log("Error al ejecutar el metodo en servidor local. " . $e->getMessage());
            }
        }
    }
}
