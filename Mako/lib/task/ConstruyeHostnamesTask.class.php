<?php
class ConstruyeHostnamesTask extends sfBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'               ,  'tolu2' ),
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección' , 'propel'),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación', 'frontend'),
            new sfCommandOption('alias'      , null, sfCommandOption::PARAMETER_REQUIRED, 'Alias del centro. Pej. T1' , null)
        ));

        $this->namespace           = 'mako';
        $this->name                = 'construye-hostnames';
        $this->briefDescription    = 'Construye la información de hostnames de las PC dada la abreviatura de centro al que pertenecen';
        $this->detailedDescription = <<<EOF
The [mako:contruye-hostnames|INFO] Construye la información de hostnames de las PC dada la abreviatura de centro al que pertenecen:

  [./symfony mako:construye-hostnames --env=prod |INFO]
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'] ? $options['connection'] : null)->getConnection();

        $alias = $options['alias'];

        //Se obtiene el objeto centro
        $c                         = new Criteria();
        $c->add(CentroPeer::ALIAS, strtoupper($alias));
        $centro   = CentroPeer::doSelectOne($c);

        //TODO: Queda pendiente la definicion de los hostnames para mako 2, debe proporcionarla tecnologia
        $hostname = "";

        //Para cada PC del centro:
        foreach ($centro->getComputadoras() as $pc) {
            $ip = $pc->getIp();

            //TODO: Contemplar las reglas para caja y recepcion
            if (substr($ip, -3) == '200' || substr($ip, -3) == '201') {
                $num = (substr($ip, -3) == '200') ? 'recepcion1' : 'recepcion2';
            } else {
                $num = substr($ip, -2);
            }

            $hostname = strtolower($alias) . "-" . $num . "-l";

            echo $ip . " => " . $hostname . "\n";
            $pc->setHostname($hostname);
            $pc->save();
        }
    }
}
?>