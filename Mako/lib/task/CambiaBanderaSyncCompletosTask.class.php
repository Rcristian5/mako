<?php
class CambiaBanderaSyncTask extends sfBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección' , 'propel'),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación', 'frontend'),
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'               ,  'tolu2' )
        ));

        $this->namespace           = 'mako';
        $this->name                = 'cambia-bandera-sync';
        $this->briefDescription    = 'Cambia la bandera de los registros de sincronia que ya hayan sido completados en sus respectivos centros.';
        $this->detailedDescription = <<<EOF
[mako:cambia-bandera-sync|INFO] Cambia la bandera de los registros de sincronia que ya hayan sido completados en sus respectivos centros.:

  [./symfony cambia-bandera-sync --env=prod |INFO]
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'] ? $options['connection'] : null)->getConnection();

        $c = new Criteria();
        $c->add(SincroniaPeer::COMPLETO, false);

        $syncs = SincroniaPeer::doSelect($c);

        //Para cada registro buscamos si tiene tareas pendientes:
        $c = new Criteria();
        $c->add(SincroniaCentroPeer::SINCRONIZADO, false);

        $cont = 0;
        $tot  = count($syncs);

        foreach ($syncs as $sync) {
            $scs  = $sync->getSincroniaCentros($c);

            if (count($scs) == 0) {
                $sync->setCompleto(true);
                $sync->save();
                $cont++;
            }
        }

        echo "Completados " . $cont . " registros de " . $tot . "\n";
    }
}
?>