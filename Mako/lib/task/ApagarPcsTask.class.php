<?php
class ApagarPcsTask extends sfBaseTask
{
    //Comando para apagar la pc
    private $cmd     = '/usr/bin/perl /opt/MakoScripts/apagar_pc.pl ';
    private $excepto = null;

    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección'            , 'propel'),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación'           , 'frontend'),
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'                          ,  'tolu2' ),
            new sfCommandOption('centro'     , null, sfCommandOption::PARAMETER_OPTIONAL, 'Centro donde se realiza la operación' , null),
            new sfCommandOption('aula'       , null, sfCommandOption::PARAMETER_OPTIONAL, 'Aula donde se realiza la operacion'   , null),
            new sfCommandOption('ip'         , null, sfCommandOption::PARAMETER_OPTIONAL, 'IP que se quiere apagar'              , null),
            new sfCommandOption('excepto'    , null, sfCommandOption::PARAMETER_OPTIONAL, 'IP que debe excluirse de la operación', '10.1.10.98')
        ));

        $this->namespace           = 'soporte';
        $this->name                = 'apagar-pcs';
        $this->briefDescription    = 'Apaga PCS del centro';
        $this->detailedDescription = <<<EOF
The [mako:apagar-pcs|INFO]  Apaga las PCs de centro completo, por aula o por PC individual:

  [mako soporte:apagar-pcs --centro=tolu1 |INFO]
  [mako soporte:apagar-pcs --ip="10.1.0.98" |INFO]
  [mako soporte:apagar-pcs --centro=tolu1 --aula="Aula 1" |INFO]
  [mako soporte:apagar-pcs --centro=tolu1 --aula="Aula 1" --excepto="10.1.10.98" |INFO]
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'] ? $options['connection'] : null)->getConnection();

        $this->excepto = $options['excepto'];
        if ($options['centro'] == null && $options['aula'] == null && $options['ip'] == null) {
            print "Se debe seleccionar el centro, o el aula o la ip a apagar.";
            exit;

        } elseif ($options['centro'] == null && $options['aula'] == null && $options['ip'] != null) {
            $this->apagarPC($options['ip']);

        } elseif ($options['centro'] != null && $options['aula'] != null && $options['ip'] == null) {
            $this->apagarAula($options['centro'], $options['aula']);

        } elseif ($options['centro'] != null && $options['aula'] == null && $options['ip'] == null) {
            $this->apagarCentro($options['centro']);
        }
    }

    function getSeccionIdPorNombre($centro_id, $nombre) {
        if (trim($nombre) == '') $nombre = "RECEPCION";

        $c      = new Criteria();
        $c->add(SeccionPeer::NOMBRE, strtoupper($nombre));
        $c->add(SeccionPeer::CENTRO_ID, $centro_id);
        $s = SeccionPeer::doSelectOne($c);
        if ($s != null) {
            return $s->getId();
        }
        return 0;
    }

    function apagarAula($centro, $aula) {
        $c = new Criteria();
        $c->add(CentroPeer::ALIAS, $centro);

        $cen        = CentroPeer::doSelectOne($c);
        $seccion_id = $this->getSeccionIdPorNombre($cen->getId(), $aula);

        if ($cen != null && $seccion_id != 0) {
            $centro_id  = $cen->getId();

            $c2 = new Criteria();
            $c2->add(ComputadoraPeer::CENTRO_ID, $centro_id);
            $c2->add(ComputadoraPeer::SECCION_ID, $seccion_id);

            $res = ComputadoraPeer::doSelect($c2);

            foreach ($res as $pc) {
                if ($this->excepto == $pc->getIp()) continue;
                echo "Apagando pc en $centro y $aula: " . $pc->getIp() . "\n";
                exec($this->cmd . " " . $pc->getIp());
            }
        }
    }

    function apagarCentro($centro) {
        $c = new Criteria();
        $c->add(CentroPeer::ALIAS, $centro);

        $cen = CentroPeer::doSelectOne($c);

        if ($cen != null) {
            $centro_id = $cen->getId();

            $c2        = new Criteria();
            $c2->add(ComputadoraPeer::CENTRO_ID, $centro_id);

            $res = ComputadoraPeer::doSelect($c2);
            foreach ($res as $pc) {
                if ($this->excepto == $pc->getIp()) continue;
                echo "Apagando: Centro: $centro -> " . $pc->getIp() . "\n";
                exec($this->cmd . " " . $pc->getIp());
            }
        }
    }

    function apagarPC($ip) {
        error_log(sfConfig::get("sf_environment"));

        $c = new Criteria();
        $c->add(ComputadoraPeer::IP, $ip);

        $compu = ComputadoraPeer::doSelectOne($c);

        if ($compu != null && $this->excepto != $ip) {
            echo "Apagando: $ip\n";
            error_log("Apagando pc con comando: " . $this->cmd . " " . $compu->getIp());
            exec($this->cmd . " " . $compu->getIp());
            return;
        }
    }
}
?>