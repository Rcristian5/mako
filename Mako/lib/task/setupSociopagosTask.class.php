<?php
class setupSociopagosTask extends sfBaseTask
{
    protected function configure() {
        // // add your own arguments here
        // $this->addArguments(array(
        //   new sfCommandArgument('my_arg', sfCommandArgument::REQUIRED, 'My argument'),
        // ));

        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación', 'frontend'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente',  'tolu2' ),
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conexión', 'propel'),
        ));

        $this->namespace           = 'setup';
        $this->name                = 'socio-pagos';
        $this->briefDescription    = 'Setea los campos faltantes en la nueva tabla de socio_pagos (20100-01-16)';
        $this->detailedDescription = <<<EOF
[setup:socio-pagos|INFO] Setea los campos faltantes en la nueva tabla de socio_pagos (20100-01-16).
 Los campos son:
 precio_lista, descuento, cantidad, tiempo, subtotal, tiempo, promocion_id
 Esto es para disociar la cobranza de los cambios que pudiera haber en los catálogos de producto y en el perfil del socio posteriores al compromiso de compra.

 También setea los centro_id que están vacíos por una omisión en socio_pagos

 [php symfony setup:socio-pagos|INFO]
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'] ? $options['connection'] : null)->getConnection();
        $configuration   = ProjectConfiguration::getApplicationConfiguration($options['application'], $options['env'], true);
        $context         = sfContext::createInstance($configuration);

        //Sacamos todos los socio_pagos de la base
        $centro_id = sfConfig::get('app_centro_actual_id');

        $c = new Criteria();
        $c->add(SocioPagosPeer::CENTRO_ID, $centro_id);
        $c->add(SocioPagosPeer::NUMERO_PAGO, 1);

        $aspp = SocioPagosPeer::doSelect($c);

        error_log("Pagos principales: " . count($aspp));

        $cont = 0;
        foreach ($aspp as $sPagoPrincipal) {
            $do = $sPagoPrincipal->getDetalleOrden();

            $sPagoPrincipal->setPrecioLista($do->getPrecioLista());
            $sPagoPrincipal->setDescuento($do->getDescuento());
            $sPagoPrincipal->setCantidad($do->getCantidad());
            $sPagoPrincipal->setTiempo($do->getTiempo());
            $sPagoPrincipal->setSubtotal($do->getSubtotal());
            $sPagoPrincipal->setPromocionId($do->getPromocionId());
            $sPagoPrincipal->save();

            //Buscamos todos los pagos siguientes que coincidan con este pago base
            $c2 = new Criteria();
            $c2->add(SocioPagosPeer::ORDEN_BASE_ID, $sPagoPrincipal->getOrdenBaseId());
            $c2->add(SocioPagosPeer::NUMERO_PAGO, 1, Criteria::GREATER_THAN);
            $c2->add(SocioPagosPeer::SUB_PRODUCTO_DE, $sPagoPrincipal->getSubProductoDe());
            $asp = SocioPagosPeer::doSelect($c2);
            foreach ($asp as $sPago) {
                //$sPago = new SocioPagos();
                $cont++;
                $sPago->setCentroId($sPagoPrincipal->getCentroId());

                $sPago->setPrecioLista($do->getPrecioLista());
                $sPago->setDescuento($do->getDescuento());
                $sPago->setCantidad($do->getCantidad());
                $sPago->setTiempo($do->getTiempo());
                $sPago->setSubtotal($do->getSubtotal());
                $sPago->setPromocionId($do->getPromocionId());
                $sPago->save();
            }
        }

        error_log("Siguientes Pagos: " . $cont);
    }
}
