<?php
class sincroniaNMPTask extends sfBaseTask
{
    protected function configure()
    {
        $this->addOptions(array(
            new sfCommandOption('application', null , sfCommandOption::PARAMETER_REQUIRED, 'Sincronización web service NMP', 'frontend'),
            new sfCommandOption('env'        , null , sfCommandOption::PARAMETER_REQUIRED, 'The environment'               ,  'tolu2' ),
            new sfCommandOption('connection' , null , sfCommandOption::PARAMETER_REQUIRED, 'The connection name'           , 'propel'),

            new sfCommandOption('id'         , 'i', sfCommandOption::PARAMETER_OPTIONAL, 'Id especifico de una sincronización'),
            new sfCommandOption('tipo'       , 'p' , sfCommandOption::PARAMETER_OPTIONAL, 'Tipo de sincronización, solo se volveran a intentar las sincronizaciones de este tipo'),
            new sfCommandOption('accion'     , 'a' , sfCommandOption::PARAMETER_OPTIONAL, 'Accion de sincronización, solo se voleran a intentar las sincronizaciones de esta acción'),
        ));

        $this->namespace           = 'NMP';
        $this->name                = 'sincroniaNMP';
        $this->briefDescription    = 'Realiza la sincronizacion con el web service de NMP';
        $this->detailedDescription = <<<EOF
[sincroniaNMP|INFO] Revisa las llamadas con errores al web service de NMP y las vuelve a intentar.
Ejcutar con:

  [php symfony NMP:sincroniaNMP  [--id="..." | --tipo="..." | --accion="..."]|INFO]
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'] ? $options['connection'] : null)->getConnection();

        // Obtener las sincronizaciones, filtradas por completadas = false y por los parametros enviados por el usuario
        $criteria = new Criteria();

        $criteria->add(SincroniaNmpPeer::COMPLETADO, false);

        if(isset($options['id'])) {
            $this->logBlock('Buscando por ID "'.$options['id'].'"', INFO);
            $criteria->add(SincroniaNmpPeer::ID, $options['id']);
        }

        if(isset($options['tipo'])) {
            $this->logBlock('Buscando por Tipo "'.$options['tipo'].'"', INFO);
            $criteria->add(SincroniaNmpPeer::TIPO, $options['tipo']);
        }

        if(isset($options['accion'])) {
            $this->logBlock('Buscando por Acción "'.$options['accion'].'"', INFO);
            $criteria->add(SincroniaNmpPeer::ACCION, $options['accion']);
        }

        $sincronizaciones = SincroniaNmpPeer::doSelect($criteria);

        // Revisar si hay sincronizaciones o no
        if(is_null($sincronizaciones) or (count($sincronizaciones) == 0)) {
            $this->logBlock('['.date('Y-F-d H:i:s').'] '.'No hay sincronizacions pendientes :D', INFO);
            exit(0);
        }

        $num_sincros = count($sincronizaciones);
        $num_exito   = '';
        $num_fallo   = '';

        $this->logBlock('['.date('Y-F-d H:i:s').'] '.
                        'Se encontr'   .(($num_sincros > 1) ? 'aron' : 'ó' ) .' '.$num_sincros.
                        ' sincronizaci'.(($num_sincros > 1) ? 'ones' : 'ón' ).
                        ' pendiente'   .(($num_sincros > 1) ? 's'    : '' )  .'.',
                        INFO);

        // Ejecutar la accion correspondiente
        foreach ($sincronizaciones as $sincronia) {
            $this->logBlock('['.date('Y-F-d H:i:s').'] '.'Intentando sincronización #'.$sincronia->getId().' ('.$sincronia->getTipo().'.'.$sincronia->getAccion().') por '.$sincronia->getIntentos().'° ves...', COMAND);

            switch ($sincronia->getTipo()) {
                case 'socio':
                    //Obtener socio
                    $socio = SocioPeer::retrieveByPk($sincronia->getReferencia());

                    if(!is_null($socio)){
                        // Armar arreglo de opciones a enviar
                        $parametros = array();
                        $accion     = '';

                        switch ($sincronia->getAccion()) {
                            case 'agregarSocio':
                                $accion     = 'registrado';
                                $parametros = array(
                                    'socio' => $socio
                                );
                                break;

                            case 'actualizarSocio':
                                $accion     = 'actualizado';
                                $parametros = array(
                                    'socio' => $socio
                                );
                                break;
                        }

                        //Disparar el evento
                        sfProjectConfiguration::getActive()->getEventDispatcher()->notify(new sfEvent(null, 'syncNMP.socio.'.$accion, $parametros));

                    } else {
                        $this->logBlock('['.date('Y-F-d H:i:s').'] '.'No se encontro un usuario con el ID "'.$sincronia->getReferencia().'"', ERROR);
                    }

                    break;

                case 'alumno':
                    $referencias = $sincronia->getReferencia();

                    //Obtener socio
                    $socio = SocioPeer::retrieveByPk($referencias['socio']);

                    //Obtener curso
                    $curso = CursoPeer::retrieveByPK($referencias['curso']);

                    //Obtener grupo
                    $grupo = GrupoPeer::retrieveByPK($referencias['grupo']);

                    if(!is_null($socio) and !is_null($curso) and !is_null($grupo)) {
                        //Construir parametros y accion
                        switch ($sincronia->getAccion()) {
                            case 'inscrito':
                                $accion     = 'inscrito';
                                $parametros = array(
                                    'socio' => $socio,
                                    'curso' => $curso,
                                    'grupo' => $grupo
                                );
                                break;
                            case 'cancelar':
                                $accion     = 'cancelar.inscripcion';
                                $parametros = array(
                                    'socio' => $socio,
                                    'curso' => $curso
                                );
                        }

                        //Disparar evento
                        sfProjectConfiguration::getActive()->getEventDispatcher()->notify(new sfEvent(null, 'syncNMP.alumno.'.$accion, $parametros));

                    } else {
                        $this->log('['.date('Y-F-d H:i:s').'] '.'No se encontraron algunos datos con estas referencias '.print_r($referencias, true));
                    }
                    break;

                default:
                    $this->log('['.date('Y-F-d H:i:s').'] '.'No se reconoce el tipo "'.$sincronia->getTipo().'"');
                    break;
            }

            $result = SincroniaNmpPeer::retrieveByPk($sincronia->getId());

            $this->logBlock('['.date('Y-F-d H:i:s').'] '.
                            'Se termino el intento de manera '.
                            ($result->getCompletado() ? 'exitosa' : 'fallida ['.Comun::limitar_cadena($result->getLogUltimoError(), 100).']'),
                            ($result->getCompletado() ? INFO      : ERROR));

            if($result->getCompletado()){
                $num_exito++;

            }else{
                $num_fallo++;
            }
        }

        $this->logBlock('['.date('Y-F-d H:i:s').'] '.'De '.$num_sincros.' sincronizaciones faltantes se completaron '.$num_exito.(($num_fallo > 0) ? ' y fallaron '.$num_fallo.'; que se intentaron nuevamente en unos minutos.' : ''), INFO);
        exit(0);
    }
}
