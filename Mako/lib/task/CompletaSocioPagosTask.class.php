<?php
class CompletaSocioPagosTask extends sfBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'               ,  'tolu2' ),
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección' , 'propel'),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación', 'frontend')
        ));

        $this->namespace           = 'mako';
        $this->name                = 'socio-pagos-completa';
        $this->briefDescription    = 'Completa los orden_base_id de la tabla socio_pagos debido al cambio de pagos extemporaneos';
        $this->detailedDescription = <<<EOF
The [mako:socio-pagos-completa|INFO] Completa los orden_base_id de la tabla socio_pagos debido al cambio de pagos extemporaneos:

  [./symfony mako:socio-pagos-completa --env=prod |INFO]
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'] ? $options['connection'] : null)->getConnection();

        //Los orden_base_id
        $c = new Criteria();
        $c->add(SocioPagosPeer::NUMERO_PAGO, 1);

        $sps = SocioPagosPeer::doSelect($c);
        foreach ($sps as $sp) {
            //$sp = new SocioPagos();

            $orden_id = $sp->getDetalleOrden()->getOrdenId();
            $socio_id = $sp->getSocioId();
            $padre_id = $sp->getSubProductoDe();

            $this->logSection('Propel', sprintf('Procesando socio %s prod: %s', $socio_id, $padre_id));
            $this->completaPagos($socio_id, $padre_id, $orden_id);
        }

        //Los alumno_pagos completos
        $c = new Criteria();
        $c->add(AlumnoPagosPeer::PAGADO, true);
        $c->add(AlumnoPagosPeer::NUMERO_PAGO, 0);
        $c->add(AlumnoPagosPeer::TIPO_COBRO_ID, null, Criteria::ISNULL);

        $aps = AlumnoPagosPeer::doSelect($c);
        foreach ($aps as $ap) {
            //$ap = new AlumnoPagos();

            $socio_id = $ap->getSocioId();
            $curso_id = $ap->getCursoId();

            $this->compraPagoCompleto($socio_id, $curso_id);
        }
    }

    /**
     * Completa los campos de los registros de pago
     * @param $socio_id
     * @param $padre_id
     * @param $orden_id
     */
    private function completaPagos($socio_id, $padre_id, $orden_id) {
        $c = new Criteria();

        $c->add(SocioPagosPeer::SOCIO_ID, $socio_id);
        $c->add(SocioPagosPeer::SUB_PRODUCTO_DE, $padre_id);

        $sps = SocioPagosPeer::doSelect($c);
        foreach ($sps as $sp) {
            //$sp = new SocioPagos();

            $this->logSection("\t\t", sprintf('Procesando pago %s', $sp->getNumeroPago()));

            $sp->setOrdenBaseId($orden_id);
            $sp->save();
        }
    }

    /**
     * Dado el socio id y el numero de pago regresa el
     */
    private function compraPagoCompleto($socio_id, $curso_id) {
        $c = new Criteria();

        $c->add(ProductoCursoPeer::CURSO_ID, $curso_id);

        $pc = ProductoCursoPeer::doSelectOne($c);
        if ($pc != null) {
            $producto_curso_id = $pc->getProductoId();
        }

        $c = new Criteria();
        $c->add(DetalleOrdenPeer::SOCIO_ID, $socio_id);

        //$c->add(DetalleOrdenPeer::PRODUCTO_ID, $producto_id);

        $dos = DetalleOrdenPeer::doSelect($c);
        foreach ($dos as $do) {
            //$do = new DetalleOrden();
            //print_r($do->getProducto());
            if ($do->getProducto()->getCategoriaId() == 2 && $do->getProducto()->getEsSubproducto() == false && $do->getOrden()->getTipoId() == 1) //Cursos
            {
                error_log($do->getId() . ",$socio_id,$curso_id,$producto_curso_id");

                $cc = new Criteria();
                $cc->add(PaquetePeer::PRODUCTO_ID, $producto_curso_id);
                $paqs    = $do->getProducto()->getPaquetesRelatedByPaqueteId();
                $arProds = array();

                foreach ($paqs as $paq) {
                    //$paq = new Paquete();
                    $arProds[] = $paq->getProductoId();
                }

                print_r($arProds);
            }
        }
    }
}
?>