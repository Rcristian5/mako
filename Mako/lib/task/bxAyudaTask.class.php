<?php
/*
 * This file is part of the symfony package.
 * (c) 2004-2006 Fabien Potencier <fabien.potencier@symfony-project.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
*/
/**
 * Displays help for a task.
 *
 * @package    symfony
 * @subpackage task
 * @author     Fabien Potencier <fabien.potencier@symfony-project.com>
 * @version    SVN: $Id: bxAyudaTask.class.php,v 1.1 2010/09/27 08:31:47 eorozco Exp $
 */
class ayudaTask extends sfCommandApplicationTask
{
    /**
     * @see sfTask
     */
    protected function configure() {
        $this->addArguments(array(new sfCommandArgument('task_name', sfCommandArgument::OPTIONAL, 'El nombre de la tarea', 'ayuda'),));
        $this->addOptions(array(new sfCommandOption('xml', null, sfCommandOption::PARAMETER_NONE, 'Salida como XML')));

        $this->addOptions(array(
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'               ,  'tolu2' ),
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección' , 'propel'),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación', 'frontend')
        ));

        $this->briefDescription    = 'Muestra la ayuda de una tarea';
        $this->detailedDescription = <<<EOF
 [ayuda|INFO] muestra la ayuda para una tarea dada:

  [mako ayuda test:all|INFO]

EOF;

    }
    /**
     * @see sfTask
     */
    protected function execute($arguments = array(), $options = array()) {
        if (!isset($this->commandApplication)) {
            throw new sfCommandException('You can only launch this task from the command line.');
        }

        $task = $this->commandApplication->getTask($arguments['task_name']);

        if ($options['xml']) {
            $this->outputAsXml($task);
        } else {
            $this->outputAsText($task);
        }
    }

    protected function outputAsText(sfTask $task) {
        $messages = array();

        $messages[] = $this->formatter->format('Uso:', 'COMMENT');
        $messages[] = $this->formatter->format(sprintf(' ' . $task->getSynopsis(), null === $this->commandApplication ? '' : $this->commandApplication->getName())) . "\n";

        // find the largest option or argument name
        $max = 0;
        foreach ($task->getOptions() as $option) {
            $max = strlen($option->getName()) + 2 > $max ? strlen($option->getName()) + 2 : $max;
        }

        foreach ($task->getArguments() as $argument) {
            $max = strlen($argument->getName()) > $max ? strlen($argument->getName()) : $max;
        }

        $max+= strlen($this->formatter->format(' ', 'INFO'));

        if ($task->getAliases()) {
            $messages[] = $this->formatter->format('Alias:', 'COMMENT') . ' ' . $this->formatter->format(implode(', ', $task->getAliases()), 'INFO') . "\n";
        }

        if ($task->getArguments()) {
            $messages[] = $this->formatter->format('Argumentos:', 'COMMENT');
            foreach ($task->getArguments() as $argument) {
                $default    = null !== $argument->getDefault() && (!is_array($argument->getDefault()) || count($argument->getDefault())) ? $this->formatter->format(sprintf(' (default: %s)', is_array($argument->getDefault()) ? str_replace("\n", '', print_r($argument->getDefault(), true)) : $argument->getDefault()), 'COMMENT') : '';
                $messages[] = sprintf(" %-${max}s %s%s", $this->formatter->format($argument->getName(), 'INFO'), $argument->getHelp(), $default);
            }

            $messages[] = '';
        }

        if ($task->getOptions()) {
            $messages[] = $this->formatter->format('Opciones:', 'COMMENT');

            foreach ($task->getOptions() as $option) {
                $default    = $option->acceptParameter() && null !== $option->getDefault() && (!is_array($option->getDefault()) || count($option->getDefault())) ? $this->formatter->format(sprintf(' (default: %s)', is_array($option->getDefault()) ? str_replace("\n", '', print_r($option->getDefault(), true)) : $option->getDefault()), 'COMMENT') : '';
                $multiple   = $option->isArray() ? $this->formatter->format(' (multiples valores permitidos)', 'COMMENT') : '';
                $messages[] = sprintf(' %-' . $max . 's %s%s%s%s', $this->formatter->format('--' . $option->getName(), 'INFO'), $option->getShortcut() ? sprintf('(-%s) ', $option->getShortcut()) : '', $option->getHelp(), $default, $multiple);
            }

            $messages[] = '';
        }

        if ($detailedDescription = $task->getDetailedDescription()) {
            $messages[] = $this->formatter->format('Descripción:', 'COMMENT');
            $messages[] = ' ' . implode("\n ", explode("\n", $detailedDescription)) . "\n";
        }

        $this->log($messages);
    }

    protected function outputAsXml(sfTask $task) {
        echo $task->asXml();
    }
}
