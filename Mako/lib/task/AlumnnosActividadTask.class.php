<?php
class AlumnoActividadTask extends sfBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación', 'frontend'),
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'               ,  'tolu2' ),
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conexión'  , 'propel'),
            new sfCommandOption('desde'      , null, sfCommandOption::PARAMETER_REQUIRED, 'Parametro requerido para indicar a partir de que fecha se requiere generar el reporte'),
            new sfCommandOption('hasta'      , null, sfCommandOption::PARAMETER_OPTIONAL, 'Parametro opcional para indicar hasta que fecha se obtendran las calificaciones')
        ));

        $this->namespace           = 'mako';
        $this->name                = 'alumnos-actividad-ria';
        $this->briefDescription    = 'Genera reporte de actividad de alumnos';
        $this->detailedDescription = <<<EOF
The [mako:reporte-pagos-semanales|INFO] task does things.
Call it with:

  [php symfony mako:reporte-pagos-semanales|INFO]
EOF;


    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'] ? $options['connection'] : null)->getConnection();

        $reporteGrupoF   = fopen("/tmp/actividad_alumnos_grupo.csv", "w");
        $reporteCursoF   = fopen("/tmp/actividad_alumnos_curso.csv", "w");
        $reporteCentroF  = fopen("/tmp/actividad_alumnos_centro.csv", "w");

        if ($options['desde'] && $options['hasta']) {
            $grupoReporte  = array();
            $cursoReporte  = array();
            $centroReporte = array();
            $centroId      = sfConfig::get('app_centro_actual_id');
            $centro        = CentroPeer::retrieveByPK($centroId);
            $desde         = $options['desde'] . ' 00:00:00';
            $hasta         = $options['hasta'] . ' 23:59:59';
            $criteria      = new Criteria();

            $criteria->clear();
            $criteria->addSelectColumn(HorarioPeer::ID);
            $criteria->add(HorarioPeer::CENTRO_ID, $centroId);
            $criteria->add(
                FechasHorarioPeer::FECHA,
                FechasHorarioPeer::FECHA .
                    " BETWEEN '" . $options['desde'] .
                    "' AND '"    . $options['hasta'] .
                    "'",
                Criteria::CUSTOM);
            $criteria->addJoin(FechasHorarioPeer::HORARIO_ID, HorarioPeer::ID);
            $criteria->addJoin(HorarioPeer::ID, GrupoPeer::HORARIO_ID);
            $criteria->add(GrupoPeer::ACTIVO, true);
            $criteria->addGroupByColumn(HorarioPeer::ID);

            foreach (HorarioPeer::doSelect($criteria) as $horario) {
                $criteriaGrupo = new Criteria();
                $criteriaGrupo->add(GrupoPeer::HORARIO_ID, $horario->getId());

                $grupo = GrupoPeer::doSelectOne($criteriaGrupo);

                foreach ($grupo->getGrupoAlumnoss() as $alumno) {
                    if (!$alumno->getPreinscrito()) {
                        $asistencias      = 0;
                        $clases           = 0;
                        $pagosProgramados = 0;
                        $pagosPagados     = 0;
                        $cobranzaEsperada = 0;
                        $cobranzaTotal    = 0;

                        // Asistencias
                        $criteriaA = new Criteria();
                        $criteriaA->add(FechasHorarioPeer::FECHA,
                            FechasHorarioPeer::FECHA .
                                " BETWEEN '" . $options['desde'] .
                                "' AND '"    . $options['hasta'] .
                                "'",
                            Criteria::CUSTOM);
                        $criteriaA->addJoin(FechasHorarioPeer::HORARIO_ID, HorarioPeer::ID);
                        $criteriaA->add(HorarioPeer::CURSO_ID, $grupo->getCursoId());
                        $criteriaA->addJoin(HorarioPeer::ID, GrupoPeer::HORARIO_ID);
                        $criteriaA->add(GrupoPeer::CENTRO_ID, $centroId);
                        $criteriaA->add(GrupoPeer::ID, $grupo->getId());
                        $criteriaA->add(GrupoPeer::ACTIVO, true);

                        foreach (FechasHorarioPeer::doSelect($criteriaA) as $fecha) {
                            $criteriaAs = new Criteria();
                            $criteriaAs->add(SesionHistoricoPeer::CENTRO_ID, $centroId);
                            $criteriaAs->add(SesionHistoricoPeer::SOCIO_ID, $alumno->getAlumnoId());

                            $desdeF = $fecha->getFecha('Y-m-d') . ' ' . $fecha->getHoraInicio('H:i:s');
                            $hastaF = $fecha->getFecha('Y-m-d') . ' ' . $fecha->getHoraFin('H:i:s');

                            $criteriaAs->add(SesionHistoricoPeer::FECHA_LOGIN,
                                             SesionHistoricoPeer::FECHA_LOGIN .
                                                " BETWEEN '" . $desdeF .
                                                "' AND '" . $hastaF .
                                                "'",
                                            Criteria::CUSTOM);

                            if (SesionHistoricoPeer::doCount($criteriaAs) > 0) {
                                $asistencias++;
                            }

                            $clases++;
                        }

                        $c3 = new Criteria();
                        $c3->add(AlumnoPagosPeer::SOCIO_ID, $alumno->getAlumnoId());
                        $c3->add(AlumnoPagosPeer::CURSO_ID, $grupo->getCursoId());
                        $c3->add(AlumnoPagosPeer::GRUPO_ID, $grupo->getId());

                        $alumnoPago = AlumnoPagosPeer::doSelectOne($c3);

                        if ($alumnoPago != null) {
                            //Obtenemos el orden_id de este pago
                            $orden           = $alumnoPago->getOrden();
                            $sub_producto_de = $alumnoPago->getSubProductoDe();

                            if ($orden != null) {
                                //Obtenemos los detalle_orden_id relacionados con esta orden
                                $detalleOrdenes  = $orden->getDetalleOrdens();

                                foreach ($detalleOrdenes as $detalleOrden) {
                                    foreach ($detalleOrden->getSocioPagoss() as $socioPago) {
                                        $orden_base_id   = $socioPago->getOrdenBaseId();
                                        $c4              = new Criteria();
                                        $c4->add(SocioPagosPeer::ORDEN_BASE_ID, $orden_base_id);
                                        $c4->add(SocioPagosPeer::SUB_PRODUCTO_DE, $sub_producto_de);
                                        $c4->add(SocioPagosPeer::FECHA_A_PAGAR,
                                                 SocioPagosPeer::FECHA_A_PAGAR .
                                                    " BETWEEN '" . $desde .
                                                    "' AND '"    . $hasta .
                                                    "'",
                                                Criteria::CUSTOM);

                                        $pagosProgramados += SocioPagosPeer::doCount($c4);

                                        foreach (SocioPagosPeer::doSelect($c4) as $esperada) {
                                            $cobranzaEsperada+= $esperada->getSubtotal();
                                        }

                                        $c5 = new Criteria();
                                        $c5->add(SocioPagosPeer::ORDEN_BASE_ID, $orden_base_id);
                                        $c5->add(SocioPagosPeer::SUB_PRODUCTO_DE, $sub_producto_de);
                                        $c5->add(SocioPagosPeer::FECHA_A_PAGAR,
                                                 SocioPagosPeer::FECHA_A_PAGAR .
                                                    " BETWEEN '" . $desde .
                                                    "' AND '"    . $hasta .
                                                    "'",
                                                Criteria::CUSTOM);
                                        $c5->add(SocioPagosPeer::PAGADO, true);
                                        $c5->add(SocioPagosPeer::CANCELADO, false);

                                        $pagosPagados+= SocioPagosPeer::doCount($c5);

                                        foreach (SocioPagosPeer::doSelect($c5) as $real) {
                                            $cobranzaTotal+= $real->getDetalleOrden()->getSubtotal();
                                        }
                                    }
                                }
                            }
                        }
                        if (!array_key_exists($grupo->getId(), $grupoReporte)) {
                            $grupoReporte[$grupo->getId() ] = array(
                                'centro'           => $grupo->getCentro()->getAliasReporte(),
                                'curso'            => $grupo->getCurso()->getNombre(),
                                'grupo'            => $grupo->getClave(),
                                'programados'      => $pagosProgramados,
                                'pagados'          => $pagosPagados,
                                'asistencias'      => $asistencias,
                                'clases'           => $clases,
                                'cobranzaEsperada' => $cobranzaEsperada,
                                'cobranzaTotal'    => $cobranzaTotal);
                        } else {
                            $grupoReporte[$grupo->getId() ]['programados']      += $pagosProgramados;
                            $grupoReporte[$grupo->getId() ]['pagados']          += $pagosPagados;
                            $grupoReporte[$grupo->getId() ]['asistencias']      += $asistencias;
                            $grupoReporte[$grupo->getId() ]['clases']           += $clases;
                            $grupoReporte[$grupo->getId() ]['cobranzaEsperada'] += $cobranzaEsperada;
                            $grupoReporte[$grupo->getId() ]['cobranzaTotal']    += $cobranzaTotal;
                        }

                        if (!array_key_exists($grupo->getCursoId(), $cursoReporte)) {
                            $cursoReporte[$grupo->getCursoId() ] = array(
                                'centro'           => $grupo->getCentro()->getAliasReporte(),
                                'curso'            => $grupo->getCurso()->getNombre(),
                                'programados'      => $pagosProgramados,
                                'pagados'          => $pagosPagados,
                                'asistencias'      => $asistencias,
                                'clases'           => $clases,
                                'cobranzaEsperada' => $cobranzaEsperada,
                                'cobranzaTotal'    => $cobranzaTotal);
                        } else {
                            $cursoReporte[$grupo->getCursoId() ]['programados']      += $pagosProgramados;
                            $cursoReporte[$grupo->getCursoId() ]['pagados']          += $pagosPagados;
                            $cursoReporte[$grupo->getCursoId() ]['asistencias']      += $asistencias;
                            $cursoReporte[$grupo->getCursoId() ]['clases']           += $clases;
                            $cursoReporte[$grupo->getCursoId() ]['cobranzaEsperada'] += $cobranzaEsperada;
                            $cursoReporte[$grupo->getCursoId() ]['cobranzaTotal']    += $cobranzaTotal;
                        }

                        if (!array_key_exists($grupo->getCentroId(), $centroReporte)) {
                            $centroReporte[$grupo->getCentroId() ] = array(
                                'centro'           => $grupo->getCentro()->getAliasReporte(),
                                'programados'      => $pagosProgramados,
                                'pagados'          => $pagosPagados,
                                'asistencias'      => $asistencias,
                                'clases'           => $clases,
                                'cobranzaEsperada' => $cobranzaEsperada,
                                'cobranzaTotal'    => $cobranzaTotal);
                        } else {
                            $centroReporte[$grupo->getCentroId() ]['programados']      += $pagosProgramados;
                            $centroReporte[$grupo->getCentroId() ]['pagados']          += $pagosPagados;
                            $centroReporte[$grupo->getCentroId() ]['asistencias']      += $asistencias;
                            $centroReporte[$grupo->getCentroId() ]['clases']           += $clases;
                            $centroReporte[$grupo->getCentroId() ]['cobranzaEsperada'] += $cobranzaEsperada;
                            $centroReporte[$grupo->getCentroId() ]['cobranzaTotal']    += $cobranzaTotal;
                        }
                    }
                }
            }
            foreach ($grupoReporte as $reporteGrupo) {
                $lineaGrupo.= $reporteGrupo['centro']           . ',' .
                              $reporteGrupo['curso']            . ',' .
                              $reporteGrupo['grupo']            . ',' .
                              $reporteGrupo['programados']      . ',' .
                              $reporteGrupo['pagados']          . ',' .
                              $reporteGrupo['clases']           . ',' .
                              $reporteGrupo['asistencias']      . ',' .
                              $reporteGrupo['cobranzaEsperada'] . ',' .
                              $reporteGrupo['cobranzaTotal']    . "\n\r";
            }
            foreach ($cursoReporte as $reporteCurso) {
                $lineaCurso.= $reporteCurso['centro']           . ',' .
                              $reporteCurso['curso']            . ',' .
                              $reporteCurso['programados']      . ',' .
                              $reporteCurso['pagados']          . ',' .
                              $reporteCurso['clases']           . ',' .
                              $reporteCurso['asistencias']      . ',' .
                              $reporteCurso['cobranzaEsperada'] . ',' .
                              $reporteCurso['cobranzaTotal']    . "\n\r";
            }
            foreach ($centroReporte as $reporteCentro) {
                $lineaCentro.= $reporteCentro['centro']           . ',' .
                               $reporteCentro['programados']      . ',' .
                               $reporteCentro['pagados']          . ',' .
                               $reporteCentro['clases']           . ',' .
                               $reporteCentro['asistencias']      . ',' .
                               $reporteCentro['cobranzaEsperada'] . ',' .
                               $reporteCentro['cobranzaTotal']    . "\n\r";
            }

            fwrite($reporteGrupoF, $lineaGrupo);
            fwrite($reporteCursoF, $lineaCurso);
            fwrite($reporteCentroF, $lineaCentro);

            fclose($reporteGrupoF);
            fclose($reporteCursoF);
            fclose($reporteCentroF);
        } else {
            $this->log('Falta la fecha de inicio o la fecha final');
        }
    }
}

