<?php
class ImportarSociosInicialTask extends sfBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'               ,  'tolu2' ),
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección' , 'propel'),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación', 'frontend')
        ));

        $this->namespace           = 'mako';
        $this->name                = 'socios-carga-inicial';
        $this->briefDescription    = 'Carga la informacion de socios de la tabla vieja de usuarios de mako 1. Solo debe ejecutarse al inicio de operacion de mako 2';
        $this->detailedDescription = <<<EOF
The [mako:socios-carga-inicial|INFO] Carga la informacion de socios de la tabla vieja de usuarios de mako 1. Solo debe ejecutarse al inicio de operacion de mako 2:

  [./symfony mako:socios-carga-inicial --env=prod |INFO]
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'] ? $options['connection'] : null)->getConnection();

        $con->exec("DELETE FROM socio");

        //Hacemos el query de usuarios limpios
        $sql = "SELECT * FROM socios_inicial";

        foreach ($con->query($sql) as $s) {
            $socio = new Socio();

            $socio->setId($s['id_usuario']);
            $socio->setRolId($s['id_rol']);
            $socio->setUsuario($s['usuario']);
            $socio->setClave($s['clave']);
            $socio->setNombre($s['nombre']);
            $socio->setApepat($s['apepat']);
            $socio->setApemat($s['apemat']);
            $socio->setSexo($s['sexo']);
            $socio->setEmail($s['email']);
            $socio->setFolio($s['folio']);
            $socio->setFechaNac($s['fecha_nac']);
            $socio->setRolId(7);

            //TODO: Implementar la busqueda del ldap_uid
            //TODO: implementar la traduccion de catalogos.

            //escolaridad

            //salud

            //habilidades_informaticas

            //dominio_ingles

            //ocupacion

            $socio->setIdEstado($s['id_estado']);
            $socio->setEstado($s['estado']);
            $socio->setMunicipio($s['municipio']);
            $socio->setColonia($s['colonia']);
            $socio->setCp($s['cp']);
            $socio->setCalle($s['calle']);
            $socio->setNumExt($s['num_ext']);
            $socio->setNumInt($s['num_int']);
            $socio->setFoto($s['foto']);
            $socio->setCentroId($s['id_centro']);
            $socio->setVigente($s['vigente']);
            $socio->setActivo($s['activo']);
            $socio->setHuella($s['huella']);
            $socio->setSaldo(($s['saldo'] < 0) ? 0 : $s['saldo']);
            $socio->setTel($s['tel']);
            $socio->setCelular($s['celular']);
            $socio->setRazonSocial($s['razon_social']);
            $socio->setRfc($s['rfc']);
            $socio->setColoniaFiscal($s['colonia_fiscal']);
            $socio->setCpFiscal($s['cp_fiscal']);
            $socio->setEstadoFiscal($s['estado_fiscal']);
            $socio->setMunicipioFiscal($s['municipio_fiscal']);
            $socio->setNumExtFiscal($s['num_ext_fiscal']);
            $socio->setNumIntFiscal($s['num_int_fiscal']);

            $socio->setCreatedAt($s['fecha_alta']);
            $socio->setUpdatedAt($s['fecha_modificacion']);

            $socio->save();
        }
    }
}
?>