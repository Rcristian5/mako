<?php
class SyncGruposTask extends sfPropelBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'               ,  'tolu2' ),
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección' , 'propel'),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación', 'frontend')
        ));

        $this->namespace           = 'mako';
        $this->name                = 'sync-grupos-central';
        $this->briefDescription    = 'Sincroniza los grupos del servidor central';
        $this->detailedDescription = <<<EOF
 [mako:add-cursos-moodlle|INFO] Crea el registro de sincronia de los grupos para los centros

  [./symfony mako:ejecuta-sync --env=prod |INFO]
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'])->getConnection();

        $this->logSection('Guardando Grupos ', '…');

        $datos = array(
            array('horario' => 128805440939390, 'facilitador' => 93),
            array('horario' => 128805447821609, 'facilitador' => 93),
            array('horario' => 128805458726064, 'facilitador' => 42),
            array('horario' => 128805527659995, 'facilitador' => 42),
            array('horario' => 128805542571041, 'facilitador' => 106),
            array('horario' => 128805549741187, 'facilitador' => 106),
            array('horario' => 128805557952093, 'facilitador' => 107),
            array('horario' => 128805567234837, 'facilitador' => 106),
            array('horario' => 128805575862705, 'facilitador' => 107),
            array('horario' => 128805587629768, 'facilitador' => 106),
            array('horario' => 128805594421332, 'facilitador' => 106),
            array('horario' => 128805624586041, 'facilitador' => 111),
            array('horario' => 128805630734417, 'facilitador' => 111),
            array('horario' => 128805635792180, 'facilitador' => 112),
            array('horario' => 128805640706807, 'facilitador' => 112),
            array('horario' => 128805647252619, 'facilitador' => 112),
            array('horario' => 128805653035439, 'facilitador' => 111),
            array('horario' => 128805663184189, 'facilitador' => 107),
            array('horario' => 128805657918388, 'facilitador' => 106),
            array('horario' => 128805670381780, 'facilitador' => 111),
            array('horario' => 128805677288565, 'facilitador' => 107),
            array('horario' => 128805682970410, 'facilitador' => 106),
            array('horario' => 128805689464975, 'facilitador' => 106),
            array('horario' => 128805701495388, 'facilitador' => 121),
            array('horario' => 128805741809204, 'facilitador' => 121)
        );

        foreach ($datos as $faltante) {
            $horario = HorarioPeer::retrieveByPK($faltante['horario']);
            $grupo   = new Grupo();
            $grupo->setCentroId($horario->getCentroId());
            $grupo->setCursoId($horario->getCursoId());
            $grupo->setAulaId($horario->getAulaId());
            $grupo->setHorarioId($horario->getId());
            $grupo->setFacilitadorId($faltanteπ['facilitador']);
            $grupo->setClave(GrupoPeer::generaClave($horario->getCurso()->getClave()));
            $grupo->setCupoTotal($horario->getAula()->getCapacidad());
            $grupo->setDuracion($horario->getCurso()->getDuracion());
            $grupo->setOperadorId(2);
            $grupo->save();
        }

        $this->logSection('Generando lista de grupos activos', '…');
        $c = new Criteria();
        $c->add(GrupoPeer::ACTIVO, true);
        $criterion = $c->getNewCriterion(GrupoPeer::CREATED_AT, '2010-10-25', Criteria::GREATER_EQUAL);
        $criterion->addAnd($h->getNewCriterion(GrupoPeer::CREATED_AT, '2010-10-14 10:50:00', Criteria::LESS_EQUAL));
        $c->add(GrupoPeer::CREATED_AT, '2010-10-25', Criteria::GREATER_THAN);
        $c->add($criterion);
        $c->add(GrupoPeer::CREATED_AT, '2010-10-12', Criteria::GREATER_THAN);
        $c->add(CursoPeer::PUBLICADO, true);
        $c->addJoin(GrupoPeer::CURSO_ID, CursoPeer::ID);
        $c->addAscendingOrderByColumn(GrupoPeer::ID);
        $this->logSection('Creando registros de sincronia', '…');

        foreach (GrupoPeer::doSelect($c) as $grupo) {
            $s = new Criteria();
            $s->add(SincroniaPeer::PK_ASYNC, $grupo->getId());

            $sincronia = SincroniaPeer::doCount($s);

            if ($sincronia == 0) {
                $this->logSection('Crenado horarios', '…');
                $this->logSection('Crenado registro horario', $grupo->getHorario()->getId());

                sfProjectConfiguration::getActive()->getEventDispatcher()->notify(new sfEvent($grupo->getHorario(), 'horario.creado', array('horario' => $grupo->getHorario())));
            }
        }

        foreach (GrupoPeer::doSelect($c) as $grupo) {
            $s = new Criteria();
            $s->add(SincroniaPeer::PK_ASYNC, $grupo->getId());
            $sincronia = SincroniaPeer::doCount($s);
            if ($sincronia == 0) {
                $this->logSection('Crenado grupos', '…');
                $this->logSection('Crenado registro grupo', $grupo->getClave());
                sfProjectConfiguration::getActive()->getEventDispatcher()->notify(new sfEvent($grupo, 'grupo.creado', array('grupo' => $grupo)));
            }
        }
        $this->logSection('Se termino de cargar los registros de sincronia', '');
    }
}
?>
