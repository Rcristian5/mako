<?php
/**
 * Enter description here ...
 * @author edgar
 *
 */
class RegeneraComprobantesCorteTask extends sfBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name'                         , 'frontend'),
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'The environment'                              ,  'tolu2' ),
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name'                          , 'propel'),
            new sfCommandOption('fecha'      , null, sfCommandOption::PARAMETER_OPTIONAL, 'Fecha del corte'                              , null),
            new sfCommandOption('hora'       , null, sfCommandOption::PARAMETER_OPTIONAL, 'Hora del corte'                               , null),
            new sfCommandOption('folio'      , null, sfCommandOption::PARAMETER_OPTIONAL, 'Folio con el que debe salir la factura'       , null),
            new sfCommandOption('folio-tasa0', null, sfCommandOption::PARAMETER_OPTIONAL, 'Folio con el que debe salir la factura tasa 0', null)
        ));

        $this->namespace           = 'mako';
        $this->name                = 'regenera-comprobantes-corte';
        $this->briefDescription    = 'Regenera los comprobantes del corte de caja dada la fecha y hora del corte';
        $this->detailedDescription = <<<EOF
[mako:regenera-comprobantes-corte --fecha="2010-01-01" --hora="18:34:32" --folio=123 --folio-tasa0=124 |INFO] Regenera los comprobantes del corte de caja dada su fecha y hora
Debe proporcionarse la fecha, hora y el folio para regenerar los comprobantes. Opcionalmente si existen facturas debidas
a operaciones de tasa 0 (ventas de libros) debe proporcionarse el folio con el que debe salir la factura tasa 0 correspondiente.

EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $ctx = sfContext::createInstance($this->configuration);
        $ctx->getConfiguration()->loadHelpers('Partial');
        $ctx->getConfiguration()->loadHelpers('Facturas');

        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'])->getConnection();

        $folio = intval($options['folio']);
        $fecha = $options['fecha'];
        $hora  = $options['hora'];

        if ($folio == null || $fecha == null || $hora == null) {
            echo "Debe proporcionar los datos FOLIO, FECHA y HORA.";
            exit;
        }

        //Primero debemos traer los datos del corte de caja
        $c = new Criteria();
        $c->add(CortePeer::CREATED_AT, $fecha . " " . $hora);

        $corte = CortePeer::doSelectOne($c);
        print ($c->toString());

        if ($corte == null) {
            echo "No existe el corte solicitado";
            exit;
        }
        /***************************************
        **  @@ LGL  2016-06-06
        **  Validamos que el corte a regenerar no tenga ordenes de tasa 0
        **  Si es así, se debe solicitar también el FOLIO-TASA0 para la factura a generar
        ****************************************/
        $ft0 = CortePeer::foliosTasa0($corte);

        //Si no lo hay entonces terminamos sin mas el proceso
        if (count($ft0['ordenes_tasa0']) && !isset($options['folio-tasa0'])) {
            echo "El corte seleccionado tiene ordenes Tasa0, por lo cual se requiere el dato FOLIO-TASA0.\n";
            exit;
        }


        self::generaFacturaCorte($corte, $folio, $connection);
        $ordenes = $corte->getOrdens();

        self::ticketCorte($corte, $ordenes);

        echo "Se ha generado factura folio $folio\n";
        if (isset($options['folio-tasa0'])) {
            self::generaFacturaCorteTasa0($corte, $options['folio-tasa0'], $connection);
            echo "Se ha generado factura tasa 0 con folio {$options['folio-tasa0']}\n";
        }
    }

    /**
     * LLama las facturas
     *
     * @param Corte $corte
     * @param unknown_type $folio
     */
    static function generaFacturaCorte(Corte $corte, $folio, $connection) {
        //Vemos si hay alguna venta publico general tasa iva

        $ft = CortePeer::foliosTasa($corte);

        //Si no lo hay entonces terminamos sin mas el proceso
        if (!count($ft['ordenes_tasa'])) return;

        $ip  = Comun::ipReal();
        $id  = $corte->getId();

        $ctx = sfContext::getInstance();
        $ctx->getConfiguration()->loadHelpers('Partial');
        $ctx->getConfiguration()->loadHelpers('Facturas');
    try{
        $connection->beginTransaction();

        list($confFactura, $conceptos, $ffis)  = self::facturaCorte($corte, $folio);

        //Sobreescribimos con seter el folio en ffis para que no ponga el actual, sino el que queremos.
        $ffis->setFolioActual($folio);

        $df       = generaFactura($confFactura, $conceptos);
        $cadena   = $df['cadena_original'];
        $sello    = $df['sello_digital'];
        $total    = $confFactura['total'];
        $subTotal = $confFactura['subTotal'];
        $importe  = $subTotal;
        $iva      = $confFactura['Impuestos']['Traslados']['Traslado']['importe'];

        $corte_factura = TrCorteFacturaPeer::getFacturaByCorte($corte->getId(),sfConfig::get('app_tipo_factura_tasa16'));
        if($corte_factura == null)
        {
            $corte_factura = new TrCorteFactura();
            $f  = new Factura();
        }
        else
            $f = $corte_factura->getFactura();

        $f->setCentroId(sfConfig::get('app_centro_actual_id'));
        $f->setFolioFiscalId($ffis->getId());
        $f->setFechaFactura($corte->getCreatedAt());
        $f->setFolio($folio);
        $f->setCadenaOriginal($cadena);
        $f->setSello($sello);
        $f->setTotal($total);
        $f->setImporte($importe);
        $f->setSubtotal($subTotal);
        $f->setRetencion($iva);
        $f->save();

        $corte_factura->setCorteId($corte->getId());
        $corte_factura->setFacturaId($f);
        $corte_factura->setTipoFacturaId(sfConfig::get('app_tipo_factura_tasa16'));
        $corte_factura->setOperacionFacturaId(sfConfig::get('app_operacion_factura_corte'));
        $corte_factura->setCentroId(sfConfig::get('app_centro_actual_id'));
        $corte_factura->save();

        // @@ LGL  2016-06-06
        //   Obtenemos ID's de ordenes para factura validando su existencia para actualización
        //$factura_ordenes = TrFacturaOrden::getFacturaOrdenes($ft['ordenes_tasa']);

        foreach ($ft['ordenes_tasa'] as $orden_id)
        {
            $fo = TrFacturaOrdenPeer::getFacturaOrden($orden_id);
            if( $fo == null)
                $fo = new TrFacturaOrden();
            $fo->setFacturaId($f->getId());
            $fo->setOrdenId($orden_id);
            $fo->setCentroId(sfConfig::get('app_centro_actual_id'));
            $fo->save();
        }

        $corte->setFactura($f);
        $corte->save();

        $c_syncsap = new Criteria();
        $c_syncsap->add(PK_REFERENCIA, $corte->getId());
        $corte_sync = SincroniaSapPeer::doSelectOne($c_syncsap);

        if ($corte_sync == null) {
            $sincronia_sap = new SincroniaSap();
            $sincronia_sap->setOperacion('Pagos');
            $sincronia_sap->setPkReferencia($corte->getId());
            $sincronia_sap->setCompleto(false);
            $sincronia_sap->setCreatedAt(time());
            $sincronia_sap->save();
        }
        $connection->commit();
    } catch (PropelException $e) {
        $connection->forceRollBack();
        throw $e;
    }


        $descripcion = isset($conceptos[0]) ? $conceptos[0]['descripcion'] : '';
        $varsPartial = array(
            'corte'     => $corte,
            'importe'   => $importe,
            'subTotal'  => $subTotal,
            'total'     => $total,
            'iva'       => $iva,
            'cadena'    => $cadena,
            'sello'     => $sello,
            'ffis'      => $ffis,
            'conceptos' => $descripcion,
            'tasa'      => intval($corte->getCentro()->getIva())
        );






        /*
        * Reingenieria  Mako. SIN - ID RQ
        * Responsables:  (BP) Bet Gader Porcayo Juárez, (LGL) Luis Gonzales Linares
        * Fecha:        21-01-2015
        * Descripción:  Se añade opcion para configuración de Nuevas Impresoras
        */
        $Prefijo = @sfConfig::get('app_prefijo');
        switch ($Prefijo) {
            case 'oki':
                $factura_corte = get_partial('corte_caja/facturaCorte', $varsPartial);
                break;
            case 'Ch':
            case 'TMT20II':
                /*
                * En este caso se incluye el tipo Ch TMT20II
                */
                $factura_corte = get_partial('corte_caja/factura' . $Prefijo . 'Corte', $varsPartial);
                break;
            default:
                /*
                * En caso de no estar configurado el parámetro app_prefijo, o que no es alguno 
                * de los anteriores se configura con respecto a OKI, 
                */
                $factura_corte = get_partial('corte_caja/facturaCorte', $varsPartial);

                error_log('ERROR GENERICO!!! No se ha configurado el parámetro PREFIJO o el valor NO EXISTE en la configuración. Fn=generaFacturaCorte');
                break;
        }





        //Imprimirmos facturas
        $nomtick = PATH_FACTURAS . "/" . $confFactura['Emisor']['rfc'] . $confFactura['serie'] . $confFactura['folio'] . '.txt';
        $fp      = fopen($nomtick, "w+");

        fwrite($fp, $factura_corte);
        fclose($fp);

        //La factura imprimible.
        $imprimible = '/tmp/factura_corte_' . $id . '.txt';
        $fp         = fopen($imprimible, "w+");

        fwrite($fp, $factura_corte);
        fclose($fp);
    }

    static public function generaFacturaCorteTasa0(Corte $corte, $folio, $connection) {
        //Vemos si hay alguna venta publico general tasa0
        $ft0 = CortePeer::foliosTasa0($corte);

        //Si no lo hay entonces terminamos sin mas el proceso
        if (!count($ft0['ordenes_tasa0'])) {
            "NO se encontraron operaciones tasa 0.\n";
            return;
        }

        $ip  = Comun::ipReal();
        $id  = $corte->getId();

        $ctx = sfContext::getInstance();
        $ctx->getConfiguration()->loadHelpers('Partial');
        $ctx->getConfiguration()->loadHelpers('Facturas');

        list($confFactura, $conceptos, $ffis)  = self::facturaCorteTasa0($corte, $folio);

        $ffis->setFolioActual($folio);

        $df       = generaFactura($confFactura, $conceptos);
        $cadena   = $df['cadena_original'];
        $sello    = $df['sello_digital'];
        $total    = $confFactura['total'];
        $subTotal = $confFactura['subTotal'];
        $importe  = $subTotal;
        $iva      = $confFactura['Impuestos']['Traslados']['Traslado']['importe'];

    try{
        $connection->beginTransaction();

        $corte_factura = TrCorteFacturaPeer::getFacturaByCorte($corte->getId(),sfConfig::get('app_tipo_factura_tasa0'));
        if($corte_factura == null)
        {
            $corte_factura = new TrCorteFactura();
            $f  = new Factura();
        }
        else
            $f = $corte_factura->getFactura();

        $f->setCentroId(sfConfig::get('app_centro_actual_id'));
        $f->setFolioFiscalId($ffis->getId());
        $f->setFechaFactura($corte->getCreatedAt());
        $f->setFolio($folio);
        $f->setCadenaOriginal($cadena);
        $f->setSello($sello);
        $f->setTotal($total);
        $f->setImporte($importe);
        $f->setSubtotal($subTotal);
        $f->setRetencion($iva);
        $f->save();

        $corte_factura->setCorteId($corte->getId());
        $corte_factura->setFacturaId($f);
        $corte_factura->setTipoFacturaId(sfConfig::get('app_tipo_factura_tasa0'));
        $corte_factura->setOperacionFacturaId(sfConfig::get('app_operacion_factura_corte'));
        $corte_factura->setCentroId(sfConfig::get('app_centro_actual_id'));
        $corte_factura->save();

         foreach ($ft0['ordenes_tasa0'] as $orden_id)
        {
            $fo = TrFacturaOrdenPeer::getFacturaOrden($orden_id);
            if( $fo == null)
                $fo = new TrFacturaOrden();
            $fo->setFacturaId($f->getId());
            $fo->setOrdenId($orden_id);
            $fo->setCentroId(sfConfig::get('app_centro_actual_id'));
            $fo->save();
        }

        $connection->commit();
    } catch (PropelException $e) {
        $connection->forceRollBack();
        throw $e;
    }

        //$corte->setFactura($f);
        //$corte->save();

        $descripcion = isset($conceptos[0]) ? $conceptos[0]['descripcion'] : '';
        $varsPartial = array(
            'corte'     => $corte,
            'importe'   => $importe,
            'subTotal'  => $subTotal,
            'total'     => $total,
            'iva'       => $iva,
            'cadena'    => $cadena,
            'sello'     => $sello,
            'ffis'      => $ffis,
            'conceptos' => $descripcion,
            'tasa'      => 0
        );







        /*
        * Reingenieria  Mako. SIN - ID RQ
        * Responsables:  (BP) Bet Gader Porcayo Juárez, (LGL) Luis Gonzales Linares
        * Fecha:        21-01-2015
        * Descripción:  Se añade opcion para configuración de Nuevas Impresoras
        */
        $Prefijo = @sfConfig::get('app_prefijo');
        switch ($Prefijo) {
            case 'oki':
                $factura_corte = get_partial('corte_caja/facturaCorte', $varsPartial);
                break;
            case 'Ch':
            case 'TMT20II':
                /*
                * En este caso se incluye el tipo Ch TMT20II
                */
                $factura_corte = get_partial('corte_caja/factura' . $Prefijo . 'Corte', $varsPartial);
                break;
            default:
                /*
                * En caso de no estar configurado el parámetro app_prefijo, o que no es alguno 
                * de los anteriores se configura con respecto a OKI, 
                */
                $factura_corte = get_partial('corte_caja/facturaCorte', $varsPartial);

                error_log('ERROR GENERICO!!! No se ha configurado el parámetro PREFIJO o el valor NO EXISTE en la configuración. Fn=generaFacturaCorteTasa0');
                break;
        }





        //Aumentamos el folio actual fiscal
        //Imprimirmos facturas
        $nomtick = PATH_FACTURAS . "/" . $confFactura['Emisor']['rfc'] . $confFactura['serie'] . $confFactura['folio'] . '.txt';
        $fp      = fopen($nomtick, "w+");

        fwrite($fp, $factura_corte);
        fclose($fp);

        //La factura imprimible.
        $imprimible = '/tmp/factura_corte_t0_' . $id . '.txt';
        $fp         = fopen($imprimible, "w+");

        fwrite($fp, $factura_corte);
        fclose($fp);

        //$res = exec("lp -h ".$ip." -d okipos $nomtick");
        //Se elimina la impresion aqui debido a los problemas con las impresoras chafas que adquirió enova en fase 2
        //Ahora solo se generan los archivos temporales y se mandan imprimir desde botones en la interfaz de corte.
    }

    /**
     * Prepara la factura del corte
     *
     * @param Corte $corte
     * @param integer $folio
     * @return multitype:multitype:multitype:string   multitype:string unknown multitype:string multitype:string   NULL mixed multitype:multitype:multitype:string    multitype:string multitype:mixed Ambigous <mixed, multitype:>    Ambigous <FolioFiscal, NULL, unknown, multitype:>
     */
    static public function facturaCorte(Corte $corte, $folio) {
        //Datos de folios fiscales para el centro
        $ffis = FolioFiscalPeer::fiscalesCentroActual();
        $ffis->setFolioActual($folio);

        //Total a facturar
        $total = $corte->getTotalVpg();

        //Desglose de iva del total de la venta al publico en general
        list($iva, $importe, $subtotal, $en_palabras) = desglosa_iva($total, $corte->getCentro()->getIva());

        //Procesamos la fecha del corte
        $fecha_factura = str_replace(" ", "T", $corte->getCreatedAt());

        //Generamos el formato para el domicilio del emisor.
        $domEmisor = array();

        if (sfConfig::get('app_fiscales_calle')        != '') $domEmisor['calle']        = sfConfig::get('app_fiscales_calle');
        if (sfConfig::get('app_fiscales_noExterior')   != '') $domEmisor['noExterior']   = sfConfig::get('app_fiscales_noExterior');
        if (sfConfig::get('app_fiscales_noInterior')   != '') $domEmisor['noInterior']   = sfConfig::get('app_fiscales_noInterior');
        if (sfConfig::get('app_fiscales_colonia')      != '') $domEmisor['colonia']      = sfConfig::get('app_fiscales_colonia');
        if (sfConfig::get('app_fiscales_municipio')    != '') $domEmisor['municipio']    = sfConfig::get('app_fiscales_municipio');
        if (sfConfig::get('app_fiscales_estado')       != '') $domEmisor['estado']       = sfConfig::get('app_fiscales_estado');
        if (sfConfig::get('app_fiscales_pais')         != '') $domEmisor['pais']         = sfConfig::get('app_fiscales_pais');
        if (sfConfig::get('app_fiscales_codigoPostal') != '') $domEmisor['codigoPostal'] = sfConfig::get('app_fiscales_codigoPostal');

        $descuento = '0.00';

        $confFactura = array(
            //Agregue xmlns, xmlns:xsi, xsi:schemaLocation y version
            'xmlns'              => "http://www.sat.gob.mx/cfd/2",
            'xmlns:xsi'          => "http://www.w3.org/2001/XMLSchema-instance",
            'xsi:schemaLocation' => "http://www.sat.gob.mx/cfd/2 cfdv2.xsd",
            'version'            => "2.0",
            'serie'              => $ffis->getSerie(),
            'folio'              => $folio,
            'fecha'              => $fecha_factura,
            'sello'              => "",
            'noAprobacion'       => $ffis->getNoAprobacion(),
            'anoAprobacion'      => $ffis->getAnoAprobacion(),
            'tipoDeComprobante'  => "ingreso",
            'formaDePago'        => "EL PAGO DE ESTA FACTURA (CONTRAPRESTACION) SE EFECTUARA EN UNA SOLA EXHIBICION, SI POR ALGUNA RAZON NO FUERA ASI, EMITIREMOS LOS COMPROBANTES DE LAS PARCIALIDADES RESPECTIVAS",
            'subTotal'           => number_format(trim($importe), 2, '.', ''),
            'total'              => number_format(trim($total), 2, '.', ''),
            'descuento'          => number_format($descuento, 2, '.', ''),
            'noCertificado'      => $ffis->getNumCertificado(),
            'certificado'        => '',
            'Emisor'             => array(
                'rfc'             => $corte->getCentro()->getRfc(),
                'nombre'          => $corte->getCentro()->getRazonSocial(),
                'DomicilioFiscal' => $domEmisor),
            'Receptor'           => array(
                'rfc'       => "XAXX010101000",
                'nombre'    => "COMPROBANTE GLOBAL DE OPERACIONES CON PUBLICO EN GENERAL",
                'Domicilio' => array(
                    //'calle'        => "",
                    //'noExterior'   => "",
                    //'noInterior'   => "",
                    //'colonia'      => "",
                    //'municipio'    => "",
                    //'estado'       => "",
                    'pais'         => "MEXICO",
                    //'codigoPostal' => ""
                )
            ),
            //Agregue en arreglo para los impuestos
            'Impuestos'          => array(
                'Traslados' => array(
                    'Traslado' => array(
                        'impuesto' => "IVA",
                        'tasa'     => number_format($corte->getCentro()->getIva(), 2, '.', ''),
                        'importe'  => number_format(trim($iva), 2, '.', '')
                    )
                )
            )
        );

        //El arreglo de conceptos
        $conceptos   = array();
        $descripcion = "VENTA AL PUBLICO EN GENERAL, NOTAS DE VENTA FOLIOS:" .
                       $corte->getFolioVentaInicial() .
                       " A " .
                       $corte->getFolioVentaFinal();

        //Si hay folios tasa cero se indican:
        $ft0 = CortePeer::foliosTasa0($corte);
        if (count($ft0['folios'])) {
            $descripcion .= ", EXCEPTO NOTAS DE VENTA CON FOLIOS: " .
                            implode(', ', $ft0['folios']) .
                            " DEBIDAS A VENTAS CON IVA TASA 0";
        }

        $conceptos[] = array(
            'cantidad'      => "1.00",
            'descripcion'   => $descripcion,
            'valorUnitario' => number_format(trim($importe), 2, '.', ''),
            'importe'       => number_format(trim($importe), 2, '.', '')
        );

        return array($confFactura, $conceptos, $ffis);
    }

    /**
     * Realiza las operaciones para formatear los datos necesarios para la factura de corte de tasa 0.
     * @param Corte $corte El objeto de corte
     * @return array Regresa el arreglo de configuración de la factura, el arreglo de,
     */
    static public function facturaCorteTasa0(Corte $corte, $folio) {
        $tasa = 0;

        //Datos de folios fiscales para el centro
        $ffis = FolioFiscalPeer::fiscalesCentroActual();
        $ffis->setFolioActual($folio);

        //Total a facturar
        $ft0   = CortePeer::foliosTasa0($corte);
        $total = $ft0['total_tasa0'];

        if (count($ft0['folios']) == 0) {
            echo "No hay ventas tasa cero que reportar.";
            exit;
        }

        //Desglose de iva del total de la venta al publico en general
        list($iva, $importe, $subtotal, $en_palabras)                = desglosa_iva($total, $tasa);

        //Procesamos la fecha del corte
        $fecha_factura = str_replace(" ", "T", $corte->getCreatedAt());

        //Generamos el formato para el domicilio del emisor.
        $domEmisor = array();

        if (sfConfig::get('app_fiscales_calle')        != '') $domEmisor['calle']        = sfConfig::get('app_fiscales_calle');
        if (sfConfig::get('app_fiscales_noExterior')   != '') $domEmisor['noExterior']   = sfConfig::get('app_fiscales_noExterior');
        if (sfConfig::get('app_fiscales_noInterior')   != '') $domEmisor['noInterior']   = sfConfig::get('app_fiscales_noInterior');
        if (sfConfig::get('app_fiscales_colonia')      != '') $domEmisor['colonia']      = sfConfig::get('app_fiscales_colonia');
        if (sfConfig::get('app_fiscales_municipio')    != '') $domEmisor['municipio']    = sfConfig::get('app_fiscales_municipio');
        if (sfConfig::get('app_fiscales_estado')       != '') $domEmisor['estado']       = sfConfig::get('app_fiscales_estado');
        if (sfConfig::get('app_fiscales_pais')         != '') $domEmisor['pais']         = sfConfig::get('app_fiscales_pais');
        if (sfConfig::get('app_fiscales_codigoPostal') != '') $domEmisor['codigoPostal'] = sfConfig::get('app_fiscales_codigoPostal');

        $descuento   = '0.00';
        $confFactura = array(
            //Agregue xmlns, xmlns:xsi, xsi:schemaLocation y version
            'xmlns'              => "http://www.sat.gob.mx/cfd/2",
            'xmlns:xsi'          => "http://www.w3.org/2001/XMLSchema-instance",
            'xsi:schemaLocation' => "http://www.sat.gob.mx/cfd/2 cfdv2.xsd",
            'version'            => "2.0",
            'serie'              => $ffis->getSerie(),
            'folio'              => $folio,
            'fecha'              => $fecha_factura,
            'sello'              => "",
            'noAprobacion'       => $ffis->getNoAprobacion(),
            'anoAprobacion'      => $ffis->getAnoAprobacion(),
            'tipoDeComprobante'  => "ingreso",
            'formaDePago'        => "EL PAGO DE ESTA FACTURA (CONTRAPRESTACION) SE EFECTUARA EN UNA SOLA EXHIBICION, SI POR ALGUNA RAZON NO FUERA ASI, EMITIREMOS LOS COMPROBANTES DE LAS PARCIALIDADES RESPECTIVAS",
            'subTotal'           => number_format(trim($importe), 2, '.', ''),
            'total'              => number_format(trim($total), 2, '.', ''),
            'descuento'          => number_format($descuento, 2, '.', ''),
            'noCertificado'      => $ffis->getNumCertificado(),
            'certificado'        => '',
            'Emisor'             => array(
                'rfc'             => $corte->getCentro()->getRfc(),
                'nombre'          => $corte->getCentro()->getRazonSocial(),
                'DomicilioFiscal' => $domEmisor
            ),
            'Receptor'           => array(
                'rfc'       => "XAXX010101000",
                'nombre'    => "COMPROBANTE GLOBAL DE OPERACIONES CON PUBLICO EN GENERAL",
                'Domicilio' => array(
                    //'calle'        => "",
                    //'noExterior'   => "",
                    //'noInterior'   => "",
                    //'colonia'      => "",
                    //'municipio'    => "",
                    //'estado'       => "",
                    'pais'         => "MEXICO",
                    //'codigoPostal' => ""
                )
            ),
            //Agregue en arreglo para los impuestos
            'Impuestos'          => array(
                'Traslados' => array(
                    'Traslado' => array(
                        'impuesto' => "IVA",
                        'tasa'     => number_format($tasa, 2, '.', ''),
                        'importe'  => number_format(trim($iva), 2, '.', '')
                    )
                )
            )
        );

        //El arreglo de conceptos
        $conceptos   = array();
        $descripcion = "VENTA AL PUBLICO EN GENERAL, NOTAS DE VENTA FOLIOS:" . implode(', ', $ft0['folios']);
        $conceptos[] = array(
            'cantidad'      => "1.00",
            'descripcion'   => $descripcion,
            'valorUnitario' => number_format(trim($importe), 2, '.', ''),
            'importe'       => number_format(trim($importe), 2, '.', '')
        );

        return array($confFactura, $conceptos, $ffis);
    }

    /**
     * Genera e imprime el ticket de corte
     * @param sfEvent $ev
     * @return void
     */
    static public function ticketCorte(Corte $corte, $ordenes) {
        $ip  = Comun::ipReal();
        $id  = $corte->getId();

        $ctx = sfContext::getInstance();
        $ctx->getConfiguration()->loadHelpers('Partial');

        $ventas_categoria = CortePeer::ventasPorCategoria($corte->getId());

        //Pasamos el total de ventas tasa 0
        $aTot        = CortePeer::foliosTasa0($corte);
        $total_tasa0 = isset($aTot['total_tasa0']) ? $aTot['total_tasa0'] : 0;

        //Pasamos un arreglo con las tasas para cada orden
        $tasas = array();
        foreach ($ordenes as $orden) {
            $tasa0 = DetalleOrdenPeer::tasa_productos($orden);

            if ($tasa0) {
                $tasas[$orden->getId() ] = 0;
            } else {
                $tasas[$orden->getId() ] = $orden->getCentro()->getIva();
            }
        }







        /*
        * Reingenieria  Mako. SIN - ID RQ
        * Responsables:  (BP) Bet Gader Porcayo Juárez, (LGL) Luis Gonzales Linares
        * Fecha:        21-01-2015
        * Descripción:  Se añade opcion para configuración de Nuevas Impresoras
        */
        $Prefijo = @sfConfig::get('app_prefijo');
        switch ($Prefijo) {
            case 'oki':
                $ticket_resumen = get_partial('corte_caja/ticketCorte',
                                          array('ords'             => $ordenes,
                                                'cc'               => $corte,
                                                'ventas_categoria' => $ventas_categoria,
                                                'total_tasa0'      => $total_tasa0));
                $ticket_detalle = get_partial('corte_caja/ticketsVentas',
                                          array('ordenes' => $ordenes,
                                                'tasas'   => $tasas));
                break;
            case 'Ch':
            case 'TMT20II':
                /*
                * En este caso se incluye el tipo Ch TMT20II
                */
                $ticket_resumen = get_partial('corte_caja/ticket' . $Prefijo . 'Corte',
                                          array('ords'             => $ordenes,
                                                'cc'               => $corte,
                                                'ventas_categoria' => $ventas_categoria,
                                                'total_tasa0'      => $total_tasa0));
                $ticket_detalle = get_partial('corte_caja/tickets' . $Prefijo . 'Ventas',
                                          array('ordenes' => $ordenes,
                                                'tasas'   => $tasas));
                break;
            default:
                /*
                * En caso de no estar configurado el parámetro app_prefijo, o que no es alguno 
                * de los anteriores se configura con respecto a OKI, 
                */
                $ticket_resumen = get_partial('corte_caja/ticketCorte',
                                          array('ords'             => $ordenes,
                                                'cc'               => $corte,
                                                'ventas_categoria' => $ventas_categoria,
                                                'total_tasa0'      => $total_tasa0));
                $ticket_detalle = get_partial('corte_caja/ticketsVentas',
                                          array('ordenes' => $ordenes,
                                                'tasas'   => $tasas));

                error_log('ERROR GENERICO!!! No se ha configurado el parámetro PREFIJO o el valor NO EXISTE en la configuración. Fn=ticketCorte');
                break;
        }





        //Imprimirmos tickets resumen
        $nomtick = "/tmp/ticket_resumen" . $id . ".txt";
        $fp      = fopen($nomtick, "w+");

        fwrite($fp, $ticket_resumen);
        fclose($fp);

        //$res = exec("lp -h ".$ip." -d okipos $nomtick");
        //por duplicado
        //$res = exec("lp -h ".$ip." -d okipos $nomtick");

        //Imprimirmos tickets detalle
        $nomtick = "/tmp/ticket_detalle" . $id . ".txt";
        $fp      = fopen($nomtick, "w+");

        fwrite($fp, $ticket_detalle);
        fclose($fp);

        //$res = exec("lp -h ".$ip." -d okipos $nomtick");
        error_log("Termina ticketCorte");
    }
}

