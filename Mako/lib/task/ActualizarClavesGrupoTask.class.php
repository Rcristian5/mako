<?php
class ActualizatClavesGrupoTask extends sfPropelBaseTask {
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección' , 'propel'),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación', 'frontend'),
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'               ,  'tolu2' )
        ));

        $this->namespace           = 'mako';
        $this->name                = 'actualizar-claves-grupo';
        $this->briefDescription    = 'Actualiza las claves de los grupo';
        $this->detailedDescription = <<<EOF
 [mako:add-cursos-moodlle|INFO] Actualiza las claves de todos los grupos

  [./symfony mako:ejecuta-sync --env=prod |INFO]
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'])->getConnection();

        $this->logSection('Actualizando claves de grupos', '...');

        foreach (GrupoPeer::doSelect(new Criteria()) as $grupo) {
            $this->log('Anterior ' . $grupo->getClave());
            $grupo->setClave(GrupoPeer::generaClave($grupo->getCurso()->getClave(), $grupo->getCentro()->getAlias(), $grupo->getHorario()->getFechaInicio('d/m/Y'), $grupo->getId()));
            $grupo->save();
            $this->log('Nuevo ' . $grupo->getClave());
            GrupoPeer::grupoASync($grupo, 'grupo.modificado');
        }
    }
}
?>
