<?php
class ActualizarDescuentoBecaTask extends sfBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección' , 'propel'),
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'               ,  'tolu2' ),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación', 'frontend'),
            new sfCommandOption('idcentro'   , null, sfCommandOption::PARAMETER_REQUIRED, 'Identificador del centro'  , null),
            new sfCommandOption('alias'      , null, sfCommandOption::PARAMETER_REQUIRED, 'Alias de centro'           , null),
            new sfCommandOption('archivo'    , null, sfCommandOption::PARAMETER_REQUIRED, 'Archivo para cargar datos' , 'lista')
        ));

        $this->namespace           = 'mako';
        $this->name                = 'actualizar-descuento-beca';
        $this->briefDescription    = 'Actualiza la fecha de asginacion de las becas';
        $this->detailedDescription = <<<EOF
 [mako:add-cursos-moodlle|INFO] Crea el registro de sincronia de aluas para los centros

  [./symfony mako:ejecuta-sync --env=prod |INFO]
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'])->getConnection();

        $this->logSection('Leyendo el archivo', '…');

        $file = dirname(__FILE__) . DIRECTORY_SEPARATOR . $options['archivo'];

        $this->log($file);

        if (file_exists($file)) {
            //$centro = CentroPeer::retrieveByPK($options['alias']);
            $cc = new Criteria();
            $cc->add(CentroPeer::ALIAS, $options['alias']);

            $centro = CentroPeer::doSelectOne($cc);

            if ($centro) {
                $this->logSection('Actualizando lista en centro', $centro->getNombre());
                foreach (file($file) as $linea) {
                    $datosSocio = explode('|', $linea);
                    $criteria   = new Criteria;
                    $criteria->add(SocioPeer::USUARIO, trim($datosSocio[0]));

                    //$this->log('USUARIO: '.trim($datosSocio[0]));
                    //$this->log('FOLIO: '.trim($datosSocio[1]));
                    //$criteria->add(SocioPeer::FOLIO, trim($datosSocio[1]));

                    $socio = SocioPeer::doSelectOne($criteria);
                    //error_log($criteria->toString());

                    if ($socio) {
                        foreach ($socio->getAsignacionBecas() as $asiganacion) {
                            if ($asiganacion->getBecaId() == 128871703877899 && $asiganacion->getCentroId() == $centro->getId()) {
                                $asiganacion->setFechaOtorgado(date('Y-m-d H:i:s'));
                                $asiganacion->save();
                                $this->log('OK: Asiganacion para ' . $datosSocio[0] . ' actualizada');
                            } else {
                                $this->log('ERR: No tiene la beca asignada:' . $datosSocio[0]);
                            }
                        }
                    } else {
                        $this->log('NO: El socio no existe: ' . trim($datosSocio[0]));
                    }
                }
            }
            $this->log('El centro no existe');
        } else {
            $this->log('No se encontro el archivo con la lista de los socios para actualizar la beca');
        }
    }
}
?>
