<?php
/**
 * Realiza los respaldos de los comprobantes, facturas pues.
 * @author edgar
 *
 */
class respaldosComprobantesTask extends sfBaseTask
{
    /**
     * Path donde se encuentran los archivos de respaldo de facturas
     * @var string
     */
    static $pbacks = '/var/www/Repositorio/Mako/web/facturas/';

    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'Aplicacion', 'frontend'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'Ambiente',  'tolu2' ),
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'La conexion a la bd', 'propel')
        ));

        $this->namespace           = 'respaldos';
        $this->name                = 'facturas';
        $this->briefDescription    = 'Guarda los archivos de facturas de cada centro.';
        $this->detailedDescription = <<<EOF
[respaldos:facturas|INFO] Genera los respaldos de las facturas de cada centro, lo copia a central, borra la instancia vieja y realiza la nueva.
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        // Inicializamos la base de datos central
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'] ? $options['connection'] : null)->getConnection();

        //Inicializamos los contextos
        $configuration = ProjectConfiguration::getApplicationConfiguration($options['application'], $options['env'], true);
        $context       = sfContext::createInstance($configuration);
        $dispatcher    = $configuration->getEventDispatcher();

        chdir(self::$pbacks);

        $ret    = null;
        $arr    = array();
        $centro = CentroPeer::retrieveByPK(sfConfig::get('app_centro_actual_id'));

        exec("tar -czf /tmp/backup_facturas_" . $centro->getAlias() . ".tar.gz *.xml", $arr, $ret);

        if (!$ret) {
            error_log("Compresion ok, enviando al servidor central desde " . $centro->getAlias());
        } else {
            error_log("Error al comprimir archivo en: " . $centro->getAlias());
        }
    }
}
