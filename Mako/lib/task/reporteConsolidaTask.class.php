<?php
class reporteConsolidaTask extends sfBaseTask
{
    protected $arLog = '/var/log/mako/reporte_consolida.log';
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'La aplicacion', 'frontend'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente',  'tolu2' ),
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conexión', 'propel'),
            new sfCommandOption('autorango', null, sfCommandOption::PARAMETER_OPTIONAL, 'Indica si se debe determinar automaticamente el rango de fechas a procesar.', false),
            new sfCommandOption('inicio', null, sfCommandOption::PARAMETER_REQUIRED, 'Fecha inicia a consultar', date("Y-m-d")),
            new sfCommandOption('fin', null, sfCommandOption::PARAMETER_REQUIRED, 'Fecha final a consultar', date("Y-m-d")),
            new sfCommandOption('centro', null, sfCommandOption::PARAMETER_REQUIRED, 'Alias del centro', null)));

        $this->namespace           = 'reporte';
        $this->name                = 'consolida';
        $this->briefDescription    = 'Extrae de los centros los reportes de metas de cada uno.';
        $this->detailedDescription = <<<EOF
[reporte:consolida|INFO] Extrae de los centros los reportes de metas de cada uno, mediante la consulta del web service.

  [php symfony reporte:consolida --inicio=2011-01-01 --fin=2011-01-31|INFO]
  [php symfony reporte:consolida --inicio=2011-01-01 --fin=2011-01-31 --centro=tolu1|INFO]
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'] ? $options['connection'] : null)->getConnection();
        $configuration   = ProjectConfiguration::getApplicationConfiguration($options['application'], $options['env'], true);
        $context         = sfContext::createInstance($configuration);
        $fini            = $options['inicio'];
        $ffin            = $options['fin'];
        $auto            = $options['autorango'] ? true : false;
        $c               = new Criteria();

        if ($options['centro'] != null) {
            $centro                    = $this->centro($options['centro']);
            $this->extraeReporte($centro, $fini, $ffin, $auto);
        } else {
            $c->add(CentroPeer::ACTIVO, true);
            $c->add(CentroPeer::ID, sfConfig::get('app_central_id'), Criteria::NOT_EQUAL);
            foreach (CentroPeer::doSelect($c) as $centro) {
                $this->extraeReporte($centro, $fini, $ffin, $auto);
            }
        }
    }

    protected function centro($alias) {
        $c = new Criteria();
        $c->add(CentroPeer::ALIAS, $alias);
        return CentroPeer::doSelectOne($c);
    }

    protected function extraeReporte(Centro $centro, $fini, $ffin, $auto) {
        try {
            $cliente = @new SoapClient("http://" . $centro->getIp() . "/MakoApi.wsdl");
            $serv    = ReporteServidoresPeer::retrieveByPK($centro->getId());
            if ($serv != null) {
                $serv->delete();
            }
        }
        catch(Exception $e) {
            error_log("Error al conectar cliente de ws " . $e->getMessage(), 3, $this->arLog);
            echo ("Para centro: " . $centro->getAlias() . " Error al conectar cliente de ws " . $e->getMessage() . "\n");

            $serv = ReporteServidoresPeer::retrieveByPK($centro->getId());
            $hoy  = date("Y-m-d");

            $f    = $this->consultaUltimaFecha('ReporteMetas', $centro->getId());
            if ($f !== false && $auto) {
                list($desde, $hasta)       = $f;
            }
            if ($f === false) {
                echo "En " . $centro->getAlias() . " ya se ha generado reporte $clase para esta fecha. $desde\n";
                return;
            }
            if (!$desde) $desde = date("Y-m-d");
            if ($serv != null) {

                $serv->setUltimaFecha($desde);
                $serv->save();
            } else {
                $serv = new ReporteServidores();
                $serv->setCentro($centro);
                $serv->setUltimaFecha($desde);
                $serv->save();
            }

            return;
        }

        $reportes = array("ReporteMetas" => 'reporte_getReporteMetas', "ReporteVentasCategorias" => 'reporte_getReporteVentasCategorias', "DiasOperacion" => 'reporte_getReporteDiasOperacion', "OcupacionHoras" => 'reporte_getReporteOcupacionHoras', "EstatusSocios" => 'reporte_getReporteEstatusSocios', "ReporteActivos" => 'reporte_getReporteSociosActivos', "ReporteCategoriaCursos" => 'reporte_getReporteCategoriaCursos', "ReporteVentaCursos" => 'reporte_getReporteVentaCursos');
        echo "\n";
        //Estos no son de metas, son de indicadores, pero aprovechamos para poder ejecutarlos ya que son muy similares.
        foreach ($reportes as $clase => $mws) {
            try {
                if ($auto) {
                    $f     = $this->consultaUltimaFecha($clase, $centro->getId());
                    if ($f !== false && $auto) {
                        list($fini, $ffin)        = $f;
                    }
                    if ($f === false) {
                        echo "En " . $centro->getAlias() . " ya se ha generado reporte $clase para esta fecha. $fini\n";
                        continue;
                    }
                }
                $sres = $cliente->$mws($fini, $ffin);
                $res  = unserialize($sres);
                error_log("En centro: " . $centro->getAlias() . " => " . $centro->getId() . " se encuentran " . count($res) . " registros de $clase entre el $fini y el $ffin.", 3, $this->arLog);
                echo ("En centro: " . $centro->getAlias() . " => " . $centro->getId() . " se encuentran " . count($res) . " registros de $clase entre el $fini y el $ffin\n");

                $de  = count($res);
                $cnt = 0;
                foreach ($res as $aRep) {
                    $cnt++;
                    $reporte = new $clase();
                    $reporte->fromArray($aRep);
                    if ($clase == 'ReporteMetas' || $clase == 'DiasOperacion' || $clase == 'OcupacionHoras' || $clase == 'ReporteVentasCategorias') $reporte->setId(Comun::generaId());

                    if ($clase == 'ReporteMetas') ($reporte->getEntero() ? '' : $reporte->setEntero(false));

                    if ($clase == 'DiasOperacion') ($reporte->getOpera() ? '' : $reporte->setOpera(false));

                    try {
                        @$reporte->save();
                    }
                    catch(Exception $e) {
                        error_log("Duplicado: " . $reporte->getFecha("Y-m-d") . " En: " . $cnt . " de " . $de . " [$clase]: " . $e, 3, $this->arLog);
                        echo ("Duplicado: " . $reporte->getFecha("Y-m-d") . " En: " . $cnt . " de " . $de . " [$clase] " . $e->getMessage() . "\n");
                    }
                }
            }
            catch(Exception $e) {
                error_log("En centro: " . $centro->getAlias() . " => " . $centro->getId() . " Al ejecutar metodo del ws $mws. " . $e->getMessage(), 3, $this->arLog);
                error_log("En centro: " . $centro->getAlias() . " => " . $centro->getId() . " Error Al ejecutar metodo del ws $mws." . $e->getMessage() . " \n");
            }
        }
    }
    /**
     * Regresa el rango de fecha inicio fecha fin inicial correcto cuando se autogenera el reporte
     *
     * @param string Clase del modelo $clase
     * @param int $centro_id centro del cual se trata la consulta
     */
    function consultaUltimaFecha($clase, $centro_id) {

        $obj  = new $clase();
        $peer = $obj->getPeer();
        $r    = new ReflectionObject($peer);

        $c    = new Criteria();
        $c->addSelectColumn("max(fecha)");
        $c->add($r->getConstant("CENTRO_ID"), $centro_id);
        $pdoStmt = $peer->doSelectStmt($c);

        $d       = $pdoStmt->fetch();

        $ahora   = date("Y-m-d H:i:s");
        $tsAhora = strtotime($ahora);

        $inicio  = "2010-09-20";

        if ($d['max'] != '') {
            $inicio  = $d['max'];
            //Sumamos un dia para no duplicar la fecha de inicio ya reportada anteriormente.
            if (strtotime("+ 1 day", strtotime($inicio)) < date("U")) $inicio  = date("Y-m-d", strtotime("+ 1 day", strtotime($inicio)));
        }
        //Si todavía no dan las nueve de la noche, restamos un día para no generar reportes con datos a medias. Puesto que no se ha realizado el corte de caja.
        if ($tsAhora < strtotime(date("Y-m-d") . " 21:00:00")) {
            $tsAhora = strtotime("- 1 day", $tsAhora);
            $fin     = date("Y-m-d", $tsAhora);
        } else {
            $fin     = date("Y-m-d");
        }
        //echo($d['max']." Rango que se va a generar: $inicio al $fin\n");
        //Preguntamos si el ultimo periodo es el mismo que el que se pretende generar, si es asi se regresa falso
        if ($d['max'] == $fin) {
            return false;
        }
        //De lo contrario regresamos el arreglo inicio y fin
        return array($inicio, $fin);
    }
}

