<?php
class CorreccionPermisosTask extends sfBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'               ,  'tolu2' ),
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección' , 'propel'),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación', 'frontend')
        ));

        $this->namespace           = 'mako';
        $this->name                = 'correccion-permisos';
        $this->briefDescription    = 'Corrige los permisos necesarios en los directorios del proyecto';
        $this->detailedDescription = <<<EOF
The [mako:correccion-permisos|INFO] Corrige los permisos necesarios en los directorios del proyecto:

  [./symfony mako:correccion-permisos --env=prod |INFO]
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        exec('chmod 775 -R ' . sfConfig::get('sf_root_dir') . '/apps');
        exec('chmod 775 -R ' . sfConfig::get('sf_root_dir') . '/lib');
        exec('chmod 775 -R ' . sfConfig::get('sf_root_dir') . '/web');
        exec('chmod 777 '    . sfConfig::get('sf_root_dir') . '/web/fotos');
        exec('chmod 777 '    . sfConfig::get('sf_root_dir') . '/web/tickets');
        exec('chmod 777 '    . sfConfig::get('sf_root_dir') . '/web/facturas');

        exec('chmod 775 -R ' . sfConfig::get('sf_root_dir') . '/llaves');

        exec('chmod 777 ' . sfConfig::get('sf_root_dir') . '/cache');

        error_log("Permisos corregidos");
    }
}
?>