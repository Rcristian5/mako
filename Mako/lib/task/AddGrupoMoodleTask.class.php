<?php
class AddGrupoMoodleTask extends sfPropelBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección' , 'propel'),
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'               ,  'tolu2' ),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación', 'frontend')
        ));

        $this->namespace           = 'mako';
        $this->name                = 'add-grupos-moodle';
        $this->briefDescription    = 'Crea grupos en moodle';
        $this->detailedDescription = <<<EOF
 [mako:add-cursos-moodlle|INFO] Crea los grupos en moodle que no fue posible crear
     durante el proceso de calendarización

  [./symfony mako:ejecuta-sync --env=prod |INFO]
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'])->getConnection();
        $c               = new Criteria();

        $c->add(CursoPeer::ASOCIADO_MOODLE, true);
        $c->add(GrupoPeer::REGISTRADO_MOODLE, false);
        $c->addJoin(GrupoPeer::CURSO_ID, CursoPeer::ID);

        $this->logSection('Creando lista de grupos asociados y que no fue posible crear en moodle', '');

        foreach (GrupoPeer::doSelect($c) as $grupo) {
            $this->logSection('Creando ' . $grupo->getClave() . ' en Moodle:', GrupoPeer::publicarMoodle($grupo->getId()));
        }

        $this->logSection('Se termino de cargar la lista de grupo asociados', '');
    }
}
?>