<?php
class EliminaSesionSocioTask extends sfBaseTask
{
    //Comando para ejecutar en pc
    private $cmd = '/usr/bin/perl /opt/MakoScripts/ejecuta_pc.pl ';

    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'                          ,  'tolu2' ),
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección'            , 'propel'),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación'           , 'frontend'),
            new sfCommandOption('centro'     , null, sfCommandOption::PARAMETER_OPTIONAL, 'Centro donde se realiza la operación' , null),
            new sfCommandOption('aula'       , null, sfCommandOption::PARAMETER_OPTIONAL, 'Aula donde se realiza la operacion'   , null),
            new sfCommandOption('ip'         , null, sfCommandOption::PARAMETER_OPTIONAL, 'IP donde se queire eliminar la sesión', null),
            new sfCommandOption('excepto'    , null, sfCommandOption::PARAMETER_OPTIONAL, 'Excepto esta aula'                    , null)
        ));

        $this->namespace           = 'mako';
        $this->name                = 'elimina-sesion';
        $this->briefDescription    = 'Elimina la sesión del socio en una PC.';
        $this->detailedDescription = <<<EOF
    [mako:elimina-sesion|INFO]  Elimina la sesión del socio en una PC o en varias, Puede ser por centro dado, o aula o una ip en concreto.

  [mako:elimina-sesion --centro=tolu1 |INFO]
  [mako:elimina-sesion --ip="10.1.0.98" |INFO]
  [mako:elimina-sesion --centro=tolu1 --aula="Aula 1" |INFO]
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'] ? $options['connection'] : null)->getConnection();

        $configuration = ProjectConfiguration::getApplicationConfiguration('frontend', $options['env'], true);
        $context       = sfContext::createInstance($configuration);

        print $this->formatter->format("--- Inicio de ejecucion " . date("Y-m-d H:i:s") . " ---", 'INFO') . "\n";

        if ($options['centro'] == null && $options['aula'] == null && $options['ip'] == null) {
            $cn    = CentroPeer::retrieveByPK(sfConfig::get('app_centro_actual_id'));
            $alias = $cn->getAlias();
            $this->apagarCentro($alias, $options['excepto']);

        } elseif ($options['centro'] == null && $options['aula'] == null && $options['ip'] != null) {
            $this->apagarPC($options['ip']);

        } elseif ($options['centro'] != null && $options['aula'] != null && $options['ip'] == null) {
            $this->apagarAula($options['centro'], $options['aula']);

        } elseif ($options['centro'] != null && $options['aula'] == null && $options['ip'] == null) {
            $this->apagarCentro($options['centro'], $options['excepto']);
        }

        print $this->formatter->format("--- FIN de ejecucion " . date("Y-m-d H:i:s") . " ---", 'INFO') . "\n";
    }

    function getSeccionIdPorNombre($centro_id, $nombre) {
        if (trim($nombre) == '') $nombre = "RECEPCION";

        $c = new Criteria();
        $c->add(SeccionPeer::NOMBRE, strtoupper($nombre));
        $c->add(SeccionPeer::CENTRO_ID, $centro_id);
        $s = SeccionPeer::doSelectOne($c);
        if ($s != null) {
            return $s->getId();
        }
        return 0;
    }

    function apagarAula($centro, $aula) {
        $c = new Criteria();
        $c->add(CentroPeer::ALIAS, $centro);

        $cen        = CentroPeer::doSelectOne($c);
        $seccion_id = $this->getSeccionIdPorNombre($cen->getId(), $aula);

        if ($cen != null && $seccion_id != 0) {
            $centro_id  = $cen->getId();

            $c2 = new Criteria();
            $c2->add(ComputadoraPeer::CENTRO_ID, $centro_id);
            $c2->add(ComputadoraPeer::SECCION_ID, $seccion_id);

            $res = ComputadoraPeer::doSelect($c2);

            foreach ($res as $pc) {
                echo "Apagando sesiones en $centro y $aula: " . $pc->getIp() . "\n";
                exec($this->cmd . " " . $pc->getIp());
            }
        }
    }

    function apagarCentro($centro, $excepto = 'RECEPCION') {
        $c = new Criteria();
        $c->add(CentroPeer::ALIAS, $centro);

        $cen = CentroPeer::doSelectOne($c);

        if ($cen != null) {
            $centro_id = $cen->getId();

            $c2 = new Criteria();
            $c2->add(ComputadoraPeer::CENTRO_ID, $centro_id);

            $res = ComputadoraPeer::doSelect($c2);
            foreach ($res as $pc) {
                if ($excepto != '') {
                    $seccion_id = $this->getSeccionIdPorNombre($centro_id, $excepto);
                    if ($seccion_id == $pc->getSeccionId()) {
                        continue;
                    }

                    echo "Apagando sesiones: Centro: $centro -> " . $pc->getIp() . "\n";
                    $this->apagarPC($pc->getIp());
                }
            }
        }
    }

    function apagarPC($ip) {
        $c = new Criteria();
        $c->add(ComputadoraPeer::IP, $ip);

        $compu = ComputadoraPeer::doSelectOne($c);
        if ($compu != null) {
            //Hacemos ping:
            $rp = exec("/usr/bin/perl /opt/MakoScripts/ping.pl " . $ip);

            //Si recibimos 0 es que esta apagada la maquina
            if ($rp == 0) {
                print "$ip " . $this->formatter->format(" PC apagada ", 'ERROR') . "\n";
                return;

            } else {
                echo exec($this->cmd . " " . $compu->getIp() . ' "restart gdm"') . "\n";
                return;
            }
        }
    }
}
?>