<?php
class makoRecursarTask extends sfBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'Ambiente',  'tolu2' ),
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la la conexión a la base', 'propel'),
            new sfCommandOption('grupo-id', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre del grupo en el que realizó el curso que se desea recursar', null),
            new sfCommandOption('usuario', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de usuario.', null)
        ));

        $this->namespace           = 'mako';
        $this->name                = 'recursar';
        $this->briefDescription    = 'Permite que se recurse un curso, dado el usuario y grupo donde se cursó.';
        $this->detailedDescription = <<<EOF
[mako:recursar|INFO] Finaliza un curso y lo pone con estatus de desertado, también cancela los pagos que haya pendientes.

  [php symfony mako:recursar --usuario=nombre.usuario --grupo-id=12309090|INFO]
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'] ? $options['connection'] : null)->getConnection();
        $grupo_id        = $options['grupo-id'];
        $usuario         = $options['usuario'];
        $grupo           = GrupoPeer::retrieveByPK($grupo_id);

        $c2 = new Criteria();
        $c2->add(SocioPeer::USUARIO, $usuario);

        $socio = SocioPeer::doSelectOne($c2);

        //$grupo = new Grupo();

        if ($grupo != null && $socio != null) {
            $curso_id = $grupo->getCursoId();
            $socio_id = $socio->getId();
            $grupo_id = $grupo->getId();

            $cursoFinalizado = new CursosFinalizados();
            $cursoFinalizado->setId(Comun::generaId());
            $cursoFinalizado->setCursoId($curso_id);
            $cursoFinalizado->setAlumnoId($socio_id);
            $cursoFinalizado->setGrupoId($grupo_id);
            $cursoFinalizado->setEstatusCursoFinalizadoId(3);
            $cursoFinalizado->save();

            error_log("Curso_id: " . $curso_id . " Socio_id: " . $socio_id . " grupo_id: " . $grupo_id);
            //Ahora vamos a cancelar los pagos. Para esto necesitamos saber el orden pago.
            $c3 = new Criteria();
            $c3->add(AlumnoPagosPeer::SOCIO_ID, $socio_id);
            $c3->add(AlumnoPagosPeer::CURSO_ID, $curso_id);
            $c3->add(AlumnoPagosPeer::GRUPO_ID, $grupo_id);
            $alumnoPago      = AlumnoPagosPeer::doSelectOne($c3);
            if ($alumnoPago != null) {
                //Obtenemos el orden_id de este pago
                $orden           = $alumnoPago->getOrden();
                $sub_producto_de = $alumnoPago->getSubProductoDe();
                if ($orden != null) {
                    //Obtenemos los detalle_orden_id relacionados con esta orden
                    $detalleOrdenes  = $orden->getDetalleOrdens();
                    foreach ($detalleOrdenes as $detalleOrden) {
                        foreach ($detalleOrden->getSocioPagoss() as $socioPago) {

                            $orden_base_id   = $socioPago->getOrdenBaseId();
                            $c4              = new Criteria();
                            $c4->add(SocioPagosPeer::ORDEN_BASE_ID, $orden_base_id);
                            $c4->add(SocioPagosPeer::SUB_PRODUCTO_DE, $sub_producto_de);

                            $aSocioPagos = SocioPagosPeer::doSelect($c4);
                            foreach ($aSocioPagos as $sp) {
                                if ($sp->getPagado() == false) {
                                    error_log("\tID: " . $sp->getId() . " cancelado: " . ($sp->getCancelado() ? 'SI' : 'NO') . " DO " . $sp->getDetalleOrdenId() . " SID " . $sp->getSocioId() . " BASE ORDEN " . $sp->getOrdenBaseId());
                                    $sp->setCancelado(true);

                                    $sp->save();
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
