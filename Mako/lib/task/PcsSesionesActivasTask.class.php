<?php

class PcsSesionesActivasTask extends sfBaseTask
{
    private $path_log = '/var/log/mako/task-sesiones-pc.log';

    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'               , 'prod'),
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección' , 'propel'),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación', 'frontend'),
            new sfCommandOption('accion'     , null, sfCommandOption::PARAMETER_OPTIONAL, 'Accion que se ejcutara'    , '')
        ));

        $this->namespace           = 'mako';
        $this->name                = 'pcs-sesiones-activas';
        $this->briefDescription    = 'Pcs watcher. Vigila la coherencia de sesiones en pcs y actúa dependiendo del intervalo de tiempo desde la ultima actualización.';
        $this->detailedDescription = <<<EOF
The [mako:pcs-sesiones-activas|INFO] Pcs sesiones activas:

  [./symfony mako:pcs-sesiones-activas --env=prod --accion=|INFO]
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {


        $databaseManager = new sfDatabaseManager($this->configuration);
        $configuration   = ProjectConfiguration::getApplicationConfiguration('frontend', $options['env'], true);
        $context         = sfContext::createInstance($configuration);
        $dispatcher      = $configuration->getEventDispatcher();

         $this->actualizaSesionesActivas($dispatcher);

    }

     /**
   * Actualiza sesion en BD basado en tiempo transcurrido desde la ultima peticion.
   * @param $configuration
   */
  private function actualizaSesionesActivas($dispatcher) {

      $datetimeactual = new DateTime('NOW');

      $c = new Criteria();
      $sesiones = SesionPeer::doSelect($c);

      foreach ($sesiones as $s) {

          $datetimebd = new DateTime($s->getUptime());
          $interval = $datetimeactual->diff($datetimebd);

          error_log($datetimeactual->format('Y-m-d H:i:s') . " IP: " . $s->getIp() . " UPTIME: " . $s->getUptime()."\n", 3, $this->path_log);
          error_log(" Minutos trancurridos desde la ultima actualizacion para ip: ".$s->getIp()." ->" . $interval->i." minutos\n", 3, $this->path_log);

          if ($interval->i >= 5){
              error_log("Se elimina sesion para " . $s->getIp()."\n", 3, $this->path_log);
              $ajuste = self::obtenerSegundosACompensar($s);

              $h = self::eliminaSesion_agente($s, $ajuste);
              $dispatcher->notify(new sfEvent($this, 'socio.logout', array('sesion_historico' => $h)));
          }
      }

    }

   /**
   * Obtiene segundos compensacion para el usuario.
   * @param $configuration
   */
  private function obtenerSegundosACompensar($sesion) {

      $dateahora = new DateTime('NOW');

      $dateuptime = new DateTime($sesion->getUptime());
      
      $diferenciaensegundos = $dateuptime->getTimestamp() - $dateahora->getTimestamp();

      error_log("Segundos a compensar: " . $diferenciaensegundos."\n", 3, $this->path_log);

      return $diferenciaensegundos;

    }

     /**
     * Elimina la sesión y genera una entrada para el histórico además de actualizar los tiempos de sesión.
     * @param Sesion $sesion
     * @param int $ajuste Segundos de ajuste para compensar tiempo.
     * @return SesionHistorico Regresa el registro de sesion histórico que corresponde a la sesion eliminada
     */
     private function eliminaSesion_agente($sesion, $ajuste)
    {
            $path_log = '/var/log/mako/rest-sesiones-pc.log';

            $tlogin = $sesion->getFechaLogin(null);
            error_log("elimina sesion ajuste " . $ajuste. "\n", 3, $path_log);
            //Generamos el historico de sesion
            $h = new SesionHistorico();
            $h->setCentroId($sesion->getCentroId());
            $h->setSocioId($sesion->getSocioId());
            $h->setUsuario($sesion->getUsuario());
            $h->setIp($sesion->getIp());
            $h->setComputadoraId($sesion->getComputadoraId());
            $h->setTipoId($sesion->getTipoId());
            $h->setFechaLogin($sesion->getFechaLogin());
            $ahora = time() + $ajuste;
            error_log("elimina sesion ahora " . print_r($ahora,true). "\n", 3, $path_log);

            $transcurrido = abs($ahora - $tlogin->format('U'));

            $h->setFechaLogout($ahora);
            $h->setOcupados($transcurrido);
            $h->setSaldoInicial($sesion->getSaldo());

            //Vemos el tipo de sesion que se trata para restar o no su saldo
            $tipo_id = $sesion->getTipoId();

            if($tipo_id == sfConfig::get('app_sesion_default_id') || $tipo_id == sfConfig::get('app_sesion_multisesion_id'))
            {
                    //actualizamos el saldo de tiempo del socio
                    $socio = $sesion->getSocio();
                    $nsaldo = ($socio->getSaldo() - $transcurrido);
                    if($nsaldo < 0 ) $nsaldo = 0;
                    $socio->setSaldo($nsaldo);
                    $socio->save();
                    $h->setSaldoFinal($nsaldo);
            }
            else
            {
                  //Si la sesion es de no consumo el saldo final queda igual
                    $h->setSaldoFinal($sesion->getSaldo());
            }
            try {
                    $h->save();
                    $sesion->delete();
            }
            catch(Exception $ex)
            {
                    error_log("Error[SesionPeer]: al tratar de guardar sesion historica: ".$ex->getMessage(). "\n", 3, $path_log);
                    $sesion->delete();
            }
            return $h;
    }
}
?>