<?php
class SeguimientoMetasDBLinkTask extends sfPropelBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección', 'propel'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente',  'tolu2' ),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación', 'frontend'),
            new sfCommandOption('desde', null, sfCommandOption::PARAMETER_OPTIONAL, 'Parametro requerido para indicar a partir de que fecha se requiere generar el reporte'),
            new sfCommandOption('hasta', null, sfCommandOption::PARAMETER_OPTIONAL, 'Parametro opcional para indicar hasta que fecha se obtendran las calificaciones'),
            new sfCommandOption('override', null, sfCommandOption::PARAMETER_OPTIONAL, 'Parametro opcional para indicar si se debee hacer override a las calificaciones guardadas', 0)
        ));

        $this->namespace           = 'mako';
        $this->name                = 'seguimiento-metas-dblink';
        $this->briefDescription    = 'Obtiene los registros por dblink de los indicadores';
        $this->detailedDescription = <<<EOF
 [mako:add-cursos-moodlle|INFO] Obtiene los registro mediante dblink de los indicadores de Mako

  [./symfony mako:seguimiento-metas-dblink --env=prod |INFO]
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'])->getConnection();

        sfContext::createInstance($this->configuration);

        // Seleccionamos todos los centros
        $criteria = new Criteria();
        $criteria->add(CentroPeer::ACTIVO, true);
        $criteria->add(CentroPeer::ID, 9999, Criteria::LESS_THAN);
        $criteria->addAscendingOrderByColumn(CentroPeer::ID);
        foreach (CentroPeer::doSelect($criteria) as $centro) {
            $this->log($centro->getAlias());
            // Primero obtenemos la fechas desde la que no se tienen los reportes del centro
            $criteriaUltimoReporte = new Criteria();
            $criteriaUltimoReporte->add(ReporteIndicadoresDiaPeer::CENTRO_ID, $centro->getId());
            $criteriaUltimoReporte->addDescendingOrderByColumn(ReporteIndicadoresDiaPeer::FECHA);
            $ultimaFecha = ReporteIndicadoresDiaPeer::doSelectOne($criteriaUltimoReporte);
            $actualizado = true;
            // Obtenemos los datos de todos los grupos que finalizan desde la ultima fecha a la fecha actual
            try {
                $sql = "SELECT *
                FROM
                dblink ('dbname=mako host=mako-" . $centro->getAlias() . " user=postgres',
                'SELECT
                grupo.id as id
                FROM
                grupo, horario
                WHERE
                grupo.horario_id = horario.id AND
                horario.fecha_fin BETWEEN ''" . $ultimaFecha->getFecha('Y-m-d') . " 00:00:00'' AND ''" . date('Y-m-d') . " 23:59:59'' AND
                grupo.activo = true AND
                grupo.cupo_actual > 0 AND
                grupo.centro_id = " . $centro->getId() . ";'
                )
                AS G1(id  bigint);";

                $connection  = Propel::getConnection(ReporteIndicadoresDiaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
                $statement   = $connection->prepare($sql);
                $statement->execute();
                $gruposFinalizan = array();
                $gruposIn        = '';
                while ($grupos          = $statement->fetch(PDO::FETCH_ASSOC)) {
                    $gruposFinalizan[]                 = $grupos['id'];
                    $gruposIn.= $grupos['id'] . ',';
                }
            }
            catch(Exception $e) {
                error_log($e->getMessage());
                $cuerpo = 'Error al tratar de obtener la lista de grupos finalizados en <b>' . $centro->getAlias() . '</b> <br>
                Mensaje: <br>' . $e->getMessage();
                $this->enviarCorreo($cuerpo);
            }
            // Si hya grupos que finalizan, buscamos que se hallan finalizado ya
            if (count($gruposFinalizan) > 0) {
                // Borramos la ultima coma del arreglo
                $gruposIn   = trim($gruposIn, ',');
                try {
                    $sql        = "
                    SELECT *
                    FROM
                    dblink ('dbname=mako host=mako-" . $centro->getAlias() . " user=postgres',
                    'SELECT
                    created_at
                    FROM
                    cursos_finalizados
                    WHERE
                    grupo_id IN (" . $gruposIn . ")
                    ORDER BY created_at DESC
                    LIMIT 1
                    ;'
                    )
                    AS D1(created_at  timestamp);";
                    $connection = Propel::getConnection(ReporteIndicadoresDiaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
                    $statement  = $connection->prepare($sql);
                    $statement->execute();
                    $fechaComparar   = false;
                    while ($fechaFinalizado = $statement->fetch(PDO::FETCH_ASSOC)) {
                        $fechaComparar   = explode(' ', $fechaFinalizado['created_at']);
                    }
                }
                catch(Exception $e) {
                    error_log($e->getMessage());
                    $cuerpo = 'Error al tratar de obtener la ultima fecha de grupos finalizados en <b>' . $centro->getAlias() . '</b> <br>
                    Mensaje: <br>' . $e->getMessage();
                    $this->enviarCorreo($cuerpo);
                }
                // Verificamos que la ultima vez que se halla finalizado un grupo sea igual a la fecha hasta la que se obtendra el reporte
                if (!$fechaComparar || $fechaComparar[0] != date('Y-m-d')) {
                    $actualizado = false;
                    error_log('No se han finalizado todos los grupos');
                    $cuerpo = 'No se han finalizdo todos los grupos  en <b>' . $centro->getAlias() . '</b>';
                    $this->enviarCorreo($cuerpo);
                }
            }
            /*
             if($actualizado)
             {
            $sql = "";
            $connection = Propel::getConnection(ReporteIndicadoresDiaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
            $statement = $connection->prepare($sql);
            $statement->execute();
            $pagosPagados = array();

            while($pagos = $statement->fetch(PDO::FETCH_ASSOC))
            {

            }
            }
            */
        }
    }

    public function enviarCorreo($cuerpo) {
        $transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')->setUsername('bixit.s.a.c.v@gmail.com')->setPassword('B1x1tM41l!');
        $mailer    = Swift_Mailer::newInstance($transport);
        $mensaje   = Swift_Message::newInstance()->setFrom(array('bixit.s.a.c.v@gmail.com' => 'BiXIT'))
                                                 ->setTo('fs@enova.mx')
                                                 ->setSubject('[MAKO] Error al obtener indicadores')
                                                 ->setBody($cuerpo, 'text/html');
        $result    = $mailer->batchSend($mensaje);
    }
}
?>

