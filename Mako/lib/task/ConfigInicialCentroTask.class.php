<?php
class ConfigInicialCentroTask extends sfBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'               ,  'tolu2' ),
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección' , 'propel'),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación', 'frontend'),
            new sfCommandOption('centro', null, sfCommandOption::PARAMETER_OPTIONAL, 'Alias del centro.', null)
        ));

        $this->namespace           = 'centro';
        $this->name                = 'config-inicial';
        $this->briefDescription    = 'Ejecuta la configuración inicial para el centro.';
        $this->detailedDescription = <<<EOF
La [centro:config-inicial|INFO] Ejecuta la config inicial del centro tomando la plantilla de configuraciones y lee la info de mako-config:

  [./symfony centro:config-inicial --env=prod |INFO]
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $yaml        = new sfYamlParser();
        $conf_centro = $yaml->parse(file_get_contents('/etc/mako.conf'));
        $conf_app    = $yaml->parse(file_get_contents(sfConfig::get('sf_root_dir') . '/apps/frontend/config/app.yml'));

        $conf_app[ 'tolu2' ]['centro_actual']    = $conf_centro['centro']['centro_actual'];
        $conf_app[ 'tolu2' ]['centro_actual_id'] = $conf_centro['centro']['centro_actual_id'];

        $conf_app[ 'tolu2' ]['ldap_master_host'] = $conf_centro['centro']['ldap_master_host'];
        $conf_app[ 'tolu2' ]['ldap_master_rdn']  = $conf_centro['centro']['ldap_master_rdn'];
        $conf_app[ 'tolu2' ]['ldap_master_pwd']  = $conf_centro['centro']['ldap_master_pwd'];
        $conf_app[ 'tolu2' ]['prefijo']          = $conf_centro['centro']['prefijo'];
        $conf_app[ 'tolu2' ]['cpl']              = $conf_centro['centro']['cpl'];
        $conf_app[ 'tolu2' ]['ldap']             = $conf_centro['centro']['ldap_master_host'];

        $dumper = new sfYamlDumper();
        $yaml   = $dumper->dump($conf_app, 3);

        file_put_contents(sfConfig::get('sf_root_dir') . '/apps/frontend/config/app.yml', $yaml);

        chmod(sfConfig::get('sf_root_dir') . '/apps/frontend/config/app.yml', 775);
    }
}
?>