<?php
class RepporteAsistenciasTask extends sfPropelBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección', 'propel'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente',  'tolu2' ),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación', 'frontend'),
        ));

        $this->namespace           = 'mako';
        $this->name                = 'reporte-asistencias';
        $this->briefDescription    = 'Guarda el reporte de asistencias';
        $this->detailedDescription = <<<EOF
 [mako:add-cursos-moodlle|INFO] Genera el reporte de asistencias, del dia actual

  [./symfony mako:reporte-asistencias --env=prod |INFO]
EOF;
    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'] ? $options['connection'] : null)->getConnection();

        $this->logSection('Obtenieno reporte de asistencias, centro:', sfConfig::get('app_centro_actual'));
        // Gurdamos el reporte de sistencias por cada curso
        foreach (CursoPeer::doSelect(new Criteria()) as $curso) {
            $reporte = ReporteAsistenciasPeer::guardaReporteAsistencias(sfConfig::get('app_centro_actual_id'), $curso->getId(), date('Y-m-d'));
            if($reporte['registrados'] != 0) {
                $this->logSection('CURSO', $curso->getNombre());
            }
        }
        $this->logSection('Finalizado..', '');
    }
}