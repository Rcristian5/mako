#!/usr/bin/perl
#Se agrega ruta para hacer uso del DBI-My_DB
use lib '/usr/local/bin';
use My_DB;
use Encode qw(decode encode);
use POSIX qw(strftime);

#Usamos strict y warnings para obligarnos a una buena estructura en el programa
use strict;
use warnings;

#Inicializamos la variable que contendra la información de sincronización
my %datos;

#Configuración inicial para conexión con Mako central
my %conf = (
  servicio   => 'PostgreSQL',
  DB         => 'prueba_tolu2',
  host       => 'localhost',
  port       => 5432,
  user       => 'postgres',
  passwd     => '',
  autocommit => 0,
);

my $dbh = mi_dbh(\%conf);

#print "\nInicia ejecucuión de query";

#Realizamos query que nos regresa los registros atorados en la sincronia
my $registros_sincronia = query($dbh,
                "
SELECT 
	sap.id as id_sincronia,
	sap.pk_referencia as orden,
	sap.operacion,
	sap.completo as estatus,
	sap.respuesta as ultimo_mensaje,
	sap.intentos as numero_intentos,
    sap.fecha_sincronia as ultimo_intento,
	sap.created_at as fecha_creacion FROM sincronia_sap sap
WHERE	
	NOT sap.completo AND sap.intentos>5
ORDER BY sap.id;
",
               );

#Nos desconectamos de la Base de Datos.
$dbh->disconnect;
my $rc = $registros_sincronia->finish;

#my $local_ip_address = get_local_ip_address();
my $local_ip_address  = eval {Net::Address::IP::Local->public};


#print "\nIp del centro\n";
#print $local_ip_address;

#Obtenemos centro actual
my $centro_sincronia = $dbh->prepare("SELECT alias FROM centro WHERE ip = ?");

$centro_sincronia->execute($local_ip_address);

#print "\nTermina ejecución de query\n";
my $centro = $centro_sincronia->{alias};


#Variable utilizada para intercalar el color de las filas en la tabla generada
my $background = '';

#Recorremos cada uno de los registros que nos regresa la consulta para colocarlos en la tabla
foreach my $sincronia_alerta ( @$registros_sincronia ) {
    #print "Entra for";
    #print $sincronia_alerta->{id_sincronia};    
    #Sincronizamos en $datos{msg} los valores de cada fila de la tabla <tr><td></td></tr>
    $datos{msg} .= "<tr style=\"color:#000000;\">
    <td style=\"font-size:8pt; text-align:left; border:1px solid #FF0000; padding:3px 7px 2px 7px;$background\">$sincronia_alerta->{orden}</td>
    <td style=\"font-size:8pt; text-align:left; border:1px solid #FF0000; padding:3px 7px 2px 7px;$background\">$sincronia_alerta->{operacion}</td>
    <td style=\"font-size:8pt; text-align:left; border:1px solid #FF0000; padding:3px 7px 2px 7px;$background\">$sincronia_alerta->{estatus}</td>
    <td style=\"font-size:8pt; text-align:left; border:1px solid #FF0000; padding:3px 7px 2px 7px;$background\">$sincronia_alerta->{ultimo_mensaje}</td>
    <td style=\"font-size:8pt; text-align:left; border:1px solid #FF0000; padding:3px 7px 2px 7px;$background\">$sincronia_alerta->{numero_intentos}</td>
    <td style=\"font-size:8pt; text-align:left; border:1px solid #FF0000; padding:3px 7px 2px 7px;$background\">$sincronia_alerta->{ultimo_intento}</td>
    <td style=\"font-size:8pt; text-align:left; border:1px solid #FF0000; padding:3px 7px 2px 7px;$background\">$sincronia_alerta->{fecha_creacion}</td>
    </tr>\n";
    $background = $background ? '' : ' background-color:#000000;';
}

#Concatenamos en la variable $datos{msg} el encabezado de la tabla
$datos{msg} = "<html>
<head>

</head>
<h3 style=\"color: #000000; font-size: 8pt\">Errores en la sincronización SAP</h3>
<br/>
<h3 style=\"color: #000000; font-size: 8pt\">Sincronias pendientes con más de 5 intentos fallidos.</h3>
<table cellspacing=\"0\" style=\"font-family:\"Trebuchet MS\", Arial, Helvetica, sans-serif; width:100%; border-collapse:collapse;\">
<tr>
    <th style=\"font-size:8pt; text-align:left; padding-top:5px; padding-bottom:4px; background-color:#000000; color:#ffffff; border:1px solid #FF0000;\">Número de orden</td>
    <th style=\"font-size:8pt; text-align:left; padding-top:5px; padding-bottom:4px; background-color:#000000; color:#ffffff; border:1px solid #FF0000;\">Operación</td>
    <th style=\"font-size:8pt; text-align:left; padding-top:5px; padding-bottom:4px; background-color:#000000; color:#ffffff; border:1px solid #FF0000;\">Estatus</td>
    <th style=\"font-size:8pt; text-align:left; padding-top:5px; padding-bottom:4px; background-color:#000000; color:#ffffff; border:1px solid #FF0000;\">Último mensaje</td>
    <th style=\"font-size:8pt; text-align:left; padding-top:5px; padding-bottom:4px; background-color:#000000; color:#ffffff; border:1px solid #FF0000;\">Número de intentos</td>
    <th style=\"font-size:8pt; text-align:left; padding-top:5px; padding-bottom:4px; background-color:#000000; color:#ffffff; border:1px solid #FF0000;\">Ultimo intento</td>
    <th style=\"font-size:8pt; text-align:left; padding-top:5px; padding-bottom:4px; background-color:#000000; color:#ffffff; border:1px solid #FF0000;\">Fecha creación</td>
$datos{msg}
</table>
</html>\n" if $datos{msg}; #Se genera la tabla si existen datos en $datos{msg}

#Se agrega la validación de acentos
$datos{msg} =~ s/á/&aacute;/g;
$datos{msg} =~ s/Á/&Aacute;/g;
$datos{msg} =~ s/é/&eacute;/g;
$datos{msg} =~ s/É/&Eacute;/g;
$datos{msg} =~ s/í/&iacute;/g;
$datos{msg} =~ s/Í/&Iacute;/g;
$datos{msg} =~ s/ó/&oacute;/g;
$datos{msg} =~ s/Ó/&Oacute;/g;
$datos{msg} =~ s/ú/&uacute;/g;
$datos{msg} =~ s/Ú/&Uacute;/g;
$datos{msg} =~ s/ñ/&ntilde;/g;
$datos{msg} =~ s/Ñ/&Ntilde;/g;
$datos{msg} =~ s/ü/&uuml;/g;
$datos{msg} =~ s/Ü/&Uuml;/g;

#Mandamos llamar a la función que envia el correo y lo manda si existen datos
envia_correo(\%datos) if $datos{msg};
#envia_correo(\%datos);
exit 0;

#Hacemos uso del módulo Mail::Sender para obtener la funcionalidad de envio de correo
use Net::SMTP::SSL;
use MIME::Lite;
use POSIX;


sub envia_correo {
    my $datos = shift;
    my $fecha = strftime "[%d/%m/%Y %T] ", localtime;
 
    #Mandamos a file.txt los logs
    open my $DEBUG, ">> ./log_centinela_operaciones_negocio.txt" or die "No se pudo abrir archivo log: $!\n";

   #Colocamos los parametros de configuración necesarios para la cuenta de centinela@enova.mx
    my $msg = MIME::Lite ->new (
        From =>'Centinela <centinela@ria.org.mx>',
        To =>'isaed.martinez@enova.mx',
	    #Cc =>'isaed.martinez@enova.mx',
        #Subject =>"Problemas sincronizacion $fecha - Centro $centro_sincronia",
        Subject =>"Problemas sincronizacion timbrado $fecha $centro",
        Data => "$datos{'msg'}",
        Type => 'text/html'
    );

#    my $USERNAME = 'centinela@enova.mx';
#    my $PASSWORD = 'Ughae8ia-867';
#    my $smtps = Net::SMTP::SSL->new("smtp.gmail.com", Port => 465, Debug=>1, Timeout=>30);
    
#    $smtps->auth ( $USERNAME, $PASSWORD ) or die "No se logro autenticar correo $!";
#    $smtps->mail('Centinela <centinela@ria.org.mx>');
#    $smtps->to('isaed.martinez@enova.mx');
#    $smtps->data();
#    $smtps->datasend( $msg->as_string() );
#    $smtps->dataend();
#    $smtps->quit;

warn "$@\n" if $@;
}
