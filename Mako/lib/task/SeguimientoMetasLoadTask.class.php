<?php
class SeguimientoMetasLoadTask extends sfPropelBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección', 'propel'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente',  'tolu2' ),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación', 'frontend'),
            new sfCommandOption('desde', null, sfCommandOption::PARAMETER_OPTIONAL, 'Fecha a partir de la cual se genera el reporte'),
            new sfCommandOption('hasta', null, sfCommandOption::PARAMETER_OPTIONAL, 'Fecha hasta la que se genera el reporte de la cual se genera el reporte'),
            new sfCommandOption('load', null, sfCommandOption::PARAMETER_OPTIONAL, 'Solo carga de datos', 0),
            new sfCommandOption('get', null, sfCommandOption::PARAMETER_OPTIONAL, 'Solo obtiene los archivos', 0),
            new sfCommandOption('tgz', null, sfCommandOption::PARAMETER_OPTIONAL, 'Obtiene el archivo tgz de cada centro', 1),
            new sfCommandOption('all', null, sfCommandOption::PARAMETER_OPTIONAL, 'todos los datos', 0),
            new sfCommandOption('archivo', null, sfCommandOption::PARAMETER_OPTIONAL, 'Solo carga de datos', false),
            new sfCommandOption('overwrite', null, sfCommandOption::PARAMETER_OPTIONAL, 'Sobreescritura de los datos guardados', 0)
        ));

        $this->namespace           = 'mako';
        $this->name                = 'seguimiento-metas-load';
        $this->briefDescription    = 'Genera el dump para el reporte de indicadores para la fase 3';
        $this->detailedDescription = <<<EOF
     [mako:add-cursos-moodlle|INFO] Genera el reporte de indicadores para la fase 3

      [./symfony mako:seguimiento-metas-load --env=prod |INFO]
EOF;

    }
    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'])->getConnection();

        $criteria = new Criteria();
        $criteria->add(CentroPeer::ACTIVO, true);

        // Si solo es carga de datos
        if ($options['get'] != 0) {
            $this->log('Creando dumps YMl por centro');
            foreach (CentroPeer::doSelect($criteria) as $centro) {
                $this->log('Centro: ' . $centro->getAlias());
                $opciones = '';

                // Determinamos si se necesitan todos los datos
                if ($options['all'] == 0) {
                    if ($options['desde'] && $options['hasta']) {
                        $opciones = ' --desde="' . $options['desde'] . '" --hasta="' . $options['hasta'] . '"';
                    } else {
                        // Buscamos la ultima fecha que se guardo el reporte
                        $criteriaIndicadoresDia = new Criteria();
                        $criteriaIndicadoresDia->add(ReporteIndicadoresDiaPeer::CENTRO_ID, $centro->getId());
                        $criteriaIndicadoresDia->addAscendingOrderByColumn(ReporteIndicadoresDiaPeer::CREATED_AT);
                        $fechaDia = ReporteIndicadoresDiaPeer::doSelectOne($criteriaIndicadoresDia);
                        if ($fechaDia) {
                            $opciones = ' --desde="' . $fechaDia->getCreatedAt('Y-m-d') . '" --hasta="' . date('Y-m-d') . '"';
                        }
                    }
                }

                if ($options['tgz'] != 0) {
                    $opciones.= ' --tgz=1';
                }
                try {
                    exec('ssh mako-' . $centro->getAlias() . ' /var/www/Repositorio/Mako/symfony mako:seguimiento-metas-dump ' . $opciones);
                }
                catch(Exception $e) {
                    $this->log('WARNING. El centro no responde');
                }
            }
            $this->log('Copiando dumps YMl de cada centro');
            foreach (CentroPeer::doSelect($criteria) as $centro) {
                $this->log('Centro: ' . $centro->getAlias());
                try {
                    error_log("scp mako-" . $centro->getAlias() . ":/tmp/reporte_indicadores_" . $centro->getAlias() . "." . ($options['tgz'] != 0 ? "tgz" : "yml") . " /tmp/");
                    exec("scp mako-" . $centro->getAlias() . ":/tmp/reporte_indicadores_" . $centro->getAlias() . "." . ($options['tgz'] != 0 ? "tgz" : "yml") . " /tmp/");
                    // Si se indico la opcion tgz hay que descomprimir el archivo una vez se  termino de copiar y borrar el comprimido
                    if ($options['tgz'] != 0) {
                        exec("tar -xf /tmp/reporte_indicadores_" . $centro->getAlias() . ".tgz -C /tmp/");
                        exec("rm /tmp/reporte_indicadores_" . $centro->getAlias() . ".tgz");
                    }
                }
                catch(Exception $e) {
                    $this->log('WARNING. El centro no responde');
                }
            }
        }
        if ($options['load'] != 0) {
            $overwrite = $options['overwrite'] == 0 ? false : true;
            $this->log('Cargando los datos');
            if (!$options['archivo']) {
                foreach (CentroPeer::doSelect($criteria) as $centro) {
                    $this->log('Centro: ' . $centro->getAlias());
                    $this->loadYMl("/tmp/tmp/reporte_indicadores_" . $centro->getAlias() . ".yml", $overwrite);
                }
            } else {
                $this->loadYMl($options['archivo'], $overwrite);
            }
        }
    }

    public function loadYMl($file, $overwrite = false) {
        try {
            error_log($file);
            $reporteI  = array();
            $reporteI  = sfYaml::load(file_get_contents($file));
            foreach ($reporteI as $clase  => $reportes) {
                $this->log('Cargando datos de: ' . $clase);
                foreach ($reportes as $key    => $reporte) {
                    $objeto = new $clase();
                    // Si los elemnteos del arreglo son otro arreglo es una relacion
                    error_log($key);
                    if (isset($reporte[0]) && is_array($reporte[0])) {
                        foreach ($reporte as $relacion) {
                            $objeto       = new $clase();
                            $indicador    = $objeto->getPeer()->retrieveByPK($relacion['Id'], (array_key_exists('ReporteIndicadorDiaId', $relacion) ? $relacion['ReporteIndicadorDiaId'] : $relacion['ReporteIndicadorDiaGrupoId']), $relacion['CentroId']);
                            if (!$indicador) {
                                // Si trae el identidicador del reporte por grupo y dia, verificacmos que exista el registro
                                if (array_key_exists('ReporteIndicadorDiaGrupoId', $relacion)) {
                                    $reporteGrupo = ReporteIndicadoresDiaGrupoPeer::retrieveByPK($relacion['ReporteIndicadorDiaGrupoId'], $relacion['CentroId']);
                                    if ($reporteGrupo) {
                                        $objeto->fromArray($relacion);
                                        $objeto->save();
                                        $this->log('OK');
                                    } else {
                                        $this->log('El padre grupo no existe, se omite');
                                    }
                                } else {
                                    $objeto->fromArray($relacion);
                                    $objeto->save();
                                    $this->log('OK');
                                }
                            } else {
                                if ($overwrite) {
                                    $this->log('Sobreescribiendo datos');
                                    $indicador->fromArray($relacion);
                                    $indicador->save();
                                    $this->log('OK');
                                } else {
                                    $this->log('El registro se guardo previamente, se omite');
                                }
                            }
                        }
                    } else {

                        $indicador = $objeto->getPeer()->retrieveByPK($reporte['Id'], $reporte['CentroId']);
                        if (!$indicador) {
                            // Si es un indicadro de grupo buscamos primero el grupo
                            if ($clase == 'ReporteIndicadoresDiaGrupo') {
                                $grupo     = GrupoPeer::retrieveByPK($reporte['GrupoId']);
                                if ($grupo) {
                                    $objeto->fromArray($reporte);
                                    $objeto->save();
                                } else {
                                    $this->log('El grupo no existe, se omite');
                                }
                            } else {
                                $objeto->fromArray($reporte);
                                $objeto->save();
                            }
                        } else {
                            if ($overwrite) {
                                $this->log('Sobreescribiendo datos');
                                $indicador->fromArray($reporte);
                                $indicador->save();
                                if (isset($reporte['RegistrosNuevos'])) {
                                    error_log('Antes: ' . $reporte['RegistrosNuevos']);
                                    error_log('Despues: ' . $indicador->getRegistrosNuevos());
                                }
                            } else {
                                $this->log('El registro se guardo previamente, se omite');
                            }
                        }
                    }
                }
            }
            error_log('Eliminando el archivo');
            exec("rm " . $file);
        }
        catch(Exception $e) {
            $this->log('WARNING. El archivo no se encuentra');
            error_log($e->getMessage());
        }
    }
}

