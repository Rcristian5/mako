<?php
class PruebaGruposCalendarizadosTask extends sfPropelBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'               ,  'tolu2' ),
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección' , 'propel'),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación', 'frontend')
        ));

        $this->namespace           = 'mako';
        $this->name                = 'prueba-grupos-calendarizados';
        $this->briefDescription    = 'Prueba los grupos calendarizados';
        $this->detailedDescription = <<<EOF
 [mako:add-cursos-moodlle|INFO] Prueba los grupos calendarizados

  [./symfony mako:ejecuta-sync --env=prod |INFO]
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'])->getConnection();

        $this->logSection('Probando GruposCalendarizados', '…');
        $this->logSection('', '');
        $this->logSection('Lista de GruposCalendarizados', '…');

        $c = new Criteria();
        $c->add(GrupoPeer::CENTRO_ID, sfConfig::get('app_centro_actual_id'));

        foreach (GrupoPeer::doSelect($c) as $grupo) {
            $this->logSection('Grupo ' . $grupo->getClave(), $grupo->getCurso()->getNombre() . '-- inicia ->' . $grupo->getHorario()->getFechaInicio() . ' Activo? ' . ($grupo->getActivo() ? 'Si' : 'No') . '  Seriado? ' . ($grupo->getCurso()->getSeriado() ? 'Si' : 'No') . ' Restricciones socio? ' . ($grupo->getCurso()->getRestriccionesSocio() ? 'Si' : 'No'));
            $this->logSection('Restricciones', '');

            foreach ($grupo->getCurso()->getRestriccionesSocios() as $restriccion) {
                $this->logSection('Restriccion edad minima: ', ($restriccion->getEdadMinima() ? $restriccion->getEdadMinima() : 'No'));
                $this->logSection('Restriccion edad maxima: ', ($restriccion->getEdadMaxima() ? $restriccion->getEdadMaxima() : 'No'));
                $this->logSection('Restriccion sexo: ', $restriccion->getSexo());
                $this->logSection('Restriccion ocupacion: ', ($restriccion->getOcupacionId() ? $restriccion->getOcupacion()->getNombre() : 'No'));
                $this->logSection('Restriccion estudia: ', ($restriccion->getEstudia() ? 'Si' : 'No'));
                $this->logSection('Restriccion nivel estudio: ', ($restriccion->getNivelEstudioId() ? $restriccion->getNivelEstudio()->getNombre() : 'No'));
                $this->logSection('Restriccion habilidad informatica: ', ($restriccion->getHabilidadInformaticaId() ? $restriccion->getHabilidadInformatica()->getNombre() : 'No'));
                $this->logSection('Restriccion dominio ingles: ', ($restriccion->getHabilidadInformaticaId() ? $restriccion->getHabilidadInformatica()->getNombre() : 'No'));
            }
        }

        $this->logSection('Buscando socios para prueba', '…');
        $this->logSection('', '');
        $this->logSection('< 5 años', '…');

        $c = new Criteria();
        $c->add(SocioPeer::CENTRO_ID, sfConfig::get('app_centro_actual_id'));
        $c->add(SocioPeer::FECHA_NAC, '2005-01-01', Criteria::GREATER_EQUAL);

        $menor5 = SocioPeer::doSelectOne($c);

        if ($menor5) {
            $this->logSection('Socio: ', $menor5->getNombreCompleto() . ', Edad: ' . $menor5->getEdad() . ' -> ' . $menor5->getId());
        } else {
            $this->logSection('No hay socio menor de 5 años', '');
        }

        $this->logSection('> 5 años < 10', '…');

        $c = new Criteria();
        $c->add(SocioPeer::CENTRO_ID, sfConfig::get('app_centro_actual_id'));

        $criterion = $c->getNewCriterion(SocioPeer::FECHA_NAC, '2000-01-01', Criteria::GREATER_EQUAL);
        $criterion->addAnd($c->getNewCriterion(SocioPeer::FECHA_NAC, '2005-01-01', Criteria::LESS_EQUAL));

        $c->add($criterion);

        $menor10 = SocioPeer::doSelectOne($c);

        if ($menor10) {
            $this->logSection('Socio: ', $menor10->getNombreCompleto() . ', Edad: ' . $menor10->getEdad() . ' -> ' . $menor10->getId());
        } else {
            $this->logSection('No hay socio menor de 10 años', '');
        }

        $this->logSection('> 10 años < 15', '…');

        $c = new Criteria();
        $c->add(SocioPeer::CENTRO_ID, sfConfig::get('app_centro_actual_id'));

        $criterion = $c->getNewCriterion(SocioPeer::FECHA_NAC, '1995-01-01', Criteria::GREATER_EQUAL);
        $criterion->addAnd($c->getNewCriterion(SocioPeer::FECHA_NAC, '2000-01-01', Criteria::LESS_EQUAL));

        $c->add($criterion);

        $menor15 = SocioPeer::doSelectOne($c);

        if ($menor15) {
            $this->logSection('Socio: ', $menor15->getNombreCompleto() . ', Edad: ' . $menor15->getEdad() . ' -> ' . $menor15->getId());
        } else {
            $this->logSection('No hay socio menor de 15 años', '');
        }

        $this->logSection('> 20', '…');

        $c = new Criteria();
        $c->add(SocioPeer::CENTRO_ID, sfConfig::get('app_centro_actual_id'));

        $criterion = $c->getNewCriterion(SocioPeer::FECHA_NAC, '1900-01-01', Criteria::GREATER_EQUAL);
        $criterion->addAnd($c->getNewCriterion(SocioPeer::FECHA_NAC, '1995-01-01', Criteria::LESS_EQUAL));

        $c->add($criterion);

        $menor20 = SocioPeer::doSelectOne($c);

        if ($menor20) {
            $this->logSection('Socio: ', $menor20->getNombreCompleto() . ', Edad: ' . $menor20->getEdad() . ' -> ' . $menor20->getId());
        } else {
            $this->logSection('No hay socio menor de 20 años', '');
        }

        //5 años
        $this->logSection('', '');
        if ($menor5) {
            $this->logSection('Probando < 5 años', '');
            $alumno = AlumnosPeer::retrieveByPK($menor5->getId());
            if (!$alumno) {
                $alumno = new Alumnos();
                $alumno->setMatricula($menor5->getId());
                $alumno->setSocioId($menor5->getId());
                $alumno->save();
            }

            $finalizados  = array();
            $preinscritos = array();

            foreach ($alumno->getCursosFinalizadoss() as $terminado) {
                $finalizados[] = $terminado->getCursoId();
            }

            $this->logSection('Preinscritos', '');

            foreach ($alumno->getGrupoAlumnoss() as $preinscrito) {
                $preinscritos[] = $preinscrito->getGrupo()->getCursoId();
                $this->logSection('Preinscrito ', $preinscrito->getGrupo()->getClave());
            }

            $c = new Criteria();
            $c->add(GrupoPeer::CENTRO_ID, sfConfig::get('app_centro_actual_id'));
            $c->add(GrupoPeer::ACTIVO, true);
            $c->add(GrupoPeer::CUPO_ALCANZADO, false);
            $c->add(HorarioPeer::FECHA_INICIO, date('Y-m-d'), Criteria::GREATER_EQUAL);
            $c->addJoin(GrupoPeer::HORARIO_ID, HorarioPeer::ID);

            $grupos     = GrupoPeer::doSelect($c);
            $cursos     = array();
            $categorias = array();
            $horario    = array();

            foreach ($grupos as $grupo) {
                //Si el curso no esta dentro de los preincritos se procede a listar todos los cursos
                if (!in_array($grupo->getCursoId(), $preinscritos)) {
                    $candidato = false;

                    //Revisamos que el curso no tenga restricciones
                    if ($grupo->getCurso()->getRestriccionesSocio() === false) {
                        //Revisamos que no este seriado
                        if (!$grupo->getCurso()->getSeriado()) {
                            $candidato = true;

                        } else { //Si es seriado checamos la ruta de aprendizaje
                            $rutas = $grupo->getCurso()->getRutaAprendizajesRelatedByCursoId();
                            foreach ($rutas as $ruta) if (in_array($ruta->getPadreId(), $finalizados)) {
                                $candidato = true;
                            }
                        }
                    } else {
                        $restricciones = $grupo->getCurso()->getRestriccionesSocios();
                        foreach ($restricciones as $restriccion) {
                            //Una bandera para contar cuantas restricciones no pasa
                            $flag = 0;

                            //Revisamos que cumpla edad minima
                            if ($restriccion->getEdadMinima()) {
                                if ($alumno->getSocio()->getEdad() < $restriccion->getEdadMinima()) {
                                    $flag++;
                                }
                            }

                            //Revisamos que cumpla la edad maxima
                            if ($restriccion->getEdadMaxima()) {
                                if ($alumno->getSocio()->getEdad() > $restriccion->getEdadMaxima()) {
                                    $flag++;
                                }
                            }

                            //Revisamos que cumpla con el sexo
                            if ($restriccion->getSexo() != ' ') {
                                if ($alumno->getSocio()->getSexo() != $restriccion->getSexo()) {
                                    $flag++;
                                }
                            }

                            //Revisamos que cumpla con la ocupacion
                            if ($restriccion->getOcupacionId()) {
                                if ($alumno->getSocio()->getOcupacionId() != $restriccion->getOcupacionId()) {
                                    $flag++;
                                }
                            }

                            //Revisamos si estudia
                            if ($restriccion->getEstudia()) {
                                if ($alumno->getSocio()->getEstudia() != $restriccion->getEstudia()) {
                                    $flag++;
                                }
                            }

                            //Revisamos el nivel de estudios
                            if ($restriccion->getNivelEstudioId()) {
                                if ($alumno->getSocio()->getNivelId() != $restriccion->getNivelEstudioId()) {
                                    $flag++;
                                }
                            }

                            //Revisamos la habilidad informatica
                            if ($restriccion->getHabilidadInformaticaId()) {
                                if ($alumno->getSocio()->getHabilidadInformaticaId() != $restriccion->getHabilidadInformaticaId()) {
                                    $flag++;
                                }
                            }
                            //Revisamos el dominio de ingles
                            if ($restriccion->getDominioInglesId()) {
                                if ($alumno->getSocio()->getDominioInglesId() != $restriccion->getDominioInglesId()) {
                                    $flag++;
                                }
                            }

                            //Revisamos que la vandera siga en 0
                            if ($flag == 0) {
                                $candidato = true;
                            }
                        }
                    }

                    if ($candidato) {
                        $this->logSection('Candidato a :',
                                          $grupo->getClave()              . ' ->' .
                                          $grupo->getCurso()->getNombre() . ' -> ' .
                                          $grupo->getAula()->getNombre());
                    } else {
                        $this->logSection('No Candidato a :',
                                          $grupo->getClave()              . ' ->' .
                                          $grupo->getCurso()->getNombre() . ' -> ' .
                                          $grupo->getAula()->getNombre());
                    }
                }
            }
        }

        //10 años
        $this->logSection('', '');
        if ($menor10) {
            $this->logSection('Probando < 10 años', '');

            $alumno = AlumnosPeer::retrieveByPK($menor10->getId());

            if (!$alumno) {
                $alumno = new Alumnos();
                $alumno->setMatricula($menor10->getId());
                $alumno->setSocioId($menor10->getId());
                $alumno->save();
            }

            $finalizados  = array();
            $preinscritos = array();

            foreach ($alumno->getCursosFinalizadoss() as $terminado) {
                $finalizados[] = $terminado->getCursoId();
            }

            $this->logSection('Preinscritos', '');

            foreach ($alumno->getGrupoAlumnoss() as $preinscrito) {
                $preinscritos[] = $preinscrito->getGrupo()->getCursoId();
                $this->logSection('Preinscrito ', $preinscrito->getGrupo()->getClave());
            }

            $c = new Criteria();
            $c->add(GrupoPeer::CENTRO_ID, sfConfig::get('app_centro_actual_id'));
            $c->add(GrupoPeer::ACTIVO, true);
            $c->add(GrupoPeer::CUPO_ALCANZADO, false);
            $c->add(HorarioPeer::FECHA_INICIO, date('Y-m-d'), Criteria::GREATER_EQUAL);
            $c->addJoin(GrupoPeer::HORARIO_ID, HorarioPeer::ID);

            $grupos     = GrupoPeer::doSelect($c);
            $cursos     = array();
            $categorias = array();
            $horario    = array();

            foreach ($grupos as $grupo) {
                //Si el curso no esta dentro de los preincritos se procede a listar todos los cursos
                if (!in_array($grupo->getCursoId(), $preinscritos)) {
                    $candidato = false;

                    //Revisamos que el curso no tenga restricciones
                    if (!$grupo->getCurso()->getRestriccionesSocio()) {
                        //Revisamos que no este seriado
                        if (!$grupo->getCurso()->getSeriado()) {
                            $candidato = true;

                        } else { //Si es seriado checamos la ruta de aprendizaje
                            $rutas = $grupo->getCurso()->getRutaAprendizajesRelatedByCursoId();
                            foreach ($rutas as $ruta) if (in_array($ruta->getPadreId(), $finalizados)) {
                                $candidato = true;
                            }
                        }
                    } else {
                        $restricciones = $grupo->getCurso()->getRestriccionesSocios();
                        foreach ($restricciones as $restriccion) {
                            //Una bandera para contar cuantas restricciones no pasa
                            $flag = 0;

                            //Revisamos que cumpla edad minima
                            if ($restriccion->getEdadMinima()) {
                                if ($alumno->getSocio()->getEdad() < $restriccion->getEdadMinima()) {
                                    $flag++;
                                }
                            }

                            //Revisamos que cumpla la edad maxima
                            if ($restriccion->getEdadMaxima()) {
                                if ($alumno->getSocio()->getEdad() > $restriccion->getEdadMaxima()) {
                                    $flag++;
                                }
                            }

                            //Revisamos que cumpla con el sexo
                            if ($restriccion->getSexo() != ' ') {
                                if ($alumno->getSocio()->getSexo() != $restriccion->getSexo()) {
                                    $flag++;
                                }
                            }

                            //Revisamos que cumpla con la ocupacion
                            if ($restriccion->getOcupacionId()) {
                                if ($alumno->getSocio()->getOcupacionId() != $restriccion->getOcupacionId()) {
                                    $flag++;
                                }
                            }

                            //Revisamos si estudia
                            if ($restriccion->getEstudia()) {
                                if ($alumno->getSocio()->getEstudia() != $restriccion->getEstudia()) {
                                    $flag++;
                                }
                            }

                            //Revisamos el nivel de estudios
                            if ($restriccion->getNivelEstudioId()) {
                                if ($alumno->getSocio()->getNivelId() != $restriccion->getNivelEstudioId()) {
                                    $flag++;
                                }
                            }

                            //Revisamos la habilidad informatica
                            if ($restriccion->getHabilidadInformaticaId()) {
                                if ($alumno->getSocio()->getHabilidadInformaticaId() != $restriccion->getHabilidadInformaticaId()) {
                                    $flag++;
                                }
                            }

                            //Revisamos el dominio de ingles
                            if ($restriccion->getDominioInglesId()) {
                                if ($alumno->getSocio()->getDominioInglesId() != $restriccion->getDominioInglesId()) {
                                    $flag++;
                                }
                            }

                            //Revisamos que la vandera siga en 0
                            if ($flag == 0) {
                                $candidato = true;
                            }
                        }
                    }

                    if ($candidato) {
                        $this->logSection('Candidato a :',
                                          $grupo->getClave()              . ' ->' .
                                          $grupo->getCurso()->getNombre() . ' -> ' .
                                          $grupo->getAula()->getNombre());
                    } else {
                        $this->logSection('No Candidato a :',
                                          $grupo->getClave()              . ' ->' .
                                          $grupo->getCurso()->getNombre() . ' -> ' .
                                          $grupo->getAula()->getNombre());
                    }
                }
            }
        }

        //15 años
        $this->logSection('', '');
        if ($menor15) {
            $this->logSection('Probando < 15 años', '');

            $alumno = AlumnosPeer::retrieveByPK($menor15->getId());

            if (!$alumno) {
                $alumno = new Alumnos();
                $alumno->setMatricula($menor15->getId());
                $alumno->setSocioId($menor15->getId());
                $alumno->save();
            }

            $finalizados  = array();
            $preinscritos = array();

            foreach ($alumno->getCursosFinalizadoss() as $terminado) {
                $finalizados[] = $terminado->getCursoId();
            }

            $this->logSection('Preinscritos', '');

            foreach ($alumno->getGrupoAlumnoss() as $preinscrito) {
                $preinscritos[] = $preinscrito->getGrupo()->getCursoId();
                $this->logSection('Preinscrito ', $preinscrito->getGrupo()->getClave());
            }

            $c = new Criteria();
            $c->add(GrupoPeer::CENTRO_ID, sfConfig::get('app_centro_actual_id'));
            $c->add(GrupoPeer::ACTIVO, true);
            $c->add(GrupoPeer::CUPO_ALCANZADO, false);
            $c->add(HorarioPeer::FECHA_INICIO, date('Y-m-d'), Criteria::GREATER_EQUAL);
            $c->addJoin(GrupoPeer::HORARIO_ID, HorarioPeer::ID);

            $grupos     = GrupoPeer::doSelect($c);
            $cursos     = array();
            $categorias = array();
            $horario    = array();

            foreach ($grupos as $grupo) {
                //Si el curso no esta dentro de los preincritos se procede a listar todos los cursos
                if (!in_array($grupo->getCursoId(), $preinscritos)) {
                    $candidato = false;

                    //Revisamos que el curso no tenga restricciones
                    if (!$grupo->getCurso()->getRestriccionesSocio()) {
                        //Revisamos que no este seriado
                        if (!$grupo->getCurso()->getSeriado()) {
                            $candidato = true;

                        } else {//Si es seriado checamos la ruta de aprendizaje
                            $rutas = $grupo->getCurso()->getRutaAprendizajesRelatedByCursoId();
                            foreach ($rutas as $ruta) if (in_array($ruta->getPadreId(), $finalizados)) {
                                $candidato = true;
                            }
                        }
                    } else {
                        $restricciones = $grupo->getCurso()->getRestriccionesSocios();
                        foreach ($restricciones as $restriccion) {
                            //Una bandera para contar cuantas restricciones no pasa

                            $flag = 0;
                            //Revisamos que cumpla edad minima
                            if ($restriccion->getEdadMinima()) {
                                if ($alumno->getSocio()->getEdad() < $restriccion->getEdadMinima()) {
                                    $flag++;
                                }
                            }

                            //Revisamos que cumpla la edad maxima
                            if ($restriccion->getEdadMaxima()) {
                                if ($alumno->getSocio()->getEdad() > $restriccion->getEdadMaxima()) {
                                    $flag++;
                                }
                            }

                            //Revisamos que cumpla con el sexo
                            if ($restriccion->getSexo() != ' ') {
                                if ($alumno->getSocio()->getSexo() != $restriccion->getSexo()) {
                                    $flag++;
                                }
                            }

                            //Revisamos que cumpla con la ocupacion
                            if ($restriccion->getOcupacionId()) {
                                if ($alumno->getSocio()->getOcupacionId() != $restriccion->getOcupacionId()) {
                                    $flag++;
                                }
                            }

                            //Revisamos si estudia
                            if ($restriccion->getEstudia()) {
                                if ($alumno->getSocio()->getEstudia() != $restriccion->getEstudia()) {
                                    $flag++;
                                }
                            }

                            //Revisamos el nivel de estudios
                            if ($restriccion->getNivelEstudioId()) {
                                if ($alumno->getSocio()->getNivelId() != $restriccion->getNivelEstudioId()) {
                                    $flag++;
                                }
                            }

                            //Revisamos la habilidad informatica
                            if ($restriccion->getHabilidadInformaticaId()) {
                                if ($alumno->getSocio()->getHabilidadInformaticaId() != $restriccion->getHabilidadInformaticaId()) {
                                    $flag++;
                                }
                            }

                            //Revisamos el dominio de ingles
                            if ($restriccion->getDominioInglesId()) {
                                if ($alumno->getSocio()->getDominioInglesId() != $restriccion->getDominioInglesId()) {
                                    $flag++;
                                }
                            }

                            //Revisamos que la vandera siga en 0
                            if ($flag == 0) {
                                $candidato = true;
                            }
                        }
                    }

                    if ($candidato) {
                        $this->logSection('Candidato a :',
                                          $grupo->getClave()              . ' ->' .
                                          $grupo->getCurso()->getNombre() . ' -> ' .
                                          $grupo->getAula()->getNombre());
                    } else {
                        $this->logSection('No Candidato a :',
                                          $grupo->getClave()              . ' ->' .
                                          $grupo->getCurso()->getNombre() . ' -> ' .
                                          $grupo->getAula()->getNombre());
                    }
                }
            }
        }

        $this->logSection('', '');
        //20 años
        if ($menor20) {
            $this->logSection('Probando < 20 años', '');

            $alumno = AlumnosPeer::retrieveByPK($menor20->getId());

            if (!$alumno) {
                $alumno = new Alumnos();
                $alumno->setMatricula($menor20->getId());
                $alumno->setSocioId($menor20->getId());
                $alumno->save();
            }

            $finalizados  = array();
            $preinscritos = array();

            foreach ($alumno->getCursosFinalizadoss() as $terminado) {
                $finalizados[] = $terminado->getCursoId();
            }

            $this->logSection('Preinscritos', '');

            foreach ($alumno->getGrupoAlumnoss() as $preinscrito) {
                $preinscritos[] = $preinscrito->getGrupo()->getCursoId();
                $this->logSection('Preinscrito ', $preinscrito->getGrupo()->getClave());
            }

            $c = new Criteria();
            $c->add(GrupoPeer::CENTRO_ID, sfConfig::get('app_centro_actual_id'));
            $c->add(GrupoPeer::ACTIVO, true);
            $c->add(GrupoPeer::CUPO_ALCANZADO, false);
            $c->add(HorarioPeer::FECHA_INICIO, date('Y-m-d'), Criteria::GREATER_EQUAL);
            $c->addJoin(GrupoPeer::HORARIO_ID, HorarioPeer::ID);

            $grupos     = GrupoPeer::doSelect($c);
            $cursos     = array();
            $categorias = array();
            $horario    = array();

            foreach ($grupos as $grupo) {
                //Si el curso no esta dentro de los preincritos se procede a listar todos los cursos
                if (!in_array($grupo->getCursoId(), $preinscritos)) {
                    $candidato = false;

                    //Revisamos que el curso no tenga restricciones
                    if (!$grupo->getCurso()->getRestriccionesSocio()) {
                        //Revisamos que no este seriado
                        if (!$grupo->getCurso()->getSeriado()) {
                            $candidato = true;

                        } else { //Si es seriado checamos la ruta de aprendizaje
                            $rutas = $grupo->getCurso()->getRutaAprendizajesRelatedByCursoId();
                            foreach ($rutas as $ruta) if (in_array($ruta->getPadreId(), $finalizados)) {
                                $candidato = true;
                            }
                        }
                    } else {
                        $restricciones = $grupo->getCurso()->getRestriccionesSocios();
                        foreach ($restricciones as $restriccion) {
                            //Una bandera para contar cuantas restricciones no pasa
                            $flag = 0;

                            //Revisamos que cumpla edad minima
                            if ($restriccion->getEdadMinima()) {
                                if ($alumno->getSocio()->getEdad() < $restriccion->getEdadMinima()) {
                                    $flag++;
                                }
                            }

                            //Revisamos que cumpla la edad maxima
                            if ($restriccion->getEdadMaxima()) {
                                if ($alumno->getSocio()->getEdad() > $restriccion->getEdadMaxima()) {
                                    $flag++;
                                }
                            }

                            //Revisamos que cumpla con el sexo
                            if ($restriccion->getSexo() != ' ') {
                                if ($alumno->getSocio()->getSexo() != $restriccion->getSexo()) {
                                    $flag++;
                                }
                            }

                            //Revisamos que cumpla con la ocupacion
                            if ($restriccion->getOcupacionId()) {
                                if ($alumno->getSocio()->getOcupacionId() != $restriccion->getOcupacionId()) {
                                    $flag++;
                                }
                            }

                            //Revisamos si estudia
                            if ($restriccion->getEstudia()) {
                                if ($alumno->getSocio()->getEstudia() != $restriccion->getEstudia()) {
                                    $flag++;
                                }
                            }

                            //Revisamos el nivel de estudios
                            if ($restriccion->getNivelEstudioId()) {
                                if ($alumno->getSocio()->getNivelId() != $restriccion->getNivelEstudioId()) {
                                    $flag++;
                                }
                            }

                            //Revisamos la habilidad informatica
                            if ($restriccion->getHabilidadInformaticaId()) {
                                if ($alumno->getSocio()->getHabilidadInformaticaId() != $restriccion->getHabilidadInformaticaId()) {
                                    $flag++;
                                }
                            }

                            //Revisamos el dominio de ingles
                            if ($restriccion->getDominioInglesId()) {
                                if ($alumno->getSocio()->getDominioInglesId() != $restriccion->getDominioInglesId()) {
                                    $flag++;
                                }
                            }

                            //Revisamos que la vandera siga en 0
                            if ($flag == 0) {
                                $candidato = true;
                            }
                        }
                    }

                    if ($candidato) {
                        $this->logSection('Candidato a :',
                                          $grupo->getClave()              . ' ->' .
                                          $grupo->getCurso()->getNombre() . ' -> ' .
                                          $grupo->getAula()->getNombre());
                    } else {
                        $this->logSection('No Candidato a :',
                                          $grupo->getClave()              . ' ->' .
                                          $grupo->getCurso()->getNombre() . ' -> ' .
                                          $grupo->getAula()->getNombre());
                    }
                }
            }
        }
    }
}
?>
