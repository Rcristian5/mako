<?php
/*
 * This file is part of the symfony package.
 * (c) 2004-2006 Fabien Potencier <fabien.potencier@symfony-project.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
*/
/**
 * Lista las tareas permitidas de mako para usuarios finales..
 *
 * @package    symfony
 * @subpackage task
 * @author     Fabien Potencier <fabien.potencier@symfony-project.com>
 * @version    SVN: $Id: bxMakoListTask.class.php,v 1.2 2010/10/03 09:01:41 eorozco Exp $
 */
class bxMakoListTask extends sfCommandApplicationTask
{
    /**
     * @see sfTask
     */
    protected function configure() {
        $this->addArguments(array(new sfCommandArgument('namespace', sfCommandArgument::OPTIONAL, 'El nombre del namespace'),));
        $this->addOptions(array(new sfCommandOption('xml', null, sfCommandOption::PARAMETER_NONE, 'Salida XML'),));

        $this->addOptions(array(
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'               ,  'tolu2' ),
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección' , 'propel'),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación', 'frontend')
        ));

        //$this->namespace = 'mako';
        $this->name                = 'lista';
        $this->briefDescription    = 'Lista las tareas';
        $this->detailedDescription = <<<EOF
La tarea [lista|INFO] lista todas las tareas:

  [mako lista|INFO]

EOF;

    }
    /**
     * @see sfTask
     */
    protected function execute($arguments = array(), $options = array()) {
        $tasks = array();

        foreach ($this->commandApplication->getTasks() as $name => $task) {
            $arguments['namespace'] = 'soporte';

            if ($arguments['namespace'] && $arguments['namespace'] != $task->getNamespace()) {
                continue;
            }

            if ($name != $task->getFullName()) {
                // it is an alias
                continue;
            }

            if (!$task->getNamespace()) {
                $name = '_default:' . $name;
            }

            $tasks[$name] = $task;
        }

        $this->outputAsText($arguments['namespace'], $tasks);
    }

    protected function outputAsText($namespace, $tasks) {
        $this->commandApplication->help();
        $this->log('');

        $width = 0;
        foreach ($tasks as $name  => $task) {
            $width = strlen($task->getName()) > $width ? strlen($task->getName()) : $width;
        }
        $width+= strlen($this->formatter->format('  ', 'INFO'));

        $messages = array();
        if ($namespace) {
            $messages[] = $this->formatter->format(sprintf("%s :", $namespace), 'COMMENT');
        } else {
            $messages[] = $this->formatter->format('Tareas disponibles:', 'COMMENT');
        }
        // display tasks
        ksort($tasks);
        $currentNamespace = '';
        foreach ($tasks as $name => $task) {
            if (!$namespace && $currentNamespace != $task->getNamespace()) {
                $currentNamespace = $task->getNamespace();
                $messages[]       = $this->formatter->format($task->getNamespace(), 'COMMENT');
            }

            $aliases    = $task->getAliases() ? $this->formatter->format(' (' . implode(', ', $task->getAliases()) . ')', 'COMMENT') : '';
            $messages[] = sprintf("  %-${width}s %s%s", $this->formatter->format(':' . $task->getName(), 'INFO'), $task->getBriefDescription(), $aliases);
        }

        $this->log($messages);
    }
}
