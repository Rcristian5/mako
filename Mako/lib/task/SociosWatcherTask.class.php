<?php 

define('INICIO_ALARMA', 600);
define('ALARMA_5', 300);
define('ALARMA_4', 240);
define('ALARMA_3', 180);
define('ALARMA_2', 120);
define('ALARMA_1', 61);
define('FIN_SESION', 60);
define('SESION_NORMAL', -2);
define('SESION_ATIPICA', 9000);
define('SESION_SIN_CONSUMO', -1);

class SociosWatcherTask extends sfBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'               ,  'tolu2' ),
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección' , 'propel'),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación', 'frontend')
        ));

        $this->namespace           = 'mako';
        $this->name                = 'socio-watcher';
        $this->briefDescription    = 'Socio watcher';
        $this->detailedDescription = <<<EOF
The [mako:socio-watcher|INFO] Socio watcher:
 
  [./symfony mako:socio-watcher --env=prod |INFO]
EOF;
  }
 
  protected function execute($arguments = array(), $options = array())
  {
    $databaseManager = new sfDatabaseManager($this->configuration);
    $configuration = ProjectConfiguration::getApplicationConfiguration('frontend', 'tolu2' , true);
	$context = sfContext::createInstance($configuration);
    $dispatcher = $configuration->getEventDispatcher();
    
	$c = new Criteria();
	$c->addDescendingOrderByColumn(SesionPeer::FECHA_LOGIN);
	
	$sesiones = SesionPeer::doSelect($c);
	
	foreach($sesiones as $sesion)
	{
		$res = $this->analiza($sesion,$dispatcher);
		if($res == SESION_SIN_CONSUMO)
		{
			continue;
		}
		
	}
	
  }
  
  
  function analiza(Sesion $sesion,$dispatcher)
  {
	$cmd = "perl /opt/MakoScripts/ejecuta_pc.pl ";
	$ahora = date("U");
  	$log = '/var/log/mako/socios-watcher.log';
	
  	if($sesion->getTipoSesion() == sfConfig::get('app_sesion_sin_consumo_id') 
  		|| 
  		$sesion->getTipoSesion() == sfConfig::get('app_sesion_multisesion_sin_consumo_id'))
  		
  	{
  			return SESION_SIN_CONSUMO;		
  	}
  	elseif ($sesion->getTipoSesion() == sfConfig::get('app_sesion_default_id')) //Sesion normal default
  	{
		$tlogin = $sesion->getFechaLogin("U");
		
		$consumido = $ahora - $tlogin;
		$restan = $sesion->getSaldo()-$consumido;
		
		error_log(sprintf("[%s]\tIP:%s,\tSocio:%s,\tTlogin:%s,\tTUsado: %s,\tRestan: %s,\tSaldo: %s\t",time("H:i:s"),$sesion->getIp(),$sesion->getUsuario(),$sesion->getFechaLogin(),$consumido,$restan,$sesion->getSaldo()),3,$log);
		
		if ($restan +60 >= INICIO_ALARMA &&  $restan -60 <= INICIO_ALARMA)
		{
			error_log(", Alarma 6 \n",3,$log);
			exec($cmd ." ".$sesion->getIp()." ". '"su - ' . $sesion->getUsuario() .  ' -c \'DISPLAY=:0 /usr/bin/notify-send -t 40000 -i /usr/share/icons/gnome/scalable/emblems/emblem-urgent.svg \"Tiempo próximo a vencer. Menos de 10 minutos de sesión.\" \"Estimado socio, tu tiempo de sesión está próximo a vencer, recuerda que puedes adquirir más tiempo en la recepción del centro.\"\'"');
		}
		elseif ($restan >= ALARMA_4 + 1 && $restan <= ALARMA_5 )
		{
			error_log(", Alarma 5 \n",3,$log);
			exec($cmd ." ".$sesion->getIp()." ". '"su - ' . $sesion->getUsuario() .  ' -c \'DISPLAY=:0 /usr/bin/notify-send -t 40000 -i /usr/share/icons/gnome/scalable/emblems/emblem-urgent.svg \"Tiempo próximo a vencer. Menos de 5 minutos de sesión.\" \"Estimado socio, tu tiempo de sesión está próximo a vencer, recuerda que puedes adquirir más tiempo en la recepción del centro.\"\'"');
		}
		elseif ($restan >= ALARMA_3 + 1 && $restan <= ALARMA_4 )
		{
			error_log(", Alarma 4 \n",3,$log);
			exec($cmd ." ".$sesion->getIp()." ". '"su - ' . $sesion->getUsuario() .  ' -c \'DISPLAY=:0 /usr/bin/notify-send -t 40000 -i /usr/share/icons/gnome/scalable/emblems/emblem-urgent.svg \"Tiempo próximo a vencer. Menos de 4 minutos de sesión.\" \"Estimado socio, tu tiempo de sesión está próximo a vencer, recuerda que puedes adquirir más tiempo en la recepción del centro.\"\'"');
		}
		elseif ($restan >= ALARMA_2 + 1 && $restan <= ALARMA_3 )
		{
			error_log(", Alarma 3 \n",3,$log);
			exec($cmd ." ".$sesion->getIp()." ". '"su - ' . $sesion->getUsuario() .  ' -c \'DISPLAY=:0 /usr/bin/notify-send -t 40000 -i /usr/share/icons/gnome/scalable/emblems/emblem-urgent.svg \"Tiempo próximo a vencer. Menos de 3 minutos de sesión.\" \"Estimado socio, tu tiempo de sesión está próximo a vencer, recuerda que puedes adquirir más tiempo en la recepción del centro.\"\'"');
		}
		elseif ($restan >= ALARMA_1 + 1 && $restan <= ALARMA_2 )
		{
			error_log(", Alarma 2 \n",3,$log);
			exec($cmd ." ".$sesion->getIp()." ". '"su - ' . $sesion->getUsuario() .  ' -c \'DISPLAY=:0 /usr/bin/notify-send -t 40000 -i /usr/share/icons/gnome/scalable/emblems/emblem-urgent.svg \"Tiempo próximo a vencer. Menos de 2 minutos de sesión.\" \"Estimado socio, tu tiempo de sesión está próximo a vencer, recuerda que puedes adquirir más tiempo en la recepción del centro.\"\'"');
		}
		elseif ($restan >= FIN_SESION + 1 && $restan <= ALARMA_1 )
		{
			error_log(", Alarma 1 \n",3,$log);
			exec($cmd ." ".$sesion->getIp()." ". '"su - ' . $sesion->getUsuario() .  ' -c \'DISPLAY=:0 /usr/bin/notify-send -t 40000 -i /usr/share/icons/gnome/scalable/emblems/emblem-urgent.svg \"Tiempo próximo a vencer. Menos de 1 minuto de sesión.\" \"Estimado socio, tu tiempo de sesión está próximo a vencer, recuerda que puedes adquirir más tiempo en la recepción del centro.\"\'"');
			
		}
		elseif ($restan <= FIN_SESION )
		{
			error_log(", Fin sesion \n",3,$log);			
			exec($cmd ." ".$sesion->getIp()." ". '"su - ' . $sesion->getUsuario() .  ' -c \'DISPLAY=:0 /usr/bin/notify-send -t 40000 -i /usr/share/icons/gnome/scalable/emblems/emblem-urgent.svg \"No cuenta con tiempo de sesión.\" \"Estimado socio, tu tiempo de sesión ha finalizado. Recuerda que puedes adquirir más tiempo en la recepción del centro.\"\'"');
			sleep(5);
			exec($cmd." ".$sesion->getIp().' "/etc/init.d/gdm restart"');
			//exec($cmd." ".$sesion->getIp().' "/etc/init.d/lightdm restart"');
			exec($cmd." ".$sesion->getIp().' "sudo service lightdm restart"');
			$h = SesionPeer::eliminaSesion($sesion);
			$dispatcher->notify(new sfEvent($this, 'socio.logout', array(
						'sesion_historico' => $h
			)));
		}
		elseif ($consumido >= SESION_ATIPICA )
		{
			error_log(", Sesion atipica \n",3,$log);
			//error_log($cmd ." ".$sesion->getIp()." ". '"DISPLAY=:0 /usr/bin/notify-send -t 40000 -i /usr/share/icons/gnome/scalable/emblems/emblem-urgent.svg \"Tiempo próximo a vencer. 10 minutos de sesión.\" \"Estimado socio, tu tiempo de sesión está próximo a vencer, recuerda que puedes adquirir más tiempo en la recepción del centro.\""');
			
			//exec($cmd ." ".$sesion->getIp()." ". '"DISPLAY=:0 /usr/bin/notify-send -t 40000 -i /usr/share/icons/gnome/scalable/emblems/emblem-urgent.svg \"Tiempo próximo a vencer. 10 minutos de sesión.\" \"Estimado socio, tu tiempo de sesión está próximo a vencer, recuerda que puedes adquirir más tiempo en la recepción del centro.\""');
		}
		else
		{
			error_log(", Sesion normal en curso \n",3,$log);
			
		}

  	}
  	
  	
  }
  
}


?>
