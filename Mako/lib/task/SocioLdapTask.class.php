<?php
class SocioLdapTask extends sfBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'                                   ,  'tolu2' ),
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección'                     , 'propel'),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación'                    , 'frontend'),
            new sfCommandOption('usuario'    , null, sfCommandOption::PARAMETER_REQUIRED, 'Nombre de usuario del socio a consultar'       , null),
            new sfCommandOption('accion'     , null, sfCommandOption::PARAMETER_REQUIRED, 'Accion a realizar, "consultar" o "sincronizar"', 'consultar')
        ));

        $this->namespace           = 'mako';
        $this->name                = 'socio-clave-ldap';
        $this->briefDescription    = 'Compara el valor actual de la clave en base local de centro con el LDAP de centro';
        $this->detailedDescription = <<<EOF
The [mako:socio-clave-ldap|INFO] Compara el valor actual de la clave en base local de centro con el LDAP de centro:

  [./symfony mako:socio-clave-ldap --usuario="nombre.usuario" --accion="consultar" |INFO]  Consulta si existe sincronia entre clave Mako y clave LDAP
  [./symfony mako:socio-clave-ldap --usuario="nombre.usuario" --accion="sincronizar" |INFO]  Sincroniza la clave existente en Mako y clave LDAP
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'] ? $options['connection'] : null)->getConnection();
        $configuration   = ProjectConfiguration::getApplicationConfiguration('frontend', $options['env'], true);
        $context         = sfContext::createInstance($configuration);

        $configuration->loadHelpers('Ldap');

        $c = new Criteria();
        $c->add(SocioPeer::USUARIO, $options['usuario']);

        $as = SocioPeer::doSelect($c);

        if (count($as) < 1) echo "===No existe el usuario en la base local de este centro.===\n";

        foreach ($as as $socio) {
            $compara = comparaClavesLdap($socio->getUsuario(), $socio->getClave());
            if ($compara === 0) {
                echo "===No existe el usuario en el LDAP local de este centro.===\n";
                continue;
            }

            echo "====Consulta de estatus de clave socio====\n";
            echo "Usuario: \t" . $socio->getUsuario() . "\n";
            echo "Nombre: \t" . $socio . "\n";
            echo "Folio : \t" . $socio->getFolio() . "\n";
            echo "Alta en: \t" . $socio->getCentro() . "\n";
            echo "Fecha Alta: \t" . $socio->getCreatedAt() . "\n";
            echo "Estatus: \t" . ($socio->getActivo() ? 'ACTIVO' : 'INACTIVO') . "\n";
            echo "UID LDAP: \t" . $socio->getLdapUid() . "\n";
            echo "ESTATUS CLAVE: \t" . (($compara ? 'Coinciden plenamente' : 'NO coinciden')) . "\n";

            if ($options['accion'] == 'sincronizar') {
                try {
                    modificarClaveSocioLDAP($socio->getUsuario(), $socio->getClave());
                    echo "**Se han sincronizado las claves**";

                } catch(Exception $e) {
                    error_log("Error: " . $e->getMessage());
                }
            }
        }
    }

    function hostname($ip, $alias) {
        if (substr($ip, -2) == '98' || substr($ip, -2) == '99') {
            $num      = (substr($ip, -2) == '98') ? 'recepcion1' : 'recepcion2';
            $hostname = $num . "-l-" . strtolower($alias);
        } else {
            $num      = substr($ip, -2);
            $hostname = "pc" . $num . "-l-" . strtolower($alias);
        }

        return $hostname;
    }

    function getSeccionIdPorNombre($centro_id, $nombre) {
        if (trim($nombre) == '') $nombre = "RECEPCION";

        $c      = new Criteria();
        $c->add(SeccionPeer::NOMBRE, strtoupper($nombre));
        $c->add(SeccionPeer::CENTRO_ID, $centro_id);

        $s = SeccionPeer::doSelectOne($c);

        if ($s != null) {
            return $s->getId();
        }

        return 0;
    }
}
?>