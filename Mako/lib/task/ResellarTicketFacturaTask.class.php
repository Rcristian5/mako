<?php
class ResellarTicketFacturaTask extends sfBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment',  'tolu2' ),
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'propel'),
            new sfCommandOption('desde', null, sfCommandOption::PARAMETER_REQUIRED, 'Fecha desde la cual se resellara'),
            new sfCommandOption('hasta', null, sfCommandOption::PARAMETER_OPTIONAL, 'Fecha hasta la cual se resellara', null),
            new sfCommandOption('certificado', null, sfCommandOption::PARAMETER_OPTIONAL, 'Numero de certificado a usar', null)
        ));

        $this->namespace           = 'mako';
        $this->name                = 'resella-ticket-factura';
        $this->briefDescription    = 'Genera un nuevo sello para facturas';
        $this->detailedDescription = <<<EOF
        [facturando:resella-ticket-factura|INFO] Sella nuevamente las facturas y genera los tickets de venta.
        Desde una fecha dada:
        mako:resella-ticket-factura --desde="2011-07-09 00:00:00"

        Desde un rango de fechas:
        mako:resella-ticket-factura --desde="2011-07-09 00:00:00" --hasta="2011-07-30 00:00:00"

        Se puede indicar otro numero de certificado para generar el sello:
        mako:resella-ticket-factura --desde="2011-07-09 00:00:00" --certificado="00000100000011111"

        [php symfony facturando:resella-ticket-factura|INFO]
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'])->getConnection();

        $criteria = new Criteria();
        $criteria->add(OrdenPeer::CENTRO_ID, sfConfig::get('app_centro_actual_id'));
        $criteria->add(OrdenPeer::ES_FACTURA, true);

        if ($options['hasta']) {
            $criteria->add(OrdenPeer::CREATED_AT,
                           OrdenPeer::CREATED_AT .
                                " BETWEEN '" . $options['desde'] .
                                "' AND '"    . $options['hasta'] .
                                "'",
                           Criteria::CUSTOM);
        } else {
            $criteria->add(OrdenPeer::CREATED_AT, $options['desde'], Criteria::GREATER_EQUAL);
        }

        $ordenes = OrdenPeer::doSelect($criteria);

        foreach ($ordenes as $orden) {
            $this->log('Fecha ' . $orden->getCreatedAt());

            $ctx = sfContext::createInstance($this->configuration);
            $ctx->getConfiguration()->loadHelpers('Partial');
            $ctx->getConfiguration()->loadHelpers('Facturas');

            $this->log('Resellando la factura');
            list($confFactura, $conceptos, $cadenaconceptos, $totalPalabras, $ffis, $caf_prods)  = FacturaPeer::facturaVenta($orden);

  
            $fo = TrFacturaOrdenPeer::getFacturaOrden($orden->getId());
            if( $fo == null)
                $fo = new TrFacturaOrden();
            $fo->setFacturaId($orden->getFactura()->getId());
            $fo->setOrdenId($orden->getId());
            $fo->setCentroId(sfConfig::get('app_centro_actual_id'));
            $fo->save();

            // Revisamos si hay que poner un numero de vertificado diferente
            if ($options['certificado']) {
                $this->log('Usando otro certificado ' . $options['certificado']);
                $confFactura['noCertificado']               = $options['certificado'];
            }

            $df          = generaFactura($confFactura, $conceptos);
            $cadena      = $df['cadena_original'];
            $sello       = $df['sello_digital'];
            $total       = $confFactura['total'];
            $subTotal    = $confFactura['subTotal'];
            $importe     = $subTotal;
            $iva         = $confFactura['Impuestos']['Traslados']['Traslado']['importe'];
            $varsPartial = array(
                'orden'           => $orden,
                'importe'         => $importe,
                'subTotal'        => $subTotal,
                'total'           => $total,
                'iva'             => $iva,
                'cadena'          => $cadena,
                'sello'           => $sello,
                'folio'           => $orden->getFactura()->getFolio(),
                'cadenaconceptos' => $cadenaconceptos,
                'ffis'            => $ffis,
                'certificado'     => $confFactura['noCertificado'],
                'totalPalabras'   => $totalPalabras,
                'caf_prods'       => $caf_prods
            );






        /*
        * Reingenieria  Mako. SIN - ID RQ
        * Responsables:  (BP) Bet Gader Porcayo Juárez, (LGL) Luis Gonzales Linares
        * Fecha:        21-01-2015
        * Descripción:  Se añade opcion para configuración de Nuevas Impresoras
        */
        $Prefijo = @sfConfig::get('app_prefijo');
        switch ($Prefijo) {
            case 'oki':
                $factura_corte = get_partial('pos/facturaVenta', $varsPartial);
                break;
            case 'Ch':
            case 'TMT20II':
                /*
                * En este caso se incluye el tipo Ch TMT20II
                */
                $factura_corte = get_partial('pos/factura' . $Prefijo . 'Venta', $varsPartial);
                break;
            default:
                /*
                * En caso de no estar configurado el parámetro app_prefijo, o que no es alguno 
                * de los anteriores se configura con respecto a OKI, 
                */
                $factura_corte = get_partial('pos/facturaVenta', $varsPartial);

                error_log('ERROR GENERICO!!! No se ha configurado el parámetro PREFIJO o el valor NO EXISTE en la configuración. Fn=execute');
                break;
        }





            //Imprimirmos tickets resumen
            $nomtick = PATH_FACTURAS . "/" .
                       $confFactura['Emisor']['rfc'] .
                       $confFactura['serie'] .
                       $confFactura['folio'] . '.txt';

            error_log('Guardando ticket en: ' . $nomtick);

            $fp = fopen($nomtick, "w+");

            fwrite($fp, $factura_corte);
            fclose($fp);
        }
    }
}

