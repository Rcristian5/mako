<?php
class SeguimientoMestasTask extends sfPropelBaseTask
{
    public $noContado = 0;
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección' , 'propel'),
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'               ,  'tolu2' ),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación', 'frontend'),
            new sfCommandOption('desde'      , null, sfCommandOption::PARAMETER_OPTIONAL, 'Fecha a partir de la cual se genera el reporte'),
            new sfCommandOption('hasta'      , null, sfCommandOption::PARAMETER_OPTIONAL, 'Fecha hasta la que se genera el reporte de la cual se genera el reporte'),
            new sfCommandOption('overwrite'  , null, sfCommandOption::PARAMETER_OPTIONAL, 'Sobreescritura de los datos guardados', 0),
            new sfCommandOption('indicadores', null, sfCommandOption::PARAMETER_OPTIONAL, 'Indicadores separados por _')
        ));

        $this->namespace           = 'mako';
        $this->name                = 'seguimiento-metas';
        $this->briefDescription    = 'Genera el reporte de indicadores para la fase 3';
        $this->detailedDescription = <<<EOF
     [mako:add-cursos-moodlle|INFO] Genera el reporte de indicadores para la fase 3

      [./symfony mako:seguimiento-metas --env=prod |INFO]
EOF;

    }
    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'])->getConnection();

        $this->logSection('Generando reporte: ', 'por dia');
        $this->reporteIndicadoresDia($options);
        $this->logSection('Generando reporte: ', 'por dia y grupo');
        $this->reporteIndicadoresDiaGrupo($options);
    }

    /**
     **
     ** FUNCIONES PRINCIPALES
     **
     **
     */

    /**
     * Funcion que genera los reportes indicadores por dia
     */
    public function reporteIndicadoresDia($opciones) {
        $centroId = sfConfig::get('app_centro_actual_id');
        $this->log('Obteniendo las fechas del reporte');
        $fechasReporte = $this->generaFechas('dia', $centroId, $opciones);
        if ($fechasReporte) {
            // Revisamos la opciones ingresadas
            if (($opciones['desde'] || $opciones['hasta']) && (int)$opciones['overwrite'] == 0) {
                if (!$this->askConfirmation('No se indico una accion, puede ser que existan datos guardados previamente. Si continua puede duplicar la informacion.
                        Desea continuar? (y/N)', 'QUESTION_LARGE', false)) {
                    $this->log('Se aborto la carga de reportes por dia');
                    return 1;
                }
            }
            $hoy   = $fechasReporte['hasta'];
            $fecha = mktime(0, 0, 0, $fechasReporte['desde']['n'], $fechasReporte['desde']['j'], $fechasReporte['desde']['Y']);
            // Determinamos si la fecha del reporte es menor a la actual
            if (date('Ymd', $fecha) < $hoy) {
                for ($i     = 1; date('Ymd', $fecha) < $hoy; $i++) {
                    // Sumamos 1 dia
                    $fecha               = mktime(0, 0, 0, $fechasReporte['desde']['n'], $fechasReporte['desde']['j'] + $i, $fechasReporte['desde']['Y']);
                    // Flag para determinar si el registro es nuevo
                    $overrideNuevo       = false;
                    // Sacamos los indicadores
                    $indicadores         = explode('_', $opciones['indicadores']);
                    $sociosActivos       = array();
                    $sociosActivosNuevos = array();
                    // Determinamos si es un registro nuevo o un overwrite de datos
                    if ($opciones['overwrite']) {
                        $criteria            = new Criteria();
                        $criteria->add(ReporteIndicadoresDiaPeer::CENTRO_ID, $centroId);
                        $criteria->add(ReporteIndicadoresDiaPeer::FECHA, date('Y-m-d', $fecha));
                        $criteria->add(ReporteIndicadoresDiaPeer::SEMANA, date('W', $fecha));
                        $criteria->add(ReporteIndicadoresDiaPeer::ANIO, date('Y', $fecha));
                        $reporteIndicadoresDia = ReporteIndicadoresDiaPeer::doSelectOne($criteria);
                        // Si no hay registro del dia creamos uno nuevo
                        if (!$reporteIndicadoresDia) {
                            $overrideNuevo         = true;
                            $this->log('El registro no se habia obtenido');
                            $this->log(' ');
                            $reporteIndicadoresDia = new ReporteIndicadoresDia();
                            $reporteIndicadoresDia->setId(Comun::generaId());
                            $reporteIndicadoresDia->setCentroId($centroId);
                            $reporteIndicadoresDia->setFecha(date('Y-m-d', $fecha));
                            $reporteIndicadoresDia->setSemana(date('W', $fecha));
                            $reporteIndicadoresDia->setAnio(date('Y', $fecha));
                        } else {
                            $this->log('Overwrite de datos');
                            $this->log(' ');
                        }
                    } else {
                        $this->log('Registro nuevo');
                        $this->log(' ');
                        $reporteIndicadoresDia = new ReporteIndicadoresDia();
                        $reporteIndicadoresDia->setId(Comun::generaId());
                        $reporteIndicadoresDia->setCentroId($centroId);
                        $reporteIndicadoresDia->setFecha(date('Y-m-d', $fecha));
                        $reporteIndicadoresDia->setSemana(date('W', $fecha));
                        $reporteIndicadoresDia->setAnio(date('Y', $fecha));
                    }
                    //  Obtenemos cada uno de los indicadores para este reporte
                    if ($opciones['overwrite'] == 0 || ($opciones['overwrite'] && in_array('registrosNuevos', $indicadores)) || ($opciones['overwrite'] && in_array('all', $indicadores)) || ($opciones['overwrite'] && $overrideNuevo)) {
                        $this->log('Registros nuevos');
                        $registrosNuevos = $this->registrosNuevos($fecha, $centroId);
                        $reporteIndicadoresDia->setRegistrosNuevos($registrosNuevos);
                    }
                    if ($opciones['overwrite'] == 0 || ($opciones['overwrite'] && in_array('sociosRegistrados', $indicadores)) || ($opciones['overwrite'] && in_array('all', $indicadores)) || ($opciones['overwrite'] && $overrideNuevo)) {
                        $this->log(('Socios registrados'));
                        $sociosRegistrados = $this->sociosRegistrados($fecha, $centroId);
                        $reporteIndicadoresDia->setSociosRegistrados($sociosRegistrados);
                    }
                    if ($opciones['overwrite'] == 0 || ($opciones['overwrite'] && in_array('sociosActivos', $indicadores)) || ($opciones['overwrite'] && in_array('all', $indicadores)) || ($opciones['overwrite'] && $overrideNuevo)) {
                        $this->log('Socios activos');
                        $sociosActivos = $this->sociosActivos($fecha, $centroId);
                        $reporteIndicadoresDia->setSociosActivos(count($sociosActivos));
                    }
                    if ($opciones['overwrite'] == 0 || ($opciones['overwrite'] && in_array('sociosActivosNuevos', $indicadores)) || ($opciones['overwrite'] && in_array('all', $indicadores)) || ($opciones['overwrite'] && $overrideNuevo)) {
                        $this->log('Socios activos nuevos');
                        $sociosActivosNuevos = $this->sociosActivosNuevos($fecha, $centroId);
                        $reporteIndicadoresDia->setSociosActivosNuevos(count($sociosActivosNuevos));
                    }
                    if ($opciones['overwrite'] == 0 || ($opciones['overwrite'] && in_array('cobranzaEducacion', $indicadores)) || ($opciones['overwrite'] && in_array('all', $indicadores)) || ($opciones['overwrite'] && $overrideNuevo)) {
                        $this->log('Cobranza educacion');
                        $cobranzaEducacion = $this->cobranzaEducacion($fecha, $centroId);
                        $reporteIndicadoresDia->setCobranzaEducacion($cobranzaEducacion);
                    }
                    if ($opciones['overwrite'] == 0 || ($opciones['overwrite'] && in_array('cobranzaInternet', $indicadores)) || ($opciones['overwrite'] && in_array('all', $indicadores)) || ($opciones['overwrite'] && $overrideNuevo)) {
                        $this->log('Cobranza de internet');
                        $cobranzaInternet = $this->cobranzaInternet($fecha, $centroId);
                        $reporteIndicadoresDia->setCobranzaInternet($cobranzaInternet);
                    }
                    if ($opciones['overwrite'] == 0 || ($opciones['overwrite'] && in_array('cobranzaOtros', $indicadores)) || ($opciones['overwrite'] && in_array('all', $indicadores)) || ($opciones['overwrite'] && $overrideNuevo)) {
                        $this->log('Cobranza otros');
                        $cobranzaOtros = $this->cobranzaOtros($fecha, $centroId);
                        $reporteIndicadoresDia->setCobranzaOtros($cobranzaOtros);
                    }
                    if ($opciones['overwrite'] == 0 || ($opciones['overwrite'] && in_array('cobranzaTotal', $indicadores)) || ($opciones['overwrite'] && in_array('all', $indicadores)) || ($opciones['overwrite'] && $overrideNuevo)) {
                        $this->log('Cobranza total');
                        $cobranzaTotal = $this->cobranzaTotal($fecha, $centroId);
                        $reporteIndicadoresDia->setCobranzaTotal($cobranzaTotal);
                    }
                    // Guardamos los datos del reporte
                    $reporteIndicadoresDia->save();
                    // Guardamos los socios activos
                    foreach ($sociosActivos as $socio) {
                        if ($opciones['overwrite'] && (in_array('sociosActivos', $indicadores) || in_array('all', $indicadores)) || ($opciones['overwrite'] && $overrideNuevo)) {
                            $criteriaActivos = new Criteria();
                            $criteriaActivos->add(ReporteIndicadoresDiaSociosActivosPeer::REPORTE_INDICADOR_DIA_ID, $reporteIndicadoresDia->getId());
                            $criteriaActivos->add(ReporteIndicadoresDiaSociosActivosPeer::CENTRO_ID, $centroId);
                            $criteriaActivos->add(ReporteIndicadoresDiaSociosActivosPeer::SOCIO_ID, $socio['id']);
                            $reporteIndicadoresSocios = ReporteIndicadoresDiaSociosActivosPeer::doSelectOne($criteriaActivos);
                            // Si no se encuentran los datos del socio se guardan
                            if (!$reporteIndicadoresSocios) {
                                $reporteIndicadoresSocios = new ReporteIndicadoresDiaSociosActivos();
                                $reporteIndicadoresSocios->setId(Comun::generaId());
                                $reporteIndicadoresSocios->setReporteIndicadorDiaId($reporteIndicadoresDia->getId());
                                $reporteIndicadoresSocios->setCentroId($reporteIndicadoresDia->getCentroId());
                                $reporteIndicadoresSocios->setSocioId($socio['id']);
                                $reporteIndicadoresSocios->setUsuario($socio['usuario']);
                                $reporteIndicadoresSocios->setNombreCompleto($socio['nombre']);
                                $reporteIndicadoresSocios->setEdad($socio['edad']);
                                $reporteIndicadoresSocios->setOcupacionId($socio['ocupacion']);
                                $reporteIndicadoresSocios->setSexo($socio['sexo']);
                                $reporteIndicadoresSocios->save();
                            }
                        } else {
                            $reporteIndicadoresSocios = new ReporteIndicadoresDiaSociosActivos();
                            $reporteIndicadoresSocios->setId(Comun::generaId());
                            $reporteIndicadoresSocios->setReporteIndicadorDiaId($reporteIndicadoresDia->getId());
                            $reporteIndicadoresSocios->setCentroId($reporteIndicadoresDia->getCentroId());
                            $reporteIndicadoresSocios->setSocioId($socio['id']);
                            $reporteIndicadoresSocios->setUsuario($socio['usuario']);
                            $reporteIndicadoresSocios->setNombreCompleto($socio['nombre']);
                            $reporteIndicadoresSocios->setEdad($socio['edad']);
                            $reporteIndicadoresSocios->setOcupacionId($socio['ocupacion']);
                            $reporteIndicadoresSocios->setSexo($socio['sexo']);
                            $reporteIndicadoresSocios->save();
                        }
                    }

                    foreach ($sociosActivosNuevos as $socio) {
                        if ($opciones['overwrite'] && (in_array('sociosActivosNuevos', $indicadores) || in_array('all', $indicadores)) || ($opciones['overwrite'] && $overrideNuevo)) {
                            $criteriaActivosNuevos = new Criteria();
                            $criteriaActivosNuevos->add(ReporteIndicadoresDiaSociosActivosNuevosPeer::REPORTE_INDICADOR_DIA_ID, $reporteIndicadoresDia->getId());
                            $criteriaActivosNuevos->add(ReporteIndicadoresDiaSociosActivosNuevosPeer::CENTRO_ID, $centroId);
                            $criteriaActivosNuevos->add(ReporteIndicadoresDiaSociosActivosNuevosPeer::SOCIO_ID, $socio['id']);
                            $reporteIndicadoresSociosN = ReporteIndicadoresDiaSociosActivosPeer::doSelectOne($criteriaActivosNuevos);
                            // Si no se encuentran los datos del socio se guardan
                            if (!$reporteIndicadoresSociosN) {
                                $reporteIndicadoresSociosN = new ReporteIndicadoresDiaSociosActivosNuevos();
                                $reporteIndicadoresSociosN->setId(Comun::generaId());
                                $reporteIndicadoresSociosN->setReporteIndicadorDiaId($reporteIndicadoresDia->getId());
                                $reporteIndicadoresSociosN->setCentroId($reporteIndicadoresDia->getCentroId());
                                $reporteIndicadoresSociosN->setSocioId($socio['id']);
                                $reporteIndicadoresSociosN->setUsuario($socio['usuario']);
                                $reporteIndicadoresSociosN->setNombreCompleto($socio['nombre']);
                                $reporteIndicadoresSociosN->setEdad($socio['edad']);
                                $reporteIndicadoresSociosN->setOcupacionId($socio['ocupacion']);
                                $reporteIndicadoresSociosN->setSexo($socio['sexo']);
                                $reporteIndicadoresSociosN->save();
                            }
                        } else {
                            $reporteIndicadoresSociosN = new ReporteIndicadoresDiaSociosActivosNuevos();
                            $reporteIndicadoresSociosN->setId(Comun::generaId());
                            $reporteIndicadoresSociosN->setReporteIndicadorDiaId($reporteIndicadoresDia->getId());
                            $reporteIndicadoresSociosN->setCentroId($reporteIndicadoresDia->getCentroId());
                            $reporteIndicadoresSociosN->setSocioId($socio['id']);
                            $reporteIndicadoresSociosN->setUsuario($socio['usuario']);
                            $reporteIndicadoresSociosN->setNombreCompleto($socio['nombre']);
                            $reporteIndicadoresSociosN->setEdad($socio['edad']);
                            $reporteIndicadoresSociosN->setOcupacionId($socio['ocupacion']);
                            $reporteIndicadoresSociosN->setSexo($socio['sexo']);
                            $reporteIndicadoresSociosN->save();
                        }
                    }
                    $this->log('Se guardaron correctamente los datos del reporte');
                }
            } else {
                $this->log('El reporte por dia esta actualizado');
            }
        }
    }
    /**
     * Funcion que genera los reportes indicadores por dia y grupo
     */
    public function reporteIndicadoresDiaGrupo($opciones) {
        $this->log('Obteniendo las fechas del reporte');
        $centroId      = sfConfig::get('app_centro_actual_id');
        $fechasReporte = $this->generaFechas('grupo', $centroId, $opciones);
        if ($fechasReporte) {
            if (($opciones['desde'] || $opciones['hasta']) && (int)$opciones['overwrite'] == 0) {
                if (!$this->askConfirmation('No se indico una accion, puede ser que existan datos guardados previamente. Si continua puede duplicar la informacion.
                        Desea continuar? (y/N)', 'QUESTION_LARGE', false)) {
                    $this->log('Se aborto la carga de reportes por dia y grupo');
                    return 1;
                }
            }
            $hoy   = $fechasReporte['hasta'];
            $fecha = mktime(0, 0, 0, $fechasReporte['desde']['n'], $fechasReporte['desde']['j'], $fechasReporte['desde']['Y']);
            // Determinamos si la fecha del reporte es menor a la actual
            if (date('Ymd', $fecha) < $hoy) {
                for ($i     = 1; date('Ymd', $fecha) < $hoy; $i++) {
                    //error_log('-----------');
                    //error_log('-----------');
                    //error_log('-----------');
                    // Sumanos un dia a la fecha
                    $fecha                      = mktime(0, 0, 0, $fechasReporte['desde']['n'], $fechasReporte['desde']['j'] + $i, $fechasReporte['desde']['Y']);
                    // Sacamos los indicadores
                    $indicadores                = explode('_', $opciones['indicadores']);
                    // Flag para determinar si el registro es nuevo
                    $overrideNuevo              = false;

                    $reporteDia                 = $this->creaReporteIndicadoresDiaGrupo($opciones, $fecha, $centroId, $indicadores);
                    $resultadosGrupo            = $reporteDia['resultadosGrupo'];
                    $alumnosActivos             = $reporteDia['alumnosActivos'];
                    $alumnosInscritos           = $reporteDia['alumnosInscritos'];
                    $alumnosPreinscritos        = $reporteDia['alumnosPreinscritos'];
                    $alumnosActividadProgramada = $reporteDia['actividadProgramada'];
                    // Obtenemos primero todos los datos de los grupos
                    $this->log('Guardando los registros por grupo');
                    foreach ($resultadosGrupo as $key                            => $resultado) {
                        $overrideNuevo                  = false;
                        $resultadoSave                  = $resultado;
                        $alumnosActivosSave             = $alumnosActivos;
                        $alumnosInscritosSave           = $alumnosInscritos;
                        $alumnosPreinscritosSave        = $alumnosPreinscritos;
                        $alumnosActividadProgramadaSave = $alumnosActividadProgramada;
                        // Determinamos si es un registro nuevo o un overwrite de datos
                        if ($opciones['overwrite']) {
                            $criteria                       = new Criteria();
                            $criteria->add(ReporteIndicadoresDiaGrupoPeer::CENTRO_ID, $centroId);
                            $criteria->add(ReporteIndicadoresDiaGrupoPeer::FECHA, date('Y-m-d', $fecha));
                            $criteria->add(ReporteIndicadoresDiaGrupoPeer::SEMANA, date('W', $fecha));
                            $criteria->add(ReporteIndicadoresDiaGrupoPeer::ANIO, date('Y', $fecha));
                            $criteria->add(ReporteIndicadoresDiaGrupoPeer::GRUPO_ID, $resultadoSave['grupo']);
                            $criteria->add(ReporteIndicadoresDiaGrupoPeer::CURSO_ID, $resultadoSave['curso']);
                            $criteria->add(ReporteIndicadoresDiaGrupoPeer::FACILITADOR_ID, ($resultadoSave['facilitador'] ? $resultadoSave['facilitador'] : 1));
                            $criteria->add(ReporteIndicadoresDiaGrupoPeer::CATEGORIA_CURSO_ID, $resultadoSave['categoria']);
                            $criteria->add(ReporteIndicadoresDiaGrupoPeer::AULA_ID, $resultadoSave['aula']);
                            $reporteIndicadoresDiaGrupo = ReporteIndicadoresDiaGrupoPeer::doSelectOne($criteria);
                            // Si no hay registro del dia creamos uno nuevo
                            if (!$reporteIndicadoresDiaGrupo) {
                                $overrideNuevo              = true;
                                $this->log('El registro no se habia obtenido');
                                $this->log(' ');
                                $reporteIndicadoresDiaGrupo = new ReporteIndicadoresDiaGrupo();
                                $reporteIndicadoresDiaGrupo->setId(Comun::generaId());
                                $reporteIndicadoresDiaGrupo->setCentroId($resultadoSave['centro']);
                                $reporteIndicadoresDiaGrupo->setGrupoId($resultadoSave['grupo']);
                                $reporteIndicadoresDiaGrupo->setCursoId($resultadoSave['curso']);
                                $reporteIndicadoresDiaGrupo->setFacilitadorId(($resultadoSave['facilitador'] ? $resultadoSave['facilitador'] : 1));
                                $reporteIndicadoresDiaGrupo->setCategoriaCursoId($resultadoSave['categoria']);
                                $reporteIndicadoresDiaGrupo->setAulaId($resultadoSave['aula']);
                                $reporteIndicadoresDiaGrupo->setFecha(date('Y-m-d', $fecha));
                                $reporteIndicadoresDiaGrupo->setSemana(date('W', $fecha));
                                $reporteIndicadoresDiaGrupo->setAnio(date('Y', $fecha));

                                $reporteDiaN                    = $this->creaReporteIndicadoresDiaGrupo($opciones, $fecha, $centroId, array('all'), $resultadoSave['grupo']);
                                $resultadoSaveSave              = $reporteDiaN['resultadosGrupo'];
                                $alumnosActivosSave             = $reporteDiaN['alumnosActivos'];
                                $alumnosInscritosSave           = $reporteDiaN['alumnosInscritos'];
                                $alumnosPreinscritosSave        = $reporteDiaN['alumnosPreinscritos'];
                                $alumnosActividadProgramadaSave = $reporteDiaN['actividadProgramada'];
                            } else {
                                $this->log('Overwrite de datos');
                                $this->log(' ');
                            }
                        } else {
                            $this->log('Registro nuevo');
                            $this->log(' ');
                            $reporteIndicadoresDiaGrupo = new ReporteIndicadoresDiaGrupo();
                            $reporteIndicadoresDiaGrupo->setId(Comun::generaId());
                            $reporteIndicadoresDiaGrupo->setCentroId($resultadoSave['centro']);
                            $reporteIndicadoresDiaGrupo->setGrupoId($resultadoSave['grupo']);
                            $reporteIndicadoresDiaGrupo->setCursoId($resultadoSave['curso']);
                            $reporteIndicadoresDiaGrupo->setFacilitadorId(($resultadoSave['facilitador'] ? $resultadoSave['facilitador'] : 1));
                            $reporteIndicadoresDiaGrupo->setCategoriaCursoId($resultadoSave['categoria']);
                            $reporteIndicadoresDiaGrupo->setAulaId($resultadoSave['aula']);
                            $reporteIndicadoresDiaGrupo->setFecha(date('Y-m-d', $fecha));
                            $reporteIndicadoresDiaGrupo->setSemana(date('W', $fecha));
                            $reporteIndicadoresDiaGrupo->setAnio(date('Y', $fecha));
                        }
                        if ($opciones['overwrite'] == 0 || ($opciones['overwrite'] != 0 && in_array('alumnosPreinscritos', $indicadores)) || ($opciones['overwrite'] != 0 && in_array('all', $indicadores)) || $overrideNuevo) {
                            $reporteIndicadoresDiaGrupo->setSociosPreinscritos((array_key_exists('preinscritos', $resultadoSave) ? $resultadoSave['preinscritos'] : 0));
                        }
                        if ($opciones['overwrite'] == 0 || ($opciones['overwrite'] != 0 && in_array('alumnosInscritos', $indicadores)) || ($opciones['overwrite'] != 0 && in_array('all', $indicadores)) || $overrideNuevo) {
                            $reporteIndicadoresDiaGrupo->setSociosInscritos((array_key_exists('inscritos', $resultadoSave) ? $resultadoSave['inscritos'] : 0));
                        }
                        if ($opciones['overwrite'] == 0 || ($opciones['overwrite'] != 0 && in_array('alumnosInscritosNuevos', $indicadores)) || ($opciones['overwrite'] != 0 && in_array('all', $indicadores)) || $overrideNuevo) {
                            $reporteIndicadoresDiaGrupo->setSociosInscritosNuevos((array_key_exists('inscritosNuevos', $resultadoSave) ? $resultadoSave['inscritosNuevos'] : 0));
                        }
                        if ($opciones['overwrite'] == 0 || ($opciones['overwrite'] != 0 && in_array('alumnosInscritosRecurrentes', $indicadores)) || ($opciones['overwrite'] != 0 && in_array('all', $indicadores)) || $overrideNuevo) {
                            $reporteIndicadoresDiaGrupo->setSociosInscritosRecurrentes((array_key_exists('inscritosRecurrentes', $resultadoSave) ? $resultadoSave['inscritosRecurrentes'] : 0));
                        }
                        if ($opciones['overwrite'] == 0 || ($opciones['overwrite'] != 0 && in_array('alumnosActivos', $indicadores)) || ($opciones['overwrite'] != 0 && in_array('all', $indicadores)) || $overrideNuevo) {
                            $reporteIndicadoresDiaGrupo->setAlumnosActivos((array_key_exists('alumnosActivos', $resultadoSave) ? count($resultadoSave['alumnosActivos']) : 0));
                        }
                        if ($opciones['overwrite'] == 0 || ($opciones['overwrite'] != 0 && in_array('actividadProgramada', $indicadores)) || ($opciones['overwrite'] != 0 && in_array('all', $indicadores)) || $overrideNuevo) {
                            $reporteIndicadoresDiaGrupo->setAlumnosActividadProgramada((array_key_exists('alumnosActividadProgramada', $resultadoSave) ? count($resultadoSave['alumnosActividadProgramada']) : 0));
                        }
                        if ($opciones['overwrite'] == 0 || ($opciones['overwrite'] != 0 && in_array('alumnosGraduados', $indicadores)) || ($opciones['overwrite'] != 0 && in_array('all', $indicadores)) || $overrideNuevo) {
                            $reporteIndicadoresDiaGrupo->setAlumnosGraduados((array_key_exists('alumnosGraduados', $resultadoSave) ? $resultadoSave['alumnosGraduados'] : 0));
                        }
                        if ($opciones['overwrite'] == 0 || ($opciones['overwrite'] != 0 && in_array('alumnosReprobados', $indicadores)) || ($opciones['overwrite'] != 0 && in_array('all', $indicadores)) || $overrideNuevo) {
                            $reporteIndicadoresDiaGrupo->setAlumnosReprobados((array_key_exists('alumnosReprobados', $resultadoSave) ? $resultadoSave['alumnosReprobados'] : 0));
                        }
                        if ($opciones['overwrite'] == 0 || ($opciones['overwrite'] != 0 && in_array('graduacionEsperada', $indicadores)) || ($opciones['overwrite'] != 0 && in_array('all', $indicadores)) || $overrideNuevo) {
                            $reporteIndicadoresDiaGrupo->setGraduacionEsperada((array_key_exists('graduacionEsperada', $resultadoSave) ? $resultadoSave['graduacionEsperada'] : 0));
                        }
                        if ($opciones['overwrite'] == 0 || ($opciones['overwrite'] != 0 && in_array('alumnosDesertores', $indicadores)) || ($opciones['overwrite'] != 0 && in_array('all', $indicadores)) || $overrideNuevo) {
                            $reporteIndicadoresDiaGrupo->setAlumnosDesertores((array_key_exists('alumnosDesertores', $resultadoSave) ? $resultadoSave['alumnosDesertores'] : 0));
                        }
                        if ($opciones['overwrite'] == 0 || ($opciones['overwrite'] != 0 && in_array('cobranzaProgramada', $indicadores)) || ($opciones['overwrite'] != 0 && in_array('all', $indicadores)) || $overrideNuevo) {
                            $reporteIndicadoresDiaGrupo->setPagosProgramados((array_key_exists('pagosProgramados', $resultadoSave) ? $resultadoSave['pagosProgramados'] : 0));
                        }
                        if ($opciones['overwrite'] == 0 || ($opciones['overwrite'] != 0 && in_array('cobranzaCobrada', $indicadores)) || ($opciones['overwrite'] != 0 && in_array('all', $indicadores)) || $overrideNuevo) {
                            $reporteIndicadoresDiaGrupo->setPagosCobrados((array_key_exists('pagosCobrados', $resultadoSave) ? $resultadoSave['pagosCobrados'] : 0));
                        }
                        if ($opciones['overwrite'] == 0 || ($opciones['overwrite'] != 0 && in_array('cobranzaProgramada', $indicadores)) || ($opciones['overwrite'] != 0 && in_array('all', $indicadores)) || $overrideNuevo) {
                            $reporteIndicadoresDiaGrupo->setCobranzaProgramada((array_key_exists('cobranzaProgramada', $resultadoSave) ? $resultadoSave['cobranzaProgramada'] ? $resultadoSave['cobranzaProgramada'] : 0 : 0));
                        }
                        if ($opciones['overwrite'] == 0 || ($opciones['overwrite'] != 0 && in_array('cobranzaCobrada', $indicadores)) || ($opciones['overwrite'] != 0 && in_array('all', $indicadores)) || $overrideNuevo) {
                            $reporteIndicadoresDiaGrupo->setCobranzaCobrada((array_key_exists('cobranzaCobrada', $resultadoSave) ? $resultadoSave['cobranzaCobrada'] ? $resultadoSave['cobranzaCobrada'] : 0 : 0));
                        }
                        $reporteIndicadoresDiaGrupo->save();
                        // Guardamos los socios inscritos al grupo
                        if ($opciones['overwrite'] != 0 && (in_array('alumnosInscritos', $indicadores) || in_array('all', $indicadores)) || $overrideNuevo) {
                            foreach ($alumnosInscritosSave[$resultadoSave['grupo']] as $inscrito) {
                                $criteriaAlumnosInscritos = new Criteria();
                                $criteriaAlumnosInscritos->add(ReporteIndicadoresDiaGrupoSociosInscritosPeer::REPORTE_INDICADOR_DIA_GRUPO_ID, $reporteIndicadoresDiaGrupo->getId());
                                $criteriaAlumnosInscritos->add(ReporteIndicadoresDiaGrupoSociosInscritosPeer::CENTRO_ID, $centroId);
                                $criteriaAlumnosInscritos->add(ReporteIndicadoresDiaGrupoSociosInscritosPeer::SOCIO_ID, $inscrito['id']);
                                $reporteInscritos = ReporteIndicadoresDiaGrupoSociosInscritosPeer::doSelectOne($criteriaAlumnosInscritos);
                                // Si no se encuentran los datos del socio se guardan
                                if (!$reporteInscritos) {
                                    $reporteInscritos = new ReporteIndicadoresDiaGrupoSociosInscritos();
                                    $reporteInscritos->setId(Comun::generaId());
                                    $reporteInscritos->setReporteIndicadorDiaGrupoId($reporteIndicadoresDiaGrupo->getId());
                                    $reporteInscritos->setSocioId($inscrito['id']);
                                    $reporteInscritos->setCentroId($reporteIndicadoresDiaGrupo->getCentroId());
                                    $reporteInscritos->setUsuario($inscrito['usuario']);
                                    $reporteInscritos->setNombreCompleto($inscrito['nombre']);
                                    $reporteInscritos->setEdad($inscrito['edad']);
                                    $reporteInscritos->setOcupacionId($inscrito['ocupacion']);
                                    $reporteInscritos->setSexo($inscrito['sexo']);
                                    $reporteInscritos->save();
                                }
                            }
                        } else {
                            foreach ($alumnosInscritosSave[$resultadoSave['grupo']] as $inscrito) {
                                $reporteInscritos = new ReporteIndicadoresDiaGrupoSociosInscritos();
                                $reporteInscritos->setId(Comun::generaId());
                                $reporteInscritos->setReporteIndicadorDiaGrupoId($reporteIndicadoresDiaGrupo->getId());
                                $reporteInscritos->setSocioId($inscrito['id']);
                                $reporteInscritos->setCentroId($reporteIndicadoresDiaGrupo->getCentroId());
                                $reporteInscritos->setUsuario($inscrito['usuario']);
                                $reporteInscritos->setNombreCompleto($inscrito['nombre']);
                                $reporteInscritos->setEdad($inscrito['edad']);
                                $reporteInscritos->setOcupacionId($inscrito['ocupacion']);
                                $reporteInscritos->setSexo($inscrito['sexo']);
                                $reporteInscritos->save();
                            }
                        }
                        // Guardamos los socios preinscritos al grupo
                        if ($opciones['overwrite'] != 0 && (in_array('alumnosPreinscritos', $indicadores) || in_array('all', $indicadores)) || $overrideNuevo) {
                            foreach ($alumnosPreinscritosSave[$resultadoSave['grupo']] as $preinscrito) {
                                $criteriaAlumnosPreinscritos = new Criteria();
                                $criteriaAlumnosPreinscritos->add(ReporteIndicadoresDiaGrupoSociosPreinscritosPeer::REPORTE_INDICADOR_DIA_GRUPO_ID, $reporteIndicadoresDiaGrupo->getId());
                                $criteriaAlumnosPreinscritos->add(ReporteIndicadoresDiaGrupoSociosPreinscritosPeer::CENTRO_ID, $centroId);
                                $criteriaAlumnosPreinscritos->add(ReporteIndicadoresDiaGrupoSociosPreinscritosPeer::SOCIO_ID, $preinscrito['id']);
                                $reportePreinscritos = ReporteIndicadoresDiaGrupoSociosPreinscritosPeer::doSelectOne($criteriaAlumnosPreinscritos);
                                // Si no se encuentran los datos del socio se guardan
                                if (!$reportePreinscritos) {
                                    $reportePreinscritos = new ReporteIndicadoresDiaGrupoSociosInscritos();
                                    $reportePreinscritos->setId(Comun::generaId());
                                    $reportePreinscritos->setReporteIndicadorDiaGrupoId($reporteIndicadoresDiaGrupo->getId());
                                    $reportePreinscritos->setSocioId($preinscrito['id']);
                                    $reportePreinscritos->setCentroId($reporteIndicadoresDiaGrupo->getCentroId());
                                    $reportePreinscritos->setUsuario($preinscrito['usuario']);
                                    $reportePreinscritos->setNombreCompleto($preinscrito['nombre']);
                                    $reportePreinscritos->setEdad($preinscrito['edad']);
                                    $reportePreinscritos->setOcupacionId($preinscrito['ocupacion']);
                                    $reportePreinscritos->setSexo($preinscrito['sexo']);
                                    $reportePreinscritos->save();
                                }
                            }
                        } else {
                            foreach ($alumnosPreinscritosSave[$resultadoSave['grupo']] as $preinscrito) {
                                $reportePreinscritos = new ReporteIndicadoresDiaGrupoSociosPreinscritos();
                                $reportePreinscritos->setId(Comun::generaId());
                                $reportePreinscritos->setReporteIndicadorDiaGrupoId($reporteIndicadoresDiaGrupo->getId());
                                $reportePreinscritos->setSocioId($preinscrito['id']);
                                $reportePreinscritos->setCentroId($reporteIndicadoresDiaGrupo->getCentroId());
                                $reportePreinscritos->setUsuario($preinscrito['usuario']);
                                $reportePreinscritos->setNombreCompleto($preinscrito['nombre']);
                                $reportePreinscritos->setEdad($preinscrito['edad']);
                                $reportePreinscritos->setOcupacionId($preinscrito['ocupacion']);
                                $reportePreinscritos->setSexo($preinscrito['sexo']);
                                $reportePreinscritos->save();
                            }
                        }
                        // Guardamos los socios activos
                        if ($opciones['overwrite'] != 0 && (in_array('alumnosActivos', $indicadores) || in_array('all', $indicadores)) || $overrideNuevo) {
                            foreach ($alumnosActivosSave[$resultadoSave['grupo']] as $activo) {
                                $criteriaAlumnosActivos = new Criteria();
                                $criteriaAlumnosActivos->add(ReporteIndicadoresDiaGrupoAlumnosActivosPeer::REPORTE_INDICADOR_DIA_GRUPO_ID, $reporteIndicadoresDiaGrupo->getId());
                                $criteriaAlumnosActivos->add(ReporteIndicadoresDiaGrupoAlumnosActivosPeer::CENTRO_ID, $centroId);
                                $criteriaAlumnosActivos->add(ReporteIndicadoresDiaGrupoAlumnosActivosPeer::SOCIO_ID, $activo['id']);
                                $reporteActivos = ReporteIndicadoresDiaGrupoAlumnosActivosPeer::doSelectOne($criteriaAlumnosActivos);
                                // Si no se encuentran los datos del socio se guardan
                                if (!reporteActivos) {
                                    $reporteActivos = new ReporteIndicadoresDiaGrupoAlumnosActivos();
                                    $reporteActivos->setId(Comun::generaId());
                                    $reporteActivos->setReporteIndicadorDiaGrupoId($reporteIndicadoresDiaGrupo->getId());
                                    $reporteActivos->setSocioId($activo['id']);
                                    $reporteActivos->setCentroId($reporteIndicadoresDiaGrupo->getCentroId());
                                    $reporteActivos->setUsuario($activo['usuario']);
                                    $reporteActivos->setNombreCompleto($activo['nombre']);
                                    $reporteActivos->setEdad($activo['edad']);
                                    $reporteActivos->setOcupacionId($activo['ocupacion']);
                                    $reporteActivos->setSexo($activo['sexo']);
                                    $reporteActivos->save();
                                }
                            }
                        } else {

                            foreach ($alumnosActivosSave[$resultadoSave['grupo']] as $activo) {
                                $reporteActivos = new ReporteIndicadoresDiaGrupoAlumnosActivos();
                                $reporteActivos->setId(Comun::generaId());
                                $reporteActivos->setReporteIndicadorDiaGrupoId($reporteIndicadoresDiaGrupo->getId());
                                $reporteActivos->setId(Comun::generaId());
                                $reporteActivos->setSocioId($activo['id']);
                                $reporteActivos->setCentroId($reporteIndicadoresDiaGrupo->getCentroId());
                                $reporteActivos->setUsuario($activo['usuario']);
                                $reporteActivos->setNombreCompleto($activo['nombre']);
                                $reporteActivos->setEdad($activo['edad']);
                                $reporteActivos->setOcupacionId($activo['ocupacion']);
                                $reporteActivos->setSexo($activo['sexo']);

                                $reporteActivos->save();
                            }
                        }
                        // Guardamos los socios con actividad programada
                        if ($opciones['overwrite'] != 0 && (in_array('actividadProgramada', $indicadores) || in_array('all', $indicadores)) || $overrideNuevo) {
                            foreach ($alumnosActividadProgramadaSave[$resultadoSave['grupo']] as $programada) {
                                $criteriaAlumnosActividadProgramada = new Criteria();
                                $criteriaAlumnosActividadProgramada->add(ReporteIndicadoresDiaGrupoAlumnosActividadProgramadaPeer::REPORTE_INDICADOR_DIA_GRUPO_ID, $reporteIndicadoresDiaGrupo->getId());
                                $criteriaAlumnosActividadProgramada->add(ReporteIndicadoresDiaGrupoAlumnosActividadProgramadaPeer::CENTRO_ID, $centroId);
                                $criteriaAlumnosActividadProgramada->add(ReporteIndicadoresDiaGrupoAlumnosActividadProgramadaPeer::SOCIO_ID, $programada['id']);
                                $reporteActividadProgramada = ReporteIndicadoresDiaGrupoAlumnosActividadProgramadaPeer::doSelectOne($criteriaAlumnosActividadProgramada);
                                // Si no se encuentran los datos del socio se guardan
                                if (!reporteActividadProgramada) {
                                    $reporteActividadProgramada = new ReporteIndicadoresDiaGrupoAlumnosActividadProgramada();
                                    $reporteActividadProgramada->setId(Comun::generaId());
                                    $reporteActividadProgramada->setReporteIndicadorDiaGrupoId($reporteIndicadoresDiaGrupo->getId());
                                    $reporteActividadProgramada->setSocioId($programada['id']);
                                    $reporteActividadProgramada->setCentroId($reporteIndicadoresDiaGrupo->getCentroId());
                                    $reporteActividadProgramada->setUsuario($programada['usuario']);
                                    $reporteActividadProgramada->setNombreCompleto($programada['nombre']);
                                    $reporteActividadProgramada->setEdad($programada['edad']);
                                    $reporteActividadProgramada->setOcupacionId($programada['ocupacion']);
                                    $reporteActividadProgramada->setSexo($programada['sexo']);
                                    $reporteActividadProgramada->save();
                                }
                            }
                        } else {
                            foreach ($alumnosActividadProgramadaSave[$resultadoSave['grupo']] as $programada) {
                                $reporteActividadProgramada = new ReporteIndicadoresDiaGrupoAlumnosActividadProgramada();
                                $reporteActividadProgramada->setReporteIndicadorDiaGrupoId($reporteIndicadoresDiaGrupo->getId());
                                $reporteActividadProgramada->setId(Comun::generaId());
                                $reporteActividadProgramada->setSocioId($programada['id']);
                                $reporteActividadProgramada->setCentroId($reporteIndicadoresDiaGrupo->getCentroId());
                                $reporteActividadProgramada->setUsuario($programada['usuario']);
                                $reporteActividadProgramada->setNombreCompleto($programada['nombre']);
                                $reporteActividadProgramada->setEdad($programada['edad']);
                                $reporteActividadProgramada->setOcupacionId($programada['ocupacion']);
                                $reporteActividadProgramada->setSexo($programada['sexo']);
                                $reporteActividadProgramada->save();
                            }
                        }
                    }
                }
            } else {
                $this->log('El reporte por dia y grupo esta actualizado');
            }
        }
    }
    /**
     **
     ** INDICARDORES
     **
     **
     */
    /**
     * Indicador que obtiene el total de nuevos socios registrados en un dia indicador
     * @param date $fecha - Fecha para la cual se ontendra el indicador
     * @return int - Cantidad de nuevos socios registrados
     */
    public function registrosNuevos($fecha, $centroId) {
        $criteriaNuevos = new Criteria();
        $criteriaNuevos->add(SocioPeer::CENTRO_ID, $centroId);
        $criteriaNuevos->add(SocioPeer::CREATED_AT, SocioPeer::CREATED_AT . " BETWEEN '" . date('Y-m-d', $fecha) . " 00:00:00' AND '" . date('Y-m-d', $fecha) . " 23:59:59'", Criteria::CUSTOM);

        return SocioPeer::doCount($criteriaNuevos);
    }
    /**
     * Indicador que obtiene la candidad de socios registrados a la fecha indicada
     * @param unknown_type $fecha
     * @return int Cantidad de socios registrados
     */
    public function sociosRegistrados($fecha, $centroId) {
        // Contamos los socios registrados
        $criteriaRegistros = new Criteria();

        $criteriaRegistros->add(SocioPeer::CENTRO_ID, $centroId);
        $criteriaRegistros->add(SocioPeer::CREATED_AT, date('Y-m-d', $fecha) . ' 00:00:00', Criteria::LESS_THAN);
        $registrosGuardados   = SocioPeer::doCount($criteriaRegistros);
        // Contamos los registros a a la fecha actual
        $criteriaRegistrosDia = new Criteria();
        $criteriaRegistrosDia->add(SocioPeer::CENTRO_ID, $centroId);
        $criteriaRegistrosDia->add(SocioPeer::CREATED_AT, date('Y-m-d', $fecha) . ' 23:59:59', Criteria::LESS_EQUAL);
        //error_log($criteriaRegistrosDia->toString());

        return SocioPeer::doCount($criteriaRegistrosDia) - $registrosGuardados;
    }
    /**
     * Indicador que obtiene la cantidad de socios activos en una fecha indicada
     * @param unknown_type $fecha
     * @return array - Arreglo con los datos de los socios activos
     */
    public function sociosActivos($fecha, $centroId) {
        // Buscamos todos los socios que adquirieron algun producto
        $sociosActivos   = array();
        $criteriaActivos = new Criteria();
        $criteriaActivos->add(DetalleOrdenPeer::CENTRO_ID, $centroId);
        $criteriaActivos->add(DetalleOrdenPeer::CREATED_AT, DetalleOrdenPeer::CREATED_AT . " BETWEEN '" . date('Y-m-d', $fecha) . " 00:00:00' AND '" . date('Y-m-d', $fecha) . " 23:59:59'", Criteria::CUSTOM);
        foreach (DetalleOrdenPeer::doSelect($criteriaActivos) as $socio) {
            if (!array_key_exists($socio->getSocioId(), $sociosActivos)) {
                $infoSocio  = SocioPeer::retrieveByPK($socio->getSocioId());
                $sociosActivos[$infoSocio->getId() ]            = array('id'            => $infoSocio->getId(), 'usuario'            => $infoSocio->getUsuario(), 'nombre'            => $infoSocio->getNombreCompleto(), 'edad'            => $infoSocio->getEdad(), 'ocupacion'            => $infoSocio->getOcupacionId(), 'sexo'            => $infoSocio->getSexo());
            }
        }
        //error_log('Socios Activos');
        //error_log($criteriaActivos->toString());
        // Buscamos todos los socios tuvieron alguna actividad
        $criteriaAs = new Criteria();
        $criteriaAs->add(SesionHistoricoPeer::CENTRO_ID, $centroId);
        $criteriaAs->add(SesionHistoricoPeer::FECHA_LOGIN, SesionHistoricoPeer::FECHA_LOGIN . " BETWEEN '" . date('Y-m-d', $fecha) . " 00:00:00' AND '" . date('Y-m-d', $fecha) . " 23:59:59'", Criteria::CUSTOM);
        foreach (SesionHistoricoPeer::doSelect($criteriaAs) as $socio) {
            if (!array_key_exists($socio->getSocioId(), $sociosActivos)) {
                $socio = SocioPeer::retrieveByPK($socio->getSocioId());
                $sociosActivos[$socio->getId() ]       = array('id' => $socio->getId(), 'usuario' => $socio->getUsuario(), 'nombre' => $socio->getNombreCompleto(), 'edad' => $socio->getEdad(), 'ocupacion' => $socio->getOcupacionId(), 'sexo' => $socio->getSexo());
            }
        }
        //error_log($criteriaAs->toString());

        // Contamos el total de socios activos en el dia
        return $sociosActivos;
    }
    /**
     * Indicador que obtiene los socios activos nuevos en una fecha indicada
     * @param unknown_type $fecha
     * @return array - Arreglo con los datos de los socios nuevos activos
     */
    public function sociosActivosNuevos($fecha, $centroId) {
        // Buscamos todos los socios que adquirieron algun producto
        $sociosActivosNuevos   = array();
        $criteriaActivosNuevos = new Criteria();
        $criteriaActivosNuevos->add(DetalleOrdenPeer::CENTRO_ID, $centroId);
        $criteriaActivosNuevos->add(DetalleOrdenPeer::CREATED_AT, DetalleOrdenPeer::CREATED_AT . " BETWEEN '" . date('Y-m-d', $fecha) . " 00:00:00' AND '" . date('Y-m-d', $fecha) . " 23:59:59'", Criteria::CUSTOM);
        $criteriaActivosNuevos->add(SocioPeer::CREATED_AT, SocioPeer::CREATED_AT . " BETWEEN '" . date('Y-m-d', $fecha) . " 00:00:00' AND '" . date('Y-m-d', $fecha) . " 23:59:59'", Criteria::CUSTOM);
        $criteriaActivosNuevos->addJoin(DetalleOrdenPeer::SOCIO_ID, SocioPeer::ID);
        foreach (DetalleOrdenPeer::doSelect($criteriaActivosNuevos) as $socio) {
            if (!array_key_exists($socio->getSocioId(), $sociosActivosNuevos)) {
                $infoSocio  = SocioPeer::retrieveByPK($socio->getSocioId());
                $sociosActivosNuevos[$infoSocio->getId() ]            = array('id'            => $infoSocio->getId(), 'usuario'            => $infoSocio->getUsuario(), 'nombre'            => $infoSocio->getNombreCompleto(), 'edad'            => $infoSocio->getEdad(), 'ocupacion'            => $infoSocio->getOcupacionId(), 'sexo'            => $infoSocio->getSexo());
            }
        }
        //error_log('Socios activos nuevos');
        //error_log($criteriaActivosNuevos->toString());
        // Buscamos todos los socios que asistieron a alguna clase

        // Buscamos todos los socios tuvieron alguna actividad
        $criteriaAs = new Criteria();
        $criteriaAs->add(SesionHistoricoPeer::CENTRO_ID, $centroId);
        $criteriaAs->add(SesionHistoricoPeer::FECHA_LOGIN, SesionHistoricoPeer::FECHA_LOGIN . " BETWEEN '" . date('Y-m-d', $fecha) . " 00:00:00' AND '" . date('Y-m-d', $fecha) . " 23:59:59'", Criteria::CUSTOM);
        $criteriaAs->add(SocioPeer::CREATED_AT, SocioPeer::CREATED_AT . " BETWEEN '" . date('Y-m-d', $fecha) . " 00:00:00' AND '" . date('Y-m-d', $fecha) . " 23:59:59'", Criteria::CUSTOM);
        $criteriaAs->addJoin(SesionHistoricoPeer::SOCIO_ID, SocioPeer::ID);
        foreach (SesionHistoricoPeer::doSelect($criteriaAs) as $socio) {
            if (!array_key_exists($socio->getSocioId(), $sociosActivos)) {
                $socio = SocioPeer::retrieveByPK($socio->getSocioId());
                $sociosActivos[$socio->getId() ]       = array('id' => $socio->getId(), 'usuario' => $socio->getUsuario(), 'nombre' => $socio->getNombreCompleto(), 'edad' => $socio->getEdad(), 'ocupacion' => $socio->getOcupacionId(), 'sexo' => $socio->getSexo());
            }
        }

        return $sociosActivosNuevos;
    }
    /**
     * Indicador que ontiene la suma de el subtotal de ventas por el producto internet
     * @param unknown_type $fecha
     * @return float - Suma del subtotal de ventar por el producto internet
     */
    public function cobranzaInternet($fecha, $centroId) {

        $criteriaInternet = new Criteria();
        $criteriaInternet->clearSelectColumns();
        $criteriaInternet->addSelectColumn('SUM(' . DetalleOrdenPeer::SUBTOTAL . ')');
        $criteriaInternet->add(DetalleOrdenPeer::CENTRO_ID, $centroId);
        $criteriaInternet->add(ProductoPeer::CATEGORIA_ID, 1);
        $criteriaInternet->add(DetalleOrdenPeer::CREATED_AT, DetalleOrdenPeer::CREATED_AT . " BETWEEN '" . date('Y-m-d', $fecha) . " 00:00:00' AND '" . date('Y-m-d', $fecha) . " 23:59:59'", Criteria::CUSTOM);
        $criteriaInternet->addJoin(DetalleOrdenPeer::PRODUCTO_ID, ProductoPeer::ID);
        $cobranzaInternet = DetalleOrdenPeer::doSelectOne($criteriaInternet);

        return $cobranzaInternet->getId() ? $cobranzaInternet->getId() : '0.00';
    }
    /**
     * Indicador que obtiene la suma del subtotal de todas las ventas de productos que no son internet o cursos
     * @param unknown_type $fecha
     * @return float - Suma del subtotal de las ventas de otros productos
     */
    public function cobranzaOtros($fecha, $centroId) {
        // Obtenemos la cobranza
        $criteriaOtros = new Criteria();
        $criteriaOtros->clearSelectColumns();
        $criteriaOtros->addSelectColumn('SUM(' . DetalleOrdenPeer::SUBTOTAL . ')');
        $criteriaOtros->add(ProductoPeer::CATEGORIA_ID, array(1, 2, 7), Criteria::NOT_IN);
        $criteriaOtros->add(DetalleOrdenPeer::CENTRO_ID, $centroId);
        $criteriaOtros->add(DetalleOrdenPeer::CREATED_AT, DetalleOrdenPeer::CREATED_AT . " BETWEEN '" . date('Y-m-d', $fecha) . " 00:00:00' AND '" . date('Y-m-d', $fecha) . " 23:59:59'", Criteria::CUSTOM);
        $criteriaOtros->addJoin(DetalleOrdenPeer::PRODUCTO_ID, ProductoPeer::ID);
        $cobranzaOtros = DetalleOrdenPeer::doSelectOne($criteriaOtros);

        return $cobranzaOtros->getId() ? $cobranzaOtros->getId() : '0.00';
    }
    /**
     * Indicador que obtiene la suma del subtotal de todas las ventas de productos relacionados con educacion
     * @param unknown_type $fecha
     * @return float - Suma del subtotal de las ventas de otros productos
     */
    public function cobranzaEducacion($fecha, $centroId) {
        // Obtenemos la cobranza de educacion
        $criteriaEducacion = new Criteria();
        $criteriaEducacion->clearSelectColumns();
        $criteriaEducacion->addSelectColumn('SUM(' . DetalleOrdenPeer::SUBTOTAL . ')');
        $criteriaEducacion->add(ProductoPeer::CATEGORIA_ID, array(2, 7), Criteria::IN);
        $criteriaEducacion->add(DetalleOrdenPeer::CENTRO_ID, $centroId);
        $criteriaEducacion->add(DetalleOrdenPeer::CREATED_AT, DetalleOrdenPeer::CREATED_AT . " BETWEEN '" . date('Y-m-d', $fecha) . " 00:00:00' AND '" . date('Y-m-d', $fecha) . " 23:59:59'", Criteria::CUSTOM);
        $criteriaEducacion->addJoin(DetalleOrdenPeer::PRODUCTO_ID, ProductoPeer::ID);
        $cobranzaEducacion = DetalleOrdenPeer::doSelectOne($criteriaEducacion);

        return $cobranzaEducacion->getId() ? $cobranzaEducacion->getId() : '0.00';
    }
    /**
     * Indicador que obtiene la suma del subtotal de todas las ventas realizadas en la fecha indicada
     * @param unknown_type $fecha
     * @return float - Suma del subtotal de las ventas de una fecha dada
     */
    public function cobranzaTotal($fecha, $centroId) {
        $criteriaTotal = new Criteria();
        $criteriaTotal->clearSelectColumns();
        $criteriaTotal->addSelectColumn('SUM(' . OrdenPeer::TOTAL . ')');
        $criteriaTotal->add(OrdenPeer::CENTRO_ID, $centroId);
        $criteriaTotal->add(OrdenPeer::TIPO_ID, array(1, 3), Criteria::IN);
        $criteriaTotal->add(OrdenPeer::CREATED_AT, OrdenPeer::CREATED_AT . " BETWEEN '" . date('Y-m-d', $fecha) . " 00:00:00' AND '" . date('Y-m-d', $fecha) . " 23:59:59'", Criteria::CUSTOM);
        $cobranzaTotal = OrdenPeer::doSelectOne($criteriaTotal);

        return $cobranzaTotal->getId() ? $cobranzaTotal->getId() : '0.00';
    }
    /**
     * Indicador para obtener el total de alumnos preinscritos en un fecha dada
     * @param unknown_type $resultadosGrupo
     * @param unknown_type $fecha
     * @return array - Arreglo con los datos de los alumnos preinscritos
     */
    public function alumnosPreinscritos($resultadosGrupo, $fecha, $centroId, $grupoId              = false) {
        $arregloPreinscritos  = array();
        $criteriaPreinscritos = new Criteria();
        $criteriaPreinscritos->add(GrupoAlumnosPeer::FECHA_PREINSCRITO, GrupoAlumnosPeer::FECHA_PREINSCRITO . " BETWEEN '" . date('Y-m-d', $fecha) . " 00:00:00' AND '" . date('Y-m-d', $fecha) . " 23:59:59'", Criteria::CUSTOM);
        $criteriaPreinscritos->add(GrupoAlumnosPeer::PREINSCRITO, true);
        $criteriaPreinscritos->addJoin(GrupoAlumnosPeer::GRUPO_ID, GrupoPeer::ID);
        //$criteriaPreinscritos->add(GrupoPeer::ACTIVO, true);
        if ($grupoId) {
            $criteriaPreinscritos->add(GrupoAlumnosPeer::ID, $grupoId);
        }
        $criteriaPreinscritos->add(GrupoPeer::CENTRO_ID, $centroId);

        foreach (GrupoAlumnosPeer::doSelect($criteriaPreinscritos) as $preinscritos) {
            if (!array_key_exists($preinscritos->getGrupoId(), $resultadosGrupo)) {
                ////error_log('almacenando los preincsrtitos');
                $resultadosGrupo[$preinscritos->getGrupoId() ]['centro'] = $centroId;
                $resultadosGrupo[$preinscritos->getGrupoId() ]['grupo'] = $preinscritos->getGrupoId();
                $resultadosGrupo[$preinscritos->getGrupoId() ]['curso'] = $preinscritos->getGrupo()->getCursoId();
                $resultadosGrupo[$preinscritos->getGrupoId() ]['facilitador'] = $preinscritos->getGrupo()->getFacilitadorId();
                $resultadosGrupo[$preinscritos->getGrupoId() ]['categoria'] = $preinscritos->getGrupo()->getCurso()->getCategoriaCursoId();
                $resultadosGrupo[$preinscritos->getGrupoId() ]['aula'] = $preinscritos->getGrupo()->getAulaId();
                $resultadosGrupo[$preinscritos->getGrupoId() ]['preinscritos'] = 1;
            } else {
                $resultadosGrupo[$preinscritos->getGrupoId() ]['preinscritos']+= 1;
            }
            $socio = SocioPeer::retrieveByPK($preinscritos->getAlumnoId());
            $arregloPreinscritos[$preinscritos->getGrupoId() ][]       = array('id' => $socio->getId(), 'usuario' => $socio->getUsuario(), 'nombre' => $socio->getNombreCompleto(), 'edad' => $socio->getEdad(), 'ocupacion' => $socio->getOcupacionId(), 'sexo' => $socio->getSexo());
        }

        return array('preinscritos'                   => $arregloPreinscritos, 'resultados'                   => ($grupoId ? $resultadosGrupo[$grupoId] : $resultadosGrupo));
    }
    /**
     * Indicador que obtiene la cantidad de alumnos inscritos en una fecha dada
     * @param unknown_type $resultadosgrupo
     * @param unknown_type $fecha
     * @return array - Arreglo con el resultado por grupo y los socios inscritos
     */
    public function alumnosInscritos($resultadosGrupo, $fecha, $centroId, $grupoId           = false) {
        $arregloInscritos  = array();
        $criteriaInscritos = new Criteria();
        $criteriaInscritos->add(GrupoAlumnosPeer::UPDATED_AT, GrupoAlumnosPeer::UPDATED_AT . " BETWEEN '" . date('Y-m-d', $fecha) . " 00:00:00' AND '" . date('Y-m-d', $fecha) . " 23:59:59'", Criteria::CUSTOM);
        $criteriaInscritos->add(GrupoAlumnosPeer::PREINSCRITO, false);
        $criteriaInscritos->addJoin(GrupoAlumnosPeer::GRUPO_ID, GrupoPeer::ID);
        //$criteriaInscritos->add(GrupoPeer::ACTIVO, true);
        if ($grupoId) {
            $criteriaInscritos->add(GrupoPeer::ID, $grupoId);
        }
        $criteriaInscritos->add(GrupoPeer::CENTRO_ID, $centroId);
        //error_log($criteriaInscritos->toString());

        foreach (GrupoAlumnosPeer::doSelect($criteriaInscritos) as $inscritos) {
            if (!array_key_exists($inscritos->getGrupoId(), $resultadosGrupo)) {
                ////error_log('almacenado inscritos');
                $resultadosGrupo[$inscritos->getGrupoId() ]['centro'] = $centroId;
                $resultadosGrupo[$inscritos->getGrupoId() ]['grupo'] = $inscritos->getGrupoId();
                $resultadosGrupo[$inscritos->getGrupoId() ]['curso'] = $inscritos->getGrupo()->getCursoId();
                $resultadosGrupo[$inscritos->getGrupoId() ]['facilitador'] = $inscritos->getGrupo()->getFacilitadorId();
                $resultadosGrupo[$inscritos->getGrupoId() ]['categoria'] = $inscritos->getGrupo()->getCurso()->getCategoriaCursoId();
                $resultadosGrupo[$inscritos->getGrupoId() ]['aula'] = $inscritos->getGrupo()->getAulaId();
                $resultadosGrupo[$inscritos->getGrupoId() ]['inscritos'] = 1;
            } else {
                $resultadosGrupo[$inscritos->getGrupoId() ]['inscritos']+= 1;
            }
            $socio = SocioPeer::retrieveByPK($inscritos->getAlumnoId());
            $arregloInscritos[$inscritos->getGrupoId() ][]       = array('id' => $socio->getId(), 'usuario' => $socio->getUsuario(), 'nombre' => $socio->getNombreCompleto(), 'edad' => $socio->getEdad(), 'ocupacion' => $socio->getOcupacionId(), 'sexo' => $socio->getSexo());
        }

        return array('inscritos'                         => $arregloInscritos, 'resultados'                         => ($grupoId ? $resultadosGrupo[$grupoId] : $resultadosGrupo));
    }
    /**
     * Indicador que obtiene los alumnos inscritos nuevos de un dia
     * @param unknown_type $reultadosGrupo
     * @param unknown_type $fecha
     * @return array - Arreglo con los socios nuevos enrolados a un grupo en un dia
     */
    public function alumnosInscritosNuevos($resultadosGrupo, $fecha, $centroId, $grupoId                 = false) {
        $sociosInscritos         = array();
        $criteriaInscritosNuevos = new Criteria();
        $criteriaInscritosNuevos->add(GrupoAlumnosPeer::UPDATED_AT, GrupoAlumnosPeer::UPDATED_AT . " BETWEEN '" . date('Y-m-d', $fecha) . " 00:00:00' AND '" . date('Y-m-d', $fecha) . " 23:59:59'", Criteria::CUSTOM);
        $criteriaInscritosNuevos->add(GrupoAlumnosPeer::PREINSCRITO, false);
        $criteriaInscritosNuevos->addJoin(GrupoAlumnosPeer::GRUPO_ID, GrupoPeer::ID);
        //$criteriaInscritosNuevos->add(GrupoPeer::ACTIVO, true);
        if ($grupoId) {
            $criteriaInscritosNuevos->add(GrupoPeer::ID, $grupoId);
        }
        $criteriaInscritosNuevos->add(GrupoPeer::CENTRO_ID, $centroId);
        $criteriaInscritosNuevos->addJoin(GrupoAlumnosPeer::ALUMNO_ID, SocioPeer::ID);
        $criteriaInscritosNuevos->add(SocioPeer::CREATED_AT, SocioPeer::CREATED_AT . " BETWEEN '" . date('Y-m-d', $fecha) . " 00:00:00' AND '" . date('Y-m-d', $fecha) . " 23:59:59'", Criteria::CUSTOM);
        $criteriaInscritosNuevos->add(SocioPeer::CENTRO_ID, $centroId);
        //error_log('Inscritos nuevos');
        //error_log($criteriaInscritosNuevos->toString());

        foreach (GrupoAlumnosPeer::doSelect($criteriaInscritosNuevos) as $inscritosNuevos) {
            if (!array_key_exists($inscritosNuevos->getGrupoId(), $resultadosGrupo)) {
                ////error_log('almacenado nuevos');
                $resultadosGrupo[$inscritosNuevos->getGrupoId() ]['centro'] = $centroId;
                $resultadosGrupo[$inscritosNuevos->getGrupoId() ]['grupo'] = $inscritosNuevos->getGrupoId();
                $resultadosGrupo[$inscritosNuevos->getGrupoId() ]['curso'] = $inscritosNuevos->getGrupo()->getCursoId();
                $resultadosGrupo[$inscritosNuevos->getGrupoId() ]['facilitador'] = $inscritosNuevos->getGrupo()->getFacilitadorId();
                $resultadosGrupo[$inscritosNuevos->getGrupoId() ]['categoria'] = $inscritosNuevos->getGrupo()->getCurso()->getCategoriaCursoId();
                $resultadosGrupo[$inscritosNuevos->getGrupoId() ]['aula'] = $inscritosNuevos->getGrupo()->getAulaId();
                $resultadosGrupo[$inscritosNuevos->getGrupoId() ]['inscritosNuevos'] = 1;
            } else {
                $resultadosGrupo[$inscritosNuevos->getGrupoId() ]['inscritosNuevos']+= 1;
            }
        }

        return ($grupoId ? $resultadosGrupo[$grupoId] : $resultadosGrupo);
    }
    /**
     * Indicador que obtiene la cantidad de socios que han finalizado al menos un curso y se han enrolado a otro
     * @param unknown_type $resultadosGrupo
     * @param unknown_type $fecha
     * @return array - Arreglo con la cantidad de alumnos recurrentes por grupo
     */
    public function alumnosInscritosrRecurrentes($resultadosGrupo, $fecha, $centroId, $grupoId                     = false) {
        $criteriaInscritosRecurrente = new Criteria();
        $criteriaInscritosRecurrente->add(GrupoAlumnosPeer::UPDATED_AT, GrupoAlumnosPeer::UPDATED_AT . " BETWEEN '" . date('Y-m-d', $fecha) . " 00:00:00' AND '" . date('Y-m-d', $fecha) . " 23:59:59'", Criteria::CUSTOM);
        $criteriaInscritosRecurrente->add(GrupoAlumnosPeer::PREINSCRITO, false);
        $criteriaInscritosRecurrente->addJoin(GrupoAlumnosPeer::GRUPO_ID, GrupoPeer::ID);
        //$criteriaInscritosRecurrente->add(GrupoPeer::ACTIVO, true);
        if ($grupoId) {
            $criteriaInscritosRecurrente->add(GrupoPeer::ID, $grupoId);
        }
        $criteriaInscritosRecurrente->add(GrupoPeer::CENTRO_ID, $centroId);
        $criteriaInscritosRecurrente->addJoin(GrupoAlumnosPeer::ALUMNO_ID, SocioPeer::ID);
        $criteriaInscritosRecurrente->add(SocioPeer::CREATED_AT, date('Y-m-d', $fecha), Criteria::LESS_THAN);
        $criteriaInscritosRecurrente->add(SocioPeer::CENTRO_ID, $centroId);
        //error_log('Inscritos Recurrentes');
        //error_log($criteriaInscritosRecurrente->toString());

        foreach (GrupoAlumnosPeer::doSelect($criteriaInscritosRecurrente) as $inscritosRecurrentes) {
            // Buscamos si esl socio ya termino al menos un curso
            $criteriaFinalizados = new Criteria();
            $criteriaFinalizados->add(CursosFinalizadosPeer::ALUMNO_ID, $inscritosRecurrentes->getAlumnoId());
            //error_log($criteriaInscritosRecurrente->toString());

            if (CursosFinalizadosPeer::doCount($criteriaFinalizados) > 0) {
                if (!array_key_exists($inscritosRecurrentes->getGrupoId(), $resultadosGrupo)) {
                    ////error_log('almacenado recurrentes');
                    $resultadosGrupo[$inscritosRecurrentes->getGrupoId() ]['centro'] = $centroId;
                    $resultadosGrupo[$inscritosRecurrentes->getGrupoId() ]['grupo'] = $inscritosRecurrentes->getGrupoId();
                    $resultadosGrupo[$inscritosRecurrentes->getGrupoId() ]['curso'] = $inscritosRecurrentes->getGrupo()->getCursoId();
                    $resultadosGrupo[$inscritosRecurrentes->getGrupoId() ]['facilitador'] = $inscritosRecurrentes->getGrupo()->getFacilitadorId();
                    $resultadosGrupo[$inscritosRecurrentes->getGrupoId() ]['categoria'] = $inscritosRecurrentes->getGrupo()->getCurso()->getCategoriaCursoId();
                    $resultadosGrupo[$inscritosRecurrentes->getGrupoId() ]['aula'] = $inscritosRecurrentes->getGrupo()->getAulaId();
                    $resultadosGrupo[$inscritosRecurrentes->getGrupoId() ]['inscritosRecurrentes'] = 1;
                } else {
                    $resultadosGrupo[$inscritosRecurrentes->getGrupoId() ]['inscritosRecurrentes']+= 1;
                }
            }
        }

        return ($grupoId ? $resultadosGrupo[$grupoId] : $resultadosGrupo);
    }
    /**
     * Indicador para obtener la informacion de los socios con actividad en un dia
     * @param unknown_type $resultadosGrupo
     * @param unknown_type $fecha
     * @return array -Arreglo con los datos de los socios y los grupos con actividad
     */
    public function alumnosActivos($resultadosGrupo, $fecha, $centroId, $grupoId         = false) {
        $alumnosActivos  = array();
        // Buscamos todas las actividades del dia
        $criteriaActivos = new Criteria();
        $criteriaActivos->add(FechasHorarioPeer::FECHA, date('Y-m-d', $fecha));
        $criteriaActivos->addJoin(FechasHorarioPeer::HORARIO_ID, HorarioPeer::ID);
        $criteriaActivos->addJoin(HorarioPeer::ID, GrupoPeer::HORARIO_ID);
        //$criteriaActivos->add(GrupoPeer::ACTIVO, TRUE);
        $criteriaActivos->addJoin(GrupoPeer::CENTRO_ID, $centroId);
        if ($grupoId) {
            $criteriaActivos->addJoin(GrupoPeer::ID, $grupoId);
        }
        //error_log('Alumnos Activos');
        //error_log($criteriaActivos->toString());

        foreach (FechasHorarioPeer::doSelect($criteriaActivos) as $fechaHorario) {
            $criteriaGrupo = new Criteria();
            $criteriaGrupo->add(GrupoPeer::HORARIO_ID, $fechaHorario->getHorarioId());
            $grupo             = GrupoPeer::doSelectOne($criteriaGrupo);
            //error_log($criteriaGrupo->toString());

            // Por cada socio inscrito al grupo vemos si tuvieron alguna actividad dentro de la fecha y hora correspondiente
            if ($grupo) {
                foreach ($grupo->getGrupoAlumnoss() as $grupoAlumno) {
                    if (!$grupoAlumno->getPreinscrito()) {
                        $criteriaActividad = new Criteria();
                        $criteriaActividad->add(SesionHistoricoPeer::CENTRO_ID, $centroId);
                        $criteriaActividad->add(SesionHistoricoPeer::SOCIO_ID, $grupoAlumno->getAlumnoId());
                        $desdeF = $fechaHorario->getFecha('Y-m-d') . ' ' . $fechaHorario->getHoraInicio('H:i:s');
                        $hastaF = $fechaHorario->getFecha('Y-m-d') . ' ' . $fechaHorario->getHoraFin('H:i:s');
                        $criteriaActividad->add(SesionHistoricoPeer::FECHA_LOGIN, SesionHistoricoPeer::FECHA_LOGIN . " BETWEEN '" . $desdeF . "' AND '" . $hastaF . "'", Criteria::CUSTOM);
                        //error_log($criteriaActividad->toString());

                        if (SesionHistoricoPeer::doCount($criteriaActividad) > 0) {
                            if (!array_key_exists($grupo->getId(), $resultadosGrupo)) {
                                $resultadosGrupo[$grupo->getId() ]['centro']       = $centroId;
                                $resultadosGrupo[$grupo->getId() ]['grupo']       = $grupo->getId();
                                $resultadosGrupo[$grupo->getId() ]['curso']       = $grupo->getCursoId();
                                $resultadosGrupo[$grupo->getId() ]['facilitador']       = $grupo->getFacilitadorId();
                                $resultadosGrupo[$grupo->getId() ]['categoria']       = $grupo->getCurso()->getCategoriaCursoId();
                                $resultadosGrupo[$grupo->getId() ]['aula']       = $grupo->getAulaId();
                                $resultadosGrupo[$grupo->getId() ]['alumnosActivos'][]       = $grupoAlumno->getAlumnoId();
                            } else {
                                if (!in_array($grupoAlumno->getAlumnoId(), $resultadosGrupo[$grupo->getId() ]['alumnosActivos'])) {
                                    $resultadosGrupo[$grupo->getId() ]['alumnosActivos'][]       = $grupoAlumno->getAlumnoId();
                                }
                            }
                            $socio = SocioPeer::retrieveByPK($grupoAlumno->getAlumnoId());
                            $alumnosActivos[$grupo->getId() ][]       = array('id' => $socio->getId(), 'usuario' => $socio->getUsuario(), 'nombre' => $socio->getNombreCompleto(), 'edad' => $socio->getEdad(), 'ocupacion' => $socio->getOcupacionId(), 'sexo' => $socio->getSexo());
                        }
                    }
                }
            }
        }

        return array('activos'                            => $alumnosActivos, 'resultados'                            => ($grupoId ? $resultadosGrupo[$grupoId] : $resultadosGrupo));
    }
    /**
     * Indicador para obtener la informacion de los socios con actividad programada
     * @param unknown_type $resultadosGrupo
     * @param unknown_type $fehca
     * @return array - Arreglo con los datos de los socios y los grupos con actividad programada
     */
    public function alumnosActividadProgramada($resultadosGrupo, $fecha, $centroId, $grupoId                    = false) {
        $alumnosActividadProgramada = array();
        // Buscamos todas las actividades del dia
        $criteriaActivos            = new Criteria();
        $criteriaActivos->add(FechasHorarioPeer::FECHA, date('Y-m-d', $fecha));
        $criteriaActivos->addJoin(FechasHorarioPeer::HORARIO_ID, HorarioPeer::ID);
        $criteriaActivos->addJoin(HorarioPeer::ID, GrupoPeer::HORARIO_ID);
        //error_log('Activiada Programada');
        //error_log($criteriaActivos->toString());
        //$criteriaActivos->add(GrupoPeer::ACTIVO, TRUE);
        if ($grupoId) {
            $criteriaActivos->add(GrupoPeer::ID, $grupoId);
        }
        $criteriaActivos->addJoin(GrupoPeer::CENTRO_ID, $centroId);
        foreach (FechasHorarioPeer::doSelect($criteriaActivos) as $fechaHorario) {
            $criteriaGrupo = new Criteria();
            $criteriaGrupo->add(GrupoPeer::HORARIO_ID, $fechaHorario->getHorarioId());
            $grupo = GrupoPeer::doSelectOne($criteriaGrupo);
            //error_log($criteriaGrupo->toString());
            // Por cada socio inscrito al grupo guaramos los datos del alumno que deberia asistir
            if ($grupo) {
                foreach ($grupo->getGrupoAlumnoss() as $grupoAlumno) {
                    if (!$grupoAlumno->getPreinscrito()) {
                        if (!array_key_exists($grupo->getId(), $resultadosGrupo)) {
                            $resultadosGrupo[$grupo->getId() ]['centro']       = $centroId;
                            $resultadosGrupo[$grupo->getId() ]['grupo']       = $grupo->getId();
                            $resultadosGrupo[$grupo->getId() ]['curso']       = $grupo->getCursoId();
                            $resultadosGrupo[$grupo->getId() ]['facilitador']       = $grupo->getFacilitadorId();
                            $resultadosGrupo[$grupo->getId() ]['categoria']       = $grupo->getCurso()->getCategoriaCursoId();
                            $resultadosGrupo[$grupo->getId() ]['aula']       = $grupo->getAulaId();
                            $resultadosGrupo[$grupo->getId() ]['alumnosActividadProgramada'][]       = $grupoAlumno->getAlumnoId();
                        } else {
                            if (!in_array($grupoAlumno->getAlumnoId(), $resultadosGrupo[$grupo->getId() ]['alumnosActividadProgramada'])) {
                                $resultadosGrupo[$grupo->getId() ]['alumnosActividadProgramada'][]       = $grupoAlumno->getAlumnoId();
                            }
                        }
                        $socio = SocioPeer::retrieveByPK($grupoAlumno->getAlumnoId());
                        $alumnosActividadProgramada[$grupo->getId() ][]       = array('id' => $socio->getId(), 'usuario' => $socio->getUsuario(), 'nombre' => $socio->getNombreCompleto(), 'edad' => $socio->getEdad(), 'ocupacion' => $socio->getOcupacionId(), 'sexo' => $socio->getSexo());
                    }
                }
            }
        }

        return array('programada'                   => $alumnosActividadProgramada, 'resultados'                   => ($grupoId ? $resultadosGrupo[$grupoId] : $resultadosGrupo));
    }
    /**
     * Indicador que obtiene la cantidad de alumnos graduados
     * @param unknown_type $resultadosGrupo
     * @param unknown_type $fecha
     * @return array - cantidad de alumnos graduados en un dia determinado
     */
    public function alumnosGraduados($resultadosGrupo, $fecha, $centroId, $grupoId           = false) {
        $criteriaGraduados = new Criteria();
        $criteriaGraduados->add(HorarioPeer::FECHA_FIN, date('Y-m-d', $fecha) . ' 00:00:00');
        $criteriaGraduados->addJoin(HorarioPeer::ID, GrupoPeer::HORARIO_ID);
        $criteriaGraduados->add(GrupoPeer::ACTIVO, true);
        $criteriaGraduados->add(GrupoPeer::CENTRO_ID, $centroId);
        if ($grupoId) {
            $criteriaGraduados->add(GrupoPeer::ID, $grupoId);
        }
        $criteriaGraduados->addJoin(GrupoPeer::ID, GrupoAlumnosPeer::GRUPO_ID);
        //////error_log($criteriaGraduados->toString());

        //error_log('Alumnos graduados');
        //error_log($criteriaGraduados->toString());

        foreach (GrupoAlumnosPeer::doSelect($criteriaGraduados) as $grupoAlumnos) {
            // Por cada alumnos inscrito al grupo buscamos si aprobo el cruso en Moodle
            $criteriaCursosFinalizados = new Criteria();
            $criteriaCursosFinalizados->add(CursosFinalizadosPeer::GRUPO_ID, $grupoAlumnos->getGrupoId());
            $criteriaCursosFinalizados->add(CursosFinalizadosPeer::CURSO_ID, $grupoAlumnos->getGrupo()->getCursoId());
            $criteriaCursosFinalizados->add(CursosFinalizadosPeer::ALUMNO_ID, $grupoAlumnos->getAlumnoId());
            $criteriaCursosFinalizados->add(CursosFinalizadosPeer::PROMEDIO_FINAL, 80, Criteria::GREATER_EQUAL);
            $calificacionFinal = CursosFinalizadosPeer::doSelectOne($criteriaCursosFinalizados);
            //error_log($criteriaCursosFinalizados->toString());

            if ($calificacionFinal) {
                if (!array_key_exists($grupoAlumnos->getGrupoId(), $resultadosGrupo)) {
                    ////error_log('almacenado graduados');
                    $resultadosGrupo[$grupoAlumnos->getGrupoId() ]['centro']                   = $centroId;
                    $resultadosGrupo[$grupoAlumnos->getGrupoId() ]['grupo']                   = $grupoAlumnos->getGrupoId();
                    $resultadosGrupo[$grupoAlumnos->getGrupoId() ]['curso']                   = $grupoAlumnos->getGrupo()->getCursoId();
                    $resultadosGrupo[$grupoAlumnos->getGrupoId() ]['facilitador']                   = $grupoAlumnos->getGrupo()->getFacilitadorId();
                    $resultadosGrupo[$grupoAlumnos->getGrupoId() ]['categoria']                   = $grupoAlumnos->getGrupo()->getCurso()->getCategoriaCursoId();
                    $resultadosGrupo[$grupoAlumnos->getGrupoId() ]['aula']                   = $grupoAlumnos->getGrupo()->getAulaId();
                    $resultadosGrupo[$grupoAlumnos->getGrupoId() ]['alumnosGraduados']                   = 1;
                } else {
                    $resultadosGrupo[$grupoAlumnos->getGrupoId() ]['alumnosGraduados']+= 1;
                }
            }
        }

        return ($grupoId ? $resultadosGrupo[$grupoId] : $resultadosGrupo);
    }
    /**
     * Indicador que obtiene la cantidad de alumnos reprobados
     * @param unknown_type $resultadosGrupo
     * @param unknown_type $fecha
     * @return array - cantidad de alumnos reprobados en un dia determinado
     */
    public function alumnosReprobados($resultadosGrupo, $fecha, $centroId, $grupoId           = false) {
        $criteriaGraduados = new Criteria();
        $criteriaGraduados->add(HorarioPeer::FECHA_FIN, date('Y-m-d', $fecha) . ' 00:00:00');
        $criteriaGraduados->addJoin(HorarioPeer::ID, GrupoPeer::HORARIO_ID);
        $criteriaGraduados->add(GrupoPeer::ACTIVO, true);
        $criteriaGraduados->add(GrupoPeer::CENTRO_ID, $centroId);
        if ($grupoId) {
            $criteriaGraduados->add(GrupoPeer::ID, $grupoId);
        }
        $criteriaGraduados->addJoin(GrupoPeer::ID, GrupoAlumnosPeer::GRUPO_ID);
        //error_log('Alumnos reprobados');
        //error_log($criteriaGraduados->toString());

        foreach (GrupoAlumnosPeer::doSelect($criteriaGraduados) as $grupoAlumnos) {
            // Por cada alumnos inscrito al grupo buscamos si reprobo el cruso en Moodle
            $criteriaCursosFinalizados = new Criteria();
            $criteriaCursosFinalizados->add(CursosFinalizadosPeer::GRUPO_ID, $grupoAlumnos->getGrupoId());
            $criteriaCursosFinalizados->add(CursosFinalizadosPeer::CURSO_ID, $grupoAlumnos->getGrupo()->getCursoId());
            $criteriaCursosFinalizados->add(CursosFinalizadosPeer::ALUMNO_ID, $grupoAlumnos->getAlumnoId());
            $criteriaCursosFinalizados->add(CursosFinalizadosPeer::PROMEDIO_FINAL, 80, Criteria::LESS_THAN);
            //error_log($criteriaCursosFinalizados->toString());

            $calificacionFinal = CursosFinalizadosPeer::doSelectOne($criteriaCursosFinalizados);
            if ($calificacionFinal) {
                if (!array_key_exists($grupoAlumnos->getGrupoId(), $resultadosGrupo)) {
                    ////error_log('almacenado graduados');
                    $resultadosGrupo[$grupoAlumnos->getGrupoId() ]['centro']                   = $centroId;
                    $resultadosGrupo[$grupoAlumnos->getGrupoId() ]['grupo']                   = $grupoAlumnos->getGrupoId();
                    $resultadosGrupo[$grupoAlumnos->getGrupoId() ]['curso']                   = $grupoAlumnos->getGrupo()->getCursoId();
                    $resultadosGrupo[$grupoAlumnos->getGrupoId() ]['facilitador']                   = $grupoAlumnos->getGrupo()->getFacilitadorId();
                    $resultadosGrupo[$grupoAlumnos->getGrupoId() ]['categoria']                   = $grupoAlumnos->getGrupo()->getCurso()->getCategoriaCursoId();
                    $resultadosGrupo[$grupoAlumnos->getGrupoId() ]['aula']                   = $grupoAlumnos->getGrupo()->getAulaId();
                    $resultadosGrupo[$grupoAlumnos->getGrupoId() ]['alumnosReprobados']                   = 1;
                } else {
                    $resultadosGrupo[$grupoAlumnos->getGrupoId() ]['alumnosReprobados']+= 1;
                }
            }
        }

        return ($grupoId ? $resultadosGrupo[$grupoId] : $resultadosGrupo);
    }
    /**
     * Indicador que muestra la cantidad de graduados que se esperaba obtener
     * @param unknown_type $resultadosGrupo
     * @param unknown_type $fecha
     * @return array - Arreglo con los datos de alumnos que se esperaba se graduaran en un grupo
     */
    public function graduacionEsperada($resultadosGrupo, $fecha, $centroId, $grupoId                    = false) {
        $criteriaGraduacionEsperada = new Criteria();
        $criteriaGraduacionEsperada->add(HorarioPeer::FECHA_FIN, date('Y-m-d', $fecha) . ' 00:00:00');
        $criteriaGraduacionEsperada->addJoin(HorarioPeer::ID, GrupoPeer::HORARIO_ID);
        $criteriaGraduacionEsperada->add(GrupoPeer::ACTIVO, true);
        if ($grupoId) {
            $criteriaGraduacionEsperada->add(GrupoPeer::ID, $grupoId);
        }
        //error_log('Graduacion esperada');
        //error_log($criteriaGraduacionEsperada->toString());

        $criteriaGraduacionEsperada->add(GrupoPeer::CENTRO_ID, $centroId);
        foreach (GrupoPeer::doSelect($criteriaGraduacionEsperada) as $grupo) {
            $resultadosGrupo[$grupo->getId() ]['centro'] = $centroId;
            $resultadosGrupo[$grupo->getId() ]['grupo'] = $grupo->getId();
            $resultadosGrupo[$grupo->getId() ]['curso'] = $grupo->getCursoId();
            $resultadosGrupo[$grupo->getId() ]['facilitador'] = $grupo->getFacilitadorId();
            $resultadosGrupo[$grupo->getId() ]['categoria'] = $grupo->getCurso()->getCategoriaCursoId();
            $resultadosGrupo[$grupo->getId() ]['aula'] = $grupo->getAulaId();
            $resultadosGrupo[$grupo->getId() ]['graduacionEsperada'] = $grupo->getCupoActual();
        }

        return ($grupoId ? $resultadosGrupo[$grupoId] : $resultadosGrupo);
    }
    /**
     * Indicador para mostrar la cantidad de alumnos que desertan de un grupo
     * @param unknown_type $resultadosGrupo
     * @param unknown_type $fecha
     * @return array - Arreglo con la cantidad de alumnos desertores por grupo
     */
    public function alumnosDesertores($resultadosGrupo, $fecha, $centroId, $grupoId           = false) {
        ////error_log('alumnos desertores');
        $criteriaGraduados = new Criteria();
        $criteriaGraduados->add(HorarioPeer::FECHA_FIN, date('Y-m-d', $fecha) . ' 00:00:00');
        $criteriaGraduados->addJoin(HorarioPeer::ID, GrupoPeer::HORARIO_ID);
        $criteriaGraduados->add(GrupoPeer::ACTIVO, true);
        if ($grupoId) {
            $criteriaGraduados->add(GrupoPeer::ID, $grupoId);
        }
        $criteriaGraduados->add(GrupoPeer::CENTRO_ID, $centroId);
        $criteriaGraduados->addJoin(GrupoPeer::ID, GrupoAlumnosPeer::GRUPO_ID);
        //error_log('Alumnos Desertores');
        //error_log($criteriaGraduados->toString());

        foreach (GrupoAlumnosPeer::doSelect($criteriaGraduados) as $grupoAlumnos) {
            $limite = 0;
            $c3     = new Criteria();
            $c3->add(AlumnoPagosPeer::SOCIO_ID, $grupoAlumnos->getAlumnoId());
            $c3->add(AlumnoPagosPeer::CURSO_ID, $grupoAlumnos->getGrupo()->getCursoId());
            $c3->add(AlumnoPagosPeer::GRUPO_ID, $grupoAlumnos->getGrupoId());
            //error_log($c3->toString());

            $alumnoPago      = AlumnoPagosPeer::doSelectOne($c3);
            if ($alumnoPago != null) {
                //Obtenemos el orden_id de este pago
                $orden           = $alumnoPago->getOrden();
                $sub_producto_de = $alumnoPago->getSubProductoDe();
                if ($orden != null) {
                    //Obtenemos los detalle_orden_id relacionados con esta orden
                    $detalleOrdenes  = $orden->getDetalleOrdens();
                    foreach ($detalleOrdenes as $detalleOrden) {
                        foreach ($detalleOrden->getSocioPagoss() as $socioPago) {
                            // Obtenemos los ultimos dos pagos para este grupo
                            $orden_base_id   = $socioPago->getOrdenBaseId();
                            $c4              = new Criteria();
                            $c4->add(SocioPagosPeer::ORDEN_BASE_ID, $orden_base_id);
                            $c4->add(SocioPagosPeer::SUB_PRODUCTO_DE, $sub_producto_de);
                            $c4->add(SocioPagosPeer::CANCELADO, false);
                            $c4->setLimit(2);
                            $c4->addDescendingOrderByColumn(SocioPagosPeer::FECHA_A_PAGAR);
                            //error_log($c4->toString());
                            foreach (SocioPagosPeer::doSelect($c4) as $ultimosPagos) {
                                if (!$ultimosPagos->getPagado()) {
                                    $limite++;
                                }
                            }
                        }
                    }
                }
                // Si no cubrio los ultimos 2 pagos buscamos si asistio las ultimas 2 fechas
                if ($limite == 2) {
                    $asistencias           = 0;
                    $criteriaUltimasFechas = new Criteria();
                    $criteriaUltimasFechas->add(FechasHorarioPeer::HORARIO_ID, $grupoAlumnos->getGrupo()->getHorarioId());
                    $criteriaUltimasFechas->setLimit(2);
                    $criteriaUltimasFechas->addDescendingOrderByColumn(FechasHorarioPeer::FECHA);
                    //error_log($criteriaUltimasFechas);
                    foreach (FechasHorarioPeer::doSelect($criteriaUltimasFechas) as $fechaHorario) {
                        $criteriaActividad = new Criteria();
                        $criteriaActividad->add(SesionHistoricoPeer::CENTRO_ID, $centroId);
                        $criteriaActividad->add(SesionHistoricoPeer::SOCIO_ID, $grupoAlumnos->getAlumnoId());
                        $desdeF = $fechaHorario->getFecha('Y-m-d') . ' ' . $fechaHorario->getHoraInicio('H:i:s');
                        $hastaF = $fechaHorario->getFecha('Y-m-d') . ' ' . $fechaHorario->getHoraFin('H:i:s');
                        $criteriaActividad->add(SesionHistoricoPeer::FECHA_LOGIN, SesionHistoricoPeer::FECHA_LOGIN . " BETWEEN '" . $desdeF . "' AND '" . $hastaF . "'", Criteria::CUSTOM);
                        //error_log($criteriaActividad->toString());
                        if (SesionHistoricoPeer::doCount($criteriaActividad) > 0) {
                            $asistencias++;
                        }
                    }
                    // Si no asistio a las ultimas de fechas se considera desertor
                    if ($asistencias == 0) {
                        if (!array_key_exists($grupoAlumnos->getGrupoId(), $resultadosGrupo)) {
                            $resultadosGrupo[$grupoAlumnos->getGrupoId() ]['centro'] = $centroId;
                            $resultadosGrupo[$grupoAlumnos->getGrupoId() ]['grupo'] = $grupoAlumnos->getGrupoId();
                            $resultadosGrupo[$grupoAlumnos->getGrupoId() ]['curso'] = $grupoAlumnos->getGrupo()->getCursoId();
                            $resultadosGrupo[$grupoAlumnos->getGrupoId() ]['facilitador'] = $grupoAlumnos->getGrupo()->getFacilitadorId();
                            $resultadosGrupo[$grupoAlumnos->getGrupoId() ]['categoria'] = $grupoAlumnos->getGrupo()->getCurso()->getCategoriaCursoId();
                            $resultadosGrupo[$grupoAlumnos->getGrupoId() ]['aula'] = $grupoAlumnos->getGrupo()->getAulaId();
                            $resultadosGrupo[$grupoAlumnos->getGrupoId() ]['alumnosDesertores'] = 1;
                        } else {
                            $resultadosGrupo[$grupoAlumnos->getGrupoId() ]['alumnosDesertores']+= 1;
                        }
                    }
                }
            }
        }

        return ($grupoId ? $resultadosGrupo[$grupoId] : $resultadosGrupo);
    }
    /**
     * Indicador que obtiene el numero de pagos programados y la cobranza de estos
     * @param unknown_type $resultadosGrupo
     * @param unknown_type $fecha
     * @param unknown_type $centroId
     * @return array - Arreglo con los datos de los pagos y la cobranza por grupo
     */
    public function cobranzaProgramada($resultadosGrupo, $fecha, $centroId, $grupoId    = false) {
        $sql        = "SELECT
        r.grupo_id,
        r.fecha_inicio, --Fecha de inicio del grupo
        r.fecha_fin,    --Fecha de fin del grupo
        r.activo,       --Bandera que indica si fue cancelado o no
        r.finalizado,   --Indica si ya finalizó el grupo (true) o no (false)
        m.socio_id,
        m.detalle_orden_id,
        m.orden_base_id,
        m.orden_id as orden_id,
        m.fecha_a_pagar,
        m.pagado,
        m.fecha_pagado,
        m.subtotal as subtotal_pagado,
        m.subtotal_a_pagar

        FROM (

        SELECT
        sp.detalle_orden_id,
        sp.socio_id,
        sp.orden_base_id,
        d.orden_id as orden_id,
        sp.fecha_a_pagar,
        sp.pagado,
        sp.fecha_pagado,
        d.subtotal,
        sp.subtotal as subtotal_a_pagar
        FROM
        socio_pagos AS sp
        LEFT JOIN
        detalle_orden AS d ON (d.id = sp.detalle_orden_id)
        WHERE sp.fecha_a_pagar = '" . date('Y-m-d', $fecha) . " 00:00:00'
        AND sp.centro_id = " . $centroId . "

        )
        AS m --Convertimos el primer select en una subtabla al vuelo

        LEFT JOIN
        (
        select
        ap.grupo_id,
        sp.orden_base_id,
        d2.orden_id,
        sp.detalle_orden_id,
        h.fecha_inicio,
        h.fecha_fin,
        g.activo,
        (CURRENT_DATE > h.fecha_fin)::boolean as finalizado
        FROM socio_pagos as sp
        LEFT JOIN detalle_orden as d2 on (d2.id = detalle_orden_id)
        JOIN alumno_pagos as ap on (ap.orden_base_id = d2.orden_id)
        JOIN grupo as g on (ap.grupo_id = g.id)
        JOIN horario as h on (h.id = g.horario_id)
        where sp.pagado = true
        " . ($grupoId ? "AND g.id=" . $grupoId : "") . "
        ORDER BY h.fecha_fin
        )
        AS r --Convertimos el segundo select en otra tabla para hacer join con el primero
        ON (r.orden_base_id = m.orden_base_id)";
        $connection = Propel::getConnection(ProductoPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        $statement  = $connection->prepare($sql);
        $statement->execute();
        $pagosProgramados = array();
        while ($pagos            = $statement->fetch(PDO::FETCH_ASSOC)) {
            // Comprobamos que la fecha de de fin de curso no sea mayor a la fecha a pagar
            $fechaPagar       = explode(' ', $pagos['fecha_a_pagar']);
            $fechaPagar       = explode('-', $fechaPagar[0]);
            $fechaFin         = explode(' ', $pagos['fecha_fin']);
            $fechaFin         = explode('-', $fechaFin[0]);
            if ((int)$fechaPagar[0] . $fechaPagar[1] . $fechaPagar[2] <= (int)$fechaFin[0] . $fechaFin[1] . $fechaFin[2]) {
                if (!array_key_exists($pagos['socio_id'], $pagosProgramados[$pagos['grupo_id']])) {
                    $pagosProgramados[$pagos['grupo_id']][$pagos['socio_id']]                  = array('aPagar'                  => $pagos['subtotal_a_pagar']);
                }
            }
        }
        //Contamos los pagos programados y la cobranza esperada
        foreach ($pagosProgramados as $grupoKey         => $cobranzaEsperada) {
            if ($grupoKey) {
                $grupo            = GrupoPeer::retrieveByPK($grupoKey);
                if (!array_key_exists($grupoKey, $resultadosGrupo)) {
                    $totalCobranza    = 0;
                    foreach ($cobranzaEsperada as $cobranza) {
                        $totalCobranza+= $cobranza['aPagar'];
                    }
                    $resultadosGrupo[$grupoKey]['centro'] = $centroId;
                    $resultadosGrupo[$grupoKey]['grupo'] = $grupoKey;
                    $resultadosGrupo[$grupoKey]['curso'] = $grupo->getCursoId();
                    $resultadosGrupo[$grupoKey]['facilitador'] = $grupo->getFacilitadorId();
                    $resultadosGrupo[$grupoKey]['categoria'] = $grupo->getCurso()->getCategoriaCursoId();
                    $resultadosGrupo[$grupoKey]['aula'] = $grupo->getAulaId();
                    $resultadosGrupo[$grupoKey]['pagosProgramados'] = count($cobranzaEsperada);
                    $resultadosGrupo[$grupoKey]['cobranzaProgramada'] = $totalCobranza;
                } else {
                    $resultadosGrupo[$grupoKey]['pagosProgramados']+= count($cobranzaEsperada);
                    $resultadosGrupo[$grupoKey]['cobranzaProgramada']+= $totalCobranza;
                }
            }
        }

        return ($grupoId ? $resultadosGrupo[$grupoId] : $resultadosGrupo);
    }
    /**
     * Indicador que obtuiene el numero de pagos realizados, la cobranza de estos y por cada pago realizado
     * agrega un alumnos activo al arreglo de alumnosActivos
     * @param unknown_type $resultadosGrupo
     * @param unknown_type $fecha
     * @param unknown_type $centroId
     * @param unknown_type $alumnosActivos
     * @return array - Arreglo con los datos de pagos, cobranza y alumnos activos
     */
    public function cobranzaCobrada($resultadosGrupo, $fecha, $centroId, $alumnosActivos, $grupoId    = false) {
        $sql        = "SELECT
        r.grupo_id,
        r.clave,
        r.fecha_inicio, --Fecha de inicio del grupo
        r.fecha_fin,    --Fecha de fin del grupo
        r.activo,       --Bandera que indica si fue cancelado o no
        r.finalizado,   --Indica si ya finalizó el grupo (true) o no (false)
        m.socio_id,
        m.detalle_orden_id,
        m.orden_base_id,
        m.orden_id as orden_id,
        m.fecha_a_pagar,
        m.pagado,
        m.fecha_pagado,
        m.subtotal as subtotal_pagado,
        m.subtotal_a_pagar,
        m.codigo
        FROM (

        SELECT
        sp.detalle_orden_id,
        sp.socio_id,
        sp.orden_base_id,
        d.orden_id as orden_id,
        sp.fecha_a_pagar,
        sp.pagado,
        sp.fecha_pagado,
        d.subtotal,
        sp.subtotal as subtotal_a_pagar,
        p.codigo
        FROM
        socio_pagos AS sp
        LEFT JOIN
        detalle_orden AS d ON (d.id = sp.detalle_orden_id)
        JOIN producto as p on (sp.producto_id = p.id)
        WHERE sp.fecha_pagado BETWEEN '" . date('Y-m-d', $fecha) . " 00:00:00' AND '" . date('Y-m-d', $fecha) . " 23:59:59'
        AND sp.centro_id = " . $centroId . "
        AND sp.pagado = true
        AND sp.cancelado = false
        )
        AS m --Convertimos el primer select en una subtabla al vuelo

        LEFT JOIN
        (
        select
        ap.grupo_id,
        c.clave,
        sp.orden_base_id,
        d2.orden_id,
        sp.detalle_orden_id,
        h.fecha_inicio,
        h.fecha_fin,
        g.activo,
        (CURRENT_DATE > h.fecha_fin)::boolean as finalizado
        FROM socio_pagos as sp
        LEFT JOIN detalle_orden as d2 on (d2.id = detalle_orden_id)
        JOIN alumno_pagos as ap on (ap.orden_base_id = d2.orden_id)
        JOIN grupo as g on (ap.grupo_id = g.id)
        JOIN curso as c on (g.curso_id = c.id)
        JOIN horario as h on (h.id = g.horario_id)
        where sp.pagado = true
        AND g.activo = true
        " . ($grupoId ? "AND g.id=" . $grupoId : "") . "
        )
        AS r --Convertimos el segundo select en otra tabla para hacer join con el primero
        ON (r.orden_base_id = m.orden_base_id)";
        $connection = Propel::getConnection(ProductoPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        $statement  = $connection->prepare($sql);
        $statement->execute();
        $pagosPagados = array();

        while ($pagos        = $statement->fetch(PDO::FETCH_ASSOC)) {
            // Comprobamos que la fecha de de fin de curso no sea mayor a la fecha a pagar
            $fechaPagar   = explode(' ', $pagos['fecha_a_pagar']);
            $fechaPagar   = explode('-', $fechaPagar[0]);
            $fechaFin     = explode(' ', $pagos['fecha_fin']);
            $fechaFin     = explode('-', $fechaFin[0]);
            //$this->log('Fecha Pagar: '.$pagos['fecha_a_pagar']);
            //$this->log('Fecha Pagado: '.$pagos['fecha_pagado']);
            //$this->log('Fecha Fin: '.$pagos['fecha_fin']);
            //$this->log((int) $fechaPagar[0].$fechaPagar[1].$fechaPagar[2]);
            //$this->log((int) $fechaFin[0].$fechaFin[1].$fechaFin[2]);
            //sleep(5);
            if ((int)$fechaPagar[0] . $fechaPagar[1] . $fechaPagar[2] <= (int)$fechaFin[0] . $fechaFin[1] . $fechaFin[2]) {
                // Comprobamos que el codigo del producto sea igual a la clave del grupo
                if ($pagos['clave'] == $pagos['codigo']) {
                    if (!array_key_exists($pagos['socio_id'], $pagosPagados[$pagos['grupo_id']])) {
                        $pagosPagados[$pagos['grupo_id']][$pagos['socio_id']]              = array('pagado' => $pagos['subtotal_pagado']);
                    } else {
                        $pagosPagados[$pagos['grupo_id']][$pagos['socio_id']]['pagado']+= $pagos['subtotal_pagado'];
                    }
                }
            } else {
                $this->noContado+= $pagos['subtotal_pagado'];
                $this->log('La fecha es mayor : ' . $this->noContado);
            }
        }
        //Contamos los pagos programados y la cobranza esperada
        foreach ($pagosPagados as $grupoKey      => $cobranzaEfectiva) {
            if ($grupoKey) {
                $grupo         = GrupoPeer::retrieveByPK($grupoKey);
                if ($grupo) {
                    if (!array_key_exists($grupoKey, $resultadosGrupo)) {
                        $totalCobranza = 0;
                        foreach ($cobranzaEfectiva as $socio => $cobranza) {
                            $totalCobranza+= $cobranza['pagado'];
                            if (!in_array($socio, $resultadosGrupo[$grupo->getId() ]['alumnosActivos'])) {
                                if (!array_key_exists($grupoKey, $resultadosGrupo)) {
                                    $this->log('');
                                    $resultadosGrupo[$grupoKey]['centro']            = $centroId;
                                    $resultadosGrupo[$grupoKey]['grupo']            = $grupoKey;
                                    $resultadosGrupo[$grupoKey]['curso']            = $grupo->getCursoId();
                                    $resultadosGrupo[$grupoKey]['facilitador']            = $grupo->getFacilitadorId();
                                    $resultadosGrupo[$grupoKey]['categoria']            = $grupo->getCurso()->getCategoriaCursoId();
                                    $resultadosGrupo[$grupoKey]['aula']            = $grupo->getAulaId();
                                    $resultadosGrupo[$grupoKey]['alumnosActivos'][]            = $socio;
                                } else {
                                    $resultadosGrupo[$grupoKey]['alumnosActivos'][]            = $socio;
                                }
                            }
                            $socioDatos = SocioPeer::retrieveByPK($socio);
                            $alumnosActivos[$grupo->getId() ][]            = array('id'            => $socioDatos->getId(), 'usuario'            => $socioDatos->getUsuario(), 'nombre'            => $socioDatos->getNombreCompleto(), 'edad'            => $socioDatos->getEdad(), 'ocupacion'            => $socioDatos->getOcupacionId(), 'sexo'            => $socioDatos->getSexo());
                        }
                        $resultadosGrupo[$grupoKey]['centro']            = $centroId;
                        $resultadosGrupo[$grupoKey]['grupo']            = $grupoKey;
                        $resultadosGrupo[$grupoKey]['curso']            = $grupo->getCursoId();
                        $resultadosGrupo[$grupoKey]['facilitador']            = $grupo->getFacilitadorId();
                        $resultadosGrupo[$grupoKey]['categoria']            = $grupo->getCurso()->getCategoriaCursoId();
                        $resultadosGrupo[$grupoKey]['aula']            = $grupo->getAulaId();
                        $resultadosGrupo[$grupoKey]['pagosCobrados']            = count($cobranzaEfectiva);
                        $resultadosGrupo[$grupoKey]['cobranzaCobrada']            = $totalCobranza;
                    } else {
                        $resultadosGrupo[$grupoKey]['pagosCobrados']+= count($cobranzaEfectiva);
                        $resultadosGrupo[$grupoKey]['cobranzaCobrada']+= $totalCobranza;
                    }
                }
            }
        }

        return array('activos'             => $alumnosActivos, 'resultados'             => $resultadosGrupo);
    }
    /**
     **
     ** MISCELANEA DE FUNCIONES
     **
     **
     */
    /**
     * Funcion que obtiene la fecha del ultimo reporte;
     * @param string $reporte - Tipo de reporte que se obtendra;
     * @return multitype:NULL Array - Arreglo con la ultima fecha que se obtuvo el reporte
     */
    public function generaFechas($reporte, $centroId, $opciones) {
        // Si se indica la fecha hasta la cual se debe generar el reporte
        if ($opciones['hasta']) {
            $hasta       = explode('-', $opciones['hasta']);
            // Verificamos que el arreglo resultante de la fecha de inicio sea igual a 2
            if (count($hasta) == 3) {
                // Verificamos el formato de la fecha ingresada
                if ((int)$hasta[0] <= 31 && (int)$hasta[1] <= 12 && $hasta[2] >= 2009) {
                    $ultimaFecha = mktime(0, 0, 0, (int)$hasta[1], (int)$hasta[0], $hasta[2]);
                    $this->log('Fecha ingresada ' . date('d/m/Y', $ultimaFecha));
                    $hasta = date('Ymd', $ultimaFecha);
                } else {
                    throw new sfException('El formato de la fecha de fin es incorecto (dd-mm-aaaa)');
                }
            } else {
                throw new sfException('El formato de la fecha de incorrecto es incorecto (dd-mm-aaaa)');
            }
        } else {
            $hasta       = date('Ymd');
        }
        // Si se indica la fecha a partir de la cual se debe generar el reporte
        if ($opciones['desde']) {
            $desde       = explode('-', $opciones['desde']);
            // Verificamos que el arreglo resultante de la fecha de inicio sea igual a 2
            if (count($desde) == 3) {
                // Verificamos el formato de la fecha ingresada
                if ((int)$desde[0] <= 31 && (int)$desde[1] <= 12 && $desde[2] >= 2009) {
                    $primerFecha = mktime(0, 0, 0, (int)$desde[1], (int)$desde[0] - 1, $desde[2]);
                    $this->log('Fecha ingresada ' . date('d/m/Y', $primerFecha));

                    return array('desde' => array('j' => date('j', $primerFecha), 'n' => date('n', $primerFecha), 'Y' => date('Y', $primerFecha)), 'hasta' => $hasta);
                } else {
                    throw new sfException('El formato de la fecha de inicio es incorecto (dd-mm-aaaa)');
                }
            } else {
                throw new sfException('El formato de la fecha de inicio es incorecto (dd-mm-aaaa)');
            }
        }
        // De los contrario se busca el ultimo dia que se guardo el reporte
        else {
            $this->logSection('Buscando el ultimo día que se guardo el reporte', '...');
            $centroId    = sfConfig::get('app_centro_actual_id');
            $ultimaFecha = false;
            // Si no es un overwrite de datos buscamos la ultima fecha guardada
            if ($opciones['overwrite'] == 0) {
                // Buscamos la ultima fecha a partir de la cual se generara el reporte
                $criteria    = new Criteria();
                // Comprobamos cual es el tipo de reporte que se esta obteniendo
                if ($reporte == 'dia') {
                    $criteria->add(ReporteIndicadoresDiaPeer::CENTRO_ID, $centroId);
                    $criteria->addDescendingOrderByColumn(ReporteIndicadoresDiaPeer::FECHA);
                    $ultimaFecha = ReporteIndicadoresDiaPeer::doSelectOne($criteria);
                }
                if ($reporte == 'grupo') {
                    $criteria->add(ReporteIndicadoresDiaGrupoPeer::CENTRO_ID, $centroId);
                    $criteria->addDescendingOrderByColumn(ReporteIndicadoresDiaGrupoPeer::FECHA);
                    $ultimaFecha = ReporteIndicadoresDiaGrupoPeer::doSelectOne($criteria);
                }
            }
            if ($ultimaFecha) {
                $this->log('Fecha del ultimo reporte: ' . $ultimaFecha->getFecha('d/m/Y'));
                // Si hay registros en la tabla obtenemos la ultima fecha de registro
                return array('desde' => array('j' => $ultimaFecha->getFecha('j'), 'n' => $ultimaFecha->getFecha('n'), 'Y' => $ultimaFecha->getFecha('Y')), 'hasta' => $hasta);
            } else {
                // Si no hay registros en la tabla tomamos como primer fecha la primer fecha de en la que se efectuo un registro de socio
                if ($opciones['overwrite'] == 0) {
                    $this->logSection('No hay fecha del reporte', 'obteniendo la primer operacion del centro');
                } else {
                    $this->logSection('Overwrite de datos', 'obteniendo la primer operacion del centro');
                }
                $criteria = new Criteria();
                $criteria->add(SocioPeer::CENTRO_ID, $centroId);
                $criteria->addAscendingOrderByColumn(SocioPeer::CREATED_AT);
                $primerFecha = SocioPeer::doSelectOne($criteria);
                if (!$primerFecha) {
                    $this->log('El centro es nuevo');
                    return false;
                }
                $this->log('Fecha inicial ' . $primerFecha->getCreatedAt('d/m/Y'));
                // Generamos la primer fecha
                $primerFecha = mktime(0, 0, 0, $primerFecha->getCreatedAt('n'), $primerFecha->getCreatedAt('j') - 1, $primerFecha->getCreatedAt('Y'));
                return array('desde'                            => array('j'                            => date('j', $primerFecha), 'n'                            => date('n', $primerFecha), 'Y'                            => date('Y', $primerFecha)), 'hasta'                            => $hasta);
            }
        }
    }
    /**
     * Funcion que obtiene el reporte de indicadores por dia y grupo
     * @param unknown_type $opciones
     * @param unknown_type $fecha
     * @param unknown_type $centroId
     * @return Ambigous <unknown, multitype:, unknown_type, number>
     */
    public function creaReporteIndicadoresDiaGrupo($opciones, $fecha, $centroId, $indicadores, $overrideNuevo              = false, $grupoId                    = false) {
        $resultadosGrupo            = array();
        $alumnosActivos             = array();
        $alumnosInscritos           = array();
        $alumnosPreinscritos        = array();
        $alumnosActividadProgramada = array();
        if ($opciones['overwrite'] == 0 || ($opciones['overwrite'] && in_array('alumnosPreinscritos', $indicadores)) || ($opciones['overwrite'] && in_array('all', $indicadores))) {
            $this->log('Alumnos preinscritos');
            $resultadosGrupo     = $this->alumnosPreinscritos($resultadosGrupo, $fecha, $centroId, $grupoId);
            $alumnosPreinscritos = $resultadosGrupo['preinscritos'];
            $resultadosGrupo     = $resultadosGrupo['resultados'];
        }
        if ($opciones['overwrite'] == 0 || ($opciones['overwrite'] && in_array('alumnosInscritos', $indicadores)) || ($opciones['overwrite'] && in_array('all', $indicadores))) {
            $this->log('Alumnos inscritos');
            $resultadosGrupo  = $this->alumnosInscritos($resultadosGrupo, $fecha, $centroId, $grupoId);
            $alumnosInscritos = $resultadosGrupo['inscritos'];
            $resultadosGrupo  = $resultadosGrupo['resultados'];
        }
        if ($opciones['overwrite'] == 0 || ($opciones['overwrite'] && in_array('alumnosInscritosNuevos', $indicadores)) || ($opciones['overwrite'] && in_array('all', $indicadores))) {
            $this->log('Alumnos inscritos nuevos');
            $resultadosGrupo = $this->alumnosInscritosNuevos($resultadosGrupo, $fecha, $centroId, $grupoId);
        }
        if ($opciones['overwrite'] == 0 || ($opciones['overwrite'] && in_array('alumnosInscritosRecurrentes', $indicadores)) || ($opciones['overwrite'] && in_array('all', $indicadores))) {
            $this->log('Alumnos inscritos recurentes');
            $resultadosGrupo = $this->alumnosInscritosrRecurrentes($resultadosGrupo, $fecha, $centroId, $grupoId);
        }
        if ($opciones['overwrite'] == 0 || ($opciones['overwrite'] && in_array('alumnosActivos', $indicadores)) || ($opciones['overwrite'] && in_array('all', $indicadores))) {
            $this->log('Alumnos activos');
            $resultadosGrupo = $this->alumnosActivos($resultadosGrupo, $fecha, $centroId, $grupoId);
            $alumnosActivos  = $resultadosGrupo['activos'];
            $resultadosGrupo = $resultadosGrupo['resultados'];
        }
        if ($opciones['overwrite'] == 0 || ($opciones['overwrite'] && in_array('actividadProgramada', $indicadores)) || ($opciones['overwrite'] && in_array('all', $indicadores))) {
            $this->log('Alumnos con actividad programada');
            $resultadosGrupo            = $this->alumnosActividadProgramada($resultadosGrupo, $fecha, $centroId, $grupoId);
            $alumnosActividadProgramada = $resultadosGrupo['programada'];
            $resultadosGrupo            = $resultadosGrupo['resultados'];
        }
        if ($opciones['overwrite'] == 0 || ($opciones['overwrite'] && in_array('alumnosGraduados', $indicadores)) || ($opciones['overwrite'] && in_array('all', $indicadores))) {
            $this->log('Alumnos Graduados');
            $resultadosGrupo = $this->alumnosGraduados($resultadosGrupo, $fecha, $centroId, $grupoId);
        }
        if ($opciones['overwrite'] == 0 || ($opciones['overwrite'] && in_array('alumnosReprobados', $indicadores)) || ($opciones['overwrite'] && in_array('all', $indicadores))) {
            $this->log('Alumnos Reprobados');
            $resultadosGrupo = $this->alumnosReprobados($resultadosGrupo, $fecha, $centroId, $grupoId);
        }
        if ($opciones['overwrite'] == 0 || ($opciones['overwrite'] && in_array('graduacionEsperada', $indicadores)) || ($opciones['overwrite'] && in_array('all', $indicadores))) {
            $this->log('Graduacion esperada');
            $resultadosGrupo = $this->graduacionEsperada($resultadosGrupo, $fecha, $centroId, $grupoId);
        }
        if ($opciones['overwrite'] == 0 || ($opciones['overwrite'] && in_array('alumnosDesertores', $indicadores)) || ($opciones['overwrite'] && in_array('all', $indicadores))) {
            $this->log('Alumnos desertores');
            $resultadosGrupo = $this->alumnosDesertores($resultadosGrupo, $fecha, $centroId, $grupoId);
        }
        if ($opciones['overwrite'] == 0 || ($opciones['overwrite'] && in_array('cobranzaProgramada', $indicadores)) || ($opciones['overwrite'] && in_array('all', $indicadores))) {
            $this->log('Cobranza programda');
            $resultadosGrupo = $this->cobranzaProgramada($resultadosGrupo, $fecha, $centroId, $grupoId);
        }
        if ($opciones['overwrite'] == 0 || ($opciones['overwrite'] && in_array('cobranzaCobrada', $indicadores)) || ($opciones['overwrite'] && in_array('all', $indicadores))) {
            $this->log('Cobranza cobrada');
            $resultadosGrupo = $this->cobranzaCobrada($resultadosGrupo, $fecha, $centroId, $alumnosActivos, $grupoId);
            $alumnosActivos  = $resultadosGrupo['activos'];
            $resultadosGrupo = $resultadosGrupo['resultados'];
        }

        return array('resultadosGrupo' => $resultadosGrupo, 'alumnosInscritos' => $alumnosInscritos, 'alumnosPreinscritos' => $alumnosPreinscritos, 'alumnosActivos' => $alumnosActivos, 'actividadProgramada' => $alumnosActividadProgramada);
    }
}
