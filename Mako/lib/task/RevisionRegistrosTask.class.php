<?php
class RevisionRegistrosTask extends sfBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'                              ,  'tolu2' ),
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección'                , 'propel'),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación'               , 'frontend'),
            new sfCommandOption('accion'     , null, sfCommandOption::PARAMETER_REQUIRED, 'Accion a realizar, "consultar", "aplicar"', 'consultar'),
            new sfCommandOption('tipo'       , null, sfCommandOption::PARAMETER_REQUIRED, 'Tipo de movimiento, LDAP o MOODLE'        , 'LDAP')
        ));

        $this->namespace           = 'mako';
        $this->name                = 'revision-registros';
        $this->briefDescription    = 'Revisa registros mako en las tres bases.';
        $this->detailedDescription = <<<EOF

  [./symfony mako:revision-registros|INFO] , puede generar registros desde mako a LDAP, Moodle leer info de ldap, regenerar secuencias en ldap modificar nombres de usuario en LDAP y Moodle.


EOF;
    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $this->con       = $databaseManager->getDatabase($options['connection'] ? $options['connection'] : null)->getConnection();
        $configuration   = ProjectConfiguration::getApplicationConfiguration('frontend', $options['env'], true);
        $context         = sfContext::createInstance($configuration);

        $configuration->loadHelpers('Ldap');

        print $this->formatter->format("\n===Sobreescritura de uids LDAP Inicio a las: " . date("Y-m-d H:i:s") . "===\n", 'INFO');

        if ($options['accion'] == 'actualizar' && $options['tipo'] == 'moodle') {
            $this->actualizarMoodle();
        } elseif ($options['accion'] == 'actualizar' && $options['tipo'] == 'ldap') {
            $this->actualizarLdapuid();
        } elseif ($options['accion'] == 'reset' && $options['tipo'] == 'secuencias') {
            $this->resetSecuenciasLdap();
        } elseif ($options['accion'] == 'consultar' && $options['tipo'] == 'ldap') {
            $this->usuariosEnLdap();
        } elseif ($options['accion'] == 'actualizar' && $options['tipo'] == 'staff') {
            $this->staffMako();
        } elseif ($options['accion'] == 'mover' && $options['tipo'] == 'faltantes') {
            $this->faltantesMako();
        }

        print $this->formatter->format("\n===FIN a las: " . date("Y-m-d H:i:s") . "===\n", 'INFO');

        exit($this->res);
    }

    function actualizarMoodle() {
        foreach ($this->con->query("SELECT * FROM fixes WHERE moodle_id_orig = 0 ORDER BY created_at") as $row) {
            $usuario = $row['usuario'];
            $id      = $row['id'];
            $alta    = $row['created_at'];
            $inm     = OperacionMoodle::consultarRegistro($usuario);
            $um      = $inm->users[0];
            $idmoo   = intval($um->id);

            $this->con->exec("UPDATE fixes SET moodle_id_orig = $idmoo WHERE id = $id");

            print $this->formatter->format("$alta | $usuario | $idmoo \n", 'COMMENT');
        }
    }

    function actualizarLdapuid() {
        foreach ($this->con->query("SELECT * FROM fixes WHERE ldap_uid_orig != 0 AND ldap_uid_nvo = 0 ORDER BY created_at") as $row) {
            $usuario   = $row['usuario'];
            $id        = $row['id'];
            $alta      = $row['created_at'];
            $centro_id = $row['centro_id'];
            $centro    = CentroPeer::retrieveByPK($centro_id);
            $alias     = $centro->getAlias();

            $nvo_uid   = siguienteUID($alias);

            if ($nvo_uid == 0) {
                print $this->formatter->format("Error al generar siguiente secuencia ldap para usuario: $usuario | $centro_id | $alias |" . date("Y-m-d H:i:s") . "===\n", 'ERROR');
                continue;
            }

            if (!modificarLDAPUid($usuario, $nvo_uid)) {
                print $this->formatter->format("Error al modificar valor en ldap para usuario: $usuario | $centro_id | $alias |" . date("Y-m-d H:i:s") . "===\n", 'ERROR');
                exit;
            }

            $this->con->exec("UPDATE fixes SET ldap_uid_nvo = $nvo_uid WHERE id = $id");
            print $this->formatter->format("$alta | $usuario | $nvo_uid \n", 'COMMENT');
        }
    }

    function resetSecuenciasLdap() {
        $ldapconn = ldap_connect(LDAP_MASTER_HOST, LDAP_MASTER_PORT);

        ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);

        if ($ldapconn) {
            // binding anonimo
            $ldapbind = ldap_bind($ldapconn, LDAP_MASTER_RDN, LDAP_MASTER_PWD);
            // verify binding
            if ($ldapbind) {
            } else {
                error_log("ERROR:[ldap] Fallo el bind del ldap.");
                return false;
            }
        }
        $bdn = "ou=UIDs," . LDAP_MASTER_BDN;
        $c   = new Criteria();
        $c->add(CentroPeer::ACTIVO, true);
        $c->addAscendingOrderByColumn(CentroPeer::ID);
        foreach (CentroPeer::doSelect($c) as $centro) {

            $alias     = $centro->getAlias();
            $uidNumber = $centro->getId() * 100000;
            if ($centro->getId() == 9999) $uidNumber = 50000;
            $info["uidNumber"]           = $uidNumber;

            print $this->formatter->format("$alias | $uidNumber \n", 'COMMENT');

            $r = ldap_modify($ldapconn, "cn=NextFreeUnixId-$alias," . $bdn, $info);
            if (!$r) {
                error_log("ERROR:[ldap:modificarLDAPUid] x " . ldap_err2str(ldap_errno($ldapconn)) . " cn=NextFreeUnixId-$alias," . $bdn);
                ldap_close($ldapconn);
                return false;
            }
        }
        ldap_close($ldapconn);
        return true;
    }

    function usuariosEnLdap() {
        $ldapconn = ldap_connect(LDAP_MASTER_HOST, LDAP_MASTER_PORT);

        ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);

        if ($ldapconn) {
            // binding anonimo
            $ldapbind = ldap_bind($ldapconn, LDAP_MASTER_RDN, LDAP_MASTER_PWD);
            // verify binding
            if ($ldapbind) {
            } else {
                error_log("ERROR:[ldap] Fallo el bind del ldap.");
                return false;
            }
        }
        $bdn  = "ou=People," . LDAP_MASTER_BDN;

        $res  = ldap_search($ldapconn, $bdn, "uid=*");
        $info = ldap_get_entries($ldapconn, $res);
        ldap_close($ldapconn);
        //print_r($info);
        for ($i = 0; $i < $info['count']; $i++) {
            $usuario   = $info[$i]['uid'][0];
            $desc      = $info[$i]['description'][0];
            list($x, $centro_id, $socio_id)            = explode("|", $desc);
            $uidnumber = $info[$i]['uidnumber'][0];
            echo "$usuario\t$uidnumber\n";
        }

        return true;
    }

    function staffMako() {
        foreach ($this->con->query("SELECT * FROM usuario ORDER BY created_at") as $row) {
            $usuario = $row['usuario'];
            $id      = $row['id'];
            $alta    = $row['created_at'];
            $alias   = "mako";

            $nvo_uid = siguienteUID($alias);

            if ($nvo_uid == 0) {
                print $this->formatter->format("Error al generar siguiente secuencia ldap para usuario de staff: $usuario |" . date("Y-m-d H:i:s") . "===\n", 'ERROR');
                continue;
            }

            if (!modificarLDAPUid($usuario, $nvo_uid)) {
                print $this->formatter->format("Error al modificar valor en ldap para usuario de staff: $usuario |" . date("Y-m-d H:i:s") . "===\n", 'ERROR');
                continue;
            }

            print $this->formatter->format("$usuario\t$nvo_uid\n", 'COMMENT');
        }
    }

    function faltantesMako() {
        $inicia  = 40000;
        foreach ($this->con->query("SELECT * FROM faltantes WHERE usuario != 'root'") as $row) {
            $usuario = $row['usuario'];
            $alias   = "mako";

            $nvo_uid = $inicia;

            if (!modificarLDAPUid($usuario, $nvo_uid)) {
                print $this->formatter->format("Error al modificar valor en ldap para usuario de staff: $usuario |" . date("Y-m-d H:i:s") . "===\n", 'ERROR');
                exit;
            }
            $inicia++;
            print $this->formatter->format("$usuario\t$nvo_uid\n", 'COMMENT');
        }
    }
}
?>