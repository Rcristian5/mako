<?php
class PcsComandoTask extends sfBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'               ,  'tolu2' ),
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección' , 'propel'),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación', 'frontend'),
            new sfCommandOption('comando'    , null, sfCommandOption::PARAMETER_REQUIRED, 'Comando a ejecutar en la PC'),
            new sfCommandOption('centro'     , null, sfCommandOption::PARAMETER_OPTIONAL, 'Centro donde se ejecuta el comando'),
            new sfCommandOption('aula'       , null, sfCommandOption::PARAMETER_OPTIONAL, 'Aula donde se ejecuta el comando'),
            new sfCommandOption('ip'         , null, sfCommandOption::PARAMETER_OPTIONAL, 'IP de la PC donde se ejecuta el comando')
        ));

        $this->namespace           = 'mako';
        $this->name                = 'pcs-comando';
        $this->briefDescription    = 'Ejecuta comandos en las PCs';
        $this->detailedDescription = <<<EOF
The [mako:pcs-comando|INFO] Pcs comando ejecuta comandos en las pcs del centro:
Si el centro no esta dado, se usa el centro donde radica el comando. Debe escribirse el alias, como tolu1, tolu2, etc.
Cuando se ejecuta en un aula se debe incluir el centro donde radica el aula.

  [./symfony mako:pcs-commando --env=prod |INFO]
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $configuration   = ProjectConfiguration::getApplicationConfiguration('frontend', $options['env'], true);
        $context         = sfContext::createInstance($configuration);
        $alias           = strtolower($options['centro']);
        $comando         = strtolower($options['comando']);
        $aula            = strtoupper($options['aula']);
        $ip              = strtolower($options['ip']);
        $c               = new Criteria();

        if ($options['centro'] == null && $options['aula'] == null && $options['pc'] == null) {
            print $this->formatter->format("\n Debe proporcionar los datos de centro, o aula o pc \n", 'ERROR');
            exit;

        } elseif ($options['centro'] == null && $options['aula'] == null && $options['ip'] != null) {
            //Por ip
            $c->add(ComputadoraPeer::IP, $ip);

        } elseif ($options['centro'] != null && $options['aula'] != null && $options['ip'] == null) {
            //Por centro y aula
            $c1 = new Criteria();
            $c1->add(CentroPeer::ALIAS, $alias);

            $centro    = CentroPeer::doSelectOne($c1);
            $centro_id = $centro->getId();
            $aula_id   = $this->getSeccionIdPorNombre($centro_id, $aula);

            $c->add(ComputadoraPeer::SECCION_ID, $aula_id);

        } elseif ($options['centro'] != null && $options['aula'] == null && $options['ip'] == null) {
            //Por centro
            $c1 = new Criteria();
            $c1->add(CentroPeer::ALIAS, $alias);

            $centro    = CentroPeer::doSelectOne($c1);
            $centro_id = $centro->getId();

            $c->add(ComputadoraPeer::CENTRO_ID, $centro_id);

        } else {
            print $this->formatter->format("\n Debe proporcionar los datos minimos para ejecutar \n", 'ERROR');
            exit;
        }

        $computadoras = ComputadoraPeer::doSelect($c);
        echo $this->formatter->format("\n CentroId:$centro_id" . date("Y-m-d H:i:s") . "INICIA RONDA----------- \n", 'COMMENT');
        foreach ($computadoras as $pc) {
            print $this->formatter->format(date("Y-m-d H:i:s") . " " . $pc->getIp() . " ", 'COMMENT');

            $cmd_base = "perl /opt/MakoScripts/ejecuta_pc.pl ";
            $ip       = $pc->getIp();

            //Hacemos ping:
            $rp = exec("/usr/bin/perl /opt/MakoScripts/ping.pl " . $ip);

            //Si recibimos 0 es que esta apagada la maquina
            if ($rp == 0) {
                print $this->formatter->format(" PC apagada ", 'ERROR') . "\n";

            } else {
                //ejecutamos comando
                $res = `$cmd_base $ip "$comando"`;
                print $this->formatter->format($res, 'INFO') . "\n";
            }
        }
        echo $this->formatter->format(date("Y-m-d H:i:s") . " FIN RONDA----------- \n", 'COMMENT');
    }

    function getSeccionIdPorNombre($centro_id, $nombre) {
        if (trim($nombre) == '') $nombre = "RECEPCION";

        $c = new Criteria();
        $c->add(SeccionPeer::NOMBRE, strtoupper($nombre));
        $c->add(SeccionPeer::CENTRO_ID, $centro_id);

        $s = SeccionPeer::doSelectOne($c);
        if ($s != null) {
            return $s->getId();
        }

        return 0;
    }
}
?>
