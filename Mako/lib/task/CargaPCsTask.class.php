<?php
class CargaPcsTask extends sfBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección' , 'propel'),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación', 'frontend'),
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'               ,  'tolu2' )
        ));

        $this->namespace           = 'mako';
        $this->name                = 'carga-pcs-ldap';
        $this->briefDescription    = 'Carga el catalogo de pcs inicial sincronizando desde ldap';
        $this->detailedDescription = <<<EOF
The [mako:carga-pcs-ldap|INFO] Carga el catalogo de pcs inicial sincronizando desde ldap:

  [./symfony mako:carga-pcs-ldap --env=prod |INFO]
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'] ? $options['connection'] : null)->getConnection();

        $con->exec("DELETE FROM computadora");

        //Hacemos el query de usuarios limpios
        $sql = "SELECT * FROM computadoras";

        $hostname = "";
        //Para cada PC del centro:
        foreach ($con->query($sql) as $c) {

            $cen      = CentroPeer::retrieveByPK($c['id_centro']);
            $alias    = $cen->getAlias();

            $ip       = $c['ip'];
            //TODO: Contemplar las reglas para caja y recepcion
            if (substr($ip, -2) == '98' || substr($ip, -2) == '99') {
                $num      = (substr($ip, -2) == '98') ? 'recepcion1' : 'recepcion2';
                $hostname = $num . "-l-" . strtolower($alias);

            } else {
                $num      = substr($ip, -2);
                $hostname = "pc" . $num . "-l-" . strtolower($alias);
            }

            echo $ip . " => " . $hostname . "\n";

            $pc = new Computadora();
            $pc->setId(Comun::generaId());
            $pc->setIp($c['ip']);
            $pc->setMacAdress($c['mac_address']);
            $pc->setCentroId($c['id_centro']);
            $pc->setAlias($c['alias']);
            $pc->setTipoId($c['id_tipo']);
            $pc->setSeccionId($this->getSeccionIdPorNombre($c['id_centro'], $c['seccion']));

            $pc->setHostname($hostname);

            $pc->save();
        }
    }

    function getSeccionIdPorNombre($centro_id, $nombre) {
        $c = new Criteria();
        $c->add(SeccionPeer::NOMBRE, strtoupper($nombre));
        $c->add(SeccionPeer::CENTRO_ID, $centro_id);

        $s = SeccionPeer::doSelectOne($c);

        if ($s != null) {
            return $s->getId();
        }
        return 0;
    }
}
?>