<?php
/**
 * Task que regresa una lista de centros para utilizarse en comandos cli para sincronizar informacion a todos los centros activos.
 *
 * @author edgar
 *
 */
class makoCentrosTask extends sfBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación', 'frontend'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente',  'tolu2' ),
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conexión', 'propel'),
            new sfCommandOption('estatus', null, sfCommandOption::PARAMETER_OPTIONAL, 'El estatus delc entro, activo, inactivo', 'activo'),
            new sfCommandOption('excluye', null, sfCommandOption::PARAMETER_OPTIONAL, 'Centro que se excluira de la operación', null),
            new sfCommandOption('id', null, sfCommandOption::PARAMETER_OPTIONAL, 'El identificador que regresa, ip, alias, nombre, extracto', 'alias')
        ));

        $this->namespace           = 'mako';
        $this->name                = 'centros';
        $this->briefDescription    = '';
        $this->detailedDescription = <<<EOF
The [mako:centros|INFO] task does things.
Call it with:

  [php symfony mako:centros|INFO]
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'] ? $options['connection'] : null)->getConnection();
        $c               = new Criteria();
        $estatus         = ($options['estatus'] == 'activo') ? true : false;

        $c->add(CentroPeer::ACTIVO, $estatus);

        $centros = CentroPeer::doSelect($c);
        $salida  = "";
		$arr_excluye = explode(',',$options['excluye']);

        foreach ($centros as $centro) {
            if ($options['id'] == 'ip')     $valor = $centro->getIp();
            if ($options['id'] == 'alias')  $valor = $centro->getAlias();
            if ($options['id'] == 'nombre') $valor = $centro;

            if ($options['id'] == 'extracto') {
                list($o1, $o2, $o3, $o4) = explode('.', $centro->getIp());
                $valor = $o3;
            }

            if (in_array($valor,$arr_excluye)) continue;
			$salida.= $valor . "\n";
        }

        echo $salida;
    }
}
