<?php
class SyncAulasCentralTask extends sfPropelBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'               ,  'tolu2' ),
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección' , 'propel'),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación', 'frontend')
        ));

        $this->namespace           = 'mako';
        $this->name                = 'sync-aulas-central';
        $this->briefDescription    = 'Sincroniza las aulas del servidor central';
        $this->detailedDescription = <<<EOF
 [mako:add-cursos-moodlle|INFO] Crea el registro de sincronia de aluas para los centros

  [./symfony mako:ejecuta-sync --env=prod |INFO]
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'])->getConnection();

        $this->logSection('Generando lista de aulas activas', '…');

        $c = new Criteria();
        $c->add(AulaPeer::ACTIVO, true);
        $c->addAscendingOrderByColumn(AulaPeer::CENTRO_ID);

        $this->logSection('Creando registros de sincronia', '…');

        foreach (AulaPeer::doSelect($c) as $aula) {
            $this->logSection('Crenado registro', $aula->getNombre());
            sfProjectConfiguration::getActive()->getEventDispatcher()->notify(new sfEvent($aula, 'aula.creada', array('aula' => $aula)));
        }

        $this->logSection('Se termino de cargar los registros de sincronia', '');
    }
}
?>
