<?php
class EjecutaSyncTask extends sfBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'                                                 ,  'tolu2' ),
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección'                                   , 'propel'),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación'                                  , 'frontend'),
            new sfCommandOption('centro'     , null, sfCommandOption::PARAMETER_OPTIONAL, 'Alias del centro a sincronizar. Nulo para ejecutar en todos.', null)
        ));

        $this->namespace           = 'mako';
        $this->name                = 'ejecuta-sync';
        $this->briefDescription    = 'Ejecuta el web service de sincronia.';
        $this->detailedDescription = <<<EOF
The [mako:ejecuta-sync|INFO] Ejecuta el web service de sincronia.:

  [./symfony mako:ejecuta-sync --env=prod |INFO]
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'] ? $options['connection'] : null)->getConnection();

        $c = new Criteria();
        $c->add(SincroniaPeer::COMPLETO, false);
        $c->addAscendingOrderByColumn(SincroniaPeer::ID);

        $syncs = SincroniaPeer::doSelect($c);

        error_log(date("Y-m-d H:i:s") . "INICIA SYNC-----------");

        echo "Syncs principales: " . count($syncs) . "\n";
        foreach ($syncs as $s) {
            SincroniaPeer::sincronizaRegistro($s);
        }

        echo (date("Y-m-d H:i:s") . "TERMINA SYNC-----------\n");
    }
}
?>