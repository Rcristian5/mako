<?php
class CreaSociosLdapTask extends sfBaseTask
{

    public $log      = array();
    public $existe   = 0;
    public $noexiste = 0;

    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'               ,  'tolu2' ),
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección' , 'propel'),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación', 'frontend'),
            new sfCommandOption('fecha'      , null, sfCommandOption::PARAMETER_REQUIRED, 'Fecha mayor o igual para consultar los socios modificados.', date("Y-m-d 00:00:00")),
            new sfCommandOption('accion'     , null, sfCommandOption::PARAMETER_REQUIRED, 'Accion que se aplica, default solo reporta estatus.'       , 'estatus')
        ));

        $this->namespace           = 'mako';
        $this->name                = 'socio-sincroniza-ldap';
        $this->briefDescription    = 'Sincroniza la base local de centro con el ldap local tomando los socios modificados apartir de una fecha dada';
        $this->detailedDescription = <<<EOF

  [mako soporte:socio-sincroniza-ldap|INFO] Sincroniza la base local de centro con el ldap local tomando los socios modificados apartir de una fecha dada,
   Si no existe el usuario en LDAP lo crea, si existe lo actualiza con los valores de la base de datos local.

   Si se agrega la opción --accion=sincronizar aplica sincronía entre base local y ldap de centro.

EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'] ? $options['connection'] : null)->getConnection();

        $configuration = ProjectConfiguration::getApplicationConfiguration('frontend', $options['env'], true);
        $context       = sfContext::createInstance($configuration);
        $configuration->loadHelpers('Ldap');

        $centro_id   = sfConfig::get('app_centro_actual_id');
        $desde_fecha = $options['fecha'];
        $c           = new Criteria();
        $c->add(SocioPeer::UPDATED_AT, $desde_fecha, Criteria::GREATER_EQUAL);
        $c->add(SocioPeer::CENTRO_ID, $centro_id);

        $Socios = SocioPeer::doSelect($c);
        $res    = true;

        foreach ($Socios as $socio) {
            //Consultamos si el socio existe en el ldap:
            if (verificaUsuarioLDAP($socio->getUsuario())) {
                if ($options['accion'] == 'sincronizar') $this->actualizarSocio($socio);
                //Actualizamos el registro en ldap
                $this->log[] = $this->formatter->format($socio->getUsuario(), 'INFO');
                $this->existe++;
            } else {
                //Si no existe entonces creamos el usuario en ldap
                if ($options['accion'] == 'sincronizar') $this->crearSocio($socio);
                $this->log[] = $this->formatter->format($socio->getUsuario(), 'ERROR');
                $this->noexiste++;
            }
        }

        $this->log[] = $this->formatter->format("===Procesados " . ($this->existe + $this->noexiste) . " Registros totales===", 'COMMENT');
        $this->log[] = $this->formatter->format("Existentes: " . $this->existe, 'INFO');
        $this->log[] = $this->formatter->format("NO Existentes: " . $this->noexiste, 'ERROR');

        $this->log($this->log);
    }

    function crearSocio(Socio $socio) {
        if ($socio->getLdapUid() > 0) {
            $ldap_uid = $socio->getLdapUid();
        } else {
            $ldap_uid = null;
        }

        $uid      = agregarSocioLdap($socio->getUsuario(), $socio->getNombre(), trim($socio->getApepat() . " " . $socio->getApemat()), $socio->getClave(), $socio->getEmail(), $socio->getEstado(), $socio->getDirgmaps(), $socio->getTel(), $socio->getCelular(), $socio->getCentro()->getAlias(), $socio->getCentroId(), $socio->getId(), $ldap_uid);
        if ($uid == false) {
            //error_log("Error[OperacionLdap.registrarSocio]: Ocurrión un error al registrar al socio ".$socio);
            $this->log[] = $this->formatter->format("Ocurrió un error al registrar al socio $socio", 'ERROR');
            $this->log[] = $this->formatter->format($socio->getUsuario(), 'ERROR');
            $this->log[] = $this->formatter->format($socio->getNombre(), 'ERROR');
            $this->log[] = $this->formatter->format(trim($socio->getApepat() . " " . $socio->getApemat()), 'ERROR');
            $this->log[] = $this->formatter->format($socio->getClave(), 'ERROR');
            $this->log[] = $this->formatter->format($socio->getEmail(), 'ERROR');
            $this->log[] = $this->formatter->format($socio->getEstado(), 'ERROR');
            $this->log[] = $this->formatter->format($socio->getDirgmaps(), 'ERROR');
            $this->log[] = $this->formatter->format($socio->getTel(), 'ERROR');
            $this->log[] = $this->formatter->format($socio->getCelular(), 'ERROR');
            $this->log[] = $this->formatter->format($socio->getCentro()->getAlias(), 'ERROR');
            $this->log[] = $this->formatter->format($socio->getCentroId(), 'ERROR');
            $this->log[] = $this->formatter->format($socio->getId(), 'ERROR');
            $this->log[] = $this->formatter->format($ldap_uid, 'ERROR');
            $this->log[] = $this->formatter->format(print_r($socio->toArray(), true), 'ERROR');

        } elseif ($ldap_uid == null) {
            $socio->setLdapUid($uid);
            $socio->save();
        }
    }

    function actualizarSocio(Socio $socio) {
        modificarSocioLDAP($socio->getUsuario(), $socio->getNombre(), trim($socio->getApepat() . " " . $socio->getApemat()), $socio->getClave(), $socio->getEmail(), $socio->getEstado(), $socio->getDirgmaps(), $socio->getTel(), $socio->getCelular(), $modificaUsername, $usuarioAnterior, $socio->getCentroId(), $socio->getId());

        $ldap_uid = leeUserUID($socio->getUsuario());

        if ($socio->getLdapUid() != $ldap_uid) {
            $socio->setLdapUid($ldap_uid);
            $socio->save();
        }
    }
}
?>