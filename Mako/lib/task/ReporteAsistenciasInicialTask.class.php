<?php
class ReporteAsistenciasInicialTask extends sfPropelBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección' , 'propel'),
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'               ,  'tolu2' ),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación', 'frontend'),
            new sfCommandOption('desde'      , null, sfCommandOption::PARAMETER_REQUIRED, 'Parametro requerido para indicar a partir de que fecha se requiere generar el reporte'),
        ));

        $this->namespace           = 'mako';
        $this->name                = 'reporte-inicial-asistencias';
        $this->briefDescription    = 'Guarda el reporte de asistencias';
        $this->detailedDescription = <<<EOF
 [mako:add-cursos-moodlle|INFO] Genera el reporte de asistencias desde una fecha dada

  [./symfony mako:reporte-inicial-asistencias --env=prod |INFO]
EOF;
    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'] ? $options['connection'] : null)->getConnection();

        if($options['desde']) {
            $this->logSection('Obtenieno reporte de asistencias, centro:', sfConfig::get('app_centro_actual'));

            // Guradamos el reporte de sistencias por cada curso
            foreach (CursoPeer::doSelect(new Criteria()) as $curso) {
                $desde        = explode('-', $options['desde']);
                $dia          = (int)$desde[0];
                $mes          = (int)$desde[1];
                $fechaInicial = mktime(0,0,0, $mes, $dia, $desde[2]);

                for($i = 0; date('Ymd',$fechaInicial) <= date('Ymd'); $i++) {
                    $fechaInicial = mktime(0,0,0, $mes, $dia, $desde[2]);
                    $reporte      = ReporteAsistenciasPeer::guardaReporteAsistencias(
                                        sfConfig::get('app_centro_actual_id'),
                                        $curso->getId(),
                                        date('Y-m-d', $fechaInicial)
                                    );

                    if($reporte['registrados'] != 0) {
                        $this->logSection('CURSO', $curso->getNombre());
                    }

                    $dia++;
                }
            }
            $this->logSection('Finalizado..', '');
        } else {
            $this->log('Falta la ceha de inicio');
        }
    }
}