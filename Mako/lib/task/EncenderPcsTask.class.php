<?php
class EncenderPcsTask extends sfBaseTask
{
    //Comando para encender la pc
    private $cmd = 'sudo /usr/sbin/etherwake -i eth0';

    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'                         ,  'tolu2' ),
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección'           , 'propel'),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación'          , 'frontend'),
            new sfCommandOption('centro'     , null, sfCommandOption::PARAMETER_OPTIONAL, 'Centro donde se realiza la operación', null),
            new sfCommandOption('aula'       , null, sfCommandOption::PARAMETER_OPTIONAL, 'Aula donde se realiza la operacion'  , null),
            new sfCommandOption('ip'         , null, sfCommandOption::PARAMETER_OPTIONAL, 'IP que se quiere encender'           , null)
        ));

        $this->namespace           = 'soporte';
        $this->name                = 'encender-pcs';
        $this->briefDescription    = 'Enciende PCS del centro';
        $this->detailedDescription = <<<EOF
The [mako:encender-pcs|INFO] Enciende las PCs de centro completo, por aula o por PC individual:

  [mako soporte:encender-pcs --centro=tolu1 |INFO]
  [mako soporte:encender-pcs --ip="10.1.0.98" |INFO]
  [mako soporte:encender-pcs --centro=tolu1 --aula="Aula 1" |INFO]
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'] ? $options['connection'] : null)->getConnection();

        //$configuration = ProjectConfiguration::getApplicationConfiguration('frontend', $options['env'], true);
        //$context = sfContext::createInstance($configuration);
        //$configuration->loadHelpers('Ldap');

        if ($options['centro'] == null && $options['aula'] == null && $options['ip'] == null) {
            print "Se debe seleccionar el centro, o el aula o la ip a encender.";
            exit;

        } elseif ($options['centro'] == null && $options['aula'] == null && $options['ip'] != null) {
            $this->enciendePC($options['ip']);

        } elseif ($options['centro'] != null && $options['aula'] != null && $options['ip'] == null) {
            $this->enciendeAula($options['centro'], $options['aula']);

        } elseif ($options['centro'] != null && $options['aula'] == null && $options['ip'] == null) {
            $this->enciendeCentro($options['centro']);
        }
    }

    function getSeccionIdPorNombre($centro_id, $nombre) {
        if (trim($nombre) == '') $nombre = "RECEPCION";

        $c      = new Criteria();
        $c->add(SeccionPeer::NOMBRE, strtoupper($nombre));
        $c->add(SeccionPeer::CENTRO_ID, $centro_id);
        $s = SeccionPeer::doSelectOne($c);
        if ($s != null) {
            return $s->getId();
        }
        return 0;
    }

    function enciendeAula($centro, $aula) {
        $c = new Criteria();
        $c->add(CentroPeer::ALIAS, strtolower($centro));

        $cen        = CentroPeer::doSelectOne($c);
        $seccion_id = $this->getSeccionIdPorNombre($cen->getId(), $aula);

        if ($cen != null && $seccion_id != 0) {
            $centro_id  = $cen->getId();

            $c2         = new Criteria();
            $c2->add(ComputadoraPeer::CENTRO_ID, $centro_id);
            $c2->add(ComputadoraPeer::SECCION_ID, $seccion_id);
            $res = ComputadoraPeer::doSelect($c2);
            foreach ($res as $pc) {
                echo "Encendiendo pc en $centro y $aula: " . $pc->getIp() . "\n";
                exec($this->cmd . " " . $pc->getMacAdress());
            }
        }
    }

    function enciendeCentro($centro) {
        $c = new Criteria();
        $c->add(CentroPeer::ALIAS, strtolower($centro));

        $cen       = CentroPeer::doSelectOne($c);

        if ($cen != null) {
            $centro_id = $cen->getId();

            $c2        = new Criteria();
            $c2->add(ComputadoraPeer::CENTRO_ID, $centro_id);
            $res = ComputadoraPeer::doSelect($c2);
            foreach ($res as $pc) {
                echo "Encendiendo: Centro: $centro -> " . $pc->getIp() . "\n";
                exec($this->cmd . " " . $pc->getMacAdress());
            }
        }
    }

    function enciendePC($ip) {
        $c = new Criteria();
        $c->add(ComputadoraPeer::IP, $ip);
        $compu = ComputadoraPeer::doSelectOne($c);
        if ($compu != null) {
            echo "Encendiendo: $ip\n";
            exec($this->cmd . " " . $compu->getMacAdress());
            return;
        }
    }
}
?>