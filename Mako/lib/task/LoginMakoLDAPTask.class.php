<?php
class LoginMakoLDAPTask extends sfPropelBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección' , 'propel'),
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'               ,  'tolu2' ),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación', 'frontend')
        ));

        $this->namespace           = 'mako';
        $this->name                = 'login-mako-ldap';
        $this->briefDescription    = 'Lee la tabla de sesiones y compara las sesiones activas con las sesiones en el LDAP';
        $this->detailedDescription = <<<EOF
     [mako:login-mako-ldap|INFO] Lee la tabla de sesiones y compara las sesiones activas con las sesiones en el LDAP

      [./symfony mako:login-mako-ldap |INFO]
EOF;

    }
    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'])->getConnection();
        $centroId        = sfConfig::get('app_centro_actual_id');
        $centro          = CentroPeer::retrieveByPK($centroId);
        $context         = sfContext::createInstance($this->configuration);

        $this->configuration->loadHelpers('Ldap');

        foreach (SesionPeer::doSelect(new Criteria()) as $enSesion) {
            $socio    = SocioPeer::retrieveByPK($enSesion->getSocioId());
            $hostMako = $enSesion->getComputadora()->getHostname();
            $hosLDAP  = hostEnLdap($enSesion->getUsuario());

            if ($hostMako != $hosLDAP) {
                $this->log('--------');
                $this->log(date('Y-m-d H:i:s'));
                $this->log("Centro: " . $centro->getAlias());
                $this->log("Socio " . $socio->getId());
                $this->log("Socio: " . $socio->getNombreCompleto());
                $this->log("Usuario: " . $enSesion->getUsuario());
                $this->log("Host Mako: " . $hostMako);
                $this->log("Host LDAP: " . $hosLDAP);
                $this->log("Fecha: " . date('d/m/Y'));
                $this->log("Hora: " . date('H:i:s'));
                $this->log('--------');
            }
        }
    }
}
