<?php
class makoCancelarGrupoTask extends sfBaseTask
{
    protected function configure() {

        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'frontend'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'Ambiente',  'tolu2' ),
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la la conexión a la base', 'propel'),
            new sfCommandOption('grupo_id', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre del grupo en el que realizó el curso que se desea recursar', null)
        ));

        $this->namespace           = 'mako';
        $this->name                = 'cancelar-grupo';
        $this->briefDescription    = 'Cancela un grupo por orden del administrador de control escolar';
        $this->detailedDescription = <<<EOF
[mako:recursar|INFO] Cancela un grupo por orden del administrador de control escolar y cancela los pagos de los socios inscritos al grupo.

  [php symfony mako:cancelar-grupo --grupo_id=911111111212|INFO]
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        // initialize the database connection
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'] ? $options['connection'] : null)->getConnection();
        $grupo           = GrupoPeer::retrieveByPK($options['grupo_id']);

        error_log("Grupo_id: " . $grupo);

        // Se cancela el grupo
        $grupo->setActivo(false);
        $grupo->save();

        // Se obtienen los socios inscritos al grupo
        if ($grupo->getGrupoAlumnoss() && count($grupo->getGrupoAlumnoss()) > 0) {
            foreach ($grupo->getGrupoAlumnoss() as $alumno) {
                $socio = SocioPeer::retrieveByPK($alumno->getAlumnoId());

                //$grupo = new Grupo();

                if ($grupo != null && $socio != null) {
                    $curso_id = $grupo->getCursoId();
                    $socio_id = $socio->getId();
                    $grupo_id = $grupo->getId();

                    if (!$alumno->getPreinscrito()) {
                        // Se finaliza el grupo con el estatus "Cancelacion Central" id = 7
                        $cursoFinalizado = new CursosFinalizados();
                        $cursoFinalizado->setId(Comun::generaId());
                        $cursoFinalizado->setCursoId($curso_id);
                        $cursoFinalizado->setAlumnoId($socio_id);
                        $cursoFinalizado->setGrupoId($grupo_id);
                        $cursoFinalizado->setEstatusCursoFinalizadoId(7);
                        $cursoFinalizado->save();
                    }
                    error_log("Soscio: " . $socio->getNombreCompleto() . " Usuario: " . $socio->getUsuario() . ' Estado:' . (!$alumno->getPreinscrito() ? 'INSCRITO' : 'PREINSCRITO'));
                    error_log("Curso_id: " . $curso_id . " Socio_id: " . $socio_id . " grupo_id: " . $grupo_id);
                    //Ahora vamos a cancelar los pagos. Para esto necesitamos saber el orden pago.
                    $c3 = new Criteria();
                    $c3->add(AlumnoPagosPeer::SOCIO_ID, $socio_id);
                    $c3->add(AlumnoPagosPeer::CURSO_ID, $curso_id);
                    $c3->add(AlumnoPagosPeer::GRUPO_ID, $grupo_id);
                    $alumnoPago      = AlumnoPagosPeer::doSelectOne($c3);
                    if ($alumnoPago != null) {
                        //Obtenemos el orden_id de este pago
                        $orden           = $alumnoPago->getOrden();
                        $sub_producto_de = $alumnoPago->getSubProductoDe();
                        if ($orden != null) {
                            //Obtenemos los detalle_orden_id relacionados con esta orden
                            $detalleOrdenes  = $orden->getDetalleOrdens();
                            foreach ($detalleOrdenes as $detalleOrden) {
                                foreach ($detalleOrden->getSocioPagoss() as $socioPago) {

                                    $orden_base_id   = $socioPago->getOrdenBaseId();
                                    $c4              = new Criteria();
                                    $c4->add(SocioPagosPeer::ORDEN_BASE_ID, $orden_base_id);
                                    $c4->add(SocioPagosPeer::SUB_PRODUCTO_DE, $sub_producto_de);

                                    $aSocioPagos = SocioPagosPeer::doSelect($c4);
                                    foreach ($aSocioPagos as $sp) {
                                        if ($sp->getPagado() == false) {
                                            error_log("\tID: " . $sp->getId() . " cancelado: " . ($sp->getCancelado() ? 'SI' : 'NO') . " DO " . $sp->getDetalleOrdenId() . " SID " . $sp->getSocioId() . " BASE ORDEN " . $sp->getOrdenBaseId());
                                            $sp->setCancelado(true);

                                            $sp->save();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

