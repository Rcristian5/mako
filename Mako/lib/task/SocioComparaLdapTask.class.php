<?php
class SocioComparaLdapTask extends sfBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'                                                      ,  'tolu2' ),
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección'                                        , 'propel'),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación'                                       , 'frontend'),
            new sfCommandOption('accion'     , null, sfCommandOption::PARAMETER_REQUIRED, 'Accion a realizar, "consultar", "aplicar", "crear-faltantes-ldap"', 'consultar'),
            new sfCommandOption('tipo'       , null, sfCommandOption::PARAMETER_REQUIRED, 'Tipo de movimiento, alta o modificacion'                          , 'modificacion'),
            new sfCommandOption('desde'      , null, sfCommandOption::PARAMETER_REQUIRED, 'Fecha desde la que se consulta. Formato YYYY-mm-dd'               , date("Y-m-d") . " 00:00:00"),
            new sfCommandOption('hasta'      , null, sfCommandOption::PARAMETER_REQUIRED, 'Fecha hasta la que se consulta. Formato YYYY-mm-dd'               , date("Y-m-d") . " 23:59:59")
        ));

        $this->namespace           = 'soporte';
        $this->name                = 'socios-info';
        $this->briefDescription    = 'Muestra información de estatus de socios en batch.';
        $this->detailedDescription = <<<EOF

  [mako soporte:socios-info|INFO] Muestra insformación de socios en batch consultando en un rango de fechas dado ya se por movimientos de modificación o por alta.


EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $this->con       = $databaseManager->getDatabase($options['connection'] ? $options['connection'] : null)->getConnection();
        $configuration   = ProjectConfiguration::getApplicationConfiguration('frontend', $options['env'], true);
        $context         = sfContext::createInstance($configuration);

        $configuration->loadHelpers('Ldap');

        //Esta accion crea los registros que haya perdido ldap, q existan en mako. Sólo crea registros apartir del 2011

        if ($options['accion'] == 'crear-faltantes-ldap') {
            $faltantes = 0;

            print $this->formatter->format("\n===INICIANDO a las: " . date("Y-m-d H:i:s") . " EN CENTRO " . sfConfig::get('app_centro_actual_id') . "===\n", 'INFO');

            $c = new Criteria();
            $c->add(SocioPeer::CENTRO_ID, sfConfig::get('app_centro_actual_id'));
            $c->add(SocioPeer::CREATED_AT, "2011-01-01", Criteria::GREATER_EQUAL);
            $c->add(SocioPeer::ACTIVO, true);

            $as = SocioPeer::doSelect($c);

            if (count($as) < 1) print $this->formatter->format("\n===Sin movimientos de socios en este rango de fechas para este centro.===\n", 'ERROR');

            foreach ($as as $socio) {
                $faltaba = $this->creaEnLDAP($socio);
                if ($faltaba) $faltantes++;
            }

            print $this->formatter->format("\n===Finalizando a las: " . date("Y-m-d H:i:s") . " con $faltantes faltantes en ldap ===\n", 'INFO');

            exit;
        }

        print $this->formatter->format("\n===Inicio a las: " . date("Y-m-d H:i:s") . " Consultando rango desde " . $options['desde'] . ". hasta " . $options['hasta'] . "===\n", 'INFO');

        list($fdesde, $hdesde) = explode(" ", pg_escape_string($options['desde']));
        list($fhasta, $hhasta) = explode(" ", pg_escape_string($options['hasta']));

        if ($hdesde != "" && $hhasta != "") {
            $desde = pg_escape_string($fdesde) . " " . $hdesde;
            $hasta = pg_escape_string($fhasta) . " " . $hhasta;
        } else {
            $desde = pg_escape_string($fdesde) . " 00:00:00";
            $hasta = pg_escape_string($fhasta) . " 23:59:59";
        }

        $c = new Criteria();
        if ($options['tipo'] == 'modificacion') {
            $c->add(SocioPeer::UPDATED_AT, $desde, Criteria::GREATER_EQUAL);
            $c->addAnd(SocioPeer::UPDATED_AT, $hasta, Criteria::LESS_EQUAL);
        } else {
            $c->add(SocioPeer::CENTRO_ID, sfConfig::get('app_centro_actual_id'));
            $c->add(SocioPeer::CREATED_AT, $desde, Criteria::GREATER_EQUAL);
            $c->addAnd(SocioPeer::CREATED_AT, $hasta, Criteria::LESS_EQUAL);
        }

        $as = SocioPeer::doSelect($c);

        if (count($as) < 1) print $this->formatter->format("\n===Sin movimientos de socios en este rango de fechas para este centro.===\n", 'ERROR');

        foreach ($as as $socio) {
            if ($options['accion'] == 'consultar') {
                $this->info($socio);
            } elseif ($options['accion'] == 'aplicar') {
                $this->info($socio, $options['accion']);
            } elseif ($options['accion'] == 'cronhost') {
                //Si no tiene sesion actual en la tabla de sesiones,
                //revisamos si el valor del host en ldap corresponde al centro,
                //si es del centro entonces se resetea el valor del host.
                //TODO: pruebas.

                if (!$this->existeSesion($socio)) {
                    $host = hostEnLdap($socio->getUsuario());

                    if ($host != '*' and trim($host) != '') {
                        if ($this->esLocalElHostAtorado($host, sfConfig::get('app_centro_actual_id'))) {
                            modificarHostPermitido($socio->getUsuario(), '*');
                        }
                    }
                }
            }

            print "\n";
        }

        print $this->formatter->format("\n===FIN a las: " .
                                           date("Y-m-d H:i:s") .
                                           " de Consulta de rango desde " .
                                           $options['desde'] .
                                           ". hasta " .
                                           $options['hasta'] .
                                           "===\n",
                                       'INFO');

        exit($this->res);
    }

    function esLocalElHostAtorado($host, $centro_id) {
        $c = new Criteria();
        $c->add(ComputadoraPeer::HOSTNAME, $host);

        $compu = ComputadoraPeer::doSelect($c);

        if ($centro_id == $compu->getCentroId()) {
            return true;
        } else {
            return false;
        }
    }

    function info(Socio $socio, $accion  = '') {
        $compara = comparaClavesLdap($socio->getUsuario(), $socio->getClave());

        if ($compara === 0) {
            print $this->formatter->format("\n" . $socio->getUsuario() . " ===No existe el usuario en el servidor LDAP.===\n", 'ERROR');

            if ($accion == 'aplicar') $this->crearSocio($socio);
            if ($accion == 'aplicar') $this->crearEnMoodle($socio);

            return;
        }

        $inm = $this->infoMoodle($socio);
        $um  = $inm->users;

        //print_r($um[0]->id);
        $idmoo  = intval($um[0]->id);
        $resmoo = $idmoo;

        if ($idmoo == 0) {
            $resmoo = $this->formatter->format("NO existe en Moodle", 'ERROR');

            if ($accion == 'aplicar') $this->actualizarSocio($socio);
            if ($accion == 'aplicar') modificarClaveSocioLDAP($socio->getUsuario(), $socio->getClave());
            if ($accion == 'aplicar') $this->crearEnMoodle($socio);
        }

        if ($socio->getLdapUid() == 0) {
            $ldap_uid = $this->formatter->format("No está sincronizado con la base local", 'ERROR');

            if ($accion == 'aplicar') $this->actualizarSocio($socio);
        } else {
            $ldap_uid   = $socio->getLdapUid();
        }

        $ensesion   = $this->enSesion($socio);
        $hostenldap = hostEnLdap($socio->getUsuario());

        print $this->formatter->format("\n====Consulta de información de socio====\n", 'COMMENT');
        print $this->formatter->format("Folio:  \t", 'COMMENT') . $socio->getFolio();
        print $this->formatter->format("\nUsuario: \t", 'COMMENT') . $socio->getUsuario();
        print $this->formatter->format("\nNombre: \t", 'COMMENT') . $socio;
        print $this->formatter->format("\nAlta en: \t", 'COMMENT') . $socio->getCentro();

        print $this->formatter->format("\nFecha Alta: \t", 'COMMENT') . $socio->getCreatedAt();
        print $this->formatter->format("\nEstatus: \t", 'COMMENT') . ($socio->getActivo() ? 'ACTIVO' : 'INACTIVO');
        print $this->formatter->format("\nUID LDAP: \t", 'COMMENT') . $ldap_uid;
        print $this->formatter->format("\nUID Mako: \t", 'COMMENT') . $socio->getId();
        print $this->formatter->format("\nID Moodle: \t", 'COMMENT') . $resmoo;
        print $this->formatter->format("\nSesión:  \t", 'COMMENT') . $ensesion;
        print $this->formatter->format("\nHost LDAP: \t", 'COMMENT') . $hostenldap;

        if ($accion == 'aplicar') $this->comparaHostSesion($socio, $ensesion, $hostenldap);

        if ($compara == true) {
            print $this->formatter->format("\nEstatus clave: \t", 'COMMENT') . $this->formatter->format("Claves coincidentes", 'INFO');
        } else {
            print $this->formatter->format("\nEstatus clave: \t", 'COMMENT') . $this->formatter->format("Claves NO coinciden", 'ERROR');
            if ($accion == 'aplicar') modificarClaveSocioLDAP($socio->getUsuario(), $socio->getClave());
        }
    }

    /**
     * Muestra si tiene sesion actualmente en este centro o no.
     * Consulta de la tabla de sesion la informacion
     * @param Socio $socio
     */
    function enSesion(Socio $socio) {
        $s = "";

        foreach ($socio->getSesions() as $sesion) {
            $s.= "Login:" . $sesion->getFechaLogin() . " IP " . $sesion->getIp() . " |" . $sesion->getComputadora()->getHostname();
        }
        return $s;
    }

    /**
     * Regresa verdadero o falso si existe o no sesion.
     *
     * @param $socio
     * @return bool Si existe sesion regresa true, si no, regresa false
     */
    function existeSesion(Socio $socio) {
        if (count($socio->getSesions()) > 0) return true;
        else return false;
    }

    function crearSocio(Socio $socio) {
        if ($socio->getLdapUid() > 0) {
            $ldap_uid = $socio->getLdapUid();
        } else {
            $ldap_uid = null;
        }

        $uid = agregarSocioLdap(
            $socio->getUsuario(),
            $socio->getNombre(),
            trim($socio->getApepat() . " " . $socio->getApemat()),
            $socio->getClave(),
            $socio->getEmail(),
            $socio->getEstado(),
            $socio->getDirgmaps(),
            $socio->getTel(),
            $socio->getCelular(),
            $socio->getCentro()->getAlias(),
            $socio->getCentroId(),
            $socio->getId(),
            $ldap_uid
        );

        if ($uid == false) {
            //error_log("Error[OperacionLdap.registrarSocio]: Ocurrión un error al registrar al socio ".$socio);
            print $this->formatter->format("\nOcurrió un error al registrar al socio $socio", 'ERROR');

            print $this->formatter->format($socio->getUsuario(), 'ERROR');
            print $this->formatter->format($socio->getNombre(), 'ERROR');
            print $this->formatter->format(trim($socio->getApepat() . " " . $socio->getApemat()), 'ERROR');
            print $this->formatter->format($socio->getClave(), 'ERROR');
            print $this->formatter->format($socio->getEmail(), 'ERROR');
            print $this->formatter->format($socio->getEstado(), 'ERROR');
            print $this->formatter->format($socio->getDirgmaps(), 'ERROR');
            print $this->formatter->format($socio->getTel(), 'ERROR');
            print $this->formatter->format($socio->getCelular(), 'ERROR');
            print $this->formatter->format($socio->getCentro()->getAlias(), 'ERROR');
            print $this->formatter->format($socio->getCentroId(), 'ERROR');
            print $this->formatter->format($socio->getId(), 'ERROR');
            print $this->formatter->format($ldap_uid, 'ERROR');
            //print $this->formatter->format(print_r($socio->toArray(),true), 'ERROR');


        } elseif ($ldap_uid == null) {
            $this->con->exec("UPDATE socio SET ldap_uid = $uid WHERE id = " . $socio->getId());
        }
    }

    function actualizarSocio(Socio $socio) {
        modificarSocioLDAP(
            $socio->getUsuario(),
            $socio->getNombre(),
            trim($socio->getApepat() . " " . $socio->getApemat()),
            $socio->getClave(),
            $socio->getEmail(),
            $socio->getEstado(),
            $socio->getDirgmaps(),
            $socio->getTel(),
            $socio->getCelular(),
            $modificaUsername,
            $usuarioAnterior,
            $socio->getCentroId(),
            $socio->getId()
        );

        $ldap_uid = leeUserUID($socio->getUsuario());

        if ($socio->getLdapUid() != $ldap_uid) {
            $this->con->exec("UPDATE socio SET ldap_uid = $ldap_uid WHERE id = " . $socio->getId());
        }
    }

    function infoMoodle(Socio $socio) {
        $info = OperacionMoodle::comprobarRegistro($socio);

        return $info;
    }

    function crearEnMoodle(Socio $socio) {
        $inm   = $this->infoMoodle($socio);
        $um    = $inm->users[0];
        $idmoo = intval($um->id);

        if ($idmoo == 0) {
            OperacionMoodle::registrarSocio($socio);
        } else {
            print $this->formatter->format("\nYa existe en Moodle", 'ERROR');
        }
    }

    function liberarHost(Socio $socio) {
        if (verificaUsuarioLDAP($socio->getUsuario())) {
            try {
                modificarHostPermitido($socio->getUsuario(), '*');
            }
            catch(Exception $e) {
                print $this->formatter->format("\nError al liberar el host: $socio", 'ERROR');
            }
        } else {
            print $this->formatter->format("\nEl usuario no existe en LDAP no es posible actualizar el host!:", 'ERROR');
        }
    }

    function comparaHostSesion(Socio $socio, $enses, $enldap) {
        if ($enldap != '*') {
            if (!preg_match("/$enldap/i", $enses)) {
                modificarHostPermitido($socio->getUsuario(), '*');
            }
        }
    }

    /**
     * Para los socios que existen en mako en el centro actual y no existen en ldap cuyo ldap_uid sea mayor que cero
     * se crea en ldap el registro, porque es probable q el ldap esté perdiendo registros.
     */
    function creaEnLDAP(Socio $socio) {
        $compara = comparaClavesLdap($socio->getUsuario(), $socio->getClave());
        if ($compara === 0) {
            print $this->formatter->format("\n" . $socio->getUsuario() . " ===No existe el usuario " . $socio->getUsuario() . " en el servidor LDAP.===\n", 'ERROR');
            $this->crearSocio($socio);
            return true;
        } else {
            return false;
        }
    }
}
?>