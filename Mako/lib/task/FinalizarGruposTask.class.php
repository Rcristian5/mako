<?php
class FinalizarGruposTask extends sfPropelBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección' , 'propel'),
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'               ,  'tolu2' ),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación', 'frontend'),
            new sfCommandOption('desde'      , null, sfCommandOption::PARAMETER_OPTIONAL, 'Parametro requerido para indicar a partir de que fecha se requiere generar el reporte'),
            new sfCommandOption('hasta'      , null, sfCommandOption::PARAMETER_OPTIONAL, 'Parametro opcional para indicar hasta que fecha se obtendran las calificaciones'),
            new sfCommandOption('override'   , null, sfCommandOption::PARAMETER_OPTIONAL, 'Parametro opcional para indicar si se debee hacer override a las calificaciones guardadas', 0)
        ));

        $this->namespace           = 'mako';
        $this->name                = 'finalizar-grupos';
        $this->briefDescription    = 'Coloca la calificacion final de los grupos asocioados a moodle';
        $this->detailedDescription = <<<EOF
 [mako:add-cursos-moodlle|INFO] Coloca la calificacion final de los grupos asocioados a moodle

  [./symfony mako:ejecuta-sync --env=prod |INFO]
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'])->getConnection();

        sfContext::createInstance($this->configuration);

        $moodleActivo = false;

        //Antes de finalizar cualquier grupo verificacmos que este activo el Moodle
        try {
            $cliente      = new SoapClient(sfConfig::get('app_soap_client'));
            $moodleActivo = true;

        }catch(Exception $e) {
            $cuerpo    = 'Error al finalizar grupos centro: ' . sfConfig::get('app_centro_actual') . '. Mensaje: ' . $e->getMessage();
            $transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')->setUsername('bixit.s.a.c.v@gmail.com')->setPassword('B1x1tM41l!');
            $mailer    = Swift_Mailer::newInstance($transport);
            $mensaje   = Swift_Message::newInstance()->setFrom(array('bixit.s.a.c.v@gmail.com' => 'BiXIT'))
                                                     ->setTo('fs@enova.mx')
                                                     ->setSubject('[MAKO] Error al finalizar cursos')
                                                     ->setBody($cuerpo, 'text/html');
            $result    = $mailer->batchSend($mensaje);

            $this->logSection('No se pudo conectar con Moodle', '');
        }

        if ($moodleActivo) {
            $this->logSection('Finalizando grupos, centro:', sfConfig::get('app_centro_actual'));

            $criteria = new Criteria();
            $criteria->add(GrupoPeer::CENTRO_ID, sfConfig::get('app_centro_actual_id'));
            $criteria->add(CursoPeer::ASOCIADO_MOODLE, true);

            if ($options['desde']) {
                $desde = $options['desde'] . ' 00:00:00';
            } else {
                $criteriaFinalizados = new Criteria();
                $criteriaFinalizados->addDescendingOrderByColumn(CursosFinalizadosPeer::CREATED_AT);

                $desde = CursosFinalizadosPeer::doSelectOne($criteriaFinalizados);
                $desde = $desde->getCreatedAt('Y-m-d') . ' 00:00:00';
            }

            if ($options['hasta']) {
                $criteria->add(HorarioPeer::FECHA_FIN,
                               HorarioPeer::FECHA_FIN .
                                    " BETWEEN '" . $desde .
                                    "' AND '" . $options['hasta'] . ' 23:59:59' . "'",
                               Criteria::CUSTOM);
            } else {
                $criteria->add(HorarioPeer::FECHA_FIN,
                               HorarioPeer::FECHA_FIN .
                                    " BETWEEN '" . $desde .
                                    "' AND '" . date('Y-m-d') . ' 00:00:00' . "'",
                               Criteria::CUSTOM);
            }

            $criteria->addJoin(GrupoPeer::CURSO_ID, CursoPeer::ID);
            $criteria->addJoin(GrupoPeer::HORARIO_ID, HorarioPeer::ID);
            $criteria->add(GrupoPeer::ACTIVO, true);

            // Guardamos el reporte de asistencias por cada curso
            foreach (GrupoPeer::doSelect($criteria) as $grupo) {
                $this->log($grupo->getClave());
                if ($options['override'] == 0) {
                    $resultado = HorarioPeer::obtenerCalificacionesMoodle($grupo->getHorario(), false);
                }

                if ($options['override'] == 1) {
                    $resultado = HorarioPeer::obtenerCalificacionesMoodle($grupo->getHorario(), true);
                }
            }

            $this->logSection('Finalizado..', '');
        }
    }
}
?>

