<?php 

class SincroniaSapTask extends sfBaseTask
{

	public $path_log = '/var/log/mako/sincronia_sap.log';
	
  protected function configure()
  {
    $this->addOptions(array(
      new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'Entorno de ambiente', 'dev'),
      new sfCommandOption('accion', null, sfCommandOption::PARAMETER_OPTIONAL, 'Accion que se ejcutara', ''),
    ));
 
    $this->namespace = 'mako';
    $this->name = 'sincronia-sap';
    $this->briefDescription = 'Sincronia SAP. Envia informacion a SAP para registrar ventas.';
 
    $this->detailedDescription = <<<EOF
The [mako:pcs-watcher|INFO] Sincronia SAP:
 
  [./symfony mako:sincronia-sap --env=prod --accion=|INFO]
EOF;
  }
 
  protected function execute($arguments = array(), $options = array())
  {
   
    $databaseManager = new sfDatabaseManager($this->configuration);
    $configuration = ProjectConfiguration::getApplicationConfiguration('frontend',  'prod' , true);    
	$context = sfContext::createInstance($configuration);
    $configuration->loadHelpers('Facturas');
    $dispatcher = $configuration->getEventDispatcher();
    $conn = Propel::getConnection();

	$sinc_sap = new Criteria();
	$sinc_sap->add(SincroniaSapPeer::COMPLETO,false);
  $sinc_sap->addAscendingOrderByColumn(SincroniaSapPeer::OPERACION);
	$sinc_pendientes = SincroniaSapPeer::doSelect($sinc_sap);

    foreach($sinc_pendientes as $sinc)
    {

      $id_sinc     = $sinc->getPkReferencia();
      $id_sinc_sap = $sinc->getId();

      switch ($sinc->getOperacion()){
        case 'Agregar_Venta':
            $datos_orden = $this->consultaViewOrden($id_sinc);
            $params = $this->construir_parametros_sap_agregar_venta($datos_orden);
            $this->enviar_wsdl_sap($params, $id_sinc_sap, $sinc->getOperacion());
        break;
        case 'Devoluciones':
            $datos_orden = $this->consultaViewOrden($id_sinc);
            $params = $this->construir_parametros_sap_cancelar_venta($datos_orden);
            $this->enviar_wsdl_sap($params, $id_sinc_sap, $sinc->getOperacion());

        break;
        case 'Facturas':
            
            $respuesta = SincroniaTimbradoTask::obtenerFacturaTimbradaBd('subscriptionWS', $id_sinc, 'sap');
            
            continue;
        break;
        case 'Pagos':
            $datos_orden = $this->consultaCorteCaja($id_sinc);
            $params = $this->construir_parametros_sap_pagos($datos_orden);
            $this->enviar_wsdl_sap($params, $id_sinc_sap, $sinc->getOperacion());
        break;
         }

    }


  }

  public function solicitaTimbradoFacturaSap($params, $id_sinc_timbrado, $origen_peticion){
    $path_log = '/var/log/mako/sincronia_sap.log';

    $id_sinc_sap = self::verificaTimbradoFacturaSap($id_sinc_timbrado);

    //error_log('Origen de peticion '.$origen_peticion,3,$path_log);

    if($origen_peticion == 'sap'){

      $respuesta = self::enviar_wsdl_sap($params,$id_sinc_sap,"Facturas");

    }

    return $respuesta;

  }

  private function verificaTimbradoFacturaSap($id_sinc_timbrado){

     $path_log = '/var/log/mako/sincronia_sap.log';
        
        $ss = new Criteria();
        $ss->add(SincroniaSapPeer::PK_REFERENCIA,$id_sinc_timbrado);
        $sincronia_sap = SincroniaSapPeer::doSelectOne($ss);

       if ($sincronia_sap){

            $id_sinc_sap = $sincronia_sap->getId();

       }else{

            $sincronia_sap = new SincroniaSap();
            $sincronia_sap->setOperacion('Facturas');
            $sincronia_sap->setPkReferencia($id_sinc_timbrado);
            $sincronia_sap->setCompleto(false);
            $sincronia_sap->setCreatedAt(time());
            $sincronia_sap->save();

            $id_sinc_sap = $sincronia_sap->getId();
       }
    

    return $id_sinc_sap;

  }


  /**
    * Envia informacion al WSDL de SAP
    * @return $params parametros
    */
   public static function enviar_wsdl_sap($params, $id_sinc_sap, $modulo_sap) {

        $path_log = '/var/log/mako/sincronia_sap.log';


         try{

         $client = self::cliente_soap_sap($modulo_sap);

         ini_set('default_socket_timeout', 600);

         $result = $client->__soapCall($modulo_sap, array($params), NULL);
         error_log('MODULO => '.$modulo_sap. "\n",3,$path_log);
         error_log('RESULT => '.print_r($result,true). "\n",3,$path_log);
         error_log("Parametros ".print_r($params, true),3,$path_log);

         switch ($modulo_sap){

            case 'Agregar_Venta':
            if(isset($result->Agregar_VentaResult) and (strpos($result->Agregar_VentaResult, 'Se creó Orden') !== false)) {
                    error_log('Venta registrada correctamente en web service '.$modulo_sap,3,$path_log);
                    error_log('[SINCRONIA]: '.print_r($result, true),3,$path_log);
                    $completo=true;
                    self::guardarSincroniaSap($result->Agregar_VentaResult, $id_sinc_sap, $completo);
            } else {
                    error_log('Ocurrio un error al registrar la venta en el web service '.$result->Agregar_VentaResult. "\n",3,$path_log);
                    $completo=false;
                    self::guardarSincroniaSap($result->Agregar_VentaResult, $id_sinc_sap, $completo);
            }
            break;
            case 'Devoluciones':
            if(isset($result->DevolucionesResult) and (strpos($result->DevolucionesResult, 'Se creó Devolucion') !== false) or (strpos($result->DevolucionesResult, 'Se creó Nota de Credito') !== false)) {
               error_log('Cancelación registrada correctamente en web service '.$modulo_sap,3,$path_log);
               error_log('[SINCRONIA] $modulo_sap: '.print_r($result, true),3,$path_log);
               $completo=true;

               self::guardarSincroniaSap($result->DevolucionesResult, $id_sinc_sap, $completo);
            } else {
               error_log('Ocurrio un error al registrar la cancelación en el web service '.$result->DevolucionesResult. "\n",3,$path_log);
               $completo=false;
               self::guardarSincroniaSap($result->DevolucionesResult, $id_sinc_sap, $completo);
            }
            break;
            case 'Facturas':

                error_log('[SINCRONIA]: '.print_r($result, true),3,$path_log);

                if(isset($result->FacturasResult) and (strpos($result->FacturasResult, 'Se creó Factura') !== false)) {
                   error_log('Facturación registrada correctamente en web service '.$modulo_sap,3,$path_log);
                   error_log('[SINCRONIA]: '.print_r($result, true),3,$path_log);
                   $completo=true;
                   self::guardarSincroniaSap($result->FacturasResult, $id_sinc_sap, $completo);
                } else {
                   error_log('Factura '.$result->FacturasResult. "\n",3,$path_log);
                   error_log('Ocurrio un error al registrar la factura en el web service '.$result->FacturasResult. "\n",3,$path_log);
                   $completo=false;
                   self::guardarSincroniaSap($result->FacturasResult, $id_sinc_sap, $completo);
                }

                return $completo;

            break;
            case 'Pagos':
            if(isset($result->PagosResult) and (strpos($result->PagosResult, 'Se creó Pago') !== false)) {
               error_log('Corte caja registrada correctamente en web service '.$modulo_sap,3,$path_log);
               error_log('[SINCRONIA]: '.print_r($result, true),3,$path_log);
               $completo=true;
               self::guardarSincroniaSap($result->PagosResult, $id_sinc_sap, $completo);
            } else {
               error_log('Ocurrio un error al registrar corte de caja en el web service '.$result->StatusCenmp. "\n",3,$path_log);
               $completo=false;
               self::guardarSincroniaSap($result->PagosResult, $id_sinc_sap, $completo);
            }
            break;

         }
           
            



      }catch( SoapFault $sp) {
         error_log("Ocurrio un error al momento de consumir el servicio de SAP para modulo ".$modulo_sap.$sp,3,$path_log);

      }

    }
  
  
   /**
    * Construye los parametros para enviar a SAP Agregar_venta
    * @param  Datos Orden $datos_orden Los datos de la venta
    * @return array         Los parametros para mandar al web service de SAP Agregar_venta
    */
   public static function construir_parametros_sap_agregar_venta($datos_orden){

        $path_log = '/var/log/mako/sincronia_sap.log';

		$centro_id = sfConfig::get('app_centro_actual_id');
		$c = new Criteria();
		$c->add(CentroPeer::ID,$centro_id);
		$centro = CentroPeer::doSelectOne($c);

		$fecha_creacion = new DateTime($datos_orden[0]['fecha_creacion']);
        $fecha_vencimiento = new DateTime($datos_orden[0]['fecha_creacion']);
        $fecha_vencimiento->modify('last day of this month');

         if($datos_orden[0]['es_factura'] == false) {
            $cardcode = sfConfig::get('app_sap_card_code');
            $cardname = 'Publico ventas en general';
            $federaltaxid = sfConfig::get('app_sap_federal_tax_id');
         }else{
            $cardcode = "C-".$datos_orden[0]['rfc'];
            $cardname = $datos_orden[0]['nombre_completo'];
            $federaltaxid = $datos_orden[0]['rfc'];
         }

         $numatcard = sfConfig::get('app_sap_centro_costo') ."-".$datos_orden[0]['id'];

         $params = array(
         "NumAtCard"        => $numatcard,
         "RS_FAE_METPAGO"   => sfConfig::get('app_sap_rs_fae_metpago'),
         "RS_FAE_NUMCUENTA" => sfConfig::get('app_sap_rs_fae_numcuenta'),
         "DocDate"          => $fecha_creacion->format("d/m/Y"),
         "TaxDate"          => $fecha_creacion->format("d/m/Y"),
         "DocDueDate"       => $fecha_vencimiento->format("d/m/Y"),
         "CardCode"         => $cardcode,
         "CardName"         => $cardname,
         "Street"           => $centro->getCalle()." / ".$centro->getNumExt(),
         "Block"            => $centro->getColonia(),
         "City"             => $centro->getColonia(),
         "ZipCode"          => $centro->getCp(),
         "County"           => $centro->getMunicipio(),
         "Country"          => 'MX' ,
         "State"            => sfConfig::get('app_sap_state'),
         "DocCurrency"      => 'MXN' ,
         "FederalTaxID"     => $federaltaxid,
         "rendondear"       => false ,
         "SlpCode"          => sfConfig::get('app_sap_slpcode'),
         "Linea"            =>array()
         );

        $lineas = array();
        foreach ($datos_orden as $key_name => $key_value) {

            if ($datos_orden[$key_name]['categoria_producto'] == 'Libros'){
               $tasa = 0;
               $tax_code = 'IVATRA00';
            }else{
               $tasa = 16;
               $tax_code = 'IVATRA16';
            }
            
             $desg_iva = desglosa_iva($datos_orden[$key_name]['precio_lista'],$tasa);
             $totallinea = $desg_iva[1] * intval($datos_orden[$key_name]['cantidad']);
             $item_code = sfConfig::get('app_sap_prefijo_centro') . "-" . $datos_orden[$key_name]['codigo_producto'];
             $item_code = trim($item_code);

             $params["Linea"]["Lineas"][] = array(
             "Itemcode"         => $item_code,
             "ItemDescription"  => substr($datos_orden[$key_name]['descripcion_producto'],0,99),
             //"ItemDescription"  => "",
             "Quantity"         => intval($datos_orden[$key_name]['cantidad']),
             "DiscountPercent"  => intval($datos_orden[$key_name]['descuento']),
             "WarehouseCode"    => sfConfig::get('app_sap_centro_costo'),
             "TaxCode"          => $tax_code,
             "MeasureUnit"      => 'NO APLICA',
             "LineTotal"        => $totallinea,
             "UnitPrice"        => $desg_iva[1],
             "COGSAccountCode"  => sfConfig::get('app_sap_cogs_account_code'),
             "COGSAccountCode2" => sfConfig::get('app_sap_cogs_account_code2'),
             "COGSAccountCode3" => sfConfig::get('app_sap_cogs_account_code3'),
             "COGSAccountCode4" => sfConfig::get('app_sap_cogs_account_code4'),
             "COGSAccountCode5" => sfConfig::get('app_sap_cogs_account_code5')
             );

        }
      
        

      return $params;
   }

 /**
    * Construye los parametros para enviar a SAP Devoluciones
    * @param  Datos Orden $datos_orden Los datos de la venta
    * @return array         Los parametros para mandar al web service de SAP Devoluciones
    */
   public static function construir_parametros_sap_cancelar_venta($datos_orden){

        $path_log = '/var/log/mako/sincronia_sap.log';

        $fecha_cancelacion = self::consultaFechaCancelacion($datos_orden[0]['id']);
        //$fecha          = new DateTime('NOW');
        $fecha = new DateTime($fecha_cancelacion[0]['fecha_cancelacion']);
        $fecha_creacion = new DateTime($datos_orden[0]['fecha_creacion']);
        $numatcard = sfConfig::get('app_sap_centro_costo') ."-".$datos_orden[0]['id'];

         $params = array(
         "Ticket"         => $numatcard,
         "Fecha"          => $fecha->format("Y-m-d"),
         "FechaDoc"       => $fecha_creacion->format("Y-m-d"),
         "Total"          => $datos_orden[0]['total'],
         "rendondear"     => false 
         
        );
   
      return $params;
   }


    /**
    * Consulta fecha cancelacion
    * @return 
    */
   public static function consultaFechaCancelacion($id_sinc) {

        $conn = Propel::getConnection();        
        $sql = "SELECT created_at as fecha_cancelacion 
                FROM cancelacion
                WHERE orden_id = $id_sinc";
        $datos = $conn->query($sql);
        $fecha_cancelacion = $datos->fetchAll(PDO::FETCH_ASSOC);

    return $fecha_cancelacion;

   }

    /**
    * Construye los parametros para enviar a SAP Pagos
    * @param  Datos Orden $datos_orden Los datos del corte de caja
    * @return array         Los parametros para mandar al web service de SAP Pagos
    */
   public static function construir_parametros_sap_pagos($datos_orden){

        $path_log = '/var/log/mako/sincronia_sap.log';

        $fecha          = new DateTime($datos_orden[0]['fecha']);
        
        $cardcode = sfConfig::get('app_sap_card_code');
        
         $params = array(
         "Centrocostos" => sfConfig::get('app_sap_centro_costo'),
         "pago"         => 'EFECTIVO',
         "fecha"        => $fecha->format("Y-m-d"),
         "DocCurrency"  => 'MXN' ,
         "Cardcode"     => $cardcode
        );

      return $params;
   }

	

    /**
    * Guarda datos de sincronizacion con SAP
    * @return 
    */
   public static function guardarSincroniaSap($result, $id_sinc_sap, $completo) {
     $path_log = '/var/log/mako/sincronia_sap.log';
        $ss = new Criteria();
		$ss->add(SincroniaSapPeer::ID,$id_sinc_sap);
		$sincronia_sap = SincroniaSapPeer::doSelectOne($ss);

		if ($sincronia_sap){
			$sincronia_sap->setRespuesta($result);
			$sincronia_sap->setCompleto($completo);
			$intentos = $sincronia_sap->getIntentos() + 1;
			$sincronia_sap->setIntentos($intentos);
			$sincronia_sap->setFechaSincronia(time());
			$sincronia_sap->save();
		}

	return $sincronia_sap;

   }

    /**
    * Consulta view datos de orden
    * @return 
    */
   public static function consultaViewOrden($id_sinc) {

        $conn = Propel::getConnection();
        $sql ="SELECT * FROM view_agregar_venta_sap where id = $id_sinc";
        $datos = $conn->query($sql);
        $datos_orden = $datos->fetchAll(PDO::FETCH_ASSOC);

    return $datos_orden;

   }

    /**
    * Consulta corte de caja
    * @return 
    */
   public static function consultaCorteCaja($id_sinc) {

        $conn = Propel::getConnection();        
        $sql = "SELECT sum(o.total) as total, co.created_at as fecha, o.rfc as cardcode 
                FROM orden o INNER JOIN corte co ON co.id = o.corte_id 
                WHERE co.id = $id_sinc AND tipo_id = 1 GROUP BY co.created_at, o.rfc";
        $datos = $conn->query($sql);
        $datos_orden = $datos->fetchAll(PDO::FETCH_ASSOC);

    return $datos_orden;

   }

   
   /**
    * Construye el cliente soap para comunicarse con el web service de SAP
    * @return SoapClient El cliente soap
    */
   public static function cliente_soap_sap($modulo_sap) {

  //error_log("\n\rConectando a....." . sfConfig::get('app_sap_wsdl_'.strtolower($modulo_sap)));

  return new SoapClient(sfConfig::get('app_sap_wsdl_'.strtolower($modulo_sap)));

   }

}
?>

