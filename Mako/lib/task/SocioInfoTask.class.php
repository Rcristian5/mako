<?php
class SocioInfoTask extends sfBaseTask
{
    public $log = array();
    public $res = 0;

    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'                                                          ,  'tolu2' ),
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección'                                            , 'propel'),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación'                                           , 'frontend'),
            new sfCommandOption('usuario'    , null, sfCommandOption::PARAMETER_REQUIRED, 'Nombre de usuario del socio a consultar'                              , null),
            new sfCommandOption('clave'      , null, sfCommandOption::PARAMETER_OPTIONAL, 'Clave nueva a modificar'                                              , null),
            new sfCommandOption('segundos'   , null, sfCommandOption::PARAMETER_OPTIONAL, 'Segundos a agregar al saldo del socio'                                , 0),
            new sfCommandOption('tipo'       , null, sfCommandOption::PARAMETER_OPTIONAL, 'Tipo de sesión del socio'                                             , "default"),
            new sfCommandOption('accion'     , null, sfCommandOption::PARAMETER_REQUIRED, 'Accion a realizar ["consultar", "sincronizar", "crear", "actualizar"]', 'consultar')
        ));

        $this->namespace           = 'soporte';
        $this->name                = 'socio-info';
        $this->briefDescription    = 'Muestra información básica de registro del socio, así como sincronía con LDAP y sesiones actuales.';
        $this->detailedDescription = <<<EOF
  [mako soporte:socio-info|INFO] Muestra información básica de registro del socio y también realiza operaciones de sincronía con LDAP,

  --accion=sincronizar : Sincroniza la clave en BD con la clave en LDAP.
  --accion=crear:  Si el usuario no existe en LDAP lo creo apartir de los datos de la BD local.
  --accion=actualizar: Si los datos del socio no están actualizados en LDAP los actualiza.
  --accion="liberar-host": Regresa el valor del host al valor default.
  --accion="crear-moodle": Crea el usuario en Moodle.
  --accion="modificar-clave" --clave="nuevaclave": Modifica la clave a la proporcionada.
  --accion="agregar-saldo" --segundos=3600: Agrega saldo al existente en la cuenta del socio, el valor debe estar en segundos, el ejemplo anterior agrega una hora al saldo actual del socio, si el usuario tuviera 30 minutos de saldo, después de ejecutar el comando tendría una hora y media de saldo.
  --accion="modificar-sesion" --tipo="default": Modifica el tipo de sesión permitida para este usuario.
  --accion="reactivar": Reactiva a un socio dado de baja en recepcion.
  --accion="info-cursos": Muestra información de los cursos pagados por el socio.
  --accion="matricular-cursos": Enrola al socio en los cursos que ha adquirido y que no ha sido enrolado.

  [mako soporte:socio-info --usuario="nombre.usuario" --accion="consultar" |INFO]  Muestra la información básica del socio.
  [mako soporte:socio-info --usuario="nombre.usuario" --accion="sincronizar" |INFO]  Sincroniza la clave existente en Mako y clave LDAP
  [mako soporte:socio-info --usuario="nombre.usuario" --accion="crear" |INFO]  Crea los datos de un usuario existente en base local en LDAP
  [mako soporte:socio-info --usuario="nombre.usuario" --accion="actualizar" |INFO]  Actualiza la información en LDAP con los datos del socio
  [mako soporte:socio-info --usuario="nombre.usuario" --accion="liberar-host" |INFO]  Regresa el valor del host al valor default *
  [mako soporte:socio-info --usuario="nombre.usuario" --accion="crear-moodle" |INFO]  Crea los datos de un usuario existente en base local en Moodle
  [mako soporte:socio-info --usuario="nombre.usuario" --accion="modificar-clave" --clave="nuevaclave" |INFO]  Modifica la clave a la proporcionada en la opcion --clave
  [mako soporte:socio-info --usuario="nombre.usuario" --accion="agregar-saldo" --segundos=[cantidad] |INFO]  Agrega "cantidad" de segundos al saldo actual del socio.
  [mako soporte:socio-info --usuario="nombre.usuario" --accion="modificar-sesion" --tipo="[tipo sesion]" |INFO]  Modifica el tipo de sesión del socio por alguno de los siguientes tipos: "default", "sin consumo", "multi sesion", "multi sesion sin consumo".
  [mako soporte:socio-info --usuario="nombre.usuario" --accion="reactivar" |INFO]  Reactiva una cuenta de socio dada de baja desde recepción.
  [mako soporte:socio-info --usuario="nombre.usuario" --accion="info-cursos" |INFO]  Muestra información de los cursos adquiridos por el socio.
  [mako soporte:socio-info --usuario="nombre.usuario" --accion="matricular-cursos" |INFO]  Matricula en los cursos pagados por el socio que por algún motivo no pudieron matricularse en el momento de la compra.

EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'] ? $options['connection'] : null)->getConnection();
        $configuration   = ProjectConfiguration::getApplicationConfiguration('frontend', $options['env'], true);
        $context         = sfContext::createInstance($configuration);

        $configuration->loadHelpers('Ldap');

        if ($options['usuario'] == null or $options['usuario'] == '') {
            $this->log[] = $this->formatter->format("Porfavor teclea:", 'ERROR') . $this->formatter->format(" mako ayuda soporte:socio-info", 'COMMENT');
            $this->log($this->log);

            throw new Exception("El nombre de usuario del socio es un dato requerido!", "1");

            $this->res = 1;
        }

        $c = new Criteria();
        $c->add(SocioPeer::USUARIO, $options['usuario']);

        $as = SocioPeer::doSelect($c);

        if (count($as) < 1) $this->log[] = $this->formatter->format("\n===No existe el usuario en la base local de este centro.===\n", 'ERROR');

        foreach ($as as $socio) {
            if ($options['accion'] == 'consultar') {
                $this->info($socio);
            } elseif ($options['accion'] == 'sincronizar') {
                try {
                    modificarClaveSocioLDAP($socio->getUsuario(), $socio->getClave());
                    $this->log[] = $this->formatter->format("****Se ha enviado orden de sincronía de claves****", 'COMMENT');
                }
                catch(Exception $e) {
                    $this->log[] = $this->formatter->format("Error al sincronizar claves: " . $e->getMessage(), 'ERROR');
                }
            } elseif ($options['accion'] == 'crear') {
                if (!verificaUsuarioLDAP($socio->getUsuario())) {
                    try {
                        $this->crearSocio($socio);
                        $this->log[] = $this->formatter->format("Se ha creado: $socio", 'INFO');
                    }
                    catch(Exception $e) {
                        $this->log[] = $this->formatter->format("Error al crear al socio: $socio", 'ERROR');
                    }
                } else {
                    $this->log[] = $this->formatter->format("Un usuario con el mismo username ya existe en LDAP, no es posible crearlo!:", 'ERROR');
                }
            } elseif ($options['accion'] == 'actualizar') {
                if (verificaUsuarioLDAP($socio->getUsuario())) {
                    try {
                        $this->actualizarSocio($socio);
                        $this->log[] = $this->formatter->format("Se ha actualizado: $socio", 'INFO');
                    }
                    catch(Exception $e) {
                        $this->log[] = $this->formatter->format("Error al actualizar al socio: $socio", 'ERROR');
                    }
                } else {
                    $this->log[] = $this->formatter->format("El usuario no existe en LDAP no es posible actualizarlo!:", 'ERROR');
                }
            } elseif ($options['accion'] == 'liberar-host') {
                if (verificaUsuarioLDAP($socio->getUsuario())) {
                    try {
                        modificarHostPermitido($options['usuario'], '*');
                        $this->log[] = $this->formatter->format("Se ha actualizado: $socio", 'INFO');
                    }
                    catch(Exception $e) {
                        $this->log[] = $this->formatter->format("Error al liberar el host: $socio", 'ERROR');
                    }
                } else {
                    $this->log[] = $this->formatter->format("El usuario no existe en LDAP no es posible actualizarlo!:", 'ERROR');
                }
            } elseif ($options['accion'] == 'crear-moodle') {
                $this->crearEnMoodle($socio);
            } elseif ($options['accion'] == 'modificar-clave') {

                if (trim($options['clave']) == '' or $options['clave'] == null) {
                    print $this->formatter->format("Debe proporcionar una clave!:", 'ERROR');
                    exit;
                }
                $this->modificarClave($socio, $options['clave']);
                $this->log[] = $this->formatter->format("Se ha modificado la clave del socio: " . $socio->getUsuario() . "\n", 'INFO');
            } elseif ($options['accion'] == 'agregar-saldo') {

                if (trim($options['segundos']) == '' or $options['segundos'] == null) {
                    print $this->formatter->format("Debe proporcionar los segundos de saldo a abonar!:", 'ERROR');
                    exit;
                }
                $this->agregarSaldo($socio, $options['segundos']);
                $this->log[] = $this->formatter->format("Se han agregado " . $options['segundos'] . " segundos al saldo del socio: " . $socio->getUsuario() . "\n", 'INFO');
            } elseif ($options['accion'] == 'modificar-sesion') {

                if (trim($options['tipo']) == '' or $options['tipo'] == null) {
                    print $this->formatter->format("Debe proporcionar el tipo de sesión!:", 'ERROR');
                    exit;
                }
                $this->modificarSesion($socio, $options['tipo']);
                $this->log[] = $this->formatter->format("Se ha modificado el tipo de sesion a '" . $options['tipo'] . "' para el socio: " . $socio->getUsuario() . "\n", 'INFO');
            } elseif ($options['accion'] == 'reactivar') {
                $this->log[] = $this->reactivar($socio);
            } elseif ($options['accion'] == 'info-cursos') {
                $this->log[] = $this->infoCursos($socio, false);
            } elseif ($options['accion'] == 'matricular-cursos') {
                $this->log[] = $this->infoCursos($socio, true);
            }
        }
        $this->log($this->log);
        exit($this->res);
    }

    function info(Socio $socio) {
        $compara = comparaClavesLdap($socio->getUsuario(), $socio->getClave());

        if ($compara === 0) {
            $this->log[] = $this->formatter->format("\n===No existe el usuario en el servidor LDAP.===\n", 'ERROR');
            return;
        }

        $inm = $this->infoMoodle($socio);
        $um  = $inm->users;

        //print_r($um[0]->id);

        $idmoo       = intval($um[0]->id);
        $resmoo      = ($idmoo == 0) ? $this->formatter->format("NO existe en Moodle", 'ERROR') : $idmoo;
        $this->log[] = $this->formatter->format("\n====Consulta de información de socio====\n", 'COMMENT');
        $this->log[] = $this->formatter->format("Folio:  \t", 'COMMENT') . $socio->getFolio();
        $this->log[] = $this->formatter->format("Usuario: \t", 'COMMENT') . $socio->getUsuario();
        $this->log[] = $this->formatter->format("Nombre: \t", 'COMMENT') . $socio;
        $this->log[] = $this->formatter->format("Alta en: \t", 'COMMENT') . $socio->getCentro();
        $this->log[] = $this->formatter->format("Fecha Alta: \t", 'COMMENT') . $socio->getCreatedAt();
        $this->log[] = $this->formatter->format("Estatus: \t", 'COMMENT') . ($socio->getActivo() ? 'ACTIVO' : 'INACTIVO');
        $this->log[] = $this->formatter->format("Saldo tiempo: \t", 'COMMENT') . ($socio->getSaldo() . " Segundos.");
        $this->log[] = $this->formatter->format("UID LDAP: \t", 'COMMENT') . $socio->getLdapUid();
        $this->log[] = $this->formatter->format("UID Mako: \t", 'COMMENT') . $socio->getId();
        $this->log[] = $this->formatter->format("ID Moodle: \t", 'COMMENT') . $resmoo;
        $this->log[] = $this->formatter->format("Sesión:  \t", 'COMMENT') . $this->enSesion($socio);
        $this->log[] = $this->formatter->format("Tipo sesión:  \t", 'COMMENT') . $socio->getTipoSesion()->getNombre();
        $this->log[] = $this->formatter->format("Host LDAP: \t", 'COMMENT') . hostEnLdap($socio->getUsuario());

        if ($compara == true) {
            $this->log[] = $this->formatter->format("Estatus clave: \t", 'COMMENT') .
                           $this->formatter->format("Claves coincidentes", 'INFO');
        } else {
            $this->log[] = $this->formatter->format("Estatus clave: \t", 'COMMENT') .
                           $this->formatter->format("Claves NO coinciden", 'ERROR');
        }
    }

    function agregarSaldo(Socio $socio, $segundos) {
        $saldo_actual = $socio->getSaldo();

        $socio->setSaldo($saldo_actual + intval($segundos));
        $socio->save();

        foreach ($socio->getSesions() as $sesion) {
            $sesion->setSaldo($saldo_actual + intval($segundos));
            $sesion->save();
        }

        return true;
    }

    /**
     * Muestra si tiene sesion actualmente en este centro o no.
     * Consulta de la tabla de sesion la informacion
     * @param Socio $socio
     */
    function enSesion(Socio $socio) {
        $arLog    = array();

        foreach ($socio->getSesions() as $sesion) {
            $arLog[] = $this->formatter->format("\tPC: " . $sesion->getIp(), 'MESSAGE');
        }
    }

    function crearSocio(Socio $socio) {
        if ($socio->getLdapUid() > 0) {
            $ldap_uid = $socio->getLdapUid();
        } else {
            $ldap_uid = null;
        }

        $uid = agregarSocioLdap($socio->getUsuario(),
                                $socio->getNombre(),
                                trim($socio->getApepat() . " " . $socio->getApemat()),
                                $socio->getClave(),
                                $socio->getEmail(),
                                $socio->getEstado(),
                                $socio->getDirgmaps(),
                                $socio->getTel(),
                                $socio->getCelular(),
                                $socio->getCentro()->getAlias(),
                                $socio->getCentroId(),
                                $socio->getId(),
                                $ldap_uid);
        if ($uid == false) {
            //error_log("Error[OperacionLdap.registrarSocio]: Ocurrión un error al registrar al socio ".$socio);
            $this->log[] = $this->formatter->format("Ocurrió un error al registrar al socio $socio", 'ERROR');
            $this->log[] = $this->formatter->format($socio->getUsuario(), 'ERROR');
            $this->log[] = $this->formatter->format($socio->getNombre(), 'ERROR');
            $this->log[] = $this->formatter->format(trim($socio->getApepat() . " " . $socio->getApemat()), 'ERROR');
            $this->log[] = $this->formatter->format($socio->getClave(), 'ERROR');
            $this->log[] = $this->formatter->format($socio->getEmail(), 'ERROR');
            $this->log[] = $this->formatter->format($socio->getEstado(), 'ERROR');
            $this->log[] = $this->formatter->format($socio->getDirgmaps(), 'ERROR');
            $this->log[] = $this->formatter->format($socio->getTel(), 'ERROR');
            $this->log[] = $this->formatter->format($socio->getCelular(), 'ERROR');
            $this->log[] = $this->formatter->format($socio->getCentro()->getAlias(), 'ERROR');
            $this->log[] = $this->formatter->format($socio->getCentroId(), 'ERROR');
            $this->log[] = $this->formatter->format($socio->getId(), 'ERROR');
            $this->log[] = $this->formatter->format($ldap_uid, 'ERROR');

            //$this->log[] = $this->formatter->format(print_r($socio->toArray(),true), 'ERROR');

        } elseif ($ldap_uid == null) {
            $socio->setLdapUid($uid);
            $socio->save();
        }
    }

    function actualizarSocio(Socio $socio) {
        modificarSocioLDAP($socio->getUsuario(),
                           $socio->getNombre(),
                           trim($socio->getApepat() . " " . $socio->getApemat()),
                           $socio->getClave(),
                           $socio->getEmail(),
                           $socio->getEstado(),
                           $socio->getDirgmaps(),
                           $socio->getTel(),
                           $socio->getCelular(),
                           $modificaUsername,
                           $usuarioAnterior,
                           $socio->getCentroId(),
                           $socio->getId());

        $ldap_uid = leeUserUID($socio->getUsuario());

        if ($socio->getLdapUid() != $ldap_uid) {
            $socio->setLdapUid($ldap_uid);
            $socio->save();
        }
    }

    function infoMoodle(Socio $socio) {
        $info = OperacionMoodle::comprobarRegistro($socio);

        return $info;
    }

    function crearEnMoodle(Socio $socio) {
        $inm   = $this->infoMoodle($socio);
        $um    = $inm->users[0];
        $idmoo = intval($um->id);

        if ($idmoo == 0) {
            OperacionMoodle::registrarSocio($socio);
        } else {
            $this->log[] = $this->formatter->format("Ya existe en Moodle", 'ERROR');
        }
    }

    function modificarClave(Socio $socio, $clave) {
        $usuario = $socio->getUsuario();
        $clave   = pg_escape_string($clave);

        modificarClaveSocioLDAP($usuario, $clave);

        $socio->setClave($clave);
        $socio->save();
    }

    function modificarSesion(Socio $socio, $tipo) {
        $tipo_id = 0;

        if ($tipo == 'default') {
            $tipo_id = 1;
        } elseif ($tipo == 'sin consumo') {
            $tipo_id = 2;
        } elseif ($tipo == 'multisesion') {
            $tipo_id = 3;
        } elseif ($tipo == 'multisesion sin consumo') {
            $tipo_id = 4;
        }

        if ($tipo_id > 0) {
            $socio->setTipoSesionId($tipo_id);
            $socio->save();
        }
    }

    function reactivar(Socio $socio) {
        $socio->setActivo(true);
        $socio->save();
        return $this->formatter->format("Se ha reactivado al socio. Ya debe aparecer en la busqueda de Mako en recepcion.", 'INFO');
    }

    function infoCursos(Socio $socio, $enrolar) {
        try {
            $cliente = new SoapClient(sfConfig::get('app_soap_client'));
            $sesion  = $cliente->login(sfConfig::get('app_soap_user'), sfConfig::get('app_soap_password'));
        }
        catch(Exception $e) {
            return $this->formatter->format("Error de conexion. No ha sido posible conectar con Moodle, el resultado es este: " . $e->getMessage(), 'ERROR');
        }

        /*$c = new Criteria();
         $c->add(ProductoPeer::CATEGORIA_ID,2); //categoria de cursos.
        $c->addAscendingOrderByColumn(DetalleOrdenPeer::CREATED_AT);
        $res = $socio->getDetalleOrdensJoinProducto($c);
        $cursos = array();

        if(count($res) == 0) //No se enecontraron pagos de cursos informamos y salimos
        {
        return $this->formatter->format("El socio no ha adquirido ningún curso.", 'ERROR');
        }
        foreach($res as $compra)
        {
        $codigo = $compra->getProducto()->getCodigo();
        $nombre = $compra->getProducto()->getNombre();
        if(!isset($cursos[$codigo]))
        {
        $cursos[$codigo] = $nombre;
        }
        }
        */

        $c = new Criteria();
        $c->add(GrupoAlumnosPeer::ALUMNO_ID, $socio->getId());
        $c->add(GrupoAlumnosPeer::PREINSCRITO, false);
        $c->addAscendingOrderByColumn(GrupoAlumnosPeer::UPDATED_AT);

        $usuario = $socio->getUsuario();
        $msg     = "";

        foreach (GrupoAlumnosPeer::doSelect($c) as $ga) {
            $grupo   = $ga->getGrupo();
            $curso   = $grupo->getCurso();
            $nombre  = $curso->getNombre();
            $codigo  = $curso->getClave();

            //Si el curso no está asociado a moodle no revisamos en moodle.
            if (!$curso->getAsociadoMoodle()) {
                $res_msg = $res_msg = $this->formatter->format("NO SE IMPARTE CON MOODLE", 'INFO');
                $msg.= $this->formatter->format($codigo . " | " . $nombre . " | ", 'COMMENT') . $res_msg . "\n";
                continue;
            }

            //Consultamos estatus de enrolamiento del alumno al grupo.
            try {
                $res     = $cliente->is_enrolled($sesion->client, $sesion->sessionkey, $codigo, $usuario, 'idnumber', 'username');
                if ($res == null) {
                    $res_msg = $this->formatter->format("NO MATRICULADO", 'ERROR');
                    if ($enrolar) {
                        $msg.= $this->enrolar($socio, $codigo, $cliente, $sesion);
                        $res_msg = "";
                    }
                } else {
                    $res_msg = $this->formatter->format("MATRICULADO", 'INFO');
                }

                $msg.= $this->formatter->format($codigo . " | " . $nombre . " | ", 'COMMENT') . $res_msg . "\n";
            }
            catch(Exception $e) {
                $err = $this->formatter->format("No ha sido posible consultar los datos de enrolamiento: " . $e->getMessage(), 'ERROR');
                return $err;
            }
        }
        return $msg;
    }

    function enrolar(Socio $socio, $curso, $cliente, $sesion) {
        $msg     = "";
        $usuario = $socio->getUsuario();

        try {
            $msg.= $this->formatter->format("MATRICULANDO en ... " . $curso . " | " . $nombre . " | ", 'COMMENT') . "\n";
            $res = $cliente->enrol_students($sesion->client, $sesion->sessionkey, $curso, array($usuario), 'idnumber', 'username');
            $msg.= $this->formatter->format("MATRICULADO OK", 'INFO') . "\n";

            print_r($res);

            return $msg;
        }
        catch(Exception $e) {
            error_log("Error[OperacionMoodle.inscribirAlumno]: " . $e->getMessage());
            $msg.= $this->formatter->format("ERROR AL MATRICULAR!! favor de revisar que existe conexion a Moodle desde el centro.", 'ERROR') . "\n";
            return $msg;
        }
    }
}
?>