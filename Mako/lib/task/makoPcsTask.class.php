<?php
/**
 * Task que regresa una lista de PCS para utilizarse en comandos cli
 *
 * @author edgar
 *
 */
class makoPcsTask extends sfBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación'                                 , 'frontend'),
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'                                                ,  'tolu2' ),
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conexión'                                   , 'propel'),
            new sfCommandOption('centro'     , null, sfCommandOption::PARAMETER_OPTIONAL, 'El alias de centro del que se requiere la lista de pcs'     , null),
            new sfCommandOption('aula'       , null, sfCommandOption::PARAMETER_OPTIONAL, 'El alias del aula que se requieren las pcs'                 , null),
            new sfCommandOption('tipo'       , null, sfCommandOption::PARAMETER_OPTIONAL, 'Tipo de PCs que se desea consultar: recepcion, socio, todas', 'todas'),
            new sfCommandOption('id'         , null, sfCommandOption::PARAMETER_OPTIONAL, 'El identificador que regresa, ip, alias, nombre, extracto'  , 'ip')
        ));

        $this->namespace           = 'mako';
        $this->name                = 'consulta-pcs';
        $this->briefDescription    = '';
        $this->detailedDescription = <<<EOF
 [mako:pcs|INFO] Regresa datos de las pcs de algún centro o sección.

  [php symfony mako:consulta-pcs |INFO]
  [php symfony mako:consulta-pcs --centro=tolu1 --id="ip" |INFO]
  [php symfony mako:consulta-pcs --centro=tolu1 --aula="AULA 1" --id="ip" |INFO]
  [php symfony mako:consulta-pcs --centro=tolu1 --aula="AULA 1" --id="ip" --tipo=socio |INFO]
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $configuration   = ProjectConfiguration::getApplicationConfiguration('frontend', $options['env'], true);
        $context         = sfContext::createInstance($configuration);
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'] ? $options['connection'] : null)->getConnection();
        $tipo            = $options['tipo'];
        $centro_id       = sfConfig::get('app_centro_actual_id');

        $c = new Criteria();
        if ($options['centro'] == null) {
            $centro = CentroPeer::retrieveByPK($centro_id);
        } else {
            $c                         = new Criteria();
            $c->add(CentroPeer::ALIAS, strtolower($options['centro']));
            $centro = CentroPeer::doSelectOne($c);
        }

        $salida = "";
        if ($tipo == 'todas') $cpc = null;
        elseif ($tipo == 'recepcion') {
            $cpc    = new Criteria();
            $cpc->add(ComputadoraPeer::TIPO_ID,
                      array(
                        sfConfig::get('app_caja_tipo_pc_id'),
                        sfConfig::get('app_operacion_tipo_pc_id')),
                      Criteria::IN);
        } elseif ($tipo == 'socio') {
            $cpc = new Criteria();
            $cpc->add(ComputadoraPeer::TIPO_ID,
                      sfConfig::get('app_usuario_tipo_pc_id'));
        }

        foreach ($centro->getComputadoras($cpc) as $pc) {
            if ($options['id'] == 'ip') $valor = $pc->getIp();
            if ($options['id'] == 'alias') $valor = $pc->getAlias();
            if ($options['id'] == 'nombre') $valor = $pc->getHostname();
            if ($options['id'] == 'extracto') {
                list($o1, $o2, $o3, $o4)        = explode('.', $pc->getIp());

                $valor = $o4;
            }

            if ($valor == $options['excluye']) continue;

            $salida.= $valor . "\n";
        }

        echo $salida;
    }
}
