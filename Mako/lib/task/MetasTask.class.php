<?php
class MetasTask extends sfBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'                                                                             ,  'tolu2' ),
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección'                                                               , 'propel'),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación'                                                              , 'frontend'),
            new sfCommandOption('inicio'     , null, sfCommandOption::PARAMETER_OPTIONAL, 'Fecha desde la que se consulta. Formato YYYY-mm-dd'                                      , date("Y-m-d")),
            new sfCommandOption('fin'        , null, sfCommandOption::PARAMETER_OPTIONAL, 'Fecha hasta la que se consulta. Formato YYYY-mm-dd'                                      , date("Y-m-d")),
            new sfCommandOption('autorango'  , null, sfCommandOption::PARAMETER_OPTIONAL, 'Indica si se debe determinar automaticamente el rango de fechas a procesar.'             , null),
            new sfCommandOption('indicadores', null, sfCommandOption::PARAMETER_OPTIONAL, 'Indica si se deben ejecutar solo los indicadores para otros reportes. (--indicadores=si)', false)
        ));

        $this->namespace           = 'reporte';
        $this->name                = 'metas';
        $this->briefDescription    = 'Genera los registros para reporte de metas.';
        $this->detailedDescription = <<<EOF

  [mako reporte:metas --inicio=2010-09-01 --fin=2010-12-31|INFO]: Genera los registros para reporte de metas, desde fecha inicio hasta fecha fin.
  [mako reporte:metas |INFO]: Genera los registros para reporte de metas de la fecha de hoy (momento en que se corre el comando).
  [mako reporte:metas --autorango=si|INFO]: Genera los registros para reporte de metas tomando la inicial como el día posterior a la fecha máxima que se ha generado el
  ultimo reporte, y fecha final el día de hoy.
  [mako reporte:metas --indicadores=si|INFO]: Genera los registros para reporte de otros indicadores solamente sin ejecutar los de metas

EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'] ? $options['connection'] : null)->getConnection();
        $configuration   = ProjectConfiguration::getApplicationConfiguration('frontend', $options['env'], true);
        $context         = sfContext::createInstance($configuration);

        $configuration->loadHelpers('ReportesMetas');

        //Aprovechamos para ejecutar algunos indicadores para reportes.
        $configuration->loadHelpers('Reportes');
        $configuration->loadHelpers('ReportesIndicadores');

        $inicio = $options['inicio'];
        $fin    = $options['fin'];
        echo "Centro:" . sfConfig::get('app_centro_actual') . " #### Inicia: " . date("Y-m-d H:i:s") . "\n";
        if ($options['autorango'] == '' || $options['autorango'] == null) {
            $centro_id = sfConfig::get('app_centro_actual_id');
            $fechas    = self::createDateRangeArray($inicio, $fin);
            foreach ($fechas as $fecha) {
                if (7 == date("N", strtotime($fecha))) {
                    error_log("- Brincamos domingo $fecha -");
                    continue;
                }

                echo $fecha . ' - ';

                if ($options['indicadores'] === false) {
                    productos($fecha);
                    becas($fecha);
                    fijas($fecha);
                }
                //Estos no son de metas, son de indicadores, pero aprovechamos para poder ejecutarlos ya que son muy similares.
                ventasCategoriaProducto($fecha);
                diaCentroOperacion($fecha);
                ocupacionHoras($fecha, $centro_id);
                reporteEstatusSocios($fecha, $centro_id);
                sociosActivosCentro($centro_id, $fecha);
                ventasCategoriaCursoCentro($centro_id, $fecha);
                ventasCursosCentro($centro_id, $fecha);
            }
        } else {
            $reportes = array("ReporteMetas", "ReporteVentasCategorias", "DiasOperacion", "OcupacionHoras", "EstatusSocios", "ReporteActivos", "ReporteCategoriaCursos", "ReporteVentaCursos");
            //Estos no son de metas, son de indicadores, pero aprovechamos para poder ejecutarlos ya que son muy similares.
            foreach ($reportes as $clase) {
                self::autorango($clase);
            }
        }
        echo "#### Fin: " . date("Y-m-d H:i:s") . "\n";
    }

    /**
     * El generador que se utilizará, para consultar al autorango,
     *
     * @param string $clase para consultar la ultima fecha existente en la tabla correspondiente a al clase.
     */
    protected function autorango($clase     = null) {
        if ($clase == null) return;

        $comienza  = '2010-09-20'; //Lunes 20 de septiembre inicia semana de fase 2
        $fin       = date("Y-m-d");

        $tsAhora   = date(U);
        //Si todavía no dan las nueve de la noche, restamos un día para no generar reportes con datos a medias. Puesto que no se ha realizado el corte de caja.
        if ($tsAhora < strtotime(date("Y-m-d") . " 21:00:00")) {
            $tsAhora   = strtotime("- 1 day", $tsAhora);
            $fin       = date("Y-m-d", $tsAhora);
        }

        //Si la opcion es autorango, Entonces vemos cual es el ultimo día que existe en la BD
        $centro_id = sfConfig::get('app_centro_actual_id');

        $obj  = new $clase();
        $peer = $obj->getPeer();
        $r    = new ReflectionObject($peer);

        $c = new Criteria();
        $c->addSelectColumn("max(fecha)");
        $c->add($r->getConstant("CENTRO_ID"), sfConfig::get('app_centro_actual_id'), Criteria::LESS_EQUAL);

        $pdoStmt = $peer->doSelectStmt($c);
        $d       = $pdoStmt->fetch();

        if ($d['max'] == $fin) {
            //Ya se ha generado el reporte para el día de hoy.
            echo "$clase Ya se ha generado el reporte para el día de hoy(1) " . $d['max'] . "\n";
        }

        if ($d['max'] != '') {
            //Preguntamos si la ultima fue sabado, para entonces brincarnos el domingo, es decir, sumarle un dia.
            if (6 == date("N", strtotime($d['max']))) {
                $comienza = date("Y-m-d", strtotime($d['max']) + 2 * 24 * 3600);

            } else {
                //Si es cualquier otro dia solo le sumamos 1
                $comienza = date("Y-m-d", strtotime($d['max']) + 24 * 3600);
            }

            //Revisamos si la fecha en que se ejecuta es domingo, entonces restamos un dia.
            if (7 == date("N")) {
                $fin      = date("Y-m-d", strtotime(date("Y-m-d")) - 24 * 3600);
                error_log("Domingo detectado ffn: $fin");
            }

            //Por ultimo revisamos si la ultima fecha en bd es la misma que la q se trata de ejecutar
            if ($d['max'] == $fin) {
                //Ya se ha generado el reporte para el día de hoy.
                echo "$clase Ya se ha generado el reporte para el día de hoy(2) " . $d['max'] . "\n";
            }
        }

        echo "$clase $comienza,$fin\t" . date("Y-m-d H:i:s") . "\n";
        $fechas = self::createDateRangeArray($comienza, $fin);

        foreach ($fechas as $fecha) {
            if (7 == date("N", strtotime($fecha))) {
                error_log("- Brincamos domingo $fecha -");
                continue;
            }

            echo $fecha . ' - ';

            if ($clase == "ReporteMetas") {
                productos($fecha);
                becas($fecha);
                fijas($fecha);
            }

            //Estos no son de metas, son de indicadores, pero aprovechamos para poder ejecutarlos ya que son muy similares.
            if ($clase == "ReporteVentasCategorias") ventasCategoriaProducto($fecha);
            if ($clase == "DiasOperacion") diaCentroOperacion($fecha);
            if ($clase == "OcupacionHoras") ocupacionHoras($fecha, $centro_id);
            if ($clase == "EstatusSocios") reporteEstatusSocios($fecha, $centro_id);
            if ($clase == "ReporteActivos") sociosActivosCentro($centro_id, $fecha);
            if ($clase == "ReporteCategoriaCursos") ventasCategoriaCursoCentro($centro_id, $fecha);
            if ($clase == "ReporteVentaCursos") ventasCursosCentro($centro_id, $fecha);
        }
    }

    function createDateRangeArray($strDateFrom, $strDateTo) {
        // takes two dates formatted as YYYY-MM-DD and creates an
        // inclusive array of the dates between the from and to dates.

        // could test validity of dates here but I'm already doing
        // that in the main script

        $aryRange  = array();

        $iDateFrom = mktime(1, 0, 0, substr($strDateFrom, 5, 2), substr($strDateFrom, 8, 2), substr($strDateFrom, 0, 4));
        $iDateTo   = mktime(1, 0, 0, substr($strDateTo, 5, 2), substr($strDateTo, 8, 2), substr($strDateTo, 0, 4));

        if ($iDateTo >= $iDateFrom) {
            array_push($aryRange, date('Y-m-d', $iDateFrom)); // first entry

            while ($iDateFrom < $iDateTo) {
                $iDateFrom+= 86400; // add 24 hours
                array_push($aryRange, date('Y-m-d', $iDateFrom));
            }
        }

        return $aryRange;
    }
}
?>