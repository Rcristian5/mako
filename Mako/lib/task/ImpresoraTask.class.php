<?php
class ImpresoraTask extends sfBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'                 ,  'tolu2' ),
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección'   , 'propel'),
            new sfCommandOption('transaccion', null, sfCommandOption::PARAMETER_OPTIONAL, 'Id de la transacción de la venta de la venta', null)
        ));

        $this->namespace           = 'mako';
        $this->name                = 'impresora';
        $this->briefDescription    = 'Ejecuta servicios alternos de impresion';
        $this->detailedDescription = <<<EOF
The [mako:impresora|INFO] Ejecuta servicios alternos de impresion.:

  [./symfony mako:impresora --env=dev |INFO]
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'] ? $options['connection'] : null)->getConnection();
        echo "- - -  Reimpresión de Ticket - - -\n";

        if(!isset($options['transaccion']))
            die("[ErrorA] No se ha introducido el Id de la Transaccion.\n\n");
        if(empty($options['transaccion']))
            die("[ErrorB] No se ha introducido el Id de la Transaccion.\n\n");
        if(!is_numeric($options['transaccion']))
            die("[Error] No se valor numerico.\n\n");
        
        echo "\n";
        echo "Transaccion: {$options['transaccion']}\n\n";
        $c = new Criteria();
        $c->add(OrdenPeer::ID, $options['transaccion']);

        $Orden = OrdenPeer::doSelectOne($c);
        
        if(!$Orden)
            die("[Error] No se ha encontrado la Orden con el Id de Transacción {$options['transaccion']}\n\n");

        $configuration   = ProjectConfiguration::getApplicationConfiguration('frontend', $options['env'], true);
        $context         = sfContext::createInstance($configuration);
        
        ImpresoraTickets::generaTicketVenta($context, $Orden);
        echo "\n --- Terminó el proceso\n\n\n";
        exit;
    }
}
?>