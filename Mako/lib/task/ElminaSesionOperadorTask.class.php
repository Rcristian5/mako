<?php
class EliminaSesionOperadorTask extends sfBaseTask
{
    //Comando para ejecutar en pc
    private $cmd = '/usr/bin/perl /opt/MakoScripts/ejecuta_pc_exp.pl ';

    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'                          ,  'tolu2' ),
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección'            , 'propel'),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación'           , 'frontend'),
            new sfCommandOption('centro'     , null, sfCommandOption::PARAMETER_OPTIONAL, 'Centro donde se realiza la operación' , null),
            new sfCommandOption('aula'       , null, sfCommandOption::PARAMETER_OPTIONAL, 'Aula donde se realiza la operacion'   , null),
            new sfCommandOption('ip'         , null, sfCommandOption::PARAMETER_OPTIONAL, 'IP donde se queire eliminar la sesión', null)
        ));

        $this->namespace           = 'mako';
        $this->name                = 'elimina-sesion-operador';
        $this->briefDescription    = 'Elimina las sesiones de operador.';
        $this->detailedDescription = <<<EOF
    [mako:elimina-sesion-operador|INFO]  Elimina sesiones de operador.

EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $con      = $databaseManager->getDatabase($options['connection'] ? $options['connection'] : null)->getConnection();

        $con->exec("DELETE FROM sesion_operador");
    }
}
?>