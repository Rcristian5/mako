<?php
/**
 * Manda a la impresora de caja del centro los comprobantes del corte indicado
 *
 * @author edgar
 *
 */
class ReimprimeComprobantesCorteTask extends sfBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'Modo'                                           , 'frontend'),
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'Contexto'                                       ,  'tolu2' ),
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'Controlador'                                    , 'propel'),
            new sfCommandOption('fecha'      , null, sfCommandOption::PARAMETER_OPTIONAL, 'Fecha del corte'                                , null),
            new sfCommandOption('hora'       , null, sfCommandOption::PARAMETER_OPTIONAL, 'Hora del corte'                                 , null),
            new sfCommandOption('factura'    , null, sfCommandOption::PARAMETER_OPTIONAL, 'Nombre del archivo de factura correspondiente'  , null),
            new sfCommandOption('facturat0'  , null, sfCommandOption::PARAMETER_OPTIONAL, 'Nombre del archivo de factura de ventas tasa 0.', null),
        ));

        $this->namespace           = 'soporte';
        $this->name                = 'reimprime-comprobantes-corte';
        $this->briefDescription    = 'Reimprime los comprobantes de corte de caja dada la fecha y hora del corte';
        $this->detailedDescription = <<<EOF
[soporte:reimprime-comprobantes-corte --fecha="2010-01-01" --hora="18:34:32" --factura=FPE081202KW5SAFEA59 --facturat0=FPE081202KW5SAFEA60 |INFO] Reimprime los comprobantes del corte de caja dada su fecha y hora
Debe proporcionarse la fecha, hora exacta con detalle de segundos y el nombre de la factura (opcional), si existe una factura debida a ventas con tasa 0 debe indicarse tambien el nombre de dicha factura tasa0 (opcional).
EOF;
    }

    protected function execute($arguments = array(), $options = array()) {
        $ctx = sfContext::createInstance($this->configuration);
        $ctx->getConfiguration()->loadHelpers('Partial');
        $ctx->getConfiguration()->loadHelpers('Facturas');

        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'] ? $options['connection'] : null)->getConnection();
        $folio           = intval($options['folio']);
        $fecha           = $options['fecha'];
        $hora            = $options['hora'];

        if($fecha == null || $hora == null) {
            echo "Debe proporcionar los datos.";
            exit;
        }

        //Primero debemos traer los datos del corte de caja
        $c = new Criteria();
        $c->add(CortePeer::CREATED_AT, $fecha." ".$hora);

        $corte = CortePeer::doSelectOne($c);

        if($corte == null) {
            echo "No existe el corte solicitado";
            exit;

        } else {
            //Despues traemos los datos de la impresora del centro.
            $centro    = $corte->getCentro();
            $centro_ip = $centro->getIp();

            list($r1,$r2,$r3,$r4) = explode(".", $centro_ip);

            //Ip de la caja.
            $ip = $r1.".".$r2.".".$r3."."."99";

            echo "IP CAJA:".$ip."\n";

            $corte_id     = $corte->getId();
            $tick_resumen = PATH_TICKETS."/ticket_resumen".$corte_id.'.txt';

            if(!file_exists($tick_resumen)) {
                echo "No existe el ticket resumen del corte de caja.";
                exit;

            } else {
                $res = exec("lp -h ".$ip." -d okipos $tick_resumen");

                //Por duplicado
                $res = exec("lp -h ".$ip." -d okipos $tick_resumen");
            }

            $tick_detalle = PATH_TICKETS."/ticket_detalle".$corte_id.'.txt';

            if(!file_exists($tick_detalle)) {
                echo "No existe el ticket detalle del corte de caja.";
                exit;

            } else {
                $res = exec("lp -h ".$ip." -d okipos $tick_detalle");
            }

            if(isset($options['factura'])) {
                $factura = PATH_FACTURAS."/".$options['factura'].".txt";

                if(!file_exists($factura)) {
                    echo "No existe el archivo de factura proporcionado.";
                    exit;
                } else {
                    $res = exec("lp -h ".$ip." -d okipos $factura");
                }
            }

            if(isset($options['facturat0'])) {
                $facturat0 = PATH_FACTURAS."/".$options['facturat0'].".txt";

                if(!file_exists($facturat0)) {
                    echo "No existe el archivo de factura tasa 0 proporcionado.";
                    exit;
                } else {
                    $res = exec("lp -h ".$ip." -d okipos $facturat0");
                }
            }

        }
    }
}