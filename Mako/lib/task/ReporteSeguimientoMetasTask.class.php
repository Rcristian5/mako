<?php
class ReporteSeguimientoMestasTask extends sfPropelBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección' , 'propel'),
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'               ,  'tolu2' ),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación', 'frontend')
        ));

        $this->namespace           = 'mako';
        $this->name                = 'reporte-seguimiento-metas';
        $this->briefDescription    = 'Genera el reporte de indicadores para la fase 3';
        $this->detailedDescription = <<<EOF
     [mako:add-cursos-moodlle|INFO] Genera el reporte de indicadores para la fase 3

      [./symfony mako:ejecuta-sync --env=prod |INFO]
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'])->getConnection();

        $this->logSection('Generando reportes', '...');

        // Repore indicadores por dia
        $this->reporteIndicadoresDia();

        // Reporte indicadores por dia y grupo
        $this->reporteIndicadoresDiaGrupo();
    }

    public function reporteIndicadoresDia() {
        $this->log('-------------');
        $this->logSection('Reporte Indicadores por Dia', '...');
        $this->logSection('Buscando el ultimo día que se guardo el reporte', '...');

        $centroId = sfConfig::get('app_centro_actual_id');

        // Buscamos la ultima fecha a partir de la cual se generara el reporte
        $criteria = new Criteria();
        $criteria->add(ReporteIndicadoresDiaPeer::CENTRO_ID, $centroId);
        $criteria->addDescendingOrderByColumn(ReporteIndicadoresDiaPeer::FECHA);

        $ultimaFecha = ReporteIndicadoresDiaPeer::doSelectOne($criteria);

        if ($ultimaFecha) {
            // Si hay registros en la tabla obtenemos la ultima fecha de registro
            $desde = array(
                'j' => $ultimaFecha->getFecha('j'),
                'n' => $ultimaFecha->getFecha('n'),
                'Y' => $ultimaFecha->getFecha('Y')
            );
        } else {
            // Si no hay registros en la tabla tomamos como primer fecha la primer fecha de en la que se efectuo un registro de socio
            $this->logSection('Obteniendo primer reporte', 'espere, esta accion puede tardar varios minutos');

            $criteria = new Criteria();
            $criteria->add(SocioPeer::CENTRO_ID, $centroId);
            $criteria->addAscendingOrderByColumn(SocioPeer::CREATED_AT);

            $primerFecha = SocioPeer::doSelectOne($criteria);

            if (!$primerFecha) {
                error_log('No hay datos de socios en este centro');
                break;
            }

            // Generamos la primer fecha
            $primerFecha = mktime(
                0,
                0,
                0,
                $primerFecha->getCreatedAt('n'),
                $primerFecha->getCreatedAt('j') - 1,
                $primerFecha->getCreatedAt('Y')
            );
            $desde       = array(
                'j' => date('j', $primerFecha),
                'n' => date('n', $primerFecha),
                'Y' => date('Y', $primerFecha)
            );
        }

        // Desde la fecha inicial hasta el la fecha actual generamos reportes diarios
        $hoy   = date('Ymd');
        $fecha = mktime(0, 0, 0, $desde['n'], $desde['j'], $desde['Y']);

        for ($i = 1; date('Ymd', $fecha) < $hoy; $i++) {
            // Sumamos 1 dia
            $fecha = mktime(0, 0, 0, $desde['n'], $desde['j'] + $i, $desde['Y']);

            /* Calculamos la candidad de registros nuevos para el dia actual */
            ////error_log('Registros nuevos');
            $criteriaNuevos = new Criteria();
            $criteriaNuevos->add(SocioPeer::CENTRO_ID, $centroId);
            $criteriaNuevos->add(SocioPeer::ACTIVO, true);
            $criteriaNuevos->add(SocioPeer::CREATED_AT,
                                 SocioPeer::CREATED_AT .
                                    " BETWEEN '"       . date('Y-m-d', $fecha) .
                                    " 00:00:00' AND '" . date('Y-m-d', $fecha) .
                                    " 23:59:59'",
                                 Criteria::CUSTOM);
            $registrosNuevos   = SocioPeer::doCount($criteriaNuevos);

            /* Calculamos la cantidad de socios registrados */
            //error_log('Socios registrados');
            // Contamos los registros creados de socios antes de la fecha
            $criteriaRegistros = new Criteria();
            $criteriaRegistros->add(SocioPeer::ACTIVO, true);
            $criteriaRegistros->add(SocioPeer::CENTRO_ID, $centroId);
            $criteriaRegistros->add(SocioPeer::CREATED_AT, date('Y-m-d', $fecha) . ' 00:00:00', Criteria::LESS_THAN);

            $registrosGuardados = SocioPeer::doCount($criteriaRegistros);

            // Contamos los registros a a la fecha actual
            $criteriaRegistrosDia = new Criteria();
            $criteriaRegistrosDia->add(SocioPeer::ACTIVO, true);
            $criteriaRegistrosDia->add(SocioPeer::CENTRO_ID, $centroId);
            $criteriaRegistrosDia->add(SocioPeer::CREATED_AT, date('Y-m-d', $fecha) . ' 23:59:59', Criteria::LESS_EQUAL);

            $sociosRegistrados = SocioPeer::doCount($criteriaRegistrosDia) - $registrosGuardados;

            /* Obtenmos los socios activos */
            //error_log('Socios Activos');
            // Buscamos todos los socios que adquirieron algun producto
            $sociosActivos   = array();
            $criteriaActivos = new Criteria();
            $criteriaActivos->add(DetalleOrdenPeer::CENTRO_ID, $centroId);
            $criteriaActivos->add(DetalleOrdenPeer::CREATED_AT,
                                  DetalleOrdenPeer::CREATED_AT .
                                        " BETWEEN '"       . date('Y-m-d', $fecha) .
                                        " 00:00:00' AND '" . date('Y-m-d', $fecha) .
                                        " 23:59:59'",
                                  Criteria::CUSTOM);

            foreach (DetalleOrdenPeer::doSelect($criteriaActivos) as $socio) {
                if (!array_key_exists($socio->getSocioId(), $sociosActivos)) {
                    $infoSocio                          = SocioPeer::retrieveByPK($socio->getSocioId());
                    $sociosActivos[$infoSocio->getId()] = array(
                        'id'        => $infoSocio->getId(),
                        'usuario'   => $infoSocio->getUsuario(),
                        'nombre'    => $infoSocio->getNombreCompleto(),
                        'edad'      => $infoSocio->getEdad(),
                        'ocupacion' => $infoSocio->getOcupacionId(),
                        'sexo'      => $infoSocio->getSexo()
                    );
                }
            }

            // Buscamos todos los socios que asistieron a alguna clase
            // Buscamos primero las clases programadas para la fecha actual
            $criteriaA = new Criteria();
            $criteriaA->add(FechasHorarioPeer::FECHA,
                            FechasHorarioPeer::FECHA .
                                " BETWEEN '"       . date('Y-m-d', $fecha) .
                                " 00:00:00' AND '" . date('Y-m-d', $fecha) .
                                " 23:59:59'",
                            Criteria::CUSTOM);
            $criteriaA->addJoin(FechasHorarioPeer::HORARIO_ID, HorarioPeer::ID);
            $criteriaA->addJoin(HorarioPeer::ID, GrupoPeer::HORARIO_ID);
            $criteriaA->add(GrupoPeer::CENTRO_ID, $centroId);
            $criteriaA->add(GrupoPeer::ACTIVO, true);

            foreach (FechasHorarioPeer::doSelect($criteriaA) as $fechaHorario) {
                // Buscamos el grupo al que pertece la fecha
                $criteriaGrupo = new Criteria();
                $criteriaGrupo->add(GrupoPeer::HORARIO_ID, $fechaHorario->getHorarioId());

                $grupo = GrupoPeer::doSelectOne($criteriaGrupo);

                foreach ($grupo->getGrupoAlumnoss() as $alumno) {
                    $criteriaAs = new Criteria();
                    $criteriaAs->add(SesionHistoricoPeer::CENTRO_ID, $centroId);
                    $criteriaAs->add(SesionHistoricoPeer::SOCIO_ID, $alumno->getAlumnoId());

                    $desdeF = $fechaHorario->getFecha('Y-m-d') . ' ' . $fechaHorario->getHoraInicio('H:i:s');
                    $hastaF = $fechaHorario->getFecha('Y-m-d') . ' ' . $fechaHorario->getHoraFin('H:i:s');

                    $criteriaAs->add(SesionHistoricoPeer::FECHA_LOGIN,
                                     SesionHistoricoPeer::FECHA_LOGIN .
                                        " BETWEEN '" . $desdeF .
                                        "' AND '"    . $hastaF .
                                        "'",
                                     Criteria::CUSTOM);

                    if (SesionHistoricoPeer::doCount($criteriaAs) > 0) {
                        if (!array_key_exists($alumno->getAlumnoId(), $sociosActivos)) {
                            $socio                          = SocioPeer::retrieveByPK($alumno->getAlumnoId());
                            $sociosActivos[$socio->getId()] = array(
                                'id'        => $socio->getId(),
                                'usuario'   => $socio->getUsuario(),
                                'nombre'    => $socio->getNombreCompleto(),
                                'edad'      => $socio->getEdad(),
                                'ocupacion' => $socio->getOcupacionId(),
                                'sexo'      => $socio->getSexo()
                            );
                        }
                    }
                }
            }

            // Contamos el total de socios activos en el dia
            $totalSociosActivos    = count($sociosActivos);

            /* Obtenmos los socios activos nuevos */
            //error_log('Socios Activos Nuevos');
            // Buscamos todos los socios que adquirieron algun producto
            $sociosActivosNuevos   = array();
            $criteriaActivosNuevos = new Criteria();
            $criteriaActivosNuevos->add(DetalleOrdenPeer::CENTRO_ID, $centroId);
            $criteriaActivosNuevos->add(DetalleOrdenPeer::CREATED_AT,
                                        DetalleOrdenPeer::CREATED_AT .
                                            " BETWEEN '"       . date('Y-m-d', $fecha) .
                                            " 00:00:00' AND '" . date('Y-m-d', $fecha) .
                                            " 23:59:59'",
                                        Criteria::CUSTOM);
            $criteriaActivosNuevos->add(SocioPeer::CREATED_AT,
                                        SocioPeer::CREATED_AT .
                                            " BETWEEN '"       . date('Y-m-d', $fecha) .
                                            " 00:00:00' AND '" . date('Y-m-d', $fecha) .
                                            " 23:59:59'",
                                        Criteria::CUSTOM);
            $criteriaActivosNuevos->addJoin(DetalleOrdenPeer::SOCIO_ID, SocioPeer::ID);

            foreach (DetalleOrdenPeer::doSelect($criteriaActivosNuevos) as $socio) {
                if (!array_key_exists($socio->getSocioId(), $sociosActivosNuevos)) {
                    $infoSocio = SocioPeer::retrieveByPK($socio->getSocioId());
                    $sociosActivosNuevos[$infoSocio->getId()] = array(
                        'id'        => $infoSocio->getId(),
                        'usuario'   => $infoSocio->getUsuario(),
                        'nombre'    => $infoSocio->getNombreCompleto(),
                        'edad'      => $infoSocio->getEdad(),
                        'ocupacion' => $infoSocio->getOcupacionId(),
                        'sexo'      => $infoSocio->getSexo()
                    );
                }
            }

            // Buscamos todos los socios que asistieron a alguna clase
            // Buscamos primero las clases programadas para la fecha actual
            $criteriaA = new Criteria();
            $criteriaA->add(FechasHorarioPeer::FECHA,
                            FechasHorarioPeer::FECHA .
                                " BETWEEN '"       . date('Y-m-d', $fecha) .
                                " 00:00:00' AND '" . date('Y-m-d', $fecha) .
                                " 23:59:59'",
                            Criteria::CUSTOM);
            $criteriaA->addJoin(FechasHorarioPeer::HORARIO_ID, HorarioPeer::ID);
            $criteriaA->addJoin(HorarioPeer::ID, GrupoPeer::HORARIO_ID);
            $criteriaA->add(GrupoPeer::CENTRO_ID, $centroId);
            $criteriaA->add(GrupoPeer::ACTIVO, true);
            $criteriaA->add(SocioPeer::CREATED_AT,
                            SocioPeer::CREATED_AT .
                                " BETWEEN '"       . date('Y-m-d', $fecha) .
                                " 00:00:00' AND '" . date('Y-m-d', $fecha) .
                                " 23:59:59'",
                            Criteria::CUSTOM);
            $criteriaA->addJoin(GrupoPeer::ID, GrupoAlumnosPeer::GRUPO_ID);
            $criteriaA->addJoin(GrupoAlumnosPeer::ALUMNO_ID, SocioPeer::ID);

            foreach (FechasHorarioPeer::doSelect($criteriaA) as $fechaHorario) {
                // Buscamos el grupo al que pertece la fecha
                $criteriaGrupo = new Criteria();
                $criteriaGrupo->add(GrupoPeer::HORARIO_ID, $fechaHorario->getHorarioId());

                $grupo = GrupoPeer::doSelectOne($criteriaGrupo);

                foreach ($grupo->getGrupoAlumnoss() as $alumno) {
                    $criteriaAs = new Criteria();
                    $criteriaAs->add(SesionHistoricoPeer::CENTRO_ID, $centroId);
                    $criteriaAs->add(SesionHistoricoPeer::SOCIO_ID, $alumno->getAlumnoId());

                    $desdeF = $fechaHorario->getFecha('Y-m-d') . ' ' . $fechaHorario->getHoraInicio('H:i:s');
                    $hastaF = $fechaHorario->getFecha('Y-m-d') . ' ' . $fechaHorario->getHoraFin('H:i:s');

                    $criteriaAs->add(SesionHistoricoPeer::FECHA_LOGIN,
                                     SesionHistoricoPeer::FECHA_LOGIN .
                                         " BETWEEN '" . $desdeF .
                                         "' AND '"    . $hastaF .
                                         "'",
                                     Criteria::CUSTOM);
                    if (SesionHistoricoPeer::doCount($criteriaAs) > 0) {
                        if (!array_key_exists($alumno->getAlumnoId(), $sociosActivosNuevos)) {
                            $socio                                = SocioPeer::retrieveByPK($alumno->getAlumnoId());
                            $sociosActivosNuevos[$socio->getId()] = array(
                                'id'        => $socio->getId(),
                                'usuario'   => $socio->getUsuario(),
                                'nombre'    => $socio->getNombreCompleto(),
                                'edad'      => $socio->getEdad(),
                                'ocupacion' => $socio->getOcupacionId(),
                                'sexo'      => $socio->getSexo()
                            );
                        }
                    }
                }
            }

            // Contamos el total de socios activos en el dia
            $totalSociosActivosNuevos = count($sociosActivosNuevos);

            /* Obtenemos la cobranza de internet */
            //error_log('Cobranza Internet');
            $criteriaInternet = new Criteria();
            $criteriaInternet->clearSelectColumns();
            $criteriaInternet->addSelectColumn('SUM(' . DetalleOrdenPeer::SUBTOTAL . ')');
            $criteriaInternet->add(DetalleOrdenPeer::CENTRO_ID, $centroId);
            $criteriaInternet->add(ProductoPeer::CODIGO, 'internet%', Criteria::LIKE);
            $criteriaInternet->add(DetalleOrdenPeer::CREATED_AT,
                                   DetalleOrdenPeer::CREATED_AT .
                                        " BETWEEN '"       . date('Y-m-d', $fecha) .
                                        " 00:00:00' AND '" . date('Y-m-d', $fecha) .
                                        " 23:59:59'",
                                   Criteria::CUSTOM);
            $criteriaInternet->addJoin(DetalleOrdenPeer::PRODUCTO_ID, ProductoPeer::ID);

            $cobranzaInternet = DetalleOrdenPeer::doSelectOne($criteriaInternet);
            $cobranzaInternet = $cobranzaInternet->getId() ? $cobranzaInternet->getId() : '0.00';

            /* Obtenemos la cobranza de productos que no son internet ni cursos */
            // Obtenmos los id de los productos relacionados con los cursos
            $sql  = "SELECT %s FROM %s, %s WHERE %s LIKE %s || ";
            $sql  = sprintf($sql,
                            ProductoPeer::ID,
                            ProductoPeer::TABLE_NAME,
                            CursoPeer::TABLE_NAME,
                            CursoPeer::CLAVE,
                            ProductoPeer::CODIGO);
            $sql .= "'%'";

            $connection = Propel::getConnection(ProductoPeer::DATABASE_NAME, Propel::CONNECTION_READ);
            $statement  = $connection->prepare($sql);

            $statement->execute();

            $productosDescartar = array();

            while ($producto = $statement->fetch(PDO::FETCH_ASSOC)) {
                $productosDescartar[] = $producto['id'];
            }

            // Obtenemos los productos relacionados con internet
            $criteriaProductosInternet = new Criteria();
            $criteriaProductosInternet->add(ProductoPeer::CODIGO, 'internet%', Criteria::LIKE);

            foreach (ProductoPeer::doSelect($criteriaProductosInternet) as $producto) {
                $productosDescartar[]               = $producto->getId();
            }
            // Obtenemos la cobranza
            $criteriaOtros = new Criteria();
            $criteriaOtros->clearSelectColumns();
            $criteriaOtros->addSelectColumn('SUM(' . DetalleOrdenPeer::SUBTOTAL . ')');
            $criteriaOtros->add(ProductoPeer::ID, $productosDescartar, Criteria::NOT_IN);
            $criteriaOtros->add(DetalleOrdenPeer::CENTRO_ID, $centroId);
            $criteriaOtros->add(DetalleOrdenPeer::CREATED_AT,
                                DetalleOrdenPeer::CREATED_AT .
                                    " BETWEEN '"       . date('Y-m-d', $fecha) .
                                    " 00:00:00' AND '" . date('Y-m-d', $fecha) .
                                    " 23:59:59'",
                                Criteria::CUSTOM);
            $criteriaOtros->addJoin(DetalleOrdenPeer::PRODUCTO_ID, ProductoPeer::ID);

            $cobranzaOtros = DetalleOrdenPeer::doSelectOne($criteriaOtros);
            $cobranzaOtros = $cobranzaOtros->getId() ? $cobranzaOtros->getId() : '0.00';

            /* Obtenemos la cobranza total del dia */
            //error_log('Cobranza total del dia');
            $criteriaTotal = new Criteria();
            $criteriaTotal->clearSelectColumns();
            $criteriaTotal->addSelectColumn('SUM(' . DetalleOrdenPeer::SUBTOTAL . ')');
            $criteriaTotal->add(DetalleOrdenPeer::CENTRO_ID, $centroId);
            $criteriaTotal->add(DetalleOrdenPeer::CREATED_AT,
                                DetalleOrdenPeer::CREATED_AT .
                                    " BETWEEN '"       . date('Y-m-d', $fecha) .
                                    " 00:00:00' AND '" . date('Y-m-d', $fecha) .
                                    " 23:59:59'",
                                Criteria::CUSTOM);

            $cobranzaTotal = DetalleOrdenPeer::doSelectOne($criteriaTotal);
            $cobranzaTotal = $cobranzaTotal->getId() ? $cobranzaTotal->getId() : '0.00';

            /* Guardamos las cantidades*/
            //error_log('Guardando registros');
            $reporteIndicadoresDia = new ReporteIndicadoresDia();
            $reporteIndicadoresDia->setCentroId($centroId);
            $reporteIndicadoresDia->setFecha(date('Y-m-d', $fecha));
            $reporteIndicadoresDia->setSemana(date('W', $fecha));
            $reporteIndicadoresDia->setAnio(date('Y', $fecha));
            $reporteIndicadoresDia->setRegistrosNuevos($registrosNuevos);
            $reporteIndicadoresDia->setSociosRegistrados($sociosRegistrados);
            $reporteIndicadoresDia->setSociosActivos($totalSociosActivos);
            $reporteIndicadoresDia->setSociosActivosNuevos($totalSociosActivosNuevos);
            $reporteIndicadoresDia->setCobranzaOtros($cobranzaOtros);
            $reporteIndicadoresDia->setCobranzaInternet($cobranzaInternet);
            $reporteIndicadoresDia->setCobranzaTotal($cobranzaTotal);
            $reporteIndicadoresDia->save();

            foreach ($sociosActivos as $socio) {
                $reporteIndicadoresSocios = new ReporteIndicadoresDiaSociosActivos();
                $reporteIndicadoresSocios->setReporteIndicadorDiaId($reporteIndicadoresDia->getId());
                $reporteIndicadoresSocios->setCentroId($reporteIndicadoresDia->getCentroId());
                $reporteIndicadoresSocios->setSocioId($socio['id']);
                $reporteIndicadoresSocios->setUsuario($socio['usuario']);
                $reporteIndicadoresSocios->setNombreCompleto($socio['nombre']);
                $reporteIndicadoresSocios->setEdad($socio['edad']);
                $reporteIndicadoresSocios->setOcupacionId($socio['ocupacion']);
                $reporteIndicadoresSocios->setSexo($socio['sexo']);
                $reporteIndicadoresSocios->save();
            }

            foreach ($sociosActivosNuevos as $socio) {
                $reporteIndicadoresSociosN = new ReporteIndicadoresDiaSociosActivosNuevos();
                $reporteIndicadoresSociosN->setId(Comun::generaId());
                $reporteIndicadoresSociosN->setReporteIndicadorDiaId($reporteIndicadoresDia->getId());
                $reporteIndicadoresSociosN->setCentroId($reporteIndicadoresDia->getCentroId());
                $reporteIndicadoresSociosN->setSocioId($socio['id']);
                $reporteIndicadoresSociosN->setUsuario($socio['usuario']);
                $reporteIndicadoresSociosN->setNombreCompleto($socio['nombre']);
                $reporteIndicadoresSociosN->setEdad($socio['edad']);
                $reporteIndicadoresSociosN->setOcupacionId($socio['ocupacion']);
                $reporteIndicadoresSociosN->setSexo($socio['sexo']);
                $reporteIndicadoresSociosN->save();
            }
        }

        $this->logSection('Se guardo correctamente el reporte por dia', '');
    }

    public function reporteIndicadoresDiaGrupo() {
        $this->log('-------------');
        $this->logSection('Reporte Indicadores por Dia y Grupo', '...');
        $this->logSection('Buscando el ultimo día que se guardo el reporte', '...');

        $centroId = sfConfig::get('app_centro_actual_id');

        // Buscamos la ultima fecha a partir de la cual se generara el reporte
        $criteria = new Criteria();
        $criteria->add(ReporteIndicadoresDiaGrupoPeer::CENTRO_ID, $centroId);
        $criteria->addDescendingOrderByColumn(ReporteIndicadoresDiaGrupoPeer::FECHA);

        $ultimaFecha = ReporteIndicadoresDiaGrupoPeer::doSelectOne($criteria);
        if ($ultimaFecha) {
            // Si hay registros en la tabla obtenemos la ultima fecha de registro
            $desde = array(
                'j' => $ultimaFecha->getFecha('j'),
                'n' => $ultimaFecha->getFecha('n'),
                'Y' => $ultimaFecha->getFecha('Y')
            );
        } else {
            // Si no hay registros en la tabla tomamos como primer fecha la primer fecha de en la que se efectuo un registro de socio
            $this->logSection('Obteniendo primer reporte', 'espere, esta accion puede tardar varios minutos');

            $criteria = new Criteria();
            $criteria->add(SocioPeer::CENTRO_ID, $centroId);
            $criteria->addAscendingOrderByColumn(SocioPeer::CREATED_AT);

            $primerFecha = SocioPeer::doSelectOne($criteria);
            if (!$primerFecha) {
                error_log('No hay datos de socios en este centro');
                break;
            }

            // Generamos la primer fecha
            $primerFecha = mktime(
                0,
                0,
                0,
                $primerFecha->getCreatedAt('n'),
                $primerFecha->getCreatedAt('j') - 1,
                $primerFecha->getCreatedAt('Y')
            );
            $desde = array(
                'j' => date('j', $primerFecha),
                'n' => date('n', $primerFecha),
                'Y' => date('Y', $primerFecha)
            );
        }

        // Desde la fecha inicial hasta el la fecha actual generamos reportes diarios
        $hoy    = date('Ymd');
        $fecha = mktime(0, 0, 0, $desde['n'], $desde['j'], $desde['Y']);

        for ($i = 1; date('Ymd', $fecha) < $hoy; $i++) {
            // Sumamos 1 dia
            $fecha           = mktime(0, 0, 0, $desde['n'], $desde['j'] + $i, $desde['Y']);
            $resultadosGrupo = array();

            //error_log('Preinscritos');
            /* Total de socios preinscritos */
            $criteriaPreinscritos = new Criteria();
            $criteriaPreinscritos->add(GrupoAlumnosPeer::FECHA_PREINSCRITO,
                                       GrupoAlumnosPeer::FECHA_PREINSCRITO .
                                            " BETWEEN '"       . date('Y-m-d', $fecha) .
                                            " 00:00:00' AND '" . date('Y-m-d', $fecha) .
                                            " 23:59:59'",
                                       Criteria::CUSTOM);
            $criteriaPreinscritos->add(GrupoAlumnosPeer::PREINSCRITO, true);
            $criteriaPreinscritos->addJoin(GrupoAlumnosPeer::GRUPO_ID, GrupoPeer::ID);
            $criteriaPreinscritos->add(GrupoPeer::ACTIVO, true);
            $criteriaPreinscritos->add(GrupoPeer::CENTRO_ID, $centroId);

            foreach (GrupoAlumnosPeer::doSelect($criteriaPreinscritos) as $preinscritos) {
                if (!array_key_exists($preinscritos->getGrupoId(), $resultadosGrupo)) {
                    //error_log('almacenando los preincsrtitos');
                    $resultadosGrupo[$preinscritos->getGrupoId()]['centro']       = $centroId;
                    $resultadosGrupo[$preinscritos->getGrupoId()]['grupo']        = $preinscritos->getGrupoId();
                    $resultadosGrupo[$preinscritos->getGrupoId()]['curso']        = $preinscritos->getGrupo()->getCursoId();
                    $resultadosGrupo[$preinscritos->getGrupoId()]['facilitador']  = $preinscritos->getGrupo()->getFacilitadorId();
                    $resultadosGrupo[$preinscritos->getGrupoId()]['categoria']    = $preinscritos->getGrupo()->getCurso()->getCategoriaCursoId();
                    $resultadosGrupo[$preinscritos->getGrupoId()]['aula']         = $preinscritos->getGrupo()->getAulaId();
                    $resultadosGrupo[$preinscritos->getGrupoId()]['preinscritos'] = 1;
                } else {
                    $resultadosGrupo[$preinscritos->getGrupoId()]['preinscritos']+= 1;
                }
            }

            /* Total de inscritos */
            //error_log('inscritos');
            $arregloInscritos  = array();
            $criteriaInscritos = new Criteria();
            $criteriaInscritos->add(GrupoAlumnosPeer::UPDATED_AT,
                                    GrupoAlumnosPeer::UPDATED_AT .
                                        " BETWEEN '"       . date('Y-m-d', $fecha) .
                                        " 00:00:00' AND '" . date('Y-m-d', $fecha) .
                                        " 23:59:59'",
                                    Criteria::CUSTOM);
            $criteriaInscritos->add(GrupoAlumnosPeer::PREINSCRITO, false);
            $criteriaInscritos->addJoin(GrupoAlumnosPeer::GRUPO_ID, GrupoPeer::ID);
            $criteriaInscritos->add(GrupoPeer::ACTIVO, true);
            $criteriaInscritos->add(GrupoPeer::CENTRO_ID, $centroId);

            foreach (GrupoAlumnosPeer::doSelect($criteriaInscritos) as $inscritos) {
                if (!array_key_exists($inscritos->getGrupoId(), $resultadosGrupo)) {
                    //error_log('almacenado inscritos');
                    $resultadosGrupo[$inscritos->getGrupoId()]['centro']      = $centroId;
                    $resultadosGrupo[$inscritos->getGrupoId()]['grupo']       = $inscritos->getGrupoId();
                    $resultadosGrupo[$inscritos->getGrupoId()]['curso']       = $inscritos->getGrupo()->getCursoId();
                    $resultadosGrupo[$inscritos->getGrupoId()]['facilitador'] = $inscritos->getGrupo()->getFacilitadorId();
                    $resultadosGrupo[$inscritos->getGrupoId()]['categoria']   = $inscritos->getGrupo()->getCurso()->getCategoriaCursoId();
                    $resultadosGrupo[$inscritos->getGrupoId()]['aula']        = $inscritos->getGrupo()->getAulaId();
                    $resultadosGrupo[$inscritos->getGrupoId()]['inscritos']   = 1;
                } else {
                    $resultadosGrupo[$inscritos->getGrupoId()]['inscritos']+= 1;
                }

                $socio                                        = SocioPeer::retrieveByPK($inscritos->getAlumnoId());
                $arregloInscritos[$inscritos->getGrupoId()][] = array(
                    'id'        => $socio->getId(),
                    'usuario'   => $socio->getUsuario(),
                    'nombre'    => $socio->getNombreCompleto(),
                    'edad'      => $socio->getEdad(),
                    'ocupacion' => $socio->getOcupacionId(),
                    'sexo'      => $socio->getSexo()
                );
            }

            /* Total de inscritos nuevos */
            //error_log('iscritos nuevos');
            $sociosInscritos         = array();
            $criteriaInscritosNuevos = new Criteria();
            $criteriaInscritosNuevos->add(GrupoAlumnosPeer::UPDATED_AT,
                                          GrupoAlumnosPeer::UPDATED_AT .
                                            " BETWEEN '"       . date('Y-m-d', $fecha) .
                                            " 00:00:00' AND '" . date('Y-m-d', $fecha) .
                                            " 23:59:59'",
                                          Criteria::CUSTOM);
            $criteriaInscritosNuevos->add(GrupoAlumnosPeer::PREINSCRITO, false);
            $criteriaInscritosNuevos->addJoin(GrupoAlumnosPeer::GRUPO_ID, GrupoPeer::ID);
            $criteriaInscritosNuevos->add(GrupoPeer::ACTIVO, true);
            $criteriaInscritosNuevos->add(GrupoPeer::CENTRO_ID, $centroId);
            $criteriaInscritosNuevos->addJoin(GrupoAlumnosPeer::ALUMNO_ID, SocioPeer::ID);
            $criteriaInscritosNuevos->add(SocioPeer::CREATED_AT,
                                          SocioPeer::CREATED_AT .
                                            " BETWEEN '"       . date('Y-m-d', $fecha) .
                                            " 00:00:00' AND '" . date('Y-m-d', $fecha) .
                                            " 23:59:59'",
                                          Criteria::CUSTOM);
            $criteriaInscritosNuevos->add(SocioPeer::CENTRO_ID, $centroId);
            $criteriaInscritosNuevos->add(SocioPeer::ACTIVO, true);

            foreach (GrupoAlumnosPeer::doSelect($criteriaInscritosNuevos) as $inscritosNuevos) {
                if (!array_key_exists($inscritosNuevos->getGrupoId(), $resultadosGrupo)) {
                    //error_log('almacenado nuevos');
                    $resultadosGrupo[$inscritosNuevos->getGrupoId()]['centro']          = $centroId;
                    $resultadosGrupo[$inscritosNuevos->getGrupoId()]['grupo']           = $inscritosNuevos->getGrupoId();
                    $resultadosGrupo[$inscritosNuevos->getGrupoId()]['curso']           = $inscritosNuevos->getGrupo()->getCursoId();
                    $resultadosGrupo[$inscritosNuevos->getGrupoId()]['facilitador']     = $inscritosNuevos->getGrupo()->getFacilitadorId();
                    $resultadosGrupo[$inscritosNuevos->getGrupoId()]['categoria']       = $inscritosNuevos->getGrupo()->getCurso()->getCategoriaCursoId();
                    $resultadosGrupo[$inscritosNuevos->getGrupoId()]['aula']            = $inscritosNuevos->getGrupo()->getAulaId();
                    $resultadosGrupo[$inscritosNuevos->getGrupoId()]['inscritosNuevos'] = 1;
                } else {
                    $resultadosGrupo[$inscritosNuevos->getGrupoId()]['inscritosNuevos']+= 1;
                }

                if (!array_key_exists($inscritosNuevos->getAlumnoId(), $sociosInscritos)) {
                    $sociosInscritos[$inscritosNuevos->getAlumnoId()] = $inscritosNuevos->getAlumnoId();
                }
            }

            /* Total de inscritos recurrentes */
            //error_log('insctritos recurrectes');
            $criteriaInscritosRecurrente = new Criteria();
            $criteriaInscritosRecurrente->add(GrupoAlumnosPeer::UPDATED_AT,
                                              GrupoAlumnosPeer::UPDATED_AT .
                                                " BETWEEN '"       . date('Y-m-d', $fecha) .
                                                " 00:00:00' AND '" . date('Y-m-d', $fecha) .
                                                " 23:59:59'",
                                              Criteria::CUSTOM);
            $criteriaInscritosRecurrente->add(GrupoAlumnosPeer::PREINSCRITO, false);
            $criteriaInscritosRecurrente->addJoin(GrupoAlumnosPeer::GRUPO_ID, GrupoPeer::ID);
            $criteriaInscritosRecurrente->add(GrupoPeer::ACTIVO, true);
            $criteriaInscritosRecurrente->add(GrupoPeer::CENTRO_ID, $centroId);
            $criteriaInscritosRecurrente->addJoin(GrupoAlumnosPeer::ALUMNO_ID, SocioPeer::ID);
            $criteriaInscritosRecurrente->add(SocioPeer::CREATED_AT, date('Y-m-d', $fecha), Criteria::LESS_THAN);
            $criteriaInscritosRecurrente->add(SocioPeer::CENTRO_ID, $centroId);
            $criteriaInscritosRecurrente->add(SocioPeer::ACTIVO, true);

            foreach (GrupoAlumnosPeer::doSelect($criteriaInscritosRecurrente) as $inscritosRecurrentes) {
                // Buscamos si esl socio ya termino al menos un curso
                $criteriaFinalizados = new Criteria();
                $criteriaFinalizados->add(CursosFinalizadosPeer::ALUMNO_ID, $inscritosRecurrentes->getAlumnoId());
                if (CursosFinalizadosPeer::doCount($criteriaFinalizados) > 0) {
                    if (!array_key_exists($inscritosRecurrentes->getGrupoId(), $resultadosGrupo)) {
                        //error_log('almacenado recurrentes');
                        $resultadosGrupo[$inscritosRecurrentes->getGrupoId()]['centro']               = $centroId;
                        $resultadosGrupo[$inscritosRecurrentes->getGrupoId()]['grupo']                = $inscritosRecurrentes->getGrupoId();
                        $resultadosGrupo[$inscritosRecurrentes->getGrupoId()]['curso']                = $inscritosRecurrentes->getGrupo()->getCursoId();
                        $resultadosGrupo[$inscritosRecurrentes->getGrupoId()]['facilitador']          = $inscritosRecurrentes->getGrupo()->getFacilitadorId();
                        $resultadosGrupo[$inscritosRecurrentes->getGrupoId()]['categoria']            = $inscritosRecurrentes->getGrupo()->getCurso()->getCategoriaCursoId();
                        $resultadosGrupo[$inscritosRecurrentes->getGrupoId()]['aula']                 = $inscritosRecurrentes->getGrupo()->getAulaId();
                        $resultadosGrupo[$inscritosRecurrentes->getGrupoId()]['inscritosRecurrentes'] = 1;
                    } else {
                        $resultadosGrupo[$inscritosRecurrentes->getGrupoId()]['inscritosRecurrentes']+= 1;
                    }
                }
            }

            /* Alumnos activos */
            //error_log('alumnos activos');
            $alumnosActivos  = array();

            // Buscamos todas las actividades del dia
            $criteriaActivos = new Criteria();
            $criteriaActivos->add(FechasHorarioPeer::FECHA, date('Y-m-d', $fecha));
            $criteriaActivos->addJoin(FechasHorarioPeer::HORARIO_ID, HorarioPeer::ID);
            $criteriaActivos->addJoin(HorarioPeer::ID, GrupoPeer::HORARIO_ID);
            $criteriaActivos->add(GrupoPeer::ACTIVO, true);
            $criteriaActivos->addJoin(GrupoPeer::CENTRO_ID, $centroId);
            foreach (FechasHorarioPeer::doSelect($criteriaActivos) as $fechaHorario) {
                $criteriaGrupo = new Criteria();
                $criteriaGrupo->add(GrupoPeer::HORARIO_ID, $fechaHorario->getHorarioId());

                $grupo = GrupoPeer::doSelectOne($criteriaGrupo);

                // Por cada socio inscrito al grupo vemos si tuvieron alguna actividad dentro de la fecha y hora correspondiente
                if ($grupo) {
                    foreach ($grupo->getGrupoAlumnoss() as $grupoAlumno) {
                        if (!$grupoAlumno->getPreinscrito()) {
                            $criteriaActividad = new Criteria();
                            $criteriaActividad->add(SesionHistoricoPeer::CENTRO_ID, $centroId);
                            $criteriaActividad->add(SesionHistoricoPeer::SOCIO_ID, $grupoAlumno->getAlumnoId());

                            $desdeF = $fechaHorario->getFecha('Y-m-d') . ' ' . $fechaHorario->getHoraInicio('H:i:s');
                            $hastaF = $fechaHorario->getFecha('Y-m-d') . ' ' . $fechaHorario->getHoraFin('H:i:s');

                            $criteriaActividad->add(SesionHistoricoPeer::FECHA_LOGIN,
                                                    SesionHistoricoPeer::FECHA_LOGIN .
                                                        " BETWEEN '" . $desdeF .
                                                        "' AND '"    . $hastaF .
                                                        "'",
                                                    Criteria::CUSTOM);
                            if (SesionHistoricoPeer::doCount($criteriaActividad) > 0) {
                                if (!array_key_exists($grupo->getId(), $resultadosGrupo)) {
                                    $resultadosGrupo[$grupo->getId()]['centro']           = $centroId;
                                    $resultadosGrupo[$grupo->getId()]['grupo']            = $grupo->getId();
                                    $resultadosGrupo[$grupo->getId()]['curso']            = $grupo->getCursoId();
                                    $resultadosGrupo[$grupo->getId()]['facilitador']      = $grupo->getFacilitadorId();
                                    $resultadosGrupo[$grupo->getId()]['categoria']        = $grupo->getCurso()->getCategoriaCursoId();
                                    $resultadosGrupo[$grupo->getId()]['aula']             = $grupo->getAulaId();
                                    $resultadosGrupo[$grupo->getId()]['alumnosActivos'][] = $grupoAlumno->getAlumnoId();
                                } else {
                                    if (!in_array($grupoAlumno->getAlumnoId(), $resultadosGrupo[$grupo->getId()]['alumnosActivos'])) {
                                        $resultadosGrupo[$grupo->getId()]['alumnosActivos'][] = $grupoAlumno->getAlumnoId();
                                    }
                                }

                                $socio                             = SocioPeer::retrieveByPK($grupoAlumno->getAlumnoId());
                                $alumnosActivos[$grupo->getId()][] = array(
                                    'id'        => $socio->getId(),
                                    'usuario'   => $socio->getUsuario(),
                                    'nombre'    => $socio->getNombreCompleto(),
                                    'edad'      => $socio->getEdad(),
                                    'ocupacion' => $socio->getOcupacionId(),
                                    'sexo'      => $socio->getSexo()
                                );
                            }
                        }
                    }
                }
            }

            /* Alumnos con actividad programada */
            //error_log('alumnos activos');
            $alumnosActividadProgramada = array();

            // Buscamos todas las actividades del dia
            $criteriaActivos = new Criteria();
            $criteriaActivos->add(FechasHorarioPeer::FECHA, date('Y-m-d', $fecha));
            $criteriaActivos->addJoin(FechasHorarioPeer::HORARIO_ID, HorarioPeer::ID);
            $criteriaActivos->addJoin(HorarioPeer::ID, GrupoPeer::HORARIO_ID);
            $criteriaActivos->add(GrupoPeer::ACTIVO, true);
            $criteriaActivos->addJoin(GrupoPeer::CENTRO_ID, $centroId);

            foreach (FechasHorarioPeer::doSelect($criteriaActivos) as $fechaHorario) {
                $criteriaGrupo = new Criteria();
                $criteriaGrupo->add(GrupoPeer::HORARIO_ID, $fechaHorario->getHorarioId());

                $grupo = GrupoPeer::doSelectOne($criteriaGrupo);

                // Por cada socio inscrito al grupo guaramos los datos del alumno que deberia asistir
                if ($grupo) {
                    foreach ($grupo->getGrupoAlumnoss() as $grupoAlumno) {
                        if (!$grupoAlumno->getPreinscrito()) {
                            if (!array_key_exists($grupo->getId(), $resultadosGrupo)) {
                                $resultadosGrupo[$grupo->getId()]['centro']                       = $centroId;
                                $resultadosGrupo[$grupo->getId()]['grupo']                        = $grupo->getId();
                                $resultadosGrupo[$grupo->getId()]['curso']                        = $grupo->getCursoId();
                                $resultadosGrupo[$grupo->getId()]['facilitador']                  = $grupo->getFacilitadorId();
                                $resultadosGrupo[$grupo->getId()]['categoria']                    = $grupo->getCurso()->getCategoriaCursoId();
                                $resultadosGrupo[$grupo->getId()]['aula']                         = $grupo->getAulaId();
                                $resultadosGrupo[$grupo->getId()]['alumnosActividadProgramada'][] = $grupoAlumno->getAlumnoId();
                            } else {
                                if (!in_array($grupoAlumno->getAlumnoId(), $resultadosGrupo[$grupo->getId()]['alumnosActividadProgramada'])) {
                                    $resultadosGrupo[$grupo->getId()]['alumnosActividadProgramada'][] = $grupoAlumno->getAlumnoId();
                                }
                            }
                            $socio                                         = SocioPeer::retrieveByPK($grupoAlumno->getAlumnoId());
                            $alumnosActividadProgramada[$grupo->getId()][] = array(
                                'id'        => $socio->getId(),
                                'usuario'   => $socio->getUsuario(),
                                'nombre'    => $socio->getNombreCompleto(),
                                'edad'      => $socio->getEdad(),
                                'ocupacion' => $socio->getOcupacionId(),
                                'sexo'      => $socio->getSexo()
                            );
                        }
                    }
                }
            }

            /* Alumnos Graduados */
            //error_log('alumno graduados');
            $criteriaGraduados = new Criteria();
            $criteriaGraduados->add(HorarioPeer::FECHA_FIN, date('Y-m-d', $fecha) . ' 00:00:00');
            $criteriaGraduados->addJoin(HorarioPeer::ID, GrupoPeer::HORARIO_ID);
            $criteriaGraduados->add(GrupoPeer::ACTIVO, true);
            $criteriaGraduados->add(GrupoPeer::CENTRO_ID, $centroId);
            $criteriaGraduados->addJoin(GrupoPeer::ID, GrupoAlumnosPeer::GRUPO_ID);

            ////error_log($criteriaGraduados->toString());
            foreach (GrupoAlumnosPeer::doSelect($criteriaGraduados) as $grupoAlumnos) {
                $clases      = 0;
                $asistencias = 0;
                foreach ($grupoAlumnos->getGrupo()->getHorario()->getFechasHorarios() as $fechaHorario) {
                    $criteriaActividad = new Criteria();
                    $criteriaActividad->add(SesionHistoricoPeer::CENTRO_ID, $centroId);
                    $criteriaActividad->add(SesionHistoricoPeer::SOCIO_ID, $grupoAlumno->getAlumnoId());

                    $desdeF = $fechaHorario->getFecha('Y-m-d') . ' ' . $fechaHorario->getHoraInicio('H:i:s');
                    $hastaF = $fechaHorario->getFecha('Y-m-d') . ' ' . $fechaHorario->getHoraFin('H:i:s');

                    $criteriaActividad->add(SesionHistoricoPeer::FECHA_LOGIN,
                                            SesionHistoricoPeer::FECHA_LOGIN .
                                                " BETWEEN '" . $desdeF .
                                                "' AND '"    . $hastaF .
                                                "'",
                                            Criteria::CUSTOM);

                    if (SesionHistoricoPeer::doCount($criteriaActividad) > 0) {
                        $asistencias++;
                    }

                    $clases++;
                }

                $porcentaje = (100 * $asistencias) / $clases;

                if ($porcentaje >= 80) {
                    if (!array_key_exists($grupoAlumnos->getGrupoId(), $resultadosGrupo)) {
                        //error_log('almacenado graduados');
                        $resultadosGrupo[$grupoAlumnos->getGrupoId()]['centro']           = $centroId;
                        $resultadosGrupo[$grupoAlumnos->getGrupoId()]['grupo']            = $grupoAlumnos->getGrupoId();
                        $resultadosGrupo[$grupoAlumnos->getGrupoId()]['curso']            = $grupoAlumnos->getGrupo()->getCursoId();
                        $resultadosGrupo[$grupoAlumnos->getGrupoId()]['facilitador']      = $grupoAlumnos->getGrupo()->getFacilitadorId();
                        $resultadosGrupo[$grupoAlumnos->getGrupoId()]['categoria']        = $grupoAlumnos->getGrupo()->getCurso()->getCategoriaCursoId();
                        $resultadosGrupo[$grupoAlumnos->getGrupoId()]['aula']             = $grupoAlumnos->getGrupo()->getAulaId();
                        $resultadosGrupo[$grupoAlumnos->getGrupoId()]['alumnosGraduados'] = 1;
                    } else {
                        $resultadosGrupo[$grupoAlumnos->getGrupoId()]['alumnosGraduados']+= 1;
                    }
                }
            }

            /* Graduacion esperada */
            $criteriaGraduacionEsperada = new Criteria();
            $criteriaGraduacionEsperada->add(HorarioPeer::FECHA_FIN, date('Y-m-d', $fecha) . ' 00:00:00');
            $criteriaGraduacionEsperada->addJoin(HorarioPeer::ID, GrupoPeer::HORARIO_ID);
            $criteriaGraduacionEsperada->add(GrupoPeer::ACTIVO, true);
            $criteriaGraduacionEsperada->add(GrupoPeer::CENTRO_ID, $centroId);

            foreach (GrupoPeer::doSelect($criteriaGraduacionEsperada) as $grupo) {
                $resultadosGrupo[$grupo->getId()]['centro']             = $centroId;
                $resultadosGrupo[$grupo->getId()]['grupo']              = $grupo->getId();
                $resultadosGrupo[$grupo->getId()]['curso']              = $grupo->getCursoId();
                $resultadosGrupo[$grupo->getId()]['facilitador']        = $grupo->getFacilitadorId();
                $resultadosGrupo[$grupo->getId()]['categoria']          = $grupo->getCurso()->getCategoriaCursoId();
                $resultadosGrupo[$grupo->getId()]['aula']               = $grupo->getAulaId();
                $resultadosGrupo[$grupo->getId()]['graduacionEsperada'] = $grupo->getCupoActual();
            }

            /* Alumnos Desertores */
            //error_log('alumnos desertores');
            $criteriaGraduados = new Criteria();
            $criteriaGraduados->add(HorarioPeer::FECHA_FIN, date('Y-m-d', $fecha) . ' 00:00:00');
            $criteriaGraduados->addJoin(HorarioPeer::ID, GrupoPeer::HORARIO_ID);
            $criteriaGraduados->add(GrupoPeer::ACTIVO, true);
            $criteriaGraduados->add(GrupoPeer::CENTRO_ID, $centroId);
            $criteriaGraduados->addJoin(GrupoPeer::ID, GrupoAlumnosPeer::GRUPO_ID);

            foreach (GrupoAlumnosPeer::doSelect($criteriaGraduados) as $grupoAlumnos) {
                $limite = 0;
                $c3     = new Criteria();
                $c3->add(AlumnoPagosPeer::SOCIO_ID, $grupoAlumnos->getAlumnoId());
                $c3->add(AlumnoPagosPeer::CURSO_ID, $grupoAlumnos->getGrupo()->getCursoId());
                $c3->add(AlumnoPagosPeer::GRUPO_ID, $grupoAlumnos->getGrupoId());

                $alumnoPago = AlumnoPagosPeer::doSelectOne($c3);

                if ($alumnoPago != null) {
                    //Obtenemos el orden_id de este pago
                    $orden           = $alumnoPago->getOrden();
                    $sub_producto_de = $alumnoPago->getSubProductoDe();

                    if ($orden != null) {
                        //Obtenemos los detalle_orden_id relacionados con esta orden
                        $detalleOrdenes  = $orden->getDetalleOrdens();

                        foreach ($detalleOrdenes as $detalleOrden) {
                            foreach ($detalleOrden->getSocioPagoss() as $socioPago) {
                                // Obtenemos los ultimos dos pagos para este grupo
                                $orden_base_id   = $socioPago->getOrdenBaseId();
                                $c4              = new Criteria();
                                $c4->add(SocioPagosPeer::ORDEN_BASE_ID, $orden_base_id);
                                $c4->add(SocioPagosPeer::SUB_PRODUCTO_DE, $sub_producto_de);
                                $c4->add(SocioPagosPeer::CANCELADO, false);
                                $c4->setLimit(2);
                                $c4->addDescendingOrderByColumn(SocioPagosPeer::FECHA_A_PAGAR);

                                foreach (SocioPagosPeer::doSelect($c4) as $ultimosPagos) {
                                    if (!$ultimosPagos->getPagado()) {
                                        $limite++;
                                    }
                                }
                            }
                        }
                    }

                    // Si no cubrio los ultimos 2 pagos buscamos si asistio las ultimas 2 fechas
                    if ($limite == 2) {
                        $assitencias           = 0;
                        $criteriaUltimasFechas = new Criteria();
                        $criteriaUltimasFechas->add(FechasHorarioPeer::HORARIO_ID, $grupoAlumnos->getGrupo()->getHorarioId());
                        $criteriaUltimasFechas->setLimit(2);
                        $criteriaUltimasFechas->addDescendingOrderByColumn(FechasHorarioPeer::FECHA);

                        foreach (FechasHorarioPeer::doSelect($criteriaUltimasFechas) as $fechaHorario) {
                            $criteriaActividad = new Criteria();
                            $criteriaActividad->add(SesionHistoricoPeer::CENTRO_ID, $centroId);
                            $criteriaActividad->add(SesionHistoricoPeer::SOCIO_ID, $grupoAlumno->getAlumnoId());

                            $desdeF = $fechaHorario->getFecha('Y-m-d') . ' ' . $fechaHorario->getHoraInicio('H:i:s');
                            $hastaF = $fechaHorario->getFecha('Y-m-d') . ' ' . $fechaHorario->getHoraFin('H:i:s');

                            $criteriaActividad->add(SesionHistoricoPeer::FECHA_LOGIN,
                                                    SesionHistoricoPeer::FECHA_LOGIN .
                                                        " BETWEEN '" . $desdeF .
                                                        "' AND '"    . $hastaF .
                                                        "'",
                                                    Criteria::CUSTOM);

                            if (SesionHistoricoPeer::doCount($criteriaActividad) > 0) {
                                $asistencias++;
                            }
                        }

                        // Si no asistio a las ultimas de fechas se considera desertor
                        if ($asistencias == 0) {
                            if (!array_key_exists($grupoAlumnos->getGrupoId(), $resultadosGrupo)) {
                                $resultadosGrupo[$grupoAlumnos->getGrupoId()]['centro']            = $centroId;
                                $resultadosGrupo[$grupoAlumnos->getGrupoId()]['grupo']             = $grupoAlumnos->getGrupoId();
                                $resultadosGrupo[$grupoAlumnos->getGrupoId()]['curso']             = $grupoAlumnos->getGrupo()->getCursoId();
                                $resultadosGrupo[$grupoAlumnos->getGrupoId()]['facilitador']       = $grupoAlumnos->getGrupo()->getFacilitadorId();
                                $resultadosGrupo[$grupoAlumnos->getGrupoId()]['categoria']         = $grupoAlumnos->getGrupo()->getCurso()->getCategoriaCursoId();
                                $resultadosGrupo[$grupoAlumnos->getGrupoId()]['aula']              = $grupoAlumnos->getGrupo()->getAulaId();
                                $resultadosGrupo[$grupoAlumnos->getGrupoId()]['alumnosDesertores'] = 1;
                            } else {
                                $resultadosGrupo[$grupoAlumnos->getGrupoId()]['alumnosDesertores']+= 1;
                            }
                        }
                    }
                }
            }

            /* Pagos porgramados*/
            //error_log('pagos programdos');
            $fec = date('Y-m-d', $fecha);
            $sql = "SELECT
                        r.grupo_id,
                        r.fecha_inicio, --Fecha de inicio del grupo
                        r.fecha_fin,    --Fecha de fin del grupo
                        r.activo,       --Bandera que indica si fue cancelado o no
                        r.finalizado,   --Indica si ya finalizó el grupo (true) o no (false)
                        m.socio_id,
                        m.detalle_orden_id,
                        m.orden_base_id,
                        m.orden_id AS orden_id,
                        m.fecha_a_pagar,
                        m.pagado,
                        m.fecha_pagado,
                        m.subtotal AS subtotal_pagado,
                        m.subtotal_a_pagar
                    FROM (
                        SELECT
                            sp.detalle_orden_id,
                            sp.socio_id,
                            sp.orden_base_id,
                            d.orden_id AS orden_id,
                            sp.fecha_a_pagar,
                            sp.pagado,
                            sp.fecha_pagado,
                            d.subtotal,
                            sp.subtotal AS subtotal_a_pagar
                        FROM
                            socio_pagos AS sp
                        LEFT JOIN
                            detalle_orden AS d ON (d.id = sp.detalle_orden_id)
                        WHERE
                            sp.fecha_a_pagar = '$fec 00:00:00'
                            AND
                            sp.centro_id = $centroId
                    ) AS m --Convertimos el primer select en una subtabla al vuelo
                    LEFT JOIN (
                        SELECT
                            ap.grupo_id,
                            sp.orden_base_id,
                            d2.orden_id,
                            sp.detalle_orden_id,
                            h.fecha_inicio,
                            h.fecha_fin,
                            g.activo,
                            (CURRENT_DATE > h.fecha_fin)::boolean AS finalizado
                        FROM
                            socio_pagos AS sp
                        LEFT JOIN
                            detalle_orden AS d2 ON (d2.id = detalle_orden_id)
                        JOIN
                            alumno_pagos AS ap ON (ap.orden_base_id = d2.orden_id)
                        JOIN
                            grupo AS g ON (ap.grupo_id = g.id)
                        JOIN
                            horario AS h ON (h.id = g.horario_id)
                        WHERE
                            sp.pagado = true
                        ORDER BY
                            h.fecha_fin
                    ) AS r --Convertimos el segundo select en otra tabla para hacer join con el primero
                    ON (r.orden_base_id = m.orden_base_id)";

            $connection = Propel::getConnection(ProductoPeer::DATABASE_NAME, Propel::CONNECTION_READ);
            $statement  = $connection->prepare($sql);

            $statement->execute();

            $pagosProgramados = array();
            while ($pagos = $statement->fetch(PDO::FETCH_ASSOC)) {
                // Comprobamos que la fecha de de fin de curso no sea mayor a la fecha a pagar
                $fechaPagar = explode(' ', $pagos['fecha_a_pagar']);
                $fechaPagar = explode('-', $fechaPagar[0]);
                $fechaFin   = explode(' ', $pagos['fecha_fin']);
                $fechaFin   = explode('-', $fechaFin[0]);

                if ((int)$fechaPagar[0] . $fechaPagar[1] . $fechaPagar[2] <= (int)$fechaFin[0] . $fechaFin[1] . $fechaFin[2]) {
                    if (!array_key_exists($pagos['socio_id'], $pagosProgramados[$pagos['grupo_id']])) {
                        $pagosProgramados[$pagos['grupo_id']][$pagos['socio_id']] = array(
                            'aPagar' => $pagos['subtotal_a_pagar']
                        );
                    }
                }
            }

            //Contamos los pagos programados y la cobranza esperada
            foreach ($pagosProgramados as $grupoKey => $cobranzaEsperada) {
                if ($grupoKey) {
                    $grupo = GrupoPeer::retrieveByPK($grupoKey);

                    if (!array_key_exists($grupoKey, $resultadosGrupo)) {
                        $totalCobranza = 0;

                        foreach ($cobranzaEsperada as $cobranza) {
                            $totalCobranza+= $cobranza['aPagar'];
                        }

                        $resultadosGrupo[$grupoKey]['centro']             = $centroId;
                        $resultadosGrupo[$grupoKey]['grupo']              = $grupoKey;
                        $resultadosGrupo[$grupoKey]['curso']              = $grupo->getCursoId();
                        $resultadosGrupo[$grupoKey]['facilitador']        = $grupo->getFacilitadorId();
                        $resultadosGrupo[$grupoKey]['categoria']          = $grupo->getCurso()->getCategoriaCursoId();
                        $resultadosGrupo[$grupoKey]['aula']               = $grupo->getAulaId();
                        $resultadosGrupo[$grupoKey]['pagosProgramados']   = count($cobranzaEsperada);
                        $resultadosGrupo[$grupoKey]['cobranzaProgramada'] = $totalCobranza;
                    } else {
                        $resultadosGrupo[$grupoKey]['pagosProgramados']+= count($cobranzaEsperada);
                        $resultadosGrupo[$grupoKey]['cobranzaProgramada']+= $totalCobranza;
                    }
                }
            }

            /* Pagos pagados*/
            //error_log('pagos pagados');
            $sql        = "SELECT
            r.grupo_id,
            r.fecha_inicio, --Fecha de inicio del grupo
            r.fecha_fin,    --Fecha de fin del grupo
            r.activo,       --Bandera que indica si fue cancelado o no
            r.finalizado,   --Indica si ya finalizó el grupo (true) o no (false)
            m.socio_id,
            m.detalle_orden_id,
            m.orden_base_id,
            m.orden_id as orden_id,
            m.fecha_a_pagar,
            m.pagado,
            m.fecha_pagado,
            m.subtotal as subtotal_pagado,
            m.subtotal_a_pagar

            FROM (

            SELECT
            sp.detalle_orden_id,
            sp.socio_id,
            sp.orden_base_id,
            d.orden_id as orden_id,
            sp.fecha_a_pagar,
            sp.pagado,
            sp.fecha_pagado,
            d.subtotal,
            sp.subtotal as subtotal_a_pagar
            FROM
            socio_pagos AS sp
            LEFT JOIN
            detalle_orden AS d ON (d.id = sp.detalle_orden_id)
            WHERE sp.fecha_pagado BETWEEN '" . date('Y-m-d', $fecha) . " 00:00:00' AND '" . date('Y-m-d', $fecha) . " 23:59:59'
            AND sp.centro_id = " . $centroId . "
            AND sp.pagado = true
            AND sp.cancelado = false

            )
            AS m --Convertimos el primer select en una subtabla al vuelo

            LEFT JOIN
            (
            select
            ap.grupo_id,
            sp.orden_base_id,
            d2.orden_id,
            sp.detalle_orden_id,
            h.fecha_inicio,
            h.fecha_fin,
            g.activo,
            (CURRENT_DATE > h.fecha_fin)::boolean as finalizado
            FROM socio_pagos as sp
            LEFT JOIN detalle_orden as d2 on (d2.id = detalle_orden_id)
            JOIN alumno_pagos as ap on (ap.orden_base_id = d2.orden_id)
            JOIN grupo as g on (ap.grupo_id = g.id)
            JOIN horario as h on (h.id = g.horario_id)
            where sp.pagado = true
            ORDER BY h.fecha_fin
            )
            AS r --Convertimos el segundo select en otra tabla para hacer join con el primero
            ON (r.orden_base_id = m.orden_base_id)";
            $connection = Propel::getConnection(ProductoPeer::DATABASE_NAME, Propel::CONNECTION_READ);
            $statement  = $connection->prepare($sql);
            $statement->execute();
            $pagosPagados  = array();
            while ($pagos         = $statement->fetch(PDO::FETCH_ASSOC)) {
                // Comprobamos que la fecha de de fin de curso no sea mayor a la fecha a pagar
                $fechaPagar    = explode(' ', $pagos['fecha_a_pagar']);
                $fechaPagar    = explode('-', $fechaPagar[0]);
                $fechaFin      = explode(' ', $pagos['fecha_fin']);
                $fechaFin      = explode('-', $fechaFin[0]);
                if ((int)$fechaPagar[0] . $fechaPagar[1] . $fechaPagar[2] <= (int)$fechaFin[0] . $fechaFin[1] . $fechaFin[2]) {
                    if (!array_key_exists($pagos['socio_id'], $pagosPagados[$pagos['grupo_id']])) {
                        $pagosPagados[$pagos['grupo_id']][$pagos['socio_id']]               = array('pagado'               => $pagos['subtotal_pagado']);
                    }
                }
            }
            //Contamos los pagos programados y la cobranza esperada
            foreach ($pagosPagados as $grupoKey      => $cobranzaEfectiva) {
                if ($grupoKey) {
                    $grupo         = GrupoPeer::retrieveByPK($grupoKey);
                    if (!array_key_exists($grupoKey, $resultadosGrupo)) {
                        $totalCobranza = 0;
                        foreach ($cobranzaEfectiva as $socio      => $cobranza) {
                            $totalCobranza+= $cobranza['pagado'];
                            if (!in_array($socio, $resultadosGrupo[$grupo->getId()]['alumnosActivos'])) {
                                if (!array_key_exists($grupoKey, $resultadosGrupo)) {
                                    ////error_log('almacenado activos');
                                    $resultadosGrupo[$grupoKey]['centro']            = $centroId;
                                    $resultadosGrupo[$grupoKey]['grupo']            = $grupoKey;
                                    $resultadosGrupo[$grupoKey]['curso']            = $grupo->getCursoId();
                                    $resultadosGrupo[$grupoKey]['facilitador']            = $grupo->getFacilitadorId();
                                    $resultadosGrupo[$grupoKey]['categoria']            = $grupo->getCurso()->getCategoriaCursoId();
                                    $resultadosGrupo[$grupoKey]['aula']            = $grupo->getAulaId();
                                    $resultadosGrupo[$grupoKey]['alumnosActivos'][]            = $socio;
                                } else {
                                    $resultadosGrupo[$grupoKey]['alumnosActivos'][]            = $socio;
                                }
                            }
                            $socioDatos = SocioPeer::retrieveByPK($socio);
                            $alumnosActivos[$grupo->getId()][]            = array('id'            => $socioDatos->getId(), 'usuario'            => $socioDatos->getUsuario(), 'nombre'            => $socioDatos->getNombreCompleto(), 'edad'            => $socioDatos->getEdad(), 'ocupacion'            => $socioDatos->getOcupacionId(), 'sexo'            => $socioDatos->getSexo());
                        }
                        $resultadosGrupo[$grupoKey]['centro']            = $centroId;
                        $resultadosGrupo[$grupoKey]['grupo']            = $grupoKey;
                        $resultadosGrupo[$grupoKey]['curso']            = $grupo->getCursoId();
                        $resultadosGrupo[$grupoKey]['facilitador']            = $grupo->getFacilitadorId();
                        $resultadosGrupo[$grupoKey]['categoria']            = $grupo->getCurso()->getCategoriaCursoId();
                        $resultadosGrupo[$grupoKey]['aula']            = $grupo->getAulaId();
                        $resultadosGrupo[$grupoKey]['pagosCobrados']            = count($cobranzaEfectiva);
                        $resultadosGrupo[$grupoKey]['cobranzaCobrada']            = $totalCobranza;
                    } else {
                        $resultadosGrupo[$grupoKey]['pagosCobrados']+= count($cobranzaEfectiva);
                        $resultadosGrupo[$grupoKey]['cobranzaCobrada']+= $totalCobranza;
                    }
                }
            }
            /* Guardamos los valores */
            foreach ($resultadosGrupo as $key                        => $resultado) {
                $reporteIndicadoresDiaGrupo = new ReporteIndicadoresDiaGrupo();
                $reporteIndicadoresDiaGrupo->setCentroId($resultado['centro']);
                $reporteIndicadoresDiaGrupo->setGrupoId($resultado['grupo']);
                $reporteIndicadoresDiaGrupo->setCursoId($resultado['curso']);
                $reporteIndicadoresDiaGrupo->setFacilitadorId(($resultado['facilitador'] ? $resultado['facilitador'] : 1));
                $reporteIndicadoresDiaGrupo->setCategoriaCursoId($resultado['categoria']);
                $reporteIndicadoresDiaGrupo->setAulaId($resultado['aula']);
                $reporteIndicadoresDiaGrupo->setFecha(date('Y-m-d', $fecha));
                $reporteIndicadoresDiaGrupo->setSemana(date('W', $fecha));
                $reporteIndicadoresDiaGrupo->setAnio(date('Y', $fecha));
                $reporteIndicadoresDiaGrupo->setSociosPreinscritos((array_key_exists('preinscritos', $resultado) ? $resultado['preinscritos'] : 0));
                $reporteIndicadoresDiaGrupo->setSociosInscritos((array_key_exists('inscritos', $resultado) ? $resultado['inscritos'] : 0));
                $reporteIndicadoresDiaGrupo->setSociosInscritosNuevos((array_key_exists('inscritosNuevos', $resultado) ? $resultado['inscritosNuevos'] : 0));
                $reporteIndicadoresDiaGrupo->setSociosInscritosRecurrentes((array_key_exists('inscritosRecurrentes', $resultado) ? $resultado['inscritosRecurrentes'] : 0));
                $reporteIndicadoresDiaGrupo->setAlumnosActivos((array_key_exists('alumnosActivos', $resultado) ? count($resultado['alumnosActivos']) : 0));
                $reporteIndicadoresDiaGrupo->setAlumnosActividadProgramada((array_key_exists('alumnosActividadProgramada', $resultado) ? count($resultado['alumnosActividadProgramada']) : 0));
                $reporteIndicadoresDiaGrupo->setAlumnosGraduados((array_key_exists('alumnosGraduados', $resultado) ? $resultado['alumnosGraduados'] : 0));
                $reporteIndicadoresDiaGrupo->setGraduacionEsperada((array_key_exists('graduacionEsperada', $resultado) ? $resultado['graduacionEsperada'] : 0));
                $reporteIndicadoresDiaGrupo->setAlumnosDesertores((array_key_exists('alumnosDesertores', $resultado) ? $resultado['alumnosDesertores'] : 0));
                $reporteIndicadoresDiaGrupo->setPagosProgramados((array_key_exists('pagosProgramados', $resultado) ? $resultado['pagosProgramados'] : 0));
                $reporteIndicadoresDiaGrupo->setPagosCobrados((array_key_exists('pagosCobrados', $resultado) ? $resultado['pagosCobrados'] : 0));
                $reporteIndicadoresDiaGrupo->setCobranzaProgramada((array_key_exists('cobranzaProgramada', $resultado) ? $resultado['cobranzaProgramada'] ? $resultado['cobranzaProgramada'] : 0 : 0));
                $reporteIndicadoresDiaGrupo->setCobranzaCobrada((array_key_exists('cobranzaCobrada', $resultado) ? $resultado['cobranzaCobrada'] ? $resultado['cobranzaCobrada'] : 0 : 0));
                $reporteIndicadoresDiaGrupo->save();
                // Guardamos los socios inscritos al grupo

                foreach ($arregloInscritos[$resultado['grupo']] as $inscrito) {
                    $reporteInscritos = new ReporteIndicadoresDiaGrupoSociosInscritos();
                    $reporteInscritos->setReporteIndicadorDiaGrupoId($reporteIndicadoresDiaGrupo->getId());
                    $reporteInscritos->setSocioId($inscrito['id']);
                    $reporteInscritos->setCentroId($reporteIndicadoresDiaGrupo->getCentroId());
                    $reporteInscritos->setUsuario($inscrito['usuario']);
                    $reporteInscritos->setNombreCompleto($inscrito['nombre']);
                    $reporteInscritos->setEdad($inscrito['edad']);
                    $reporteInscritos->setOcupacionId($inscrito['ocupacion']);
                    $reporteInscritos->setSexo($inscrito['sexo']);
                    $reporteInscritos->save();
                }
                // Guardamos los socios activos
                foreach ($alumnosActivos[$resultado['grupo']] as $activo) {
                    $reporteActivos = new ReporteIndicadoresDiaGrupoAlumnosActivos();
                    $reporteActivos->setReporteIndicadorDiaGrupoId($reporteIndicadoresDiaGrupo->getId());
                    $reporteActivos->setId(Comun::generaId());
                    $reporteActivos->setSocioId($activo['id']);
                    $reporteActivos->setCentroId($reporteIndicadoresDiaGrupo->getCentroId());
                    $reporteActivos->setUsuario($activo['usuario']);
                    $reporteActivos->setNombreCompleto($activo['nombre']);
                    $reporteActivos->setEdad($activo['edad']);
                    $reporteActivos->setOcupacionId($activo['ocupacion']);
                    $reporteActivos->setSexo($activo['sexo']);

                    $reporteActivos->save();
                }
                // Guardamos los socios con actividad programada
                foreach ($alumnosActividadProgramada[$resultado['grupo']] as $programada) {
                    $reporteActivos = new ReporteIndicadoresDiaGrupoAlumnosActividadProgramada();
                    $reporteActivos->setReporteIndicadorDiaGrupoId($reporteIndicadoresDiaGrupo->getId());
                    $reporteActivos->setId(Comun::generaId());
                    $reporteActivos->setSocioId($programada['id']);
                    $reporteActivos->setCentroId($reporteIndicadoresDiaGrupo->getCentroId());
                    $reporteActivos->setUsuario($programada['usuario']);
                    $reporteActivos->setNombreCompleto($programada['nombre']);
                    $reporteActivos->setEdad($programada['edad']);
                    $reporteActivos->setOcupacionId($programada['ocupacion']);
                    $reporteActivos->setSexo($programada['sexo']);

                    $reporteActivos->save();
                }
            }
        }
        $this->logSection('Se guardo correctamente el reporte por dia y grupo', '');
    }
}
?>

