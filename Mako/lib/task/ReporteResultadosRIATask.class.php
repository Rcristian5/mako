<?php
class ReporteResultadosRIATask extends sfBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación', 'frontend'),
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'               ,  'tolu2' ),
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conexión'  , 'propel'),
            new sfCommandOption('desde'      , null, sfCommandOption::PARAMETER_REQUIRED, 'Parametro requerido para indicar a partir de que fecha se requiere generar el reporte'),
            new sfCommandOption('hasta'      , null, sfCommandOption::PARAMETER_OPTIONAL, 'Parametro opcional para indicar hasta que fecha se obtendran las calificaciones')
        ));

        $this->namespace           = 'mako';
        $this->name                = 'resultados-ria';
        $this->briefDescription    = 'Genera reporte general de resultados de la ria';
        $this->detailedDescription = <<<EOF
The [mako:reporte-pagos-semanales|INFO] task does things.
Call it with:

  [php symfony mako:reporte-pagos-semanales|INFO]
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        // initialize the database connection
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'] ? $options['connection'] : null)->getConnection();
        $reporteCurso    = fopen("/tmp/resultados_centro.csv", "w");
        $reporteRIA      = fopen("/tmp/resultados_ria.csv", "w");

        if ($options['desde'] && $options['hasta']) {
            $centroId = sfConfig::get('app_centro_actual_id');
            $desde    = $options['desde'] . ' 00:00:00';
            $hasta    = $options['hasta'] . ' 23:59:59';

            foreach (CursoPeer::doSelect(new Criteria()) as $curso) {
                $inscritos        = 0;
                $semanas          = 0;
                $cobranzaTotal    = 0;
                $cobranzaEsperada = 0;
                $cobranzaPagos    = 0;
                $asistencias      = 0;
                $graduados        = 0;
                $extensionPago    = 0;
                $asistieron       = 0;
                $clases           = 0;

                // Inscritos
                $criteria = new Criteria();
                $criteria->add(GrupoAlumnosPeer::PREINSCRITO, false);
                $criteria->add(GrupoAlumnosPeer::UPDATED_AT,
                               GrupoAlumnosPeer::UPDATED_AT .
                                    " BETWEEN '" . $desde .
                                    "' AND '"    . $hasta .
                                    "'",
                               Criteria::CUSTOM);
                $criteria->addJoin(GrupoAlumnosPeer::GRUPO_ID, GrupoPeer::ID);
                $criteria->add(GrupoPeer::CURSO_ID , $curso->getId());
                $criteria->add(GrupoPeer::CENTRO_ID, $centroId);
                $criteria->add(GrupoPeer::ACTIVO   , true);

                $inscritos = GrupoAlumnosPeer::doCount($criteria);

                // Cobranza General
                $criteria  = new Criteria();
                $criteria->add(DetalleOrdenPeer::CREATED_AT,
                               DetalleOrdenPeer::CREATED_AT .
                                    " BETWEEN '" . $desde .
                                    "' AND '"    . $hasta .
                                    "'",
                               Criteria::CUSTOM);
                $criteria->addJoin(DetalleOrdenPeer::PRODUCTO_ID, ProductoPeer::ID);
                $criteria->add(ProductoPeer::CODIGO,
                               ProductoPeer::CODIGO .
                                    " LIKE '" . $curso->getClave() .
                                    "%'",
                               Criteria::CUSTOM);

                foreach (DetalleOrdenPeer::doSelect($criteria) as $detalle) {
                    $cobranzaTotal+= $detalle->getSubtotal();
                }

                // Cobranza Esperada
                $criteria = new Criteria();
                $criteria->add(SocioPagosPeer::FECHA_A_PAGAR,
                               SocioPagosPeer::FECHA_A_PAGAR .
                                    " BETWEEN '" . $desde .
                                    "' AND '"    . $hasta .
                                    "'",
                               Criteria::CUSTOM);
                $criteria->addJoin(SocioPagosPeer::PRODUCTO_ID, ProductoPeer::ID);
                $criteria->add(ProductoPeer::CODIGO,
                               ProductoPeer::CODIGO .
                                    " LIKE '" . $curso->getClave() .
                                    "%'",
                               Criteria::CUSTOM);

                foreach (SocioPagosPeer::doSelect($criteria) as $pagos) {
                    $cobranzaEsperada+= $pagos->getSubtotal();
                }

                // Cobranza "Real"
                $criteria = new Criteria();
                $criteria->add(SocioPagosPeer::PAGADO, true);
                $criteria->add(SocioPagosPeer::FECHA_PAGADO,
                               SocioPagosPeer::FECHA_PAGADO .
                                    " BETWEEN '" . $desde .
                                    "' AND '"    . $hasta .
                                    "'",
                               Criteria::CUSTOM);
                $criteria->addJoin(SocioPagosPeer::PRODUCTO_ID, ProductoPeer::ID);
                $criteria->add(ProductoPeer::CODIGO,
                               ProductoPeer::CODIGO .
                                    " LIKE '" . $curso->getClave() .
                                    "%'",
                               Criteria::CUSTOM);

                foreach (SocioPagosPeer::doSelect($criteria) as $pago) {
                    $cobranzaPagos+= $pago->getDetalleOrden()->getSubtotal();
                }

                // Asistencias

                // Sacamos primero las fechas de clase del mes
                $criteria = new Criteria();
                $criteria->add(FechasHorarioPeer::FECHA,
                               FechasHorarioPeer::FECHA .
                                    " BETWEEN '" . $options['desde'] .
                                    "' AND '"    . $options['hasta'] .
                                    "'",
                               Criteria::CUSTOM);
                $criteria->addJoin(FechasHorarioPeer::HORARIO_ID, HorarioPeer::ID);
                $criteria->add(HorarioPeer::CURSO_ID, $curso->getId());
                $criteria->addJoin(HorarioPeer::ID, GrupoPeer::HORARIO_ID);
                $criteria->add(GrupoPeer::CENTRO_ID, $centroId);
                $criteria->add(GrupoPeer::ACTIVO, true);

                foreach (FechasHorarioPeer::doSelect($criteria) as $fecha) {
                    $criteriaL = new Criteria();
                    $criteriaL->add(GrupoPeer::HORARIO_ID, $fecha->getHorarioId());
                    $criteriaL->add(GrupoAlumnosPeer::PREINSCRITO, false);
                    $criteriaL->addJoin(GrupoPeer::ID, GrupoAlumnosPeer::GRUPO_ID);

                    foreach (GrupoAlumnosPeer::doSelect($criteriaL) as $alumno) {
                        $criteriaAs = new Criteria();
                        $criteriaAs->add(SesionHistoricoPeer::CENTRO_ID, $centroId);
                        $criteriaAs->add(SesionHistoricoPeer::SOCIO_ID, $alumno->getAlumnoId());

                        $desdeF = $fecha->getFecha('Y-m-d') . ' ' . $fecha->getHoraInicio('H:i:s');
                        $hastaF = $fecha->getFecha('Y-m-d') . ' ' . $fecha->getHoraFin('H:i:s');

                        $criteriaAs->add(SesionHistoricoPeer::FECHA_LOGIN, SesionHistoricoPeer::FECHA_LOGIN . " BETWEEN '" . $desdeF . "' AND '" . $hastaF . "'", Criteria::CUSTOM);

                        if (SesionHistoricoPeer::doCount($criteriaAs) > 0) {
                            $asistieron++;
                        }

                        $clases++;
                    }
                }

                $asistencias = ($asistieron * 100) / $clases;

                // Graduados
                // Hay que obtener unicamente los grupos que finalizaron en el mes
                $criteriaA   = new Criteria();
                $criteriaA->add(GrupoPeer::CURSO_ID, $curso->getId());
                $criteriaA->add(GrupoPeer::ACTIVO, true);
                $criteria->add(GrupoPeer::CENTRO_ID, $centroId);
                $criteriaA->add(HorarioPeer::FECHA_FIN,
                                HorarioPeer::FECHA_FIN .
                                    " BETWEEN '" . $desde .
                                    "' AND '"    . $hasta .
                                    "'",
                                Criteria::CUSTOM);
                $criteriaA->add(GrupoAlumnosPeer::PREINSCRITO, false);
                $criteriaA->addJoin(GrupoAlumnosPeer::GRUPO_ID, GrupoPeer::ID);
                $criteriaA->addJoin(HorarioPeer::ID, GrupoPeer::HORARIO_ID);

                // Buscamos las asistencias
                foreach (GrupoAlumnosPeer::doSelect($criteriaA) as $registro) {
                    // Linea de reporte de inscritos
                    $socio             = SocioPeer::retrieveByPK($registro->getAlumnoId());
                    $asisntenciasSocio = 0;
                    $clasesSocio       = 0;
                    $grupo             = GrupoPeer::retrieveByPK($registro->getGrupoId());

                    // Reporte de asistencias
                    foreach ($grupo->getHorario()->getFechasHorarios() as $dias) {
                        $criteriaAs = new Criteria();
                        $criteriaAs->add(SesionHistoricoPeer::CENTRO_ID, $centroId);
                        $criteriaAs->add(SesionHistoricoPeer::SOCIO_ID, $registro->getAlumnoId());

                        $desdeC = $dias->getFecha('Y-m-d') . ' ' . $dias->getHoraInicio('H:i:s');
                        $hastaC = $dias->getFecha('Y-m-d') . ' ' . $dias->getHoraFin('H:i:s');

                        $criteriaAs->add(SesionHistoricoPeer::FECHA_LOGIN,
                                         SesionHistoricoPeer::FECHA_LOGIN .
                                            " BETWEEN '" . $desdeC .
                                            "' AND '"    . $hastaC .
                                            "'",
                                         Criteria::CUSTOM);

                        if (SesionHistoricoPeer::doCount($criteriaAs) > 0) {
                            $asisntenciasSocio++;
                        }

                        $clasesSocio++;
                    }
                    $porcentajeSocio = ($asisntenciasSocio * 100) / $clasesSocio;
                    if ($porcentajeSocio >= 80) {
                        $graduados++;
                    }
                }

                // Buscamos las extensiones de cobro
                $criteria = new Criteria();
                $criteria->add(PlazoFechaCobroPeer::FECHA_FIN, PlazoFechaCobroPeer::FECHA_FIN . " BETWEEN '" . $desde . "' AND '" . $hasta . "'", Criteria::CUSTOM);
                $criteria->add(PlazoFechaCobroPeer::ACTIVO, true);
                $criteria->addJoin(PlazoFechaCobroPeer::GRUPO_ID, GrupoPeer::ID);
                $criteria->add(GrupoPeer::CENTRO_ID, $centroId);
                $criteria->add(GrupoPeer::ACTIVO, true);
                $criteria->add(GrupoPeer::CURSO_ID, $curso->getId);

                $extensionPago = PlazoFechaCobroPeer::doCount($criteria);

                if ($asistencias > 0 || $inscritos > 0 || $cobranzaTotal > 0 || $cobranzaPagos > 0 || $graduados > 0 || $extensionPago > 0) {
                    $centro            = CentroPeer::retrieveByPK($centroId);
                    $resultadosCentro .= $curso->getNombre()  . ',' .
                                         $centro->getNombre() . ',' .
                                         $inscritos           . ',' .
                                         $extensionPago       . ',' .
                                         $cobranzaTotal       . ',' .
                                         $cobranzaPagos       . ',' .
                                         $cobranzaEsperada    . ',' .
                                         $asistencias         . ',' .
                                         $graduados           . "\n\r";
                    $resultadosRIA    .= $curso->getId()      . ',' .
                                         $inscritos           . ',' .
                                         $extensionPago       . ',' .
                                         $cobranzaTotal       . ',' .
                                         $cobranzaPagos       . ',' .
                                         $cobranzaEsperada    . ',' .
                                         $asistieron          . ',' .
                                         $clases              . "," .
                                         $graduados           . "," .
                                         $curso->getNombre() . "\n\r";
                }
            }

            fwrite($reporteCurso, $resultadosCentro);
            fwrite($reporteRIA, $resultadosRIA);

            fclose($reporteCurso);
            fclose($reporteRIA);
        } else {
            $this->log('Falta la fecha de inicio o la fecha final');
        }
    }
}

