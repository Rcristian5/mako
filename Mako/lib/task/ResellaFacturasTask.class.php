<?php
class facturandoResellaFacturasTask extends sfBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment',  'tolu2' ),
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'propel'),
            new sfCommandOption('certificado', null, sfCommandOption::PARAMETER_REQUIRED, 'Opcion que indica el numero del certificado', null),
            new sfCommandOption('cancelar', null, sfCommandOption::PARAMETER_REQUIRED, 'Opcion para indicar si la factura se cancela y debe asiganarsele una nueva fecha', 0),
            new sfCommandOption('fecha_inicio', null, sfCommandOption::PARAMETER_REQUIRED, 'Opcion para dar una fecha apartir de la cual se deben resellar'),
            new sfCommandOption('fecha_fin', null, sfCommandOption::PARAMETER_OPTIONAL, 'Opcion para dar una fecha hasta la que se deben resellar', null)
        ));

        $this->namespace           = 'mako';
        $this->name                = 'resella-facturas';
        $this->briefDescription    = 'Genera un nuevo sello para facturas';
        $this->detailedDescription = <<<EOF
        [facturando:resella-facturas|INFO] Sella nuevamente las facturas.
Guarda las facturas reselladas en la carpeta "fixes", la cual se crea en la carpeta "facturas"

Se debe especificar al menos la fecha a partir de la cual se requiere resellar

symfony mako:resella-facturas --application="frontend" --certificado="00001000000103720604" --fecha_inicio="20110520"

Tambien se puede especificar la fecha de termino

symfony mako:resella-facturas --application="frontend" --certificado="00001000000103720604" --fecha_inicio="20110520" --fecha_fin="20110601"

Por ultimo, si se requiere cancelar la factura

symfony mako:resella-facturas --application="frontend" --certificado="00001000000103720604" --fecha_inicio="20110520" --fecha_fin="20110601" cancelar=1

*La feha se especifica con aaaammdd

        [php symfony facturando:resella-facturas|INFO]
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager           = new sfDatabaseManager($this->configuration);
        $connection                = $databaseManager->getDatabase($options['connection'] ? $options['connection'] : null)->getConnection();
        $rutaLlaves                = dirname(__FILE__) . '/../../llaves';
        $rutaFacturas              = dirname(__FILE__) . '/../../web/facturas';
        $rutaFacturasFixes         = dirname(__FILE__) . '/../../web/facturas/fixes';

        // Antes de todo guardamos el folio actual
        $this->logSection('Guardando el folio actual', '...');

        $folioActual = FolioFiscalPeer::fiscalesCentroActual();
        $folio_original['centro_actual']             = sfConfig::get('app_centro_actual');
        $folio_original['folio']             = $folioActual->getFolioActual();
        $dumper      = new sfYamlDumper();
        $yaml        = $dumper->dump($folio_original, 3);

        file_put_contents($rutaFacturas . '/foliofiscal_original.yml', $yaml);
        chmod($rutaFacturas . '/foliofiscal_original.yml', 775);

        $ctx     = sfContext::createInstance($this->configuration);
        // Revisamos la ruta de los sellos
        if (file_exists($rutaLlaves)) {
            // Revisamos que las llaves estan defininas y "desdobladas"
            $cerPath = $rutaLlaves . DIRECTORY_SEPARATOR . $options['certificado'] . '.cer.pem';
            $keyPath = $rutaLlaves . DIRECTORY_SEPARATOR . $options['certificado'] . '.key.pem';
            if (!file_exists($cerPath) && !file_exists($keyPath)) {
                throw new sfCommandException('No se encontraron las llaves');
            }
        } else {
            throw new sfCommandException('No se encontro carpeta de las llaves');
        }
        // Revisamos que exista la ruta de las facturas
        if (file_exists($rutaFacturas)) {
            foreach (scandir($rutaFacturas) as $file) {
                if (is_file($rutaFacturas . DIRECTORY_SEPARATOR . $file)) {
                    $infoFile = pathinfo($rutaFacturas . DIRECTORY_SEPARATOR . $file);
                    if ($infoFile['extension'] == 'xml') {
                        $this->logSection('Analizando', $infoFile['filename']);

                        // Leemos el XMl
                        $objetoXMl    = simplexml_load_file($rutaFacturas . DIRECTORY_SEPARATOR . $file);
                        $nameSpaces   = $objetoXMl->getNamespaces(true);

                        $arrayXML     = $this->Obj2ArrRecursivo($objetoXMl);
                        $arregloDatos = array();

                        // Comprobamos que tenga el namespace
                        $arregloDatos['xmlns']              = 'http://www.sat.gob.mx/cfd/2';
                        $arregloDatos['xmlns:xsi']          = 'http://www.w3.org/2001/XMLSchema-instance';
                        $arregloDatos['xsi:schemaLocation'] = 'http://www.sat.gob.mx/cfd/2 cfdv2.xsd';
                        $arregloDatos                       = array_merge((array)$arregloDatos, (array)$arrayXML['@attributes']);

                        if ($options['certificado']) {
                            $arregloDatos['noCertificado'] = $options['certificado'];
                        }

                        $arregloDatos['certificado'] = '';

                        //Obtenemos la fecha original de la factura
                        // Sacamos la fecha del arreglo para saber que tipo de encriptacion
                        $fechaFactura = explode('T', $arregloDatos['fecha']);
                        $fechaFactura = explode('-', $fechaFactura[0]);
                        $ffis         = null;
                        $createdAt    = null;

                        if ($options['cancelar']) {
                            $arregloDatos['fecha'] = date('Y-m-d') . 'T' . date('H:i:s');
                            $fechaCreated          = explode('T', $arregloDatos);
                            $createdAt             = $fechaCreated[0] . ' ' . $fechaCreated[1];
                            $ffis                  = FolioFiscalPeer::fiscalesCentroActual();
                            $arregloDatos['folio'] = $ffis->getFolioActual();
                        }

                        // Agregamos los datos emisor
                        $arregloDatos['Emisor'] = $arrayXML['Emisor']['@attributes'];
                        $domEmisor              = array();

                        if (sfConfig::get('app_fiscales_calle')        != '') $domEmisor['calle']        = sfConfig::get('app_fiscales_calle');
                        if (sfConfig::get('app_fiscales_noExterior')   != '') $domEmisor['noExterior']   = sfConfig::get('app_fiscales_noExterior');
                        if (sfConfig::get('app_fiscales_noInterior')   != '') $domEmisor['noInterior']   = sfConfig::get('app_fiscales_noInterior');
                        if (sfConfig::get('app_fiscales_colonia')      != '') $domEmisor['colonia']      = sfConfig::get('app_fiscales_colonia');
                        if (sfConfig::get('app_fiscales_municipio')    != '') $domEmisor['municipio']    = sfConfig::get('app_fiscales_municipio');
                        if (sfConfig::get('app_fiscales_estado')       != '') $domEmisor['estado']       = sfConfig::get('app_fiscales_estado');
                        if (sfConfig::get('app_fiscales_pais')         != '') $domEmisor['pais']         = sfConfig::get('app_fiscales_pais');
                        if (sfConfig::get('app_fiscales_codigoPostal') != '') $domEmisor['codigoPostal'] = sfConfig::get('app_fiscales_codigoPostal');

                        $arregloDatos['Emisor']['DomicilioFiscal'] = $domEmisor;

                        // Agregamos los datos del receptor
                        $arregloDatos['Receptor']              = $arrayXML['Receptor']['@attributes'];
                        $arregloDatos['Receptor']['Domicilio'] = $arrayXML['Receptor']['Domicilio']['@attributes'];

                        // Agregamos los conceptos
                        $conceptos = array();
                        if (count($arrayXML['Conceptos']['Concepto']) > 1) {
                            foreach ($arrayXML['Conceptos']['Concepto'] as $concepto) {
                                $conceptos[] = $concepto['@attributes'];
                            }
                        } else {
                            $conceptos[] = $arrayXML['Conceptos']['Concepto']['@attributes'];
                        }
                        // Agregamos los conceptos
                        foreach ($conceptos as $key => $concepto) {

                            $conceptos[$key]['cantidad']      = number_format((double)$concepto['cantidad'], 2, '.', '');
                            $conceptos[$key]['valorUnitario'] = number_format((double)$concepto['valorUnitario'], 2, '.', '');
                            $conceptos[$key]['importe']       = number_format((double)$concepto['importe'], 2, '.', '');
                        }

                        // Obtenemos los impuestos
                        $impuestos = array();
                        if (isset($arrayXML['Impuestos']['Traslados'])) {
                            foreach ($arrayXML['Impuestos']['Traslados'] as $traslados) {
                                if (gettype($traslados) == 'array') {
                                    foreach ($traslados as $traslado) {
                                        $impuestos = array(
                                            'impuesto' => $traslado['impuesto'],
                                            'tasa'     => number_format((double)$traslado['tasa'], 2, '.', ''),
                                            'importe'  => number_format((double)$traslado['importe'], 2, '.', '')
                                        );
                                    }

                                    $traslados = $impuestos;
                                }
                            }
                        }

                        unset($arrayXML['Impuestos']['Traslados']['Traslado']);

                        $arrayXML['Impuestos']['Traslados']['Traslado'] = $impuestos;
                        $arregloDatos['Impuestos']                      = $arrayXML['Impuestos'];
                        $arregloDatos['total']                          = number_format((double)$arregloDatos['total'], 2, '.', '');
                        $arregloDatos['subTotal']                       = number_format((double)$arregloDatos['subTotal'], 2, '.', '');

                        // Obtenemos la cadena original
                        $ctx->getConfiguration()->loadHelpers('Facturas');

                        $cadenaOriginal     = cadenaOriginal($arregloDatos, $conceptos);
                        $fechaFacturaString = $fechaFactura[0] . $fechaFactura[1] . $fechaFactura[2];

                        if ($fechaFacturaString >= $options['fecha_inicio']) {
                            if ($options['fecha_fin'] != null) {
                                if ($fechaFacturaString <= $options['fecha_fin']) {
                                    $this->log('Resellando factura');
                                    $this->crearFactura($cadenaOriginal,
                                                        $fechaFactura,
                                                        $arregloDatos,
                                                        $conceptos,
                                                        $rutaFacturasFixes,
                                                        $infoFile,
                                                        $keyPath,
                                                        $cerPath,
                                                        $options['cancelar'],
                                                        $ffis,
                                                        $createdAt);

                                } else {
                                    $this->log('Se omite esta factura...');
                                }

                            } else {
                                $this->log('Resellando factura...');
                                $this->crearFactura($cadenaOriginal,
                                                    $fechaFactura,
                                                    $arregloDatos,
                                                    $conceptos,
                                                    $rutaFacturasFixes,
                                                    $infoFile,
                                                    $keyPath,
                                                    $cerPath,
                                                    $options['cancelar'],
                                                    $ffis,
                                                    $createdAt);
                            }
                        } else {
                            $this->log('Se omite esta factura...');
                        }
                    }
                }
            }
        } else {
            throw new sfCommandException('No se encontro la ruta de las facturas');
        }
    }

    function Obj2ArrRecursivo($Objeto) {
        if (is_object($Objeto)) {
            $Objeto = get_object_vars($Objeto);
        }

        if (is_array($Objeto)) {
            foreach ($Objeto as $key => $value) {
                $Objeto[$key] = $this->Obj2ArrRecursivo($Objeto[$key]);
            }
        }
        return $Objeto;
    }
    function crearFactura($cadenaOriginal, $fechaFactura, $arregloDatos, $conceptos, $rutaFacturasFixes, $infoFile, $keyPath, $cerPath, $cancelar, $ffis, $createdAt) {
        // Generamos el sello nuevamente
        $crypttext = '';
        $pkeyid    = openssl_get_privatekey(file_get_contents($keyPath));

        openssl_sign($cadenaOriginal, $crypttext, $pkeyid, (((int)$fechaFactura[0] < 2011) ? OPENSSL_ALGO_MD5 : OPENSSL_ALGO_SHA1));
        openssl_free_key($pkeyid);

        $sello = base64_encode($crypttext);
        $datos = file($cerPath);

        // Sacamos el certificado
        $certificado = "";
        $carga       = false;

        for ($i = 0; $i < sizeof($datos); $i++) {
            if (strstr($datos[$i], "END CERTIFICATE")) {
                $carga = false;
            }

            if ($carga) {
                $certificado.= trim($datos[$i]);
            }

            if (strstr($datos[$i], "BEGIN CERTIFICATE")) {
                $carga = true;
            }
        }

        $arregloDatos['certificado'] = $certificado;
        $arregloDatos['sello']       = $sello;

        $xml = cfdxml($arregloDatos, $conceptos);

        // Verirficamos que exista la carpeta para poner las facturas reselladas
        if (!file_exists($rutaFacturasFixes)) {
            mkdir($rutaFacturasFixes);
        }

        if ($cancelar) {
            $arhivo = $arregloDatos['Emisor']['rfc'] . $arregloDatos['serie'] . $arregloDatos['folio'];

            file_put_contents($rutaFacturasFixes . DIRECTORY_SEPARATOR . $arhivo . '.xml', utf8_encode_seguro($xml));

            $f = new Factura();
            $f->setFolioFiscal($ffis);
            $f->setCentroId(sfConfig::get('app_centro_actual_id'));
            $f->setFechaFactura($createdAt);
            $f->setFolio($ffis->getFolioActual());
            $f->setCadenaOriginal($cadenaOriginal);
            $f->setSello($sello);
            $f->setTotal($arregloDatos['total']);
            $f->setImporte($arregloDatos['subTotal']);
            $f->setSubtotal($arregloDatos['subTotal']);
            $f->setRetencion($arregloDatos['Impuestos']['Traslados']['Traslado']['importe']);
            $f->save();

            $ffis->setFolioActual($ffis->getFolioActual() + 1);
            $ffis->save();
        } else {
            file_put_contents($rutaFacturasFixes . DIRECTORY_SEPARATOR . $infoFile['filename'] . '.xml', utf8_encode_seguro($xml));
        }
    }
}

