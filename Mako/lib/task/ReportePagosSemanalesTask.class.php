<?php
class ReportePagosSemanalesTask extends sfBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación', 'frontend'),
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'               ,  'tolu2' ),
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conexión'  , 'propel'),
            new sfCommandOption('desde'      , null, sfCommandOption::PARAMETER_REQUIRED, 'Parametro requerido para indicar a partir de que fecha se requiere generar el reporte'),
            new sfCommandOption('hasta'      , null, sfCommandOption::PARAMETER_OPTIONAL, 'Parametro opcional para indicar hasta que fecha se obtendran las calificaciones'),
            new sfCommandOption('curso_id'   , null, sfCommandOption::PARAMETER_OPTIONAL, 'Parametro opcional para el curso del cual se tomaron los pagos semanales'),
            new sfCommandOption('grupo_id'   , null, sfCommandOption::PARAMETER_OPTIONAL, 'Parametro opcional para indicar un unico grupo para obtener los pgaos semanles')
        ));

        $this->namespace           = 'mako';
        $this->name                = 'reporte-pagos-semanales';
        $this->briefDescription    = 'Genera un archivo con el detalle de pagos semanales para un surso o grupo';
        $this->detailedDescription = <<<EOF
The [mako:reporte-pagos-semanales|INFO] task does things.
Call it with:

  [php symfony mako:reporte-pagos-semanales|INFO]
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'] ? $options['connection'] : null)->getConnection();

        if ($options['grupo_id']) {
            $this->logSection('Lista por grupo:', sfConfig::get('app_centro_actual'));
            $grupo = GrupoPeer::retrieveByPK($options['grupo_id']);

            if ($grupo) {
                $reporteSocios = fopen("/tmp/reporte_pagos_semanales.csv", "w");

                fwrite($reporteSocios, sfConfig::get('app_centro_actual') . "\r\n\r\n");
                fclose($reporteSocios);

                self::generarReporte($grupo);
            } else {
                $this->log('El grupo no existe');
            }
        } else if ($options['desde']) {
            $centroId = sfConfig::get('app_centro_actual_id');
            $desde    = $options['desde'] . ' 00:00:00';
            $hasta    = $options['hasta'] . ' 23:59:59';

            if (!$options['hasta']) {
                $hasta = date('Y-m-d H:i:s');
            }

            if ($options['curso_id']) {
                $curso = CursoPeer::retrieveByPK($options['curso_id']);

                if ($curso) {
                    $reporteSocios = fopen("/tmp/reporte_pagos_semanales.csv", "w");

                    fwrite($reporteSocios, sfConfig::get('app_centro_actual') . "\r\n\r\n");
                    fclose($reporteSocios);

                    $this->logSection('Lista por curso', sfConfig::get('app_centro_actual'));

                    $criteria = new Criteria();
                    $criteria->add(GrupoPeer::CURSO_ID, $curso->getId());
                    $criteria->add(GrupoPeer::CENTRO_ID, $centroId);
                    $criteria->add(GrupoPeer::ACTIVO, true);
                    $criteria->add(HorarioPeer::FECHA_INICIO, HorarioPeer::FECHA_INICIO . " BETWEEN '" . $desde . "' AND '" . $hasta . "' ", Criteria::CUSTOM);
                    $criteria->addJoin(GrupoPeer::HORARIO_ID, HorarioPeer::ID);

                    foreach (GrupoPeer::doSelect($criteria) as $grupo) {
                        self::generarReporte($grupo);
                    }
                } else {
                    $this->log('El curso no existe');
                }

                $databaseManager = new sfDatabaseManager($this->configuration);
            } else {
                $this->log('Falta la el identificador del curso');
            }
        } else {
            $this->log('Falta la fecha de inicio, o el identificador del grupo');
        }
    }

    public function generarReporte(Grupo $grupo) {
        // Abrimos el archivo
        $reporteSocios = fopen("/tmp/reporte_pagos_semanales.csv", "a");

        error_log('Generando lista para el grupo: ' . $grupo->getClave());

        /* Creamos los encabezados del reporte
         * Curso
         * Grupo
         * Fecha Inicio
         * Fecha Fin
         * Inscritos
         * Socios pagron completo
         * Semanas para pagos semanales
        */
        $header = 'Curso, Grupo, Facilitador, Fecha Inicio, Fecha Fin, Total Inscritos, Socios que realizaron pago completo';

        // Agregamos el curso
        $linea  = $grupo->getCurso()->getNombre() . ', ';

        // Agregamos el grupo
        $linea .= $grupo->getClave() . ', ';

        // Agregamos el facilitador
        $linea .= $grupo->getUsuarioRelatedByFacilitadorId()->getNombreCompleto() . ', ';

        // Agregamos la fecha de inicio
        $linea .= $grupo->getHorario()->getFechaInicio('d/m/Y') . ', ';

        // Agregamos la fecha de fin
        $linea .= $grupo->getHorario()->getFechaFin('d/m/Y') . ', ';

        // Agregamos el total de inscritos
        $criteria = new Criteria();
        $criteria->add(GrupoAlumnosPeer::GRUPO_ID, $grupo->getId());
        $criteria->add(GrupoAlumnosPeer::PREINSCRITO, false);

        $linea .= GrupoAlumnosPeer::doCount($criteria) . ', ';

        // Agregamos el total de socio que pagaron completo
        $criteria = new Criteria();
        $criteria->add(AlumnoPagosPeer::GRUPO_ID, $grupo->getId());
        $criteria->add(AlumnoPagosPeer::PAGADO, true);
        $criteria->add(AlumnoPagosPeer::NUMERO_PAGO, 0);

        $linea .= AlumnoPagosPeer::doCount($criteria);

        $fechaAlcanzada = false;
        $dias           = 0;
        $desdeFecha     = $grupo->getHorario()->getFechaInicio('Y-m-d') . ' 23:59:59';
        $hastaFecha     = false;

        do {
            // Creamos una fecha para el header
            $fecha = mktime(
                0,
                0,
                0,
                $grupo->getHorario()->getFechaInicio('n'),
                $grupo->getHorario()->getFechaInicio('j') + $dias,
                $grupo->getHorario()->getFechaInicio('Y')
            );

            if (date('Ymd', $fecha) > $grupo->getHorario()->getFechaFin('Ymd')) {
                $fechaAlcanzada  = true;
                $databaseManager = new sfDatabaseManager($this->configuration);

            } else {
                // Buscamos el numero pagos
                $pagaron = 0;
                foreach ($grupo->getGrupoAlumnoss() as $alumno) {
                    $c3 = new Criteria();
                    $c3->add(AlumnoPagosPeer::SOCIO_ID, $alumno->getAlumnoId());
                    $c3->add(AlumnoPagosPeer::GRUPO_ID, $grupo->getId());

                    $alumnoPago = AlumnoPagosPeer::doSelectOne($c3);

                    if ($alumnoPago != null) {
                        //Obtenemos el orden_id de este pago
                        $orden           = $alumnoPago->getOrden();
                        $sub_producto_de = $alumnoPago->getSubProductoDe();

                        if ($orden != null) {
                            //Obtenemos los detalle_orden_id relacionados con esta orden
                            foreach ($orden->getDetalleOrdens() as $detalleOrden) {
                                foreach ($detalleOrden->getSocioPagoss() as $socioPago) {
                                    $orden_base_id = $socioPago->getOrdenBaseId();
                                    break;
                                }
                                break;
                            }

                            $c4 = new Criteria();
                            $c4->add(SocioPagosPeer::ORDEN_BASE_ID, $orden_base_id);
                            $c4->add(SocioPagosPeer::SUB_PRODUCTO_DE, $sub_producto_de);
                            $c4->add(SocioPagosPeer::PAGADO, true);
                            $c4->add(SocioPagosPeer::CANCELADO, false);

                            if ($hastaFecha) {
                                $c4->add(SocioPagosPeer::FECHA_PAGADO, SocioPagosPeer::FECHA_PAGADO . " BETWEEN '" . $desdeFecha . "' AND '" . date('Y-m-d', $fecha) . " 23:59:59' ", Criteria::CUSTOM);
                            } else {
                                $c4->add(SocioPagosPeer::FECHA_PAGADO, $desdeFecha, Criteria::LESS_EQUAL);
                                $hastaFecha = true;
                            }

                            error_log(SocioPagosPeer::doCount($c4));
                            $pagaron += SocioPagosPeer::doCount($c4);
                        }
                    }
                }

                error_log('__________');

                // Metemos en el header la fecha
                $header.= ', ' . date('d/m/Y', $fecha);

                // Agrefamos a la linea el numero de socios que pagaron
                $linea      .= ', ' . $pagaron;
                $desdeFecha  = date('Y-m-d H:i:s', mktime(0, 0, 0, $grupo->getHorario()->getFechaInicio('n'), $grupo->getHorario()->getFechaInicio('j') + $dias + 1, $grupo->getHorario()->getFechaInicio('Y')));
                $dias       += 7;
            }
        } while (!$fechaAlcanzada);

        fwrite($reporteSocios, $header . "\r\n\r\n");
        fwrite($reporteSocios, $linea . "\r\n\r\n");
        fclose($reporteSocios);
    }
}

