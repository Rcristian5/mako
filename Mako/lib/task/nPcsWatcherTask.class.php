<?php
//Los segundos de compensacion deben coincidir con el numero de minutos que se dispara el script mediante cron
define('NP_SEGUNDOS_COMPENSACION', -300);
class NPcsWatcherTask extends sfBaseTask
{
    private $path_log = '/var/log/mako/sesiones-pc.log';

    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'               ,  'tolu2' ),
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección' , 'propel'),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación', 'frontend'),
            new sfCommandOption('accion'     , null, sfCommandOption::PARAMETER_OPTIONAL, 'Accion que se ejcutara'    , ''),
            new sfCommandOption('debug'      , null, sfCommandOption::PARAMETER_OPTIONAL, 'Muestra info debug'        , false)
        ));

        $this->namespace           = 'mako';
        $this->name                = 'npcs-watcher';
        $this->briefDescription    = 'Pcs watcher. Vigila la coherencia de sesiones en pcs y actúa dependiendo del contexto de uso y rol de usuario.';
        $this->detailedDescription = <<<EOF
The [mako:pcs-watcher|INFO] Pcs watcher:

  [./symfony mako:pcs-watcher --env=prod --accion=|INFO]
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $configuration   = ProjectConfiguration::getApplicationConfiguration('frontend', $options['env'], true);
        $context         = sfContext::createInstance($configuration);
        $dispatcher      = $configuration->getEventDispatcher();
        $debug           = $options['debug'];
        $centro_id       = sfConfig::get('app_centro_actual_id');

        if ($options['accion'] == 'liberar-hosts') {
            $this->liberarHostsAtorados($configuration);
            exit;
        }

        //Las Pcs Del centro
        $pcsc = array();
        $c    = CentroPeer::retrieveByPK(sfConfig::get('app_centro_actual_id'));

        foreach ($c->getComputadoras() as $oPc) {
            $pcsc[$oPc->getIp()] = array(
                'success' => false,
                'failure' => false,
                'who'     => null
            );
        }

        $res  = array();
        unset($res);
        exec("/opt/MakoScripts/tespec who", $res);

        error_log("\n CentroId:$centro_id " . date("Y-m-d H:i:s") . " INICIA RONDA----------- \n", 3, $this->path_log);

        $block      = array();
        $abierto    = false;
        $con_sesion = array();

        foreach ($res as $pc) {
            //error_log(date("Y-m-d H:i:s")." ".$pc->getIp()." ",3,$this->path_log);
            //error_log("IN: ".$pc);

            $abre       = "/^\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b:(.+)/";
            $cierra     = "/(.+)SUCCESS(.+)/";
            $error      = "/(.+)FAILURE(.+)/";
            $ip_actual  = "x";

            if (preg_match($abre, $pc, $aip)) {
                $block = array();

                echo ">>>===================\n";
                //echo "Detectado: ".$aip[0]."\n";

                list($mip, $lodemas)  = explode(":", $aip[0], 2);
                error_log("$mip $lodemas", 3, '/tmp/testnsw.log');

                //$block[]= $aip[0];
                $block[]           = $mip;
                $block[]           = trim($lodemas);
                $abierto           = true;
                $con_sesion[$mip]  = true;
                $pcsc[$mip]['who'] = $lodemas;

            } elseif (preg_match($cierra, $pc, $cip)) {
                //$block[$ip_actual][]= $cip[0];
                //print_r($block);
                $pcsc[$mip]['success'] = $cip;

                if ($abierto) {
                    foreach ($block as $linea) {
                        echo $linea . "\n";
                    }

                    echo "===================<<<\n";
                    $abierto   = false;

                } else {
                    $block     = array();
                    $ip_actual = "x";
                    $ipresp    = preg_replace(array("/\s+/", "/]/"), array("", ""), $cip[2]);

                    if (!isset($con_sesion[$ipresp])) echo $ipresp . ": SESION VACIA\n";
                }
            } elseif (preg_match($error, $pc, $eip)) {
                //$block[$ip_actual][]= $cip[0];
                $pcsc[$mip]['failure'] = $eip;
                //print_r($eip);

            } else {
                $block[] = $pc;

                if ($debug == 'true') print_r($pc);
            }

            //$this->analiza($pc,$dispatcher,$centro_id);
            error_log("\n", 3, $this->path_log);
        }

        print_r($pcsc);

        error_log("\n" . date("Y-m-d H:i:s") . " FIN RONDA----------- \n", 3, $this->path_log);
    }

    function analiza($pc, sfEventDispatcher $dispatcher, $centro_id) {
        echo $pc;
        exit;

        $cmd_base = "/opt/MakoScripts/tespec ";
        $ip       = $pc->getIp();

        //Hacemos ping:
        $res = exec("/usr/bin/perl /opt/MakoScripts/ping.pl " . $ip);

        //Si recibimos que esta apagada la maquina borramos cualquier sesion en la tabla de sesiones q pueda existir
        if ($res == 0) {
            error_log(" maquina apagada.", 3, $this->path_log);

            $c = new Criteria();
            $c->add(SesionPeer::IP, $ip);

            $s = SesionPeer::doSelectOne($c);

            if ($s != null) {
                error_log(" Sesion existente con maquina apagada: " . $s->getUsuario(), 3, $this->path_log);
                $h = SesionPeer::eliminaSesion($s, NP_SEGUNDOS_COMPENSACION);

                $dispatcher->notify(new sfEvent($this, 'socio.logout', array('sesion_historico' => $h)));
            }

        } else {
            //Si esta prendida analizamos si hay sesion
            $this->analizaSesionPc($ip, $dispatcher, $centro_id);
        }
    }

    private function analizaSesionPc($ip, sfEventDispatcher $dispatcher, $centro_id) {
        $cmd_base = "perl /opt/MakoScripts/ejecuta_pc.pl ";
        $res      = exec($cmd_base . $ip . " " . '/usr/bin/who');

        //error_log(" $res ",3,$this->path_log);
        $lins     = explode("\n", $res);

        foreach ($lins as $lin) {
            $lin = preg_replace("/\s{1,}/", "|", $lin);

            list($u, $term, $flogin, $hlogin, $xdisp) = explode("|", $lin);

            $u = strtolower($u);

            if ($u != 'root' && $u != 'bixit' && $u != 'administrador' && $xdisp == '(:0)') {
                error_log(" $u,$term,$flogin,$hlogin,$xdisp ", 3, $this->path_log);

                //Revisamos si esta en sesion
                $c = new Criteria();
                $c->add(SesionPeer::IP, $ip);

                $s = SesionPeer::doSelectOne($c);
                if ($s != null) {
                    $uses = $s->getUsuario();

                    //Revisamos si es el mismo usario el de la sesion que el logueado en la pc
                    if ($uses != $u) {
                        //Si no es el mismo usuario borramos la sesion y creamos una nueva para q no ande de gorron
                        error_log("Socio suplantando sesion: $uses suplantado por $u", 3, $this->path_log);

                        //Eliminamos la sesion suplantada
                        $h = SesionPeer::eliminaSesion($s, NP_SEGUNDOS_COMPENSACION);

                        $dispatcher->notify(new sfEvent($this, 'socio.logout', array('sesion_historico'       => $h)));

                        //Creamos la sesion real del suplantador
                        //Obtenemos el socio rel usuario:
                        $socio = SocioPeer::porNombreUsuario($u);
                        if ($socio == null) {
                            error_log(" NO EXISTE EL USUARIO COMO SOCIO EN LA BD LOCAL!", 3, $this->path_log);
                            continue;
                        }

                        $pc = ComputadoraPeer::getPcPorIp($ip);

                        //$ts = "$flogin $hlogin:00";
                        //Para evitar el caso en que las pcs tienen el tiempo defasado y contemplando que el PcsWatcher corre cada 5 minutos y lo mas que hay de perdida son 5 minutos
                        //mejor tomamos el tiempo del servidor local.

                        $ts     = date("Y-m-d H:i:s");
                        $sesion = SesionPeer::crearSesion($socio, $pc, $ts, $centro_id);

                        $dispatcher->notify(new sfEvent($this, 'socio.login', array('sesion' => $sesion)));

                    } else {
                        error_log("Sesion normal: $uses ", 3, $this->path_log);
                    }
                } else {
                    //Revisamos que tipo de usuario es, si es un usuario de staff no hacemos nada.
                    $c = new Criteria();
                    $c->add(UsuarioPeer::USUARIO, $u);

                    $ustaff = UsuarioPeer::doSelectOne($c);
                    if ($ustaff != null) {
                        error_log("Usuario staff.", 3, $this->path_log);
                        continue;
                    }

                    //Si no es usuario de staff buscamos si es socio
                    $c = new Criteria();
                    $c->add(SocioPeer::USUARIO, $u);

                    $usoc = SocioPeer::doSelectOne($c);
                    if ($usoc != null) {
                        //Si es un socio generamos una sesion para q no se ande paseando de gorra por el centro.
                        error_log("Socio de gorra.", 3, $this->path_log);

                        $pc = ComputadoraPeer::getPcPorIp($ip);

                        //$ts = "$flogin $hlogin:00";
                        //Para evitar el caso en que las pcs tienen el tiempo defasado y contemplando que el PcsWatcher corre cada 5 minutos y lo mas que hay de perdida son 5 minutos
                        //mejor tomamos el tiempo del servidor local.

                        $ts = date("Y-m-d H:i:s");

                        //TODO: Contemplar el caso en que dos sesiones del mismo usuario el mismo dia se traslapan.

                        $sesion = SesionPeer::crearSesion($usoc, $pc, $ts, $centro_id);

                        $dispatcher->notify(new sfEvent($this, 'socio.login', array('sesion' => $sesion)));
                    }
                }
            } else {
                error_log("PC Sin sesion .", 3, $this->path_log);

                //Revisamos si hay sesion en la tabla de sesiones
                $c = new Criteria();
                $c->add(SesionPeer::IP, $ip);

                $s = SesionPeer::doSelectOne($c);
                if ($s != null) {
                    //Si existe una sesion la borramos para que no este consumiendo tiempo de mas.
                    error_log("Con sesion en db: " . $s->getUsuario(), 3, $this->path_log);

                    $h = SesionPeer::eliminaSesion($s, NP_SEGUNDOS_COMPENSACION);
                    $dispatcher->notify(new sfEvent($this, 'socio.logout', array('sesion_historico' => $h)));
                }
            }
        }
    }

    /**
     * Libera los hosts atorados despues de una sesion en el dia.
     * @param $configuration
     */
    private function liberarHostsAtorados($configuration) {
        $configuration->loadHelpers('Ldap');

        $hops  = getHostsOcupados();
        $count = $hops['count'];

        error_log("Parseando " . $count . " Registros");

        for ($i = 0; $i < $count; $i++) {
            $usuario = $hops[$i][cn][0];

            //echo ($i+1).".- ".$usuario."\n";

            modificarHostPermitido($usuario, '*');
        }
    }
}
?>
