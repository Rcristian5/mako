<?php
class ObtenerAsistenciasGeneralesTask extends sfPropelBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección'                                            , 'propel'),
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'                                                          ,  'tolu2' ),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación'                                           , 'frontend'),
            new sfCommandOption('todas'      , null, sfCommandOption::PARAMETER_OPTIONAL, 'Parametro opcional, que indica si se registrarn todas las asistencias', false),
            new sfCommandOption('curso_id'   , null, sfCommandOption::PARAMETER_OPTIONAL, 'Identificador del curso'                                              , false)
        ));

        $this->namespace           = 'mako';
        $this->name                = 'obten-asistencias-generales';
        $this->briefDescription    = 'Obtiene las asistencias generales de un centro';
        $this->detailedDescription = <<<EOF
 [mako:add-cursos-moodlle|INFO] Crea el registro de sincronia de aluas para los centros

  [./symfony mako:ejecuta-sync --env=prod |INFO]
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'])->getConnection();
        $centroActual    = sfConfig::get('app_centro_actual_id');

        $this->logSection('Obtenieno asistencias, centro:', sfConfig::get('app_centro_actual'));

        HorarioPeer::getFechasHorario($centroActual, $options['curos_id'], $options['todas']);

        $this->logSection('Finalizado..', '');
    }
}
?>
