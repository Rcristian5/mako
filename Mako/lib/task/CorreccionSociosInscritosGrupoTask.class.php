<?php
class CorrecionSociosInscritosGrupoTask extends sfPropelBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección' , 'propel'),
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'               ,  'tolu2' ),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación', 'frontend')
        ));

        $this->namespace           = 'mako';
        $this->name                = 'correcion-socios-incritos-grupo';
        $this->briefDescription    = 'Actualiza el cupo de los socios incritos a un grupo';
        $this->detailedDescription = <<<EOF
 [mako:add-cursos-moodlle|INFO] Actualiza las claves de todos los grupos

  [./symfony mako:ejecuta-sync --env=prod |INFO]
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'])->getConnection();

        $this->logSection('Actualizando claves de grupos', '...');

        $criteria = new Criteria();
        $criteria->add(GrupoPeer::CENTRO_ID, sfConfig::get('app_centro_actual_id'));
        $criteria->add(GrupoPeer::ACTIVO, true);

        foreach (GrupoPeer::doSelect($criteria) as $grupo) {
            $this->log('Actualizando grupo: ' . $grupo->getClave());

            $socios = count($grupo->getGrupoAlumnoss());
            $grupo->setCupoActual($socios);

            if ($socios == $grupo->getCupoTotal()) {
                $grupo->setCupoAlcanzado(true);
            }

            $grupo->save();
        }
    }
}
?>

