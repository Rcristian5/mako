<?php
class SyncNoLaborablesCentralTask extends sfPropelBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'               ,  'tolu2' ),
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección' , 'propel'),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación', 'frontend')
        ));

        $this->namespace           = 'mako';
        $this->name                = 'sync-no-laborables-central';
        $this->briefDescription    = 'Sincroniza los dias no laborables del servidor central';

        $this->detailedDescription = <<<EOF
 [mako:add-cursos-moodlle|INFO] Crea el registro de sincronia de los dias no laborables para los centros

  [./symfony mako:ejecuta-sync --env=prod |INFO]
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'])->getConnection();

        $this->logSection('Creando registros de sincronia', '…');

        foreach (DiasNoLaborablesPeer::doSelect(new Criteria()) as $dia) {
            $this->logSection('Crenado registro', $dia->getDescripcion());
            sfProjectConfiguration::getActive()->getEventDispatcher()->notify(new sfEvent($dia, 'nolaborable.creado', array('dia' => $dia)));
        }

        $this->logSection('Se termino de cargar los registros de sincronia', '');
    }
}
?>
