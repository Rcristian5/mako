<?php
class SyncCursosCentralTask extends sfPropelBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'               ,  'tolu2' ),
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección' , 'propel'),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación', 'frontend')
        ));

        $this->namespace           = 'mako';
        $this->name                = 'sync-cursos-central';
        $this->briefDescription    = 'Sincroniza los cursos del servidor central';
        $this->detailedDescription = <<<EOF
 [mako:add-cursos-moodlle|INFO] Crea el registro de sincronia de cursos

  [./symfony mako:ejecuta-sync --env=prod |INFO]
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'])->getConnection();

        $this->logSection('Generando lista de cursos activos', '…');

        $c = new Criteria();
        $c->add(CursoPeer::ACTIVO, true);
        $c->addAscendingOrderByColumn(CursoPeer::NOMBRE);

        $this->logSection('Creando registros de sincronia', '…');

        foreach (CursoPeer::doSelect($c) as $curso) {
            $this->logSection('Creando registro', $curso->getNombre());
            sfProjectConfiguration::getActive()->getEventDispatcher()->notify(new sfEvent($curso, 'curso.creado', array('curso' => $curso)));
        }

        $this->logSection('Se termino de cargar los registros de sincronia', '');
    }
}
?>
