<?php
class makoCargametasTask extends sfBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_OPTIONAL, 'The application name', 'frontend'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_OPTIONAL, 'The environment',  'tolu2' ),
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_OPTIONAL, 'The connection name', 'propel'),
            new sfCommandOption('archivo', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre del archivo'),
            new sfCommandOption('desde', null, sfCommandOption::PARAMETER_OPTIONAL, 'Fecha desde la cual incluir la meta', null)
        ));

        $this->namespace           = 'mako';
        $this->name                = 'carga-metas';
        $this->briefDescription    = 'Carga las metas iniciales de la RIA desde un archivo csv';
        $this->detailedDescription = <<<EOF
[mako:carga-metas|INFO]
  [php symfony mako:carga-metas [--desde=2011-01-01] --archivo=/tmp/metas.csv |INFO]
  El archivo de metas contiene los datos sin encabezados, comenzando desde la primera celda, sin renglones de espacio el siguiente formato:
  Columna A: ID de centro
  Columna B: Meta registro de socios
  Columna C: Meta cursos vendidos (inscripciones)
  Columna D: Cobranza
  Columna E: Periodo mensual
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'] ? $options['connection'] : null)->getConnection();
        $centro_id       = sfConfig::get('app_centro_actual_id');
        $este_centro     = CentroPeer::retrieveByPK($centro_id);
        $alias_reporte   = $este_centro->getAliasReporte();
        $archivo         = $options['archivo'];
        $desde           = $options['desde'];

        if (($gestor = fopen($archivo, "r")) !== false) {
            while (($registro = fgetcsv($gestor, 1000, ",")) !== false) {
                echo join("|", $registro) . "\n";

                if ($centro_id != 9999 and trim($registro[0]) != trim($alias_reporte)) continue;

                $this->analiza($registro);
            }
            fclose($gestor);
        }
    }

    function analiza($r) {
        $meses         = array('ENERO'      => 1,
                               'FEBRERO'    => 2,
                               'MARZO'      => 3,
                               'ABRIL'      => 4,
                               'MAYO'       => 5,
                               'JUNIO'      => 6,
                               'JULIO'      => 7,
                               'AGOSTO'     => 8,
                               'SEPTIEMBRE' => 9,
                               'OCTUBRE'    => 10,
                               'NOVIEMBRE'  => 11,
                               'DICIEMBRE'  => 12);
        $anio          = 2011;
        $alias         = trim($r[0]);
        $registros     = $r[1];
        $inscripciones = $r[2];
        $cobranza      = $r[3];
        $periodo       = $r[4];
        $mes           = $meses[$periodo];
        $c             = new Criteria();
        $c->add(CentroPeer::ALIAS_REPORTE, $alias);

        $centro = CentroPeer::doSelectOne($c);

        if ($centro == null) {
            error_log("El centro no existe!!! $alias");
            return;
        }

        $centro_id        = $centro->getId();
        //Encontramos el numero de días del periodo del centro para sacar la meta diaria.
        $conn             = Propel::getConnection();
        $sql              = "select count(*) from reporte_metas where centro_id = $centro_id and date_part('month',fecha) = $mes and date_part('year',fecha) = 2011 and categoria_fija_id = 1";
        $res              = $conn->query($sql);
        $d                = $res->fetch();
        $dias             = 1;
        if (isset($d)) {
            $dias             = $d['count'];
        }

        if ($mes != 10) {
            $md_registros     = number_format($registros / $dias, 2, '.', '');
            $md_inscripciones = number_format($inscripciones / $dias, 2, '.', '');
            $md_cobranza      = number_format($cobranza / $dias, 2, '.', '');
        } else {
            error_log("Es octubre:" . $cobranza . "/" . "6");
            $md_registros     = number_format($registros / 6, 2, '.', '');
            $md_inscripciones = number_format($inscripciones / 6, 2, '.', '');
            $md_cobranza      = number_format($cobranza / 6, 2, '.', '');
        }
        //Obtenemos los registros de metas para este periodo y centro y los actualizamos con los valores de metas diarias obtenidas.
        $sql              = "select * from reporte_metas where centro_id = $centro_id and date_part('month',fecha) = $mes and date_part('year',fecha) = 2011";
        $res              = $conn->query($sql);
        while ($d                = $res->fetch()) {
            $rep              = new ReporteMetas();
            $rep->hydrate($d);

            echo "-----ANTES-----\n";
            echo join("|", $rep->toArray()) . "\n";

            if ($rep->getCategoriaFijaId() == 1) {
                $rep->setMetaDiaria($md_registros);
            } elseif ($rep->getCategoriaFijaId() == 2) {
                $rep->setMetaDiaria($md_inscripciones);
            } elseif ($rep->getCategoriaFijaId() == 3) {
                $rep->setMetaDiaria($md_cobranza);
            }

            echo "-----DESPUES-----\n";
            echo join("|", $rep->toArray()) . "\n\n";

            $rep->save();
            //exit;

        }
    }
}
