<?php
class SeguimientoMetasDumpTask extends sfPropelBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección', 'propel'),
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente',  'tolu2' ),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación', 'frontend'),
            new sfCommandOption('desde'      , null, sfCommandOption::PARAMETER_OPTIONAL, 'Fecha a partir de la cual se genera el reporte'),
            new sfCommandOption('hasta'      , null, sfCommandOption::PARAMETER_OPTIONAL, 'Fecha hasta la que se genera el reporte de la cual se genera el reporte'),
            new sfCommandOption('tgz'        , null, sfCommandOption::PARAMETER_OPTIONAL, 'Comprime el archivo', 0)
        ));

        $this->namespace           = 'mako';
        $this->name                = 'seguimiento-metas-dump';
        $this->briefDescription    = 'Genera el dump para el reporte de indicadores para la fase 3';
        $this->detailedDescription = <<<EOF
     [mako:add-cursos-moodlle|INFO] Genera el reporte de indicadores para la fase 3

      [./symfony mako:seguimiento-metas-dump --env=prod |INFO]
EOF;

    }
    protected function execute($arguments = array(), $options = array()) {
        $databaseManager =
        new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'])->getConnection();
        $centroId        = sfConfig::get('app_centro_actual_id');
        $centro          = CentroPeer::retrieveByPK($centroId);
        $reporte         = fopen("/tmp/reporte_indicadores_" . $centro->getAlias() . ".yml", "w");

        error_log('Creadndo el dump de de datos por dia');

        $criteriaDia = new Criteria();
        $criteriaDia->add(ReporteIndicadoresDiaPeer::CENTRO_ID, $centroId);
        if ($options['desde'] && $options['hasta']) {
            $criteriaDia->add(ReporteIndicadoresDiaPeer::FECHA, ReporteIndicadoresDiaPeer::FECHA . " BETWEEN  '" . $options['desde'] . "' AND '" . $options['hasta'] . "'", Criteria::CUSTOM);
        } else {
            $criteriaDia->add(ReporteIndicadoresDiaPeer::FECHA, date('Y-m-d H:i:s'), Criteria::LESS_THAN);
        }
        $reporteDia = array('ReporteIndicadoresDia'            => array());
        $flag       = 1;

        error_log($criteriaDia->toString());

        foreach (ReporteIndicadoresDiaPeer::doSelect($criteriaDia) as $datosDia) {
            $reporteDia['ReporteIndicadoresDia']['ReporteIndicadoresDia_' . $flag] = $datosDia->toArray();
            // Socios activos nuevos
            foreach ($datosDia->getReporteIndicadoresDiaSociosActivosNuevoss() as $activosNuevos) {
                $reporteDia['ReporteIndicadoresDiaSociosActivosNuevos']['ReporteIndicadoresDiaSociosActivosNuevos_' . $flag][] = $activosNuevos->toArray();
            }
            // Socios activos nuevos
            foreach ($datosDia->getReporteIndicadoresDiaSociosActivoss() as $activos) {
                $reporteDia['ReporteIndicadoresDiaSociosActivos']['ReporteIndicadoresDiaSociosActivos_' . $flag][] = $activos->toArray();
            }
            $flag++;
        }
        $yml = sfYaml::dump($reporteDia);
        fwrite($reporte, $yml);

        error_log('Creando el dump de de datos por dia y grupo');
        $criteriaDiaGrupo = new Criteria();
        $criteriaDiaGrupo->add(ReporteIndicadoresDiaGrupoPeer::CENTRO_ID, $centroId);
        if ($options['desde'] && $options['hasta']) {
            $criteriaDiaGrupo->add(ReporteIndicadoresDiaGrupoPeer::FECHA, ReporteIndicadoresDiaGrupoPeer::FECHA . " BETWEEN  '" . $options['desde'] . "' AND '" . $options['hasta'] . "'", Criteria::CUSTOM);
        } else {
            $criteriaDia->add(ReporteIndicadoresDiaGrupoPeer::FECHA, date('Y-m-d H:i:s'), Criteria::LESS_THAN);
        }
        $reporteDiaGrupo = array();
        $flag            = 1;
        error_log($criteriaDiaGrupo->toString());
        foreach (ReporteIndicadoresDiaGrupoPeer::doSelect($criteriaDiaGrupo) as $datosDiaGrupo) {

            $reporteDiaGrupo['ReporteIndicadoresDiaGrupo']['ReporteIndicadoresDiaGrupo_' . $flag] = $datosDiaGrupo->toArray();
            // Actividad programada
            foreach ($datosDiaGrupo->getReporteIndicadoresDiaGrupoAlumnosActividadProgramadas() as $actividadProgramada) {
                $reporteDiaGrupo['ReporteIndicadoresDiaGrupoAlumnosActividadProgramada']['ReporteIndicadoresDiaGrupoAlumnosActividadProgramada_' . $flag][] = $actividadProgramada->toArray();
            }
            // Alumnos activos
            foreach ($datosDiaGrupo->getReporteIndicadoresDiaGrupoAlumnosActivoss() as $activos) {
                $reporteDiaGrupo['ReporteIndicadoresDiaGrupoAlumnosActivos']['ReporteIndicadoresDiaGrupoAlumnosActivos_' . $flag][] = $activos->toArray();
            }
            // Alumnos Inscritos
            foreach ($datosDiaGrupo->getReporteIndicadoresDiaGrupoSociosInscritoss() as $inscritos) {
                $reporteDiaGrupo['ReporteIndicadoresDiaGrupoSociosInscritos']['ReporteIndicadoresDiaGrupoSociosInscritos_' . $flag][] = $inscritos->toArray();
            }
            // Alumnos Preinscritos
            //$datosDiaGrupo = ReporteIndicadoresDiaGrupoPeer::retrieveByPK($id, $centro_id);
            foreach ($datosDiaGrupo->getReporteIndicadoresDiaGrupoSociosPreinscritoss() as $preinscritos) {
                $reporteDiaGrupo['ReporteIndicadoresDiaGrupoSociosPreinscritos']['ReporteIndicadoresDiaGrupoSociosPreinscritos_' . $flag][] = $preinscritos->toArray();
            }
            $flag++;
        }
        $yml = sfYaml::dump($reporteDiaGrupo);
        fwrite($reporte, $yml);
        fclose($reporte);

        if ($options['tgz'] != 0) {
            // comprimimos el archivo
            exec("tar -czf /tmp/reporte_indicadores_" . $centro->getAlias() . ".tgz /tmp/reporte_indicadores_" . $centro->getAlias() . ".yml");
            // Borramos el YMl para no ocupar espacio
            exec("rm /tmp/reporte_indicadores_" . $centro->getAlias() . ".yml");
        }
    }
}
