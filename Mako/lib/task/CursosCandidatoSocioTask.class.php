<?php
class CursosCandidatoSocioTask extends sfPropelBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección' , 'propel'),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación', 'frontend'),
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'               ,  'tolu2' ),
            new sfCommandOption('socio_id'   , null, sfCommandOption::PARAMETER_REQUIRED, 'Id del socio')));

        $this->namespace           = 'mako';
        $this->name                = 'cursos-candidato-socio';
        $this->briefDescription    = 'Muestra los cursos a los que es candidato un socio';
        $this->detailedDescription = <<<EOF
 [mako:add-cursos-moodlle|INFO] Muestra los cursos a los que es candidato un socio

  [./symfony mako:ejecuta-sync --env=prod |INFO]
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'] ? $options['connection'] : null)->getConnection();

        $this->logSection('Buscando socio', '…');
        $this->logSection('', '…');

        $finalizados  = array();
        $preinscritos = array();

        $socio = SocioPeer::retrieveByPK($options['socio_id']);
        if ($socio) {
            $this->logSection('Busando grupos para:', $socio->getNombreCompleto());
            $this->logSection('Edad:', $socio->getEdad());

            $alumno = AlumnosPeer::retrieveByPK($socio->getId());
            if (!$alumno) {
                $alumno = new Alumnos();
                $alumno->setMatricula($socio->getId());
                $alumno->setSocioId($socio->getId());
                $alumno->save();
            }

            foreach ($alumno->getCursosFinalizadoss() as $terminado) {
                $finalizados[] = $terminado->getCursoId();
                $this->logSection('Finalizados ', $terminado->getGrupo()->getCurso()->getNombre() . ', ' . $terminado->getGrupo()->getId());
            }

            $this->logSection('Preinscritos', '');
            foreach ($alumno->getGrupoAlumnoss() as $preinscrito) {
                if (!in_array($preinscrito->getGrupo()->getCursoId(), $finalizados)) {
                    $preinscritos[] = $preinscrito->getGrupo()->getCursoId();
                    $this->logSection('Preinscrito ', $preinscrito->getGrupo()->getCurso()->getNombre());
                }
            }

            $this->logSection('', '…');
            $this->logSection('Buscando cursos activos', '...');

            $c = new Criteria();
            $c->add(GrupoPeer::CENTRO_ID, sfConfig::get('app_centro_actual_id'));
            $c->add(GrupoPeer::ACTIVO, true);
            $c->add(GrupoPeer::CUPO_ALCANZADO, false);
            $c->add(HorarioPeer::FECHA_INICIO, date('Y-m-d'), Criteria::GREATER_EQUAL);
            $c->addJoin(GrupoPeer::HORARIO_ID, HorarioPeer::ID);

            $grupos = GrupoPeer::doSelect($c);

            $this->analizarCandidatos($grupos, $finalizados, $preinscritos, $alumno);
            $this->logSection('', '…');
            $this->logSection('Buscando cursos con extension de pago', '...');

            $criteria = new Criteria();
            $criteria->add(GrupoPeer::CENTRO_ID, sfConfig::get('app_centro_actual_id'));
            $criteria->add(GrupoPeer::ACTIVO, true);
            $criteria->add(GrupoPeer::CUPO_ALCANZADO, false);
            $criteria->add(HorarioPeer::FECHA_INICIO, date('Y-m-d'), Criteria::LESS_THAN);
            $criteria->add(HorarioPeer::FECHA_FIN, date('Y-m-d'), Criteria::GREATER_THAN);
            $criteria->add(PlazoFechaCobroPeer::ACTIVO, true);
            $criteria->add(PlazoFechaCobroPeer::FECHA_FIN, date('Y-m-d'), Criteria::GREATER_EQUAL);
            $criteria->addJoin(GrupoPeer::HORARIO_ID, HorarioPeer::ID);
            $criteria->addJoin(GrupoPeer::ID, PlazoFechaCobroPeer::GRUPO_ID);

            $grupos = GrupoPeer::doSelect($criteria);

            $this->analizarCandidatos($grupos, $finalizados, $preinscritos, $alumno);
        }
    }

    protected function analizarCandidatos($grupos, $finalizados, $preinscritos, $alumno) {
        foreach ($grupos as $grupo) {
            $this->logSection('Validando candidato', 'Grupo: ' . $grupo->getCurso()->getNombre());
            $candidato = false;

            //Si el curso no esta dentro de los preincritos se procede a listar todos los cursos
            if (!in_array($grupo->getCursoId(), $preinscritos)) {
                //Revisamos que el curso no tenga restricciones
                if ($grupo->getCurso()->getRestriccionesSocio() === false) {
                    //Revisamos que no este seriado
                    if (!$grupo->getCurso()->getSeriado()) {
                        $candidato = true;

                    } else { //Si es seriado checamos la ruta de aprendizaje
                        $rutas = $grupo->getCurso()->getRutaAprendizajesRelatedByCursoId();
                        foreach ($rutas as $ruta) if (in_array($ruta->getPadreId(), $finalizados)) {
                            $candidato = true;
                        }
                    }
                } else {
                    $restricciones = $grupo->getCurso()->getRestriccionesSocios();
                    foreach ($restricciones as $restriccion) {
                        //Una bandera para contar cuantas restricciones no pasa
                        $flag = 0;

                        //Revisamos que cumpla edad minima
                        if ($restriccion->getEdadMinima()) {
                            if ($alumno->getSocio()->getEdad() < $restriccion->getEdadMinima()) {
                                $this->log('No cumple la edad minima');
                                $flag++;
                            }
                        }

                        //Revisamos que cumpla la edad maxima
                        if ($restriccion->getEdadMaxima()) {
                            if ($alumno->getSocio()->getEdad() > $restriccion->getEdadMaxima()) {
                                $this->log('No cumple la edad maxima');
                                $flag++;
                            }
                        }

                        //Revisamos que cumpla con el sexo
                        if ($restriccion->getSexo() != ' ') {
                            if ($alumno->getSocio()->getSexo() != $restriccion->getSexo()) {
                                $this->log('No cumple con el sexo');
                                $flag++;
                            }
                        }

                        //Revisamos que cumpla con la ocupacion
                        if ($restriccion->getOcupacionId()) {
                            if ($alumno->getSocio()->getOcupacionId() != $restriccion->getOcupacionId()) {
                                $this->log('No cumple con la ocupacion');
                                $flag++;
                            }
                        }

                        //Revisamos si estudia
                        if ($restriccion->getEstudia()) {
                            if ($alumno->getSocio()->getEstudia() != $restriccion->getEstudia()) {
                                $this->log('No cumple con la restriccion de estudiar');
                                $flag++;
                            }
                        }

                        //Revisamos el nivel de estudios
                        if ($restriccion->getNivelEstudioId()) {
                            if ($alumno->getSocio()->getNivelId() != $restriccion->getNivelEstudioId()) {
                                $this->log('No cumple con el nivel de estudios');
                                $flag++;
                            }
                        }

                        //Revisamos la habilidad informatica
                        if ($restriccion->getHabilidadInformaticaId()) {
                            if ($alumno->getSocio()->getHabilidadInformaticaId() != $restriccion->getHabilidadInformaticaId()) {
                                $this->log('No cumple con la habilidad informatica');
                                $flag++;
                            }
                        }

                        //Revisamos el dominio de ingles
                        if ($restriccion->getDominioInglesId()) {
                            if ($alumno->getSocio()->getDominioInglesId() != $restriccion->getDominioInglesId()) {
                                $this->log('No cumple con el dominio de ingles');
                                $flag++;
                            }
                        }

                        //Revisamos que la vandera siga en 0
                        if ($flag == 0) {
                            $candidato = true;
                        }
                    }
                }

                if ($candidato) {
                    $this->logSection('Candidato a :',
                                      $grupo->getClave()              . ' ->' .
                                      $grupo->getCurso()->getNombre() . ' -> ' .
                                      $grupo->getAula()->getNombre());
                } else {
                    $this->logSection('No Candidato a :',
                                      $grupo->getClave()              . ' ->' .
                                      $grupo->getCurso()->getNombre() . ' -> ' .
                                      $grupo->getAula()->getNombre());
                }
            } else {
                $this->logSection('No Candidato a :',
                                  $grupo->getClave()              . ' ->' .
                                  $grupo->getCurso()->getNombre() . ' -> ' .
                                  $grupo->getAula()->getNombre() . ' preinscrito actualmente');
            }
        }
    }
}
?>

