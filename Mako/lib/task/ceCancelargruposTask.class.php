<?php
class ceCancelargruposTask extends sfBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'               ,  'tolu2' ),
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección' , 'propel'),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación', 'frontend'),
        ));

        $this->namespace           = 'controlEscolar';
        $this->name                = 'cancelar-grupos';
        $this->briefDescription    = 'Cancela automáticamente los grupos sin socios inscritos.';
        $this->detailedDescription = <<<EOF
La task [controlEscolar:cancelar-grupos|INFO] cancela de manera automática los
grupos que para la fecha de inscripción no tengan socios inscritos.

Uso:
  [php symfony controlEscolar:cancelar-grupos|INFO]
EOF;
    }

    protected function execute($arguments = array() , $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'] ? $options['connection'] : null)->getConnection();
        $mail_from       = 'mako@enova.mx';
        $rol_mails       = array(
            6  => 'Administrador de centro',
            8  => 'Administrador de control escolar',
            11 => 'Coordinador académico'
        );

        $context = sfContext::createInstance($this->configuration);
        $this->configuration->loadHelpers('Partial');

        $c = new Criteria();
        $c->add(GrupoPeer::ACTIVO,true);
        $c->add(HorarioPeer::CENTRO_ID, sfConfig::get('app_centro_actual_id'));

        $time = strtotime('yesterday');
        $año  = date('Y', $time);
        $mes  = date('m', $time);
        $dia  = date('d', $time);

        $fec_inicio_prn = $c->getNewCriterion(HorarioPeer::FECHA_INICIO, $año.'-'.$mes.'-'.$dia.' 00:00:00', Criteria::GREATER_EQUAL);
        $fec_inicio_fin = $c->getNewCriterion(HorarioPeer::FECHA_INICIO, $año.'-'.$mes.'-'.$dia.' 23:59:59', Criteria::LESS_EQUAL);
        $fec_inicio_prn->addAnd($fec_inicio_fin);

        $c->add($fec_inicio_prn);
        $c->addJoin(HorarioPeer::ID, GrupoPeer::HORARIO_ID);
        $c->addJoin(CursoPeer::ID, GrupoPeer::CURSO_ID);
        $c->addAscendingOrderByColumn(HorarioPeer::FECHA_INICIO);

        $c_emails = new Criteria();
        foreach ($rol_mails as $rol => $name) {
            $c_emails->add(UsuarioPeer::ROL_ID, $rol);

            $usuarios = UsuarioPeer::doSelect($c_emails);

            if(!isset($emails[$name])) {
                $emails[$name] = array();
            }

            foreach ($usuarios as $usuario) {
                $emails[$name][$usuario->getNombre()] = $usuario->getEmail();
            }
        }

        $grupos = GrupoPeer::doSelect($c);
        if(count($grupos) == 0) {
            $this->logBlock('No hay grupos que cancelar.', 'ERROR');
            exit(0);
        }

        foreach ($grupos as $grupo) {
            $alumnos = GrupoPeer::getAlumnosInscritosYpreInscritos($grupo);

            if(($alumnos['inscritos'] == 0) and ($alumnos['preinscritos'] == 0) and (strtotime(GrupoPeer::getFechaFinalExtensionPago($grupo)) >= $time)) {
                if(GrupoPeer::cancelar($grupo)) {
                    $this->logBlock('Se cancelara el grupo "'.$grupo->getClave().'" por falta de socios inscritos ('.$alumnos['inscritos'].') o preinscritos ('.$alumnos['preinscritos'].').', 'INFO');

                    $emails['Facilitador'] = array(
                        $grupo->getUsuarioRelatedByFacilitadorId()->getNombre() => $grupo->getUsuarioRelatedByFacilitadorId()->getEmail()
                    );

                    $this->logBlock('Se enviará un mail para informar sobre la cancelación a: ', 'INFO');

                    foreach ($emails as $rol => $usuario) {
                        foreach ($usuario as $nombre => $email) {
                            //No mandes correos a las personas reales cuando estes haciendo pruebas!!!
                            if((sfConfig::get('sf_environment') !=  'tolu2' ) or (sfConfig::get('sf_environment') != 'test')) {
                                $email = 'arturo.bruno@enova.mx';
                            }

                            $tema      = 'Cancelación de grupo '.$grupo->getClave();
                            $data      = array(
                                'grupo'   => $grupo,
                                'alumnos' => $alumnos
                            );
                            $msg       = get_partial('control/mailCursoCancelado', $data);
                            $msg_plain = get_partial('control/mailCursoCanceladoPlano', $data);

                            $mail = $this->getMailer()->compose();
                            $mail->setSubject($tema);
                            $mail->setTo($email);
                            $mail->setFrom($mail_from);

                            $mail->setBody($msg, 'text/html');
                            $mail->addPart($msg_plain, 'text/plain');

                            if($this->getMailer()->send($mail)) {
                                $this->logBlock($rol.' '.$nombre.' <'.$email.'>');
                            } else {
                                $this->logBlock('No pudo enviarse el correo al '.strtolower($rol).' '.$nombre.' <'.$email.'>', 'ERROR');
                            }
                        }
                    }

                } else {
                    $this->logBlock('No fue posible cancelar el grupo "'.$grupo->getClave().'"', 'ERROR');
                }
            }
        }
    }
}
