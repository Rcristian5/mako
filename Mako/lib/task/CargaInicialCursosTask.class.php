<?php
class CargaInicialCursosTask extends sfPropelBaseTask
{
    protected function configure() {
        $this->addOptions(array(
            new sfCommandOption('env'        , null, sfCommandOption::PARAMETER_REQUIRED, 'El ambiente'               ,  'tolu2' ),
            new sfCommandOption('connection' , null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la conección' , 'propel'),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'El nombre de la aplicación', 'frontend')
        ));

        $this->namespace           = 'mako';
        $this->name                = 'carga-inicial-cursos';
        $this->briefDescription    = 'Carga inicial de cursos';
        $this->detailedDescription = <<<EOF
 [mako:add-cursos-moodlle|INFO] Toma todos los cursos registrados en Moodle y los
     carga en Mako
  [./symfony mako:ejecuta-sync --env=prod |INFO]
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection      = $databaseManager->getDatabase($options['connection'] ? $options['connection'] : null)->getConnection();

        //Se intenta conectar con moodle
        try {
            $cargados      = 0;
            $noEncontrados = 0;
            $repetidos     = 0;
            $relaciones    = 0;

            $this->logSection('Conectando con Moodle', '');

            $cliente = new SoapClient(sfConfig::get('app_soap_client'));

            //Obtenemos el token de sesion
            $sesion = $cliente->login(sfConfig::get('app_soap_user'), sfConfig::get('app_soap_password'));

            //Se obtinen todas las categorias de moodle
            $categoriasMoodle = $cliente->get_categories($sesion->client, $sesion->sessionkey);

            //Obtenemos las categorias de Mako y creamos un arreglo
            $categoriasMako = array();
            foreach (CategoriaCursoPeer::doSelect(new Criteria()) as $categoriaMako) {
                $categoriasMako[$categoriaMako->getId()] = $categoriaMako->getNombre();
            }

            $categoriasParent = array();
            foreach ($categoriasMoodle->categories as $categoria) {
                $path = explode('/', $categoria->path);

                //$this->logSection('Categorias limpias',  var_dump($path));
                if (count($path) > 1) {
                    $categoriasParent[$categoria->id] = $path[1];
                }
            }

            //Se crea un arreglo con id de la categoria y Nombre
            $categorias = array();
            foreach ($categoriasMoodle->categories as $categoria) {
                $key = array_search($categoria->name, $categoriasMako);
                if ($key) {
                    $categorias[$categoria->id] = $key;
                }
            }

            foreach ($categoriasParent as $key => $padres) {
                if ($categorias[$padres]) {
                    $categorias[$key] = $categorias[$padres];
                }
            }

            //Se obtienen todos los cursos de Moodle
            $cursosMoodle   = $cliente->get_courses($sesion->client, $sesion->sessionkey);
            $relacionesFail = array();

            //Se insertan los cursos en Mako
            foreach ($cursosMoodle->courses as $curso) {
                $c = new Criteria();
                $c->add(ProductoPeer::CODIGO, $curso->idnumber);

                $producto = ProductoPeer::doSelectOne($c);
                if ($producto) {
                    if (!CursoPeer::buscarClave($curso->idnumber)) {
                        if ($categorias[$curso->category]) {
                            $this->logSection('Guardadno curso:', $curso->fullname);

                            //Obtenemos de nuevo la categoria del curso para
                            //deterinar el perfil del facilitadro
                            $category = CategoriaCursoPeer::retrieveByPK($categorias[$curso->category]);
                            $prefil   = ($category->getNombre() ? 2 : 1);
                            $course   = new Curso();
                            $course->setCategoriaCursoId($categorias[$curso->category]);
                            $course->setClave($curso->idnumber);
                            $course->setNombre($curso->fullname);
                            $course->setDescripcion($curso->fullname);
                            $course->setPerfilId($prefil);
                            $course->setDuracion(($producto->getTiempo() / 60) / 60);
                            $course->setAsociadoMoodle(true);
                            $course->save();

                            //sfProjectConfiguration::getActive()->getEventDispatcher()->notify(new sfEvent($course, 'curso.publicado', array( 'curso' => $course )));

                            $cargados++;
                            //Se intenta cargar la relacion ProductoCurso
                            try {
                                $ProductoCurso = new ProductoCurso();
                                $ProductoCurso->setCursoId($course->getId());
                                $ProductoCurso->setProductoId($producto->getId());
                                $ProductoCurso->save();
                            }
                            catch(Exception $e) {
                                error_log($e->getMessage());
                                $relaciones++;
                                $relacionesFail[] = array('curso' => $course->getId(), 'producto' => $producto->getId());
                                $this->logSection('No se pudo crear la relación ProductoCurso ', '');
                            }
                        } else {
                            $this->logSection('No se encontro la categoria:', $curso->fullname);
                            $noEncontrados++;
                        }
                    } else {
                        $this->logSection('El curso ya esta registrado en Mako:', $curso->fullname);
                        $repetidos++;
                    }
                } else {
                    $this->logSection('No se encontro el producto:', $curso->fullname);
                    $noEncontrados++;
                }
            }
        }
        catch(Exception $e) {
            $this->logSection('Fallo la conección con Moodle', '');
        }

        $this->logSection('', '');
        $this->logSection('Total de cursos en Moodle: ', count($cursosMoodle->courses));
        $this->logSection('Total de cursos cargados en Mako: ', $cargados);
        $this->logSection('Total de cursos no encontrados: ', $noEncontrados);
        $this->logSection('Total de cursos ya regitrados: ', $repetidos);
        $this->logSection('', '');
        $this->logSection('Relaciones ProductoCurso no creadas', $relaciones);

        if ($relaciones > 0) {
            $this->logSection('Detalle de relaciones ProductoCurso no creadas', '');
            foreach ($relacionesFail as $relacion) {
                $this->logSection('Relación fallida', 'curso: ' . $relacion['curso'] . ', producto: ' . $relacion['producto']);
            }
        }

        $this->logSection('Fin de la carga', '');
    }
}
?>
