<?php

/**
 * Regresa la cadena formateada de productos para impresión de ticket
 * @param Orden $orden
 * @return string
 */

/*
 * POS
*/
//Inscripciones Cant - Global
function estadisticaPOSInscripciones($inicio=NULL, $fin=NULL)
{

	if($inicio != NULL)
		$whr = " WHERE created_at BETWEEN '$inicio' AND '$fin'";

	$conn = Propel::getConnection();
	$sql = "SELECT 'inscripciones' as label,
	created_at
	FROM socio
	$whr";

	return $conn->query($sql);;
}

//Inscripciones por Curso / Paquete
function estadisticaPOSInscripcionesCurso($inicio=NULL, $fin=NULL)
{

	if($inicio != NULL)
		$whr = " AND o.created_at BETWEEN '$inicio' AND '$fin'";

	$conn = Propel::getConnection();
	$sql = "SELECT 'inscripciones por curso' as label,
	o.created_at
	FROM detalle_orden as d,
	producto as p,
	orden as o
	WHERE d.producto_id = p.id
	AND p.categoria_id = 2
	AND d.orden_id = o.id
	$whr";

	return $conn->query($sql);;
}

//Ventas por categoria
function estadisticaPOSVentasCategoria($inicio=NULL, $fin=NULL)
{

	if($inicio != NULL)
		$whr = " AND o.created_at BETWEEN '$inicio' AND '$fin'";

	$conn = Propel::getConnection();
	$sql = "SELECT cp.nombre as label,
	SUM(d.precio_lista * d.cantidad) AS cuenta,
	o.created_at
	FROM orden as o,
	detalle_orden as d,
	producto AS p,
	categoria_producto as cp
	WHERE d.producto_id = p.id
	AND p.categoria_id = cp.id
	AND o.id = d.orden_id
	$whr
	GROUP BY cp.nombre,
	o.created_at
	";

	$datos = $conn->query($sql);

	return $datos;

}


//Socio

function estadisticaSociosGenero($inicio=NULL, $fin=NULL)
{

	if($inicio != NULL)
		$whr = " WHERE created_at BETWEEN '$inicio' AND '$fin'";

	$conn = Propel::getConnection();
	$sql = "SELECT CASE WHEN sexo = 'F'
	THEN 'Femenino'
	ELSE 'Masculino'
	END AS label,
	created_at
	FROM socio
	$whr";

	return $conn->query($sql);;
}


function estadisticaSociosEdad($inicio=NULL, $fin=NULL)
{
	if($inicio != NULL)
		$whr = " WHERE created_at BETWEEN '$inicio' AND '$fin'";

	$conn = Propel::getConnection();
	$sql = "SELECT fecha_nac,
	created_at
	FROM socio
	$whr";

	return $conn->query($sql);;
}

function estadisticaSociosEscolaridad($inicio=NULL, $fin=NULL)
{
	if($inicio != NULL)
		$whr = " AND s.created_at BETWEEN '$inicio' AND '$fin'";

	$conn = Propel::getConnection();
	$sql = "SELECT e.nombre as label,
	s.created_at
	FROM socio as s,
	nivel_estudio as e
	WHERE
	s.nivel_id = e.id
	$whr";

	return $conn->query($sql);;
}


function estadisticaSociosColonia($inicio=NULL, $fin=NULL)
{
	if($inicio != NULL)
		$whr = " WHERE created_at BETWEEN '$inicio' AND '$fin'";

	$conn = Propel::getConnection();
	$sql = "SELECT trim(colonia) as label,
	created_at
	FROM socio
	$whr";

	return  $conn->query($sql);;

}

function estadisticaSociosEmpresa($inicio=NULL, $fin=NULL)
{
	if($inicio != NULL)
		$whr = "AND s.created_at BETWEEN '$inicio' AND '$fin'";

	$conn = Propel::getConnection();
	$sql = "SELECT trim(e.nombre) as label,
	s.created_at
	FROM socio as s,
	empresa as e
	WHERE s.empresa = e.id
	$whr";

	return $conn->query($sql);;


}


//Servicio


function estadisticaServicio($inicio=NULL, $fin=NULL)
{

	if($inicio != NULL)
		$whr = " AND orden.created_at BETWEEN '$inicio' AND '$fin'";

	$conn = Propel::getConnection();
	$sql = "SELECT p.nombre as label,
	orden.created_at
	FROM producto as p,
	detalle_orden as o,
	orden
	WHERE o.producto_id = p.id
	AND orden.id = o.orden_id
	AND p.tipo_id = 3
	$whr ";

	return $conn->query($sql);;


}


function rangoEdad($estadisticaSociosEdad){

	$edad = array();
	$rangoEdad = array();
	foreach ($estadisticaSociosEdad as $row)
	{
		$edad[] = Comun::getEdad($row['fecha_nac']);
		$edadC[Comun::getEdad($row['fecha_nac'])][] = $row['created_at'];
	}

	$rangoEdad = (array_count_values($edad));
	//6 a 8
	$edadL[0] = '6 a 8';
	$rangoEdad6a8 = range(0,8);
	foreach ($rangoEdad6a8 as $llave => $valor){

		if(array_key_exists($valor, $rangoEdad)){
			$edadV[0] +=  $rangoEdad[$valor];
			foreach($edadC[$valor] as $creacion){
				$edadF[$edadL[0]][]=$creacion;
			}
		}
		else $edadV[0] +=  0;
	}

	// 9 a 12
	$edadL[1] = '9 a 12';
	$rangoEdad9a12 = range(9,12);
	foreach ($rangoEdad9a12 as $llave => $valor){

		if(array_key_exists($valor, $rangoEdad)){
			$edadV[1] +=  $rangoEdad[$valor];
			foreach($edadC[$valor] as $creacion){
				$edadF[$edadL[1]][]=$creacion;
			}
		}
		else $edadV[1] +=  0;
	}
		

	// 13 a 15
	$edadL[2] = '13 a 15';
	$rangoEdad13a15 = range(13,15);
	foreach ($rangoEdad13a15 as $llave => $valor){

		if(array_key_exists($valor, $rangoEdad)){
			$edadV[2] +=  $rangoEdad[$valor];
			foreach($edadC[$valor] as $creacion){
				$edadF[$edadL[2]][]=$creacion;
			}
		}
		else $edadV[2] +=  0;
	}
		
	// 16 a 25
	$edadL[3] = '16 a 25';
	$rangoEdad16a25 = range(16,25);
	foreach ($rangoEdad16a25 as $llave => $valor){

		if(array_key_exists($valor, $rangoEdad)){
			$edadV[3] +=  $rangoEdad[$valor];
			foreach($edadC[$valor] as $creacion){
				$edadF[$edadL[3]][]=$creacion;
			}
		}
		else $edadV[3] +=  0;
	}
		
	// 26 a 35
	$edadL[4] = '26 a 35';
	$rangoEdad26a35 = range(26,35);
	foreach ($rangoEdad26a35 as $llave => $valor){
		if(array_key_exists($valor, $rangoEdad)){
			$edadV[4] +=  $rangoEdad[$valor];
			foreach($edadC[$valor] as $creacion){
				$edadF[$edadL[4]][]=$creacion;
			}
		}
		else $edadV[4] +=  0;
	}
		
	// 35 en adelante
	$edadL[5] = '35 en adelante';
	$rangoEdad36 = range(36,105);
	foreach ($rangoEdad36 as $llave => $valor){

		if(array_key_exists($valor, $rangoEdad)){
			$edadV[5] +=  $rangoEdad[$valor];
			foreach($edadC[$valor] as $creacion){
				$edadF[$edadL[5]][]=$creacion;
			}
		}
		else $edadV[5] +=  0;
	}


	return array ($edadV,$edadL, $edadF) ;


}


/**
 *
 * @param $arregloQuery
 * @param $titulo
 * @param $arRangos
 * @param $arDias
 * @return XML
 */


function generaXML($arregloQuery, $titulo, $arRangos , $arDias){

	foreach ($arregloQuery as $row)
	{
		$cuentaAr[$row['label']][] = strftime("%Y-%m-%d", strtotime($row['created_at']));
		$labelAr[$row['label']] = $row['label'];
		if(!empty($row['cuenta'])){
			$labelCuenta[$row['label']][strftime("%Y-%m-%d", strtotime($row['created_at']))] = $row['cuenta'];
		}
		$i++;
	}

	$xml = "<chart caption='$titulo' showPercentageValues='1' showBorder ='1' useRoundEdges='1' labelDisplay='ROTATE' slantLabels='1' showValues='0'>";
	$xml .= "<categories>";
	for ($i = 0; $i < count($arRangos); $i++){
		$xml.= "<category label='".$arRangos[$i]."' />";
	}
	$xml.= "</categories>";
	foreach ($labelAr as $label){
		$cuenta = 0;
		$xml .= "<dataset seriesName='$label'>";
		for ($i = 0; $i < count($arRangos); $i++){
			foreach($arDias as $dias){
				if($dias >= $arRangos[$i] & $dias < $arRangos[$i+1])
					if(array_keys($cuentaAr[$label], $dias)){
					if(!empty($labelCuenta[$label][$dias])){
						$cuenta = $cuenta+$labelCuenta[$label][$dias];
					}else{
						$cuenta = $cuenta+ count(array_keys($cuentaAr[$label], $dias));
					}

				}
			}
			$xml.= "<set value='" .$cuenta."' />";
		}
		$xml.= "</dataset>";
	}
		
	$xml.= "</chart>";

	return $xml;


}


function generaTabla($arregloQuery, $titulo, $arRangos , $arDias){


	foreach ($arregloQuery as $row)
	{
		$cuentaAr[$row['label']][] = strftime("%Y-%m-%d", strtotime($row['created_at']));
		$labelAr[$row['label']] = $row['label'];
		if(!empty($row['cuenta'])){
			$labelCuenta[$row['label']][strftime("%Y-%m-%d", strtotime($row['created_at']))] = $row['cuenta'];
		}
		$i++;
	}

	$tabla = "<thead>";
	$tabla .= "<tr>";
	$tabla.= "<th class=\"ui-state-default\">Detalle</th>";
	for ($i = 0; $i < count($arRangos); $i++){
		$tabla.= "<th class=\"ui-state-default\">".$arRangos[$i]."</th>";
	}
	$tabla.= "</tr>
	</thead>
	<tbody>";

	foreach ($labelAr as $label){
		$cuenta = 0;
		$tabla .= "<tr>";
		$tabla.= " <td>" .$label."</td>";
		for ($i = 0; $i < count($arRangos); $i++){
			//se resetea la cuenta por día, en la tabla no es acumulativo
			$cuenta = 0;
			foreach($arDias as $dias){
				if($dias >= $arRangos[$i] & $dias < $arRangos[$i+1])
					if(array_keys($cuentaAr[$label], $dias)){
					if(!empty($labelCuenta[$label][$dias])){
						$cuenta = $cuenta+$labelCuenta[$label][$dias];
					}else{
						$cuenta = $cuenta+ count(array_keys($cuentaAr[$label], $dias));
					}
				}
			}
			$tabla.= " <td>" .$cuenta."</td>";
		}
	}
		
	$tabla.= "</tr>
	</tbody>
	";

	return $tabla;


}
?>