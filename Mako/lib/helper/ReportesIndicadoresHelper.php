<?php
/**
 * Funciones helper para elaborar los reportes de indicadores
 */


/**
 * Suma la ocupación de centros por hora y minutos consumidos
 * NOTA: Si existen sesiones después de la hora de cierre declarada en la configuración del centro no se toman en cuenta.
 * Esta función debe correr en centro.
 * @param string $fecha Fecha a reportar
 * @param integer $centro_id centro a reportar
 */
function ocupacionHoras($fecha,$centro_id)
{

	$conn = Propel::getConnection();
	//Primero sacamos los datos de hora de apertura y horas laborables en el centro
	$sql =" SELECT hora_apertura,pcs_usuario,
	EXTRACT(epoch FROM (hora_cierre -hora_apertura)/3600) as horas_abierto  FROM centro AS c
	WHERE
	id = $centro_id";


	//$sql = "SELECT * from usuario";
	$datos = $conn->query($sql);
	$datos->setFetchMode(PDO::FETCH_ASSOC);
	foreach($datos->fetchAll() as $row)
	{
		//print_r($row);
		$horas_abierto = $row['horas_abierto'];
		$hora_apertura = $row['hora_apertura'];
		$pcs_usuario = $row['pcs_usuario'];
	}


	//Para cada hora abierto realizamos la consulta de la tabla de sesion_historico
	for($hora=0; $hora < $horas_abierto; $hora++)
	{
		$ts1 = date("Y-m-d H:i:s",strtotime("$hora_apertura + $hora hour" ,strtotime($fecha)));
		$ts2 = date("Y-m-d H:i:s",strtotime("$hora_apertura + $hora hour + 1 hour" ,strtotime($fecha)));
		//echo "Evaluando: $ts1 a $ts2 \n";
		$sql = "
		SELECT (EXTRACT(epoch FROM sum(consumo) )/60)::numeric as consumo_minutos, sum(consumo) AS consumo, count(distinct(usuario)) as sesiones, hora as fecha, capacidad_total, capacidad_hora, horas_apertura FROM
		(
		SELECT usuario, h.ip, fecha_login, fecha_logout, ('$fecha'::timestamp + hora_apertura::interval + '$hora hours'::interval)::timestamp as hora,
		Case
		WHEN
		('$fecha'::timestamp + hora_apertura::interval + '$hora hours'::interval)::timestamp > fecha_login and (('$fecha'::timestamp + hora_apertura::interval + '$hora hours' +' 1 hours'::interval)::timestamp < fecha_logout)
		THEN
		('$fecha'::timestamp + hora_apertura::interval + '$hora hours' + ' 1 hours'::interval)::timestamp - ('$fecha'::timestamp + hora_apertura::interval + '$hora hours' + '1 hours'::interval)::timestamp
		WHEN
		(('$fecha'::timestamp + hora_apertura::interval + '$hora hours'::interval)::timestamp <= fecha_login and ('$fecha'::timestamp + hora_apertura::interval + '$hora hours' + '1 hours'::interval)::timestamp >= fecha_logout)
		THEN
		fecha_logout-fecha_login
		WHEN
		(('$fecha'::timestamp + hora_apertura::interval + '$hora hours'::interval)::timestamp >= fecha_login and ('$fecha'::timestamp + hora_apertura::interval + '$hora hours' + '1 hours'::interval)::timestamp > fecha_logout)
		THEN
		fecha_logout - ('$fecha'::timestamp + hora_apertura::interval + '$hora hours'::interval)::timestamp
		WHEN
		(('$fecha'::timestamp + hora_apertura::interval + '$hora hours'::interval)::timestamp < fecha_login and ('$fecha'::timestamp + hora_apertura::interval + '$hora hours' + '1 hours'::interval)::timestamp <= fecha_logout)
		THEN
		('$fecha'::timestamp + hora_apertura::interval + '$hora hours' + '1 hours'::interval)::timestamp -fecha_login

		else null
		END AS consumo,

		Case
		WHEN
		(('$fecha'::timestamp + hora_apertura::interval + '$hora hours'::interval)::timestamp > fecha_login and ('$fecha'::timestamp + hora_apertura::interval + '$hora hours' + '1 hours'::interval)::timestamp < fecha_logout)
		THEN
		'CASO 1'
		WHEN
		(('$fecha'::timestamp + hora_apertura::interval + '$hora hours'::interval)::timestamp <= fecha_login and ('$fecha'::timestamp + hora_apertura::interval + '$hora hours' + '1 hours'::interval)::timestamp >= fecha_logout)
		THEN
		'CASO 2'
		WHEN
		(('$fecha'::timestamp + hora_apertura::interval + '$hora hours'::interval)::timestamp >= fecha_login and ('$fecha'::timestamp + hora_apertura::interval + '$hora hours' + '1 hours'::interval)::timestamp > fecha_logout)
		THEN
		'CASO 3'
		WHEN
		(('$fecha'::timestamp + hora_apertura::interval + '$hora hours'::interval)::timestamp < fecha_login and ('$fecha'::timestamp + hora_apertura::interval + '$hora hours' + '1 hours'::interval)::timestamp <= fecha_logout)
		THEN
		'CASO 4'

		else 'OTRO'
		END AS caso,
		c.pcs_usuario,
		EXTRACT(epoch FROM (hora_cierre -hora_apertura))/60 as mins_abierto,
		EXTRACT(epoch FROM (hora_cierre -hora_apertura))/60 * c.pcs_usuario as capacidad_total,
		60 * c.pcs_usuario as capacidad_hora,
		EXTRACT(epoch FROM (hora_cierre -hora_apertura))/3600 as horas_apertura

		from sesion_historico as h
		join centro as c on (c.id = h.centro_id)
		where
		c.id = $centro_id
		AND
		(fecha_login,fecha_logout)
		overlaps
		(('$fecha'::timestamp + hora_apertura::interval + '$hora hours'::interval)::timestamp, ('$fecha'::timestamp + hora_apertura::interval + '$hora hours' + '1 hours'::interval)::timestamp)
		) AS x

		group by hora,capacidad_total,capacidad_hora,horas_apertura
		";
		//echo $sql;
		$datos = $conn->query($sql);
		$datos->setFetchMode(PDO::FETCH_ASSOC);
		$hubo = false;
		foreach($datos->fetchAll() as $row)
		{
			$hubo = true;
			//print_r($row);
			list($f,$h) = explode(" ", $row['fecha']);
			$oh = new OcupacionHoras();
			$oh->setId(Comun::generaId());
			$oh->setCentroId($centro_id);
			$oh->setFechaHora($row['fecha']);
			$oh->setFecha($f);
			$oh->setHora($h);
			$oh->setConsumo($row['consumo_minutos']);
			$oh->setSesiones($row['sesiones']);
			$oh->setCapacidadHora($row['capacidad_hora']);
			$oh->save();
		}

		if(!$hubo) //Si no hubo datos para esa hora, entonces sintetizamos el registro manualmente
		{
			list($f,$h) = explode(" ", $ts1);
			$oh = new OcupacionHoras();
			$oh->setId(Comun::generaId());
			$oh->setCentroId($centro_id);
			$oh->setFechaHora($ts1);
			$oh->setFecha($f);
			$oh->setHora($h);
			$oh->setConsumo(0);
			$oh->setSesiones(0);
			$oh->setCapacidadHora($pcs_usuario*60);
			$oh->save();
		}


	}

	return true;
}

/**
 * Construye los datos para el reporte de estatus de socios en centro
 *
 * @param string $fecha fecha a construir
 * @param integer $centro_id Identificador del centro
 */
function reporteEstatusSocios($fecha,$centro_id)
{
	$conn = Propel::getConnection();
	//Registros nuevos.
	$sql = "
	SELECT
	count(*) as registros
	FROM
	socio
	WHERE
	created_at::date = '$fecha'
	AND centro_id = $centro_id
	";
	$res = $conn->query($sql);
	$d = $res->fetch();
	$nuevos = 0;
	if(isset($d))
	{
		$nuevos = $d['registros'];
	}

	//Socios activos. Se define como socios activos a los que han tenido algun registro en sesion_historico o en orden apartir de la fecha 2 semanas hacia atras.
	$sql="
	SELECT count(*) AS activos FROM
	(
	SELECT
	distinct(socio_id)
	FROM
	sesion_historico
	WHERE
	fecha_login::date BETWEEN '$fecha'::date + '-4 weeks'::interval AND '$fecha'
	AND centro_id = $centro_id

	UNION

	SELECT
	distinct(socio_id)
	FROM
	orden
	WHERE
	tipo_id IN (1,3)  --Orden de tipo ventas y cancelaciones solamente
	AND created_at::date BETWEEN '$fecha'::date + '-4 weeks'::interval AND '$fecha'
	AND centro_id = $centro_id
	) as x
	";
	$res = $conn->query($sql);
	$d = $res->fetch();
	$activos = 0;
	if(isset($d))
	{
		$activos = $d['activos'];
	}

	$reg = new EstatusSocios();
	$reg->setFecha($fecha);
	$reg->setCentroId($centro_id);
	$reg->setNuevos($nuevos);
	$reg->setActivos($activos);
	$reg->save();
	return true;
}

/**
 * Devuelve los registros del indicador de tasa de ocupacion
 *
 * @param string $fI
 * @param string $fF
 * @param integer $centro_id
 * @param integer $zona_id
 * @param integer $nivel
 * @param float $ponderacion
 */
function tasaOcupacion($fI,$fF,$centro_id,$zona_id,$nivel,$ponderacion, $hora_ini, $hora_fin)
{
	if($zona_id != 0)
	{
		$zona = ZonaPeer::retrieveByPK($zona_id);
	}
	if($centro_id != 0)
	{
		$centro = CentroPeer::retrieveByPK($centro_id);
	}
	if($nivel == 0) {
		$sqNivel = "date_trunc('month',fecha )::date as mes,";
	}
	if($nivel == 1) {
		$sqNivel = "date_trunc('week',fecha )::date as mes,";
	}
	if($nivel == 2) {
		$sqNivel = "date_trunc('day',fecha )::date as mes,";
	}

	if($hora_ini != 0 && $hora_fin != 0) {
		$sqHr = " AND date_part('hour',fecha_hora) between $hora_ini AND $hora_fin";
	}
	error_log("$hora_ini a $hora_fin");
	if($fI == '' or $fI == null) $fI = '2010-09-01';
	if($fF == '' or $fF == null) $fF = date("Y-m-d");

	if($zona_id == 0 && $centro_id == 0)
	{
		$sql = "
		SELECT
		$sqNivel
		SUM(consumo) AS minutos,
		SUM(sesiones) AS sesiones,
		SUM(capacidad_hora) AS capacidad_hora
		FROM ocupacion_horas AS o
		LEFT JOIN centro AS c ON (c.id = o.centro_id)
		WHERE fecha BETWEEN '$fI' AND '$fF'
		$sqHr
		GROUP BY mes
		ORDER BY mes";
		$ns1 = 'Tasa Global';
			
	}
	elseif($zona_id != 0 && $centro_id == 0)
	{
		$sql = "
		SELECT
		$sqNivel
		SUM(consumo) AS minutos,
		SUM(sesiones) AS sesiones,
		SUM(capacidad_hora) AS capacidad_hora
		FROM ocupacion_horas AS o
		LEFT JOIN centro AS c ON (c.id = o.centro_id)
		WHERE fecha BETWEEN '$fI' AND '$fF'
		$sqHr
		AND zona_id = $zona_id
		GROUP BY mes
		ORDER BY mes";

		$ns1 = $zona.' ';
		//$ns2 = $zona.' Meta';
	}
	elseif($zona_id == 0 && $centro_id != 0)
	{
		$sql = "
		SELECT
		$sqNivel
		SUM(consumo) AS minutos,
		SUM(sesiones) AS sesiones,
		SUM(capacidad_hora) AS capacidad_hora
		FROM ocupacion_horas AS o
		LEFT JOIN centro AS c ON (c.id = o.centro_id)
		WHERE fecha BETWEEN '$fI' AND '$fF'
		$sqHr
		AND centro_id = $centro_id
		GROUP BY mes
		ORDER BY mes";

		$ns1 = $centro->getAliasReporte().'';
		//$ns2 = $centro->getAliasReporte().' Meta';
	}
	error_log($sql);

	$conn = Propel::getConnection();
	$d = array();

	foreach($conn->query($sql) as $r)
	{

		$minutos = floatval($r['minutos']);
		$unixtime = strtotime($r['mes']);
		$jsunixtime=$unixtime*1000;
			
		$capacidad = floatval($r['capacidad_hora']);
			
		$d[$ns1][] = array(
				$jsunixtime,
				($ponderacion == 0) ? $minutos/$capacidad : $minutos/($capacidad*$ponderacion)
		);
	}
	return $d;
}

/**
 * Devuelve los registros del indicador de ocupacion
 *
 * @param $fI
 * @param $fF
 * @param $centro_id
 * @param $zona_id
 * @param $nivel
 */
function ocupacion($fI,$fF,$centro_id,$zona_id,$nivel)
{
	if($zona_id != 0)
	{
		$zona = ZonaPeer::retrieveByPK($zona_id);
	}
	if($centro_id != 0)
	{
		$centro = CentroPeer::retrieveByPK($centro_id);
	}

	if($nivel == 0) {
		$sqNivel = "date_trunc('month',fecha )::date as mes,";
	}
	if($nivel == 1) {
		$sqNivel = "date_trunc('week',fecha )::date as mes,";
	}
	if($nivel == 2) {
		$sqNivel = "date_trunc('day',fecha )::date as mes,";
	}

	if($fI == '' or $fI == null) $fI = '2010-09-01';
	if($fF == '' or $fF == null) $fF = date("Y-m-d");
	if($zona_id == 0 && $centro_id == 0)
	{
		$sql = "
		SELECT
		$sqNivel
		SUM(consumo) AS minutos,
		SUM(sesiones) AS sesiones,
		SUM(capacidad_hora) AS capacidad_hora
		FROM ocupacion_horas AS o
		LEFT JOIN centro AS c ON (c.id = o.centro_id)
		WHERE fecha BETWEEN '$fI' AND '$fF'
		GROUP BY mes
		ORDER BY mes";
		$ns1 = 'Minutos Consumidos';
		$ns2 = 'Capacidad Total';
	}
	elseif($zona_id != 0 && $centro_id == 0)
	{
		$sql = "
		SELECT
		$sqNivel
		SUM(consumo) AS minutos,
		SUM(sesiones) AS sesiones,
		SUM(capacidad_hora) AS capacidad_hora
		FROM ocupacion_horas AS o
		LEFT JOIN centro AS c ON (c.id = o.centro_id)
		WHERE fecha BETWEEN '$fI' AND '$fF'
		AND zona_id = $zona_id
		GROUP BY mes
		ORDER BY mes";

		$ns1 = $zona.'';
		$ns2 = $zona.' Capacidad';
	}
	elseif($zona_id == 0 && $centro_id != 0)
	{
		$sql = "SELECT
		$sqNivel
		SUM(consumo) AS minutos,
		SUM(sesiones) AS sesiones,
		SUM(capacidad_hora) AS capacidad_hora
		FROM ocupacion_horas AS o
		LEFT JOIN centro AS c ON (c.id = o.centro_id)
		WHERE fecha BETWEEN '$fI' AND '$fF'
		AND centro_id = $centro_id
		GROUP BY mes
		ORDER BY mes";

		$ns1 = $centro->getAliasReporte().'';
		$ns2 = $centro->getAliasReporte().' Capacidad';
	}
	error_log($sql);

	$conn = Propel::getConnection();
	$d = array();

	foreach($conn->query($sql) as $r)
	{

		$minutos = floatval($r['minutos']);
		$unixtime = strtotime($r['mes']);
		$jsunixtime=$unixtime*1000;
			
		$capacidad = floatval($r['capacidad_hora']);
			
		$d[$ns1][] = array(
				$jsunixtime,
				$minutos
		);
		$d[$ns2][] = array(
				$jsunixtime,
				$capacidad
		);
	}
	return $d;
}

/**
 * Devuelve los registros del indicador de ocupacion por horas
 *
 * @param $fI
 * @param $fF
 * @param $centro_id
 * @param $zona_id
 * @param $nivel
 */
function ocupacionPorHora($fI,$fF,$centro_id,$zona_id,$nivel)
{
	if($zona_id != 0)
	{
		$zona = ZonaPeer::retrieveByPK($zona_id);
	}
	if($centro_id != 0)
	{
		$centro = CentroPeer::retrieveByPK($centro_id);
	}

	if($fI == '' or $fI == null) $fI = '2010-09-01';
	if($fF == '' or $fF == null) $fF = date("Y-m-d");

	if($nivel == 0) {
		$sqNivel = "date_trunc('month',fecha )::date as mes,";
	}
	if($nivel == 1) {
		$sqNivel = "date_trunc('week',fecha )::date as mes,";
	}
	if($nivel == 2) {
		$sqNivel = "date_trunc('day',fecha )::date as mes,";
	}

	if($zona_id == 0 && $centro_id == 0)
	{
		$sql = "
		SELECT
		$sqNivel
		date_trunc('hour',hora)::TIME AS hora,
		SUM(consumo) AS minutos,
		SUM(sesiones) AS sesiones,
		SUM(capacidad_hora) AS capacidad_hora
		FROM ocupacion_horas AS o
		LEFT JOIN centro AS c ON (c.id = o.centro_id)
		WHERE fecha BETWEEN '$fI' AND '$fF'
		GROUP BY mes,hora
		ORDER BY mes,hora";
		$ns1 = '';
		//$ns2 = 'Capacidad Total';
	}
	elseif($zona_id != 0 && $centro_id == 0)
	{
		$sql = "
		SELECT
		$sqNivel
		date_trunc('hour',hora)::TIME AS hora,
		SUM(consumo) AS minutos,
		SUM(sesiones) AS sesiones,
		SUM(capacidad_hora) AS capacidad_hora
		FROM ocupacion_horas AS o
		LEFT JOIN centro AS c ON (c.id = o.centro_id)
		WHERE fecha BETWEEN '$fI' AND '$fF'
		AND zona_id = $zona_id
		GROUP BY mes,hora
		ORDER BY mes,hora";

		$ns1 = $zona.'';
		//$ns2 = $zona.' Meta';
	}
	elseif($zona_id == 0 && $centro_id != 0)
	{
		$sql = "
		SELECT
		$sqNivel
		date_trunc('hour',hora)::TIME AS hora,
		SUM(consumo) AS minutos,
		SUM(sesiones) AS sesiones,
		SUM(capacidad_hora) AS capacidad_hora
		FROM ocupacion_horas AS o
		LEFT JOIN centro AS c ON (c.id = o.centro_id)
		WHERE fecha BETWEEN '$fI' AND '$fF'
		AND centro_id = $centro_id
		GROUP BY mes,hora
		ORDER BY mes,hora			";

		$ns1 = $centro->getAliasReporte().'';

	}
	error_log($sql);

	$conn = Propel::getConnection();
	$d = array();

	foreach($conn->query($sql) as $r)
	{

		$minutos = floatval($r['minutos']);
		$unixtime = strtotime($r['mes']);
		$jsunixtime=$unixtime*1000;
		$hora = substr($r['hora'], 0,5);
			
		$capacidad = floatval($r['capacidad_hora']);
			
		$d[$hora." ".$ns1][] = array(
				$jsunixtime,
				$minutos
		);

		//		$d[$ns2][] = array(
		//		$jsunixtime,
		//		$capacidad
		//		);
	}
	return $d;
}


/**
 * Devuelve los registros del indicador de tasa de ocupacion por horas
 *
 * @param $fI
 * @param $fF
 * @param $centro_id
 * @param $zona_id
 * @param $nivel
 */
function tasaOcupacionPorHora($fI,$fF,$centro_id,$zona_id,$nivel)
{
	if($zona_id != 0)
	{
		$zona = ZonaPeer::retrieveByPK($zona_id);
	}
	if($centro_id != 0)
	{
		$centro = CentroPeer::retrieveByPK($centro_id);
	}

	if($nivel == 0) {
		$sqNivel = "date_trunc('month',fecha )::date as mes,";
	}
	if($nivel == 1) {
		$sqNivel = "date_trunc('week',fecha )::date as mes,";
	}
	if($nivel == 2) {
		$sqNivel = "date_trunc('day',fecha )::date as mes,";
	}

	if($fI == '' or $fI == null) $fI = '2010-09-01';
	if($fF == '' or $fF == null) $fF = date("Y-m-d");
	if($zona_id == 0 && $centro_id == 0)
	{
		$sql = "
		SELECT
		$sqNivel
		date_trunc('hour',hora)::TIME AS hora,
		SUM(consumo) AS minutos,
		SUM(sesiones) AS sesiones,
		SUM(capacidad_hora) AS capacidad_hora
		FROM ocupacion_horas AS o
		LEFT JOIN centro AS c ON (c.id = o.centro_id)
		WHERE fecha BETWEEN '$fI' AND '$fF'
		GROUP BY mes,hora
		ORDER BY mes,hora";
		//$ns1 = 'Minutos Consumidos por Hora';
		//$ns2 = 'Capacidad Total';
	}
	elseif($zona_id != 0 && $centro_id == 0)
	{
		$sql = "
		SELECT
		$sqNivel
		date_trunc('hour',hora)::TIME AS hora,
		SUM(consumo) AS minutos,
		SUM(sesiones) AS sesiones,
		SUM(capacidad_hora) AS capacidad_hora
		FROM ocupacion_horas AS o
		LEFT JOIN centro AS c ON (c.id = o.centro_id)
		WHERE fecha BETWEEN '$fI' AND '$fF'
		AND zona_id = $zona_id
		GROUP BY mes,hora
		ORDER BY mes,hora
		";

		$ns1 = $zona.'';

	}
	elseif($zona_id == 0 && $centro_id != 0)
	{
		$sql = "
		SELECT
		$sqNivel
		date_trunc('hour',hora)::TIME AS hora,
		SUM(consumo) AS minutos,
		SUM(sesiones) AS sesiones,
		SUM(capacidad_hora) AS capacidad_hora
		FROM ocupacion_horas AS o
		LEFT JOIN centro AS c ON (c.id = o.centro_id)
		WHERE fecha BETWEEN '$fI' AND '$fF'
		AND centro_id = $centro_id
		GROUP BY mes,hora
		ORDER BY mes,hora
		";

		$ns1 = $centro->getAliasReporte().'';

	}


	$conn = Propel::getConnection();
	$d = array();

	foreach($conn->query($sql) as $r)
	{

		$minutos = floatval($r['minutos']);
		$unixtime = strtotime($r['mes']);
		$jsunixtime=$unixtime*1000;
		$hora = substr($r['hora'], 0,5);
			
		$capacidad = floatval($r['capacidad_hora']);
			
		$d[$hora." ".$ns1][] = array(
				$jsunixtime,
				$minutos/$capacidad
		);

		//		$d[$ns2][] = array(
		//		$jsunixtime,
		//		$capacidad
		//		);
	}
	return $d;
}

/**
 * Devuelve los registros del indicador de ocupacion por dias
 *
 * @param $fI
 * @param $fF
 * @param $centro_id
 * @param $zona_id
 * @param $nivel
 */
function ocupacionPorDia($fI,$fF,$centro_id,$zona_id,$nivel)
{
	if($zona_id != 0)
	{
		$zona = ZonaPeer::retrieveByPK($zona_id);
	}
	if($centro_id != 0)
	{
		$centro = CentroPeer::retrieveByPK($centro_id);
	}

	if($nivel == 0) {
		$sqNivel = "date_trunc('month',fecha )::date as mes,";
	}
	if($nivel == 1) {
		$sqNivel = "date_trunc('week',fecha )::date as mes,";
	}
	if($nivel == 2) {
		$sqNivel = "date_trunc('week',fecha )::date as mes,";
	}

	if($fI == '' or $fI == null) $fI = '2010-09-01';
	if($fF == '' or $fF == null) $fF = date("Y-m-d");

	//Ajuste para que comience en lunes y termine en sabado la consulta.
	$fI = date('Y-m-d', strtotime(date("o-\WW",strtotime($fI))));
	$fF = date('Y-m-d', strtotime('+ 5 days',strtotime(date("o-\WW",strtotime($fF)))));

	if($zona_id == 0 && $centro_id == 0)
	{
		$sql = "
		SELECT
		$sqNivel
		extract('dow' from fecha) AS dia,
		SUM(consumo) AS minutos,
		SUM(sesiones) AS sesiones,
		SUM(capacidad_hora) AS capacidad_hora
		FROM ocupacion_horas AS o
		LEFT JOIN centro AS c ON (c.id = o.centro_id)
		WHERE fecha BETWEEN '$fI' AND '$fF'
		GROUP BY mes,dia
		ORDER BY mes,dia";
		//$ns1 = 'Minutos Consumidos por Hora';
		//$ns2 = 'Capacidad Total';
	}
	elseif($zona_id != 0 && $centro_id == 0)
	{
		$sql = "
		SELECT
		$sqNivel
		extract('dow' from fecha) AS dia,
		SUM(consumo) AS minutos,
		SUM(sesiones) AS sesiones,
		SUM(capacidad_hora) AS capacidad_hora
		FROM ocupacion_horas AS o
		LEFT JOIN centro AS c ON (c.id = o.centro_id)
		WHERE fecha BETWEEN '$fI' AND '$fF'
		AND zona_id = $zona_id
		GROUP BY mes,dia
		ORDER BY mes,dia";

		$ns1 = $zona.'';
	}
	elseif($zona_id == 0 && $centro_id != 0)
	{
		$sql = "
		SELECT
		$sqNivel
		extract('dow' from fecha) AS dia,
		SUM(consumo) AS minutos,
		SUM(sesiones) AS sesiones,
		SUM(capacidad_hora) AS capacidad_hora
		FROM ocupacion_horas AS o
		LEFT JOIN centro AS c ON (c.id = o.centro_id)
		WHERE fecha BETWEEN '$fI' AND '$fF'
		AND centro_id = $centro_id
		GROUP BY mes,dia
		ORDER BY mes,dia";

		$ns1 = $centro->getAliasReporte().'';

	}

	$conn = Propel::getConnection();
	$d = array();
	$dias = array('Lun','Mar','Mié','Jue','Vie','Sáb');
	$c = 0;
	foreach($conn->query($sql) as $r)
	{
		error_log('DIA:'.$r['dia']. " => ".$dias[$r['dia']-1]. " N: ".$nivel. " C: ".$c." DN: ".$r['dia']);
		$dia = $dias[$r['dia']-1];

		if($nivel > 0)
		{
			if($c==0 && $r['dia'] != $c)
			{
				compensaDias($r['mes'], $c,$ns1,$d,'inicio');
			}
		}

		$minutos = floatval($r['minutos']);
		$unixtime = strtotime($r['mes']);
		$jsunixtime=$unixtime*1000;


		$capacidad = floatval($r['capacidad_hora']);
			
		$d[$dia." ".$ns1][] = array(
				$jsunixtime,
				$minutos
		);

		$c++;
		if($c > 5) $c = 0;

	}

	if($nivel > 0)
	{
		if($c>0 && $r['dia'] != $c)
		{
			compensaDias($r['mes'], $c,$ns1,$d,'fin');
		}
	}


	return $d;
}


function compensaDias($fecha,&$cnt,$ns1,&$d,$extremo)
{
	$dias = array('Lun','Mar','Mié','Jue','Vie','Sáb');
	$ut = strtotime($fecha);
	$dn = date("N",strtotime($fecha));

	error_log("ENTRANDO A COMPENSAR:$fecha,$cnt,$ns1,$d, $extremo");


	//$d = array();
	if($extremo == 'inicio')
	{
		$fn = date('Y-m-d', strtotime(date("o-\WW",strtotime($fecha))));
		$ut = strtotime(date("o-\WW",strtotime($fecha)));
		for($cnt; $cnt < $dn -1; $cnt++)
		{
			$dia = $dias[date("N",$ut)];
			error_log("COMPENSANDO $fn => $fecha => $dn ".date("Y-m-d => N",$ut));
			$d[$dia." ".$ns1][] = array(
					$ut*1000,0
			);
			$ut = $ut + 24*3600;
		}
	}
	else
	{
		for($dn; $dn <= 5; $dn++)
		{
			$ut = $ut + 24*3600;
			$dia = $dias[date("N",$ut)];
			error_log("$ut => $fecha => $dn ".date("Y-m-d => N",$ut));
			$d[$dia." ".$ns1][] = array(
					$ut*1000,0
			);
		}
		$cnt = $dn;
	}
	//error_log(print_r($d,true));
}


/**
 * Devuelve los registros del indicador de tasa de ocupacion por dias
 *
 * @param $fI
 * @param $fF
 * @param $centro_id
 * @param $zona_id
 * @param $nivel
 */
function tasaOcupacionPorDia($fI,$fF,$centro_id,$zona_id,$nivel)
{
	if($zona_id != 0)
	{
		$zona = ZonaPeer::retrieveByPK($zona_id);
	}
	if($centro_id != 0)
	{
		$centro = CentroPeer::retrieveByPK($centro_id);
	}

	if($nivel == 0) {
		$sqNivel = "date_trunc('month',fecha )::date as mes,";
	}
	if($nivel == 1) {
		$sqNivel = "date_trunc('week',fecha )::date as mes,";
	}
	if($nivel == 2) {
		$sqNivel = "date_trunc('week',fecha )::date as mes,";
	}

	if($fI == '' or $fI == null) $fI = '2010-09-01';
	if($fF == '' or $fF == null) $fF = date("Y-m-d");

	//Ajuste para que comience en lunes y termine en sabado la consulta.
	$fI = date('Y-m-d', strtotime(date("o-\WW",strtotime($fI))));
	$fF = date('Y-m-d', strtotime('+ 5 days',strtotime(date("o-\WW",strtotime($fF)))));

	if($zona_id == 0 && $centro_id == 0)
	{
		$sql = "
		SELECT
		$sqNivel
		extract('dow' from fecha) AS dia,
		SUM(consumo) AS minutos,
		SUM(sesiones) AS sesiones,
		SUM(capacidad_hora) AS capacidad_hora
		FROM ocupacion_horas AS o
		LEFT JOIN centro AS c ON (c.id = o.centro_id)
		WHERE fecha BETWEEN '$fI' AND '$fF'
		GROUP BY mes,dia
		ORDER BY mes,dia";
		//$ns1 = 'Minutos Consumidos por Hora';
		//$ns2 = 'Capacidad Total';
	}
	elseif($zona_id != 0 && $centro_id == 0)
	{
		$sql = "
		SELECT
		$sqNivel
		extract('dow' from fecha) AS dia,
		SUM(consumo) AS minutos,
		SUM(sesiones) AS sesiones,
		SUM(capacidad_hora) AS capacidad_hora
		FROM ocupacion_horas AS o
		LEFT JOIN centro AS c ON (c.id = o.centro_id)
		WHERE fecha BETWEEN '$fI' AND '$fF'
		AND zona_id = $zona_id
		GROUP BY mes,dia
		ORDER BY mes,dia";

		$ns1 = $zona.'';

	}
	elseif($zona_id == 0 && $centro_id != 0)
	{
		$sql = "
		SELECT
		$sqNivel
		extract('dow' from fecha) AS dia,
		SUM(consumo) AS minutos,
		SUM(sesiones) AS sesiones,
		SUM(capacidad_hora) AS capacidad_hora
		FROM ocupacion_horas AS o
		LEFT JOIN centro AS c ON (c.id = o.centro_id)
		WHERE fecha BETWEEN '$fI' AND '$fF'
		AND centro_id = $centro_id
		GROUP BY mes,dia
		ORDER BY mes,dia";

		$ns1 = $centro->getAliasReporte().'';

	}
	error_log($sql);

	$conn = Propel::getConnection();
	$d = array();
	$dias = array('Lun','Mar','Mié','Jue','Vie','Sáb');
	foreach($conn->query($sql) as $r)
	{

		$minutos = floatval($r['minutos']);
		$unixtime = strtotime($r['mes']);
		$jsunixtime=$unixtime*1000;
		$dia = $dias[$r['dia']-1];
			
		$capacidad = floatval($r['capacidad_hora']);
			
		$d[$dia." ".$ns1][] = array(
				$jsunixtime,
				$minutos/$capacidad
		);

		//		$d[$ns2][] = array(
		//		$jsunixtime,
		//		$capacidad
		//		);
	}
	return $d;
}


/**
 * Socios activos nuevos y acumulados
 *
 * @param string $fI
 * @param string $fF
 * @param integer $centro_id
 * @param integer $zona_id
 */
function sociosActivos($fI,$fF,$centro_id,$zona_id, $nivel)
{
	if($zona_id != 0)
	{
		$zona = ZonaPeer::retrieveByPK($zona_id);
	}
	if($centro_id != 0)
	{
		$centro = CentroPeer::retrieveByPK($centro_id);
	}

	if($fI == '' or $fI == null) $fI = '2010-09-01';
	if($fF == '' or $fF == null) $fF = date("Y-m-d");

	if($nivel == 0) {
		$sqNivel = "date_trunc('month',fecha )::date as mes,";
	}
	if($nivel == 1) {
		$sqNivel = "date_trunc('week',fecha )::date as mes,";
	}
	if($nivel == 2) {
		$sqNivel = "date_trunc('day',fecha )::date as mes,";
	}

	//Ajuste para que comience en lunes y termine en sabado la consulta.
	$fI = date('Y-m-d', strtotime(date("o-\WW",strtotime($fI))));
	$fF = date('Y-m-d', strtotime('+ 5 days',strtotime(date("o-\WW",strtotime($fF)))));

	if($zona_id == 0 && $centro_id == 0)
	{
		$sql = "
		SELECT
		$sqNivel
		count(distinct(socio_id)) AS activos
		FROM
		reporte_activos
		WHERE fecha BETWEEN '$fI' AND '$fF'
		GROUP BY
		mes
		ORDER BY
		mes
		";

		$sqlb = "
		SELECT
		$sqNivel
		sum(cantidad) as cantidad
		FROM reporte_metas AS r
		WHERE categoria_fija_id = 1
		AND fecha BETWEEN '$fI' AND '$fF'
		GROUP BY mes
		ORDER BY mes;
		";

	}
	elseif($zona_id != 0 && $centro_id == 0)
	{
		$sql = "
		SELECT
		$sqNivel
		count(distinct(socio_id)) AS activos
		FROM
		reporte_activos
		JOIN centro AS c ON (c.id = centro_id)
		WHERE zona_id = $zona_id
		AND fecha BETWEEN '$fI' AND '$fF'
		GROUP BY
		mes
		ORDER BY
		mes
		";
		$sqlb = "
		SELECT
		$sqNivel
		sum(cantidad) as cantidad
		FROM reporte_metas AS r
		LEFT JOIN centro AS c ON (r.centro_id = c.id)
		WHERE categoria_fija_id = 1
		AND fecha BETWEEN '$fI' AND '$fF'
		AND c.zona_id = $zona_id
		GROUP BY mes
		ORDER BY mes;
		";
		$ns1 = $zona.'';

	}
	elseif($zona_id == 0 && $centro_id != 0)
	{
		$sql = "
		SELECT
		$sqNivel
		count(distinct(socio_id)) AS activos
		FROM
		reporte_activos
		JOIN centro AS c ON (c.id = centro_id)
		WHERE centro_id = $centro_id
		AND fecha BETWEEN '$fI' AND '$fF'
		GROUP BY
		mes
		ORDER BY
		mes
		";
		$sqlb = "
		SELECT
		$sqNivel
		sum(cantidad) as cantidad
		FROM reporte_metas AS r
		LEFT JOIN centro AS c ON (r.centro_id = c.id)
		WHERE categoria_fija_id = 1
		AND fecha BETWEEN '$fI' AND '$fF'
		AND c.id = $centro_id
		GROUP BY mes
		ORDER BY mes;
		";
		$ns1 = $centro->getAliasReporte().'';

	}
	error_log($sqlb,3,'/tmp/query.log');

	$conn = Propel::getConnection();
	$d2 = array();
	foreach($conn->query($sqlb) as $r2)
	{
		$d2[strtotime($r2['mes'])*1000] = floatval($r2['cantidad']);
	}
	foreach($conn->query($sql) as $r)
	{
		$activos = floatval($r['activos']);
		$unixtime = strtotime($r['mes']);
		$jsunixtime=$unixtime*1000;

		$acumulados = $acumulados + $d2[$jsunixtime];

		$d['Nuevos'." ".$ns1][] = array(
				$jsunixtime,
				$d2[$jsunixtime]
		);
		$d['Activos'." ".$ns1][] = array(
				$jsunixtime,
				$activos
		);
		$d['Acumulados'." ".$ns1][] = array(
				$jsunixtime,
				$acumulados
		);
	}
	return $d;
}

/**
 * Inserta los socios activos en la tabla reporte_activos del lado de centro
 *
 * @param integer $centro_id
 * @param string $fecha
 */
function sociosActivosCentro($centro_id,$fecha)
{
	$sql ="
	SELECT socio_id FROM sesion_historico WHERE centro_id = $centro_id AND fecha_login::date ='$fecha'
	UNION
	SELECT socio_id FROM orden WHERE tipo_id IN (1,3) AND centro_id = $centro_id AND created_at::date ='$fecha'
	";
	$conn = Propel::getConnection();
	$d = array();

	foreach($conn->query($sql) as $r)
	{
		$socio = SocioPeer::retrieveByPK($r['socio_id']);
		$a = new ReporteActivos();
		$a->setCentroId($centro_id);
		$a->setFecha($fecha);
		$a->setSocioId($socio->getId());
		$a->setUsuario($socio->getUsuario());
		$a->setFolio($socio->getFolio());
		$a->setNombre($socio->getNombre());
		$a->setApepat($socio->getApepat());
		$a->setApemat($socio->getApemat());
		$a->setNombreCompleto($socio->getNombreCompleto());
		$a->setFechaNac($socio->getFechaNac());
		$a->setSexo($socio->getSexo());
		$a->setEmail($socio->getEmail());
		$a->setTel($socio->getTel());
		$a->setCelular($socio->getCelular());
		$a->setEstado($socio->getEstado());
		$a->setCiudad($socio->getCiudad());
		$a->setMunicipio($socio->getMunicipio());
		$a->setColonia($socio->getColonia());
		$a->setCp($socio->getCp());
		$a->setCalle($socio->getCalle());
		$a->setNumExt($socio->getNumExt());
		$a->setNumInt($socio->getNumInt());
		$a->setLat($socio->getLat());
		$a->setLong($socio->getLong());
		$a->save();
	}

}

/**
 * Devuelve el numero total de Cursos vendidos en un periodo.
 * @param string $fI
 * @param string $fI
 * @param integer $nivel
 * @param integer $centro_id
 * @param integer $zona_id
 * @return array
 */
function numCursosPeriodo($fI,$fF,$nivel,$centro_id,$zona_id)
{
	$conn = Propel::getConnection();

	if($fI == '' or $fI == null) $fI = '2010-09-01';
	if($fF == '' or $fF == null) $fF = date("Y-m-d");

	if($nivel == 0) {
		$sqNivel = "date_trunc('month',fecha )::date as mes,";
	}
	if($nivel == 1) {
		$sqNivel = "date_trunc('week',fecha )::date as mes,";
	}
	if($nivel == 2) {
		$sqNivel = "date_trunc('day',fecha )::date as mes,";
	}
	if($zona_id == 0 && $centro_id == 0)
	{
		$sql = "
		SELECT
		$sqNivel
		SUM(cantidad) AS cantidad,
		SUM(monto) AS monto
		FROM reporte_venta_cursos AS r
		WHERE fecha BETWEEN '$fI' AND '$fF'
		GROUP BY mes
		ORDER BY mes;			";
	}
	elseif($zona_id != 0 && $centro_id == 0)
	{
		$sql = "
		SELECT
		$sqNivel
		SUM(cantidad) AS cantidad,
		SUM(monto) AS monto
		FROM reporte_venta_cursos AS r
		LEFT JOIN centro AS c ON (c.id = r.centro_id)
		WHERE fecha BETWEEN '$fI' AND '$fF'
		AND c.zona_id = $zona_id
		GROUP BY mes
		ORDER BY mes;			";
	}
	elseif($zona_id == 0 && $centro_id != 0)
	{
		$sql = "
		SELECT
		$sqNivel
		SUM(cantidad) AS cantidad,
		SUM(monto) AS monto
		FROM reporte_venta_cursos AS r
		LEFT JOIN centro AS c ON (c.id = r.centro_id)
		WHERE fecha BETWEEN '$fI' AND '$fF'
		AND c.id = $centro_id
		GROUP BY mes
		ORDER BY mes;			";
	}
	$ar=array();
	foreach($conn->query($sql) as $r)
	{
		$ar[$r['mes']] = array('cantidad'=>$r['cantidad'],'monto'=>$r['monto']);
	}
	return $ar;
}



/**
 * Ventas por categoría de curso
 *
 * @param integer $centro_id
 * @param string $fecha
 */
function ventasCategoriaCursoCentro($centro_id,$fecha){
	$sql ="
	SELECT
	o.created_at::date as fecha,
	SUM(d.subtotal) AS monto,
	count(*) AS cantidad,
	CASE WHEN a.id IS NULL THEN 99999 ELSE a.id END AS categoria_id,
	CASE WHEN a.siglas IS NULL THEN 'OT' ELSE a.siglas END AS siglas,
	CASE WHEN a.nombre IS NULL THEN 'Otro' ELSE a.nombre END AS nombre
	FROM
	detalle_orden AS d
	JOIN orden AS o ON (o.id = d.orden_id)
	JOIN producto AS p ON (p.id = d.producto_id AND p.categoria_id = 2)
	JOIN centro AS e ON (e.id = d.centro_id)
	LEFT JOIN curso AS c ON (c.clave = p.codigo)
	LEFT JOIN categoria_curso AS a ON (a.id = c.categoria_curso_id)
	WHERE p.categoria_id = 2
	AND d.centro_id = $centro_id
	AND o.created_at::date = '$fecha'
	GROUP BY fecha,siglas,a.nombre,a.id
	ORDER BY fecha,siglas		";
	//echo $sql;
	$conn = Propel::getConnection();
	$d = array();

	foreach($conn->query($sql) as $r)
	{
		$semana = date('W', strtotime($r['fecha']));
		$anio = date('Y', strtotime($r['fecha']));

		$e = new ReporteCategoriaCursos();
		$e->setCentroId($centro_id);
		$e->setMonto($r['monto']);
		$e->setCantidad($r['cantidad']);
		$e->setFecha($r['fecha']);
		$e->setSemana($semana);
		$e->setAnio($anio);
		$e->setCategoriaId($r['categoria_id']);
		$e->setSiglas($r['siglas']);
		$e->setCategoriaNombre($r['nombre']);

		$e->save();
	}

}


/**
 * Ventas cursos
 * @param integer $centro_id
 * @param string $fecha
 */
function ventasCursosCentro($centro_id,$fecha)
{
	$sql ="
	SELECT
	o.created_at::date as fecha,
	d.subtotal AS monto,
	d.id as detalle_orden_id,
	d.socio_id,
	d.cantidad,
	c.id as curso_id,
	CASE WHEN a.id IS NULL THEN 99999 ELSE a.id END AS categoria_id,
	CASE WHEN a.siglas IS NULL THEN 'OT' ELSE a.siglas END AS siglas,
	CASE WHEN a.nombre IS NULL THEN 'Otro' ELSE a.nombre END AS nombre
	FROM
	detalle_orden AS d
	JOIN orden AS o ON (o.id = d.orden_id)
	JOIN producto AS p ON (p.id = d.producto_id AND p.categoria_id = 2)
	JOIN centro AS e ON (e.id = d.centro_id)
	LEFT JOIN curso AS c ON (c.clave = p.codigo)
	LEFT JOIN categoria_curso AS a ON (a.id = c.categoria_curso_id)
	WHERE p.categoria_id = 2
	AND d.centro_id = $centro_id
	AND o.created_at::date = '$fecha'
	ORDER BY fecha,detalle_orden_id";
	//echo $sql;
	$conn = Propel::getConnection();
	$d = array();

	foreach($conn->query($sql) as $r)
	{
		$e = new ReporteVentaCursos();
		$e->setCentroId($centro_id);
		$e->setDetalleOrdenId($r['detalle_orden_id']);
		$e->setSocioId($r['socio_id']);
		$e->setFecha($r['fecha']);
		$e->setMonto($r['monto']);
		$e->setCursoId($r['curso_id']);
		$e->setCategoriaId($r['categoria_id']);
		$e->setSiglas($r['siglas']);
		$e->setCategoriaNombre($r['nombre']);
		$e->setCantidad($r['cantidad']);
		$e->save();
	}
}
?>