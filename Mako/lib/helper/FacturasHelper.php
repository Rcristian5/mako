<?php

/*
 * Componentes Bixit
*
* Copyright (c) 2009-2011 Bixit SA de CV
* Bajo Licencia dual MIT (http://www.bixit.mx/legal/MIT-LICENSE.txt)
* y GPL (http://www.bixit.mx/legal/GPL-LICENSE.txt).
*
* File:        FacturasHelper.php
* Description: Genera la cadeba original y forma el XMl de una comprobante fiscal digital
* Author:      David Galindo Garcia
*/


define("PATH_PROYECTO",dirname(__FILE__).'/../..');
define("PATH_FACTURAS",PATH_PROYECTO."/web/facturas");
define("PATH_TICKETS",PATH_PROYECTO."/web/tickets");
define("PATH_TIMBRADO",PATH_PROYECTO."/web/timbrado");
define("PATH_LLAVES",PATH_PROYECTO."/llaves");
define("PATH_XSD",dirname(__FILE__));

/**
 * Regresa la cadena formateada de productos para impresión de ticket
 * @param Orden $orden
 * @return string
 */
function cadenaProductosTicket(Orden $orden, $tasa0=false)
{
	$productos = $orden->getDetalleOrdens();

	$cadp="";
	$caf_prods="";
	foreach($productos as $p)
	{
		$cla = array();
		$psplit = array();
		$cant = array();
		$pre = array();

		//usamos este hacksillo para cortar las descripciones largas de los productos en varios renglones
		$cla[] = $p->getProducto()->getCodigo();
		$cant[] = intval($p->getCantidad());







		/*
        * Reingenieria  Mako. SIN - ID RQ
        * Responsables:  (BP) Bet Gader Porcayo Juárez, (LGL) Luis Gonzales Linares
        * Fecha:        21-01-2015
        * Descripción:  Se añade opcion para configuración de Nuevas Impresoras
        */
        $Prefijo = @sfConfig::get('app_prefijo');
		switch ($Prefijo) {
			case 'oki':
				$car_nom_prod = 20;
				$car_can = 9;
				$car_pre = 9;
				$tpl_tick = "%-20s %7s   %9s ";
				break;
			case 'Ch':
			case 'TMT20II':
				/*
				* En este caso se incluye el tipo Ch TMT20II
				*/
				//Para impresoras de 40 caracteres de ancho.
				$car_nom_prod = sfConfig::get('app_cpl') - 16;
				$car_can = 4;
				$car_pre = 8;
				$tpl_tick = "%-".$car_nom_prod."s %".$car_can."s  %s";
				break;
			default:
				/*
				* En caso de no estar configurado el parámetro app_prefijo, o que no es alguno 
				* de los anteriores se configura con respecto a OKI, 
				*/
				//Para impresoras de 40 caracteres de ancho.
				$car_nom_prod = sfConfig::get('app_cpl') - 16;
				$car_can = 4;
				$car_pre = 8;
				$tpl_tick = "%-".$car_nom_prod."s %".$car_can."s  %s";

				error_log('ERROR GENERICO!!! No se ha configurado el parámetro PREFIJO o el valor NO EXISTE en la configuración. Fn=cadenaProductosTicket');
				break;
		}




		$tasa = intval($orden->getCentro()->getIva());
		if($tasa0) $tasa = 0;

		$pre[] = '$'.sprintf("%".$car_pre."s",money_format('%(#1n',tsi($p->getSubtotal(), $tasa)));

		$psplit=split('\|',wordwrap(fa($p->getProducto()->getNombre()),$car_nom_prod,'|'));
		foreach($psplit as $id=>$d)
		{
			$cantidad_ticket = (isset($cant[$id])) ? intval($cant[$id]) : '';
			$cadp .= sprintf($tpl_tick,$d,$cantidad_ticket,(isset($pre[$id]))? $pre[$id] : '' )."\n";
		}

		error_log($p->getProducto()->getCategoriaId() ."==". sfConfig::get('app_categoria_cafeteria_id'));
		if($p->getProducto()->getCategoriaId() == sfConfig::get('app_categoria_cafeteria_id'))
		{
			$caf_prods .= intval($p->getCantidad())." " .fa($p->getProducto()->getNombre())."\n";
		}

	}
	return array($cadp,$caf_prods);
}

/**
 * Regresa la cadena formateada de productos para impresión de factura
 * @param Orden $orden
 * @return array La cadena de productos y el arreglo de conceptos
 */
function cadenaProductosFactura(Orden $orden, $tasa0=false)
{
	$productos = $orden->getDetalleOrdens();
	//Determinamos la tasa del iva
	$tasa = $tasa0 ? 0 : $orden->getCentro()->getIva();

	$cadp="";
	$caf_prods="";
	foreach($productos as $p){

		//Se genera para la factura electronica.
		$precioUnitario = $p->getSubtotal()/$p->getCantidad();
		$valorUnitario = number_format($precioUnitario / ($tasa/100+1), 6, '.', '');
		$conceptos[] = array(
				'descripcion' => $p->getProducto()->getNombre(),
				'unidad' => unidadesSAT($p->getProducto()->getUnidadProducto()->getNombre()),
				'cantidad' => number_format(intval($p->getCantidad()),2,'.',''),
				'valorUnitario' => $valorUnitario,
				'importe' => number_format($valorUnitario * $p->getCantidad(),6,'.',''),
		);

		$cla = array();
		$psplit = array();
		$cant = array();
		$pre = array();
		$unidad= array();

		//usamos este hacksillo para cortar las descripciones largas de los productos en varios renglones
		$cla[] = $p->getProducto()->getCodigo();
		$cant[] = $p->getCantidad();
		$unidad[] = unidadesSAT($p->getProducto()->getUnidadProducto()->getNombre());






		/*
        * Reingenieria  Mako. SIN - ID RQ
        * Responsables:  (BP) Bet Gader Porcayo Juárez, (LGL) Luis Gonzales Linares
        * Fecha:        21-01-2015
        * Descripción:  Se añade opcion para configuración de Nuevas Impresoras
        */
        $Prefijo = @sfConfig::get('app_prefijo');
		switch ($Prefijo) {
			case 'oki':
				$car_nom_prod = 19;
				$car_can = 5;
				$car_pre = 9;
				$tpl_tick = "%-19s %-4s %10s   %s";
				break;
			case 'Ch':
			case 'TMT20II':
				/*
				* En este caso se incluye el tipo Ch TMT20II
				*/
				//Para impresoras de 40 caracteres de ancho.
				$car_nom_prod = sfConfig::get('app_cpl') - 24;
				$car_can = 4;
				$car_pre = 6;
				$tpl_tick = "%-".$car_nom_prod."s %-".$car_can."s %-10s %s";
				break;
			default:
				/*
				* En caso de no estar configurado el parámetro app_prefijo, o que no es alguno 
				* de los anteriores se configura con respecto a OKI, 
				*/
				//Para impresoras de 40 caracteres de ancho.
				$car_nom_prod = sfConfig::get('app_cpl') - 24;
				$car_can = 4;
				$car_pre = 6;
				$tpl_tick = "%-".$car_nom_prod."s %-".$car_can."s %-10s %s";

				error_log('ERROR GENERICO!!! No se ha configurado el parámetro PREFIJO o el valor NO EXISTE en la configuración. Fn=cadenaProductosFactura');
				break;
		}




		$pre[] = '$'.sprintf("%".$car_pre."s",sprintf("%.2f", $valorUnitario ));

		$psplit=split('\|',wordwrap(fa($p->getProducto()->getNombre()),$car_nom_prod,'|'));
		foreach($psplit as $id=>$d)
		{
			$cantidad_ticket = (isset($cant[$id])) ? intval($cant[$id]) : '';
				
			$cadp .= sprintf($tpl_tick,$d,$cantidad_ticket, (isset($unidad[$id]) ) ? $unidad[$id] : ''  , (isset($pre[$id]))? $pre[$id] : '' )."\n";
		}

		if($p->getProducto()->getCategoriaId() == sfConfig::get('app_categoria_cafeteria_id'))
		{
			$caf_prods .= intval($p->getCantidad())." " .fa($p->getProducto()->getNombre())."\n";
		}

	}

	return array($cadp,$conceptos,$caf_prods);
}
/**
 * Verifica si la unidad del producto se encuntra dentro de las unidades
 * recomendades por el SAT
 *
 * @param varchar $unidad
 */
function unidadesSAT($unidad)
{
	$unidades = array("KILO","GRAMO","METRO LINEAL","METRO CUADRADO","METRO CUBICO","PIEZA","CABEZA","LITRO","PAR","KILOWATT","MILLAR","JUEGO","KILOWATT/HORA",
			"TONELADA","BARRIL","GRAMO NETO","DECENAS","CIENTOS","DOCENAS","CAJA","BOTELLA", "SERVICIO", "NO APLICA");
	$unidad = strtoupper($unidad);

	error_log($unidad);

	return in_array($unidad, $unidades) ? $unidad : 'NO APLICA';
}

/**
 * Convierte numeros a letras
 */
function numeros_letras($num){
	require_once 'Numbers/Words.php';
	$sp = split('\.',$num);
	$entero = 0;
	$dec = 0;
	if(isset($sp[0]))
	{
		$entero = $sp[0];
	}
	if(isset($sp[1]))
	{
		$dec = $sp[1];
	}

	$n2w = new Numbers_Words();
	$letras=strtoupper($n2w->toWords($entero,"es_AR"));
	$letras = str_replace('UNO','UN',$letras);
	$letras = $letras . " PESOS ". sprintf("%02d",$dec) ."/100 M.N.";
	return $letras;
}

/**
 * Funcion que desglosa el iva y devuelve un array con el importe y el iva
 */
function desglosa_iva($total,$mi_iva){

	$piva=$mi_iva/100+1;	//Porcentaje de iva
	$totalp=$total;
	$importe=$totalp/$piva;
	$iva= $totalp - $importe;

	$ret[]= money_format('%#1n', $iva);
	$ret[]= money_format('%#1n', $importe);
	$ret[]= money_format('%#1n', $totalp);
	$ret[]= numeros_letras($totalp);

	return $ret;
}

/**
 * Regresa el importe menos el iva para el desglose de subtotales
 */
function tsi($importe,$ret){

	$im = $importe  / (1 + $ret/100);
	return $im;
}

/**
 * Filtra acentos, se usa para
 * @param string $cad
 * @return string
 */
function fa($cad){
	if($cad == '')
	{
		return $cad;
	}

	$cad = str_replace("á", "a", $cad);
	$cad = str_replace("é", "e", $cad);
	$cad = str_replace("í", "i", $cad);
	$cad = str_replace("ó", "o", $cad);
	$cad = str_replace("ú", "u", $cad);

	$cad = str_replace("Á", "A", $cad);
	$cad = str_replace("É", "E", $cad);
	$cad = str_replace("Í", "I", $cad);
	$cad = str_replace("Ó", "O", $cad);
	$cad = str_replace("Ú", "U", $cad);

	$cad = str_replace("Ü", "U", $cad);
	$cad = str_replace("ü", "u", $cad);

	$cad = str_replace("ñ", "n", $cad);
	$cad = str_replace("Ñ", "N", $cad);

	return $cad;
}


/**
 * Regresa los datos de la factura electrónica. Cadena original, sello digital y xml.
 * @param array $confFactura Es un arreglo con los datos necesarios para "configurar" la factura:
 * serie, folio, fecha, noAprobacion, anoAprobacion, tipoDeComprobante, formaDePago, noCertificado, Emisor, Receptor y Traslados
 * Donde: Emisor es un arreglo con datos del emisor fiscal, Receptor un arreglo con los datos del receptor fiscal y Traslados
 * un arreglo con la información de traslados.
 *
 * @param array $conceptos Es un arreglo con los datos de los conceptos a facturar.
 * @return array La función regresa un arreglo con tres cadenas, $fact['cadena_original'],$fact['sello'],$fact['xml']
 */
function generaFactura($confFactura,$conceptos){

	$fact = array();
	$fact['cadena_original']=cadenaOriginal($confFactura,$conceptos);
	
	// Obtenmos el sello digital y el certificado de la factura
	$sellos = generaSello($fact['cadena_original'],$confFactura['noCertificado']);
	$fact['sello_digital'] = $sellos['sello'];
	$confFactura['sello']= $fact['sello_digital'];
	$confFactura['certificado']= $sellos['certificado'];
	$fact['xml'] = cfdxml($confFactura,$conceptos);

	//aqui empieza la magia negra.

	$dir=PATH_FACTURAS;
	$nombreArchivo = $dir."/".$confFactura['Emisor']['rfc'].$confFactura['serie'].$confFactura['folio'].'.xml';    // Junta el numero de factura   serie + folio
	file_put_contents($nombreArchivo,utf8_encode_seguro($fact['xml']));
	$xml = new DOMDocument();
	$xml->load($nombreArchivo);
	$xml->schemaValidate(PATH_XSD."/".'cfdv2.xsd');

	//Se crea factura para timbrado.
	FacturaPeer::creacionFacturatimbrado($confFactura,$conceptos);


	return $fact;
}

/**
 * Genera la cadena original
 */
function cadenaOriginal($arreglo,$conceptos)
{
	// La cadena original lleva un orden especifico, asi que la armamos en la siguiente secuencia
	// Comienza cadena original nodo Comprobante
	$cadena=array(	$arreglo['version'],
			$arreglo['serie'],
			$arreglo['folio'],
			$arreglo['fecha'],
			$arreglo['noAprobacion'],
			$arreglo['anoAprobacion'],
			$arreglo['tipoDeComprobante'],
			$arreglo['formaDePago'],
			$arreglo['subTotal'],
			$arreglo['descuento'],
			$arreglo['total'],
			$arreglo['metodoDePago'],
			$arreglo['LugarExpedicion'],
			$arreglo['Emisor']['rfc'],
			$arreglo['Emisor']['nombre'],
			isset($arreglo['Emisor']['DomicilioFiscal']['calle'])?$arreglo['Emisor']['DomicilioFiscal']['calle']:null,
			isset($arreglo['Emisor']['DomicilioFiscal']['noExterior'])?$arreglo['Emisor']['DomicilioFiscal']['noExterior']:null,
			isset($arreglo['Emisor']['DomicilioFiscal']['noInterior'])?$arreglo['Emisor']['DomicilioFiscal']['noInterior']:null,
			isset($arreglo['Emisor']['DomicilioFiscal']['colonia'])?$arreglo['Emisor']['DomicilioFiscal']['colonia']:null,
			isset($arreglo['Emisor']['DomicilioFiscal']['municipio'])?$arreglo['Emisor']['DomicilioFiscal']['municipio']:null,
			isset($arreglo['Emisor']['DomicilioFiscal']['estado'])?$arreglo['Emisor']['DomicilioFiscal']['estado']:null,
			isset($arreglo['Emisor']['DomicilioFiscal']['pais'])?$arreglo['Emisor']['DomicilioFiscal']['pais']:null,
			isset($arreglo['Emisor']['DomicilioFiscal']['codigoPostal'])?$arreglo['Emisor']['DomicilioFiscal']['codigoPostal']:null,
			// Expedido en
			isset($arreglo['Emisor']['ExpedidoEn']['calle'])?$arreglo['Emisor']['ExpedidoEn']['calle']:null,
			isset($arreglo['Emisor']['ExpedidoEn']['noExterior'])?$arreglo['Emisor']['ExpedidoEn']['noExterior']:null,
			isset($arreglo['Emisor']['ExpedidoEn']['noInterior'])?$arreglo['Emisor']['ExpedidoEn']['noInterior']:null,
			isset($arreglo['Emisor']['ExpedidoEn']['colonia'])?$arreglo['Emisor']['ExpedidoEn']['colonia']:null,
			isset($arreglo['Emisor']['ExpedidoEn']['municipio'])?$arreglo['Emisor']['ExpedidoEn']['municipio']:null,
			isset($arreglo['Emisor']['ExpedidoEn']['estado'])?$arreglo['Emisor']['ExpedidoEn']['estado']:null,
			isset($arreglo['Emisor']['ExpedidoEn']['pais'])?$arreglo['Emisor']['ExpedidoEn']['pais']:null,
			isset($arreglo['Emisor']['ExpedidoEn']['codigoPostal'])?$arreglo['Emisor']['ExpedidoEn']['codigoPostal']:null,
			$arreglo['Emisor']['RegimenFiscal']['Regimen'],
			$arreglo['Receptor']['rfc'],
			$arreglo['Receptor']['nombre'],
			isset($arreglo['Receptor']['Domicilio']['calle'])?$arreglo['Receptor']['Domicilio']['calle']:null,
			isset($arreglo['Receptor']['Domicilio']['noExterior'])?$arreglo['Receptor']['Domicilio']['noExterior']:null,
			isset($arreglo['Receptor']['Domicilio']['noInterior'])?$arreglo['Receptor']['Domicilio']['noInterior']:null,
			isset($arreglo['Receptor']['Domicilio']['colonia'])?$arreglo['Receptor']['Domicilio']['colonia']:null,
			isset($arreglo['Receptor']['Domicilio']['municipio'])?$arreglo['Receptor']['Domicilio']['municipio']:null,
			isset($arreglo['Receptor']['Domicilio']['estado'])?$arreglo['Receptor']['Domicilio']['estado']:null,
			isset($arreglo['Receptor']['Domicilio']['pais'])?$arreglo['Receptor']['Domicilio']['pais']:null,
			isset($arreglo['Receptor']['Domicilio']['codigoPostal'])?$arreglo['Receptor']['Domicilio']['codigoPostal']:null);
	foreach ($conceptos as $productos => $detalle)
	{
		array_push($cadena,	$detalle['cantidad'],
				$detalle['unidad'],
				$detalle['descripcion'],
				$detalle['valorUnitario'],
				$detalle['importe']);
	}
	array_push($cadena,	$arreglo['Impuestos']['Traslados']['Traslado']['impuesto'],
			$arreglo['Impuestos']['Traslados']['Traslado']['tasa'],
			$arreglo['Impuestos']['Traslados']['Traslado']['importe']);
	$cadena_original="||";
	foreach ($cadena as $key => $val) {
		$val = preg_replace('/\s\s+/', ' ', $val);
		$val = trim($val);
		if (strlen($val)>0) {
			$val = utf8_encode_seguro(str_replace("|","/",$val));
			$cadena_original .= $val . "|";
		}
	}
	$cadena_original.="|";
	return $cadena_original;
}


/**
 * Codifica las cadenas
 */
function utf8_encode_seguro($texto)
{
	return (codificacion($texto)==ISO_8859_1) ? utf8_encode($texto) : $texto;
}

/**
 * Genera el sello digital y obtine el certificado para incluir en la factura
 *
 * @return array - [sello] => Sello generado a partir de la cadena original, [certificado] => certificado para incluir en el XML
 */
function generaSello($cadena_original,$noCertificado)
{
	$ruta = PATH_LLAVES;
	$file=$ruta."/".$noCertificado.".key.pem";
	$crypttext='';
	$pkeyid = openssl_get_privatekey(file_get_contents($file));
	openssl_sign($cadena_original, $crypttext, $pkeyid, OPENSSL_ALGO_SHA1);
	openssl_free_key($pkeyid);
	$sello = base64_encode($crypttext);
	$file=$ruta."/".$noCertificado.".cer.pem";
	$datos = file($file);
	$certificado = ""; $carga=false;
	for ($i=0; $i<sizeof($datos); $i++) {
		if (strstr($datos[$i],"END CERTIFICATE")) $carga=false;
		if ($carga) $certificado .= trim($datos[$i]);
		if (strstr($datos[$i],"BEGIN CERTIFICATE")) $carga=true;
	}
	return array('sello' => $sello, 'certificado' => $certificado);
}

define("UTF_8", 1);
define("ASCII", 2);
define("ISO_8859_1", 3);
function codificacion($texto)
{
	$c = 0;
	$ascii = true;
	for ($i = 0;$i<strlen($texto);$i++) {
		$byte = ord($texto[$i]);
		if ($c>0) {
			if (($byte>>6) != 0x2) {
				return ISO_8859_1;
			} else {
				$c--;
			}
		} elseif ($byte&0x80) {
			$ascii = false;
			if (($byte>>5) == 0x6) {
				$c = 1;
			} elseif (($byte>>4) == 0xE) {
				$c = 2;
			} elseif (($byte>>3) == 0x1E) {
				$c = 3;
			} else {
				return ISO_8859_1;
			}
		}
	}
	return ($ascii) ? ASCII:UTF_8;
}

/**
 * Genera el xml de la factura
 */
function cfdxml($arreglo,$productos)
{
	$root='';
	$Emisor='';
	$DomicilioFiscal='';
	$ExpedidoEn='';
	$RegimenFiscal='';
	$Receptor='';
	$Domicilio='';
	$Conceptos='';
	$Concepto='';
	$Impuestos='';
	$Traslado='';
	$Traslados='';
	$xml_writer_object = new xml_writer_class;
	$xml_writer_object->addtag('Comprobante',$arreglo,'',$root,1);
	$xml_writer_object->addtag('Emisor',$arreglo['Emisor'],$root,$Emisor,1);
	$xml_writer_object->addtag('DomicilioFiscal',$arreglo['Emisor']['DomicilioFiscal'],$Emisor,$DomicilioFiscal,1);
	if (isset($arreglo['Emisor']['ExpedidoEn']))
	{
		$xml_writer_object->addtag('ExpedidoEn',$arreglo['Emisor']['ExpedidoEn'],$Emisor,$ExpedidoEn,1);
	}
	$xml_writer_object->addtag('RegimenFiscal',$arreglo['Emisor']['RegimenFiscal'],$Emisor,$RegimenFiscal,1);
	$xml_writer_object->addtag('Receptor',$arreglo['Receptor'],$root,$Receptor,1);
	$xml_writer_object->addtag('Domicilio',$arreglo['Receptor']['Domicilio'],$Receptor,$Domicilio,1);
	$xml_writer_object->addtag('Conceptos',$productos,$root,$Conceptos,1);
	for ($i=0; $i<=(sizeof($productos)-1); $i++)
	{
		$xml_writer_object->addtag('Concepto',$productos[$i],$Conceptos,$Concepto,1);
	}
	$xml_writer_object->addtag('Impuestos',$arreglo['Impuestos'],$root,$Impuestos,1);
	$xml_writer_object->addtag('Traslados',$arreglo['Impuestos']['Traslados'],$Impuestos,$Traslados,1);
	$xml_writer_object->addtag('Traslado',$arreglo['Impuestos']['Traslados']['Traslado'],$Traslados,$Traslado,1);
	$cfdxml='';
	if(!$xml_writer_object->write($cfdxml)){
		$cfd='Error: '.$xml_writer_object->error;
	}
	return $cfdxml;
}


if(!defined("METAL_LIBRARY_XML_XML_WRITER_CLASS"))
{
	define("METAL_LIBRARY_XML_XML_WRITER_CLASS",1);

	/*
	 *
	* Copyright (C) Manuel Lemos 2001-2002
	*
	* @(#) $Id: FacturasHelper.php,v 1.11 2012-01-30 12:25:59 eorozco Exp $
	* Modificada funcion addtag 9 sep 2007 no publicado *Alfredo*
	*/

	class xml_writer_class
	{
		/*
		 * Protected variables
	 *
	 */
		var $structure=array();
		var $nodes=array();

		/*
		 * Public variables
	 *
	 */
		var $stylesheet="";
		var $stylesheettype="text/xsl";
		var $dtdtype="";
		var $dtddefinition="";
		var $dtdurl="";
		var $outputencoding="utf-8";
		var $inputencoding="utf-8";
		var $linebreak="\n";
		var $indenttext=" ";
		var $error="";



		/*
		 * Protected functions
	 *
	 */
		Function escapedata($data)
		{

			$position=0;
			$length=strlen($data);
			$escapeddata="";
			for(;$position<$length;)
			{
				$character=substr($data,$position,1);
				$code=Ord($character);
				switch($code)
				{
					case 34:
						$character="&quot;";
						break;
					case 38:
						$character="&amp;";
						break;
					case 39:
						$character="&apos;";
						break;
					case 60:
						$character="&lt;";
						break;
					case 62:
						$character="&gt;";
						break;
					default:
						if($code<32)
							$character=("&#".strval($code).";");
						break;
				}
				$escapeddata.=$character;
				$position++;
			}
			return $escapeddata;
		}

		Function encodedata($data,&$encodeddata)
		{
			if(!strcmp($this->inputencoding,$this->outputencoding))
				$encodeddata=$this->escapedata($data);
			else
			{
				switch(strtolower($this->outputencoding))
				{
					case "utf-8":
						if(!strcmp(strtolower($this->inputencoding),"iso-8859-1"))
						{
							$encoded_data=utf8_encode($this->escapedata($data));
							$encodeddata=$encoded_data;
						}
						else
						{
							$this->error=("can not encode iso-8859-1 data in ".$this->outputencoding);
							return 0;
						}
						break;
					case "iso-8859-1":
						if(!strcmp(strtolower($this->inputencoding),"utf-8"))
						{
							$decoded=utf8_decode($data);
							$encodeddata=$this->escapedata($decoded);
						}
						else
						{
							$this->error=("can not encode utf-8 data in ".$this->outputencoding);
							return 0;
						}
						break;
					default:
						$this->error=("can not encode data in ".$this->inputencoding);
						return 0;
				}
			}
			return 1;
		}

		Function writetag(&$output,$path,$indent)
		{
			$tag=$this->structure[$path]["Tag"];
			$output.=("<".$tag);
			$attributecount=count($this->structure[$path]["Attributes"]);
			if($attributecount>0)
			{
				$attributes=$this->structure[$path]["Attributes"];
				Reset($attributes);
				$end=(GetType($key=Key($attributes))!="string");
				for(;!$end;)
				{
					$output.=(" ".$key."=\"".$attributes[$key]."\"");
					Next($attributes);
					$end=(GetType($key=Key($attributes))!="string");
				}
			}
			$elements=$this->structure[$path]["Elements"];
			if($elements>0)
			{
				$output.=">";
				$doindent=$this->structure[$path]["Indent"];
				$elementindent=(($doindent) ? $this->linebreak.$indent.$this->indenttext : "");
				$element=0;
				for(;$element<$elements;)
				{
					$elementpath=($path.",".strval($element));
					$output.=$elementindent;
					if(IsSet($this->nodes[$elementpath]))
					{
						if(!($this->writetag($output,$elementpath,$indent.$this->indenttext)))
							return 0;
					}
					else
						$output.=$this->structure[$elementpath];
					$element++;
				}
				$output.=((($doindent) ? $this->linebreak.$indent : "")."</".$tag.">");
			}
			else
				$output.="/>";
			return 1;
		}

		/*
		 * Public functions
	 *
	 */
		Function write(&$output)
		{
			if(strcmp($this->error,""))
				return 0;
			if(!(IsSet($this->structure["0"])))
			{
				$this->error="XML document structure is empty";
				return 0;
			}
			$output=("<?xml version=\"1.0\" encoding=\"".$this->outputencoding."\"?>".$this->linebreak);
			if(strcmp($this->dtdtype,""))
			{
				$output.=("<!DOCTYPE ".$this->structure["0"]["Tag"]." ");
				switch($this->dtdtype)
				{
					case "INTERNAL":
						if(!strcmp($this->dtddefinition,""))
						{
							$this->error="it was not specified a valid internal DTD definition";
							return 0;
						}
						$output.=("[".$this->linebreak.$this->dtddefinition.$this->linebreak."]");
						break;
					case "SYSTEM":
						if(!strcmp($this->dtdurl,""))
						{
							$this->error="it was not specified a valid system DTD url";
							return 0;
						}
						$output.="SYSTEM";
						if(strcmp($this->dtddefinition,""))
							$output.=(" \"".$this->dtddefinition."\"");
						$output.=(" \"".$this->dtdurl."\"");
						break;
					case "PUBLIC":
						if(!strcmp($this->dtddefinition,""))
						{
							$this->error="it was not specified a valid public DTD definition";
							return 0;
						}
						$output.=("PUBLIC \"".$this->dtddefinition."\"");
						if(strcmp($this->dtdurl,""))
							$output.=(" \"".$this->dtdurl."\"");
						break;
					default:
						$this->error="it was not specified a valid DTD type";
						return 0;
				}
				$output.=(">".$this->linebreak);
			}
			if(strcmp($this->stylesheet,""))
			{
				if(!strcmp($this->stylesheettype,""))
				{
					$this->error="it was not specified a valid stylesheet type";
					return 0;
				}
				$output.=("<?xml-stylesheet type=\"".$this->stylesheettype."\" href=\"".$this->stylesheet."\"?>".$this->linebreak);
			}
			if(isset($this->generatedcomment))
			{
				if(strcmp($this->generatedcomment,""))
					$output.=("<!-- ".$this->generatedcomment." -->".$this->linebreak);
			}
			return $this->writetag($output,"0","");
		}

		Function addtag($tag,&$attributes,$parent,&$path,$indent)
		{

			if(strcmp($this->error,""))
				return 0;
			$path=((!strcmp($parent,"")) ? "0" : ($parent.",".strval($this->structure[$parent]["Elements"])));
			if(IsSet($this->structure[$path]))
			{
				$this->error=("tag with path ".$path." is already defined");
				return 0;
			}
			$encodedattributes=array();
			Reset($attributes);
			$end=((GetType($attribute_name=Key($attributes))!="string") or (is_array($attributes[$attribute_name])));
			for(;!$end;)
			{
				$encodedattributes[$attribute_name]="";
				$encoded_data="";

				if(!($this->encodedata($attributes[$attribute_name],$encoded_data)))
					return 0;
				$encodedattributes[$attribute_name]=$encoded_data;
				Next($attributes);
				$end=((GetType($attribute_name=Key($attributes))!="string") or (is_array($attributes[$attribute_name])));
			}

			$this->structure[$path]=array(
					"Tag"=>$tag,
					"Attributes"=>$encodedattributes,
					"Elements"=>0,
					"Indent"=>$indent
			);
			$this->nodes[$path]=1;
			if(strcmp($parent,""))
				$this->structure[$parent]["Elements"]=($this->structure[$parent]["Elements"]+1);
			return 1;
		}

		Function adddata($data,$parent,&$path)
		{
			if(strcmp($this->error,""))
				return 0;
			if(!(IsSet($this->structure[$parent])))
			{
				$this->error=("the parent tag path".$path."is not defined");
				return 0;
			}
			if(!strcmp($data,""))
				return 1;
			$path=($parent.",".strval($this->structure[$parent]["Elements"]));
			$encoded_data="";
			if(!($this->encodedata($data,$encoded_data)))
				return 0;
			$this->structure[$path]=$encoded_data;
			$this->structure[$parent]["Elements"]=($this->structure[$parent]["Elements"]+1);
			return 1;
		}

		Function adddatatag($tag,&$attributes,$data,$parent,&$path)
		{
			$datapath="";
			return $this->addtag($tag,$attributes,$parent,$path,0) && $this->adddata($data,$path,$datapath);
		}
		function xml_fech($fech) {
			$ano = substr($fech,0,4);
			$mes = substr($fech,4,2);
			$dia = substr($fech,6,2);
			$hor = substr($fech,8,2);
			$min = substr($fech,10,2);
			$seg = substr($fech,12,2);
			$aux = $ano."-".$mes."-".$dia."T".$hor.":".$min.":".$seg;
			return ($aux);
		}
	};

}
?>
