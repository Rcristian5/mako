<?php

//Metas variables de productos
function productos($fecha)
{

	$centro_id =  sfConfig::get('app_centro_actual_id');

	$conn = Propel::getConnection();
	//checamos dos  veces, una para la meta como entero (=1) o como monetaria (=0)
	for($i=0; $i<=1;$i++){
		$sql = "SELECT *
		FROM metas
		WHERE categoria_producto_id IS NOT NULL
		AND fecha_inicio < '$fecha'
		AND centro_id = $centro_id
		AND activo = True
		AND entero = '$i'
		ORDER BY fecha_inicio DESC
		LIMIT 1";

		//error_log($sql);

		$metas = $conn->query($sql);

		foreach ($metas as $meta){
			//si es entero hacer count, si es monetario hacer sum
			($meta['entero']==1)? $cuenta = "count(p.producto_id) as cuenta": $cuenta = "sum(o.subtotal) as cuenta";
				
			$sql = "SELECT o.centro_id,
			p.categoria_id,
			".$cuenta."
			FROM detalle_orden as o,
			orden as r,
			categoria_reporte_producto as p,
			subproducto_cobranza as sub
			WHERE
			p.categoria_id = ".$meta['categoria_producto_id']."
			AND o.producto_id = p.producto_id
			AND o.orden_id = r.id
			AND sub.producto_id = o.producto_id
			AND sub.numero_pago = 1
			AND r.created_at::date = '".$fecha."'
			AND o.centro_id = $centro_id
			GROUP BY o.centro_id,
			p.categoria_id";

			//error_log($sql);

			$datos = $conn->query($sql);
			insertaMetas(
					$datos,
					"NULL",
					$meta['categoria_producto_id'],
					"NULL",
					$fecha,
					($meta['entero'])?$meta['entero']:'0',
					$meta['meta']/6);
		}
	}
}

//Metas variables de becas
function becas($fecha)
{

	$centro_id =  sfConfig::get('app_centro_actual_id');

	$conn = Propel::getConnection();
	$sql = "SELECT *
	FROM metas
	WHERE categoria_beca_id IS NOT NULL
	AND fecha_inicio < '$fecha'
	AND centro_id = $centro_id
	AND activo = True
	ORDER BY fecha_inicio DESC
	LIMIT 1";

	//error_log($sql);
	$metas = $conn->query($sql);

	foreach ($metas as $meta){
		$sql = "SELECT count(*)
		FROM asignacion_beca
		WHERE beca_id = ".$meta['categoria_beca_id']."
		AND fecha_solicitud = '".$fecha."'
		AND centro_id = $centro_id";

		//error_log($sql);
		$datos = $conn->query($sql);
		insertaMetas($datos, 'NULL', 'NULL', $meta['categoria_beca_id'], $fecha, 1, $meta['meta']/6);
	}
}




function fijas($fecha)
{

	$centro_id =  sfConfig::get('app_centro_actual_id');

	$conn = Propel::getConnection();

	//socio
	$sql = "SELECT *
	FROM metas
	WHERE categoria_fija_id = 1
	AND fecha_inicio < '$fecha'
	AND centro_id = $centro_id
	AND activo = True
	ORDER BY fecha_inicio DESC
	LIMIT 1";
	$metas = $conn->query($sql);
	$meta = $metas->fetch();


	$sql = "SELECT count(id) as cuenta
	FROM socio
	WHERE created_at::date = '".$fecha."'
	AND centro_id = $centro_id";

	//error_log($sql);
	$datos = $conn->query($sql);
	insertaMetas($datos, 1, 'NULL', 'NULL', $fecha, 1,$meta['meta']/6);

	//inscritos
	$sql = "SELECT *
	FROM metas
	WHERE categoria_fija_id = 2
	AND fecha_inicio < '$fecha'
	AND centro_id = $centro_id
	AND activo = True
	ORDER BY fecha_inicio DESC
	LIMIT 1";
	$metas = $conn->query($sql);
	$meta = $metas->fetch();

	$sql = "SELECT sum(cuenta) as cuenta FROM
	(
	(
	SELECT
	count(*) AS cuenta
	FROM
	detalle_orden AS d
	JOIN subproducto_cobranza AS s ON (d.producto_id = s.producto_id)
	JOIN orden AS o ON (d.orden_id = o.id)
	WHERE
	o.created_at::date = '$fecha'
	AND s.numero_pago = 1
	AND o.tipo_id = 1
	AND o.centro_id = $centro_id
	)
	UNION ALL
	(
	SELECT
	count(*) AS cuenta
	FROM
	detalle_orden AS d
	JOIN orden AS o ON (d.orden_id = o.id)
	JOIN producto as p ON (d.producto_id = p.id)
	JOIN producto_curso AS c on (c.producto_id = d.producto_id)
	WHERE
	o.created_at::date = '$fecha'
	AND p.es_subproducto = false
	AND o.tipo_id = 1
	AND o.centro_id = $centro_id
	)
	) AS x";
	//error_log($sql);
	$datos = $conn->query($sql);
	insertaMetas($datos, 2, 'NULL', 'NULL', $fecha, 1,$meta['meta']/6);

	//cobranza
	$sql = "SELECT *
	FROM metas
	WHERE categoria_fija_id = 3
	AND fecha_inicio < '$fecha'
	AND centro_id = $centro_id
	AND activo = True
	ORDER BY fecha_inicio DESC
	LIMIT 1";
	$metas = $conn->query($sql);
	$meta = $metas->fetch();

	$sql = "SELECT count(id), sum(total) as cuenta
	FROM orden
	WHERE created_at::date = '".$fecha."'
	AND tipo_id IN (1,3)
	AND centro_id = $centro_id";
	//error_log($sql);
	$datos = $conn->query($sql);
	insertaMetas($datos, 3, 'NULL', 'NULL', $fecha, '0',$meta['meta']/6);
}

function insertaMetas($datos, $fija, $producto, $beca, $fecha, $entero=1, $metaDiaria=1){

	$conn = Propel::getConnection();
	$id = Comun::generaId();
	$centroId =  sfConfig::get('app_centro_actual_id');

	foreach ($datos as $d){
		if(empty($d['cuenta'])){
			$d['cuenta']=0;
		}

		$sql="INSERT INTO reporte_metas
		(id, centro_id, categoria_fija_id, categoria_producto_id, categoria_beca_id, cantidad, fecha, semana, anio,
		entero,
		meta_diaria)
		VALUES ($id,
		$centroId,
		$fija,
		$producto,
		$beca,
		$d[cuenta],
		'$fecha',
		".date('W',strtotime($fecha)).",
		".date('Y',strtotime($fecha)).",
		'$entero',
		$metaDiaria)";

		//error_log($sql);
		$conn->query($sql);
		//}
		return;
	}
}

?>
