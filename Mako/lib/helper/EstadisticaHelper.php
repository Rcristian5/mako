<?php

/**
 * Regresa la cadena formateada de productos para impresión de ticket
 * @param Orden $orden
 * @return string
 */


/*
 * POS
*/
//Inscripciones Cant - Global
function estadisticaPOSInscripciones($inicio=NULL, $fin=NULL)
{

	if($inicio != NULL)
		$whr = " WHERE created_at BETWEEN '$inicio' AND '$fin'";

	$conn = Propel::getConnection();
	$sql = "SELECT count(*) as cuenta
	FROM socio
	$whr";

	$inscripciones = $conn->query($sql);

	return $inscripciones;


}

//Inscripciones por Curso / Paquete
function estadisticaPOSInscripcionesCurso($inicio=NULL, $fin=NULL)
{

	if($inicio != NULL)
		$whr = " AND o.created_at BETWEEN '$inicio' AND '$fin'";

	$conn = Propel::getConnection();
	$sql = "SELECT count(*) as cuenta
	FROM detalle_orden as d,
	producto as p,
	orden as o
	WHERE d.producto_id = p.id
	AND p.categoria_id = 2
	AND d.orden_id = o.id
	$whr";

	$datos = $conn->query($sql);

	return $datos;

}


function estadisticaPOSVentasCategoria($inicio=NULL, $fin=NULL)
{

	if($inicio != NULL)
		$whr = " AND o.created_at BETWEEN '$inicio' AND '$fin'";

	$conn = Propel::getConnection();
	$sql = "SELECT cp.nombre as label,
	SUM( do.precio_lista * cantidad) AS cuenta
	FROM orden as o,
	detalle_orden as do,
	producto AS p,
	categoria_producto as cp
	WHERE do.producto_id = p.id
	AND p.categoria_id = cp.id
	AND o.id = do.orden_id
	$whr
	GROUP BY cp.nombre
	";

	$datos = $conn->query($sql);

	return $datos;

}






//Socio

function estadisticaSociosGenero($inicio=NULL, $fin=NULL)
{

	if($inicio != NULL)
		$whr = " WHERE created_at BETWEEN '$inicio' AND '$fin'";

	$conn = Propel::getConnection();
	$sql = "SELECT CASE WHEN sexo = 'F'
	THEN 'Femenino'
	ELSE 'Masculino'
	END AS genero,
	count(*) as cuenta
	FROM socio
	$whr
	GROUP BY sexo";

	$genero = $conn->query($sql);

	return $genero;


}


function estadisticaSociosEdad($inicio=NULL, $fin=NULL)
{
	if($inicio != NULL)
		$whr = " WHERE created_at BETWEEN '$inicio' AND '$fin'";

	$conn = Propel::getConnection();
	$sql = "SELECT fecha_nac
	FROM socio
	$whr";

	$edad = $conn->query($sql);

	return $edad;


}




function estadisticaSociosEscolaridad($inicio=NULL, $fin=NULL)
{
	if($inicio != NULL)
		$whr = " AND s.created_at BETWEEN '$inicio' AND '$fin'";

	$conn = Propel::getConnection();
	$sql = "SELECT e.nombre as estudio,
	count(*) as cuenta
	FROM socio as s,
	nivel_estudio as e
	WHERE
	s.nivel_id = e.id
	$whr
	GROUP BY e.id, e.nombre";

	$escolaridad = $conn->query($sql);

	return $escolaridad;


}


function estadisticaSociosColonia($inicio=NULL, $fin=NULL)
{
	if($inicio != NULL)
		$whr = " WHERE created_at BETWEEN '$inicio' AND '$fin'";

	$conn = Propel::getConnection();
	$sql = "SELECT trim(colonia) as colonia,
	count(*) as cuenta
	FROM socio
	$whr
	GROUP BY trim(colonia)
	ORDER BY cuenta DESC
	LIMIT 10";

	$colonia = $conn->query($sql);

	return $colonia;


}

function estadisticaSociosEmpresa($inicio=NULL, $fin=NULL)
{
	if($inicio != NULL)
		$whr = "AND s.created_at BETWEEN '$inicio' AND '$fin'";

	$conn = Propel::getConnection();
	$sql = "SELECT trim(e.nombre) as empresa,
	count(*) as cuenta
	FROM socio as s,
	empresa as e
	WHERE s.empresa = e.id
	$whr
	GROUP BY trim(e.nombre)
	ORDER BY cuenta DESC
	LIMIT 10";

	$empresa = $conn->query($sql);

	return $empresa;


}


//Servicio


function estadisticaServicio($inicio=NULL, $fin=NULL)
{

	if($inicio != NULL)
		$whr = " AND orden.created_at BETWEEN '$inicio' AND '$fin'";

	$conn = Propel::getConnection();
	$sql = "SELECT p.nombre as paquete,
	count(*) as cuenta
	FROM producto as p,
	detalle_orden as o,
	orden
	WHERE o.producto_id = p.id
	AND orden.id = o.orden_id
	AND p.tipo_id = 3
	$whr
	GROUP BY p.codigo,p.nombre
	ORDER BY cuenta DESC
	LIMIT 25";

	$paquete = $conn->query($sql);

	return $paquete;


}


//Etapas Proceso
function estadisticaEtapasProceso($inicio=NULL, $fin=NULL)
{
	//TODO: checar de donde tomar la fecha de inicio
	//if($inicio != NULL)
	//$whr = " AND a.created_at BETWEEN '$inicio' AND '$fin'";

	$conn = Propel::getConnection();
	$sql = "SELECT c.nombre as curso,
	cat.nombre as categoria,
	g.clave,
	count(ga.grupo_id) as cuenta
	FROM curso AS c ,
	grupo as g,
	categoria_curso AS cat,
	grupo_alumnos as ga
	WHERE c.categoria_curso_id = cat.id
	AND g.curso_id = c.id
	AND ga.grupo_id = g.id
	$whr
	GROUP BY c.nombre,  ga.grupo_id ,cat.nombre, g.clave
	ORDER BY cuenta DESC
	LIMIT 25";

	$proceso = $conn->query($sql);

	return $proceso;


}



function addDate($date,$day)//add days
{
	$sum = strtotime(date("Y-m-d", strtotime("$date")) . " +$day days");
	$dateTo=date('Y-m-d',$sum);
	return $dateTo;
}



function rangoEdad($estadisticaSociosEdad){


	$edad = array();
	$rangoEdad = array();
	foreach ($estadisticaSociosEdad as $row)
	{
		$edad[] =  Comun::getEdad($row['fecha_nac']);
	}

	$rangoEdad = (array_count_values($edad));
	//6 a 8
	$edadL[0] = '6 a 8';
	$rangoEdad6a8 = range(0,8);
	foreach ($rangoEdad6a8 as $llave => $valor){

		if(array_key_exists($valor, $rangoEdad)){
			$edadV[0] +=  $rangoEdad[$valor];
				
		}
		else $edadV[0] +=  0;
	}

	// 9 a 12
	$edadL[1] = '9 a 12';
	$rangoEdad9a12 = range(9,12);
	foreach ($rangoEdad9a12 as $llave => $valor){

		if(array_key_exists($valor, $rangoEdad)){
			$edadV[1] +=  $rangoEdad[$valor];
				
		}
		else $edadV[1] +=  0;
	}
		

	// 13 a 15
	$edadL[2] = '13 a 15';
	$rangoEdad13a15 = range(13,15);
	foreach ($rangoEdad13a15 as $llave => $valor){

		if(array_key_exists($valor, $rangoEdad)){
			$edadV[2] +=  $rangoEdad[$valor];
				
		}
		else $edadV[2] +=  0;
	}
		
	// 16 a 25
	$edadL[3] = '16 a 25';
	$rangoEdad16a25 = range(16,25);
	foreach ($rangoEdad16a25 as $llave => $valor){

		if(array_key_exists($valor, $rangoEdad)){
			$edadV[3] +=  $rangoEdad[$valor];
				
		}
		else $edadV[3] +=  0;
	}
		
	// 26 a 35
	$edadL[4] = '26 a 35';
	$rangoEdad26a35 = range(26,35);
	foreach ($rangoEdad26a35 as $llave => $valor){
		if(array_key_exists($valor, $rangoEdad)){
			$edadV[4] +=  $rangoEdad[$valor];
				
		}
		else $edadV[4] +=  0;
	}
		
	// 35 en adelante
	$edadL[5] = '35 en adelante';
	$rangoEdad36 = range(36,105);
	foreach ($rangoEdad36 as $llave => $valor){

		if(array_key_exists($valor, $rangoEdad)){
			$edadV[5] +=  $rangoEdad[$valor];
				
		}
		else $edadV[5] +=  0;
	}


	return array('edadV' => $edadV, 'edadL' => $edadL);


}

?>