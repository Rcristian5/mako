<?php
/**
 * Funciones helper para la operación con el servidor LDAP
 */

define('LDAP_MASTER_HOST',sfConfig::get('app_ldap_master_host'));
define('LDAP_MASTER_PORT',sfConfig::get('app_ldap_master_port'));
define('LDAP_MASTER_RDN',sfConfig::get('app_ldap_master_rdn'));
define('LDAP_MASTER_BDN',sfConfig::get('app_ldap_master_bdn'));
define('LDAP_MASTER_PWD',sfConfig::get('app_ldap_master_pwd'));

/**
 * Agrega al LDAP a los socios nuevos.
 * @param string $usuario
 * @param string $nombre
 * @param string $apepat
 * @param string $clave
 * @param string $email
 * @param string $estado
 * @param string $dir_comp
 * @param string $tel
 * @param string $celular
 * @return mixed integer en caso de exito, false en caso de fallo
 */
function agregarSocioLDAP($usuario,$nombre,$apepat,$clave,$email,$estado,$dir_comp,$tel,$celular,$alias_centro,$centro_id,$usuario_id,$ldap_uid=null){

	$info = array();
	//Campos de compatibilidad
	$info["description"] = "Usuario|".$centro_id."|".$usuario_id;
	$info["l"] = "MX";
	$info["preferredLanguage"] = "ES";
	$info["st"] = $estado;
	if($tel !='')
	{
		$info["telephoneNumber"][]=$tel;
	}
	if($celular !='')
	{
		$info["telephoneNumber"][]=$celular;
	}
	$info["registeredAddress"] = $dir_comp;

	//El siguiente numero
	if($ldap_uid == null)
	{
		$uidNumber = siguienteUID($alias_centro);
	}
	else
	{
		$uidNumber = $ldap_uid;
	}

	//Encriptamos con MD5 la clave para uso de los linux
	$clave_linux = '{MD5}' . base64_encode(mhash(MHASH_MD5, $clave));

	$ldapconn = ldap_connect(LDAP_MASTER_HOST, LDAP_MASTER_PORT);

	ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);

	if ($ldapconn) {

		// binding anonimo
		$ldapbind = ldap_bind($ldapconn, LDAP_MASTER_RDN, LDAP_MASTER_PWD);

		// verify binding
		if ($ldapbind) {

		} else {
			error_log("ERROR:[ldap] Fallo el bind del ldap. -host: ".LDAP_MASTER_HOST);
			throw new MakoLdapException("Error al conectar con el servidor LDAP: ".LDAP_MASTER_HOST);
			return false;
		}

	}

	$bdn = "ou=people,".LDAP_MASTER_BDN;

	//REGLA DE NEGOCIO: a todos los usuarios inscritos se les deben regalar 5 hrs de internet
	//Por lo tanto sus cuentas deben estar activas desde el registro.
	$shadowExpire = 1577836800;

	$info["cn"]= array($usuario);
	$info["givenName"]= array($nombre);
	$info["sn"]= array($apepat);
	$info["uid"]= array($usuario);
	if(trim($email)!='')
	{
		$info["mail"]= array($email);
	}
	else
	{
		//Esto es para evitar que el moodle pida el correo sin dejar al usuario pasar.
		$info["mail"]= array("cursos@clubidea.com.mx");
	}

	$info["displayName"]=$usuario;
	$info["gecos"]=$usuario;
	$info["loginShell"]='/bin/bash';

	//Info pa linuxes (posixAccount)
	$info["uidNumber"]= $uidNumber;
	$info["gidNumber"]= 513; //Grupo de usuarios
	$info["homeDirectory"]= "/home/usuarios/$usuario";

	//Info para el host
	$info["host"] = '*';

	//Info pal shadow ()
	$info["shadowExpire"] = $shadowExpire;
	$info["userPassword"] = $clave_linux;
	$info["objectclass"]=array(
			"top",
			"person",
			"organizationalPerson",
			"inetOrgPerson",
			"posixAccount",
			"hostObject",
			"shadowAccount" );

	error_log(print_r($info,true));
	// add data to directory
	//Modificado por Yanett
	$r = ldap_add($ldapconn, "uid=$usuario,".$bdn, $info);
	if(!$r)
	{
		error_log("ERROR:[ldap] Num: ".ldap_errno($ldapconn). " msg: ".ldap_err2str( ldap_errno($ldapconn) ));
		return false;
	}
	ldap_close($ldapconn);
	return $uidNumber;
}

/**
 * Regresa el siguiente unix id del centro
 * @param string $centro_alias
 * @return integer
 */
function siguienteUID($centro_alias){

	$ldapconn = ldap_connect(LDAP_MASTER_HOST, LDAP_MASTER_PORT);

	ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);

	if ($ldapconn) {
		// binding anonimo
		$ldapbind = ldap_bind($ldapconn, LDAP_MASTER_RDN, LDAP_MASTER_PWD);

		// verify binding
		if ($ldapbind)
		{

		}
		else
		{
			error_log("ERROR:[ldap] Fallo el bind del ldap.");
			throw new MakoLdapException("Error al conectar con el servidor LDAP: ".LDAP_MASTER_HOST);
			return false;
		}
	}

	$centro = strtolower($centro_alias);

	$res = ldap_search($ldapconn, 'ou=UIDs,'.LDAP_MASTER_BDN, 'cn=NextFreeUnixId-'.$centro);
	$information = ldap_get_entries($ldapconn, $res);
	ldap_modify($ldapconn, 'cn=NextFreeUnixId-'.$centro.',ou=UIDs,'.LDAP_MASTER_BDN, array('uidnumber' => $information[0]['uidnumber'][0] + 1));
	ldap_close($ldapconn);
	//$qry = 'ou=UIDs,'.LDAP_MASTER_BDN. ',cn=NextFreeUnixId-'.$centro;
	//error_log("$qry El uid: ".$information[0]['uidnumber'][0]);
	return $information[0]['uidnumber'][0];
}

/**
 * Verifica la existencia del username en el ldap, para evitar colisiones con ldap.
 * @param string $usuario
 * @return boolean
 */
function verificaUsuarioLDAP($usuario){

	$ldapconn = ldap_connect(LDAP_MASTER_HOST, LDAP_MASTER_PORT);

	ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);

	if ($ldapconn) {
		// binding anonimo
		$ldapbind = ldap_bind($ldapconn, LDAP_MASTER_RDN, LDAP_MASTER_PWD);

		// verify binding
		if ($ldapbind)
		{

		}
		else
		{
			error_log("ERROR:[ldap] Fallo el bind del ldap.");
			throw new MakoLdapException("Error al conectar con el servidor LDAP: ".LDAP_MASTER_HOST);
			return 0;
		}
	}
	//Modificado por Yanett
	$res = ldap_search($ldapconn, LDAP_MASTER_BDN, "cn=$usuario");
	$information = ldap_get_entries($ldapconn, $res);
	if($information['count'] > 0)
	{
		error_log("ERROR:[ldapCoincidenciaUsuarioRegistro]: $usuario");
		return true;
	}
	else
	{
		return false;
	}
}
/**
 * Cancela la cuenta del usuario en el arbol LDAP pero no la borra
 * @param bigint $id_usuario
 * @return void
 */
function cancelarLDAP($id_usuario,$usuario){

	//Encriptamos con MD5 la clave
	$clave = '{MD5}' . base64_encode(mhash(MHASH_MD5, ""));

	$ldapconn = ldap_connect(LDAP_MASTER_HOST, LDAP_MASTER_PORT);

	ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);

	if ($ldapconn) {

		// binding anonimo
		//$ldapbind = ldap_bind($ldapconn);
		$ldapbind = ldap_bind($ldapconn, LDAP_MASTER_RDN, LDAP_MASTER_PWD);

		// verify binding
		if ($ldapbind) {

		} else {
			error_log("ERROR:[ldap] Fallo el bind del ldap.");
			throw new MakoLdapException("Error al conectar con el servidor LDAP: ".LDAP_MASTER_HOST);
			return false;
		}

	}

	$bdn = "ou=people,".LDAP_MASTER_BDN;

	$passwd = $clave;

	//Info pal shadow ()
	$info["shadowExpire"] = 1;

	$info["objectclass"]=array(
			"top",
			"person",
			"organizationalPerson",
			"inetOrgPerson",
			"hostObject",
			"posixAccount","shadowAccount" );

	// Modificamos el dato
	$r = ldap_modify($ldapconn, "uid=$usuario, ".$bdn, $info);

	if(!$r)
	{
		error_log("ERROR:[ldap] ". ldap_err2str( ldap_errno($ldapconn) ));
	}
	ldap_close($ldapconn);
}

/**
 * Modifica los datos del socio en el ldap
 * @param bigint $id_usuario
 * @param string $nombre
 * @param string $apepat
 * @param string $clave
 * @param string $email
 * @param string $estado
 * @param string $dir_comp
 * @param string $tel
 * @param string $celular
 * @return void
 */
function modificarSocioLDAP($usuario,$nombre,$apepat,$clave,$email,$estado,$dir_comp,$tel,$celular,$cambioUsuario,$usuario_anterior,$centro_id,$socio_id,$ldap_uid = null){

	//Campos de compatibilidad
	$info["description"] = "Usuario|$centro_id|$socio_id";
	$info["l"] = "MX";
	$info["preferredLanguage"] = "ES";
	$info["st"] = $estado;
	if($tel !='')
	{
		$info["telephoneNumber"][]=$tel;
	}
	if($celular !='')
	{
		$info["telephoneNumber"][]=$celular;
	}
	$info["registeredAddress"] = $dir_comp;

	//Encriptamos con MD5 la clave
	$clave = '{MD5}' . base64_encode(mhash(MHASH_MD5, $clave));

	$ldapconn = ldap_connect(LDAP_MASTER_HOST, LDAP_MASTER_PORT);

	ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);

	if ($ldapconn) {

		// binding anonimo
		$ldapbind = ldap_bind($ldapconn, LDAP_MASTER_RDN, LDAP_MASTER_PWD);

		// verify binding
		if ($ldapbind) {

		} else {
			error_log("ERROR:[ldap] Fallo el bind del ldap.");
			throw new MakoLdapException("Error al conectar con el servidor LDAP: ".LDAP_MASTER_HOST);
			return false;
		}

	}

	$bdn = "ou=people,".LDAP_MASTER_BDN;

	$info["givenName"]= array($nombre);
	$info["sn"]= array($apepat);

	if(trim($email)!='')
	{
		$info["mail"]= array($email);
	}
	else
	{
		//Esto es para evitar que el moodle pida el correo sin dejar al usuario pasar.
		$info["mail"]= array("cursos@clubidea.org.mx");
	}

	$passwd = $clave;

	$info["userPassword"] = $passwd;

	if($ldap_uid != null)
	{
		$info["uidNumber"]= $uidNumber;
	}


	$info["objectclass"]=array(
			"top",
			"person",
			"organizationalPerson",
			"inetOrgPerson",
			"hostObject",
			"posixAccount","shadowAccount" );

	// Modificamos el dato
	if($cambioUsuario)
	{
		$info["cn"]= array($usuario);
		$info["displayName"]=$usuario;
		$info["gecos"]=$usuario;
		$info["homeDirectory"]= "/home/usuarios/$usuario";

		$r = ldap_modify($ldapconn, "uid=$usuario_anterior, ".$bdn, $info);
		$r = ldap_rename($ldapconn, "uid=$usuario_anterior, ".$bdn, "uid=$usuario", null,true);

	}
	else
	{
		$r = ldap_modify($ldapconn, "uid=$usuario, ".$bdn, $info);
	}
	if(!$r)
	{
		error_log("ERROR:[ldap:modificarSocioLDAP] x ". ldap_err2str( ldap_errno($ldapconn) ). "uid=$usuario, ".$bdn);
	}
	ldap_close($ldapconn);
}

/**
 * Agrega al LDAP a los usuarios nuevos.
 * @param string $usuario
 * @param string  $nombre
 * @param string  $apepat
 * @param string  $clave
 * @param string  $email
 * @return void
 */
function agregarUsuarioLDAP($usuario,$nombre,$apepat,$clave,$email,$alias_centro){

	$info = array();
	//Campos de compatibilidad
	$info["description"] = "Operator";
	$info["l"] = "MX";
	$info["preferredLanguage"] = "ES";
	$info["st"] = "ESTADO DE MEXICO";

	//El siguiente numero
	$uidNumber = siguienteUID($alias_centro);

	//Encriptamos con MD5 la clave para uso de los linux
	$clave_linux = '{MD5}' . base64_encode(mhash(MHASH_MD5, $clave));

	$ldapconn = ldap_connect(LDAP_MASTER_HOST, LDAP_MASTER_PORT);

	ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);

	if ($ldapconn) {

		// binding anonimo
		$ldapbind = ldap_bind($ldapconn, LDAP_MASTER_RDN, LDAP_MASTER_PWD);

		// verify binding
		if ($ldapbind) {

		} else {
			error_log("ERROR:[ldap] Fallo el bind del ldap.");
			throw new MakoLdapException("Error al conectar con el servidor LDAP: ".LDAP_MASTER_HOST);
			return false;
		}

	}

	$bdn = "ou=people,".LDAP_MASTER_BDN;

	$shadowExpire = 1577836800;

	$info["cn"]= array($usuario);
	$info["givenName"]= array($nombre);
	$info["sn"]= array($apepat);
	$info["uid"]= array($usuario);
	if(trim($email)!='')
	{
		$info["mail"]= array($email);
	}
	else
	{
		//Esto es para evitar que el moodle pida el correo sin dejar al usuario pasar.
		$info["mail"]= array("cursos@clubidea.com.mx");
	}

	$info["displayName"]=$usuario;
	$info["gecos"]=$usuario;
	$info["loginShell"]='/bin/bash';

	//Info pa linuxes (posixAccount)
	$info["uidNumber"]= $uidNumber;
	$info["gidNumber"]= 513; //Grupo de usuarios
	$info["homeDirectory"]= "/home/usuarios/$usuario";

	//Info para el host
	$info["host"] = '*';

	//Info pal shadow ()
	$info["shadowExpire"] = $shadowExpire;
	$info["userPassword"] = $clave_linux;
	$info["objectclass"]=array(
			"top",
			"person",
			"organizationalPerson",
			"inetOrgPerson",
			"posixAccount",
			"hostObject",
			"shadowAccount" );

	// add data to directory
	$r = ldap_add($ldapconn, "uid=$usuario, ".$bdn, $info);
	if(!$r)
	{
		error_log("ERROR:[ldap] uid=$usuario,$bdn Num: ".ldap_errno($ldapconn). " msg: ".ldap_err2str( ldap_errno($ldapconn) ));
	}
	ldap_close($ldapconn);
}

/**
 * Se encarga de leer los valores de grupo para los cursos.
 * @param bigint $id_usuario
 * @param string $usuario
 * @return integer
 */
function leeUserUID($usuario){


	$ldapconn = ldap_connect(LDAP_MASTER_HOST, LDAP_MASTER_PORT);

	ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);

	if ($ldapconn) {
		// binding anonimo
		$ldapbind = ldap_bind($ldapconn, LDAP_MASTER_RDN, LDAP_MASTER_PWD);
		if ($ldapbind)
		{

		}
		else
		{
			error_log("ERROR:[ldap] Fallo el bind del ldap.");
			throw new MakoLdapException("Error al conectar con el servidor LDAP: ".LDAP_MASTER_HOST);
			return false;
		}
	}

	$res = ldap_search($ldapconn, LDAP_MASTER_BDN, "uid=$usuario");
	$information = ldap_get_entries($ldapconn, $res);

	ldap_close($ldapconn);
	return $information[0]['uidnumber'][0];
}

/**
 * Agrega el uid del usuario al member uid del curso
 * @param $id_usuario
 * @param $usuario
 * @param $email
 * @param $clave_curso
 * @return boolean
 */
function asociaSocioCursoLDAP($usuario,$email,$clave_curso){


	$memberuid = leeUserUID($usuario);

	$ldapconn = ldap_connect(LDAP_MASTER_HOST, LDAP_MASTER_PORT);

	ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);

	if ($ldapconn) {
		// binding anonimo
		$ldapbind = ldap_bind($ldapconn, LDAP_MASTER_RDN, LDAP_MASTER_PWD);
		if ($ldapbind)
		{

		}
		else
		{
			error_log("ERROR:[ldap] Fallo el bind del ldap.");
			throw new MakoLdapException("Error al conectar con el servidor LDAP: ".LDAP_MASTER_HOST);
			return false;
		}
	}
	$entry = array();
	$entryu = array();

	$entry['memberUid']= $memberuid;

	//El moodle requiere siempre que el usuario tenga un correo.
	if($email == '')
	{
		$entryu['mail']= array('cursos@clubidea.com.mx');
	}
	else
	{
		$entryu['mail']= array($email);
	}

	$bdnu = "ou=people,".LDAP_MASTER_BDN;
	$ru = ldap_modify($ldapconn, "uid=$usuario, ".$bdnu, $entryu);

	$bdn = "ou=Estudiantes,".LDAP_MASTER_BDN;
	$dn = "cn=$clave_curso, $bdn";

	$r = ldap_mod_add($ldapconn, "cn=$clave_curso, $bdn", $entry);
	$erldap = ldap_err2str( ldap_errno($ldapconn));

	if($erldap != '')
	{
		ldap_close($ldapconn);
		error_log("ERROR[ldap]: Asocia curso grupo moodle.Usr:$usuario:$memberuid cn=$clave_curso, $bdn ".$erldap);
		return false;
	}
	ldap_close($ldapconn);
	return true;
}

/**
 * Modifica los datos del usuario en el arbol LDAP
 * @param string $usuario
 * @param string $nombre
 * @param string $apepat
 * @param string $apemat
 * @param string $clave
 * @param string $email
 * @return void
 */
function modificarUsuarioLDAP($usuario,$nombre,$apepat,$clave,$email,$usuarioModificado,$usuario_anterior){


	//Encriptamos con MD5 la clave
	$clave = '{MD5}' . base64_encode(mhash(MHASH_MD5, $clave));

	$ldapconn = ldap_connect(LDAP_MASTER_HOST, LDAP_MASTER_PORT);

	ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);

	if ($ldapconn) {

		$ldapbind = ldap_bind($ldapconn, LDAP_MASTER_RDN, LDAP_MASTER_PWD);

		// verify binding
		if ($ldapbind) {

		} else {
			error_log("ERROR:[ldap] Fallo el bind del ldap.");
			throw new MakoLdapException("Error al conectar con el servidor LDAP: ".LDAP_MASTER_HOST);
			return false;
		}
	}

	$bdn = "ou=people,".LDAP_MASTER_BDN;

	$info["givenName"]= array($nombre);
	$info["sn"]= array($apepat);
	$info["mail"]= array($email);

	$passwd = $clave;

	$info["userPassword"] = $passwd;
	$info["objectclass"]=array("top",
			"person",
			"organizationalPerson",
			"inetOrgPerson",
			"posixAccount",
			"hostObject",
			"shadowAccount" );

	// Modificamos el dato
	if($usuarioModificado)
	{
		$info["cn"]= array($usuario);
		$info["displayName"]=$usuario;
		$info["gecos"]=$usuario;
		$info["homeDirectory"]= "/home/usuarios/$usuario";

		$r = ldap_modify($ldapconn, "uid=$usuario_anterior, ".$bdn, $info);
		$r = ldap_rename($ldapconn, "uid=$usuario_anterior, ".$bdn, "uid=$usuario", null,true);
	}
	else
	{
		$r = ldap_modify($ldapconn, "uid=$usuario, ".$bdn, $info);
	}


	if(!$r)
	{
		error_log("uid=$usuario_anterior, ".$bdn);
		error_log("ERROR:[ldap] ".ldap_errno($ldapconn)." ". ldap_err2str( ldap_errno($ldapconn) ));
	}
	ldap_close($ldapconn);
}

/**
 * Agrega un nuevo curso al arbol
 * @param string $clave
 * @return bool
 */
function agregarCurso($clave,$nombre){
	$info = array();

	$info["cn"]= array($clave);
	$info["description"]= array($nombre);
	$info["gidNumber"]= maxUidCursos()+1;

	$info["objectclass"]=array(
			"top",
			"posixGroup"
	);

	$ldapconn = ldap_connect(LDAP_MASTER_HOST, LDAP_MASTER_PORT);
	ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);
	if ($ldapconn)
	{
		$ldapbind = ldap_bind($ldapconn, LDAP_MASTER_RDN, LDAP_MASTER_PWD);
		if ($ldapbind) {

		}
		else
		{
			error_log("ERROR:[ldap] Fallo el bind del ldap. -host: ".LDAP_MASTER_HOST);
			throw new MakoLdapException("Error al conectar con el servidor LDAP: ".LDAP_MASTER_HOST);
			return false;
		}
	}
	$bdn = "ou=Estudiantes,".LDAP_MASTER_BDN;
	$r = ldap_add($ldapconn, "cn=$clave,".$bdn, $info);
	if(!$r)
	{
		$msg = ldap_err2str( ldap_errno($ldapconn) );
		error_log("ERROR:[ldap] Num: ".ldap_errno($ldapconn). " msg: ".$msg." En Curso: $clave");
		return false;
	}
	else
	{
		ldap_close($ldapconn);
		return true;
	}

}

/**
 * Modifica los datos de un curso
 * @param string $clave_anterior,
 * @param string $clave_nueva,
 * @param string $nombre,
 * @return bool
 */
function modificarCurso($clave_actual,$clave_nueva,$nombre){
	$info = array();

	$ldapconn = ldap_connect(LDAP_MASTER_HOST, LDAP_MASTER_PORT);
	ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);
	if ($ldapconn)
	{
		$ldapbind = ldap_bind($ldapconn, LDAP_MASTER_RDN, LDAP_MASTER_PWD);
		if ($ldapbind) {

		}
		else
		{
			error_log("ERROR:[ldap] Fallo el bind del ldap. -host: ".LDAP_MASTER_HOST);
			throw new MakoLdapException("Error al conectar con el servidor LDAP: ".LDAP_MASTER_HOST);
			return false;
		}
	}

	$bdn = "ou=Estudiantes,".LDAP_MASTER_BDN;

	$info["description"]= array($nombre);
	$info["objectclass"]=array(
			"top",
			"posixGroup"
	);

	if($clave_nueva != $clave_actual)
	{
		$r = ldap_modify($ldapconn, "cn=$clave_actual, ".$bdn, $info);
		$r = ldap_rename($ldapconn, "cn=$clave_actual, ".$bdn, "cn=$clave_nueva", null,true);
	}
	else
	{
		$r = ldap_modify($ldapconn, "cn=$clave_actual, ".$bdn, $info);
	}

	if(!$r)
	{
		$msg = ldap_err2str( ldap_errno($ldapconn) );
		error_log("ERROR:[ldap] Num: ".ldap_errno($ldapconn). " msg: ".$msg." En Curso: $clave");
		return false;
	}
	else
	{
		ldap_close($ldapconn);
		return true;
	}

}

/**
 * Regresa el uid maximo para los cursos.
 * @return integer
 */
function maxUidCursos()
{
	$ldapconn = ldap_connect(LDAP_MASTER_HOST, LDAP_MASTER_PORT);
	ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);
	if ($ldapconn)
	{
		$ldapbind = ldap_bind($ldapconn, LDAP_MASTER_RDN, LDAP_MASTER_PWD);
		if ($ldapbind) {

		}
		else
		{
			error_log("ERROR:[ldap] Fallo el bind del ldap. -host: ".LDAP_MASTER_HOST);
			throw new MakoLdapException("Error al conectar con el servidor LDAP: ".LDAP_MASTER_HOST);
			return false;
		}
	}

	$bdn = "ou=estudiantes,".LDAP_MASTER_BDN;
	$res = ldap_search($ldapconn, $bdn,'cn=*',array('gidNumber'),0);

	$information = ldap_get_entries($ldapconn, $res);
	$max = 5000;
	$c = $information['count'];
	$a = array();
	for($i=0;$i<$c;$i++)
	{
		if($max < $information[$i]['gidnumber'][0])
		{
			$max = $information[$i]['gidnumber'][0];
		}
	}
	ldap_close($ldapconn);
	return $max;
}


/**
 * Modifica la clave del usuario
 * @param string $usuario
 * @param string $clave
 */
function modificarClaveSocioLDAP($usuario,$clave){


	//Encriptamos con MD5 la clave
	$clave = '{MD5}' . base64_encode(mhash(MHASH_MD5, $clave));

	$ldapconn = ldap_connect(LDAP_MASTER_HOST, LDAP_MASTER_PORT);

	ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);

	if ($ldapconn) {

		// binding anonimo
		$ldapbind = ldap_bind($ldapconn, LDAP_MASTER_RDN, LDAP_MASTER_PWD);

		// verify binding
		if ($ldapbind) {

		} else {
			error_log("ERROR:[ldap] Fallo el bind del ldap.");
			throw new MakoLdapException("Error al conectar con el servidor LDAP: ".LDAP_MASTER_HOST);
			return false;
		}

	}

	$bdn = "ou=people,".LDAP_MASTER_BDN;

	$passwd = $clave;

	$info["userPassword"] = $passwd;

	$r = ldap_modify($ldapconn, "uid=$usuario, ".$bdn, $info);
	if(!$r)
	{
		error_log("ERROR:[ldap:modificarClaveSocioLDAP] x ". ldap_err2str( ldap_errno($ldapconn) ). " uid=$usuario, ".$bdn);
	}
	ldap_close($ldapconn);
}

/**
 * Compara la clave del usuario con el hash md5 guardado en ldap
 *
 * @param $usuario
 * @param $clave
 * @return true en caso de coincidir, false, en caso contrario.
 */
function comparaClavesLdap($usuario,$clave)
{
	//Sacamos el hash md5
	$clave = '{MD5}' . base64_encode(mhash(MHASH_MD5, $clave));

	$ldapconn = ldap_connect(LDAP_MASTER_HOST, LDAP_MASTER_PORT);

	ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);

	if ($ldapconn) {
		// binding anonimo
		$ldapbind = ldap_bind($ldapconn, LDAP_MASTER_RDN, LDAP_MASTER_PWD);
		if ($ldapbind)
		{

		}
		else
		{
			error_log("ERROR:[ldap] Fallo el bind del ldap.");
			throw new MakoLdapException("Error al conectar con el servidor LDAP: ".LDAP_MASTER_HOST);
			return false;
		}
	}

	try{
		$res = ldap_search($ldapconn, LDAP_MASTER_BDN, "uid=$usuario");
		$information = ldap_get_entries($ldapconn, $res);
		$pass_ldap = $information[0]['userpassword'][0];
		if(!isset($information[0]['userpassword'][0]))
		{
			return 0;
		}
	}
	catch(Exception $e)
	{
		error_log("Error[LDAPHelper.comparaClavesLdap]". $e->getMessage());
	}
	ldap_close($ldapconn);
	if($pass_ldap == $clave) return true;
	else return false;

}

/**
 * Obtiene el valor del host en ldap
 * @param $usuario
 * @return mixed El valor del host, false, en caso de fallo.
 */
function hostEnLdap($usuario)
{

	$ldapconn = ldap_connect(LDAP_MASTER_HOST, LDAP_MASTER_PORT);

	ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);

	if ($ldapconn) {
		// binding anonimo
		$ldapbind = ldap_bind($ldapconn, LDAP_MASTER_RDN, LDAP_MASTER_PWD);
		if ($ldapbind)
		{

		}
		else
		{
			error_log("ERROR:[ldap] Fallo el bind del ldap.");
			throw new MakoLdapException("Error al conectar con el servidor LDAP: ".LDAP_MASTER_HOST);
			return false;
		}
	}

	try{
		$res = ldap_search($ldapconn, LDAP_MASTER_BDN, "uid=$usuario");
		$information = ldap_get_entries($ldapconn, $res);
		$host = $information[0]['host'][0];

	}
	catch(Exception $e)
	{
		error_log("Error[LDAPHelper.comparaClavesLdap]". $e->getMessage());
	}
	ldap_close($ldapconn);
	if($host != '') return $host;
	else return false;

}

/**
 * Modifica el host desde donde se pemite acceder al usuario
 * @param string $usuario
 * @param string $host
 */
function modificarHostPermitido($usuario,$host){
	error_log("Entrando a modificar host permitido: $usuario,$host");

	$ldapconn = ldap_connect(LDAP_MASTER_HOST, LDAP_MASTER_PORT);

	ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);

	if ($ldapconn) {

		// binding anonimo
		$ldapbind = ldap_bind($ldapconn, LDAP_MASTER_RDN, LDAP_MASTER_PWD);

		// verify binding
		if ($ldapbind) {

		} else {
			error_log("ERROR:[ldap] Fallo el bind del ldap.");
			throw new MakoLdapException("Error al conectar con el servidor LDAP: ".LDAP_MASTER_HOST);
			return false;
		}

	}

	$bdn = "ou=people,".LDAP_MASTER_BDN;
	$info["host"] = $host;

	$r = ldap_modify($ldapconn, "uid=$usuario, ".$bdn, $info);
	if(!$r)
	{
		error_log("ERROR:[ldap:modificaHostPermitido]  ". ldap_err2str( ldap_errno($ldapconn) ). " uid=$usuario, ".$bdn);
	}
	ldap_close($ldapconn);
}


function getComputadorasCentro($nombre_centro,$alias=null)
{
	$ldapconn = ldap_connect(LDAP_MASTER_HOST, LDAP_MASTER_PORT);

	ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);

	if ($ldapconn) {
		// binding anonimo
		$ldapbind = ldap_bind($ldapconn, LDAP_MASTER_RDN, LDAP_MASTER_PWD);
		if ($ldapbind)
		{

		}
		else
		{
			error_log("ERROR:[ldap] Fallo el bind del ldap a: ".LDAP_MASTER_HOST);
			throw new MakoLdapException("Error al conectar con el servidor LDAP: ".LDAP_MASTER_HOST);
			return false;
		}
	}

	$base = "cn=$nombre_centro,cn=$nombre_centro DHCP Service Config,ou=DHCP Servers";
	$res = ldap_search($ldapconn, $base.",".LDAP_MASTER_BDN, "objectClass=dhcpHost");
	$information = ldap_get_entries($ldapconn, $res);
	if($information['count']=='' && $alias != null)
	{
		$base = "cn=$alias DHCP Service Config,ou=DHCP Servers";
		$res = ldap_search($ldapconn, $base.",".LDAP_MASTER_BDN, "objectClass=dhcpHost");
		$information = ldap_get_entries($ldapconn, $res);
	}
	ldap_close($ldapconn);
	return $information;

}

/**
 * Regresa la info de usuario que tienen hosts marcados como ocupados, diferentes de *
 * @throws MakoLdapException
 */
function getHostsOcupados()
{
	$ldapconn = ldap_connect(LDAP_MASTER_HOST, LDAP_MASTER_PORT);

	ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);

	if ($ldapconn) {
		// binding anonimo
		$ldapbind = ldap_bind($ldapconn, LDAP_MASTER_RDN, LDAP_MASTER_PWD);
		if ($ldapbind)
		{

		}
		else
		{
			error_log("ERROR:[ldap] Fallo el bind del ldap a: ".LDAP_MASTER_HOST);
			throw new MakoLdapException("Error al conectar con el servidor LDAP: ".LDAP_MASTER_HOST);
			return false;
		}
	}

	$filtro = "host=pc*";
	$res = ldap_search($ldapconn, "ou=People,".LDAP_MASTER_BDN, $filtro);
	$information = ldap_get_entries($ldapconn, $res);
	ldap_close($ldapconn);
	return $information;

}

function getInfoAltaSocio($usuario)
{
	$ldapconn = ldap_connect(LDAP_MASTER_HOST, LDAP_MASTER_PORT);

	ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);

	if ($ldapconn) {
		// binding anonimo
		$ldapbind = ldap_bind($ldapconn, LDAP_MASTER_RDN, LDAP_MASTER_PWD);
		if ($ldapbind)
		{

		}
		else
		{
			error_log("ERROR:[ldap] Fallo el bind del ldap.");
			throw new MakoLdapException("Error al conectar con el servidor LDAP: ".LDAP_MASTER_HOST);
			return false;
		}
	}

	$res = ldap_search($ldapconn, LDAP_MASTER_BDN, "uid=$usuario");
	$information = ldap_get_entries($ldapconn, $res);

	ldap_close($ldapconn);
	$infores = $information[0]['description'][0];
	if(isset($infores))
	{
		list($x,$centro_id,$socio_id) = explode("|",$infores);
		return array($centro_id,$socio_id);
	}
	else
	{
		return false;
	}

}



function modificarLDAPUid($usuario,$uidNumber){

	$ldapconn = ldap_connect(LDAP_MASTER_HOST, LDAP_MASTER_PORT);

	ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);

	if ($ldapconn) {

		// binding anonimo
		$ldapbind = ldap_bind($ldapconn, LDAP_MASTER_RDN, LDAP_MASTER_PWD);

		// verify binding
		if ($ldapbind) {

		} else {
			error_log("ERROR:[ldap] Fallo el bind del ldap.");
			throw new MakoLdapException("Error al conectar con el servidor LDAP: ".LDAP_MASTER_HOST);
			return false;
		}

	}

	$bdn = "ou=people,".LDAP_MASTER_BDN;
	$info["uidNumber"]= $uidNumber;
	$info["objectclass"]=array(
			"top",
			"person",
			"organizationalPerson",
			"inetOrgPerson",
			"hostObject",
			"posixAccount","shadowAccount" );

	$r = ldap_modify($ldapconn, "uid=$usuario, ".$bdn, $info);
	if(!$r)
	{
		error_log("ERROR:[ldap:modificarLDAPUid] x ". ldap_err2str( ldap_errno($ldapconn) ). "uid=$usuario, ".$bdn);
		ldap_close($ldapconn);
		return false;
	}
	else
	{
		ldap_close($ldapconn);
		return true;
	}
}

?>
