<?php

/**
 * Regresa la cadena formateada de productos para impresión de ticket
 * @param Orden $orden
 * @return string
 */


/**
 *
 * Genera el reporte del ranking por centro (local) ...
 * @param int $semana
 * @param int $anio
 * @param int $centroId
 */
function reporteRankingLocal($semana, $anio, $centroId, $fI, $fF)
{
	if($centroId == '')
		$centroId = sfConfig::get('app_centro_actual_id');

	$fechaInicio = $fI{6}.$fI{7}.$fI{8}.$fI{9}.'-'.$fI{3}.$fI{4}.'-'.$fI{0}.$fI{1};
	$fechaFin = $fF{6}.$fF{7}.$fF{8}.$fF{9}.'-'.$fF{3}.$fF{4}.'-'.$fF{0}.$fF{1};

	$conn = Propel::getConnection();

	$sql = "SELECT *
	FROM metas
	WHERE semana <=$semana
	AND anio <= $anio
	AND centro_id = $centroId";


	//Comprobamos si la primera semana del año comienza en un dia de la semana del año anterior.
	//Como el caso de la primera semana del 2013 que comenzó en 2012
	if($semana == 1) {
		if(strtotime("first monday january $anio") < strtotime($fechaInicio)) {

			$sql = "select  * from metas where semana <=52 and anio <=$anio and centro_id = $centroId UNION (select  * from metas where semana <=1 and anio <=$anio + 1 and centro_id = $centroId) order by id";
		}
	}
	else {
		$sql = "select  * from metas where semana <=52 and anio <=$anio-1 and centro_id = $centroId UNION (select  * from metas where semana <=$semana and anio <=$anio and centro_id = $centroId) order by id";
	}

	$metas = $conn->query($sql);

	//error_log($sql);
	foreach ($metas as $meta){

		//fija
		if(empty($meta['categoria_fija_id']))$meta['categoria_fija_id']=0;
		else $categoria = 'fija';
		//producto
		if(empty($meta['categoria_producto_id']))$meta['categoria_producto_id']=0;
		else $categoria = 'producto';
		//beca
		if(empty($meta['categoria_beca_id']))$meta['categoria_beca_id']=0;
		else$categoria = 'beca';

		$sql = "SELECT fecha,
		categoria_fija_id,
		categoria_producto_id,
		categoria_beca_id,
		entero,
		SUM(cantidad) AS cantidad,
		SUM(meta_diaria) as meta
		FROM reporte_metas
		WHERE fecha between '$fechaInicio' AND '$fechaFin'
		AND (categoria_fija_id = ".$meta['categoria_fija_id']
		." OR categoria_producto_id = ".$meta['categoria_producto_id']
		." OR categoria_beca_id = ".$meta['categoria_beca_id']
		.")
		AND centro_id = ".$centroId."
		GROUP BY fecha,
		categoria_fija_id,
		categoria_producto_id,
		categoria_beca_id,
		entero
		ORDER BY fecha";

		$datos = $conn->query($sql);

		foreach ($datos as $d){

			$fecha = $d['fecha'];
			$fechas[]=$d['fecha'];
			$categoriaId = $d['categoria_fija_id'].$d['categoria_producto_id'].$d['categoria_beca_id'];
			$entero = $d['entero'];

			//trae los datos por cada centro para cada categoria
			$reporte[$fecha][$categoria][$categoriaId][$entero]=$d['cantidad'];

			//trae los datos de metas para cada centro para cada categoria
			//$metaReporte[$centroId][$categoria][$categoriaId][$entero]= $meta['meta'];
			$metaReporte[$fecha][$categoria][$categoriaId][$entero]= round($d['meta'],2);
			//$metaReporte[$fecha][$categoria][$categoriaId][$entero]= round($d['meta'],2);
			$metasCategorias[$categoria][$categoriaId][$entero] = $categoriaId;

			//totales
			if(!isset($detecta_fecha[$fecha][$categoria][$categoriaId][$entero])) {
				$detecta_fecha[$fecha][$categoria][$categoriaId][$entero]=true;
				$cantidadesTotales[$categoria][$categoriaId][$entero]=$d['cantidad']+$cantidadesTotales[$categoria][$categoriaId][$entero];
			}
			//$cantidadesTotales[$categoria][$categoriaId][$entero] += $d['cantidad'];
			//$metasTotales[$categoria][$categoriaId][$entero]+= $meta['meta'];
			if(!isset($detecta_meta_fecha[$fecha][$categoria][$categoriaId][$entero])) {
				$detecta_meta_fecha[$fecha][$categoria][$categoriaId][$entero]=true;
				$metasTotales[$categoria][$categoriaId][$entero]+= round($d['meta'],2);
			}

		}
			
	}
	$fechas = array_unique($fechas);
	$fechasTotal = count($fechas);


	$r=array(
			'fechas'=>$fechas,
			'fechasTotal'=>$fechasTotal,
			'reporte'=>$reporte,
			'metaReporte'=>$metaReporte,
	 	'metasCategorias'=>$metasCategorias,
			'cantidadesTotales'=>$cantidadesTotales,
			'metasTotales' => $metasTotales
	);

	return $r;
}

/**
 * Toma los datos y genera la plantilla html dinamicamente

 * @param $par Es un arreglo de paremtros que regresa la funcion ReporteRankingLocal
 */
function tablaReporteRankinLocal($par){


	$fechas = $par['fechas'];
	$fechasTotal = $par['fechasTotal'];
	$reporte= $par['reporte'];
	$metaReporte= $par['metaReporte'];
	$metasCategorias= $par['metasCategorias'];
	$cantidadesTotales= $par['cantidadesTotales'];
	$metasTotales= $par['metasTotales'];

	$tbody='';

	$tbody.='<thead>';
	$tbody.='<tr>';
	$tbody.='<th colspan="2" class="ui-state-default">'.'Fecha'.'</th>';

	//cabeceras
	foreach ($metasCategorias as $categoria => $valorMC){
		foreach ($valorMC as $llaveD => $valorD){
			foreach ($valorD as $entero => $valor){
					
				if( $categoria=='fija') $fija = CategoriaMetasFijasPeer::retrieveByPK($valor);
				elseif( $categoria=='producto')$producto = CategoriaReportePeer::retrieveByPK($valor);
				elseif( $categoria=='beca')$beca = BecaPeer::retrieveByPK($valor);

				$titulo =(!empty($producto)) ? $producto->getNombre():'';
				$titulo .=(!empty($beca)) ? $beca->getNombre():'';
				$titulo .= (!empty($fija)) ? $fija->getNombre():'';

				($entero==0)?$moneda = '$':$moneda='';

				$tbody.='<th colspan="4" class="ui-state-default"><div align="center">'.$titulo.' '.$moneda.'</div></th>';
					
				$producto=null;
				$beca=null;
				$fija=null;
			}
		}
	}
	$tbody.="</tr>";

	$tbody.='<tr>';
	$tbody.='<th></th>';
	$tbody.='<th></th>';
	foreach ($metasCategorias as $categoria => $valorMC){
		foreach ($valorMC as $llaveD => $valorD){
			foreach ($valorD as $entero => $valor){
				//si no es entero, hay que meter el signo de peso en el encabezado
				($entero==0)?$moneda = '$':$moneda='';

				$tbody.='<th>'.$moneda.' Cant.</th>';
				$tbody.='<th>'.$moneda.' Meta</th>';
				$tbody.='<th>'.$moneda.' Dif</th>';
				$tbody.='<th>Alcance %</th>';
			}
		}
	}
	$tbody.='</tr>';
	$tbody.='</thead>';
	$tbody.='<tbody>';

	setlocale('LC_TIME', 'es_MX.UTF8');

	//cuerpo
	foreach($fechas as $fechaK => $fecha){

		$tbody.='<tr>';

		$tbody.='<td>'.ucfirst(strftime("%A",strtotime($fecha))).'</td>';
		$tbody.='<td>'.$fecha.'</td>';

		//hay q checar que centros traen datos y cuales no y rellenar estos ultimos con 0
		foreach ($metasCategorias as $categoria => $valorMC){
			foreach ($valorMC as $llaveD => $valorD){
				foreach ($valorD as $entero => $categoriaId){
					if(!isset($reporte[$fecha][$categoria][$categoriaId][$entero]))
						$reporteCentral[$fecha][$categoria][$categoriaId][$entero] ='0';
					else
						$reporteCentral[$fecha][$categoria][$categoriaId][$entero] = $reporte[$fecha][$categoria][$categoriaId][$entero];
				}

			}
		}

		foreach ($reporteCentral[$fecha] as $categoria => $valorR){
			foreach ($valorR as $categoriaId => $valorD){
				foreach ($valorD as $entero => $cuenta){

					$meta =round($metaReporte[$fecha][$categoria][$categoriaId][$entero],2);
					//$cuenta = $valor;
					$diferencia = $cuenta-$meta;

					//sio la meta es de 0 pero la cuenta es igual o mayor que 1, el alcance es del 100%
					if($meta==0 & $cuenta != 0)
						$alcance = number_format(100,2);
					else
						$alcance = number_format(($cuenta/$meta)*100,2);

					//si no es entero entonces es moneda
					if($entero==0) {
						$meta = number_format($meta,2);
						$cuenta = number_format($cuenta,2);
						$diferencia =  number_format($diferencia,2);
					}else
						$cuenta = number_format($cuenta,0);

					$dif = ($diferencia>=0) ? $diferencia:'<font color="Red">'.$diferencia.'</font>';

					$tbody.='<td align="right">'.$cuenta.'</td>';
					$tbody.='<td align="right">'.$meta.'</td>';
					$tbody.='<td align="right">'.$dif.'</td>';
					$tbody.='<td align="right">'.$alcance.'</td>';
				}
			}
		}
		$tbody.='</tr>';
	}
	$tbody.='</tbody>';


	//totales
	$tbody.='<tfoot>';
	$tbody.='<tr>';
	$tbody.='<td></td>';
	$tbody.='<td><b>Total</b></td>';

	foreach ($cantidadesTotales as $categoria => $cuentasTotales){
		foreach($cuentasTotales as $categoriaId => $valor){
			foreach($valor as $entero => $cuentaTotal){
				$metaTotal = $metasTotales[$categoria][$categoriaId][$entero];
				$diferenciaTotal = round($cuentaTotal-$metaTotal,2);
					
				$alcanceTotal =number_format((($cuentaTotal/$metaTotal)*100),2);
					
				//si no es entero entonces es moneda
				if($entero==0) {
					$metaTotal = number_format($metaTotal,2);
					$cuentaTotal = number_format($cuentaTotal,2);
					$diferenciaTotal =  number_format($diferenciaTotal,2);
				}
				else
					$cuentaTotal = number_format($cuentaTotal,0);
					
				$difTotal = ($diferenciaTotal>=0) ? $diferenciaTotal:'<font color="Red">'.$diferenciaTotal.'</font>';
					
				$tbody.='<td align="right"><b>'.$cuentaTotal.'</b></td>';
				$tbody.='<td align="right"><b>'.$metaTotal.'</b></td>';
				$tbody.='<td align="right"><b>'.$difTotal.'</b></td>';
				$tbody.='<td align="right"><b>'.$alcanceTotal.'%</b></td>';
			}
		}
	}

	$tbody.='</tr>';
	$tbody.='</tfoot>';

	return $tbody;

}

/**
 * Regresa los datos del ranking para el servidor central
 *
 * @param int $semana Numero de semana
 * @param int $anio Numero de año
 * @param string $fI Fecha inicial
 * @param string $fF Fecha final
 *
 * @return array Arreglo de valores
 */
function reporteRankingCentral($semana, $anio, $fI, $fF)
{

	$fechaInicio = $fI{6}.$fI{7}.$fI{8}.$fI{9}.'-'.$fI{3}.$fI{4}.'-'.$fI{0}.$fI{1};
	$fechaFin = $fF{6}.$fF{7}.$fF{8}.$fF{9}.'-'.$fF{3}.$fF{4}.'-'.$fF{0}.$fF{1};

	$conn = Propel::getConnection();

	$sql = "SELECT *
	FROM metas
	WHERE
	semana <= $semana
	AND anio <= $anio
	";

	//Comprobamos si la primera semana del año comienza en un dia de la semana del año anterior.
	//Como el caso de la primera semana del 2013 que comenzó en 2012
	if($semana == 1) {
		if(strtotime("first monday january $anio") < strtotime($fechaInicio)) {
			$sql = "select  * from metas where semana <=52 and anio <=$anio UNION (select  * from metas where semana <=1 and anio <=$anio + 1) order by id";
		}
	}
	else
	{
		$sql = "select  * from metas where semana <=52 and anio <=$anio-1 UNION (select  * from metas where semana <=$semana and anio <=$anio) order by id";
	}

	//echo $sql." \n<br>\n<br>";
	//$sql = "SELECT * FROM metas WHERE fecha_inicio <='$fechaFin' AND activo = 't' ORDER BY fecha_inicio DESC";

	$metas = $conn->query($sql);

	//error_log($sql);
	foreach ($metas as $meta){

		//fija
		if(empty($meta['categoria_fija_id']))$meta['categoria_fija_id']=0;
		else $categoria = 'fija';
		//producto
		if(empty($meta['categoria_producto_id']))$meta['categoria_producto_id']=0;
		else $categoria = 'producto';
		//beca
		if(empty($meta['categoria_beca_id']))$meta['categoria_beca_id']=0;
		else$categoria = 'beca';

		$sql = "SELECT centro_id,
		categoria_fija_id,
		categoria_producto_id,
		categoria_beca_id,
		entero,
		SUM(cantidad) AS cantidad,
		SUM(meta_diaria) as meta
		FROM reporte_metas
		WHERE
		fecha between '$fechaInicio' AND '$fechaFin'
		AND (categoria_fija_id = ".$meta['categoria_fija_id']
		." OR categoria_producto_id = ".$meta['categoria_producto_id']
		." OR categoria_beca_id = ".$meta['categoria_beca_id']
		.")
		AND centro_id = ".$meta['centro_id']."
		GROUP BY centro_id,
		categoria_fija_id,
		categoria_producto_id,
		categoria_beca_id,
		entero";

		$datos = $conn->query($sql);
			
		foreach ($datos as $d){

			$centroId = $d['centro_id'];
			$categoriaId = $d['categoria_fija_id'].$d['categoria_producto_id'].$d['categoria_beca_id'];
			$entero = $d['entero'];

			//trae los datos por cada centro para cada categoria
			$reporte[$centroId][$categoria][$categoriaId][$entero]=$d['cantidad'];

			//trae los datos de metas para cada centro para cada categoria
			//$metaReporte[$centroId][$categoria][$categoriaId][$entero]= $meta['meta'];
			$metaReporte[$centroId][$categoria][$categoriaId][$entero]= round($d['meta'], 2);
			$metasCategorias[$categoria][$categoriaId][$entero] = $categoriaId;

			$centro = CentroPeer::retrieveByPK($centroId);
			//trae los nombres de los centros
			$centrosTxt[$centroId]=$centro->getAliasReporte();
			//trae los centros totales
			$centros[$centroId]=$centroId;

			//totales
			if(!isset($detecta_cantidad[$centroId][$categoria][$categoriaId][$entero])) {
				$cantidadesTotales[$categoria][$categoriaId][$entero]=$d['cantidad']+$cantidadesTotales[$categoria][$categoriaId][$entero];
				$detecta_cantidad[$centroId][$categoria][$categoriaId][$entero] = true;
			}
				
			if(!isset($detecta_metas[$centroId][$categoria][$categoriaId][$entero])) {
				//$metasTotales[$categoria][$categoriaId][$entero]+= $meta['meta'];
				$metasTotales[$categoria][$categoriaId][$entero]+= round($d['meta'], 2);
				$detecta_metas[$centroId][$categoria][$categoriaId][$entero] = true;
			}

		}
			
	}

	$centrosTotal = count($centro);

	$r = array(
			'reporte'=>$reporte,
			'metaReporte'=>$metaReporte,
			'metasCategorias'=>$metasCategorias,
			'centrosTxt'=>$centrosTxt,
			'centros'=>$centros,
			'cantidadesTotales'=>$cantidadesTotales,
			'metasTotales'=>$metasTotales,
			'centrosTotal'=>$centrosTotal,
			'semana' => $semana,
			'anio' => $anio,
			'fI' => $fI,
			'fF' => $fF
	);

	return $r;
}

function tablaReporteRankingCentral($par)
{

	$reporte = $par['reporte'];
	$metaReporte = $par['metaReporte'];
	$metasCategorias = $par['metasCategorias'];
	$centrosTxt = $par['centrosTxt'];
	$centros = $par['centros'];
	$cantidadesTotales = $par['cantidadesTotales'];
	$metasTotales = $par['metasTotales'];
	$centrosTotal = $par['centrosTotal'];
	$semana = $par['semana'];
	$anio = $par['anio'];
	$fI = $par['fI'];
	$fF = $par['fF'];

	$tbody='';

	$tbody.='<thead>';
	$tbody.='<tr>';
	$tbody.='<th colspan="2" class="ui-state-default">'.'Centro'.'</th>';

	//cabeceras
	foreach ($metasCategorias as $categoria => $valorMC){
		foreach ($valorMC as $llaveD => $valorD){
			foreach ($valorD as $entero => $valor){
					
				if( $categoria=='fija') $fija = CategoriaMetasFijasPeer::retrieveByPK($valor);
				elseif( $categoria=='producto')$producto = CategoriaReportePeer::retrieveByPK($valor);
				elseif( $categoria=='beca')$beca = BecaPeer::retrieveByPK($valor);

				$titulo =(!empty($producto)) ? $producto->getNombre():'';
				$titulo .=(!empty($beca)) ? $beca->getNombre():'';
				$titulo .= (!empty($fija)) ? $fija->getNombre():'';

				($entero==0)?$moneda = '$':$moneda='';

				$tbody.='<th colspan="4" class="ui-state-default"><div align="center">'.$titulo.' '.$moneda.'</div></th>';
					
				$producto=null;
				$beca=null;
				$fija=null;
			}
		}
	}
	$tbody.="</tr>";

	$tbody.='<tr>';
	$tbody.='<th></th>';
	$tbody.='<th></th>';
	foreach ($metasCategorias as $categoria => $valorMC){
		foreach ($valorMC as $llaveD => $valorD){
			foreach ($valorD as $entero => $valor){
				//si no es entero, hay que meter el signo de peso en el encabezado
				($entero==0)?$moneda = '$':$moneda='';

				$tbody.='<th>'.$moneda.' Cant.</th>';
				$tbody.='<th>'.$moneda.' Meta</th>';
				$tbody.='<th>'.$moneda.' Dif</th>';
				$tbody.='<th>Alcance %</th>';
			}
		}
	}
	$tbody.='</tr>';
	$tbody.='</thead>';
	$tbody.='<tbody>';

	//cuerpo

	foreach($centros as $centroId => $centroIdVal){
		$tbody.='<tr>';
		$tbody.='<td><a href="javascript:void(0);" onClick="verDetalle(\''.$semana.'\',\''.$anio.'\',\''.$centroId.'\',\''.$fI.'\',\''.$fF.'\')"><span class="ui-icon  ui-icon-circle-zoomin"></span></a></td>';
		$tbody.='<td>'.$centrosTxt[$centroId].'</td>';

		//hay q checar que centros traen datos y cuales no y rellenar estos ultimos con 0
		foreach ($metasCategorias as $categoria => $valorMC){
			foreach ($valorMC as $llaveD => $valorD){
				foreach ($valorD as $entero => $categoriaId){
					if(!isset($reporte[$centroId][$categoria][$categoriaId][$entero]))
						$reporteCentral[$centroId][$categoria][$categoriaId][$entero] ='0';
					else
						$reporteCentral[$centroId][$categoria][$categoriaId][$entero] = $reporte[$centroId][$categoria][$categoriaId][$entero];
				}

			}
		}

		foreach ($reporteCentral[$centroId] as $categoria => $valorR){
			foreach ($valorR as $categoriaId => $valorD){
				foreach ($valorD as $entero => $cuenta){

					$meta =round($metaReporte[$centroId][$categoria][$categoriaId][$entero],2);
					//$cuenta = $valor;
					$diferencia = $cuenta-$meta;

					//sio la meta es de 0 pero la cuenta es igual o mayor que 1, el alcance es del 100%
					if($meta==0 & $cuenta != 0)
						$alcance = number_format(100,2);
					else
						$alcance = number_format(($cuenta/$meta)*100,2);

					//si no es entero entonces es moneda
					if($entero==0) {
						$meta = number_format($meta,2);
						$cuenta = number_format($cuenta,2);
						$diferencia =  number_format($diferencia,2);
					}else
						$cuenta = number_format($cuenta,0);

					$dif = ($diferencia>=0) ? $diferencia:'<font color="Red">'.$diferencia.'</font>';

					$tbody.='<td align="right">'.$cuenta.'</td>';
					$tbody.='<td align="right">'.$meta.'</td>';
					$tbody.='<td align="right">'.$dif.'</td>';
					$tbody.='<td align="right">'.$alcance.'</td>';
				}
			}
		}
		$tbody.='</tr>';
	}
	$tbody.='</tbody>';


	//totales
	$tbody.='<tfoot>';
	$tbody.='<tr>';
	$tbody.='<td></td>';
	$tbody.='<td><b>Total</b></td>';

	foreach ($cantidadesTotales as $categoria => $cuentasTotales){
		foreach($cuentasTotales as $categoriaId => $valor){
			foreach($valor as $entero => $cuentaTotal){
				$metaTotal = $metasTotales[$categoria][$categoriaId][$entero];
				$diferenciaTotal = round($cuentaTotal-$metaTotal,2);
					
				$alcanceTotal =number_format((($cuentaTotal/$metaTotal)*100),2);
					
				//si no es entero entonces es moneda
				if($entero==0) {
					$metaTotal = number_format($metaTotal,2);
					$cuentaTotal = number_format($cuentaTotal,2);
					$diferenciaTotal =  number_format($diferenciaTotal,2);
				}
				else
					$cuentaTotal = number_format($cuentaTotal,0);
					
				$difTotal = ($diferenciaTotal>=0) ? $diferenciaTotal:'<font color="Red">'.$diferenciaTotal.'</font>';
					
				$tbody.='<td align="right"><b>'.$cuentaTotal.'</b></td>';
				$tbody.='<td align="right"><b>'.$metaTotal.'</b></td>';
				$tbody.='<td align="right"><b>'.$difTotal.'</b></td>';
				$tbody.='<td align="right"><b>'.$alcanceTotal.'%</b></td>';
			}
		}
	}

	$tbody.='</tr>';
	$tbody.='</tfoot>';

	return $tbody;

}

function diaDeLaSemana($wk,$yr){

	// comienza en lunes (1)
	$inicia = '1';
	//termina en dgo (7)
	$termina = '7';

	//Find out the date of weeknumber where day is Monday
	$dia[0] = date('Y-m-d',strtotime($yr.'W'.str_pad($wk,2,'0').$inicia));
	$dia[1] = date('Y-m-d',strtotime($yr.'W'.str_pad($wk,2,'0').$termina));
	//Display result result = '2010-05-03'
	return $dia;


}

function createDateRangeArray($start, $end) {
	// Modified by JJ Geewax

	$range = array();

	if (is_string($start) === true) $start = strtotime($start);
	if (is_string($end) === true ) $end = strtotime($end);

	if ($start > $end) return createDateRangeArray($end, $start);

	do {
		$range[] = date('Y-m-d', $start);
		$start = strtotime("+ 1 day", $start);
	}
	while($start < $end);

	return $range;
}

function addDate($date,$day)//add days
{
	$sum = strtotime(date("Y-m-d", strtotime("$date")) . " +$day days");
	$dateTo=date('Y-m-d',$sum);
	return $dateTo;
}


/**
 * Reporte de las ventas por categoria de producto en una fecha dada.
 * @param string $fecha
 */
function ventasCategoriaProducto($fecha)
{
	$conn = Propel::getConnection();
	$centro_id =  sfConfig::get('app_centro_actual_id');
	$centro = CentroPeer::retrieveByPK($centro_id);
	$pcs = $centro->getPcsUsuario();
	$c = new Criteria(); $c->add(SeccionPeer::ACTIVO,true); $c->add(SeccionPeer::EN_DASHBOARD,true);
	$aulas = 0;
	foreach($centro->getSeccions($c) as $aula)
	{
		if(preg_match("/aula (\d){1}/i",$aula->getNombre()))
			$aulas++;
	}

	//TODO: Preguntar como se calcula esto.
	$cursos = cursosEnCentro($fecha,$centro_id);


	$sql = "SELECT
	o.centro_id,
	date_trunc('day',r.created_at::date )::date AS fecha,
	sum(subtotal) AS monto,
	sum(cantidad) AS cantidad,
	sum(o.tiempo) AS tiempo,
	p.categoria_id,
	c.nombre AS categoria
	FROM detalle_orden AS o
	JOIN orden AS r ON (r.id = o.orden_id)
	LEFT JOIN producto AS p ON (p.id = o.producto_id)
	LEFT JOIN categoria_producto as c on (c.id = p.categoria_id)
	WHERE
	r.created_at::date = '$fecha'
	AND o.centro_id = $centro_id
	GROUP BY o.centro_id,fecha, p.categoria_id, c.nombre";

	$sth = $conn->prepare($sql);
	$sth->execute();
	$datos = array();
	$datos = $sth->fetchAll(PDO::FETCH_ASSOC);
	foreach($datos as $d)
	{
		$d['cursos'] = $cursos;
		$d['aulas'] = $aulas;
		$d['pcs'] = $pcs;
		if($d['categoria_id']==2)
		{
			//Son cursos diferentes por categoria
			//TODO: hacer la funcion para todas las demas categorias
			//TODO: Contemplar los cursos unicos, es decir, contemplar el curso unico relacionado con un producto de cobranza.
			$d['cantidad'] = cursosDiferentesComprados($fecha, $centro_id);
		}
		insertaReportesVentasCategoria($d);
	}
}

function insertaReportesVentasCategoria($datos)
{

	if(!isset($datos['fecha'])) return false;

	$semana = date('W', strtotime($datos['fecha']));
	$anio = date('Y', strtotime($datos['fecha']));

	$centro_id = $datos['centro_id'];

	$rp = new ReporteVentasCategorias();
	$rp->setId(Comun::generaId());
	$rp->setCentroId($centro_id);
	$rp->setFecha($datos['fecha']);
	$rp->setAnio($anio);
	$rp->setSemana($semana);
	$rp->setMonto($datos['monto']);
	$rp->setCantidad($datos['cantidad']);
	$rp->setTiempo($datos['tiempo']);
	$rp->setCategoriaId($datos['categoria_id']);
	$rp->setCategoria($datos['categoria']);

	$rp->setPcs($datos['pcs']);
	$rp->setCursos($datos['cursos']);
	$rp->setAulas($datos['aulas']);


	$rp->save();

}

/**
 * Cursos en el centro a la fecha
 */
function cursosEnCentro($fecha,$centro_id)
{
	$conn = Propel::getConnection();

	$cursos = 0;
	$sql = "select
	count(distinct(curso_id)) as cursos
	from horario as h
	left join curso as c on (c.id = h.curso_id)
	where h.created_at::date <= '$fecha' and h.centro_id = $centro_id
	and c.activo = 't'";

	$sth = $conn->prepare($sql);
	$sth->execute();
	$datos = $sth->fetchAll(PDO::FETCH_ASSOC);
	foreach($datos as $d)
	{
		$cursos = $d['cursos'];
	}
	return $cursos;
}


/**
 * Ingressa un registro si hubo operación para ese dia en ese centro.
 */
function diaCentroOperacion($fecha)
{
	$sql = "(SELECT fecha_login::date AS fecha FROM sesion_operador WHERE fecha_login::date = '$fecha' LIMIT 1)
	UNION
	(SELECT fecha_login::date AS fecha FROM sesion_historico WHERE fecha_login::date = '$fecha' LIMIT 1)
	UNION
	(SELECT fecha_login::date AS fecha FROM sesion_operador_historico WHERE fecha_login::date = '$fecha' LIMIT 1)
	UNION
	(SELECT created_at::date AS fecha FROM orden WHERE created_at::date='$fecha' LIMIT 1)
	";

	$conn = Propel::getConnection();
	$sth = $conn->prepare($sql);
	$sth->execute();
	$datos = $sth->fetchAll(PDO::FETCH_ASSOC);
	$centro_id =  sfConfig::get('app_centro_actual_id');
	$dop = new DiasOperacion();
	$dop->setId(Comun::generaId());
	$dop->setFecha($fecha);
	$dop->setCentroId($centro_id);

	if(count($datos) > 0)
	{
		$dop->setOpera(true);
	}
	else
	{
		$dop->setOpera(false);
	}
	$dop->save();
}

function cursosDiferentesComprados($fecha,$centro_id)
{
	$conn = Propel::getConnection();

	$cursos = 0;
	$sql = "select
	count(distinct(producto_id)) as cursos
	from detalle_orden as d
	left join producto as p on (p.id = d.producto_id)
	join orden as o on (o.id = d.orden_id)
	where o.created_at::date = '$fecha'
	and p.categoria_id = 2
	and d.centro_id = $centro_id
	";

	$sth = $conn->prepare($sql);
	$sth->execute();
	$datos = $sth->fetchAll(PDO::FETCH_ASSOC);
	foreach($datos as $d)
	{
		$cursos = $d['cursos'];
	}
	return $cursos;

}
?>