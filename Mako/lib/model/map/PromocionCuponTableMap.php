<?php


/**
 * This class defines the structure of the 'promocion_cupon' table.
 *
 *
 * This class was autogenerated by Propel 1.4.0 on:
 *
 * Thu May 31 01:06:39 2012
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    lib.model.map
 */
class PromocionCuponTableMap extends TableMap {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'lib.model.map.PromocionCuponTableMap';

	/**
	 * Initialize the table attributes, columns and validators
	 * Relations are not initialized by this method since they are lazy loaded
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function initialize()
	{
		// attributes
		$this->setName('promocion_cupon');
		$this->setPhpName('PromocionCupon');
		$this->setClassname('PromocionCupon');
		$this->setPackage('lib.model');
		$this->setUseIdGenerator(false);
		// columns
		$this->addPrimaryKey('ID', 'Id', 'BIGINT', true, null, null);
		$this->addForeignKey('CENTRO_ID', 'CentroId', 'INTEGER', 'centro', 'ID', false, null, null);
		$this->addColumn('NOMBRE', 'Nombre', 'VARCHAR', true, 256, null);
		$this->addColumn('DESCRIPCION', 'Descripcion', 'LONGVARCHAR', true, null, null);
		$this->addColumn('VIGENTE_DE', 'VigenteDe', 'TIMESTAMP', false, null, null);
		$this->addColumn('VIGENTE_A', 'VigenteA', 'TIMESTAMP', false, null, null);
		$this->addColumn('DESCUENTO', 'Descuento', 'DECIMAL', false, 10, 0);
		$this->addColumn('CANTIDAD_CUPONES', 'CantidadCupones', 'INTEGER', false, null, 0);
		$this->addForeignKey('CREADO_POR', 'CreadoPor', 'INTEGER', 'usuario', 'ID', false, null, null);
		$this->addColumn('ACTIVO', 'Activo', 'BOOLEAN', false, null, true);
		$this->addColumn('CREATED_AT', 'CreatedAt', 'TIMESTAMP', false, null, null);
		$this->addColumn('UPDATED_AT', 'UpdatedAt', 'TIMESTAMP', false, null, null);
		// validators
	} // initialize()

	/**
	 * Build the RelationMap objects for this table relationships
	 */
	public function buildRelations()
	{
		$this->addRelation('Centro', 'Centro', RelationMap::MANY_TO_ONE, array('centro_id' => 'id', ), null, null);
		$this->addRelation('Usuario', 'Usuario', RelationMap::MANY_TO_ONE, array('creado_por' => 'id', ), null, null);
		$this->addRelation('Cupon', 'Cupon', RelationMap::ONE_TO_MANY, array('id' => 'promocion_cupon_id', ), null, null);
		$this->addRelation('PromocionCuponProducto', 'PromocionCuponProducto', RelationMap::ONE_TO_MANY, array('id' => 'promocion_id', ), null, null);
	} // buildRelations()

	/**
	 *
	 * Gets the list of behaviors registered for this table
	 *
	 * @return array Associative array (name => parameters) of behaviors
	 */
	public function getBehaviors()
	{
		return array(
				'symfony' => array('form' => 'true', 'filter' => 'true', ),
				'symfony_behaviors' => array(),
				'symfony_timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', ),
		);
	} // getBehaviors()

} // PromocionCuponTableMap
