<?php


/**
 * This class defines the structure of the 'cursos_finalizados' table.
 *
 *
 * This class was autogenerated by Propel 1.4.0 on:
 *
 * Thu May 31 01:06:54 2012
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    lib.model.map
 */
class CursosFinalizadosTableMap extends TableMap {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'lib.model.map.CursosFinalizadosTableMap';

	/**
	 * Initialize the table attributes, columns and validators
	 * Relations are not initialized by this method since they are lazy loaded
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function initialize()
	{
		// attributes
		$this->setName('cursos_finalizados');
		$this->setPhpName('CursosFinalizados');
		$this->setClassname('CursosFinalizados');
		$this->setPackage('lib.model');
		$this->setUseIdGenerator(false);
		// columns
		$this->addPrimaryKey('ID', 'Id', 'BIGINT', true, null, null);
		$this->addForeignKey('CURSO_ID', 'CursoId', 'BIGINT', 'curso', 'ID', true, null, null);
		$this->addForeignKey('ALUMNO_ID', 'AlumnoId', 'BIGINT', 'alumnos', 'MATRICULA', true, null, null);
		$this->addForeignKey('GRUPO_ID', 'GrupoId', 'BIGINT', 'grupo', 'ID', true, null, null);
		$this->addForeignKey('CALIFICACION_ID', 'CalificacionId', 'BIGINT', 'calificacion_final', 'ID', false, null, null);
		$this->addColumn('PROMEDIO_FINAL', 'PromedioFinal', 'NUMERIC', false, 10, null);
		$this->addColumn('RECURSADO', 'Recursado', 'BOOLEAN', true, null, false);
		$this->addColumn('CREATED_AT', 'CreatedAt', 'TIMESTAMP', false, null, null);
		$this->addColumn('UPDATED_AT', 'UpdatedAt', 'TIMESTAMP', false, null, null);
		$this->addForeignKey('ESTATUS_CURSO_FINALIZADO_ID', 'EstatusCursoFinalizadoId', 'INTEGER', 'estatus_curso_finalizado', 'ID', true, null, null);
		// validators
	} // initialize()

	/**
	 * Build the RelationMap objects for this table relationships
	 */
	public function buildRelations()
	{
		$this->addRelation('Curso', 'Curso', RelationMap::MANY_TO_ONE, array('curso_id' => 'id', ), 'RESTRICT', null);
		$this->addRelation('Alumnos', 'Alumnos', RelationMap::MANY_TO_ONE, array('alumno_id' => 'matricula', ), null, null);
		$this->addRelation('Grupo', 'Grupo', RelationMap::MANY_TO_ONE, array('grupo_id' => 'id', ), null, null);
		$this->addRelation('CalificacionFinal', 'CalificacionFinal', RelationMap::MANY_TO_ONE, array('calificacion_id' => 'id', ), 'RESTRICT', null);
		$this->addRelation('EstatusCursoFinalizado', 'EstatusCursoFinalizado', RelationMap::MANY_TO_ONE, array('estatus_curso_finalizado_id' => 'id', ), 'RESTRICT', null);
	} // buildRelations()

	/**
	 *
	 * Gets the list of behaviors registered for this table
	 *
	 * @return array Associative array (name => parameters) of behaviors
	 */
	public function getBehaviors()
	{
		return array(
				'symfony' => array('form' => 'true', 'filter' => 'true', ),
				'symfony_behaviors' => array(),
				'symfony_timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', ),
		);
	} // getBehaviors()

} // CursosFinalizadosTableMap
