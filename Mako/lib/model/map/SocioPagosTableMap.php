<?php


/**
 * This class defines the structure of the 'socio_pagos' table.
 *
 *
 * This class was autogenerated by Propel 1.4.0 on:
 *
 * Thu May 31 01:06:47 2012
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    lib.model.map
 */
class SocioPagosTableMap extends TableMap {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'lib.model.map.SocioPagosTableMap';

	/**
	 * Initialize the table attributes, columns and validators
	 * Relations are not initialized by this method since they are lazy loaded
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function initialize()
	{
		// attributes
		$this->setName('socio_pagos');
		$this->setPhpName('SocioPagos');
		$this->setClassname('SocioPagos');
		$this->setPackage('lib.model');
		$this->setUseIdGenerator(false);
		// columns
		$this->addPrimaryKey('ID', 'Id', 'BIGINT', true, null, null);
		$this->addForeignKey('CENTRO_ID', 'CentroId', 'INTEGER', 'centro', 'ID', false, null, null);
		$this->addForeignKey('SOCIO_ID', 'SocioId', 'BIGINT', 'socio', 'ID', false, null, null);
		$this->addForeignKey('TIPO_COBRO_ID', 'TipoCobroId', 'INTEGER', 'tipo_cobro', 'ID', false, null, null);
		$this->addForeignKey('PRODUCTO_ID', 'ProductoId', 'INTEGER', 'producto', 'ID', false, null, null);
		$this->addForeignKey('SUB_PRODUCTO_DE', 'SubProductoDe', 'INTEGER', 'producto', 'ID', false, null, null);
		$this->addColumn('NUMERO_PAGO', 'NumeroPago', 'INTEGER', false, null, 0);
		$this->addForeignKey('DETALLE_ORDEN_ID', 'DetalleOrdenId', 'BIGINT', 'detalle_orden', 'ID', false, null, null);
		$this->addColumn('FECHA_A_PAGAR', 'FechaAPagar', 'TIMESTAMP', false, null, null);
		$this->addColumn('FECHA_PAGADO', 'FechaPagado', 'TIMESTAMP', false, null, null);
		$this->addColumn('PAGADO', 'Pagado', 'BOOLEAN', false, null, false);
		$this->addColumn('ACUERDO_PAGO', 'AcuerdoPago', 'BOOLEAN', false, null, false);
		$this->addColumn('OBSERVACIONES', 'Observaciones', 'LONGVARCHAR', false, null, null);
		$this->addColumn('CANCELADO', 'Cancelado', 'BOOLEAN', false, null, false);
		$this->addForeignKey('ORDEN_BASE_ID', 'OrdenBaseId', 'BIGINT', 'orden', 'ID', false, null, null);
		$this->addColumn('CREATED_AT', 'CreatedAt', 'TIMESTAMP', false, null, null);
		$this->addColumn('UPDATED_AT', 'UpdatedAt', 'TIMESTAMP', false, null, null);
		$this->addColumn('PRECIO_LISTA', 'PrecioLista', 'DECIMAL', false, 10, 0);
		$this->addColumn('TIEMPO', 'Tiempo', 'INTEGER', false, null, 0);
		$this->addColumn('CANTIDAD', 'Cantidad', 'DECIMAL', false, 10, 0);
		$this->addColumn('DESCUENTO', 'Descuento', 'DECIMAL', false, 10, 0);
		$this->addColumn('SUBTOTAL', 'Subtotal', 'DECIMAL', false, 10, 0);
		$this->addForeignKey('PROMOCION_ID', 'PromocionId', 'BIGINT', 'promocion', 'ID', false, null, null);
		// validators
	} // initialize()

	/**
	 * Build the RelationMap objects for this table relationships
	 */
	public function buildRelations()
	{
		$this->addRelation('Centro', 'Centro', RelationMap::MANY_TO_ONE, array('centro_id' => 'id', ), null, null);
		$this->addRelation('Socio', 'Socio', RelationMap::MANY_TO_ONE, array('socio_id' => 'id', ), null, null);
		$this->addRelation('TipoCobro', 'TipoCobro', RelationMap::MANY_TO_ONE, array('tipo_cobro_id' => 'id', ), null, null);
		$this->addRelation('ProductoRelatedByProductoId', 'Producto', RelationMap::MANY_TO_ONE, array('producto_id' => 'id', ), null, null);
		$this->addRelation('ProductoRelatedBySubProductoDe', 'Producto', RelationMap::MANY_TO_ONE, array('sub_producto_de' => 'id', ), null, null);
		$this->addRelation('DetalleOrden', 'DetalleOrden', RelationMap::MANY_TO_ONE, array('detalle_orden_id' => 'id', ), null, null);
		$this->addRelation('Orden', 'Orden', RelationMap::MANY_TO_ONE, array('orden_base_id' => 'id', ), null, null);
		$this->addRelation('Promocion', 'Promocion', RelationMap::MANY_TO_ONE, array('promocion_id' => 'id', ), null, null);
	} // buildRelations()

	/**
	 *
	 * Gets the list of behaviors registered for this table
	 *
	 * @return array Associative array (name => parameters) of behaviors
	 */
	public function getBehaviors()
	{
		return array(
				'symfony' => array('form' => 'true', 'filter' => 'true', ),
				'symfony_behaviors' => array(),
				'symfony_timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', ),
		);
	} // getBehaviors()

} // SocioPagosTableMap
