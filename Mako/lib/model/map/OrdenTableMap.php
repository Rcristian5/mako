<?php


/**
 * This class defines the structure of the 'orden' table.
 *
 *
 * This class was autogenerated by Propel 1.4.2 on:
 *
 * Wed Mar  5 16:44:07 2014
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    lib.model.map
 */
class OrdenTableMap extends TableMap {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'lib.model.map.OrdenTableMap';

	/**
	 * Initialize the table attributes, columns and validators
	 * Relations are not initialized by this method since they are lazy loaded
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function initialize()
	{
		// attributes
		$this->setName('orden');
		$this->setPhpName('Orden');
		$this->setClassname('Orden');
		$this->setPackage('lib.model');
		$this->setUseIdGenerator(false);
		// columns
		$this->addPrimaryKey('ID', 'Id', 'BIGINT', true, null, null);
		$this->addForeignKey('CENTRO_ID', 'CentroId', 'INTEGER', 'centro', 'ID', false, null, null);
		$this->addForeignKey('SOCIO_ID', 'SocioId', 'BIGINT', 'socio', 'ID', false, null, null);
		$this->addColumn('FOLIO', 'Folio', 'INTEGER', false, null, null);
		$this->addColumn('TOTAL', 'Total', 'NUMERIC', false, 10, 0);
		$this->addForeignKey('FORMA_PAGO_ID', 'FormaPagoId', 'INTEGER', 'forma_pago', 'ID', false, null, 1);
		$this->addForeignKey('OPERADOR_ID', 'OperadorId', 'INTEGER', 'usuario', 'ID', false, null, null);
		$this->addForeignKey('TIPO_ID', 'TipoId', 'INTEGER', 'tipo_orden', 'ID', false, null, null);
		$this->addColumn('IP_CAJA', 'IpCaja', 'VARCHAR', false, 15, null);
		$this->addColumn('ES_FACTURA', 'EsFactura', 'BOOLEAN', false, null, false);
		$this->addColumn('RAZON_SOCIAL', 'RazonSocial', 'VARCHAR', false, 256, null);
		$this->addColumn('RFC', 'Rfc', 'VARCHAR', false, 13, null);
		$this->addColumn('CALLE_FISCAL', 'CalleFiscal', 'VARCHAR', false, 256, null);
		$this->addColumn('COLONIA_FISCAL', 'ColoniaFiscal', 'VARCHAR', false, 256, null);
		$this->addColumn('CP_FISCAL', 'CpFiscal', 'VARCHAR', false, 5, null);
		$this->addColumn('ESTADO_FISCAL', 'EstadoFiscal', 'VARCHAR', false, 256, null);
		$this->addColumn('MUNICIPIO_FISCAL', 'MunicipioFiscal', 'VARCHAR', false, 256, null);
		$this->addColumn('NUM_EXT_FISCAL', 'NumExtFiscal', 'VARCHAR', false, 60, null);
		$this->addColumn('NUM_INT_FISCAL', 'NumIntFiscal', 'VARCHAR', false, 60, null);
		$this->addColumn('PAIS_FISCAL', 'PaisFiscal', 'VARCHAR', false, 60, 'MEXICO');
		$this->addForeignKey('ESTATUS_ID', 'EstatusId', 'INTEGER', 'estatus_orden', 'ID', false, null, 1);
		$this->addForeignKey('CORTE_ID', 'CorteId', 'BIGINT', 'corte', 'ID', false, null, null);
		$this->addForeignKey('FACTURA_ID', 'FacturaId', 'BIGINT', 'factura', 'ID', false, null, null);
		$this->addColumn('CREATED_AT', 'CreatedAt', 'TIMESTAMP', false, null, null);
		$this->addColumn('UPDATED_AT', 'UpdatedAt', 'TIMESTAMP', false, null, null);
		$this->addColumn('NUM_APROBACION_TARJETA', 'NumAprobacionTarjeta', 'VARCHAR', false, 20, null);
		$this->addForeignKey('ID_COLONIA_FISCAL', 'IdColoniaFiscal', 'INTEGER', 'n_m01_colonia', 'CLNA_ID', false, null, null);
		// validators
	} // initialize()

	/**
	 * Build the RelationMap objects for this table relationships
	 */
	public function buildRelations()
	{
		$this->addRelation('Centro', 'Centro', RelationMap::MANY_TO_ONE, array('centro_id' => 'id', ), null, null);
		$this->addRelation('Socio', 'Socio', RelationMap::MANY_TO_ONE, array('socio_id' => 'id', ), null, null);
		$this->addRelation('FormaPago', 'FormaPago', RelationMap::MANY_TO_ONE, array('forma_pago_id' => 'id', ), null, null);
		$this->addRelation('Usuario', 'Usuario', RelationMap::MANY_TO_ONE, array('operador_id' => 'id', ), null, null);
		$this->addRelation('TipoOrden', 'TipoOrden', RelationMap::MANY_TO_ONE, array('tipo_id' => 'id', ), null, null);
		$this->addRelation('EstatusOrden', 'EstatusOrden', RelationMap::MANY_TO_ONE, array('estatus_id' => 'id', ), null, null);
		$this->addRelation('Corte', 'Corte', RelationMap::MANY_TO_ONE, array('corte_id' => 'id', ), null, null);
		$this->addRelation('Factura', 'Factura', RelationMap::MANY_TO_ONE, array('factura_id' => 'id', ), null, null);
		$this->addRelation('NM01Colonia', 'NM01Colonia', RelationMap::MANY_TO_ONE, array('id_colonia_fiscal' => 'clna_id', ), null, null);
		$this->addRelation('AlumnoPagos', 'AlumnoPagos', RelationMap::ONE_TO_MANY, array('id' => 'orden_base_id', ), null, null);
		$this->addRelation('CancelacionRelatedByOrdenId', 'Cancelacion', RelationMap::ONE_TO_MANY, array('id' => 'orden_id', ), null, null);
		$this->addRelation('CancelacionRelatedByOrdenCancelacionId', 'Cancelacion', RelationMap::ONE_TO_MANY, array('id' => 'orden_cancelacion_id', ), null, null);
		$this->addRelation('Conciliacion', 'Conciliacion', RelationMap::ONE_TO_MANY, array('id' => 'orden_id', ), null, null);
		$this->addRelation('DetalleOrden', 'DetalleOrden', RelationMap::ONE_TO_MANY, array('id' => 'orden_id', ), null, null);
		$this->addRelation('Salida', 'Salida', RelationMap::ONE_TO_MANY, array('id' => 'orden_id', ), null, null);
		$this->addRelation('SocioPagos', 'SocioPagos', RelationMap::ONE_TO_MANY, array('id' => 'orden_base_id', ), null, null);
	} // buildRelations()

	/**
	 *
	 * Gets the list of behaviors registered for this table
	 *
	 * @return array Associative array (name => parameters) of behaviors
	 */
	public function getBehaviors()
	{
		return array(
				'symfony' => array('form' => 'true', 'filter' => 'true', ),
				'symfony_behaviors' => array(),
				'symfony_timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', ),
		);
	} // getBehaviors()

} // OrdenTableMap