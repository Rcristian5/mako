<?php


/**
 * This class defines the structure of the 'reporte_categoria_cursos' table.
 *
 *
 * This class was autogenerated by Propel 1.4.0 on:
 *
 * Thu May 31 01:07:03 2012
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    lib.model.map
 */
class ReporteCategoriaCursosTableMap extends TableMap {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'lib.model.map.ReporteCategoriaCursosTableMap';

	/**
	 * Initialize the table attributes, columns and validators
	 * Relations are not initialized by this method since they are lazy loaded
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function initialize()
	{
		// attributes
		$this->setName('reporte_categoria_cursos');
		$this->setPhpName('ReporteCategoriaCursos');
		$this->setClassname('ReporteCategoriaCursos');
		$this->setPackage('lib.model');
		$this->setUseIdGenerator(false);
		// columns
		$this->addForeignPrimaryKey('CENTRO_ID', 'CentroId', 'INTEGER' , 'centro', 'ID', true, null, null);
		$this->addPrimaryKey('FECHA', 'Fecha', 'DATE', true, null, null);
		$this->addColumn('SEMANA', 'Semana', 'INTEGER', true, null, null);
		$this->addColumn('ANIO', 'Anio', 'INTEGER', true, null, null);
		$this->addColumn('MONTO', 'Monto', 'NUMERIC', true, 12, 0);
		$this->addColumn('CANTIDAD', 'Cantidad', 'INTEGER', true, null, null);
		$this->addPrimaryKey('CATEGORIA_ID', 'CategoriaId', 'INTEGER', true, null, null);
		$this->addColumn('SIGLAS', 'Siglas', 'VARCHAR', false, 60, null);
		$this->addColumn('CATEGORIA_NOMBRE', 'CategoriaNombre', 'VARCHAR', false, 120, null);
		// validators
	} // initialize()

	/**
	 * Build the RelationMap objects for this table relationships
	 */
	public function buildRelations()
	{
		$this->addRelation('Centro', 'Centro', RelationMap::MANY_TO_ONE, array('centro_id' => 'id', ), null, null);
	} // buildRelations()

	/**
	 *
	 * Gets the list of behaviors registered for this table
	 *
	 * @return array Associative array (name => parameters) of behaviors
	 */
	public function getBehaviors()
	{
		return array(
				'symfony' => array('form' => 'true', 'filter' => 'true', ),
				'symfony_behaviors' => array(),
		);
	} // getBehaviors()

} // ReporteCategoriaCursosTableMap
