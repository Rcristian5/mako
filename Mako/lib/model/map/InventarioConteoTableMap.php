<?php


/**
 * This class defines the structure of the 'inventario_conteo' table.
 *
 *
 * This class was autogenerated by Propel 1.4.0 on:
 *
 * Thu May 31 01:06:24 2012
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    lib.model.map
 */
class InventarioConteoTableMap extends TableMap {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'lib.model.map.InventarioConteoTableMap';

	/**
	 * Initialize the table attributes, columns and validators
	 * Relations are not initialized by this method since they are lazy loaded
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function initialize()
	{
		// attributes
		$this->setName('inventario_conteo');
		$this->setPhpName('InventarioConteo');
		$this->setClassname('InventarioConteo');
		$this->setPackage('lib.model');
		$this->setUseIdGenerator(true);
		$this->setPrimaryKeyMethodInfo('inventario_conteo_id_seq');
		// columns
		$this->addPrimaryKey('ID', 'Id', 'INTEGER', true, null, null);
		$this->addForeignKey('CORTE_ID', 'CorteId', 'INTEGER', 'inventario_corte', 'ID', false, null, null);
		$this->addForeignKey('OPERACION_ID', 'OperacionId', 'INTEGER', 'inventario_operacion', 'ID', false, null, null);
		$this->addForeignKey('PRODUCTO_ID', 'ProductoId', 'INTEGER', 'producto', 'ID', true, null, null);
		$this->addColumn('CANTIDAD_FINAL', 'CantidadFinal', 'BIGINT', true, null, null);
		$this->addColumn('CONTEO1', 'Conteo1', 'BIGINT', true, null, null);
		$this->addColumn('CONTEO2', 'Conteo2', 'BIGINT', true, null, null);
		$this->addForeignKey('CENTRO_ID', 'CentroId', 'INTEGER', 'centro', 'ID', true, null, null);
		$this->addColumn('CREATED_AT', 'CreatedAt', 'TIMESTAMP', false, null, null);
		// validators
	} // initialize()

	/**
	 * Build the RelationMap objects for this table relationships
	 */
	public function buildRelations()
	{
		$this->addRelation('InventarioCorte', 'InventarioCorte', RelationMap::MANY_TO_ONE, array('corte_id' => 'id', ), null, null);
		$this->addRelation('InventarioOperacion', 'InventarioOperacion', RelationMap::MANY_TO_ONE, array('operacion_id' => 'id', ), null, null);
		$this->addRelation('Producto', 'Producto', RelationMap::MANY_TO_ONE, array('producto_id' => 'id', ), null, null);
		$this->addRelation('Centro', 'Centro', RelationMap::MANY_TO_ONE, array('centro_id' => 'id', ), null, null);
	} // buildRelations()

	/**
	 *
	 * Gets the list of behaviors registered for this table
	 *
	 * @return array Associative array (name => parameters) of behaviors
	 */
	public function getBehaviors()
	{
		return array(
				'symfony' => array('form' => 'true', 'filter' => 'true', ),
				'symfony_behaviors' => array(),
				'symfony_timestampable' => array('create_column' => 'created_at', ),
		);
	} // getBehaviors()

} // InventarioConteoTableMap
