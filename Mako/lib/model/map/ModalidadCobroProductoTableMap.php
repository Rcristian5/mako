<?php


/**
 * This class defines the structure of the 'modalidad_cobro_producto' table.
 *
 *
 * This class was autogenerated by Propel 1.4.0 on:
 *
 * Thu May 31 01:06:45 2012
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    lib.model.map
 */
class ModalidadCobroProductoTableMap extends TableMap {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'lib.model.map.ModalidadCobroProductoTableMap';

	/**
	 * Initialize the table attributes, columns and validators
	 * Relations are not initialized by this method since they are lazy loaded
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function initialize()
	{
		// attributes
		$this->setName('modalidad_cobro_producto');
		$this->setPhpName('ModalidadCobroProducto');
		$this->setClassname('ModalidadCobroProducto');
		$this->setPackage('lib.model');
		$this->setUseIdGenerator(false);
		// columns
		$this->addForeignPrimaryKey('PRODUCTO_ID', 'ProductoId', 'INTEGER' , 'producto', 'ID', true, null, null);
		$this->addForeignPrimaryKey('COBRO_ID', 'CobroId', 'INTEGER' , 'tipo_cobro', 'ID', true, null, null);
		$this->addColumn('NUMERO_PAGOS', 'NumeroPagos', 'INTEGER', false, null, 0);
		// validators
	} // initialize()

	/**
	 * Build the RelationMap objects for this table relationships
	 */
	public function buildRelations()
	{
		$this->addRelation('Producto', 'Producto', RelationMap::MANY_TO_ONE, array('producto_id' => 'id', ), null, null);
		$this->addRelation('TipoCobro', 'TipoCobro', RelationMap::MANY_TO_ONE, array('cobro_id' => 'id', ), null, null);
	} // buildRelations()

	/**
	 *
	 * Gets the list of behaviors registered for this table
	 *
	 * @return array Associative array (name => parameters) of behaviors
	 */
	public function getBehaviors()
	{
		return array(
				'symfony' => array('form' => 'true', 'filter' => 'true', ),
				'symfony_behaviors' => array(),
		);
	} // getBehaviors()

} // ModalidadCobroProductoTableMap
