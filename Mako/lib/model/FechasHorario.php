<?php


/**
 * Skeleton subclass for representing a row from the 'fechas_horario' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.4.0 on:
 *
 * Wed Aug 18 14:05:03 2010
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    lib.model
 */
class FechasHorario extends BaseFechasHorario {
	public function  save(PropelPDO $con = null)
	{
		if($this->isNew() && !$this->getId())
		{
			$id = Comun::generaId();
			$this->setId($id);

		}

		parent::save($con);
	}
} // FechasHorario
