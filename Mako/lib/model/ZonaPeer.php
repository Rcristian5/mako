<?php


/**
 * Skeleton subclass for performing query and update operations on the 'zona' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.4.0 on:
 *
 * Wed Aug 10 15:33:04 2011
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    lib.model
 */
class ZonaPeer extends BaseZonaPeer {

	/**
	 * Regresa un arreglo con el listado de zonas
	 * @return Array Listado de zonas
	 */
	public function listaZonas()
	{
		$c = new Criteria();
		$c->add(self::ACTIVO,true);
		$c->addAscendingOrderByColumn(self::NOMBRE);
		return self::doSelect($c);
	}
} // ZonaPeer
