<?php


/**
 * Skeleton subclass for performing query and update operations on the 'cancelacion' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.4.0 on:
 *
 * Fri Jan 22 23:28:34 2010
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    lib.model
 */
class CancelacionPeer extends BaseCancelacionPeer {

	/**
	 * Devuelve la orden de compra original dada su orden de cancelacion
	 * @param Orden $oc
	 * @return Orden Orden original
	 */
	public static function ordenOriginal(Orden $oc)
	{
		$cs = $oc->getCancelacionsRelatedByOrdenCancelacionId();
		if(isset($cs[0]))
		{
			$clase="Cancelacion";
			if($cs[0] instanceof $clase)
			{
				return $cs[0]->getOrdenRelatedByOrdenId();
			}
		}
		return null;
	}

} // CancelacionPeer
