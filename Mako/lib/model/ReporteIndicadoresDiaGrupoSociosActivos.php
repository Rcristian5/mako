<?php


/**
 * Skeleton subclass for representing a row from the 'reporte_indicadores_dia_grupo_socios_activos' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.4.0 on:
 *
 * Mon Mar 19 10:03:20 2012
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    lib.model
 */
class ReporteIndicadoresDiaGrupoSociosActivos extends BaseReporteIndicadoresDiaGrupoSociosActivos {
	public function  save(PropelPDO $con = null)
	{
		if($this->isNew() && !$this->getId())
		{
			$this->setId(Comun::generaId());
		}

		parent::save($con);
	}
} // ReporteIndicadoresDiaGrupoSociosActivos
