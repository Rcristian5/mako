<?php


/**
 * Skeleton subclass for representing a row from the 'empresa' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.4.0 on:
 *
 * 03/03/10 21:30:14
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    lib.model
 */
class Empresa extends BaseEmpresa {

	/**
	 * Initializes internal state of Empresa object.
	 * @see        parent::__construct()
	 */
	public function __construct()
	{
		// Make sure that parent constructor is always invoked, since that
		// is where any default values for this object are set.
		parent::__construct();
	}

	public function __toString()
	{
		return trim($this->getNombre());
	}


} // Empresa
