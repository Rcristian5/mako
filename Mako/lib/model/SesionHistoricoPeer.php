<?php


/**
 * Skeleton subclass for performing query and update operations on the 'sesion_historico' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.4.0 on:
 *
 * Sat Feb 13 00:33:34 2010
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    lib.model
 */
class SesionHistoricoPeer extends BaseSesionHistoricoPeer {

	/**
	 * Historial de sesiones del socio
	 * @param bigint $socio
	 * @return array
	 */

	static public function historialSesionesSocios($socio)
	{
		$criteria = new Criteria();
		$criteria->add(SesionHistoricoPeer::SOCIO_ID,$socio,Criteria::EQUAL);
		$criteria->addAscendingOrderByColumn(SesionHistoricoPeer::FECHA_LOGOUT);
		$Sesiones = SesionHistoricoPeer::doSelect($criteria);

		return $Sesiones;

	}

	/**
	 * Historial por fechas de sesion
	 *
	 * @param string $fecha_ini
	 * @param string $fecha_fin
	 * @param int $centro_id
	 * @return array
	 */
	static public function historialPorFecha($fecha_ini,$fecha_fin,$centro_id)
	{
		$c = new Criteria();
		$c->addSelectColumn('sesion_historico.*');
		$c->addSelectColumn(CentroPeer::NOMBRE);
		$c->addSelectColumn(SocioPeer::FOLIO);
		$c->addSelectColumn(SeccionPeer::NOMBRE);

		$c->add(CentroPeer::ID,$centro_id);
		$c->add(SesionHistoricoPeer::FECHA_LOGIN,$fecha_ini,Criteria::GREATER_EQUAL);
		$c->addAnd(SesionHistoricoPeer::FECHA_LOGIN,$fecha_fin,Criteria::LESS_EQUAL);

		$c->addJoin(SesionHistoricoPeer::CENTRO_ID,CentroPeer::ID,  Criteria::JOIN);
		$c->addJoin(SesionHistoricoPeer::SOCIO_ID, SocioPeer::ID, Criteria::JOIN);
		$c->addJoin(SesionHistoricoPeer::COMPUTADORA_ID, ComputadoraPeer::ID, Criteria::JOIN);
		$c->addJoin(ComputadoraPeer::SECCION_ID, SeccionPeer::ID, Criteria::JOIN);

		$c->addAscendingOrderByColumn(SesionHistoricoPeer::FECHA_LOGIN);

		$rs = SesionHistoricoPeer::doSelectStmt($c);
		$sesiones =  $rs->fetchAll(PDO::FETCH_ASSOC);
		return $sesiones;
	}


} // SesionHistoricoPeer
