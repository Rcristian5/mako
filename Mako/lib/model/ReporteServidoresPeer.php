<?php


/**
 * Skeleton subclass for performing query and update operations on the 'reporte_servidores' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.4.0 on:
 *
 * Fri Oct  7 19:38:16 2011
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    lib.model
 */
class ReporteServidoresPeer extends BaseReporteServidoresPeer {

	public function sinRespuesta()
	{
		$d = array();
		foreach(self::doSelect(new Criteria()) as $r)
		{
			$d[]=array('centro'=>$r->getCentro()->getAliasReporte(),ultima_fecha=>$r->getUltimaFecha());
		}
		return $d;
	}
} // ReporteServidoresPeer
