<?php


/**
 * Skeleton subclass for representing a row from the 'sesion' table.
 *
 * 
 *
 * This class was autogenerated by Propel 1.4.0 on:
 *
 * Mon Feb 29 11:02:09 2016
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    lib.model
 */
class Sesion extends BaseSesion {

	/**
	 * Initializes internal state of Sesion object.
	 * @see        parent::__construct()
	 */
	public function __construct()
	{
		// Make sure that parent constructor is always invoked, since that
		// is where any default values for this object are set.
		parent::__construct();
	}

	 public function __toString()
        {

                return $this->getFechaLogin();

        }

	  /**
         * Override del metodo save, para poder generar el id custom
         * @param PropelPDO $con
         * @return void
         */
        public function save(PropelPDO $con = null)
        {
                //Si nuestro objeto es nuevo, generamos su identificador único.
                if($this->isNew())
                {
                        $this->setId(Comun::generaId());
                }

                parent::save();
        }


} // Sesion
