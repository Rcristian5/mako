<?php
/**
 * Skeleton subclass for representing a row from the 'tiempo_actividad' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.4.2 on:
 *
 * Tue Apr 29 11:50:27 2014
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    lib.model
 */
class TiempoActividad extends BaseTiempoActividad
{
   /**
    * Initializes internal state of TiempoActividad object.
    * @see        parent::__construct()
    */
   public function __construct()
   {
      // Make sure that parent constructor is always invoked, since that
      // is where any default values for this object are set.
      parent::__construct();
   }

   /**
    * Regresa el nombre de "TiempoActividad"
    * @return string El nombre del tiempo de la actividad
    */
   public function __toString()
   {
      return $this->getNombre();
   }
} // TiempoActividad
