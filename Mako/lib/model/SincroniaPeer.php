<?php

ini_set('default_socket_timeout', 10);
/**
 * Skeleton subclass for performing query and update operations on the 'sincronia' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.4.0 on:
 *
 * Sun Aug 22 23:21:59 2010
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    lib.model
 */
class SincroniaPeer extends BaseSincroniaPeer {

	/**
	 * Utilizamos este arreglo para llevar control de cuales centros tienen algun problema al sincronizar y detenemos la ejecucion hasta que
	 * Alguien manualmente revise el problem y actue en consecuencia.
	 *
	 * @var array
	 */
	private static $centrosError = array();


	/**
	 * Cliente  de ws de sincronización.
	 *
	 * @param Sincronia $sync El registro a sincronizar.
	 */
	public static function sincronizaRegistro(Sincronia $sync)
	{

		switch ($sync->getTipo())
		{
			case 'broadcast': //En broadcast el registro a sincronizar se envía a todos los centros.

				//Traemos las tareas que no han sido completadas por centro
				$c = new Criteria();
				$c->add(SincroniaCentroPeer::SINCRONIZADO,false);
				$c->add(SincroniaCentroPeer::CENTRO_ID,9999,Criteria::NOT_EQUAL);
				$c->addAscendingOrderByColumn(SincroniaCentroPeer::CREATED_AT);
				$aCs = $sync->getSincroniaCentros($c);

				//echo "Tareas por hacer en esta principal: ".$sync->getPkAsync()." " .count($aCs)."\n";

				//Si ya no hay tareas que sincronizar en el centro, entonces la tarea principal ha sigo completada.
				if(count($aCs)==0)
				{
					$sync->setCompleto(true);
					$sync->save();
						
				}

				//echo("-----TRAE N REGISTROS ----------------------------".count($aCs)."\n");
				$conta=0;
				foreach($aCs as $sc)
				{

					$conta++;
					if(!$sc->getCentro()->getActivo()) continue;
						
					error_log($sc->getCentroId().": ".$sc->getId()." Sync".$sc->getSincronizado()." c:".$conta);
						
					if(isset(self::$centrosError[$sc->getCentroId()]))
					{
						//error_log("Saltando registro por error previo en cenrtro: ".$sc->getCentroId()." : ".$sc->getId()." op ".$sc->getSincronia()->getOperacion(). " PK: ".$sc->getSincronia()->getPkAsync());
						continue;
					}
						
					$res = '';
					try  //El try de la conexion al cliente
					{
						//Inicializamos el cliente del ws para el centro dado:
						$p = sfConfig::get('app_proto_wsdl');
						$p='http://';

						$chost = $sc->getCentro()->getIp();
						$wsdl = sfConfig::get('app_path_mako_wsdl');
						$wsdl = 'MakoApi.wsdl';

						$cliente = @new SoapClient($p.$chost."/".$wsdl,array("connection_timeout"=>5));
					}
					catch (Exception $e)
					{
						$ermsg = "Error:[ClienteWsSyncConexion]: $chost sync:".$sync->getId()." operacion: ".$sync->getOperacion()." PK: ".$sync->getPkAsync()." Err: ".$e->getMessage();
						error_log($ermsg);
						$sc->setUltimoMensaje($e->getMessage());
						$sc->setIntento($sc->getIntento()+1);
						$sc->save();
						self::$centrosError[$sc->getCentroId()] = true;
						continue;
					}
						
					//El try de la ejecucion del metodo
					try
					{
						$res = $cliente->sync_recibeSync($sync->getDetalle(),$sync->getOperacion(),$sync->getClase(),$sync->getPkAsync());

						$sc->setIntento($sc->getIntento()+1);
						$sc->setSincronizado(true);
						$sc->save();

						error_log("Sync OK: $chost sync: ".$sync->getId()." operacion: ".$sync->getOperacion()." PK: ".$sync->getPkAsync());
						continue;
					}
						
					catch (Exception $e2)
					{
						$ermsg = "Error:[ClienteWsSync]: $chost sync: ".$sync->getId(). " operacion: ".$sync->getOperacion()." PK: ".$sync->getPkAsync()." Err: ".$e2->getMessage();
						error_log($ermsg);
						$sc->setUltimoMensaje($e2->getMessage());
						$sc->setIntento($sc->getIntento()+1);
						$sc->save();
						self::$centrosError[$sc->getCentroId()] = true;
						continue;
					}
				}
				break;
					
		}

	}

	/**
	 * Funcion para borrar los registros de sincronia local si
	 * no se han mandado al centro
	 *
	 * @param bigint $horarioId - Identificador del horario
	 * @param bigint $grupoId - Identificador del grupo
	 */
	public static function borraSincroniaLocalCalendarizacion($horarioId, $grupoId)
	{
		$sincroniaHorario = self::retrieveByPKAsync($horarioId);
		$sincroniaGrupo = self::retrieveByPKAsync($grupoId);
		if(!$sincroniaGrupo->getCompleto() && !$sincroniaHorario->getCompleto())
		{
			// Si no se ha ejecutado el task de sincronia se borran los registros
			// de sincronia de horario y grupo
			foreach ($sincroniaHorario->getSincroniaCentros() as $sincroniaCentro)
			{
				$sincroniaCentro->delete();
			}
			$sincroniaHorario->delete();
			foreach ($sincroniaGrupo->getSincroniaCentros() as $sincroniaCentro)
			{
				$sincroniaCentro->delete();
			}
			$sincroniaGrupo->delete();
		}

	}
	/**
	 * Funcion que regresa el registro de sincronia por la el registro pk_async
	 *
	 * @param bigint $pkAsync - Identificador de la sincronia
	 *
	 * @return Sincronia - Objeto asociado con el registro de sincronia
	 */
	public static function retrieveByPKAsync($pkAsync)
	{
		$criteria = new Criteria();
		$criteria->add(SincroniaPeer::PK_ASYNC, $pkAsync);

		return SincroniaPeer::doSelectOne($criteria);
	}



} // SincroniaPeer
