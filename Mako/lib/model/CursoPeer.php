<?php


/**
 * Skeleton subclass for performing query and update operations on the 'curso' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.4.0 on:
 *
 * 01/30/10 01:59:29
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    lib.model
 */
class CursoPeer extends BaseCursoPeer {


	/**
	 * Event dispatcher para curso publicado
	 *
	 * @param array $cursp - Objeto curso
	 */
	static public function dispatcherCursosCreado($curso)
	{
		sfProjectConfiguration::getActive()->getEventDispatcher()->notify(new sfEvent($this, 'curso.publicado', array( 'curso' => $curso )));
	}

	/**
	 * Busca si un curso esta registrado en Mako por su clave
	 *
	 * @param string $q - Clave del curso a buscar
	 * @return boolean - TRUE existe, FALSE no existe
	 */
	static public function buscarClave($q)
	{
		$c = new Criteria();
		$c->add(CursoPeer::CLAVE, $q);
		$curso = CursoPeer::doSelectOne($c);
		$flag = false;
		if($curso)
		{
			$flag = true;
		}

		return $flag;
	}

	/**
	 * Publica un curso, devolvioendo TRUE si ya ha sido publicado y FALSE si no
	 * @param int $q id del curso
	 * @return boolean Estado del curso
	 */
	static public function publicaCurso($q)
	{
		$curso = CursoPeer::retrieveByPK($q);
		$flag = false;
		if($curso->getPublicado())
		{
			$flag = true;
		}
		else
		{
			$flag = false;
		}
		$curso->setPublicado(true);
		$curso->save();

		return $flag;
	}

	/**
	 * Publica un curso en moodle
	 *
	 * @param int $q - Id del curso
	 * @return string - Respuesta de la operación
	 */
	static public function publicarMoodle($q)
	{
		$curso = CursoPeer::retrieveByPK($q);
		//Se intenta conectar con moodle
		try
		{
			//Inicializamos el cliente con la direccion del wsdl de los ws de moodle
			$cliente = new SoapClient(sfConfig::get('app_soap_client'));
			//Obtenemos el token de sesion
			$sesion = $cliente->login(sfConfig::get('app_soap_user'),sfConfig::get('app_soap_password'));
			//Se obtinen todas la categorias de moodle
			$categorias = $cliente->get_categories($sesion->client, $sesion->sessionkey);
			$categoriaMdlId = 0;
			//Buscamos la categoria del curos en moodle
			foreach($categorias->categories as $categoria)
			{
				if($categoria->name == $curso->getCategoriaCurso()->getNombre())
				{
					$categoriaMdlId = $categoria->id;
				}
			}
			if($categoriaMdlId!=0)
			{
				//Se cargan las classes de tupos de objetos que espera recibir el ws de moodle.
				require_once dirname(__FILE__).'/../../ws_proxy/editCoursesInput.php';
				require_once dirname(__FILE__).'/../../ws_proxy/courseData.php';

				//Instanciamos el tipo
				$cursos = new editCoursesInput();

				//Generamos los atributos
				$cursos->courses = array(
						array(
								'action' => 'Add',
								'id'=>$curso->getId(),
								'sortorder'=>0,
								'password'=>'',
								'category' => $categoriaMdlId,
								'fullname' => $curso->getNombre(),
								'shortname' => $curso->getNombreCorto(),
								'idnumber' => $curso->getClave(),
								'summary' => $curso->getDescripcion(),
								'format' => 'topics',
								'showgrades'=> 1,
								'newsitems'=> 5,
								'teacher'=> 'Profesor',
								'teachers'=> 'Profesores',
								'student'=> 'Estudiante',
								'students'=> 'Estudiantes',
								'guest'=> 0,
								'startdate'=> time()+18000*10,
								'enrolperiod'=> 0,
								'numsections'=> 4,
								'marker'=> 0,
								'maxbytes'=> 2097152,
								'visible'=> 1,
								'hiddensections'=> 0,
								'groupmode'=> 0,
								'groupmodeforce'=>0 ,
								'lang'=> 'es_mx_utf8',
								'theme'=> '',
								'cost'=> '',
								'timecreated'=> time(),
								'timemodified'=> time(),
								'metacourse'=> 0
						)
				);
				//Se intenta añadir el curso a moodle
				try
				{
					$res = $cliente->edit_courses($sesion->client, $sesion->sessionkey, $cursos);

					if($res->curses[0]->error =='')
					{
						$curso->setRegistradoMoodle(true);
						$curso->save();
						$mensaje = 'El curso se creo correctamente en Moodle';
					}
					else
					{
						$mensaje = 'El curso ya esta registrado en Moodle';
					}
				}
				catch (ErrorException $e)
				{
					error_log($e->getMessage());
					$mensaje = 'No se pudo crear el curso en Moodle';
				}
			}
			else
			{
				$mensaje = 'No se encontro la categoría en Moodle';
			}
		}
		catch (Exception $e)
		{
			error_log($e);
			$mensaje = 'No se pudo conectar con moodle';
		}

		return $mensaje;
	}

	/**
	 * Verifica la asociacion de todos los cursos
	 * @return array Arreglo con la aosciacion de todos los cursos
	 */
	static public function verificarTodosCursos()
	{
		$cursos = CursoPeer::doSelect(new Criteria());
		$cursosMako = array();
		foreach ($cursos as $curso)
		{
			$actualizacion = $curso->getUpdatedAt('Y-m-d H:i:s');
			$vinculado = 'NINGUNA';
			if($curso->getAsociadoMoodle())
			{
				$vinculado = 'MOODLE';
			}
			if($curso->getAsociadoRs())
			{
				$vinculado = 'ROSETTA STONE';
			}
			if($curso->getAsociadoRs() && $curso->getAsociadoMoodle())
			{
				$vinculado = 'MOODLE Y ROSETTA STONE';
			}
			$cursosMako[$curso->getClave()] = array('categoria'   => $curso->getCategoriaCurso()->getNombre(),
					'clave'       => $curso->getClave(),
					'nombre'      => $curso->getNombre(),
					'vinculacion' => $vinculado,
					'asociado'    => array('moodle'=>$curso->getAsociadoMoodle(), 'rs' => $curso->getAsociadoRs()),
					'conflicto'   => '');
			//Guardamos la última fecha de verificacion
			$curso->setUltimaVerificacion(date());
			$curso->save();
			$curso->setUpdatedAt($actualizacion);
			$curso->save();
		}
		//Inicializamos el cliente con la direccion del wsdl de los ws de moodle
		$cliente = new SoapClient(sfConfig::get('app_soap_client'));

		//Obtenemos el token de sesion

		//Tratamos de conectar con Moodle
		try
		{
			//Obtenemos el token de sesion
			$sesion = $cliente->login(sfConfig::get('app_soap_user'),sfConfig::get('app_soap_password'));
			//Obtenemos las categrias de los cursos
			$categorias = $cliente->get_categories($sesion->client, $sesion->sessionkey);
			//Creamos un arreglo con los id`s y el nombre de la cateoria
			$categoriasCurso = array();
			foreach ($categorias->categories as $categoria)
			{
				$categoriasCurso[$categoria->id] = $categoria->name;
			}
			//Solicitamos la lista de cursos guardad en moodle
			$cursos = $cliente->get_courses($sesion->client, $sesion->sessionkey);
			$cursosMoodle = array();
			foreach ($cursos->courses as $curso)
			{
				$cursosMoodle[$curso->idnumber] = array('categoria'   => $categoriasCurso[$curso->category],
						'clave'       => $curso->idnumber,
						'nombre'      => $curso->fullname,
						'vinculacion' => 'MOODLE',
						'conflicto'   => '');
			}
			//Eliminamos los cursos que si coinciden
			$cursosCorrectos = 0;
			foreach ($cursosMako as $key => $curso)
			{
				if(array_key_exists($key, $cursosMoodle))
				{
					if($curso['asociado']['moodle'])
					{
						unset ($cursosMako[$key]);
						unset ($cursosMoodle[$key]);
						$cursosCorrectos++;
					}
					else
					{
						$cursosMako[$key]['conflicto'] = 'El curso existe en Moodle, pero no hay vinculación en Mako';
					}
				}
				else
				{
					if($curso['asociado']['moodle'])
					{
						$cursosMako[$key]['conflicto'] = 'El curso no existe en Moodle';
					}
					else
					{
						unset ($cursosMako[$key]);
					}
				}
			}

			$arreglo = array('conflicto' => false);

			//Si quedan cursos en los arreglos se arma un arreglo con los conflictos encontrados
			if( (count($cursosMako) > 0) || (count($cursosMoodle) > 0) )
			{
				$arreglo = array('conflicto' => true, 'cursos_correctos' => $cursosCorrectos, 'cursos_conflicto' => count($cursosMako) + count($cursosMoodle), 'lista_conflicto' => array());
				foreach ($cursosMako as $curso)
				{
					$arreglo['lista_conflicto'][] = $curso;
				}
				foreach ($cursosMoodle as $curso)
				{
					$curso['conflicto'] = 'El curso no existe en Mako';
					$arreglo['lista_conflicto'][] = $curso;
				}
			}

			return $arreglo;
		}
		//Enviamos error a la consola
		catch (Exception $e)
		{
			error_log($e->getMessage());

			return false;
		}
	}

	/**
	 * Verifica la asociacion de un curso a Moodle o RS
	 * @param array $curso - Objeto del curso
	 * @return array Arreglo con la aosciacion de todos los cursos
	 */
	static public function verificarCurso($curso)
	{
		//Inicializamos el cliente con la direccion del wsdl de los ws de moodle
		$cliente = new SoapClient(sfConfig::get('app_soap_client'));
		//Tratamos de conectar con moodle
		try
		{
			//Obtenemos el token de sesion
			$sesion = $cliente->login(sfConfig::get('app_soap_user'),sfConfig::get('app_soap_password'));
			//Se revisa si el curso esta asociado a Moodle
			if($curso->getAsociadoMoodle())
			{
				try
				{
					$verificacion = $cliente->get_courses($sesion->client, $sesion->sessionkey, array($curso->getClave()), 'idnumber');
					$mensaje = 'Curso asociado a Moodle correctamente';
					$verificacion = true;
				}
				catch (Exception $e)
				{
					error_log($e->getMessage());
					$mensaje = 'No se encontro el curso en Moodle';
					$verificacion = false;
				}
			}
			//Si no esta asociado se busca que no exista en Moodle
			else
			{
				try
				{
					$verificacion = $cliente->get_courses($sesion->client, $sesion->sessionkey, array($curso->getClave()), 'idnumber');
					$mensaje = 'Existe un curso en Moodle con la misma clave, pero no esta asociado a un curso en Mako';
					$verificacion = false;
				}
				catch (Exception $e)
				{
					error_log($e->getMessage());
					$mensaje = 'El curso no esta asociado en Mako, y no se encontro en Moodle';
					$verificacion = true;
				}
			}
			if($curso->getAsociadoRs())
			{
				//IMPLEMENTAR AQUI LA BUSQUEDA DE CURSOS EN RS EN LA SEGUNDA ETAPA
				$mensaje = 'Por el momento no hay verificación con Rosetta Stone';
				$verificacion = false;
			}
			$vinculado = 'NINGUNA';
			if($curso->getAsociadoMoodle())
			{
				$vinculado = 'MOODLE';
			}
			if($curso->getAsociadoRs())
			{
				$vinculado = 'ROSETTA STONE';
			}
			if($curso->getAsociadoRs() && $curso->getAsociadoMoodle())
			{
				$vinculado = 'MOODLE Y ROSETTA STONE';
			}
			$vinculacion = array('curso' => $curso->getNombre(),
					'categoria'    => $curso->getCategoriaCurso()->getNombre(),
					'clave'        => $curso->getClave(),
					'vinculacion'  => $vinculado,
					'verificacion' => $verificacion,
					'mensaje'      => $mensaje
			);
			//Guardamos la última fecha de verificacion
			$curso->setUltimaVerificacion(date());
			$curso->save();

			return $vinculacion;
		}
		catch (Exception $e)
		{
			error_log($e->getMessage());

			return false;
		}
	}


	/**
	 * Obtine las "califaciones"
	 * @param array $curso - Objeto del curso
	 * @return array Arreglo con la aosciacion de todos los cursos
	 */
	static public function listaCalificacionesMoodle()
	{
		//Inicializamos el cliente con la direccion del wsdl de los ws de moodle
		$cliente = new SoapClient(sfConfig::get('app_soap_client'));
		//Obtenemos el token de sesion
		$sesion = $cliente->login(sfConfig::get('app_soap_user'),sfConfig::get('app_soap_password'));
		//Se revisa si el curso esta asociado a Moodle
		try
		{
			$evalucaiones = $cliente->get_grades($sesion->client, $sesion->sessionkey);
			foreach ($evalucaiones as $evaluacion)
			{
				print_r($evaluacion);
				echo '</br>';
			}
		}
		catch (Exception $e)
		{
			echo 'No se encontro el curso en Moodle';
			$verificacion = false;
		}

		return false;
	}

	/**
	 * Obtiene la lista de cursos para ser seriados
	 * @param int $id id del curso
	 * @return boolean Estado del curso
	 */
	static public function rutaAprendizaje($id)
	{
		$arreglo = array();
		if($id)
		{
			$c = new Criteria();
			$c->add(CursoPeer::ID, $id, Criteria::NOT_EQUAL);
			$cursos = CursoPeer::doSelect($c);
		}
		else
		{
			$cursos = CursoPeer::doSelect(new Criteria());
		}
		foreach ($cursos as $curso)
		{
			$arreglo[$curso->getId()] = $curso->getClave();
		}

		return $arreglo;
	}
	/**
	 * Comprueba si el curso se encuentra publicado
	 * @param int $q id del curso
	 * @return boolean Estado del curso
	 */
	static public function esCursoPublicado($q)
	{
		$curso = CursoPeer::retrieveByPK($q);
		$flag = false;
		if($curso->getPublicado())
		{
			$flag = true;
		}

		return $flag;
	}

	/**
	 * Devulve un arreglo con los datos del curso seleccionado
	 * @param int $q id del curso
	 * @return boolean Estado del curso
	 */
	static public function datosCurso($q)
	{
		$c = new Criteria();
		$c->add(CursoPeer::ID, $q);
		$curso = CursoPeer::doSelectJoinAll($c);
		foreach ($curso as $detalle)
		{
			$datos_curso = $detalle;
		}
		$arreglo = array('datos'=>$datos_curso, 'restriccion_horario' => array(), 'restriccion_socio'=>array());
		//Se obtienen las restricciones de horario
		$rh = new Criteria();
		$rh->add(RestriccionesHorarioPeer::CURSO_ID, $datos_curso->getId());
		$restriccion_horario = RestriccionesHorarioPeer::doSelectOne($rh);
		$arreglo['restriccion_horario'] = $restriccion_horario;
		//Se obtinen las restricciones de socio
		$rs = new Criteria();
		$rs->add(RestriccionesSocioPeer::CURSO_ID, $datos_curso->getId());
		$restriccion_socio = RestriccionesSocioPeer::doSelectOne($rs);
		$arreglo['restriccion_socio'] = $restriccion_socio;

		return $arreglo;
	}

	/**
	 * Devuelve la duración porcentual, total y por capitulo de un curso
	 * @param int $q id del curso
	 * @return array Areglo con los tipos de ducraciones por curso
	 */
	static public function duracionTotal($q)
	{
		$curso = CursoPeer::retrieveByPK($q);
		$c = new Criteria();
		$c->clearSelectColumns();
		$c->addSelectColumn('SUM('.CapituloPeer::DURACION.')');
		$c->add(CapituloPeer::CURSO_ID,$curso->getId());
		$stmt = CursoPeer::doSelectStmt($c);
		$duracion = $stmt->fetch();
		$porcentaje = number_format(($duracion['sum'] * 100) / $curso->getDuracion(), 2,'.','.');
		$arreglo = array('total'=>$curso->getDuracion(), 'capitulos'=>$duracion['sum'], 'porcentual'=>$porcentaje);

		return $arreglo;
	}
	/**
	 * Obtiene todas las fechas de cursos que se impartan en un aula
	 * @param int $q id del aula
	 * @return array Arreglo con las fechas ocupadas en un alua
	 */
	static public function listaCursosCalendarizados($q)
	{
		$c = new Criteria();
		$c->clearSelectColumns();
		$c->addSelectColumn(CursoPeer::CLAVE);
		$c->addSelectColumn(CursoPeer::NOMBRE);
		$c->addSelectColumn(CursoPeer::CATEGORIA_CURSO_ID.' AS categoria');
		$c->addSelectColumn(DiasPeer::DIA);
		$c->addSelectColumn(DiasPeer::HORA_INICIO);
		$c->addSelectColumn(DiasPeer::HORA_FIN);
		$c->addSelectColumn(GrupoPeer::CLAVE.' AS grupo');
		$c->addSelectColumn(UsuarioPeer::NOMBRE_COMPLETO.' AS facilitador');
		$c->addSelectColumn(GrupoPeer::CUPO);
		$c->addSelectColumn(GrupoPeer::ID);
		$c->addSelectColumn(RecursoInfraestructuraPeer::NOMBRE.' AS aula');
		if($q!='')
		{
			$c->add(GrupoPeer::AULA_ID,$q);
		}
		$c->add(GrupoPeer::ACTIVO,true);
		$c->add(GrupoPeer::CENTRO_ID,sfConfig::get('app_centro_actual_id'));
		$c->addJoin(GrupoPeer::HORARIO_ID, HorarioPeer::ID);
		$c->addJoin(HorarioPeer::ID, DiasPeer::HORARIO_ID);
		$c->addJoin(CursoPeer::ID, GrupoPeer::CURSO_ID);
		$c->addJoin(GrupoPeer::AULA_ID, RecursoInfraestructuraPeer::ID);
		$c->addJoin(GrupoFacilitadorPeer::GRUPO_ID,GrupoPeer::ID);
		$c->addJoin(UsuarioPeer::ID, GrupoFacilitadorPeer::USUARIO_ID);
		$stmt = DiasPeer::doSelectStmt($c);
		$cursos = $stmt->fetchAll();

		return $cursos;
	}

	/**
	 * Obtiene todos los cursos no seriados de una categoria con su respectivo horario
	 * @param int $q id de la categoria del curso
	 * @return array Arreglo los cursos no seriados de un categoria y su horario
	 */
	static public function listaCursosNoSeriados($q, $u)
	{
		$finalizados = CursosFinalizadosPeer::listaFinalizados($u);
		$tomados = GrupoAlumnosPeer::cursosInscrito(array('alumno_id'=>$u, 'cursos'=>$finalizados));
		$socio = SocioPeer::retrieveByPK($u);
		$c = new Criteria();
		$c->clearSelectColumns();
		$c->addSelectColumn(CursoPeer::ID);
		$c->addSelectColumn(CursoPeer::NOMBRE);
		$c->addSelectColumn(CursoPeer::CLAVE);
		$c->addSelectColumn(CursoPeer::DESCRIPCION);
		$c->addSelectColumn(CursoPeer::DURACION);
		$c->add(CursoPeer::CATEGORIA_CURSO_ID,$q);
		$c->add(CursoPeer::SERIADO,false);
		$c->add(CursoPeer::PUBLICADO,true);
		$c->add(GrupoPeer::ACTIVO,true);
		$c->addJoin(GrupoPeer::CURSO_ID, CursoPeer::ID);
		$c->addGroupByColumn(CursoPeer::ID);
		$c->addGroupByColumn(CursoPeer::NOMBRE);
		$c->addGroupByColumn(CursoPeer::CLAVE);
		$c->addGroupByColumn(CursoPeer::DESCRIPCION);
		$c->addGroupByColumn(CursoPeer::DURACION);
		$stmt = GrupoPeer::doSelectStmt($c);
		$cursos = $stmt->fetchAll();
		//Se obtienen las restricciones de los cursos
		$no_calificados = array();
		foreach ($cursos as $curso)
		{
			$r = new Criteria();
			$r->add(RestriccionesSocioPeer::CURSO_ID,$curso['id']);
			$restricciones = RestriccionesSocioPeer::doSelectJoinAll($r);
			if(count($restricciones)>0)
			{
				foreach ($restricciones as $restriccion)
				{
					if($restriccion->getEdadMinima())
					{
						if($restriccion->getEdadMinima()>$socio->getEdad())
						{
							$no_calificados[$curso['id']] = $curso['id'];
						}
					}
					if($restriccion->getEdadMaxima())
					{
						if($restriccion->getEdadMaxima()<$socio->getEdad())
						{
							$no_calificados[$curso['id']] = $curso['id'];
						}
					}
				}
			}
		}
		//Se obtienen la s fechas de inicio y fin de cursos
		$arreglo = array();
		$validos = array();
		foreach ($cursos as $curso)
		{
			$valido = true;
			if(array_key_exists($curso['id'], $tomados))
			{
				$valido = false;
			}
			if(array_key_exists($curso['id'], $finalizados))
			{
				$valido = false;
			}
			if(array_key_exists($curso['id'], $no_calificados))
			{
				$valido = false;
			}

			if($valido)
			{
				$h = new Criteria();
				$h->clearSelectColumns();
				$h->addSelectColumn(GrupoPeer::ID);
				$h->addSelectColumn(CursoPeer::NOMBRE);
				$h->addSelectColumn(HorarioPeer::FECHA_FIN);
				$h->addSelectColumn(HorarioPeer::FECHA_INICIO);
				$h->addSelectColumn(DiasPeer::HORA_FIN);
				$h->addSelectColumn(DiasPeer::HORA_INICIO);
				$h->addSelectColumn(GrupoPeer::CUPO);
				$h->addSelectColumn(RecursoInfraestructuraPeer::CAPACIDAD);
				$h->add(GrupoPeer::CURSO_ID,$curso['id']);
				$h->add(GrupoPeer::CUPO_ALCANZADO,false);
				$h->addJoin(HorarioPeer::ID, GrupoPeer::HORARIO_ID);
				$h->addJoin(HorarioPeer::FECHA_INICIO, DiasPeer::DIA);
				$h->addJoin(HorarioPeer::ID, DiasPeer::HORARIO_ID);
				$h->addJoin(GrupoPeer::CURSO_ID,CursoPeer::ID);
				$h->add(GrupoPeer::ACTIVO,true);
				$h->addJoin(GrupoPeer::AULA_ID,RecursoInfraestructuraPeer::ID);
				$stmt = HorarioPeer::doSelectStmt($h);
				$horario = $stmt->fetchAll();
				if (!array_key_exists($curso['id'], $arreglo))
				{
					$arreglo[$curso['id']] = $horario;
					$validos[$curso['id']] = $curso;
				}
			}
		}

		return array('cursos'=>$validos, 'horario'=>$arreglo);
	}

	/**
	 * Obtiene todos los cursos  seriados de una categoria
	 * @param int $q id de la categoria del curso
	 * @return array Arreglo los cursos  seriados e una categoria
	 */
	static public function listaCursosSeriados($q)
	{
		$finalizados = CursosFinalizadosPeer::listaFinalizados($u);
		$c = new Criteria();
		$c->clearSelectColumns();
		$c->addAlias('c1', CursoPeer::TABLE_NAME);
		$c->addAlias('c2', CursoPeer::TABLE_NAME);
		$c->addSelectColumn(CursoPeer::alias('c1', CursoPeer::ID));
		$c->addSelectColumn(CursoPeer::alias('c2', CursoPeer::ID.' AS ID_SERIADO'));
		$c->addSelectColumn(CursoPeer::alias('c1', CursoPeer::NOMBRE));
		$c->addSelectColumn(CursoPeer::alias('c2', CursoPeer::NOMBRE. ' AS NOMBRE_SERIADO'));
		$c->addSelectColumn(CursoPeer::alias('c1', CursoPeer::CLAVE));
		$c->addSelectColumn(CursoPeer::alias('c1', CursoPeer::DESCRIPCION));
		$c->addSelectColumn(CursoPeer::alias('c1', CursoPeer::DURACION));
		$c->add(CursoPeer::CATEGORIA_CURSO_ID,$q);
		$c->add(CursoPeer::SERIADO,true);
		foreach ($finalizados as $finalizado)
		{
			$c->add(CursoPeer::ID, $finalizado, Criteria::NOT_EQUAL);
		}
		$c->addJoin(GrupoPeer::CURSO_ID, CursoPeer::ID);
		$c->add(GrupoPeer::ACTIVO,true);
		$c->addJoin(CursoPeer::alias('c1', CursoPeer::CURSO_SERIADO_ID), CursoPeer::alias('c2', CursoPeer::ID));
		$c->addGroupByColumn(GrupoPeer::CURSO_ID);
		$c->addGroupByColumn(CursoPeer::alias('c1', CursoPeer::ID));
		$c->addGroupByColumn(CursoPeer::alias('c2', CursoPeer::ID));
		$c->addGroupByColumn(CursoPeer::alias('c1', CursoPeer::NOMBRE));
		$c->addGroupByColumn(CursoPeer::alias('c2', CursoPeer::NOMBRE));
		$c->addGroupByColumn(CursoPeer::alias('c1', CursoPeer::CLAVE));
		$c->addGroupByColumn(CursoPeer::alias('c1', CursoPeer::DESCRIPCION));
		$c->addGroupByColumn(CursoPeer::alias('c1', CursoPeer::DURACION));
		$stmt = GrupoPeer::doSelectStmt($c);
		$cursos = $stmt->fetchAll();

		$arreglo = array();
		foreach ($cursos as $curso)
		{
			$h = new Criteria();
			$h->clearSelectColumns();
			$h->addSelectColumn(GrupoPeer::ID);
			$h->addSelectColumn(CursoPeer::NOMBRE);
			$h->addSelectColumn(HorarioPeer::FECHA_FIN);
			$h->addSelectColumn(HorarioPeer::FECHA_INICIO);
			$h->addSelectColumn(DiasPeer::HORA_FIN);
			$h->addSelectColumn(DiasPeer::HORA_INICIO);
			$h->add(GrupoPeer::CURSO_ID,$curso['id']);
			$h->add(GrupoPeer::ACTIVO,true);
			$h->addJoin(HorarioPeer::ID, GrupoPeer::HORARIO_ID);
			$h->addJoin(HorarioPeer::FECHA_INICIO, DiasPeer::DIA);
			$h->addJoin(HorarioPeer::ID, DiasPeer::HORARIO_ID);
			$h->addJoin(GrupoPeer::CURSO_ID,CursoPeer::ID);
			$stmt = HorarioPeer::doSelectStmt($h);
			$horario = $stmt->fetchAll();
			$arreglo[$curso['id']] = $horario;
		}

		return array('cursos'=>$cursos, 'horario'=>$arreglo);
	}

	/**
	 * Obtiene todos los cursos no asociados a moodle o rosseta
	 * @return array Arreglo los cursos no asociados
	 */
	static public function listaCursosNoAsociados()
	{
		$c = new Criteria();
		$c->add(CursoPeer::ASOCIADO_MOODLE, false);
		$c->add(CursoPeer::ASOCIADO_RS, false);
		$c->add(CursoPeer::ACTIVO,true);

		return CursoPeer::doSelectJoinAll($c);
	}

	/**
	 * Obtiene todos los cursos activos
	 * @return array Arreglo con los cursos activos
	 */
	static public function listaCursosActivos()
	{
		$c = new Criteria();
		$c->add(CursoPeer::ACTIVO,true);
		$c->add(CursoPeer::PUBLICADO, true);
		$c->addAscendingOrderByColumn(CursoPeer::NOMBRE);

		return CursoPeer::doSelect($c);
	}
	/**
	 * Devuelve la lista de cursos activos, para un select
	 */
	static public function listaCursosActivosSelect($asociadoMoodle = false)
	{
		$arregloCursos = array();

		foreach (self::listaCursosActivos() as $curso)
		{
			if($asociadoMoodle)
			{
				if($curso->getAsociadoMoodle())
				{
					$arregloCursos[$curso->getId()] = $curso->getNombre();
				}
			}
			else
			{
				$arregloCursos[$curso->getId()] = $curso->getNombre();
			}
		}

		return $arregloCursos;
	}
	/*
	 * PARTE DE SINCRONIZACION DE CURSOS
	*/

	/**
	 * Bind de sincronia de curso creado
	 * @param sfEvent $ev
	 */
	static public function syncCursoCreadoBind(sfEvent $ev)
	{
		$curso = $ev['curso'];
		self::cursoASync($curso,'curso.creado');
		error_log(" Nuevo curso a sincronizar. :".$curso);
	}

	/**
	 * Bind de sincronia de curso modificado
	 * @param sfEvent $ev
	 */
	static public function syncCursoActualizadoBind(sfEvent $ev)
	{
		$curso = $ev['curso'];
		self::cursoASync($curso,'curso.actualizado');
		error_log(" Curso modificado a sincronizar. :".$curso);
	}

	/**
	 * Función para sincronizar un curso
	 * @param Producto $curso
	 * @param strig $operacion
	 */
	static public function cursoASync(Curso $curso, $operacion)
	{
		//Convertimos sus atributos a arreglo
		$cA = array();
		$cA['Curso'] =  $curso->toArray();
		//$cA['CategoriaCurso'] = $curso->getCategoriaCurso()->toArray();
		//Extraemos sus objetos relacionados

		//$cA['CartegoriaCurso'][] = $curso->getCategoriaCurso()->toArray();

		//Relacion RestriccionesHorario
		foreach ($curso->getRestriccionesHorarios() as $restriccionHorario)
		{
			$cA['RestriccionesHorario'][] =  $restriccionHorario->toArray();
		}

		foreach ($curso->getRestriccionesSocios() as $restriccionSocio)
		{
			$cA['RestriccionesSocio'][] =  $restriccionSocio->toArray();
		}

		//Convertimos el arreglo a estructura YAML
		$yml =  sfYaml::dump($cA);

		//Creamos el registro de sincronia:
		$sync = new Sincronia();

		//Seteamos la operacion a sincronizar
		$sync->setOperacion($operacion);

		//Seteamos la estructura yaml
		$sync->setDetalle($yml);

		//seteamos la pk del objeto a sincronizar
		$sync->setPkAsync($curso->getPrimaryKey());

		//Seteamos la clase principal
		$sync->setClase($curso->getPeer()->getOMClass(false));

		//Seteamos el tipo de sync. boradcast trata de sincronizar en todos los centros.
		$sync->setTipo('broadcast');
		//Guardamos
		$sync->save();

		$c = new Criteria();
		$c->add(CentroPeer::ACTIVO,true);
		$c->add(CentroPeer::ID,9999,Criteria::LESS_THAN);
		//$c->add(CentroPeer::ID,1);
		$aC = CentroPeer::doSelect($c);
		foreach($aC as $centro)
		{
			$sc = new SincroniaCentro();
			$sc->setCentro($centro);
			$sc->setSincronia($sync);
			$sc->save();
		}
	}

	/**
	 * Carga los datos que se enviaron mediante sync desde centrales
	 * @param string $datos La cadena YAML que contiene los datos a cargar.
	 */
	static public function cargaSync($datos,$operacion,$clase,$pk)
	{
		//Array que contendra los datos parseados
		$a = array();
		$a = sfYaml::load($datos);
		//El array debe traer la estructura completa de relaciones a cargar, por lo que seleccinamos
		//mediante la llave la relacion que se carga del arreglo de datos.

		if($operacion == 'curso.creado')
		{
			//Si la operacion es un insert hacemos una nueva instancia del objeto e intenamos insertarlo
			error_log("Entra en registrar");
			try
			{
				foreach ($a as $clase => $attrs)
				{
					//Si los elementos de este array a su vez son arrays quiere decir que esta clase es una relacion muchos a 1.
					//Por lo que insertamos uno a uno esos elementos.
					if(isset($attrs[0]) && is_array($attrs[0]))
					{
						foreach($attrs as $regs)
						{
							error_log($clase);
							$nobj = new $clase();
							$nobj->fromArray($regs);
							$nobj->save();
						}
					}
					else
					{
						//los elementos de este array no son arrays, lo que quiere decir que esta es la clase padre
						$nobj = new $clase();
						$nobj->fromArray($attrs);
						$nobj->save();
					}
				}
				return true;
			}
			catch (Exception $e)
			{
				error_log("Desde CursoPeer::cargaSync". $e->getMessage());
				return $e->getMessage();
			}
		}
		else if($operacion == 'curso.actualizado')
		{
			//Realizamos una instancia vacia para obtener el peer
			$obj = new $clase();

			//Aqui cargamos las relaciones que vienen como llaves foraneas asociadas al objeto a sincronizar.
			$aRel = array();
			try
			{
				//Clase actual existente en el servidor.
				$oa = $obj->getPeer()->retrieveByPK($pk);

			}
			catch(Exception $e)
			{
				error_log("No ha sido posible encontrar el objeto desde CursoPeer");

				return $e->getMessage();
			}

			try
			{
				foreach ($a as $clase => $attrs)
				{
					//Si los elementos de este array a su vez son arrays quiere decir que esta clase es una relacion muchos a 1.
					//Por lo que borramos todos e insertamos uno a uno esos elementos (identico a como lo hace symfony).
					if(isset($attrs[0]) && is_array($attrs[0]))
					{
						error_log("Aqui andamos. en las relaciones");

						//Aqui viene el problema... necesitamos saber que borrar del sistema local y que actualizar
						//de las relaciones 1 a muchos de nuestro objeto.

						//Para eso comparamos los arrays d los objetos locales relacionados con los que vienen del sync
						$geter = "get".$clase."s";
						$relacionados = $oa->$geter();
						foreach($relacionados as $r)
						{
							error_log("Borrando relaciones. ".serialize($r->getPrimaryKey()));
							$r->delete();
						}
						//Creamos los nuevos
						foreach($attrs as $regs)
						{
							$nobj = new $clase();
							$nobj->fromArray($regs);
							$nobj->save();

						}
					}
					else
					{
						$oa->fromArray($attrs);
						$oa->save();
					}
				}
			}
			catch (Exception $e)
			{
				error_log("Error: ".$e->getMessage());
			}
			//Supongo que aca se gurdara el curso
			//$obj = new $clase()->getPeer()
		}

	}


} // CursoPeer
