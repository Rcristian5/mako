<?php


/**
 * Skeleton subclass for representing a row from the 'socio_pagos' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.4.0 on:
 *
 * Thu Mar 11 01:37:55 2010
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    lib.model
 */
class SocioPagos extends BaseSocioPagos {

	public function __toString()
	{
		return $this->getProductoId()."|".$this->getSubProductoDe()."|".$this->getNumeroPago();
	}

	public function save(PropelPDO $con = null)
	{
		//Si nuestro objeto es nuevo, generamos su identificador único.
		if($this->isNew())
		{
			$this->setId(Comun::generaId());
		}
		parent::save();
	}

} // SocioPagos
