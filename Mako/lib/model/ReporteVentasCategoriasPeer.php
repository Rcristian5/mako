<?php


/**
 * Skeleton subclass for performing query and update operations on the 'reporte_ventas_categorias' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.4.0 on:
 *
 * Thu Aug  4 00:21:50 2011
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    lib.model
 */
class ReporteVentasCategoriasPeer extends BaseReporteVentasCategoriasPeer {

	/**
	 *
	 * Devuelve un arreglo con los datos de la tabla entre las fecha_ini y fecha_fin para un centro_id dado
	 * @param string $fecha_ini
	 * @param string $fecha_fin
	 * @param int $centro_id
	 * @return array
	 */
	public static function arregloReporte($fecha_ini, $fecha_fin, $centro_id)
	{
		$fecha_ini = pg_escape_string($fecha_ini);
		$fecha_fin = pg_escape_string($fecha_fin);

		$c = new Criteria();
		$c->add(self::FECHA,$fecha_ini,Criteria::GREATER_EQUAL);
		$c->addAnd(self::FECHA,$fecha_fin,Criteria::LESS_EQUAL);
		$c->add(self::CENTRO_ID,$centro_id);

		$reportesMetas = self::doSelect($c);
		$aRepo = array();

		foreach ($reportesMetas as $reporteMetas)
		{
			$aRepo[] = $reporteMetas->toArray();
		}
		return $aRepo;
	}

} // ReporteVentasCategoriasPeer
