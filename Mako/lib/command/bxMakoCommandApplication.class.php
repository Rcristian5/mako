<?php

/*
 * This file is part of the symfony package.
* (c) 2004-2006 Fabien Potencier <fabien.potencier@symfony-project.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

/**
 * bxMakoCommandApplication manages the mako CLI.
 *
 * @package    mako
 * @subpackage command
 * @author     Fabien Potencier <fabien.potencier@symfony-project.com>
 * @version    SVN: $Id: bxMakoCommandApplication.class.php,v 1.2 2010/10/03 08:55:47 eorozco Exp $
 */
class bxMakoCommandApplication extends sfCommandApplication
{
	protected $taskFiles = array();


	/**
	 * Sobre escritura del Constructor.
	 *
	 * @param sfEventDispatcher $dispatcher   A sfEventDispatcher instance
	 * @param sfFormatter       $formatter    A sfFormatter instance
	 * @param array             $options      An array of options
	 */
	public function __construct(sfEventDispatcher $dispatcher, sfFormatter $formatter = null, $options = array())
	{
		$this->dispatcher = $dispatcher;
		$this->formatter = null === $formatter ? $this->guessBestFormatter(STDOUT) : $formatter;
		$this->options = $options;

		$this->fixCgi();

		$argumentSet = new sfCommandArgumentSet(array(
				new sfCommandArgument('task', sfCommandArgument::REQUIRED, 'La tarea a ejecutar'),
		));
		$optionSet = new sfCommandOptionSet(array(
				new sfCommandOption('ayuda',    '', sfCommandOption::PARAMETER_NONE, 'Muestra la ayuda de una tarea.')
		));
		$this->commandManager = new sfCommandManager($argumentSet, $optionSet);
		$this->configure();
		$this->registerTasks();
	}

	/**
	 * Sobre escritura de la funcion de ayuda
	 */
	public function help()
	{
		$messages = array(
				$this->formatter->format('Uso:', 'COMMENT'),
				sprintf("  %s [opciones] soporte:nombre_tarea [argumentos]\n", $this->getName()),
				$this->formatter->format('Opciones:', 'COMMENT'),
		);

		foreach ($this->commandManager->getOptionSet()->getOptions() as $option)
		{
			$messages[] = sprintf('  %-24s %s  %s',
					$this->formatter->format($option->getName(), 'INFO'),
					$option->getShortcut() ? $this->formatter->format('-'.$option->getShortcut(), 'INFO') : '  ',
					$option->getHelp()
			);
		}

		$this->dispatcher->notify(new sfEvent($this, 'command.log', $messages));
	}


	/**
	 * Configures the current symfony command application.
	 */
	public function configure()
	{
		if (!isset($this->options['symfony_lib_dir']))
		{
			throw new sfInitializationException('You must pass a "symfony_lib_dir" option.');
		}

		$configurationFile = getcwd().'/config/ProjectConfiguration.class.php';
		if (is_readable($configurationFile))
		{
			require_once $configurationFile;
			$configuration = new ProjectConfiguration(getcwd(), $this->dispatcher);
		}
		else
		{
			$configuration = new sfProjectConfiguration(getcwd(), $this->dispatcher);
		}

		// application
		$this->setName('mako');
		$this->setVersion("2.0");

		$this->loadTasks($configuration);
	}

	/**
	 * Runs the current application.
	 *
	 * @param mixed $options The command line options
	 *
	 * @return integer 0 if everything went fine, or an error code
	 */
	public function run($options = null)
	{
		$this->handleOptions($options);
		$arguments = $this->commandManager->getArgumentValues();

		if (!isset($arguments['task']))
		{
			$arguments['task'] = 'lista';
			$this->commandOptions .= $arguments['task'];
		}

		$this->currentTask = $this->getTaskToExecute($arguments['task']);

		if ($this->currentTask instanceof sfCommandApplicationTask)
		{
			$this->currentTask->setCommandApplication($this);
		}

		$ret = $this->currentTask->runFromCLI($this->commandManager, $this->commandOptions);

		$this->currentTask = null;

		return $ret;
	}

	/**
	 * Loads all available tasks.
	 *
	 * Looks for tasks in the symfony core, the current project and all project plugins.
	 *
	 * @param sfProjectConfiguration $configuration The project configuration
	 */
	public function loadTasks(sfProjectConfiguration $configuration)
	{
		// Symfony core tasks
		//$dirs = array(sfConfig::get('sf_symfony_lib_dir').'/task');


		// project tasks
		$dirs[] = sfConfig::get('sf_lib_dir').'/task';

		$finder = sfFinder::type('file')->name('*Task.class.php');
		foreach ($finder->in($dirs) as $file)
		{
			$this->taskFiles[basename($file, '.class.php')] = $file;
		}

		// register local autoloader for tasks
		spl_autoload_register(array($this, 'autoloadTask'));

		// require tasks
		foreach ($this->taskFiles as $task => $file)
		{
			// forces autoloading of each task class
			class_exists($task, true);
		}

		// unregister local autoloader
		spl_autoload_unregister(array($this, 'autoloadTask'));
	}

	/**
	 * Autoloads a task class
	 *
	 * @param  string  $class  The task class name
	 *
	 * @return Boolean
	 */
	public function autoloadTask($class)
	{
		if (isset($this->taskFiles[$class]))
		{
			require_once $this->taskFiles[$class];

			return true;
		}

		return false;
	}

	/**
	 * @see sfCommandApplication
	 */
	public function getLongVersion()
	{
		return sprintf('%s version %s (%s)', $this->getName(), $this->formatter->format($this->getVersion(), 'INFO'), sfConfig::get('sf_symfony_lib_dir'))."\n";
	}
}
