<?php
/**
 * Clase de métodos estáticos para realizar operaciones diversas
 * @author edgar
 * @version: $Id: Comun.php,v 1.9 2010/11/11 08:29:09 eorozco Exp $
 */
class Comun
{

    /**
     * Genera un entero de 8bits unico irrepetible pra usar como llave primaria en entidades que lo ameriten
     * @return bigint
     */
    public static function generaId(){
        usleep(1000);
        list($useg, $seg) = explode(" ", microtime());
        $museg = sprintf("%05s",substr($useg,2,5));
        $cseg = $seg.$museg;
        return $cseg;
    }

    /**
     * Convierte cadenas con guiones bajos: mi_atributo_nombre a MiAtributoNombre
     * Para usarlo de conversiones de parámetros posts a geters y seters
     * @param $cad
     * @return string
     */
    public static function udscACamel($cad)
    {
        $arp = split("_",$cad);
        $r="";
        foreach($arp as $parte)
        {
            $r .= ucfirst($parte);
        }
        return $r;
    }

    /**
     * IP real del usuario, aunque este detras de un proxy.
     */
    public static function ipReal(){
        $ip = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
        return $ip;
    }

    /**
     * Genera cadena random para clave
     * @return string password
     */
    public static function generaClave(){
        //global $patterns;
        //global $replacements;

        //palabras incomodas
        $patterns= array();
        $replacements= array();

        $patterns[0] = '/ano/';
        $replacements[0] = 'anx';
        $patterns[1] = '/berga/';
        $replacements[1] = 'berxy';
        $patterns[2] = '/verga/';
        $replacements[2] = 'verxy';
        $patterns[3] = '/cola/';
        $replacements[3] = 'coxy';
        $patterns[4] = '/culo/';
        $replacements[4] = 'cuxy';
        $patterns[5] = '/pedo/';
        $replacements[5] = 'pexy';
        $patterns[6] = '/peda/';
        $replacements[6] = 'pexy';
        $patterns[7] = '/puto/';
        $replacements[7] = 'puxy';
        $patterns[8] = '/puta/';
        $replacements[8] = 'puxy';
        $patterns[9] = '/pinche/';
        $replacements[9] = 'pinxy';
        $patterns[10] = '/pendejo/';
        $replacements[10] = 'penxyz';
        $patterns[11] = '/pendeja/';
        $replacements[11] = 'penxyz';

        $length = 8;
        $chars = "0123456789"; //Sólo passwds numericos predefinidos, a petición de Mois.
        #$chars = "23456789abcdefghijkmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ";
        $i = 0;
        $password = "";
        while ($i <= $length)
        {
            $max = strlen(preg_replace($patterns, $replacements,str_replace(' ', '', $chars)));
            $mtr = mt_rand(0,$max-1);
            $password .= $chars{$mtr};
            $i++;
        }
        return $password;

    }

    /**
     * Convierte de segundos a HH:mm
     * @param integer $sec
     * @param boolean $padHours
     * @return string
     */
    static public function seg2hm($sec, $padHours = true)
    {
        $hms = "";
        $hours = intval(intval($sec) / 3600);
        $hms .= ($padHours)
        ? str_pad($hours, 2, "0", STR_PAD_LEFT). ':'
        : $hours. ':';
        $minutes = intval(($sec / 60) % 60);

        $seconds = intval($sec % 60);
        if($seconds > 30) $minutes=$minutes+1;
        $hms .= str_pad($minutes, 2, "0", STR_PAD_LEFT);
        return $hms;
    }

    /**
     * Calcula las próximas fechas de pago.
     * @param string $fecha_inicial
     * @param string $temporalidad
     * @param integer $cantidad
     * @param integer $num_pago
     * @return string
     */
    static public function calculaFechaPago($fecha_inicial,$temporalidad,$cantidad,$num_pago)
    {
        list($a,$m,$d) = split("-",$fecha_inicial);

        $dd = 0; //Diferencial dias
        $dm = 0; //Diferencial meses
        $da = 0; //Diferencial años

        if($temporalidad == 'dia')
        {
            $dd = $cantidad*$num_pago;
        }
        if($temporalidad == 'dias')
        {
            $dd = $cantidad*$num_pago;
        }
        if($temporalidad == 'mes')
        {
            $dm = $cantidad*$num_pago;
        }
        if($temporalidad == 'meses')
        {
            $dm = $cantidad*$num_pago;
        }
        if($temporalidad == 'anio')
        {
            $da = $cantidad*$num_pago;
        }
        if($temporalidad == 'anios')
        {
            $da = $cantidad*$num_pago;
        }
        //error_log("$m + $dm, $d + $dd, $a + $da");
        return date("Y-m-d",mktime(0,0,0, $m + $dm, $d + $dd, $a + $da));
    }

    /**
     * Regresa un arreglo de fechas desde fecha inicio hasta fecha fin con intervalos dados por la temporalidad, puede ser dias, semanas,
     * quincenas, meses, bimestres, trimestres, semestres o anios.
     * @param string $fecha_inicial
     * @param string $fecha_final
     * @param string $temporalidad
     * @return array Arreglo con las fechas que caben entre fecha_inicial y fecha_final dada la temporalidad.
     */
    static public function arregloIntervaloFechas($fecha_inicial,$fecha_final,$temporalidad)
    {
        list($ai,$mi,$di) = split("-",$fecha_inicial);
        list($af,$mf,$df) = split("-",$fecha_final);
        $arfa = array();
        $arfa[]=$fecha_inicial;

        while(mktime(0,0,0, $mi, $di, $ai) < mktime(0,0,0, $mf, $df, $af))
        {
            $nf = self::calculaIntervaloFecha(date("Y-m-d",mktime(0,0,0, $mi, $di, $ai)),$temporalidad);
            list($ai,$mi,$di) = split("-",$nf);
            $arfa[] = $nf;
        }
        return $arfa;
    }

    /**
     * Calcula la siguiente fecha dada una fecha inicial y un intervalo que puede ser dias, semanas, quincenas, etc
     * @param string $fecha_inicial
     * @param string $temporalidad
     */
    static public function calculaIntervaloFecha($fecha_inicial,$temporalidad)
    {
        list($a,$m,$d) = split("-",$fecha_inicial);

        $dd = 0; //Diferencial dias
        $dm = 0; //Diferencial meses
        $da = 0; //Diferencial años

        if($temporalidad == 'dias')
        {
            $dd = 1;
        }
        if($temporalidad == 'semanas')
        {
            $dd = 7;
        }
        if($temporalidad == 'quincenas')
        {
            $dd = 14;
        }
        if($temporalidad == 'meses')
        {
            $dm = 1;
        }
        if($temporalidad == 'bimestres')
        {
            $dm = 2;
        }
        if($temporalidad == 'trimestres')
        {
            $dm = 3;
        }
        if($temporalidad == 'semestres')
        {
            $dm = 6;
        }
        if($temporalidad == 'anios')
        {
            $da = 1;
        }
        return date("Y-m-d",mktime(0,0,0, $m + $dm, $d + $dd, $a + $da));
    }




    /**
     * Regresa la edad del socio
     * @return string edad
     */
    public function getEdad($birthday){
        //$birthday = $this->getFechaNac();

        list($year,$month,$day) = explode("-",$birthday);
        $year_diff  = date("Y") - $year;
        $month_diff = date("m") - $month;
        $day_diff   = date("d") - $day;
        if ($month_diff < 0) $year_diff--;
        elseif (($month_diff==0) && ($day_diff < 0)) $year_diff--;
        return $year_diff;
    }


    /**
     * Regresa forma textual de una cantidad de segundos a horas, minutos y segundos, segun sea el caso.
     *
     * @param integer $segs
     * @return string
     */
    public static function segsATxt($segs)
    {

        if($segs == '') return 0;

        $min = 60;
        $hr = 3600;
        $dy = 3600*24;

        /*if($segs > $dy)
         {
        $mds = $segs % $dy;
        $ds =  floor($segs/$dy);

        //echo "M: $mds D: $ds";
        if($mds > 0)
        {
        return $ds . " días ".self::segsATxt($mds);
        }
        else
        {
        return $ds. " días ";
        }
        }
        else
            */
        if($segs >= $hr )
        {
            $mds = $segs % $hr;
            $ds =  floor($segs/$hr);
            if($mds > 0)
            {
                return $ds . " horas ".self::segsATxt($mds);
            }
            else
            {
                return $ds." horas ";
            }
        }
        elseif($segs > $min && $segs < $hr)
        {
            $mds = $segs % $min;
            $ds =  floor($segs/$min);
            if($mds > 0)
            {
                return $ds . " minutos ".self::segsATxt($mds);
            }
            else
            {
                return $ds. " minutos ";
            }
        }
        else
        {
            return $segs. " segundos";
        }
    }

    /**
     * Filtra los acentos y caracteres especiales
     *
     * @param string $cadena
     */
    public static function filtraAcentos($cadena)
    {
        $sust = array(
                "/á/",
                "/é/",
                "/í/",
                "/ó/",
                "/ú/",
                "/Á/",
                "/É/",
                "/Í/",
                "/Ó/",
                "/Ú/"
        );
        $por = array(
                'a',
                'e',
                'i',
                'o',
                'u',
                'A',
                'E',
                'I',
                'O',
                'U'
        );

        return preg_replace($sust, $por, $cadena);

    }

    /**
     * **************************************************************************************************************   *
     * Manejo de fechas para encontrar el lunes anterior inmediato a una fecha determinada.                         *
     * @param $date fecha a partir de donde se requiere buscar el lunes de la semana.   Formato Y-m-d                   *
     * @return $date Fecha que representa el dia lunes de la semana.                                                    *
     * @author silvio.bravo@enova.mx 11-Marzo-2014                                                                  *
     * @copyright Enova                                                                                             *
     * @category Reingenieria Mako                                                                                  *
     * **************************************************************************************************************   *
     */
    public static function getLunesDeSemana($fecha){
        $fecha      = new DateTime($fecha);
        $diaSemana  = $fecha->format("N");
        $intervalo  =sprintf("P%dD",((1-$diaSemana)*-1));
        $fecha->sub(new DateInterval($intervalo));
        return $fecha->format("Y-m-d");

    }

    /**
     * ************************************************************************************************************************
     * Serializamos un array de objetos a array, al parecer esta version de propel no permite hacerlo de forma automatica :(    *
     * @param $objectArray array de Obbetos                                                                                 *
     * @return array de objetos serializado                                                                                 *
     * @author silvio.bravo@enova.mx 11-Marzo-2014                                                                          *
     * @copyright Enova                                                                                                     *
     * @category Reingenieria Mako                                                                                          *
     **************************************************************************************************************************
     **/

    public static function serializeObjectArray($objectArray){

        foreach($objectArray as $key=>$object){
            $objectArray[$key]  =$object->toArray();
        }
        return ($objectArray);

    }


    /**
     * * *********************************************************************************************************************
     * Convertimos una fecha en formato yyyy-mm-dd <hh:mm:ss> en formato dia nombre mes año <hora> separadas en array cada una.
     * @param string $fecha                                                                                                 *
     * @return array string                                                                                                 *
     * @author silvio.bravo@enova.mx 11-Marzo-2014                                                                          *
     * @copyright Enova                                                                                                     *
     * @category Reingenieria Mako                                                                                          *
     * * **********************************************************************************************************************
     */
    public static function getFechaAndHoraIntoArray($fecha){

        list($fecha, $hora)     = explode(" ",$fecha);
        list($anio,$mes,$dia)   = explode("-",$fecha);
        $stringFecha            = "{$dia} " . self::nombreMes($mes) . " {$anio}";

        return(array("fecha"=>$stringFecha,"hora"=>$hora));

    }

    private function nombreMes($mes){
        $mes    =intval($mes);
        $meses  = array(
                1=>'Enero'      , 2=>'Febrero',
                3=>'Marzo'      , 4=>'Abril',
                5=>'Mayo'       , 6=>'Junio',
                7=>'Julio'      , 8=>'Agosto',
                9=>'Septiembre' , 10=>'Octubre',
                11=>'Noviembre' , 12=>'Diciembre',
        );
        if(key_exists($mes, $meses))
            return($meses[$mes]);
        return "";

    }


    private function nombreDia($dia){
        $dia    =intval($dia);
        $dias   = array(
                1=>'Lunes'          , 2=>'Martes',
                3=>'Miercoles'      , 4=>'Jueves',
                5=>'Viernes'        , 6=>'Sabado',
                7=>'Domingo'
        );
        if(key_exists($dia, $dias))
            return($dias[$dia]);
        return "";

    }

    /**
     * * **************************************************************************************************************************
     * Comparamos los keys de 1 array con otro y retornamos true en caso de que si esten todos los keys o false e caso de que       *
     * false algun key                                                                                                          *
     * @param array $arrayOrigen                                                                                                    *
     * @param array $arrayDestino                                                                                                   *
     * @return bool                                                                                                             *
     * @author silvio.bravo@enova.mx 20-Marzo-2014 Reingen                                                                      *
     * @copyright Enova                                                                                                         *
     * @category Reingenieria Mako                                                                                              *
     * * **************************************************************************************************************************
     */
    public static function keysArrayExistInArray(array $arrayOrigen, array $arrayDestino){
        $ok=true;
        foreach ($arrayOrigen as $key=>$valor){
            if(!array_key_exists($key,$arrayDestino)){
                $ok=false;
                break;
            }

        }
        return($ok);
    }

    /**
     * * **************************************************************************************************************************
     * Convierte un objeto de tipo stdClas a array                                                                              *
     * @param stdClass $datos                                                                                                       *
     * @return array                                                                                                                *
     * @author silvio.bravo@enova.mx 20-Marzo-2014 Reingen                                                                      *
     * @copyright Enova                                                                                                         *
     * @category Reingenieria Mako                                                                                              *
     * * **************************************************************************************************************************
     */
    public static function stdClassToArray(stdClass $datos){
        $vt=array();

        foreach ($datos as $key=>$valor){
            $vt[$key]=$datos->$key;
        }
        return $vt;

    }


    public static function validateCP($cp){
        $patron         ='/^[0-9]([0-9]{3})[0-9]$/';
        $coincidencia   =array();
        preg_match($patron, $cp, $coincidencia);
        if(count($coincidencia)<=0)
            return false;
        return true;
    }


    /**
     *
     * Validamos que una fecha sea valida la fecha esperada debe llegar en formato yyyy-mm-dd
     * @param string $fecha formato YYYY-MM-DD
     * @return boolean
     * @author silvio.bravo
     * @copyright ENOVA
     * @category Reingenieria Mako
     *
     */
    public static function validaFecha($fecha){
        list($anio,$mes,$dia)=explode("-",$fecha);
        return checkdate($mes, $dia, $anio);
    }

    /**
     * Funcion para imprimir la variable dada, a manera de que sea facil de leer
     * y ayude al desarrollo
     *
     * @param  mixed $dbg La variable a "debugear"
     *
     * @return string      Un output "bonito" de la variable debuggeada
     */
    public static function dbg($dbg, $return = 'die') {
        $type   = gettype($dbg);
        $from   = debug_backtrace();
        $extra  = '';

        $from = $from[0]['file'].'@'.$from[0]['line'].' '.
                (isset($from[1]['class'])    ? $from[1]['class'].   '.' : '').
                (isset($from[1]['function']) ? $from[1]['function'].'()' : '');

        switch ($type) {
            case 'boolean':
                $dbg = $dbg === true ? 'true' : 'false';
                break;

            case 'array':
                $extra = count($dbg);
                break;

            case 'string':
                $extra = strlen($dbg);
                break;
        }

        $break = "\n".'=================================================='."\n";

        if(sfConfig::get('sf_environment') == 'prod') {
            $return = 'log';
        }

        if(php_sapi_name() == 'cli') {
            if(($return != 'cli') or ($return != 'cli-die')) {
                $return = 'cli-die';
            }
        }

        switch ($return === true ? 'plain' : $return) {
            case 'die':
                die('<pre class="debug">'.
                    $from.
                    $break.
                    $type.' '.$extra.
                    $break.
                    print_r($dbg, true).
                    $break.
                '</pre>');
                break;

            case 'cli':
                echo "\n".$from.
                     $break.
                     $type.' '.$extra.
                     $break.
                     print_r($dbg, true).
                     $break;

            case 'cli-die':
                die("\n".$from.
                    $break.
                    $type.' '.$extra.
                    $break.
                    print_r($dbg, true).
                    $break);

            case 'pre':
                echo '<pre class="debug">'.
                        $from.
                        $break.
                        $type.' '.$extra.
                        $break.
                        print_r($dbg, true).
                        $break.
                     '</pre>';
                break;

            case 'plain':
                return print_r($dbg, true);

            case 'log':
                error_log($break);
                error_log($from);
                error_log($break);
                error_log($type.' '.$extra);
                error_log($break);
                error_log(str_replace("\n", '    ', print_r($dbg, true)));
                error_log($break);

            default:
                echo $from.' '.$type.' - '.$extra.' - '.print_r($dbg);
                break;
        }
    }

    /**
     * Limita el largo de una cadena de texto y agrega un marcador para mostrar
     * que la cadena se corto.
     *
     * @param  string  $cadena   La cadena a revisar
     * @param  integer $limite   El limite que no puede pasar el largo de la cadena de texto
     * @param  string  $marcador El marcador que se agregara para mostrar que la cadena se corto
     *
     * @return string            La cadena cortada
     */
    public static function limitar_cadena($cadena, $limite = 100, $marcador = '...') {
        if($limite < strlen($marcador)){
            $marcador = '';
        }

        return (strlen($cadena) > intval($limite)) ? substr($cadena, 0, intval($limite) - strlen($marcador)).$marcador : $cadena;
    }

    /**
     * La fecha con el formato de timestamp (YYYY-MM-DD HH:MM:SS)
     * @param  int    $time el tiempo
     * @return string       La fecha con el formato
     */
    public static function fecha_timestamp($time) {
        return date('Y-m-d H:i:s', isset($time) ? $time : time());
    }

    public static function notificacion($mensaje, $titulo = '', $a_quien = array()) {
        if(count($a_quien) == 0) {
            $a_quien[] = 'fs@enova.mx';
        }

        //Registrar la notificacion en logs
        $log = '/var/log/Mako/notificaciones.log';
        error_log('['.$titulo.'] '.$mensaje, 3, $log);

        //Mandar la notificacion por los medios configurados (email, telegram, twitter, etc...)
    }


    /**************************************************
    **
    **  Funcion: EsJson
    **  Valida que una cadena sea un objeto JSON valido
    **  Author: Luis González
    **  Enova 8 - Junio - 2015
    ***************************************************/
   public static function EsJson($cadena) {
        $arreglo = json_decode($cadena, true);
        return (!empty($cadena) && is_string($cadena)) || (is_array($arreglo) && !empty($arreglo)) || json_last_error() == 0;
    }


    /**************************************************
    **
    **  Funcion: EsBigInt
    **  Valida que un valor sea un BigInt Valido
    **  Author: Luis González
    **  Enova 9 - Junio - 2015
    ***************************************************/

    public static function EsBigInt($valor) {
        $valor = trim($valor);
        if ( ctype_digit($valor) ) {
            return $valor;
        }
        $valor = preg_replace("/[^0-9](.*)$/", '', $valor);
        if ( ctype_digit($valor) ) {
            return $valor;
        }
        return 0;
    }

}