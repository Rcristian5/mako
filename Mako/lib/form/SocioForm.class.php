<?php

/**
 * Socio form.
 *
 * @package    mako
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: SocioForm.class.php,v 1.26 2011/01/06 00:14:02 marcela Exp $
 */
class SocioForm extends BaseSocioForm
{

	/**
	 * Se usa para saber si se hace un cambio de nombre de usuario, se hace true cuando hay un cambio en el nombre o apepat
	 * @var boolean
	 */
	private $cambioNombre = false;

	/**
	 * Aqui almacenamos el objeto Socio antes del cambio de nombre de usuario.
	 * @var Socio
	 */
	private $SocioAnterior = null;

	public function configure()
	{

		//$this->embedForm('parentesco_id', new ParentescoSocioForm($this->getOption('parentesco_id')));

		unset(
				$this['created_at'], $this['updated_at'],
				$this['saldo'] , $this['vigente'] ,
				$this['operador'] , $this['activo'],
				$this['centro_id'] , $this['operador_id'],
				$this['observaciones'], $this['rol_id'],
				$this['usuario'],
				$this['ldap_uid'],
				$this['tipo_sesion_id']
		);

		if($this->isNew()){
			unset($this['clave']);
		}

		/*
		 * Datos personales
		*/
		//amplia rango fecha de nacimiento

		# Fecha: 24-06-2013. Inicio.
		# Sugerencia/Acciones: Según matriz 4M RegSocios ()
		# ID Seguimiento: 19
		# Responsable: ES. Se cambia el número de años
		$years = range(date('Y')-100, date('Y')-4); //Creates array of years between actual-80 and actual year (menos 1)
		# Fecha: 24-06-2013. Fin.

		$years_list = array_combine($years, $years); //Creates new array where key and value are both values from $years list
		//$this->setWidget('fecha_nac', new sfWidgetFormDate($options = array('format' => '%day%/%month%/%year%','years' => $years_list,'months'=>array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'))));
		$this->setWidget('fecha_nac', new sfWidgetFormDate($options = array('format' => '%day%/%month%/%year%','years' => $years_list)));

		//sexo
		# Fecha: 24-06-2013. Inicio.
		# Sugerencia/Acciones: Según matriz 4M RegSocios ()
		# ID Seguimiento: NA
		# Responsable: ES. Se agrega una opción por default en blanco
		# MR: 2013-07-17 13:38
		# Eliminación de números de secuencia en la lista
		$this->setWidget('sexo', new sfWidgetFormSelectRadio($options = array('choices' => array('F' => 'Femenino', 'M' => 'Masculino'))));
		# Fecha: 24-06-2013. Fin.

		//huella
		$this->setWidget('huella',new sfWidgetFormInputHidden());
		//foto
		$this->setWidget('foto',new sfWidgetFormInputHidden());
		//estado
		$this->widgetSchema['id_estado']->setOption('default', sfConfig::get('app_registro_estado_id'));
		$this->widgetSchema['id_estado']->addOption('add_empty',false);

		/*
		 * Escolaridad
		*/
		//estudia
		$this->setWidget('estudia', new sfWidgetFormSelectRadio($options = array('choices' => array('true' => 'Si', 'false' => 'No'))));
		$this->widgetSchema['estudia']->setOption('default', -1);
		//nivel - Secundaria
		//Se agrega el criteria para el combo de Nivel de estudios en curso - ES
		$nivel_criteria = new Criteria();
		$nivel_criteria->add(NivelEstudioPeer::ACTIVO,true);
		//$this->widgetSchema['nivel_id']->setOption('default',sfConfig::get('app_registro_nivel'));
		$this->widgetSchema['nivel_id']->setOption('criteria', $nivel_criteria);
		$this->widgetSchema['nivel_id']->setOption('order_by',array('Id','asc'));
		$this->widgetSchema['nivel_id']->addOption('add_empty',false);
		$this->widgetSchema['nivel_id']->setOption('default', -1);

		//Trabaja radio button Si / No
		$this->setWidget('trabaja', new sfWidgetFormSelectRadio($options = array('choices' => array('true' => 'Si', 'false' => 'No'))));

	 /*************************************************
	  **      @@ LGL
		**		Fecha: 2013-07-09
		**		Sugerencia / Acciones : Cambiar lista de opciones del campo Profesión actual según resultado de la investigación. (ID Actividad #7)
	 **      Según matriz 4M Fase II
	 **      Id Actividad: 28
		**		Responsable: LG
	 **			Se quitan de la lista PROFESION ACTUAL todos los REGISTROS NO ACTIVOS en la tabla PROFESION
	 */
		//Creamos CRITERIA
		$profesion_actual_criteria = new Criteria();
		$profesion_actual_criteria->add(ProfesionPeer::ACTIVO,true);

		$grado_id_criteria = new Criteria();
		$grado_id_criteria->add(GradoEstudioPeer::ACTIVO, true);

		//Agregamos WIDGET para manipular campo PROFESION_ACTUAL
		$this->widgetSchema['profesion_actual_id']->setOption('criteria', $profesion_actual_criteria);
		$this->widgetSchema['profesion_ultimo_id']->setOption('criteria', $profesion_actual_criteria);
		$this->widgetSchema['grado_id']           ->setOption('criteria', $grado_id_criteria);

	 /*************************************************
	  **      @@ LGL
		**		Fecha: 2013-07-09
		**		Sugerencia / Acciones : Quitar las opciones “Diplomado” y “Certificación” de la lista Otros estudios,
		**								dejar las opciones que resulten de reunión con áreas de negocio.
	 **      Según matriz 4M Fase II
	 **      Id Actividad: 29
		**		Responsable: LG
	 **			Se quitan de la lista OTROS_ESTUDIOS todos los REGISTROS NO ACTIVOS en la tabla ESTUDIO
	 */
		//Creamos CRITERIA
		$estudio_criteria = new Criteria();
		$estudio_criteria->add(EstudioPeer::ACTIVO,true);
		//Agregamos WIDGET para manipular campo OTROS_ESTUDIOS
		$this->widgetSchema['estudio_actual_id']->setOption('criteria',$estudio_criteria);
		$this->widgetSchema['estudio_ultimo_id']->setOption('criteria',$estudio_criteria);

		//tipo de escuela - Publica
		$this->widgetSchema['tipo_escuela_id']->setOption('default',0);
		 $this->widgetSchema['tipo_escuela_id']->addOption('add_empty',false); 
		//tipo de estudios - Presencial
		$this->widgetSchema['tipo_estudio_id']->setOption('default',0);
		 $this->widgetSchema['tipo_estudio_id']->addOption('add_empty',false); 
		//beca
		$this->setWidget('beca', new sfWidgetFormInputCheckbox());
		//
		/***********************************************************************/
		/* Fecha: 2013-07-18 17:10
		 Actividad: Cambiar opciones de la lista Sector
		ID Actividad: M4F2-35
		Responsable: MR. Filtro para mostrar sólo elementos activos.
		/**********************************************************************/
		$sector_socioeconomico_criteria = new Criteria();
		$sector_socioeconomico_criteria->add(SectorPeer::ACTIVO,true);
		$this->widgetSchema['sector_id']->setOption('criteria',$sector_socioeconomico_criteria);
		$this->widgetSchema['sector_id']->setOption('default',0);
		$this->widgetSchema['sector_id']->addOption('add_empty',false); 

	 /************************************************
	  **      @@ LGL     2013-07-09
	 **      Se quitan de la lista todos los REGISTROS no activos en la tabla OCUPACIÓN
	 **      Según matriz 4M Fase II
	 **      Id Actividad: 37
	 */
		//Ocupacion - Ama de casa

		$ocupacion_criteria = new Criteria();
		$ocupacion_criteria->add(OcupacionPeer::ACTIVO,true);

		//$this->widgetSchema['ocupacion_id']->setOption('criteria',$ocupacion_criteria);
		//$this->widgetSchema['ocupacion_id']->setOption('default', 0);
		//$this->widgetSchema['ocupacion_id']->addOption('add_empty',false);
	 /************************************************
	  **      @@ LGL     2014-10-09
	 **      Se modifica Widget para que sea CHECKBOX MULTIPLE 
	 **      Es decir, el socio puede tener más de una ocupación
	 **      Id Actividad: 
	 */
		/*
		 * Discapacidad
	 */

		$ocupaciones = OcupacionPeer::doSelect($ocupacion_criteria);
		//error_log(("OCUPACIONES_SQL = " . $ocupacion_criteria->toString());
		$this->setWidget('socio_ocupacion' ,new sfWidgetFormPropelChoice(array('model' => 'Ocupacion', 'add_empty' => False, 'criteria' => $ocupacion_criteria)));
		$this->getWidget('socio_ocupacion')->setOption('expanded',true);
		$this->getWidget('socio_ocupacion')->setOption('multiple',true);
		$this->setValidator('socio_ocupacion', new sfValidatorPass());


		if ($socio_id = $this->getObject()->getId())
		{
			$flt_ocupa = new Criteria();
			$flt_ocupa->add(RelOcupacionSocioPeer::SOCIO_ID,$socio_id);
                        $ocupacion_socio = RelOcupacionSocioPeer::doSelect($flt_ocupa);
                        $ocupaciones_socio = (array) null;
                        foreach($ocupacion_socio as $ocupa)
                        {
                                $ocupaciones_socio[] = $ocupa->getOcupacionId();
                        }
                        $this->getWidget('socio_ocupacion')->setOption('default',$ocupaciones_socio);
		}
		

		/*
		 * Medio de acercamiento
		*/
		//como se enteró de la RIA? - Volante
		$nivel_criteria = new Criteria();
		$nivel_criteria->add(CanalContactoPeer::ACTIVO,true);
		$this->widgetSchema['canal_contacto_id']->setOption('criteria', $nivel_criteria);
		$this->widgetSchema['canal_contacto_id']->setOption('order_by',array('Id','asc'));
		$this->widgetSchema['canal_contacto_id']->setOption('default', 0);
		$this->widgetSchema['canal_contacto_id']->addOption('add_empty',false);
		//promotor

		$this->setWidget('promotor_ref_id',new sfWidgetFormPropelJQueryAutocompleter(
				array(
						'model' => 'Usuario',
						'url'   => $this->getOption('url'),
						'config' => '{mustMatch:true}'
				)
		));

		//Socio que recomienda
		$this->setWidget('socio_ref_id',new sfWidgetFormInputHidden());


		//Campos para la georeferencia con el gmaps
		$this->setWidget('lat',new sfWidgetFormInputHidden());
		$this->setWidget('long',new sfWidgetFormInputHidden());
		$this->setWidget('dirgmaps',new sfWidgetFormInputHidden());

		//Triggers de eventos de campos para reflejar la direccion en el mapa
		$this->widgetSchema['id_estado']->setAttributes(array('onblur' => 'dirAMapa();'));
		$this->widgetSchema['municipio']->setAttributes(array('onblur' => 'dirAMapa();'));
		$this->widgetSchema['colonia']->setAttributes(array('onblur' => 'dirAMapa();'));
		$this->widgetSchema['cp']->setAttributes(array('onblur' => 'dirAMapa();'));
		$this->widgetSchema['calle']->setAttributes(array('onblur' => 'dirAMapa();'));
		$this->widgetSchema['num_ext']->setAttributes(array('onblur' => 'dirAMapa();'));

		/***
		 *
	 *  @Description: Agregamos los criteria para cargar los combos de municipio y colonia vacios
	 *  @Author: silvio.bravo@enova.mx
	 *  @Date: 25 Feb 2014
	 *
	 *
	 ***/

	 //Criteria para Municipios
		$municipioCriteria	= new Criteria();
		$municipioCriteria->add(NM01MunicipioPeer::EDO_ID,sfConfig::get('app_registro_estado_id'));
		$this->widgetSchema['id_municipio']->setOption('criteria', $municipioCriteria);
		$this->widgetSchema['id_municipio']->setOption('order_by',array('McpioNombre','asc'));
	 //Criteria para Coloniaas :)

		$coloniaCriteria	= new Criteria();
		$coloniaCriteria->add(NM01ColoniaPeer::CLNA_ID,0);
		$this->widgetSchema['id_colonia']->setOption('criteria', $coloniaCriteria);
		$this->widgetSchema['id_colonia']->setOption('order_by',array('ClnaNombre','asc'));
		$this->widgetSchema['tr_clna'] = new sfWidgetFormInputCheckbox();
		$this->setValidator('tr_clna', new sfValidatorPass());



	 /************************************************
	  **      @@ LGL     2014-09-26
	 **      Se modifica Widget para que sea CHECKBOX MULTIPLE 
	 **      Es decir, el socio puede tener más de una discapacidad
	 **      Id Actividad: 
	 */
		/*
		 * Habilidades
	 */
		//discapacidad - Sin discapacidad

		$criteria_discapacidad = new Criteria();
		$criteria_discapacidad->add(DiscapacidadPeer::ACTIVO,true);
		$discapacidades = DiscapacidadPeer::doSelect($criteria_discapacidad);
		//error_log("DISCAPACIDADES_SQL = " . $criteria_discapacidad->toString());
		$this->setWidget('socio_discapacidad' ,new sfWidgetFormPropelChoice(array('model' => 'Discapacidad', 'add_empty' => False, 'criteria' => $criteria_discapacidad)));
		$this->getWidget('socio_discapacidad')->setOption('expanded',true);
		$this->getWidget('socio_discapacidad')->setOption('multiple',true);
		$this->setValidator('socio_discapacidad', new sfValidatorPass());


		if ($socio_id = $this->getObject()->getId())
		{
			$flt_disc = new Criteria();
			$flt_disc->add(RelDiscapacidadSocioPeer::SOCIO_ID,$socio_id);
                        $discapacidad_socio = RelDiscapacidadSocioPeer::doSelect($flt_disc);
                        $discapacidades_socio = (array) null;
                        foreach($discapacidad_socio as $disc)
                        {
                                $discapacidades_socio[] = $disc->getDiscapacidadId();
                        }
                        $this->getWidget('socio_discapacidad')->setOption('default',$discapacidades_socio);

		}
		
		//Hablilidad informatica - Principiante
		/***********************************************************************/
		/* Fecha: 2013-07-18 17:43
		 Actividad: Modificar opciones de la lista Habilidad informática y Dominio ingles
		ID Actividad: M4F2-35
		Responsable: MR. Filtro para mostrar sólo elementos activos.
		/**********************************************************************/
		$habilidad_informatica_id_criteria = new Criteria();
		$habilidad_informatica_id_criteria->add(HabilidadInformaticaPeer::ACTIVO,true);
		$habilidad_informatica_id_criteria->addAscendingOrderByColumn(HabilidadInformaticaPeer::ID);
		$this->widgetSchema['habilidad_informatica_id']->setOption('criteria',$habilidad_informatica_id_criteria);
		$this->widgetSchema['habilidad_informatica_id']->setOption('default', 0);
		$this->widgetSchema['habilidad_informatica_id']->addOption('add_empty',false);

		//Dominio ingles - Principiante
		$dominio_ingles_id_criteria = new Criteria();
		$dominio_ingles_id_criteria->add(DominioInglesPeer::ACTIVO,true);
		$this->widgetSchema['dominio_ingles_id']->setOption('criteria',$dominio_ingles_id_criteria);
		//$this->widgetSchema['dominio_ingles_id']->setOption('default',sfConfig::get('app_registro_dominio_ingles'));
		$this->widgetSchema['dominio_ingles_id']->addOption('order_by',array('Id', 'asc'));
		$this->widgetSchema['dominio_ingles_id']->setOption('default',0);
		$this->widgetSchema['dominio_ingles_id']->addOption('add_empty',false);
		//


		/*
		 * NSE
	 */
		//Estado civil - Soltero
        	$estados_civiles_activos = new Criteria();
        	$estados_civiles_activos->add(EstadoCivilPeer::ACTIVO,true);
        	$this->widgetSchema['estado_civil_id']->setOption('criteria', $estados_civiles_activos);
        	$this->widgetSchema['estado_civil_id']->addOption('order_by',array('Id', 'asc'));
		$this->widgetSchema['estado_civil_id']->addOption('add_empty',false);
		$this->widgetSchema['estado_civil_id']->setOption('default',0);
		//Servicio salud - Ninguno
		$this->widgetSchema['servicio_salud_id']->setOption('default', 0);
		$this->widgetSchema['servicio_salud_id']->addOption('order_by',array('Nombre', 'asc'));
		//Tipo de vivienda - Casa independiente 
        $vivienda_criteria = new Criteria();
		$vivienda_criteria->add(ViviendaTipoPeer::ACTIVO,true);
		$this->widgetSchema['vivienda_tipo_id']->setOption('criteria', $vivienda_criteria);
		$this->widgetSchema['vivienda_tipo_id']->setOption('order_by',array('Id','asc'));
		$this->widgetSchema['vivienda_tipo_id']->setOption('default', 0);
		//La vivienda es - Rentada
        $vivienda_es = new Criteria();
		$vivienda_es->add(ViviendaEsPeer::ACTIVO,true);
		$this->widgetSchema['vivienda_es_id']->setOption('criteria', $vivienda_es);
		$this->widgetSchema['vivienda_es_id']->setOption('order_by',array('Id','asc'));
		$this->widgetSchema['vivienda_es_id']->setOption('default', 0);

		//$this->widgetSchema['vivienda_es_id']->setOption('default',sfConfig::get('app_registro_vivienda_es'));
		//Material del techo - Concreto
		$this->widgetSchema['material_techo_id']->setOption('default', 0);
		//Material del piso - Cemento
		$this->widgetSchema['material_piso_id']->setOption('default', 0);
		//Nivel de estudio de Jefe de Hogar
		
		
		//Opciones para el uso de automovil
		$this->setWidget('automovil_propio', new sfWidgetFormSelectRadio($options = array('choices' => array('true' => 'Si', 'false' => 'No'))));
		//Uso que se le da al automovil - Familiar
		$this->widgetSchema['uso_automovil_id']->setOption('default', 0);
		//Madre
		$this->setWidget('socio_madre_id',new sfWidgetFormInputHidden());
		//Padre
		$this->setWidget('socio_padre_id',new sfWidgetFormInputHidden());


		//Validators
		$this->validatorSchema['email'] = new sfValidatorAnd(array(
				$this->validatorSchema['email'],
				new sfValidatorEmail(array(),array('invalid'=>'La direción de correo es inválida, por favor verifica el dato.'))),
				array('required'=>false)
		);

		$this->validatorSchema['email'] = new sfValidatorAnd(array(
				$this->validatorSchema['email'],
				new sfValidatorRegex(array('pattern'=>"/[áéíóúÁÉÍÓÚñÑ]/",'must_match'=>false),array('invalid'=>'La direción de correo no puede llevar letra Ñ o acentos.'))),
				array('required'=>false)
		);


		$this->validatorSchema['fecha_nac'] = new sfValidatorDate(array(), array('invalid'=>'La fecha no es válida, por favor verifica el dato.'));

		$this->validatorSchema['curp'] = new sfValidatorString(array('min_length'=>18,'max_length'=>18,'required'=>false),array('min_length'=>'La CURP debe contener 18 caracteres, por favor revise el dato.','max_length'=>'La CURP debe contener 18 caracteres, por favor revise el dato.'));

		// Password
		if(!$this->isNew()){
			$this->widgetSchema['clave'] = new sfWidgetFormInputPassword();
			$this->widgetSchema['clave_confirmacion']  = new sfWidgetFormInputPassword();

			$this->validatorSchema['clave_confirmacion'] = clone $this->validatorSchema['clave'];
			$this->validatorSchema['clave']->setOption('required', true);

			$this->mergePostValidator(new sfValidatorSchemaCompare('clave',
					sfValidatorSchemaCompare::EQUAL, 'clave_confirmacion',
					array(),
					array('invalid' => 'Las constraseñas deben coincidir.')));

		 $this->validatorSchema['clave'] = new sfValidatorString (array(
		 		'min_length'  => 8,
		 		'max_length' => 30
		 ),
		 		array(
		 				'min_length' => 'La contraseña es demasiado corta (8 carácteres mínimo)'));

		}




		//Club Idea
		//sexo contacto emergencia
 		//$this->setWidget('estudia', new sfWidgetFormSelectRadio($options = array('choices' => array('true' => 'Si', 'false' => 'No'))
		// Acceso a la tecnología
		//$this->setWidget('linea_telefonica', new sfWidgetFormSelectCheckbox());

		//Validators
		//email contacto emergencia
		/*
		 $this->validatorSchema['er_email'] = new sfValidatorAnd(array(
		 		$this->validatorSchema['er_email'],
		 		new sfValidatorEmail(array(),array('invalid'=>'La direción de correo es invalida.'))),
		 		array('required'=>false)
		 );

		$this->setWidget('er_fecha_nac', new sfWidgetFormDate($options = array('format' => '%day%/%month%/%year%','years' => $years_list)));

		//empresa
		$this->setWidget('empresa',new sfWidgetFormPropelJQueryAutocompleter(
				array(
						'model' => 'Empresa',
						'url'   => $this->getOption('url'),
						'config' => '{mustMatch:true}'
				)
		));

		*/

		#Inicio
		#Se agrega esta línea - Agregar el formato de comentario correcto
		#Agregar el ID de seguimiento
		//$this->setDefault('estudia', $this->getOption('default'));
		#Fin
	}


	/**
	 * Hacemos override del metodo doUpdateObject para saber si hubo cambio de usuario o no
	 * @param $values
	 */
	public function doUpdateObject($values)
	{

		if(!$this->isNew())
		{
			$this->SocioAnterior = $this->getObject()->copy();
		}

		parent::doUpdateObject($values);

		if(!$this->isNew())
		{
			if(in_array(SocioPeer::NOMBRE,$this->getObject()->getModifiedColumns()) || in_array(SocioPeer::APEPAT,$this->getObject()->getModifiedColumns()))
			{
				$this->cambioNombre = true;

				$nombre = $this->getObject()->getNombre();
				$apepat = $this->getObject()->getApepat();
				$this->getObject()->setUsuario($this->getObject()->generaUsuario($nombre, $apepat));

			}
			else
			{
				$this->SocioAnterior = null;
			}
		}


	}

	/**
	 * Getter de la propiedad de cambio de nombre
	 * @return boolean true si cambio el nombre o apepat o false de lo contrario
	 */
	public function cambioNombreUsuario()
	{
		return $this->cambioNombre;
	}

	/**
	 * Regresa el objeto Socio anterior al cambio de nombre
	 * @return Socio
	 */
	public function getSocioAnterior()
	{
		return $this->SocioAnterior;
	}

}
