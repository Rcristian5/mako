<?php

/**
 * Expediente form.
 *
 * @package    mako
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: ExpedienteForm.class.php,v 1.5 2010/04/15 15:34:15 david Exp $
 */
class ExpedienteForm extends BaseExpedienteForm
{
	public function configure()
	{
		unset ($this['id'],$this['activo'], $this['created_at'], $this['updated_at']);
		$this->widgetSchema['alumno_id'] = new sfWidgetFormInputHidden();
		$this->widgetSchema['matricula'] = new sfWidgetFormInput();
		$this->widgetSchema['acta_nacimiento'] = new sfWidgetFormInputFile();
		$this->widgetSchema['id_fotografia'] = new sfWidgetFormInputFile();
		$this->widgetSchema['comprobante_domicilio'] = new sfWidgetFormInputFile();
		$this->widgetSchema['curp'] = new sfWidgetFormInputFile();


		$this->validatorSchema['acta_nacimiento'] = new sfValidatorFile(array('required'=>false,'max_size'=>41943040, 'mime_types'=>'web_images'), array('max_size'=>'El tamaño máximo de la imagen es de 5 MB', 'mime_types'=>'Archivo invalido (%mime_type%) Solo se permiten archivos de imagenes'));
		$this->validatorSchema['id_fotografia'] = new sfValidatorFile(array('required'=>false,'max_size'=>41943040, 'mime_types'=>'web_images'), array('max_size'=>'El tamaño máximo de la imagen es de 5 MB', 'mime_types'=>'Archivo invalido (%mime_type%) Solo se permiten archivos de imagenes'));
		$this->validatorSchema['comprobante_domicilio'] = new sfValidatorFile(array('required'=>false,'max_size'=>41943040, 'mime_types'=>'web_images'), array('max_size'=>'El tamaño máximo de la imagen es de 5 MB', 'mime_types'=>'Archivo invalido (%mime_type%) Solo se permiten archivos de imagenes'));
		$this->validatorSchema['curp'] = new sfValidatorFile(array('required'=>false,'max_size'=>41943040, 'mime_types'=>'web_images'), array('max_size'=>'El tamaño máximo de la imagen es de 5 MB', 'mime_types'=>'Archivo invalido (%mime_type%) Solo se permiten archivos de imagenes'));
		$this->validatorSchema['alumno_id'] = new sfValidatorString(array('required' => true));
		$this->validatorSchema['matricula'] = new sfValidatorString(array('required' => false));
	}

}
