<?php

/**
 * Capitulo form.
 *
 * @package    mako
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: CapituloForm.class.php,v 1.3 2010/03/06 22:53:40 david Exp $
 */
class CapituloForm extends BaseCapituloForm
{
	public function configure()
	{
		unset($this['curso_id'],$this['activo'],$this['operador_id'],
				$this['created_at'],$this['updated_at']);
		/*
		 * Widget checkbox para seleccionar todos los cursos
		*/
		$this->widgetSchema['temas'] = new sfWidgetFormInputText();
		$this->validatorSchema['temas'] = new sfValidatorInteger(array('required' => false,'min' => -2147483648, 'max' => 2147483647));
	}
}
