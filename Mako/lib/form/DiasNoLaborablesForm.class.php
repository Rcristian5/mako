<?php

/**
 * DiasNoLaborables form.
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: DiasNoLaborablesForm.class.php,v 1.2 2010/08/30 23:38:29 david Exp $
 */
class DiasNoLaborablesForm extends BaseDiasNoLaborablesForm
{
	public function configure()
	{
		unset($this['id']);
		$this->widgetSchema['dia'] = new sfWidgetFormInputHidden();
		$this->validatorSchema['dia'] = new sfValidatorString();
	}
}
