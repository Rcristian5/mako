<?php

/**
 * RestriccionesHorario form.
 *
 * @package    mako
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: RestriccionesHorarioForm.class.php,v 1.6 2010/09/10 08:04:17 david Exp $
 */
class RestriccionesHorarioForm extends BaseRestriccionesHorarioForm
{
	public function configure()
	{
		$this->useFields(array('hora_inicio', 'hora_fin'));
		$horas = array(''=>'',9=>'09',10=>'10',11=>'11',12=>'12',13=>'13',14=>'14',15=>'15',16=>'16',17=>'17',
				18=>'18',19=>'19',20=>'20');
		$minutos = array(''=>'',0=>'00');
		$this->widgetSchema['hora_inicio'] = new sfWidgetFormTime(array('hours' => $horas,'minutes' => $minutos));
		$this->widgetSchema['hora_fin'] = new sfWidgetFormTime(array('hours' => $horas,'minutes' => $minutos));

	}
}
