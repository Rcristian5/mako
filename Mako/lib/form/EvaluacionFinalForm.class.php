<?php

/**
 * EvaluacionFinal form.
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: EvaluacionFinalForm.class.php,v 1.2 2010/09/12 07:12:21 david Exp $
 */
class EvaluacionFinalForm extends BaseEvaluacionFinalForm
{
	public function configure()
	{
		if($this->getObject()->isNew())
		{
			$this->useFields(array('curso_id', 'nombre','descripcion','calificacion_minima','calificacion_maxima'));
		}
		else
		{
			$this->useFields(array('curso_id', 'nombre','descripcion','calificacion_minima','calificacion_maxima', 'activo'));
		}
		$this->setWidget('curso_id',  new sfWidgetFormInputHidden());
		$this->setValidator('calificacion_minima', new sfValidatorNumber(array('min' => 0.01, 'max' => 10.00, 'required' => false), array('max'=>'La calificación mínima-máxima es 10.00', 'min'=>'La calificación mínima-mínima es 0.01') ));
		$this->setValidator('calificacion_maxima', new sfValidatorNumber(array('min' => 0.01, 'max' => 10.00, 'required' => false), array('max'=>'La calificación máxima-máxima es 10.00', 'min'=>'La calificación máxima-máxima es 0.01') ));
		//Se pasa el id del curso por default
		$this->setDefault('curso_id', $this->getOption('curso'));
	}
}
