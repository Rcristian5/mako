<?php

/**
 * Grupo form.
 *
 * @package    mako
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: GrupoForm.class.php,v 1.3 2010/03/23 17:12:14 david Exp $
 */
class GrupoForm extends BaseGrupoForm
{
	public function configure()
	{
		unset($this['id'], $this['usuario_id'], $this['horario_id'], $this['activo'],
				$this['created_at'], $this['updated_at'], $this['grupo_alumnos_list']);
		$this->widgetSchema['curso_id'] = new sfWidgetFormChoice(array('choices' => CursoCentroPeer::cursosPermitidos()));
		$this->widgetSchema['aula_id'] = new sfWidgetFormChoice(array('choices' => RecursoInfraestructuraPeer::listaRecursosCentro()));
	}
}
