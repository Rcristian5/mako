<?php

/**
 * Evaluacion form.
 *
 * @package    mako
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: EvaluacionForm.class.php,v 1.4 2010/09/09 03:57:58 david Exp $
 */
class EvaluacionForm extends BaseEvaluacionForm
{
	public function configure()
	{
		$this->useFields(array('tema_id', 'nombre','descripcion','calificacion_minima','califiacion_maxima','porcentaje'));
		$this->setWidget('tema_id',  new sfWidgetFormSelect(array('choices' => TemaPeer::listTemas($this->getOption('curso')))));
	}
}
