<?php

/**
 * RestriccionesSocio form.
 *
 * @package    mako
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: CursoRestriccionesCollectionForm.class.php,v 1.1 2010/08/30 23:38:33 david Exp $
 */

class CursoRestriccionesCollectionForm extends sfForm
{
	public function configure()
	{
		if (!$curso = $this->getOption('curso'))
		{
			throw new InvalidArgumentException('Es necesario un objeto de tipo curso');
		}
		$horario = new RestriccionesHorario();
		$socio = new RestriccionesSocio();
		$horario->Curso = $curso;
		$socio->Curso = $curso;

		$formH = new RestriccionesHorarioForm($horario);
		$formS = new RestriccionesSocioForm($socio);

		$this->embedForm('rh', $formH);
		$this->embedForm('rs', $formS);
	}
}