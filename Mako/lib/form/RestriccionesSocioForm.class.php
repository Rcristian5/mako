<?php

/**
 * RestriccionesSocio form.
 *
 * @package    mako
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: RestriccionesSocioForm.class.php,v 1.7 2010/09/12 07:12:21 david Exp $
 */
class RestriccionesSocioForm extends BaseRestriccionesSocioForm
{
	public function configure()
	{
		$this->useFields(array('edad_minima', 'edad_maxima', 'sexo', 'ocupacion_id', 'estudia', 'nivel_estudio_id', 'habilidad_informatica_id', 'dominio_ingles_id'));
		$this->widgetSchema['sexo'] = new sfWidgetFormChoice(array('choices' => array(''=>'','F'=>'Femenino','M'=>'Masculino')));
		$this->setValidator('edad_maxima', new sfValidatorInteger(array('min' => 2, 'max' => 100, 'required'=>false), array('max'=>'La edad máxima es de 100 años', 'min'=>'La edad mínima es de 2 años')));
		$this->setValidator('edad_minima', new sfValidatorInteger(array('min' => 2, 'max' => 100, 'required'=>false), array('max'=>'La edad máxima es de 100 años', 'min'=>'La edad mínima es de 2 años')));
	}
}
