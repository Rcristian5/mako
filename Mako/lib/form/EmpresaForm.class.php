<?php

/**
 * Empresa form.
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: EmpresaForm.class.php,v 1.1 2010/03/04 06:42:33 marcela Exp $
 */
class EmpresaForm extends BaseEmpresaForm
{
	public function configure()
	{
		unset(
				$this['created_at'], $this['updated_at'],
				$this['activo']
		);
	}
}
