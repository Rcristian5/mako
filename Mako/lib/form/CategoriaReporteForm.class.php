<?php

/**
 * CategoriaReporte form.
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: CategoriaReporteForm.class.php,v 1.1 2010/12/30 08:14:04 eorozco Exp $
 */
class CategoriaReporteForm extends BaseCategoriaReporteForm
{
	public function configure()
	{
		unset(
				$this['created_at'],
				$this['updated_at']
		);

		$this->widgetSchema['categoria_reporte_producto_list']->setOption('label','Productos en esta categoría');
		 
		//Mostramos en el select, unicamente los productos activos.
		$c = new Criteria();
		$c->add(ProductoPeer::ACTIVO,true);
		$c->add(ProductoPeer::CATEGORIA_ID,2);
		$c->addAscendingOrderByColumn(ProductoPeer::NOMBRE);
		 
		$this->widgetSchema['categoria_reporte_producto_list']->setOption(
				'renderer_class',
				'sfWidgetFormSelectDoubleList'
		);

		$this->widgetSchema['categoria_reporte_producto_list']->setOption('criteria',$c);

		$this->widgetSchema['categoria_reporte_producto_list']->setOption(
				'renderer_options',
				array(
						'label_associated' => 'Productos seleccionados',
						'label_unassociated' => 'Lista de Productos'
				)

		);
	}
}
