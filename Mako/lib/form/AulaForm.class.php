<?php

/**
 * Aula form.
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: AulaForm.class.php,v 1.2 2010/09/12 07:12:08 david Exp $
 */
class AulaForm extends BaseAulaForm
{
	public function configure()
	{
		$this->useFields(array('centro_id', 'nombre', 'descripcion', 'capacidad'));

		$this->setWidget('centro_id',  new sfWidgetFormSelect(array('choices' => CentroPeer::listaCentros())));
	}
}
