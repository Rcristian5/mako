<?php

/**
 * Bitacora form.
 *
 * @package    mako
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: BitacoraForm.class.php,v 1.2 2010/04/09 12:03:39 david Exp $
 */
class BitacoraForm extends BaseBitacoraForm
{
	public function configure()
	{
		$this->useFields(array('comentario'));
	}
}
