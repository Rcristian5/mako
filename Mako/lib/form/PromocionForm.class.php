<?php

/**
 * Promocion form.
 *
 * @package    mako
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: PromocionForm.class.php,v 1.4 2010/09/12 05:00:36 eorozco Exp $
 */
class PromocionForm extends BasePromocionForm
{
	public function configure()
	{
		unset(
				$this['created_at'],
				$this['updated_at'],
				$this['promocion_categoria_list']
		);

		// Reingenieria Mako C. ID 35. Responsable:  FE. Fecha: 23-01-2014.  Descripción del cambio: Se valido campos de fecha inicial y final.
		$this->widgetSchema['vigente_de'] = new sfWidgetFormInputText();
		$this->widgetSchema['vigente_de']->setOption('label','Vigente desde');
		$this->widgetSchema['vigente_de']->setAttributes(array('requerido' => 1,'readonly'=>'1'));

		$this->widgetSchema['vigente_a'] = new sfWidgetFormInputText();
		$this->widgetSchema['vigente_a']->setOption('label','Vigente hasta');
		$this->widgetSchema['vigente_a']->setAttributes(array('requerido' => 1,'readonly'=>'1'));
		// Fecha: 23-01-2014 - Fin..

		// Reingenieria Mako C. ID 50,51,52. Responsable:  FE. Fecha: 07-04-2014.  Descripción del cambio: Se agrega filtro del tipo texto
		$this->widgetSchema['nombre']->setAttributes(array('requerido' => 1, 'nouppercase'=>1,'filtro' => 'texto','maxlength'=>'75'));
		//$this->widgetSchema['descripcion']->setAttributes(array('requerido' => 1));
		$this->widgetSchema['descripcion']->setOption('label','Descripción de la promoción');
		$this->widgetSchema['descripcion']->setAttributes(array('requerido' => 1,'rows'=>'4', 'cols'=>'50','filtro' => 'texto','maxlength'=>'500'));
		// Fecha: 07-04-2014 Fin

		$this->widgetSchema['descuento']->setOption('label','Descuento (%)');
		$this->widgetSchema['descuento']->setAttributes(array('requerido' => 1, 'size'=>"10", 'maxlength'=>3, 'filtro' => 'numerico'));

		$this->widgetSchema['promocion_producto_list']->setOption('label','Productos en esta promoción');
		$this->widgetSchema['promocion_producto_list']->setAttributes(array('requerido' => 1));

		//$this->widgetSchema['descripcion']->setOption('label','Descripción de la promoción');
		//$this->widgetSchema['descripcion']->setAttributes(array('requerido' => 1,'rows'=>'4', 'cols'=>'50','filtro' => 'texto','maxlength'=>'500'));

		$this->widgetSchema['promocion_producto_list']->setOption(
				'renderer_class',
				'sfWidgetFormSelectDoubleList'
		);
		$this->widgetSchema['promocion_producto_list']->setOption(
				'renderer_options',
				array(
						'label_associated' => 'Productos seleccionados',
						'label_unassociated' => 'Lista de Productos'
				)
		);
		$this->widgetSchema['promocion_producto_list']->setOption('order_by',array('Nombre','asc'));
		/*Reingeniería MAKO Administrar descuento | Responsable:  FE. - Fecha: 14-01-2014. - ID: 36. - Campo: Pantalla “Generar nueva promoción”.
		 Descripción del cambio: Se modifico query para mostrar únicamente productos activos.*/
		//Mostramos en el select, unicamente los productos activos.
		$c = new Criteria();
		$c->add(ProductoPeer::ACTIVO,true);
		//$c->addAscendingOrderByColumn(ProductoPeer::NOMBRE);

		$this->widgetSchema['promocion_producto_list']->setOption('criteria',$c);
		/*Fecha: 14-01-2014. Fin.*/
	}
}
