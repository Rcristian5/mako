<?php

/**
 * Seccion form.
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: SeccionForm.class.php,v 1.3 2011/01/13 03:56:25 eorozco Exp $
 */
class SeccionForm extends BaseSeccionForm
{
	public function configure()
	{
		 
		unset(
				$this['created_at'],
				$this['updated_at']
		);

		$this->widgetSchema['centro_id']->setAttributes(array('requerido' => 1));
		$this->widgetSchema['nombre']->setAttributes(array('requerido' => 1,'filtro'=>'alfanumerico'));
		$this->widgetSchema['en_dashboard']->setOption('label','Visible en dashboard de ocupación');
		 
		$c = new Criteria();
		$centro_id = sfConfig::get('app_centro_actual_id');
		if($centro_id != sfConfig::get('app_central_id'))
		{
			$c->add(CentroPeer::ID,$centro_id);
		}
		$this->widgetSchema['centro_id']->setOption('criteria',$c);
		$this->widgetSchema['centro_id']->setOption('add_empty',false);
		 
	}
}
