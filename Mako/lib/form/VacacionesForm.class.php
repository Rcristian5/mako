<?php

/**
 * Vacaciones form.
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: VacacionesForm.class.php,v 1.2 2010/09/03 18:05:44 david Exp $
 */
class VacacionesForm extends BaseVacacionesForm
{
	public function configure()
	{
		$this->useFields(array('usuario_id', 'fecha_inicio', 'fecha_fin'));
		$this->widgetSchema['usuario_id']->setOption('renderer_class', 'sfWidgetFormPropelJQueryAutocompleter');
		$this->widgetSchema['usuario_id']->setOption('renderer_options', array(
				'model' => 'Usuario',
				'url'   => $this->getOption('url'),
		));
		$this->setWidget('fecha_inicio', new sfWidgetFormInput());
		$this->setWidget('fecha_fin', new sfWidgetFormInput());
	}
}
