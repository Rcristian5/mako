<?php

/**
 * Conciliacion form.
 *
 * @package    mako
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: ConciliacionForm.class.php,v 1.5 2011/02/04 04:16:04 marcela Exp $
 */
class ConciliacionForm extends BaseConciliacionForm
{
	public function configure()
	{
		unset(
				$this['created_at'],
				$this['folio'],
				$this['operador_id'],
				$this['orden_id']
		);
		 
		//Si es central, necesitamos mostrar una lista con las cajas del centro y su ip.
		if(sfConfig::get('app_centro_actual_id') == sfConfig::get('app_central_id'))
		{

			//$cajas = array_merge(array('0'=>'--Seleccione una caja.'),CentroPeer::getListaCajas($c));
			 
			$this->widgetSchema['ip_caja'] = new sfWidgetFormPropelDependentSelect(array(
					'model' => 'Computadora',
					'depends' => 'Centro',
					'key_method' => 'getIp',
					'method' => 'getAlias',
					'peer_method' => 'cajasCentro',
					'add_empty' => '-- Seleccione la caja a conciliar.'

					//,'ajax' => true
			));

			$this->widgetSchema->moveField('ip_caja', 'after', 'centro_id');
			$this->widgetSchema['ip_caja']->setOption('label','Caja');
			$this->widgetSchema['ip_caja']->setAttributes(array('requerido' => 1));
			$this->widgetSchema['centro_id']->setOption('add_empty','-- Seleccione el centro a conciliar');
			$this->widgetSchema['centro_id']->setAttributes(array('requerido' => 1));
			$this->widgetSchema['centro_id']->setOption(
					'order_by',array('Nombre','asc'),
					'method', 'getLabel'
			);
		}
		else
		{
			unset(
					$this['ip_caja']
					,$this['centro_id']
			);

		}




		$this->widgetSchema['monto']->setAttributes(array('requerido' => 1,'filtro'=>'numerico','size'=>'6','maxlenght'=>'6'));
		$this->widgetSchema['folio_docto_aval']->setOption('label','Folio referencia');
		 
	}
}
