<?php

/**
 * Tema form.
 *
 * @package    mako
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: TemaForm.class.php,v 1.6 2010/09/09 03:57:58 david Exp $
 */
class TemaForm extends BaseTemaForm
{
	//protected $id = 1;
	public function configure()
	{
		$this->useFields(array('curso_id','nombre','numero','descripcion'));
		$this->setWidget('curso_id', new sfWidgetFormInputHidden());

		//Se pasa el id del curso por default
		$this->setDefault('curso_id', $this->getOption('curso'));
	}
}
