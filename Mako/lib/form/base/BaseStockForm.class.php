<?php

/**
 * Stock form base class.
 *
 * @method Stock getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseStockForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'centro_id'   => new sfWidgetFormInputHidden(),
				'producto_id' => new sfWidgetFormInputHidden(),
				'existencia'  => new sfWidgetFormInputText(),
				'activo'      => new sfWidgetFormInputCheckbox(),
				'created_at'  => new sfWidgetFormDateTime(),
				'updated_at'  => new sfWidgetFormDateTime(),
		));

		$this->setValidators(array(
				'centro_id'   => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id', 'required' => false)),
				'producto_id' => new sfValidatorPropelChoice(array('model' => 'Producto', 'column' => 'id', 'required' => false)),
				'existencia'  => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
				'activo'      => new sfValidatorBoolean(array('required' => false)),
				'created_at'  => new sfValidatorDateTime(array('required' => false)),
				'updated_at'  => new sfValidatorDateTime(array('required' => false)),
		));

		$this->widgetSchema->setNameFormat('stock[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Stock';
	}


}
