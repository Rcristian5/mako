<?php

/**
 * InventarioCorte form base class.
 *
 * @method InventarioCorte getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseInventarioCorteForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'           => new sfWidgetFormInputHidden(),
				'operacion_id' => new sfWidgetFormPropelChoice(array('model' => 'InventarioOperacion', 'add_empty' => true)),
				'estado'       => new sfWidgetFormInputCheckbox(),
				'centro_id'    => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => false)),
				'created_at'   => new sfWidgetFormDateTime(),
				'updated_at'   => new sfWidgetFormDateTime(),
		));

		$this->setValidators(array(
				'id'           => new sfValidatorPropelChoice(array('model' => 'InventarioCorte', 'column' => 'id', 'required' => false)),
				'operacion_id' => new sfValidatorPropelChoice(array('model' => 'InventarioOperacion', 'column' => 'id', 'required' => false)),
				'estado'       => new sfValidatorBoolean(array('required' => false)),
				'centro_id'    => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id')),
				'created_at'   => new sfValidatorDateTime(array('required' => false)),
				'updated_at'   => new sfValidatorDateTime(array('required' => false)),
		));

		$this->widgetSchema->setNameFormat('inventario_corte[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'InventarioCorte';
	}


}
