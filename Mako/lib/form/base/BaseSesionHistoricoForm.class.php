<?php

/**
 * SesionHistorico form base class.
 *
 * @method SesionHistorico getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseSesionHistoricoForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'             => new sfWidgetFormInputHidden(),
				'centro_id'      => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'computadora_id' => new sfWidgetFormPropelChoice(array('model' => 'Computadora', 'add_empty' => true)),
				'ip'             => new sfWidgetFormInputText(),
				'socio_id'       => new sfWidgetFormPropelChoice(array('model' => 'Socio', 'add_empty' => true)),
				'usuario'        => new sfWidgetFormInputText(),
				'fecha_login'    => new sfWidgetFormDateTime(),
				'fecha_logout'   => new sfWidgetFormDateTime(),
				'saldo_inicial'  => new sfWidgetFormInputText(),
				'saldo_final'    => new sfWidgetFormInputText(),
				'tipo_id'        => new sfWidgetFormPropelChoice(array('model' => 'TipoSesion', 'add_empty' => true)),
				'ocupados'       => new sfWidgetFormInputText(),
		));

		$this->setValidators(array(
				'id'             => new sfValidatorPropelChoice(array('model' => 'SesionHistorico', 'column' => 'id', 'required' => false)),
				'centro_id'      => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id', 'required' => false)),
				'computadora_id' => new sfValidatorPropelChoice(array('model' => 'Computadora', 'column' => 'id', 'required' => false)),
				'ip'             => new sfValidatorString(array('max_length' => 15)),
				'socio_id'       => new sfValidatorPropelChoice(array('model' => 'Socio', 'column' => 'id', 'required' => false)),
				'usuario'        => new sfValidatorString(array('max_length' => 90)),
				'fecha_login'    => new sfValidatorDateTime(),
				'fecha_logout'   => new sfValidatorDateTime(),
				'saldo_inicial'  => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
				'saldo_final'    => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
				'tipo_id'        => new sfValidatorPropelChoice(array('model' => 'TipoSesion', 'column' => 'id', 'required' => false)),
				'ocupados'       => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
		));

		$this->widgetSchema->setNameFormat('sesion_historico[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'SesionHistorico';
	}


}
