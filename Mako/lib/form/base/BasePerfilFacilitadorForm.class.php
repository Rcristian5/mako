<?php

/**
 * PerfilFacilitador form base class.
 *
 * @method PerfilFacilitador getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePerfilFacilitadorForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'                  => new sfWidgetFormInputHidden(),
				'nombre'              => new sfWidgetFormInputText(),
				'operador_id'         => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'activo'              => new sfWidgetFormInputCheckbox(),
				'created_at'          => new sfWidgetFormDateTime(),
				'updated_at'          => new sfWidgetFormDateTime(),
				'usuario_perfil_list' => new sfWidgetFormPropelChoice(array('multiple' => true, 'model' => 'Usuario')),
		));

		$this->setValidators(array(
				'id'                  => new sfValidatorPropelChoice(array('model' => 'PerfilFacilitador', 'column' => 'id', 'required' => false)),
				'nombre'              => new sfValidatorString(array('max_length' => 255)),
				'operador_id'         => new sfValidatorPropelChoice(array('model' => 'Usuario', 'column' => 'id', 'required' => false)),
				'activo'              => new sfValidatorBoolean(array('required' => false)),
				'created_at'          => new sfValidatorDateTime(array('required' => false)),
				'updated_at'          => new sfValidatorDateTime(array('required' => false)),
				'usuario_perfil_list' => new sfValidatorPropelChoice(array('multiple' => true, 'model' => 'Usuario', 'required' => false)),
		));

		$this->validatorSchema->setPostValidator(
				new sfValidatorPropelUnique(array('model' => 'PerfilFacilitador', 'column' => array('nombre')))
		);

		$this->widgetSchema->setNameFormat('perfil_facilitador[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'PerfilFacilitador';
	}


	public function updateDefaultsFromObject()
	{
		parent::updateDefaultsFromObject();

		if (isset($this->widgetSchema['usuario_perfil_list']))
		{
			$values = array();
			foreach ($this->object->getUsuarioPerfils() as $obj)
			{
				$values[] = $obj->getUsuarioId();
			}

			$this->setDefault('usuario_perfil_list', $values);
		}

	}

	protected function doSave($con = null)
	{
		parent::doSave($con);

		$this->saveUsuarioPerfilList($con);
	}

	public function saveUsuarioPerfilList($con = null)
	{
		if (!$this->isValid())
		{
			throw $this->getErrorSchema();
		}

		if (!isset($this->widgetSchema['usuario_perfil_list']))
		{
			// somebody has unset this widget
			return;
		}

		if (null === $con)
		{
			$con = $this->getConnection();
		}

		$c = new Criteria();
		$c->add(UsuarioPerfilPeer::PERFIL_ID, $this->object->getPrimaryKey());
		UsuarioPerfilPeer::doDelete($c, $con);

		$values = $this->getValue('usuario_perfil_list');
		if (is_array($values))
		{
			foreach ($values as $value)
			{
				$obj = new UsuarioPerfil();
				$obj->setPerfilId($this->object->getPrimaryKey());
				$obj->setUsuarioId($value);
				$obj->save();
			}
		}
	}

}
