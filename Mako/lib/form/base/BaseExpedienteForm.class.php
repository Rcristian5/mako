<?php

/**
 * Expediente form base class.
 *
 * @method Expediente getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseExpedienteForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'                    => new sfWidgetFormInputHidden(),
				'alumno_id'             => new sfWidgetFormPropelChoice(array('model' => 'Alumnos', 'add_empty' => false, 'key_method' => 'getSocioId')),
				'acta_nacimiento'       => new sfWidgetFormTextarea(),
				'id_fotografia'         => new sfWidgetFormTextarea(),
				'comprobante_domicilio' => new sfWidgetFormTextarea(),
				'curp'                  => new sfWidgetFormTextarea(),
				'activo'                => new sfWidgetFormInputCheckbox(),
				'created_at'            => new sfWidgetFormDateTime(),
				'updated_at'            => new sfWidgetFormDateTime(),
		));

		$this->setValidators(array(
				'id'                    => new sfValidatorPropelChoice(array('model' => 'Expediente', 'column' => 'id', 'required' => false)),
				'alumno_id'             => new sfValidatorPropelChoice(array('model' => 'Alumnos', 'column' => 'socio_id')),
				'acta_nacimiento'       => new sfValidatorString(array('required' => false)),
				'id_fotografia'         => new sfValidatorString(array('required' => false)),
				'comprobante_domicilio' => new sfValidatorString(array('required' => false)),
				'curp'                  => new sfValidatorString(array('required' => false)),
				'activo'                => new sfValidatorBoolean(array('required' => false)),
				'created_at'            => new sfValidatorDateTime(array('required' => false)),
				'updated_at'            => new sfValidatorDateTime(array('required' => false)),
		));

		$this->validatorSchema->setPostValidator(
				new sfValidatorPropelUnique(array('model' => 'Expediente', 'column' => array('alumno_id')))
		);

		$this->widgetSchema->setNameFormat('expediente[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Expediente';
	}


}
