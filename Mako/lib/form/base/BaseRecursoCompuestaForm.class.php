<?php

/**
 * RecursoCompuesta form base class.
 *
 * @method RecursoCompuesta getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseRecursoCompuestaForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'compuesta_id_1' => new sfWidgetFormPropelChoice(array('model' => 'RecursoInfraestructura', 'add_empty' => true)),
				'compuesta_id_2' => new sfWidgetFormPropelChoice(array('model' => 'RecursoInfraestructura', 'add_empty' => true)),
				'id'             => new sfWidgetFormInputHidden(),
		));

		$this->setValidators(array(
				'compuesta_id_1' => new sfValidatorPropelChoice(array('model' => 'RecursoInfraestructura', 'column' => 'id', 'required' => false)),
				'compuesta_id_2' => new sfValidatorPropelChoice(array('model' => 'RecursoInfraestructura', 'column' => 'id', 'required' => false)),
				'id'             => new sfValidatorPropelChoice(array('model' => 'RecursoCompuesta', 'column' => 'id', 'required' => false)),
		));

		$this->widgetSchema->setNameFormat('recurso_compuesta[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'RecursoCompuesta';
	}


}
