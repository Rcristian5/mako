<?php

/**
 * GrupoAlumnos form base class.
 *
 * @method GrupoAlumnos getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseGrupoAlumnosForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'                => new sfWidgetFormInputHidden(),
				'grupo_id'          => new sfWidgetFormPropelChoice(array('model' => 'Grupo', 'add_empty' => false)),
				'alumno_id'         => new sfWidgetFormPropelChoice(array('model' => 'Alumnos', 'add_empty' => false)),
				'preinscrito'       => new sfWidgetFormInputCheckbox(),
				'fecha_preinscrito' => new sfWidgetFormDateTime(),
				'created_at'        => new sfWidgetFormDateTime(),
				'updated_at'        => new sfWidgetFormDateTime(),
		));

		$this->setValidators(array(
				'id'                => new sfValidatorPropelChoice(array('model' => 'GrupoAlumnos', 'column' => 'id', 'required' => false)),
				'grupo_id'          => new sfValidatorPropelChoice(array('model' => 'Grupo', 'column' => 'id')),
				'alumno_id'         => new sfValidatorPropelChoice(array('model' => 'Alumnos', 'column' => 'matricula')),
				'preinscrito'       => new sfValidatorBoolean(array('required' => false)),
				'fecha_preinscrito' => new sfValidatorDateTime(),
				'created_at'        => new sfValidatorDateTime(array('required' => false)),
				'updated_at'        => new sfValidatorDateTime(array('required' => false)),
		));

		$this->widgetSchema->setNameFormat('grupo_alumnos[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'GrupoAlumnos';
	}


}
