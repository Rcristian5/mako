<?php

/**
 * Conciliacion form base class.
 *
 * @method Conciliacion getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseConciliacionForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'               => new sfWidgetFormInputHidden(),
				'centro_id'        => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'operador_id'      => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'monto'            => new sfWidgetFormInputText(),
				'folio_docto_aval' => new sfWidgetFormInputText(),
				'folio'            => new sfWidgetFormInputText(),
				'ip_caja'          => new sfWidgetFormInputText(),
				'observaciones'    => new sfWidgetFormTextarea(),
				'orden_id'         => new sfWidgetFormPropelChoice(array('model' => 'Orden', 'add_empty' => true)),
				'created_at'       => new sfWidgetFormDateTime(),
		));

		$this->setValidators(array(
				'id'               => new sfValidatorPropelChoice(array('model' => 'Conciliacion', 'column' => 'id', 'required' => false)),
				'centro_id'        => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id', 'required' => false)),
				'operador_id'      => new sfValidatorPropelChoice(array('model' => 'Usuario', 'column' => 'id', 'required' => false)),
				'monto'            => new sfValidatorNumber(array('required' => false)),
				'folio_docto_aval' => new sfValidatorString(array('max_length' => 256, 'required' => false)),
				'folio'            => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
				'ip_caja'          => new sfValidatorString(array('max_length' => 15, 'required' => false)),
				'observaciones'    => new sfValidatorString(array('required' => false)),
				'orden_id'         => new sfValidatorPropelChoice(array('model' => 'Orden', 'column' => 'id', 'required' => false)),
				'created_at'       => new sfValidatorDateTime(array('required' => false)),
		));

		$this->widgetSchema->setNameFormat('conciliacion[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Conciliacion';
	}


}
