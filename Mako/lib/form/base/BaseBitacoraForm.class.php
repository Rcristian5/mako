<?php

/**
 * Bitacora form base class.
 *
 * @method Bitacora getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseBitacoraForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'          => new sfWidgetFormInputHidden(),
				'alumno_id'   => new sfWidgetFormInputHidden(),
				'curso_id'    => new sfWidgetFormPropelChoice(array('model' => 'Curso', 'add_empty' => false)),
				'grupo_id'    => new sfWidgetFormPropelChoice(array('model' => 'Grupo', 'add_empty' => false)),
				'comentario'  => new sfWidgetFormTextarea(),
				'operador_id' => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'created_at'  => new sfWidgetFormDateTime(),
				'updated_at'  => new sfWidgetFormDateTime(),
		));

		$this->setValidators(array(
				'id'          => new sfValidatorPropelChoice(array('model' => 'Bitacora', 'column' => 'id', 'required' => false)),
				'alumno_id'   => new sfValidatorPropelChoice(array('model' => 'Alumnos', 'column' => 'socio_id', 'required' => false)),
				'curso_id'    => new sfValidatorPropelChoice(array('model' => 'Curso', 'column' => 'id')),
				'grupo_id'    => new sfValidatorPropelChoice(array('model' => 'Grupo', 'column' => 'id')),
				'comentario'  => new sfValidatorString(),
				'operador_id' => new sfValidatorPropelChoice(array('model' => 'Usuario', 'column' => 'id', 'required' => false)),
				'created_at'  => new sfValidatorDateTime(array('required' => false)),
				'updated_at'  => new sfValidatorDateTime(array('required' => false)),
		));

		$this->widgetSchema->setNameFormat('bitacora[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Bitacora';
	}


}
