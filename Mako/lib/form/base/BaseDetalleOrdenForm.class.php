<?php

/**
 * DetalleOrden form base class.
 *
 * @method DetalleOrden getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseDetalleOrdenForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'           => new sfWidgetFormInputHidden(),
				'orden_id'     => new sfWidgetFormPropelChoice(array('model' => 'Orden', 'add_empty' => true)),
				'centro_id'    => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'socio_id'     => new sfWidgetFormPropelChoice(array('model' => 'Socio', 'add_empty' => true)),
				'producto_id'  => new sfWidgetFormPropelChoice(array('model' => 'Producto', 'add_empty' => true)),
				'precio_lista' => new sfWidgetFormInputText(),
				'tiempo'       => new sfWidgetFormInputText(),
				'cantidad'     => new sfWidgetFormInputText(),
				'descuento'    => new sfWidgetFormInputText(),
				'subtotal'     => new sfWidgetFormInputText(),
				'cupon_id'     => new sfWidgetFormPropelChoice(array('model' => 'Cupon', 'add_empty' => true)),
				'promocion_id' => new sfWidgetFormPropelChoice(array('model' => 'Promocion', 'add_empty' => true)),
				'created_at'   => new sfWidgetFormDateTime(),
				'updated_at'   => new sfWidgetFormDateTime(),
		));

		$this->setValidators(array(
				'id'           => new sfValidatorPropelChoice(array('model' => 'DetalleOrden', 'column' => 'id', 'required' => false)),
				'orden_id'     => new sfValidatorPropelChoice(array('model' => 'Orden', 'column' => 'id', 'required' => false)),
				'centro_id'    => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id', 'required' => false)),
				'socio_id'     => new sfValidatorPropelChoice(array('model' => 'Socio', 'column' => 'id', 'required' => false)),
				'producto_id'  => new sfValidatorPropelChoice(array('model' => 'Producto', 'column' => 'id', 'required' => false)),
				'precio_lista' => new sfValidatorNumber(array('required' => false)),
				'tiempo'       => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
				'cantidad'     => new sfValidatorNumber(array('required' => false)),
				'descuento'    => new sfValidatorNumber(array('required' => false)),
				'subtotal'     => new sfValidatorNumber(array('required' => false)),
				'cupon_id'     => new sfValidatorPropelChoice(array('model' => 'Cupon', 'column' => 'id', 'required' => false)),
				'promocion_id' => new sfValidatorPropelChoice(array('model' => 'Promocion', 'column' => 'id', 'required' => false)),
				'created_at'   => new sfValidatorDateTime(array('required' => false)),
				'updated_at'   => new sfValidatorDateTime(array('required' => false)),
		));

		$this->widgetSchema->setNameFormat('detalle_orden[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'DetalleOrden';
	}


}
