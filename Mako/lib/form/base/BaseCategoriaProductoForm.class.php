<?php

/**
 * CategoriaProducto form base class.
 *
 * @method CategoriaProducto getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCategoriaProductoForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'                       => new sfWidgetFormInputHidden(),
				'nombre'                   => new sfWidgetFormInputText(),
				'activo'                   => new sfWidgetFormInputCheckbox(),
				'created_at'               => new sfWidgetFormDateTime(),
				'updated_at'               => new sfWidgetFormDateTime(),
				'promocion_categoria_list' => new sfWidgetFormPropelChoice(array('multiple' => true, 'model' => 'Promocion')),
		));

		$this->setValidators(array(
				'id'                       => new sfValidatorPropelChoice(array('model' => 'CategoriaProducto', 'column' => 'id', 'required' => false)),
				'nombre'                   => new sfValidatorString(array('max_length' => 255)),
				'activo'                   => new sfValidatorBoolean(array('required' => false)),
				'created_at'               => new sfValidatorDateTime(array('required' => false)),
				'updated_at'               => new sfValidatorDateTime(array('required' => false)),
				'promocion_categoria_list' => new sfValidatorPropelChoice(array('multiple' => true, 'model' => 'Promocion', 'required' => false)),
		));

		$this->validatorSchema->setPostValidator(
				new sfValidatorPropelUnique(array('model' => 'CategoriaProducto', 'column' => array('nombre')))
		);

		$this->widgetSchema->setNameFormat('categoria_producto[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'CategoriaProducto';
	}


	public function updateDefaultsFromObject()
	{
		parent::updateDefaultsFromObject();

		if (isset($this->widgetSchema['promocion_categoria_list']))
		{
			$values = array();
			foreach ($this->object->getPromocionCategorias() as $obj)
			{
				$values[] = $obj->getPromocionId();
			}

			$this->setDefault('promocion_categoria_list', $values);
		}

	}

	protected function doSave($con = null)
	{
		parent::doSave($con);

		$this->savePromocionCategoriaList($con);
	}

	public function savePromocionCategoriaList($con = null)
	{
		if (!$this->isValid())
		{
			throw $this->getErrorSchema();
		}

		if (!isset($this->widgetSchema['promocion_categoria_list']))
		{
			// somebody has unset this widget
			return;
		}

		if (null === $con)
		{
			$con = $this->getConnection();
		}

		$c = new Criteria();
		$c->add(PromocionCategoriaPeer::CATEGORIA_ID, $this->object->getPrimaryKey());
		PromocionCategoriaPeer::doDelete($c, $con);

		$values = $this->getValue('promocion_categoria_list');
		if (is_array($values))
		{
			foreach ($values as $value)
			{
				$obj = new PromocionCategoria();
				$obj->setCategoriaId($this->object->getPrimaryKey());
				$obj->setPromocionId($value);
				$obj->save();
			}
		}
	}

}
