<?php

/**
 * CalificacionParcial form base class.
 *
 * @method CalificacionParcial getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCalificacionParcialForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'            => new sfWidgetFormInputHidden(),
				'curso_id'      => new sfWidgetFormPropelChoice(array('model' => 'Curso', 'add_empty' => false)),
				'evaluacion_id' => new sfWidgetFormPropelChoice(array('model' => 'EvaluacionParcial', 'add_empty' => true)),
				'alumno_id'     => new sfWidgetFormPropelChoice(array('model' => 'Alumnos', 'add_empty' => false)),
				'calificacion'  => new sfWidgetFormInputText(),
				'operador_id'   => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'created_at'    => new sfWidgetFormDateTime(),
				'updated_at'    => new sfWidgetFormDateTime(),
		));

		$this->setValidators(array(
				'id'            => new sfValidatorPropelChoice(array('model' => 'CalificacionParcial', 'column' => 'id', 'required' => false)),
				'curso_id'      => new sfValidatorPropelChoice(array('model' => 'Curso', 'column' => 'id')),
				'evaluacion_id' => new sfValidatorPropelChoice(array('model' => 'EvaluacionParcial', 'column' => 'id', 'required' => false)),
				'alumno_id'     => new sfValidatorPropelChoice(array('model' => 'Alumnos', 'column' => 'matricula')),
				'calificacion'  => new sfValidatorNumber(),
				'operador_id'   => new sfValidatorPropelChoice(array('model' => 'Usuario', 'column' => 'id', 'required' => false)),
				'created_at'    => new sfValidatorDateTime(array('required' => false)),
				'updated_at'    => new sfValidatorDateTime(array('required' => false)),
		));

		$this->widgetSchema->setNameFormat('calificacion_parcial[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'CalificacionParcial';
	}


}
