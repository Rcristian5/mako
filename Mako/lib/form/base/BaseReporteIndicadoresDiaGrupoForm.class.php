<?php

/**
 * ReporteIndicadoresDiaGrupo form base class.
 *
 * @method ReporteIndicadoresDiaGrupo getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseReporteIndicadoresDiaGrupoForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'                           => new sfWidgetFormInputHidden(),
				'centro_id'                    => new sfWidgetFormInputHidden(),
				'grupo_id'                     => new sfWidgetFormPropelChoice(array('model' => 'Grupo', 'add_empty' => false)),
				'curso_id'                     => new sfWidgetFormPropelChoice(array('model' => 'Curso', 'add_empty' => false)),
				'facilitador_id'               => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => false)),
				'categoria_curso_id'           => new sfWidgetFormPropelChoice(array('model' => 'CategoriaCurso', 'add_empty' => false)),
				'aula_id'                      => new sfWidgetFormPropelChoice(array('model' => 'Aula', 'add_empty' => false)),
				'fecha'                        => new sfWidgetFormDate(),
				'semana'                       => new sfWidgetFormInputText(),
				'anio'                         => new sfWidgetFormInputText(),
				'socios_preinscritos'          => new sfWidgetFormInputText(),
				'socios_inscritos'             => new sfWidgetFormInputText(),
				'socios_inscritos_nuevos'      => new sfWidgetFormInputText(),
				'socios_inscritos_recurrentes' => new sfWidgetFormInputText(),
				'alumnos_activos'              => new sfWidgetFormInputText(),
				'alumnos_actividad_programada' => new sfWidgetFormInputText(),
				'alumnos_graduados'            => new sfWidgetFormInputText(),
				'alumnos_reprobados'           => new sfWidgetFormInputText(),
				'graduacion_esperada'          => new sfWidgetFormInputText(),
				'alumnos_desertores'           => new sfWidgetFormInputText(),
				'pagos_programados'            => new sfWidgetFormInputText(),
				'pagos_cobrados'               => new sfWidgetFormInputText(),
				'cobranza_programada'          => new sfWidgetFormInputText(),
				'cobranza_cobrada'             => new sfWidgetFormInputText(),
				'created_at'                   => new sfWidgetFormDateTime(),
				'updated_at'                   => new sfWidgetFormDateTime(),
		));

		$this->setValidators(array(
				'id'                           => new sfValidatorPropelChoice(array('model' => 'ReporteIndicadoresDiaGrupo', 'column' => 'id', 'required' => false)),
				'centro_id'                    => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id', 'required' => false)),
				'grupo_id'                     => new sfValidatorPropelChoice(array('model' => 'Grupo', 'column' => 'id')),
				'curso_id'                     => new sfValidatorPropelChoice(array('model' => 'Curso', 'column' => 'id')),
				'facilitador_id'               => new sfValidatorPropelChoice(array('model' => 'Usuario', 'column' => 'id')),
				'categoria_curso_id'           => new sfValidatorPropelChoice(array('model' => 'CategoriaCurso', 'column' => 'id')),
				'aula_id'                      => new sfValidatorPropelChoice(array('model' => 'Aula', 'column' => 'id')),
				'fecha'                        => new sfValidatorDate(),
				'semana'                       => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'anio'                         => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'socios_preinscritos'          => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'socios_inscritos'             => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'socios_inscritos_nuevos'      => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'socios_inscritos_recurrentes' => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'alumnos_activos'              => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'alumnos_actividad_programada' => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'alumnos_graduados'            => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'alumnos_reprobados'           => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'graduacion_esperada'          => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'alumnos_desertores'           => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'pagos_programados'            => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'pagos_cobrados'               => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'cobranza_programada'          => new sfValidatorNumber(),
				'cobranza_cobrada'             => new sfValidatorNumber(),
				'created_at'                   => new sfValidatorDateTime(array('required' => false)),
				'updated_at'                   => new sfValidatorDateTime(array('required' => false)),
		));

		$this->widgetSchema->setNameFormat('reporte_indicadores_dia_grupo[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'ReporteIndicadoresDiaGrupo';
	}


}
