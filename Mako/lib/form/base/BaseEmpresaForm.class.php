<?php

/**
 * Empresa form base class.
 *
 * @method Empresa getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseEmpresaForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'              => new sfWidgetFormInputHidden(),
				'nombre'          => new sfWidgetFormInputText(),
				'razon_social'    => new sfWidgetFormInputText(),
				'tipo_id'         => new sfWidgetFormPropelChoice(array('model' => 'TipoEmpresa', 'add_empty' => true)),
				'id_estado'       => new sfWidgetFormPropelChoice(array('model' => 'EntidadFederativa', 'add_empty' => true)),
				'estado'          => new sfWidgetFormInputText(),
				'ciudad'          => new sfWidgetFormInputText(),
				'id_del'          => new sfWidgetFormInputText(),
				'municipio'       => new sfWidgetFormInputText(),
				'id_municipio'    => new sfWidgetFormInputText(),
				'colonia'         => new sfWidgetFormInputText(),
				'id_asentamiento' => new sfWidgetFormInputText(),
				'cp'              => new sfWidgetFormInputText(),
				'calle'           => new sfWidgetFormInputText(),
				'num_ext'         => new sfWidgetFormInputText(),
				'num_int'         => new sfWidgetFormInputText(),
				'activo'          => new sfWidgetFormInputCheckbox(),
				'created_at'      => new sfWidgetFormDateTime(),
				'updated_at'      => new sfWidgetFormDateTime(),
		));

		$this->setValidators(array(
				'id'              => new sfValidatorPropelChoice(array('model' => 'Empresa', 'column' => 'id', 'required' => false)),
				'nombre'          => new sfValidatorString(array('max_length' => 255)),
				'razon_social'    => new sfValidatorString(array('max_length' => 120, 'required' => false)),
				'tipo_id'         => new sfValidatorPropelChoice(array('model' => 'TipoEmpresa', 'column' => 'id', 'required' => false)),
				'id_estado'       => new sfValidatorPropelChoice(array('model' => 'EntidadFederativa', 'column' => 'id', 'required' => false)),
				'estado'          => new sfValidatorString(array('max_length' => 30, 'required' => false)),
				'ciudad'          => new sfValidatorString(array('max_length' => 60, 'required' => false)),
				'id_del'          => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
				'municipio'       => new sfValidatorString(array('max_length' => 60, 'required' => false)),
				'id_municipio'    => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
				'colonia'         => new sfValidatorString(array('max_length' => 60, 'required' => false)),
				'id_asentamiento' => new sfValidatorString(array('max_length' => 60, 'required' => false)),
				'cp'              => new sfValidatorString(array('max_length' => 5, 'required' => false)),
				'calle'           => new sfValidatorString(array('max_length' => 60, 'required' => false)),
				'num_ext'         => new sfValidatorString(array('max_length' => 30, 'required' => false)),
				'num_int'         => new sfValidatorString(array('max_length' => 30, 'required' => false)),
				'activo'          => new sfValidatorBoolean(array('required' => false)),
				'created_at'      => new sfValidatorDateTime(array('required' => false)),
				'updated_at'      => new sfValidatorDateTime(array('required' => false)),
		));

		$this->validatorSchema->setPostValidator(
				new sfValidatorPropelUnique(array('model' => 'Empresa', 'column' => array('nombre')))
		);

		$this->widgetSchema->setNameFormat('empresa[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Empresa';
	}


}
