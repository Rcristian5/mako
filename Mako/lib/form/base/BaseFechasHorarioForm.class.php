<?php

/**
 * FechasHorario form base class.
 *
 * @method FechasHorario getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseFechasHorarioForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'          => new sfWidgetFormInputHidden(),
				'horario_id'  => new sfWidgetFormPropelChoice(array('model' => 'Horario', 'add_empty' => true)),
				'fecha'       => new sfWidgetFormDateTime(),
				'hora_inicio' => new sfWidgetFormTime(),
				'hora_fin'    => new sfWidgetFormTime(),
		));

		$this->setValidators(array(
				'id'          => new sfValidatorPropelChoice(array('model' => 'FechasHorario', 'column' => 'id', 'required' => false)),
				'horario_id'  => new sfValidatorPropelChoice(array('model' => 'Horario', 'column' => 'id', 'required' => false)),
				'fecha'       => new sfValidatorDateTime(),
				'hora_inicio' => new sfValidatorTime(),
				'hora_fin'    => new sfValidatorTime(),
		));

		$this->widgetSchema->setNameFormat('fechas_horario[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'FechasHorario';
	}


}
