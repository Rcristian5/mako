<?php

/**
 * DiasOperacion form base class.
 *
 * @method DiasOperacion getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseDiasOperacionForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'        => new sfWidgetFormInputHidden(),
				'centro_id' => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => false)),
				'fecha'     => new sfWidgetFormDate(),
				'opera'     => new sfWidgetFormInputCheckbox(),
		));

		$this->setValidators(array(
				'id'        => new sfValidatorPropelChoice(array('model' => 'DiasOperacion', 'column' => 'id', 'required' => false)),
				'centro_id' => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id')),
				'fecha'     => new sfValidatorDate(),
				'opera'     => new sfValidatorBoolean(array('required' => false)),
		));

		$this->validatorSchema->setPostValidator(
				new sfValidatorPropelUnique(array('model' => 'DiasOperacion', 'column' => array('centro_id', 'fecha')))
		);

		$this->widgetSchema->setNameFormat('dias_operacion[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'DiasOperacion';
	}


}
