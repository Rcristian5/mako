<?php

/**
 * CursosFinalizados form base class.
 *
 * @method CursosFinalizados getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCursosFinalizadosForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'                          => new sfWidgetFormInputHidden(),
				'curso_id'                    => new sfWidgetFormPropelChoice(array('model' => 'Curso', 'add_empty' => false)),
				'alumno_id'                   => new sfWidgetFormPropelChoice(array('model' => 'Alumnos', 'add_empty' => false)),
				'grupo_id'                    => new sfWidgetFormPropelChoice(array('model' => 'Grupo', 'add_empty' => false)),
				'calificacion_id'             => new sfWidgetFormPropelChoice(array('model' => 'CalificacionFinal', 'add_empty' => true)),
				'promedio_final'              => new sfWidgetFormInputText(),
				'recursado'                   => new sfWidgetFormInputCheckbox(),
				'created_at'                  => new sfWidgetFormDateTime(),
				'updated_at'                  => new sfWidgetFormDateTime(),
				'estatus_curso_finalizado_id' => new sfWidgetFormPropelChoice(array('model' => 'EstatusCursoFinalizado', 'add_empty' => false)),
		));

		$this->setValidators(array(
				'id'                          => new sfValidatorPropelChoice(array('model' => 'CursosFinalizados', 'column' => 'id', 'required' => false)),
				'curso_id'                    => new sfValidatorPropelChoice(array('model' => 'Curso', 'column' => 'id')),
				'alumno_id'                   => new sfValidatorPropelChoice(array('model' => 'Alumnos', 'column' => 'matricula')),
				'grupo_id'                    => new sfValidatorPropelChoice(array('model' => 'Grupo', 'column' => 'id')),
				'calificacion_id'             => new sfValidatorPropelChoice(array('model' => 'CalificacionFinal', 'column' => 'id', 'required' => false)),
				'promedio_final'              => new sfValidatorNumber(array('required' => false)),
				'recursado'                   => new sfValidatorBoolean(),
				'created_at'                  => new sfValidatorDateTime(array('required' => false)),
				'updated_at'                  => new sfValidatorDateTime(array('required' => false)),
				'estatus_curso_finalizado_id' => new sfValidatorPropelChoice(array('model' => 'EstatusCursoFinalizado', 'column' => 'id')),
		));

		$this->widgetSchema->setNameFormat('cursos_finalizados[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'CursosFinalizados';
	}


}
