<?php

/**
 * PromocionCategoria form base class.
 *
 * @method PromocionCategoria getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePromocionCategoriaForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'promocion_id' => new sfWidgetFormInputHidden(),
				'categoria_id' => new sfWidgetFormInputHidden(),
		));

		$this->setValidators(array(
				'promocion_id' => new sfValidatorPropelChoice(array('model' => 'Promocion', 'column' => 'id', 'required' => false)),
				'categoria_id' => new sfValidatorPropelChoice(array('model' => 'CategoriaProducto', 'column' => 'id', 'required' => false)),
		));

		$this->widgetSchema->setNameFormat('promocion_categoria[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'PromocionCategoria';
	}


}
