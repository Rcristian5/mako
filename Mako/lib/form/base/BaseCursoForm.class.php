<?php

/**
 * Curso form base class.
 *
 * @method Curso getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCursoForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'                    => new sfWidgetFormInputHidden(),
				'categoria_curso_id'    => new sfWidgetFormPropelChoice(array('model' => 'CategoriaCurso', 'add_empty' => false)),
				'clave'                 => new sfWidgetFormInputText(),
				'nombre'                => new sfWidgetFormInputText(),
				'descripcion'           => new sfWidgetFormTextarea(),
				'perfil_id'             => new sfWidgetFormPropelChoice(array('model' => 'PerfilFacilitador', 'add_empty' => false)),
				'duracion'              => new sfWidgetFormInputText(),
				'seriado'               => new sfWidgetFormInputCheckbox(),
				'ultima_verificacion'   => new sfWidgetFormDateTime(),
				'asociado_moodle'       => new sfWidgetFormInputCheckbox(),
				'registrado_moodle'     => new sfWidgetFormInputCheckbox(),
				'asociado_rs'           => new sfWidgetFormInputCheckbox(),
				'registrado_rs'         => new sfWidgetFormInputCheckbox(),
				'restricciones_socio'   => new sfWidgetFormInputCheckbox(),
				'restricciones_horario' => new sfWidgetFormInputCheckbox(),
				'calificacion_minima'   => new sfWidgetFormInputText(),
				'publicado'             => new sfWidgetFormInputCheckbox(),
				'operador_id'           => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'activo'                => new sfWidgetFormInputCheckbox(),
				'created_at'            => new sfWidgetFormDateTime(),
				'updated_at'            => new sfWidgetFormDateTime(),
				'nombre_corto'          => new sfWidgetFormInputText(),
				'curso_centro_list'     => new sfWidgetFormPropelChoice(array('multiple' => true, 'model' => 'Centro')),
				'producto_curso_list'   => new sfWidgetFormPropelChoice(array('multiple' => true, 'model' => 'Producto')),
		));

		$this->setValidators(array(
				'id'                    => new sfValidatorPropelChoice(array('model' => 'Curso', 'column' => 'id', 'required' => false)),
				'categoria_curso_id'    => new sfValidatorPropelChoice(array('model' => 'CategoriaCurso', 'column' => 'id')),
				'clave'                 => new sfValidatorString(array('max_length' => 100)),
				'nombre'                => new sfValidatorString(array('max_length' => 255)),
				'descripcion'           => new sfValidatorString(),
				'perfil_id'             => new sfValidatorPropelChoice(array('model' => 'PerfilFacilitador', 'column' => 'id')),
				'duracion'              => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'seriado'               => new sfValidatorBoolean(array('required' => false)),
				'ultima_verificacion'   => new sfValidatorDateTime(array('required' => false)),
				'asociado_moodle'       => new sfValidatorBoolean(array('required' => false)),
				'registrado_moodle'     => new sfValidatorBoolean(array('required' => false)),
				'asociado_rs'           => new sfValidatorBoolean(array('required' => false)),
				'registrado_rs'         => new sfValidatorBoolean(array('required' => false)),
				'restricciones_socio'   => new sfValidatorBoolean(array('required' => false)),
				'restricciones_horario' => new sfValidatorBoolean(array('required' => false)),
				'calificacion_minima'   => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
				'publicado'             => new sfValidatorBoolean(),
				'operador_id'           => new sfValidatorPropelChoice(array('model' => 'Usuario', 'column' => 'id', 'required' => false)),
				'activo'                => new sfValidatorBoolean(array('required' => false)),
				'created_at'            => new sfValidatorDateTime(array('required' => false)),
				'updated_at'            => new sfValidatorDateTime(array('required' => false)),
				'nombre_corto'          => new sfValidatorString(array('max_length' => 100)),
				'curso_centro_list'     => new sfValidatorPropelChoice(array('multiple' => true, 'model' => 'Centro', 'required' => false)),
				'producto_curso_list'   => new sfValidatorPropelChoice(array('multiple' => true, 'model' => 'Producto', 'required' => false)),
		));

		$this->validatorSchema->setPostValidator(
				new sfValidatorAnd(array(
						new sfValidatorPropelUnique(array('model' => 'Curso', 'column' => array('clave'))),
						new sfValidatorPropelUnique(array('model' => 'Curso', 'column' => array('nombre'))),
						new sfValidatorPropelUnique(array('model' => 'Curso', 'column' => array('nombre_corto'))),
				))
		);

		$this->widgetSchema->setNameFormat('curso[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Curso';
	}


	public function updateDefaultsFromObject()
	{
		parent::updateDefaultsFromObject();

		if (isset($this->widgetSchema['curso_centro_list']))
		{
			$values = array();
			foreach ($this->object->getCursoCentros() as $obj)
			{
				$values[] = $obj->getCentroId();
			}

			$this->setDefault('curso_centro_list', $values);
		}

		if (isset($this->widgetSchema['producto_curso_list']))
		{
			$values = array();
			foreach ($this->object->getProductoCursos() as $obj)
			{
				$values[] = $obj->getProductoId();
			}

			$this->setDefault('producto_curso_list', $values);
		}

	}

	protected function doSave($con = null)
	{
		parent::doSave($con);

		$this->saveCursoCentroList($con);
		$this->saveProductoCursoList($con);
	}

	public function saveCursoCentroList($con = null)
	{
		if (!$this->isValid())
		{
			throw $this->getErrorSchema();
		}

		if (!isset($this->widgetSchema['curso_centro_list']))
		{
			// somebody has unset this widget
			return;
		}

		if (null === $con)
		{
			$con = $this->getConnection();
		}

		$c = new Criteria();
		$c->add(CursoCentroPeer::CURSO_ID, $this->object->getPrimaryKey());
		CursoCentroPeer::doDelete($c, $con);

		$values = $this->getValue('curso_centro_list');
		if (is_array($values))
		{
			foreach ($values as $value)
			{
				$obj = new CursoCentro();
				$obj->setCursoId($this->object->getPrimaryKey());
				$obj->setCentroId($value);
				$obj->save();
			}
		}
	}

	public function saveProductoCursoList($con = null)
	{
		if (!$this->isValid())
		{
			throw $this->getErrorSchema();
		}

		if (!isset($this->widgetSchema['producto_curso_list']))
		{
			// somebody has unset this widget
			return;
		}

		if (null === $con)
		{
			$con = $this->getConnection();
		}

		$c = new Criteria();
		$c->add(ProductoCursoPeer::CURSO_ID, $this->object->getPrimaryKey());
		ProductoCursoPeer::doDelete($c, $con);

		$values = $this->getValue('producto_curso_list');
		if (is_array($values))
		{
			foreach ($values as $value)
			{
				$obj = new ProductoCurso();
				$obj->setCursoId($this->object->getPrimaryKey());
				$obj->setProductoId($value);
				$obj->save();
			}
		}
	}

}
