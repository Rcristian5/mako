<?php

/**
 * RestriccionesRuta form base class.
 *
 * @method RestriccionesRuta getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseRestriccionesRutaForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'                          => new sfWidgetFormInputHidden(),
				'tipo_restriccion_id'         => new sfWidgetFormPropelChoice(array('model' => 'TipoRestriccion', 'add_empty' => false)),
				'curso_id'                    => new sfWidgetFormPropelChoice(array('model' => 'Curso', 'add_empty' => false)),
				'restriccion_ruta_curso_list' => new sfWidgetFormPropelChoice(array('multiple' => true, 'model' => 'Curso')),
		));

		$this->setValidators(array(
				'id'                          => new sfValidatorPropelChoice(array('model' => 'RestriccionesRuta', 'column' => 'id', 'required' => false)),
				'tipo_restriccion_id'         => new sfValidatorPropelChoice(array('model' => 'TipoRestriccion', 'column' => 'id')),
				'curso_id'                    => new sfValidatorPropelChoice(array('model' => 'Curso', 'column' => 'id')),
				'restriccion_ruta_curso_list' => new sfValidatorPropelChoice(array('multiple' => true, 'model' => 'Curso', 'required' => false)),
		));

		$this->widgetSchema->setNameFormat('restricciones_ruta[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'RestriccionesRuta';
	}


	public function updateDefaultsFromObject()
	{
		parent::updateDefaultsFromObject();

		if (isset($this->widgetSchema['restriccion_ruta_curso_list']))
		{
			$values = array();
			foreach ($this->object->getRestriccionRutaCursos() as $obj)
			{
				$values[] = $obj->getCursoId();
			}

			$this->setDefault('restriccion_ruta_curso_list', $values);
		}

	}

	protected function doSave($con = null)
	{
		parent::doSave($con);

		$this->saveRestriccionRutaCursoList($con);
	}

	public function saveRestriccionRutaCursoList($con = null)
	{
		if (!$this->isValid())
		{
			throw $this->getErrorSchema();
		}

		if (!isset($this->widgetSchema['restriccion_ruta_curso_list']))
		{
			// somebody has unset this widget
			return;
		}

		if (null === $con)
		{
			$con = $this->getConnection();
		}

		$c = new Criteria();
		$c->add(RestriccionRutaCursoPeer::RESTRICCION_RUTA_ID, $this->object->getPrimaryKey());
		RestriccionRutaCursoPeer::doDelete($c, $con);

		$values = $this->getValue('restriccion_ruta_curso_list');
		if (is_array($values))
		{
			foreach ($values as $value)
			{
				$obj = new RestriccionRutaCurso();
				$obj->setRestriccionRutaId($this->object->getPrimaryKey());
				$obj->setCursoId($value);
				$obj->save();
			}
		}
	}

}
