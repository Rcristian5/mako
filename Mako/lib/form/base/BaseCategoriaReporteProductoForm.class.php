<?php

/**
 * CategoriaReporteProducto form base class.
 *
 * @method CategoriaReporteProducto getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCategoriaReporteProductoForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'categoria_id' => new sfWidgetFormInputHidden(),
				'producto_id'  => new sfWidgetFormInputHidden(),
		));

		$this->setValidators(array(
				'categoria_id' => new sfValidatorPropelChoice(array('model' => 'CategoriaReporte', 'column' => 'id', 'required' => false)),
				'producto_id'  => new sfValidatorPropelChoice(array('model' => 'Producto', 'column' => 'id', 'required' => false)),
		));

		$this->widgetSchema->setNameFormat('categoria_reporte_producto[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'CategoriaReporteProducto';
	}


}
