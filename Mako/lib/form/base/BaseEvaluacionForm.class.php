<?php

/**
 * Evaluacion form base class.
 *
 * @method Evaluacion getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseEvaluacionForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'                 => new sfWidgetFormInputHidden(),
				'activo'             => new sfWidgetFormInputCheckbox(),
				'curso_id'           => new sfWidgetFormPropelChoice(array('model' => 'Curso', 'add_empty' => false)),
				'capitulo_inicio'    => new sfWidgetFormPropelChoice(array('model' => 'Capitulo', 'add_empty' => true)),
				'capitulo_fin'       => new sfWidgetFormPropelChoice(array('model' => 'Capitulo', 'add_empty' => true)),
				'descripcion'        => new sfWidgetFormTextarea(),
				'tema_inicio'        => new sfWidgetFormPropelChoice(array('model' => 'Tema', 'add_empty' => true)),
				'tema_fin'           => new sfWidgetFormPropelChoice(array('model' => 'Tema', 'add_empty' => true)),
				'tipo'               => new sfWidgetFormInputText(),
				'porcentaje'         => new sfWidgetFormInputText(),
				'numero'             => new sfWidgetFormInputText(),
				'tipo_evaluacion_id' => new sfWidgetFormPropelChoice(array('model' => 'TipoEvaluacion', 'add_empty' => false)),
				'operador_id'        => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'created_at'         => new sfWidgetFormDateTime(),
				'updated_at'         => new sfWidgetFormDateTime(),
		));

		$this->setValidators(array(
				'id'                 => new sfValidatorPropelChoice(array('model' => 'Evaluacion', 'column' => 'id', 'required' => false)),
				'activo'             => new sfValidatorBoolean(array('required' => false)),
				'curso_id'           => new sfValidatorPropelChoice(array('model' => 'Curso', 'column' => 'id')),
				'capitulo_inicio'    => new sfValidatorPropelChoice(array('model' => 'Capitulo', 'column' => 'id', 'required' => false)),
				'capitulo_fin'       => new sfValidatorPropelChoice(array('model' => 'Capitulo', 'column' => 'id', 'required' => false)),
				'descripcion'        => new sfValidatorString(),
				'tema_inicio'        => new sfValidatorPropelChoice(array('model' => 'Tema', 'column' => 'id', 'required' => false)),
				'tema_fin'           => new sfValidatorPropelChoice(array('model' => 'Tema', 'column' => 'id', 'required' => false)),
				'tipo'               => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'porcentaje'         => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'numero'             => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'tipo_evaluacion_id' => new sfValidatorPropelChoice(array('model' => 'TipoEvaluacion', 'column' => 'id')),
				'operador_id'        => new sfValidatorPropelChoice(array('model' => 'Usuario', 'column' => 'id', 'required' => false)),
				'created_at'         => new sfValidatorDateTime(array('required' => false)),
				'updated_at'         => new sfValidatorDateTime(array('required' => false)),
		));

		$this->widgetSchema->setNameFormat('evaluacion[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Evaluacion';
	}


}
