<?php

/**
 * GrupoHorario form base class.
 *
 * @method GrupoHorario getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseGrupoHorarioForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'grupo_id'   => new sfWidgetFormPropelChoice(array('model' => 'Grupo', 'add_empty' => false)),
				'horario_id' => new sfWidgetFormPropelChoice(array('model' => 'Horario', 'add_empty' => false)),
				'id'         => new sfWidgetFormInputHidden(),
		));

		$this->setValidators(array(
				'grupo_id'   => new sfValidatorPropelChoice(array('model' => 'Grupo', 'column' => 'id')),
				'horario_id' => new sfValidatorPropelChoice(array('model' => 'Horario', 'column' => 'id')),
				'id'         => new sfValidatorPropelChoice(array('model' => 'GrupoHorario', 'column' => 'id', 'required' => false)),
		));

		$this->widgetSchema->setNameFormat('grupo_horario[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'GrupoHorario';
	}


}
