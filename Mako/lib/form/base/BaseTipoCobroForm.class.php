<?php

/**
 * TipoCobro form base class.
 *
 * @method TipoCobro getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseTipoCobroForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'                            => new sfWidgetFormInputHidden(),
				'nombre'                        => new sfWidgetFormInputText(),
				'cantidad'                      => new sfWidgetFormInputText(),
				'temporalidad'                  => new sfWidgetFormInputText(),
				'activo'                        => new sfWidgetFormInputCheckbox(),
				'created_at'                    => new sfWidgetFormDateTime(),
				'updated_at'                    => new sfWidgetFormDateTime(),
				'modalidad_cobro_producto_list' => new sfWidgetFormPropelChoice(array('multiple' => true, 'model' => 'Producto')),
				'subproducto_cobranza_list'     => new sfWidgetFormPropelChoice(array('multiple' => true, 'model' => 'Producto')),
		));

		$this->setValidators(array(
				'id'                            => new sfValidatorPropelChoice(array('model' => 'TipoCobro', 'column' => 'id', 'required' => false)),
				'nombre'                        => new sfValidatorString(array('max_length' => 255)),
				'cantidad'                      => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
				'temporalidad'                  => new sfValidatorString(array('max_length' => 30, 'required' => false)),
				'activo'                        => new sfValidatorBoolean(array('required' => false)),
				'created_at'                    => new sfValidatorDateTime(array('required' => false)),
				'updated_at'                    => new sfValidatorDateTime(array('required' => false)),
				'modalidad_cobro_producto_list' => new sfValidatorPropelChoice(array('multiple' => true, 'model' => 'Producto', 'required' => false)),
				'subproducto_cobranza_list'     => new sfValidatorPropelChoice(array('multiple' => true, 'model' => 'Producto', 'required' => false)),
		));

		$this->validatorSchema->setPostValidator(
				new sfValidatorPropelUnique(array('model' => 'TipoCobro', 'column' => array('nombre')))
		);

		$this->widgetSchema->setNameFormat('tipo_cobro[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'TipoCobro';
	}


	public function updateDefaultsFromObject()
	{
		parent::updateDefaultsFromObject();

		if (isset($this->widgetSchema['modalidad_cobro_producto_list']))
		{
			$values = array();
			foreach ($this->object->getModalidadCobroProductos() as $obj)
			{
				$values[] = $obj->getProductoId();
			}

			$this->setDefault('modalidad_cobro_producto_list', $values);
		}

		if (isset($this->widgetSchema['subproducto_cobranza_list']))
		{
			$values = array();
			foreach ($this->object->getSubproductoCobranzas() as $obj)
			{
				$values[] = $obj->getProductoId();
			}

			$this->setDefault('subproducto_cobranza_list', $values);
		}

	}

	protected function doSave($con = null)
	{
		parent::doSave($con);

		$this->saveModalidadCobroProductoList($con);
		$this->saveSubproductoCobranzaList($con);
	}

	public function saveModalidadCobroProductoList($con = null)
	{
		if (!$this->isValid())
		{
			throw $this->getErrorSchema();
		}

		if (!isset($this->widgetSchema['modalidad_cobro_producto_list']))
		{
			// somebody has unset this widget
			return;
		}

		if (null === $con)
		{
			$con = $this->getConnection();
		}

		$c = new Criteria();
		$c->add(ModalidadCobroProductoPeer::COBRO_ID, $this->object->getPrimaryKey());
		ModalidadCobroProductoPeer::doDelete($c, $con);

		$values = $this->getValue('modalidad_cobro_producto_list');
		if (is_array($values))
		{
			foreach ($values as $value)
			{
				$obj = new ModalidadCobroProducto();
				$obj->setCobroId($this->object->getPrimaryKey());
				$obj->setProductoId($value);
				$obj->save();
			}
		}
	}

	public function saveSubproductoCobranzaList($con = null)
	{
		if (!$this->isValid())
		{
			throw $this->getErrorSchema();
		}

		if (!isset($this->widgetSchema['subproducto_cobranza_list']))
		{
			// somebody has unset this widget
			return;
		}

		if (null === $con)
		{
			$con = $this->getConnection();
		}

		$c = new Criteria();
		$c->add(SubproductoCobranzaPeer::COBRO_ID, $this->object->getPrimaryKey());
		SubproductoCobranzaPeer::doDelete($c, $con);

		$values = $this->getValue('subproducto_cobranza_list');
		if (is_array($values))
		{
			foreach ($values as $value)
			{
				$obj = new SubproductoCobranza();
				$obj->setCobroId($this->object->getPrimaryKey());
				$obj->setProductoId($value);
				$obj->save();
			}
		}
	}

}
