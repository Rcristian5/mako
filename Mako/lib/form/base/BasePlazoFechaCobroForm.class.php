<?php

/**
 * PlazoFechaCobro form base class.
 *
 * @method PlazoFechaCobro getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePlazoFechaCobroForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'grupo_id'    => new sfWidgetFormInputHidden(),
				'fecha_fin'   => new sfWidgetFormDate(),
				'operador_id' => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'activo'      => new sfWidgetFormInputCheckbox(),
				'created_at'  => new sfWidgetFormDateTime(),
				'updated_at'  => new sfWidgetFormDateTime(),
		));

		$this->setValidators(array(
				'grupo_id'    => new sfValidatorPropelChoice(array('model' => 'Grupo', 'column' => 'id', 'required' => false)),
				'fecha_fin'   => new sfValidatorDate(),
				'operador_id' => new sfValidatorPropelChoice(array('model' => 'Usuario', 'column' => 'id', 'required' => false)),
				'activo'      => new sfValidatorBoolean(array('required' => false)),
				'created_at'  => new sfValidatorDateTime(array('required' => false)),
				'updated_at'  => new sfValidatorDateTime(array('required' => false)),
		));

		$this->widgetSchema->setNameFormat('plazo_fecha_cobro[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'PlazoFechaCobro';
	}


}
