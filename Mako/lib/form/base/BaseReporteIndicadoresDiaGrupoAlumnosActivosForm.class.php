<?php

/**
 * ReporteIndicadoresDiaGrupoAlumnosActivos form base class.
 *
 * @method ReporteIndicadoresDiaGrupoAlumnosActivos getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseReporteIndicadoresDiaGrupoAlumnosActivosForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'                             => new sfWidgetFormInputHidden(),
				'reporte_indicador_dia_grupo_id' => new sfWidgetFormInputHidden(),
				'centro_id'                      => new sfWidgetFormInputHidden(),
				'socio_id'                       => new sfWidgetFormInputText(),
				'usuario'                        => new sfWidgetFormInputText(),
				'nombre_completo'                => new sfWidgetFormInputText(),
				'edad'                           => new sfWidgetFormInputText(),
				'ocupacion_id'                   => new sfWidgetFormPropelChoice(array('model' => 'Ocupacion', 'add_empty' => true)),
				'sexo'                           => new sfWidgetFormInputText(),
		));

		$this->setValidators(array(
				'id'                             => new sfValidatorPropelChoice(array('model' => 'ReporteIndicadoresDiaGrupoAlumnosActivos', 'column' => 'id', 'required' => false)),
				'reporte_indicador_dia_grupo_id' => new sfValidatorPropelChoice(array('model' => 'ReporteIndicadoresDiaGrupo', 'column' => 'id', 'required' => false)),
				'centro_id'                      => new sfValidatorPropelChoice(array('model' => 'ReporteIndicadoresDiaGrupo', 'column' => 'centro_id', 'required' => false)),
				'socio_id'                       => new sfValidatorInteger(array('min' => -9.2233720368548E+18, 'max' => 9.2233720368548E+18)),
				'usuario'                        => new sfValidatorString(array('max_length' => 90)),
				'nombre_completo'                => new sfValidatorString(array('max_length' => 200)),
				'edad'                           => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'ocupacion_id'                   => new sfValidatorPropelChoice(array('model' => 'Ocupacion', 'column' => 'id', 'required' => false)),
				'sexo'                           => new sfValidatorString(array('max_length' => 1)),
		));

		$this->widgetSchema->setNameFormat('reporte_indicadores_dia_grupo_alumnos_activos[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'ReporteIndicadoresDiaGrupoAlumnosActivos';
	}


}
