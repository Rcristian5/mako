<?php

/**
 * SesionOperadorHistorico form base class.
 *
 * @method SesionOperadorHistorico getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseSesionOperadorHistoricoForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'            => new sfWidgetFormInputHidden(),
				'centro_id'     => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'ip'            => new sfWidgetFormInputText(),
				'usuario_id'    => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'usuario'       => new sfWidgetFormInputText(),
				'fecha_login'   => new sfWidgetFormDateTime(),
				'fecha_logout'  => new sfWidgetFormDateTime(),
				'transcurridos' => new sfWidgetFormInputText(),
		));

		$this->setValidators(array(
				'id'            => new sfValidatorPropelChoice(array('model' => 'SesionOperadorHistorico', 'column' => 'id', 'required' => false)),
				'centro_id'     => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id', 'required' => false)),
				'ip'            => new sfValidatorString(array('max_length' => 15)),
				'usuario_id'    => new sfValidatorPropelChoice(array('model' => 'Usuario', 'column' => 'id', 'required' => false)),
				'usuario'       => new sfValidatorString(array('max_length' => 90)),
				'fecha_login'   => new sfValidatorDateTime(),
				'fecha_logout'  => new sfValidatorDateTime(),
				'transcurridos' => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
		));

		$this->widgetSchema->setNameFormat('sesion_operador_historico[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'SesionOperadorHistorico';
	}


}
