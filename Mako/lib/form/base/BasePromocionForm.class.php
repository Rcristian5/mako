<?php

/**
 * Promocion form base class.
 *
 * @method Promocion getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePromocionForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'                       => new sfWidgetFormInputHidden(),
				'nombre'                   => new sfWidgetFormInputText(),
				'descripcion'              => new sfWidgetFormTextarea(),
				'vigente_de'               => new sfWidgetFormDateTime(),
				'vigente_a'                => new sfWidgetFormDateTime(),
				'activo'                   => new sfWidgetFormInputCheckbox(),
				'descuento'                => new sfWidgetFormInputText(),
				'created_at'               => new sfWidgetFormDateTime(),
				'updated_at'               => new sfWidgetFormDateTime(),
				'promocion_categoria_list' => new sfWidgetFormPropelChoice(array('multiple' => true, 'model' => 'CategoriaProducto')),
				'promocion_producto_list'  => new sfWidgetFormPropelChoice(array('multiple' => true, 'model' => 'Producto')),
		));

		$this->setValidators(array(
				'id'                       => new sfValidatorPropelChoice(array('model' => 'Promocion', 'column' => 'id', 'required' => false)),
				'nombre'                   => new sfValidatorString(array('max_length' => 255)),
				'descripcion'              => new sfValidatorString(),
				'vigente_de'               => new sfValidatorDateTime(array('required' => false)),
				'vigente_a'                => new sfValidatorDateTime(array('required' => false)),
				'activo'                   => new sfValidatorBoolean(array('required' => false)),
				'descuento'                => new sfValidatorNumber(array('required' => false)),
				'created_at'               => new sfValidatorDateTime(array('required' => false)),
				'updated_at'               => new sfValidatorDateTime(array('required' => false)),
				'promocion_categoria_list' => new sfValidatorPropelChoice(array('multiple' => true, 'model' => 'CategoriaProducto', 'required' => false)),
				'promocion_producto_list'  => new sfValidatorPropelChoice(array('multiple' => true, 'model' => 'Producto', 'required' => false)),
		));

		$this->validatorSchema->setPostValidator(
				new sfValidatorPropelUnique(array('model' => 'Promocion', 'column' => array('nombre')))
		);

		$this->widgetSchema->setNameFormat('promocion[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Promocion';
	}


	public function updateDefaultsFromObject()
	{
		parent::updateDefaultsFromObject();

		if (isset($this->widgetSchema['promocion_categoria_list']))
		{
			$values = array();
			foreach ($this->object->getPromocionCategorias() as $obj)
			{
				$values[] = $obj->getCategoriaId();
			}

			$this->setDefault('promocion_categoria_list', $values);
		}

		if (isset($this->widgetSchema['promocion_producto_list']))
		{
			$values = array();
			foreach ($this->object->getPromocionProductos() as $obj)
			{
				$values[] = $obj->getProductoId();
			}

			$this->setDefault('promocion_producto_list', $values);
		}

	}

	protected function doSave($con = null)
	{
		parent::doSave($con);

		$this->savePromocionCategoriaList($con);
		$this->savePromocionProductoList($con);
	}

	public function savePromocionCategoriaList($con = null)
	{
		if (!$this->isValid())
		{
			throw $this->getErrorSchema();
		}

		if (!isset($this->widgetSchema['promocion_categoria_list']))
		{
			// somebody has unset this widget
			return;
		}

		if (null === $con)
		{
			$con = $this->getConnection();
		}

		$c = new Criteria();
		$c->add(PromocionCategoriaPeer::PROMOCION_ID, $this->object->getPrimaryKey());
		PromocionCategoriaPeer::doDelete($c, $con);

		$values = $this->getValue('promocion_categoria_list');
		if (is_array($values))
		{
			foreach ($values as $value)
			{
				$obj = new PromocionCategoria();
				$obj->setPromocionId($this->object->getPrimaryKey());
				$obj->setCategoriaId($value);
				$obj->save();
			}
		}
	}

	public function savePromocionProductoList($con = null)
	{
		if (!$this->isValid())
		{
			throw $this->getErrorSchema();
		}

		if (!isset($this->widgetSchema['promocion_producto_list']))
		{
			// somebody has unset this widget
			return;
		}

		if (null === $con)
		{
			$con = $this->getConnection();
		}

		$c = new Criteria();
		$c->add(PromocionProductoPeer::PROMOCION_ID, $this->object->getPrimaryKey());
		PromocionProductoPeer::doDelete($c, $con);

		$values = $this->getValue('promocion_producto_list');
		if (is_array($values))
		{
			foreach ($values as $value)
			{
				$obj = new PromocionProducto();
				$obj->setPromocionId($this->object->getPrimaryKey());
				$obj->setProductoId($value);
				$obj->save();
			}
		}
	}

}
