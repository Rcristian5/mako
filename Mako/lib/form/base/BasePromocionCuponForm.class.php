<?php

/**
 * PromocionCupon form base class.
 *
 * @method PromocionCupon getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePromocionCuponForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'                            => new sfWidgetFormInputHidden(),
				'centro_id'                     => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'nombre'                        => new sfWidgetFormInputText(),
				'descripcion'                   => new sfWidgetFormTextarea(),
				'vigente_de'                    => new sfWidgetFormDateTime(),
				'vigente_a'                     => new sfWidgetFormDateTime(),
				'descuento'                     => new sfWidgetFormInputText(),
				'cantidad_cupones'              => new sfWidgetFormInputText(),
				'creado_por'                    => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'activo'                        => new sfWidgetFormInputCheckbox(),
				'created_at'                    => new sfWidgetFormDateTime(),
				'updated_at'                    => new sfWidgetFormDateTime(),
				'promocion_cupon_producto_list' => new sfWidgetFormPropelChoice(array('multiple' => true, 'model' => 'Producto')),
		));

		$this->setValidators(array(
				'id'                            => new sfValidatorPropelChoice(array('model' => 'PromocionCupon', 'column' => 'id', 'required' => false)),
				'centro_id'                     => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id', 'required' => false)),
				'nombre'                        => new sfValidatorString(array('max_length' => 256)),
				'descripcion'                   => new sfValidatorString(),
				'vigente_de'                    => new sfValidatorDateTime(array('required' => false)),
				'vigente_a'                     => new sfValidatorDateTime(array('required' => false)),
				'descuento'                     => new sfValidatorNumber(array('required' => false)),
				'cantidad_cupones'              => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
				'creado_por'                    => new sfValidatorPropelChoice(array('model' => 'Usuario', 'column' => 'id', 'required' => false)),
				'activo'                        => new sfValidatorBoolean(array('required' => false)),
				'created_at'                    => new sfValidatorDateTime(array('required' => false)),
				'updated_at'                    => new sfValidatorDateTime(array('required' => false)),
				'promocion_cupon_producto_list' => new sfValidatorPropelChoice(array('multiple' => true, 'model' => 'Producto', 'required' => false)),
		));

		$this->widgetSchema->setNameFormat('promocion_cupon[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'PromocionCupon';
	}


	public function updateDefaultsFromObject()
	{
		parent::updateDefaultsFromObject();

		if (isset($this->widgetSchema['promocion_cupon_producto_list']))
		{
			$values = array();
			foreach ($this->object->getPromocionCuponProductos() as $obj)
			{
				$values[] = $obj->getProductoId();
			}

			$this->setDefault('promocion_cupon_producto_list', $values);
		}

	}

	protected function doSave($con = null)
	{
		parent::doSave($con);

		$this->savePromocionCuponProductoList($con);
	}

	public function savePromocionCuponProductoList($con = null)
	{
		if (!$this->isValid())
		{
			throw $this->getErrorSchema();
		}

		if (!isset($this->widgetSchema['promocion_cupon_producto_list']))
		{
			// somebody has unset this widget
			return;
		}

		if (null === $con)
		{
			$con = $this->getConnection();
		}

		$c = new Criteria();
		$c->add(PromocionCuponProductoPeer::PROMOCION_ID, $this->object->getPrimaryKey());
		PromocionCuponProductoPeer::doDelete($c, $con);

		$values = $this->getValue('promocion_cupon_producto_list');
		if (is_array($values))
		{
			foreach ($values as $value)
			{
				$obj = new PromocionCuponProducto();
				$obj->setPromocionId($this->object->getPrimaryKey());
				$obj->setProductoId($value);
				$obj->save();
			}
		}
	}

}
