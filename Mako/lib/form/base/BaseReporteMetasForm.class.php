<?php

/**
 * ReporteMetas form base class.
 *
 * @method ReporteMetas getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseReporteMetasForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'                    => new sfWidgetFormInputHidden(),
				'centro_id'             => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => false)),
				'categoria_fija_id'     => new sfWidgetFormPropelChoice(array('model' => 'CategoriaMetasFijas', 'add_empty' => true)),
				'categoria_producto_id' => new sfWidgetFormPropelChoice(array('model' => 'CategoriaReporte', 'add_empty' => true)),
				'categoria_beca_id'     => new sfWidgetFormPropelChoice(array('model' => 'Beca', 'add_empty' => true)),
				'cantidad'              => new sfWidgetFormInputText(),
				'fecha'                 => new sfWidgetFormDate(),
				'semana'                => new sfWidgetFormInputText(),
				'anio'                  => new sfWidgetFormInputText(),
				'entero'                => new sfWidgetFormInputCheckbox(),
				'meta_diaria'           => new sfWidgetFormInputText(),
				'activo'                => new sfWidgetFormInputCheckbox(),
				'sync'                  => new sfWidgetFormInputCheckbox(),
				'created_at'            => new sfWidgetFormDateTime(),
				'updated_at'            => new sfWidgetFormDateTime(),
		));

		$this->setValidators(array(
				'id'                    => new sfValidatorPropelChoice(array('model' => 'ReporteMetas', 'column' => 'id', 'required' => false)),
				'centro_id'             => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id')),
				'categoria_fija_id'     => new sfValidatorPropelChoice(array('model' => 'CategoriaMetasFijas', 'column' => 'id', 'required' => false)),
				'categoria_producto_id' => new sfValidatorPropelChoice(array('model' => 'CategoriaReporte', 'column' => 'id', 'required' => false)),
				'categoria_beca_id'     => new sfValidatorPropelChoice(array('model' => 'Beca', 'column' => 'id', 'required' => false)),
				'cantidad'              => new sfValidatorNumber(),
				'fecha'                 => new sfValidatorDate(),
				'semana'                => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'anio'                  => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'entero'                => new sfValidatorBoolean(array('required' => false)),
				'meta_diaria'           => new sfValidatorNumber(),
				'activo'                => new sfValidatorBoolean(array('required' => false)),
				'sync'                  => new sfValidatorBoolean(array('required' => false)),
				'created_at'            => new sfValidatorDateTime(array('required' => false)),
				'updated_at'            => new sfValidatorDateTime(array('required' => false)),
		));

		$this->widgetSchema->setNameFormat('reporte_metas[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'ReporteMetas';
	}


}
