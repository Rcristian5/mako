<?php

/**
 * DiasNoLaborables form base class.
 *
 * @method DiasNoLaborables getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseDiasNoLaborablesForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'          => new sfWidgetFormInputHidden(),
				'dia'         => new sfWidgetFormDateTime(),
				'descripcion' => new sfWidgetFormTextarea(),
				'operador_id' => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'created_at'  => new sfWidgetFormDateTime(),
				'updated_at'  => new sfWidgetFormDateTime(),
		));

		$this->setValidators(array(
				'id'          => new sfValidatorPropelChoice(array('model' => 'DiasNoLaborables', 'column' => 'id', 'required' => false)),
				'dia'         => new sfValidatorDateTime(),
				'descripcion' => new sfValidatorString(array('required' => false)),
				'operador_id' => new sfValidatorPropelChoice(array('model' => 'Usuario', 'column' => 'id', 'required' => false)),
				'created_at'  => new sfValidatorDateTime(array('required' => false)),
				'updated_at'  => new sfValidatorDateTime(array('required' => false)),
		));

		$this->widgetSchema->setNameFormat('dias_no_laborables[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'DiasNoLaborables';
	}


}
