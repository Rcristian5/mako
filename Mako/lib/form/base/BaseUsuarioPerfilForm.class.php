<?php

/**
 * UsuarioPerfil form base class.
 *
 * @method UsuarioPerfil getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseUsuarioPerfilForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'usuario_id' => new sfWidgetFormInputHidden(),
				'perfil_id'  => new sfWidgetFormInputHidden(),
		));

		$this->setValidators(array(
				'usuario_id' => new sfValidatorPropelChoice(array('model' => 'Usuario', 'column' => 'id', 'required' => false)),
				'perfil_id'  => new sfValidatorPropelChoice(array('model' => 'PerfilFacilitador', 'column' => 'id', 'required' => false)),
		));

		$this->widgetSchema->setNameFormat('usuario_perfil[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'UsuarioPerfil';
	}


}
