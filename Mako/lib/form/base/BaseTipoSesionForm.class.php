<?php

/**
 * TipoSesion form base class.
 *
 * @method TipoSesion getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseTipoSesionForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'         => new sfWidgetFormInputHidden(),
				'nombre'     => new sfWidgetFormInputText(),
				'activo'     => new sfWidgetFormInputCheckbox(),
				'created_at' => new sfWidgetFormDateTime(),
				'updated_at' => new sfWidgetFormDateTime(),
		));

		$this->setValidators(array(
				'id'         => new sfValidatorPropelChoice(array('model' => 'TipoSesion', 'column' => 'id', 'required' => false)),
				'nombre'     => new sfValidatorString(array('max_length' => 90)),
				'activo'     => new sfValidatorBoolean(array('required' => false)),
				'created_at' => new sfValidatorDateTime(array('required' => false)),
				'updated_at' => new sfValidatorDateTime(array('required' => false)),
		));

		$this->widgetSchema->setNameFormat('tipo_sesion[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'TipoSesion';
	}


}
