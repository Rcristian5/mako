<?php

/**
 * GrupoFacilitador form base class.
 *
 * @method GrupoFacilitador getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseGrupoFacilitadorForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'         => new sfWidgetFormInputHidden(),
				'grupo_id'   => new sfWidgetFormPropelChoice(array('model' => 'Grupo', 'add_empty' => false)),
				'usuario_id' => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => false)),
		));

		$this->setValidators(array(
				'id'         => new sfValidatorPropelChoice(array('model' => 'GrupoFacilitador', 'column' => 'id', 'required' => false)),
				'grupo_id'   => new sfValidatorPropelChoice(array('model' => 'Grupo', 'column' => 'id')),
				'usuario_id' => new sfValidatorPropelChoice(array('model' => 'Usuario', 'column' => 'id')),
		));

		$this->widgetSchema->setNameFormat('grupo_facilitador[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'GrupoFacilitador';
	}


}
