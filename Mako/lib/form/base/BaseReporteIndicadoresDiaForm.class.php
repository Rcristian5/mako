<?php

/**
 * ReporteIndicadoresDia form base class.
 *
 * @method ReporteIndicadoresDia getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseReporteIndicadoresDiaForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'                    => new sfWidgetFormInputHidden(),
				'centro_id'             => new sfWidgetFormInputHidden(),
				'fecha'                 => new sfWidgetFormDate(),
				'semana'                => new sfWidgetFormInputText(),
				'anio'                  => new sfWidgetFormInputText(),
				'registros_nuevos'      => new sfWidgetFormInputText(),
				'socios_registrados'    => new sfWidgetFormInputText(),
				'socios_activos'        => new sfWidgetFormInputText(),
				'socios_activos_nuevos' => new sfWidgetFormInputText(),
				'cobranza_educacion'    => new sfWidgetFormInputText(),
				'cobranza_otros'        => new sfWidgetFormInputText(),
				'cobranza_internet'     => new sfWidgetFormInputText(),
				'cobranza_total'        => new sfWidgetFormInputText(),
				'created_at'            => new sfWidgetFormDateTime(),
				'updated_at'            => new sfWidgetFormDateTime(),
		));

		$this->setValidators(array(
				'id'                    => new sfValidatorPropelChoice(array('model' => 'ReporteIndicadoresDia', 'column' => 'id', 'required' => false)),
				'centro_id'             => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id', 'required' => false)),
				'fecha'                 => new sfValidatorDate(),
				'semana'                => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'anio'                  => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'registros_nuevos'      => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'socios_registrados'    => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'socios_activos'        => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'socios_activos_nuevos' => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'cobranza_educacion'    => new sfValidatorNumber(),
				'cobranza_otros'        => new sfValidatorNumber(),
				'cobranza_internet'     => new sfValidatorNumber(),
				'cobranza_total'        => new sfValidatorNumber(),
				'created_at'            => new sfValidatorDateTime(array('required' => false)),
				'updated_at'            => new sfValidatorDateTime(array('required' => false)),
		));

		$this->widgetSchema->setNameFormat('reporte_indicadores_dia[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'ReporteIndicadoresDia';
	}


}
