<?php

/**
 * AceptacionTerminos form base class.
 *
 * @method AceptacionTerminos getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseAceptacionTerminosForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'socio_id'   => new sfWidgetFormInputHidden(),
				'ip'         => new sfWidgetFormInputText(),
				'created_at' => new sfWidgetFormDateTime(),
		));

		$this->setValidators(array(
				'socio_id'   => new sfValidatorPropelChoice(array('model' => 'Socio', 'column' => 'id', 'required' => false)),
				'ip'         => new sfValidatorString(array('max_length' => 15)),
				'created_at' => new sfValidatorDateTime(array('required' => false)),
		));

		$this->widgetSchema->setNameFormat('aceptacion_terminos[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'AceptacionTerminos';
	}


}
