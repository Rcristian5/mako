<?php

/**
 * Grupo form base class.
 *
 * @method Grupo getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseGrupoForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'                => new sfWidgetFormInputHidden(),
				'centro_id'         => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => false)),
				'curso_id'          => new sfWidgetFormPropelChoice(array('model' => 'Curso', 'add_empty' => false)),
				'aula_id'           => new sfWidgetFormPropelChoice(array('model' => 'Aula', 'add_empty' => false)),
				'horario_id'        => new sfWidgetFormPropelChoice(array('model' => 'Horario', 'add_empty' => false)),
				'facilitador_id'    => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'clave'             => new sfWidgetFormInputText(),
				'cupo_total'        => new sfWidgetFormInputText(),
				'cupo_actual'       => new sfWidgetFormInputText(),
				'cupo_alcanzado'    => new sfWidgetFormInputCheckbox(),
				'registrado_moodle' => new sfWidgetFormInputCheckbox(),
				'registrado_rs'     => new sfWidgetFormInputCheckbox(),
				'duracion'          => new sfWidgetFormInputText(),
				'duracion_perdida'  => new sfWidgetFormInputText(),
				'operador_id'       => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'activo'            => new sfWidgetFormInputCheckbox(),
				'created_at'        => new sfWidgetFormDateTime(),
				'updated_at'        => new sfWidgetFormDateTime(),
		));

		$this->setValidators(array(
				'id'                => new sfValidatorPropelChoice(array('model' => 'Grupo', 'column' => 'id', 'required' => false)),
				'centro_id'         => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id')),
				'curso_id'          => new sfValidatorPropelChoice(array('model' => 'Curso', 'column' => 'id')),
				'aula_id'           => new sfValidatorPropelChoice(array('model' => 'Aula', 'column' => 'id')),
				'horario_id'        => new sfValidatorPropelChoice(array('model' => 'Horario', 'column' => 'id')),
				'facilitador_id'    => new sfValidatorPropelChoice(array('model' => 'Usuario', 'column' => 'id', 'required' => false)),
				'clave'             => new sfValidatorString(array('max_length' => 100)),
				'cupo_total'        => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'cupo_actual'       => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'cupo_alcanzado'    => new sfValidatorBoolean(),
				'registrado_moodle' => new sfValidatorBoolean(),
				'registrado_rs'     => new sfValidatorBoolean(),
				'duracion'          => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'duracion_perdida'  => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'operador_id'       => new sfValidatorPropelChoice(array('model' => 'Usuario', 'column' => 'id', 'required' => false)),
				'activo'            => new sfValidatorBoolean(array('required' => false)),
				'created_at'        => new sfValidatorDateTime(array('required' => false)),
				'updated_at'        => new sfValidatorDateTime(array('required' => false)),
		));

		$this->widgetSchema->setNameFormat('grupo[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Grupo';
	}


}
