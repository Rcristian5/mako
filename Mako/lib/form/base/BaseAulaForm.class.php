<?php

/**
 * Aula form base class.
 *
 * @method Aula getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseAulaForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'          => new sfWidgetFormInputHidden(),
				'centro_id'   => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => false)),
				'nombre'      => new sfWidgetFormInputText(),
				'descripcion' => new sfWidgetFormTextarea(),
				'capacidad'   => new sfWidgetFormInputText(),
				'disponible'  => new sfWidgetFormInputCheckbox(),
				'activo'      => new sfWidgetFormInputCheckbox(),
				'operador_id' => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'created_at'  => new sfWidgetFormDateTime(),
				'updated_at'  => new sfWidgetFormDateTime(),
		));

		$this->setValidators(array(
				'id'          => new sfValidatorPropelChoice(array('model' => 'Aula', 'column' => 'id', 'required' => false)),
				'centro_id'   => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id')),
				'nombre'      => new sfValidatorString(array('max_length' => 255)),
				'descripcion' => new sfValidatorString(array('max_length' => 255)),
				'capacidad'   => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'disponible'  => new sfValidatorBoolean(array('required' => false)),
				'activo'      => new sfValidatorBoolean(array('required' => false)),
				'operador_id' => new sfValidatorPropelChoice(array('model' => 'Usuario', 'column' => 'id', 'required' => false)),
				'created_at'  => new sfValidatorDateTime(array('required' => false)),
				'updated_at'  => new sfValidatorDateTime(array('required' => false)),
		));

		$this->widgetSchema->setNameFormat('aula[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Aula';
	}


}
