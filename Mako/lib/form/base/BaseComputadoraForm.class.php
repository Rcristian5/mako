<?php

/**
 * Computadora form base class.
 *
 * @method Computadora getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseComputadoraForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'         => new sfWidgetFormInputHidden(),
				'centro_id'  => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'seccion_id' => new sfWidgetFormPropelChoice(array('model' => 'Seccion', 'add_empty' => true)),
				'ip'         => new sfWidgetFormInputText(),
				'hostname'   => new sfWidgetFormInputText(),
				'mac_adress' => new sfWidgetFormInputText(),
				'alias'      => new sfWidgetFormInputText(),
				'tipo_id'    => new sfWidgetFormPropelChoice(array('model' => 'TipoPc', 'add_empty' => true)),
				'activo'     => new sfWidgetFormInputCheckbox(),
				'created_at' => new sfWidgetFormDateTime(),
				'updated_at' => new sfWidgetFormDateTime(),
		));

		$this->setValidators(array(
				'id'         => new sfValidatorPropelChoice(array('model' => 'Computadora', 'column' => 'id', 'required' => false)),
				'centro_id'  => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id', 'required' => false)),
				'seccion_id' => new sfValidatorPropelChoice(array('model' => 'Seccion', 'column' => 'id', 'required' => false)),
				'ip'         => new sfValidatorString(array('max_length' => 15)),
				'hostname'   => new sfValidatorString(array('max_length' => 90, 'required' => false)),
				'mac_adress' => new sfValidatorString(array('max_length' => 17, 'required' => false)),
				'alias'      => new sfValidatorString(array('max_length' => 150, 'required' => false)),
				'tipo_id'    => new sfValidatorPropelChoice(array('model' => 'TipoPc', 'column' => 'id', 'required' => false)),
				'activo'     => new sfValidatorBoolean(array('required' => false)),
				'created_at' => new sfValidatorDateTime(array('required' => false)),
				'updated_at' => new sfValidatorDateTime(array('required' => false)),
		));

		$this->widgetSchema->setNameFormat('computadora[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Computadora';
	}


}
