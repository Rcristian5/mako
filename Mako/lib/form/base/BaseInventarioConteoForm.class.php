<?php

/**
 * InventarioConteo form base class.
 *
 * @method InventarioConteo getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseInventarioConteoForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'             => new sfWidgetFormInputHidden(),
				'corte_id'       => new sfWidgetFormPropelChoice(array('model' => 'InventarioCorte', 'add_empty' => true)),
				'operacion_id'   => new sfWidgetFormPropelChoice(array('model' => 'InventarioOperacion', 'add_empty' => true)),
				'producto_id'    => new sfWidgetFormPropelChoice(array('model' => 'Producto', 'add_empty' => false)),
				'cantidad_final' => new sfWidgetFormInputText(),
				'conteo1'        => new sfWidgetFormInputText(),
				'conteo2'        => new sfWidgetFormInputText(),
				'centro_id'      => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => false)),
				'created_at'     => new sfWidgetFormDateTime(),
		));

		$this->setValidators(array(
				'id'             => new sfValidatorPropelChoice(array('model' => 'InventarioConteo', 'column' => 'id', 'required' => false)),
				'corte_id'       => new sfValidatorPropelChoice(array('model' => 'InventarioCorte', 'column' => 'id', 'required' => false)),
				'operacion_id'   => new sfValidatorPropelChoice(array('model' => 'InventarioOperacion', 'column' => 'id', 'required' => false)),
				'producto_id'    => new sfValidatorPropelChoice(array('model' => 'Producto', 'column' => 'id')),
				'cantidad_final' => new sfValidatorInteger(array('min' => -9.2233720368548E+18, 'max' => 9.2233720368548E+18)),
				'conteo1'        => new sfValidatorInteger(array('min' => -9.2233720368548E+18, 'max' => 9.2233720368548E+18)),
				'conteo2'        => new sfValidatorInteger(array('min' => -9.2233720368548E+18, 'max' => 9.2233720368548E+18)),
				'centro_id'      => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id')),
				'created_at'     => new sfValidatorDateTime(array('required' => false)),
		));

		$this->widgetSchema->setNameFormat('inventario_conteo[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'InventarioConteo';
	}


}
