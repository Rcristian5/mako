<?php

/**
 * ReporteIndicadoresDiaGrupoSociosActivos form base class.
 *
 * @method ReporteIndicadoresDiaGrupoSociosActivos getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseReporteIndicadoresDiaGrupoSociosActivosForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'                             => new sfWidgetFormInputHidden(),
				'reporte_indicador_dia_grupo_id' => new sfWidgetFormPropelChoice(array('model' => 'ReporteIndicadoresDiaGrupo', 'add_empty' => false)),
				'socio_id'                       => new sfWidgetFormInputText(),
		));

		$this->setValidators(array(
				'id'                             => new sfValidatorPropelChoice(array('model' => 'ReporteIndicadoresDiaGrupoSociosActivos', 'column' => 'id', 'required' => false)),
				'reporte_indicador_dia_grupo_id' => new sfValidatorPropelChoice(array('model' => 'ReporteIndicadoresDiaGrupo', 'column' => 'id')),
				'socio_id'                       => new sfValidatorInteger(array('min' => -9.2233720368548E+18, 'max' => 9.2233720368548E+18)),
		));

		$this->widgetSchema->setNameFormat('reporte_indicadores_dia_grupo_socios_activos[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'ReporteIndicadoresDiaGrupoSociosActivos';
	}


}
