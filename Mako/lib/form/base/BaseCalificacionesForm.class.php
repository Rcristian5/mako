<?php

/**
 * Calificaciones form base class.
 *
 * @method Calificaciones getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCalificacionesForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'            => new sfWidgetFormInputHidden(),
				'curso_id'      => new sfWidgetFormPropelChoice(array('model' => 'Curso', 'add_empty' => false)),
				'grupo_id'      => new sfWidgetFormPropelChoice(array('model' => 'Grupo', 'add_empty' => false)),
				'evaluacion_id' => new sfWidgetFormPropelChoice(array('model' => 'Evaluacion', 'add_empty' => true)),
				'calificacion'  => new sfWidgetFormInputText(),
				'alumno_id'     => new sfWidgetFormPropelChoice(array('model' => 'Alumnos', 'add_empty' => true, 'key_method' => 'getSocioId')),
				'operador_id'   => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'created_at'    => new sfWidgetFormDateTime(),
				'updated_at'    => new sfWidgetFormDateTime(),
		));

		$this->setValidators(array(
				'id'            => new sfValidatorPropelChoice(array('model' => 'Calificaciones', 'column' => 'id', 'required' => false)),
				'curso_id'      => new sfValidatorPropelChoice(array('model' => 'Curso', 'column' => 'id')),
				'grupo_id'      => new sfValidatorPropelChoice(array('model' => 'Grupo', 'column' => 'id')),
				'evaluacion_id' => new sfValidatorPropelChoice(array('model' => 'Evaluacion', 'column' => 'id', 'required' => false)),
				'calificacion'  => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'alumno_id'     => new sfValidatorPropelChoice(array('model' => 'Alumnos', 'column' => 'socio_id', 'required' => false)),
				'operador_id'   => new sfValidatorPropelChoice(array('model' => 'Usuario', 'column' => 'id', 'required' => false)),
				'created_at'    => new sfValidatorDateTime(array('required' => false)),
				'updated_at'    => new sfValidatorDateTime(array('required' => false)),
		));

		$this->widgetSchema->setNameFormat('calificaciones[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Calificaciones';
	}


}
