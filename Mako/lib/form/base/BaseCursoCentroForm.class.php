<?php

/**
 * CursoCentro form base class.
 *
 * @method CursoCentro getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCursoCentroForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'curso_id'  => new sfWidgetFormInputHidden(),
				'centro_id' => new sfWidgetFormInputHidden(),
		));

		$this->setValidators(array(
				'curso_id'  => new sfValidatorPropelChoice(array('model' => 'Curso', 'column' => 'id', 'required' => false)),
				'centro_id' => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id', 'required' => false)),
		));

		$this->widgetSchema->setNameFormat('curso_centro[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'CursoCentro';
	}


}
