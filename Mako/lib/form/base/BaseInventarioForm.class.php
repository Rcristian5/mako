<?php

/**
 * Inventario form base class.
 *
 * @method Inventario getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseInventarioForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'           => new sfWidgetFormInputHidden(),
				'operacion_id' => new sfWidgetFormPropelChoice(array('model' => 'InventarioOperacion', 'add_empty' => true)),
				'producto_id'  => new sfWidgetFormPropelChoice(array('model' => 'Producto', 'add_empty' => false)),
				'cantidad'     => new sfWidgetFormInputText(),
				'centro_id'    => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => false)),
				'motivo'       => new sfWidgetFormTextarea(),
				'created_at'   => new sfWidgetFormDateTime(),
		));

		$this->setValidators(array(
				'id'           => new sfValidatorPropelChoice(array('model' => 'Inventario', 'column' => 'id', 'required' => false)),
				'operacion_id' => new sfValidatorPropelChoice(array('model' => 'InventarioOperacion', 'column' => 'id', 'required' => false)),
				'producto_id'  => new sfValidatorPropelChoice(array('model' => 'Producto', 'column' => 'id')),
				'cantidad'     => new sfValidatorInteger(array('min' => -9.2233720368548E+18, 'max' => 9.2233720368548E+18)),
				'centro_id'    => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id')),
				'motivo'       => new sfValidatorString(array('required' => false)),
				'created_at'   => new sfValidatorDateTime(array('required' => false)),
		));

		$this->widgetSchema->setNameFormat('inventario[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Inventario';
	}


}
