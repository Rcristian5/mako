<?php

/**
 * Corte form base class.
 *
 * @method Corte getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCorteForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'                   => new sfWidgetFormInputHidden(),
				'centro_id'            => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'folio_venta_inicial'  => new sfWidgetFormInputText(),
				'folio_venta_final'    => new sfWidgetFormInputText(),
				'fondo_inicial'        => new sfWidgetFormInputText(),
				'fondo_caja'           => new sfWidgetFormInputText(),
				'operador_id'          => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'total_ventas'         => new sfWidgetFormInputText(),
				'total_salidas'        => new sfWidgetFormInputText(),
				'total_cancelaciones'  => new sfWidgetFormInputText(),
				'total_vpg'            => new sfWidgetFormInputText(),
				'total_facturas'       => new sfWidgetFormInputText(),
				'total_conciliaciones' => new sfWidgetFormInputText(),
				'factura_id'           => new sfWidgetFormPropelChoice(array('model' => 'Factura', 'add_empty' => true)),
				'ip_caja'              => new sfWidgetFormInputText(),
				'created_at'           => new sfWidgetFormDateTime(),
				'updated_at'           => new sfWidgetFormDateTime(),
		));

		$this->setValidators(array(
				'id'                   => new sfValidatorPropelChoice(array('model' => 'Corte', 'column' => 'id', 'required' => false)),
				'centro_id'            => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id', 'required' => false)),
				'folio_venta_inicial'  => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
				'folio_venta_final'    => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
				'fondo_inicial'        => new sfValidatorNumber(array('required' => false)),
				'fondo_caja'           => new sfValidatorNumber(array('required' => false)),
				'operador_id'          => new sfValidatorPropelChoice(array('model' => 'Usuario', 'column' => 'id', 'required' => false)),
				'total_ventas'         => new sfValidatorNumber(array('required' => false)),
				'total_salidas'        => new sfValidatorNumber(array('required' => false)),
				'total_cancelaciones'  => new sfValidatorNumber(array('required' => false)),
				'total_vpg'            => new sfValidatorNumber(array('required' => false)),
				'total_facturas'       => new sfValidatorNumber(array('required' => false)),
				'total_conciliaciones' => new sfValidatorNumber(array('required' => false)),
				'factura_id'           => new sfValidatorPropelChoice(array('model' => 'Factura', 'column' => 'id', 'required' => false)),
				'ip_caja'              => new sfValidatorString(array('max_length' => 15, 'required' => false)),
				'created_at'           => new sfValidatorDateTime(array('required' => false)),
				'updated_at'           => new sfValidatorDateTime(array('required' => false)),
		));

		$this->widgetSchema->setNameFormat('corte[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Corte';
	}


}
