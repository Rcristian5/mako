<?php

/**
 * Capitulo form base class.
 *
 * @method Capitulo getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCapituloForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'          => new sfWidgetFormInputHidden(),
				'curso_id'    => new sfWidgetFormPropelChoice(array('model' => 'Curso', 'add_empty' => false)),
				'numero'      => new sfWidgetFormInputText(),
				'nombre'      => new sfWidgetFormInputText(),
				'duracion'    => new sfWidgetFormInputText(),
				'descripcion' => new sfWidgetFormTextarea(),
				'objetivo'    => new sfWidgetFormTextarea(),
				'activo'      => new sfWidgetFormInputCheckbox(),
				'operador_id' => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'created_at'  => new sfWidgetFormDateTime(),
				'updated_at'  => new sfWidgetFormDateTime(),
		));

		$this->setValidators(array(
				'id'          => new sfValidatorPropelChoice(array('model' => 'Capitulo', 'column' => 'id', 'required' => false)),
				'curso_id'    => new sfValidatorPropelChoice(array('model' => 'Curso', 'column' => 'id')),
				'numero'      => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'nombre'      => new sfValidatorString(array('max_length' => 255)),
				'duracion'    => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'descripcion' => new sfValidatorString(),
				'objetivo'    => new sfValidatorString(),
				'activo'      => new sfValidatorBoolean(array('required' => false)),
				'operador_id' => new sfValidatorPropelChoice(array('model' => 'Usuario', 'column' => 'id', 'required' => false)),
				'created_at'  => new sfValidatorDateTime(array('required' => false)),
				'updated_at'  => new sfValidatorDateTime(array('required' => false)),
		));

		$this->widgetSchema->setNameFormat('capitulo[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Capitulo';
	}


}
