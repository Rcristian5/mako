<?php

/**
 * Vacaciones form base class.
 *
 * @method Vacaciones getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseVacacionesForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'           => new sfWidgetFormInputHidden(),
				'usuario_id'   => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'fecha_inicio' => new sfWidgetFormDate(),
				'fecha_fin'    => new sfWidgetFormDate(),
				'operador_id'  => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'created_at'   => new sfWidgetFormDateTime(),
				'updated_at'   => new sfWidgetFormDateTime(),
		));

		$this->setValidators(array(
				'id'           => new sfValidatorPropelChoice(array('model' => 'Vacaciones', 'column' => 'id', 'required' => false)),
				'usuario_id'   => new sfValidatorPropelChoice(array('model' => 'Usuario', 'column' => 'id', 'required' => false)),
				'fecha_inicio' => new sfValidatorDate(),
				'fecha_fin'    => new sfValidatorDate(),
				'operador_id'  => new sfValidatorPropelChoice(array('model' => 'Usuario', 'column' => 'id', 'required' => false)),
				'created_at'   => new sfValidatorDateTime(array('required' => false)),
				'updated_at'   => new sfValidatorDateTime(array('required' => false)),
		));

		$this->widgetSchema->setNameFormat('vacaciones[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Vacaciones';
	}


}
