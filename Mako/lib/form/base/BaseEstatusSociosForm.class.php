<?php

/**
 * EstatusSocios form base class.
 *
 * @method EstatusSocios getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseEstatusSociosForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'centro_id' => new sfWidgetFormInputHidden(),
				'fecha'     => new sfWidgetFormInputHidden(),
				'nuevos'    => new sfWidgetFormInputText(),
				'activos'   => new sfWidgetFormInputText(),
		));

		$this->setValidators(array(
				'centro_id' => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id', 'required' => false)),
				'fecha'     => new sfValidatorPropelChoice(array('model' => 'EstatusSocios', 'column' => 'fecha', 'required' => false)),
				'nuevos'    => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'activos'   => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
		));

		$this->widgetSchema->setNameFormat('estatus_socios[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'EstatusSocios';
	}


}
