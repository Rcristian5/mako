<?php

/**
 * PromocionRegla form base class.
 *
 * @method PromocionRegla getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePromocionReglaForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'                       => new sfWidgetFormInputHidden(),
				'promocion_id'             => new sfWidgetFormPropelChoice(array('model' => 'Promocion', 'add_empty' => true)),
				'sexo'                     => new sfWidgetFormInputText(),
				'edad_de'                  => new sfWidgetFormInputText(),
				'edad_a'                   => new sfWidgetFormInputText(),
				'nivel_estudio'            => new sfWidgetFormPropelChoice(array('model' => 'NivelEstudio', 'add_empty' => true)),
				'recomendados_de'          => new sfWidgetFormInputText(),
				'recomendados_a'           => new sfWidgetFormInputText(),
				'centro_alta_id'           => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'centro_venta_id'          => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'municipio'                => new sfWidgetFormInputText(),
				'colonia'                  => new sfWidgetFormInputText(),
				'cp'                       => new sfWidgetFormInputText(),
				'discapacidad_id'          => new sfWidgetFormPropelChoice(array('model' => 'Discapacidad', 'add_empty' => true)),
				'habilidad_informatica_id' => new sfWidgetFormPropelChoice(array('model' => 'HabilidadInformatica', 'add_empty' => true)),
				'dominio_ingles_id'        => new sfWidgetFormPropelChoice(array('model' => 'DominioIngles', 'add_empty' => true)),
				'profesion_id'             => new sfWidgetFormPropelChoice(array('model' => 'Profesion', 'add_empty' => true)),
				'ocupacion_id'             => new sfWidgetFormPropelChoice(array('model' => 'Ocupacion', 'add_empty' => true)),
				'posicion_id'              => new sfWidgetFormPropelChoice(array('model' => 'Posicion', 'add_empty' => true)),
				'estudia'                  => new sfWidgetFormInputCheckbox(),
				'trabaja'                  => new sfWidgetFormInputCheckbox(),
				'created_at'               => new sfWidgetFormDateTime(),
				'updated_at'               => new sfWidgetFormDateTime(),
				'beca_id'                  => new sfWidgetFormPropelChoice(array('model' => 'Beca', 'add_empty' => true)),
		));

		$this->setValidators(array(
				'id'                       => new sfValidatorPropelChoice(array('model' => 'PromocionRegla', 'column' => 'id', 'required' => false)),
				'promocion_id'             => new sfValidatorPropelChoice(array('model' => 'Promocion', 'column' => 'id', 'required' => false)),
				'sexo'                     => new sfValidatorString(array('max_length' => 1, 'required' => false)),
				'edad_de'                  => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
				'edad_a'                   => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
				'nivel_estudio'            => new sfValidatorPropelChoice(array('model' => 'NivelEstudio', 'column' => 'id', 'required' => false)),
				'recomendados_de'          => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
				'recomendados_a'           => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
				'centro_alta_id'           => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id', 'required' => false)),
				'centro_venta_id'          => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id', 'required' => false)),
				'municipio'                => new sfValidatorString(array('max_length' => 256, 'required' => false)),
				'colonia'                  => new sfValidatorString(array('max_length' => 256, 'required' => false)),
				'cp'                       => new sfValidatorString(array('max_length' => 256, 'required' => false)),
				'discapacidad_id'          => new sfValidatorPropelChoice(array('model' => 'Discapacidad', 'column' => 'id', 'required' => false)),
				'habilidad_informatica_id' => new sfValidatorPropelChoice(array('model' => 'HabilidadInformatica', 'column' => 'id', 'required' => false)),
				'dominio_ingles_id'        => new sfValidatorPropelChoice(array('model' => 'DominioIngles', 'column' => 'id', 'required' => false)),
				'profesion_id'             => new sfValidatorPropelChoice(array('model' => 'Profesion', 'column' => 'id', 'required' => false)),
				'ocupacion_id'             => new sfValidatorPropelChoice(array('model' => 'Ocupacion', 'column' => 'id', 'required' => false)),
				'posicion_id'              => new sfValidatorPropelChoice(array('model' => 'Posicion', 'column' => 'id', 'required' => false)),
				'estudia'                  => new sfValidatorBoolean(array('required' => false)),
				'trabaja'                  => new sfValidatorBoolean(array('required' => false)),
				'created_at'               => new sfValidatorDateTime(array('required' => false)),
				'updated_at'               => new sfValidatorDateTime(array('required' => false)),
				'beca_id'                  => new sfValidatorPropelChoice(array('model' => 'Beca', 'column' => 'id', 'required' => false)),
		));

		$this->widgetSchema->setNameFormat('promocion_regla[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'PromocionRegla';
	}


}
