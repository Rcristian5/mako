<?php

/**
 * Producto form base class.
 *
 * @method Producto getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseProductoForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'                              => new sfWidgetFormInputHidden(),
				'categoria_id'                    => new sfWidgetFormPropelChoice(array('model' => 'CategoriaProducto', 'add_empty' => false)),
				'tipo_id'                         => new sfWidgetFormPropelChoice(array('model' => 'TipoProducto', 'add_empty' => false)),
				'unidad_id'                       => new sfWidgetFormPropelChoice(array('model' => 'UnidadProducto', 'add_empty' => false)),
				'codigo'                          => new sfWidgetFormInputText(),
				'nombre'                          => new sfWidgetFormInputText(),
				'descripcion'                     => new sfWidgetFormTextarea(),
				'precio_lista'                    => new sfWidgetFormInputText(),
				'tiempo'                          => new sfWidgetFormInputText(),
				'venta_unitaria'                  => new sfWidgetFormInputCheckbox(),
				'en_pv'                           => new sfWidgetFormInputCheckbox(),
				'es_subproducto'                  => new sfWidgetFormInputCheckbox(),
				'activo'                          => new sfWidgetFormInputCheckbox(),
				'created_at'                      => new sfWidgetFormDateTime(),
				'updated_at'                      => new sfWidgetFormDateTime(),
				'stock_list'                      => new sfWidgetFormPropelChoice(array('multiple' => true, 'model' => 'Centro')),
				'modalidad_cobro_producto_list'   => new sfWidgetFormPropelChoice(array('multiple' => true, 'model' => 'TipoCobro')),
				'subproducto_cobranza_list'       => new sfWidgetFormPropelChoice(array('multiple' => true, 'model' => 'TipoCobro')),
				'promocion_cupon_producto_list'   => new sfWidgetFormPropelChoice(array('multiple' => true, 'model' => 'PromocionCupon')),
				'promocion_producto_list'         => new sfWidgetFormPropelChoice(array('multiple' => true, 'model' => 'Promocion')),
				'categoria_reporte_producto_list' => new sfWidgetFormPropelChoice(array('multiple' => true, 'model' => 'CategoriaReporte')),
				'producto_curso_list'             => new sfWidgetFormPropelChoice(array('multiple' => true, 'model' => 'Curso')),
		));

		$this->setValidators(array(
				'id'                              => new sfValidatorPropelChoice(array('model' => 'Producto', 'column' => 'id', 'required' => false)),
				'categoria_id'                    => new sfValidatorPropelChoice(array('model' => 'CategoriaProducto', 'column' => 'id')),
				'tipo_id'                         => new sfValidatorPropelChoice(array('model' => 'TipoProducto', 'column' => 'id')),
				'unidad_id'                       => new sfValidatorPropelChoice(array('model' => 'UnidadProducto', 'column' => 'id')),
				'codigo'                          => new sfValidatorString(array('max_length' => 250, 'required' => false)),
				'nombre'                          => new sfValidatorString(array('max_length' => 250, 'required' => false)),
				'descripcion'                     => new sfValidatorString(array('required' => false)),
				'precio_lista'                    => new sfValidatorNumber(array('required' => false)),
				'tiempo'                          => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
				'venta_unitaria'                  => new sfValidatorBoolean(array('required' => false)),
				'en_pv'                           => new sfValidatorBoolean(array('required' => false)),
				'es_subproducto'                  => new sfValidatorBoolean(array('required' => false)),
				'activo'                          => new sfValidatorBoolean(array('required' => false)),
				'created_at'                      => new sfValidatorDateTime(array('required' => false)),
				'updated_at'                      => new sfValidatorDateTime(array('required' => false)),
				'stock_list'                      => new sfValidatorPropelChoice(array('multiple' => true, 'model' => 'Centro', 'required' => false)),
				'modalidad_cobro_producto_list'   => new sfValidatorPropelChoice(array('multiple' => true, 'model' => 'TipoCobro', 'required' => false)),
				'subproducto_cobranza_list'       => new sfValidatorPropelChoice(array('multiple' => true, 'model' => 'TipoCobro', 'required' => false)),
				'promocion_cupon_producto_list'   => new sfValidatorPropelChoice(array('multiple' => true, 'model' => 'PromocionCupon', 'required' => false)),
				'promocion_producto_list'         => new sfValidatorPropelChoice(array('multiple' => true, 'model' => 'Promocion', 'required' => false)),
				'categoria_reporte_producto_list' => new sfValidatorPropelChoice(array('multiple' => true, 'model' => 'CategoriaReporte', 'required' => false)),
				'producto_curso_list'             => new sfValidatorPropelChoice(array('multiple' => true, 'model' => 'Curso', 'required' => false)),
		));

		$this->widgetSchema->setNameFormat('producto[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Producto';
	}


	public function updateDefaultsFromObject()
	{
		parent::updateDefaultsFromObject();

		if (isset($this->widgetSchema['stock_list']))
		{
			$values = array();
			foreach ($this->object->getStocks() as $obj)
			{
				$values[] = $obj->getCentroId();
			}

			$this->setDefault('stock_list', $values);
		}

		if (isset($this->widgetSchema['modalidad_cobro_producto_list']))
		{
			$values = array();
			foreach ($this->object->getModalidadCobroProductos() as $obj)
			{
				$values[] = $obj->getCobroId();
			}

			$this->setDefault('modalidad_cobro_producto_list', $values);
		}

		if (isset($this->widgetSchema['subproducto_cobranza_list']))
		{
			$values = array();
			foreach ($this->object->getSubproductoCobranzas() as $obj)
			{
				$values[] = $obj->getCobroId();
			}

			$this->setDefault('subproducto_cobranza_list', $values);
		}

		if (isset($this->widgetSchema['promocion_cupon_producto_list']))
		{
			$values = array();
			foreach ($this->object->getPromocionCuponProductos() as $obj)
			{
				$values[] = $obj->getPromocionId();
			}

			$this->setDefault('promocion_cupon_producto_list', $values);
		}

		if (isset($this->widgetSchema['promocion_producto_list']))
		{
			$values = array();
			foreach ($this->object->getPromocionProductos() as $obj)
			{
				$values[] = $obj->getPromocionId();
			}

			$this->setDefault('promocion_producto_list', $values);
		}

		if (isset($this->widgetSchema['categoria_reporte_producto_list']))
		{
			$values = array();
			foreach ($this->object->getCategoriaReporteProductos() as $obj)
			{
				$values[] = $obj->getCategoriaId();
			}

			$this->setDefault('categoria_reporte_producto_list', $values);
		}

		if (isset($this->widgetSchema['producto_curso_list']))
		{
			$values = array();
			foreach ($this->object->getProductoCursos() as $obj)
			{
				$values[] = $obj->getCursoId();
			}

			$this->setDefault('producto_curso_list', $values);
		}

	}

	protected function doSave($con = null)
	{
		parent::doSave($con);

		$this->saveStockList($con);
		$this->saveModalidadCobroProductoList($con);
		$this->saveSubproductoCobranzaList($con);
		$this->savePromocionCuponProductoList($con);
		$this->savePromocionProductoList($con);
		$this->saveCategoriaReporteProductoList($con);
		$this->saveProductoCursoList($con);
	}

	public function saveStockList($con = null)
	{
		if (!$this->isValid())
		{
			throw $this->getErrorSchema();
		}

		if (!isset($this->widgetSchema['stock_list']))
		{
			// somebody has unset this widget
			return;
		}

		if (null === $con)
		{
			$con = $this->getConnection();
		}

		$c = new Criteria();
		$c->add(StockPeer::PRODUCTO_ID, $this->object->getPrimaryKey());
		StockPeer::doDelete($c, $con);

		$values = $this->getValue('stock_list');
		if (is_array($values))
		{
			foreach ($values as $value)
			{
				$obj = new Stock();
				$obj->setProductoId($this->object->getPrimaryKey());
				$obj->setCentroId($value);
				$obj->save();
			}
		}
	}

	public function saveModalidadCobroProductoList($con = null)
	{
		if (!$this->isValid())
		{
			throw $this->getErrorSchema();
		}

		if (!isset($this->widgetSchema['modalidad_cobro_producto_list']))
		{
			// somebody has unset this widget
			return;
		}

		if (null === $con)
		{
			$con = $this->getConnection();
		}

		$c = new Criteria();
		$c->add(ModalidadCobroProductoPeer::PRODUCTO_ID, $this->object->getPrimaryKey());
		ModalidadCobroProductoPeer::doDelete($c, $con);

		$values = $this->getValue('modalidad_cobro_producto_list');
		if (is_array($values))
		{
			foreach ($values as $value)
			{
				$obj = new ModalidadCobroProducto();
				$obj->setProductoId($this->object->getPrimaryKey());
				$obj->setCobroId($value);
				$obj->save();
			}
		}
	}

	public function saveSubproductoCobranzaList($con = null)
	{
		if (!$this->isValid())
		{
			throw $this->getErrorSchema();
		}

		if (!isset($this->widgetSchema['subproducto_cobranza_list']))
		{
			// somebody has unset this widget
			return;
		}

		if (null === $con)
		{
			$con = $this->getConnection();
		}

		$c = new Criteria();
		$c->add(SubproductoCobranzaPeer::PRODUCTO_ID, $this->object->getPrimaryKey());
		SubproductoCobranzaPeer::doDelete($c, $con);

		$values = $this->getValue('subproducto_cobranza_list');
		if (is_array($values))
		{
			foreach ($values as $value)
			{
				$obj = new SubproductoCobranza();
				$obj->setProductoId($this->object->getPrimaryKey());
				$obj->setCobroId($value);
				$obj->save();
			}
		}
	}

	public function savePromocionCuponProductoList($con = null)
	{
		if (!$this->isValid())
		{
			throw $this->getErrorSchema();
		}

		if (!isset($this->widgetSchema['promocion_cupon_producto_list']))
		{
			// somebody has unset this widget
			return;
		}

		if (null === $con)
		{
			$con = $this->getConnection();
		}

		$c = new Criteria();
		$c->add(PromocionCuponProductoPeer::PRODUCTO_ID, $this->object->getPrimaryKey());
		PromocionCuponProductoPeer::doDelete($c, $con);

		$values = $this->getValue('promocion_cupon_producto_list');
		if (is_array($values))
		{
			foreach ($values as $value)
			{
				$obj = new PromocionCuponProducto();
				$obj->setProductoId($this->object->getPrimaryKey());
				$obj->setPromocionId($value);
				$obj->save();
			}
		}
	}

	public function savePromocionProductoList($con = null)
	{
		if (!$this->isValid())
		{
			throw $this->getErrorSchema();
		}

		if (!isset($this->widgetSchema['promocion_producto_list']))
		{
			// somebody has unset this widget
			return;
		}

		if (null === $con)
		{
			$con = $this->getConnection();
		}

		$c = new Criteria();
		$c->add(PromocionProductoPeer::PRODUCTO_ID, $this->object->getPrimaryKey());
		PromocionProductoPeer::doDelete($c, $con);

		$values = $this->getValue('promocion_producto_list');
		if (is_array($values))
		{
			foreach ($values as $value)
			{
				$obj = new PromocionProducto();
				$obj->setProductoId($this->object->getPrimaryKey());
				$obj->setPromocionId($value);
				$obj->save();
			}
		}
	}

	public function saveCategoriaReporteProductoList($con = null)
	{
		if (!$this->isValid())
		{
			throw $this->getErrorSchema();
		}

		if (!isset($this->widgetSchema['categoria_reporte_producto_list']))
		{
			// somebody has unset this widget
			return;
		}

		if (null === $con)
		{
			$con = $this->getConnection();
		}

		$c = new Criteria();
		$c->add(CategoriaReporteProductoPeer::PRODUCTO_ID, $this->object->getPrimaryKey());
		CategoriaReporteProductoPeer::doDelete($c, $con);

		$values = $this->getValue('categoria_reporte_producto_list');
		if (is_array($values))
		{
			foreach ($values as $value)
			{
				$obj = new CategoriaReporteProducto();
				$obj->setProductoId($this->object->getPrimaryKey());
				$obj->setCategoriaId($value);
				$obj->save();
			}
		}
	}

	public function saveProductoCursoList($con = null)
	{
		if (!$this->isValid())
		{
			throw $this->getErrorSchema();
		}

		if (!isset($this->widgetSchema['producto_curso_list']))
		{
			// somebody has unset this widget
			return;
		}

		if (null === $con)
		{
			$con = $this->getConnection();
		}

		$c = new Criteria();
		$c->add(ProductoCursoPeer::PRODUCTO_ID, $this->object->getPrimaryKey());
		ProductoCursoPeer::doDelete($c, $con);

		$values = $this->getValue('producto_curso_list');
		if (is_array($values))
		{
			foreach ($values as $value)
			{
				$obj = new ProductoCurso();
				$obj->setProductoId($this->object->getPrimaryKey());
				$obj->setCursoId($value);
				$obj->save();
			}
		}
	}

}
