<?php

/**
 * InventarioOperacion form base class.
 *
 * @method InventarioOperacion getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseInventarioOperacionForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'        => new sfWidgetFormInputHidden(),
				'operacion' => new sfWidgetFormInputText(),
		));

		$this->setValidators(array(
				'id'        => new sfValidatorPropelChoice(array('model' => 'InventarioOperacion', 'column' => 'id', 'required' => false)),
				'operacion' => new sfValidatorString(array('max_length' => 30, 'required' => false)),
		));

		$this->widgetSchema->setNameFormat('inventario_operacion[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'InventarioOperacion';
	}


}
