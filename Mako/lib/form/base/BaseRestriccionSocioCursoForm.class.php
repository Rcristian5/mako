<?php

/**
 * RestriccionSocioCurso form base class.
 *
 * @method RestriccionSocioCurso getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseRestriccionSocioCursoForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'restriccion_socio_id' => new sfWidgetFormInputHidden(),
				'curso_id'             => new sfWidgetFormInputHidden(),
		));

		$this->setValidators(array(
				'restriccion_socio_id' => new sfValidatorPropelChoice(array('model' => 'RestriccionesSocio', 'column' => 'id', 'required' => false)),
				'curso_id'             => new sfValidatorPropelChoice(array('model' => 'Curso', 'column' => 'id', 'required' => false)),
		));

		$this->widgetSchema->setNameFormat('restriccion_socio_curso[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'RestriccionSocioCurso';
	}


}
