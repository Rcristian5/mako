<?php

/**
 * ReporteCategoriaCursos form base class.
 *
 * @method ReporteCategoriaCursos getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseReporteCategoriaCursosForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'centro_id'        => new sfWidgetFormInputHidden(),
				'fecha'            => new sfWidgetFormInputHidden(),
				'semana'           => new sfWidgetFormInputText(),
				'anio'             => new sfWidgetFormInputText(),
				'monto'            => new sfWidgetFormInputText(),
				'cantidad'         => new sfWidgetFormInputText(),
				'categoria_id'     => new sfWidgetFormInputHidden(),
				'siglas'           => new sfWidgetFormInputText(),
				'categoria_nombre' => new sfWidgetFormInputText(),
		));

		$this->setValidators(array(
				'centro_id'        => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id', 'required' => false)),
				'fecha'            => new sfValidatorPropelChoice(array('model' => 'ReporteCategoriaCursos', 'column' => 'fecha', 'required' => false)),
				'semana'           => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'anio'             => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'monto'            => new sfValidatorNumber(),
				'cantidad'         => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'categoria_id'     => new sfValidatorPropelChoice(array('model' => 'ReporteCategoriaCursos', 'column' => 'categoria_id', 'required' => false)),
				'siglas'           => new sfValidatorString(array('max_length' => 60, 'required' => false)),
				'categoria_nombre' => new sfValidatorString(array('max_length' => 120, 'required' => false)),
		));

		$this->widgetSchema->setNameFormat('reporte_categoria_cursos[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'ReporteCategoriaCursos';
	}


}
