<?php

/**
 * RestriccionesHorario form base class.
 *
 * @method RestriccionesHorario getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseRestriccionesHorarioForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'          => new sfWidgetFormInputHidden(),
				'curso_id'    => new sfWidgetFormPropelChoice(array('model' => 'Curso', 'add_empty' => false)),
				'hora_inicio' => new sfWidgetFormTime(),
				'hora_fin'    => new sfWidgetFormTime(),
				'activo'      => new sfWidgetFormInputCheckbox(),
				'created_at'  => new sfWidgetFormDateTime(),
				'updated_at'  => new sfWidgetFormDateTime(),
		));

		$this->setValidators(array(
				'id'          => new sfValidatorPropelChoice(array('model' => 'RestriccionesHorario', 'column' => 'id', 'required' => false)),
				'curso_id'    => new sfValidatorPropelChoice(array('model' => 'Curso', 'column' => 'id')),
				'hora_inicio' => new sfValidatorTime(array('required' => false)),
				'hora_fin'    => new sfValidatorTime(array('required' => false)),
				'activo'      => new sfValidatorBoolean(array('required' => false)),
				'created_at'  => new sfValidatorDateTime(array('required' => false)),
				'updated_at'  => new sfValidatorDateTime(array('required' => false)),
		));

		$this->widgetSchema->setNameFormat('restricciones_horario[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'RestriccionesHorario';
	}


}
