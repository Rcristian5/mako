<?php

/**
 * RecursosAsignados form base class.
 *
 * @method RecursosAsignados getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseRecursosAsignadosForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'curso_id'   => new sfWidgetFormInputHidden(),
				'recurso_id' => new sfWidgetFormInputHidden(),
				'cantidad'   => new sfWidgetFormInputText(),
		));

		$this->setValidators(array(
				'curso_id'   => new sfValidatorPropelChoice(array('model' => 'Curso', 'column' => 'id', 'required' => false)),
				'recurso_id' => new sfValidatorPropelChoice(array('model' => 'Recurso', 'column' => 'id', 'required' => false)),
				'cantidad'   => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
		));

		$this->widgetSchema->setNameFormat('recursos_asignados[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'RecursosAsignados';
	}


}
