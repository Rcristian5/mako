<?php

/**
 * FolioVenta form base class.
 *
 * @method FolioVenta getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseFolioVentaForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'centro_id' => new sfWidgetFormInputHidden(),
				'folio'     => new sfWidgetFormInputText(),
		));

		$this->setValidators(array(
				'centro_id' => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id', 'required' => false)),
				'folio'     => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
		));

		$this->widgetSchema->setNameFormat('folio_venta[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'FolioVenta';
	}


}
