<?php

/**
 * Paquete form base class.
 *
 * @method Paquete getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePaqueteForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'paquete_id'     => new sfWidgetFormInputHidden(),
				'producto_id'    => new sfWidgetFormInputHidden(),
				'cantidad'       => new sfWidgetFormInputText(),
				'precio_paquete' => new sfWidgetFormInputText(),
		));

		$this->setValidators(array(
				'paquete_id'     => new sfValidatorPropelChoice(array('model' => 'Producto', 'column' => 'id', 'required' => false)),
				'producto_id'    => new sfValidatorPropelChoice(array('model' => 'Producto', 'column' => 'id', 'required' => false)),
				'cantidad'       => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
				'precio_paquete' => new sfValidatorNumber(array('required' => false)),
		));

		$this->widgetSchema->setNameFormat('paquete[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Paquete';
	}


}
