<?php

/**
 * Metas form base class.
 *
 * @method Metas getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseMetasForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'                    => new sfWidgetFormInputHidden(),
				'centro_id'             => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => false)),
				'categoria_fija_id'     => new sfWidgetFormPropelChoice(array('model' => 'CategoriaMetasFijas', 'add_empty' => true)),
				'categoria_producto_id' => new sfWidgetFormPropelChoice(array('model' => 'CategoriaReporte', 'add_empty' => true)),
				'categoria_beca_id'     => new sfWidgetFormPropelChoice(array('model' => 'Beca', 'add_empty' => true)),
				'fecha_inicio'          => new sfWidgetFormDate(),
				'fecha_fin'             => new sfWidgetFormDate(),
				'semana'                => new sfWidgetFormInputText(),
				'anio'                  => new sfWidgetFormInputText(),
				'meta'                  => new sfWidgetFormInputText(),
				'entero'                => new sfWidgetFormInputCheckbox(),
				'usuario_id'            => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'activo'                => new sfWidgetFormInputCheckbox(),
				'created_at'            => new sfWidgetFormDateTime(),
				'updated_at'            => new sfWidgetFormDateTime(),
		));

		$this->setValidators(array(
				'id'                    => new sfValidatorPropelChoice(array('model' => 'Metas', 'column' => 'id', 'required' => false)),
				'centro_id'             => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id')),
				'categoria_fija_id'     => new sfValidatorPropelChoice(array('model' => 'CategoriaMetasFijas', 'column' => 'id', 'required' => false)),
				'categoria_producto_id' => new sfValidatorPropelChoice(array('model' => 'CategoriaReporte', 'column' => 'id', 'required' => false)),
				'categoria_beca_id'     => new sfValidatorPropelChoice(array('model' => 'Beca', 'column' => 'id', 'required' => false)),
				'fecha_inicio'          => new sfValidatorDate(),
				'fecha_fin'             => new sfValidatorDate(array('required' => false)),
				'semana'                => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'anio'                  => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'meta'                  => new sfValidatorNumber(),
				'entero'                => new sfValidatorBoolean(array('required' => false)),
				'usuario_id'            => new sfValidatorPropelChoice(array('model' => 'Usuario', 'column' => 'id', 'required' => false)),
				'activo'                => new sfValidatorBoolean(array('required' => false)),
				'created_at'            => new sfValidatorDateTime(array('required' => false)),
				'updated_at'            => new sfValidatorDateTime(array('required' => false)),
		));

		$this->validatorSchema->setPostValidator(
				new sfValidatorPropelUnique(array('model' => 'Metas', 'column' => array('centro_id', 'categoria_fija_id', 'categoria_producto_id', 'categoria_beca_id', 'fecha_inicio', 'entero')))
		);

		$this->widgetSchema->setNameFormat('metas[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Metas';
	}


}
