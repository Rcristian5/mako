<?php

/**
 * Alumnos form base class.
 *
 * @method Alumnos getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseAlumnosForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'matricula' => new sfWidgetFormInputHidden(),
				'socio_id'  => new sfWidgetFormPropelChoice(array('model' => 'Socio', 'add_empty' => false)),
		));

		$this->setValidators(array(
				'matricula' => new sfValidatorPropelChoice(array('model' => 'Alumnos', 'column' => 'matricula', 'required' => false)),
				'socio_id'  => new sfValidatorPropelChoice(array('model' => 'Socio', 'column' => 'id')),
		));

		$this->validatorSchema->setPostValidator(
				new sfValidatorPropelUnique(array('model' => 'Alumnos', 'column' => array('matricula')))
		);

		$this->widgetSchema->setNameFormat('alumnos[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Alumnos';
	}


}
