<?php

/**
 * Beca form base class.
 *
 * @method Beca getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseBecaForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'         => new sfWidgetFormInputHidden(),
				'nombre'     => new sfWidgetFormInputText(),
				'activo'     => new sfWidgetFormInputCheckbox(),
				'descuento'  => new sfWidgetFormInputText(),
				'created_at' => new sfWidgetFormDateTime(),
				'updated_at' => new sfWidgetFormDateTime(),
		));

		$this->setValidators(array(
				'id'         => new sfValidatorPropelChoice(array('model' => 'Beca', 'column' => 'id', 'required' => false)),
				'nombre'     => new sfValidatorString(array('max_length' => 255)),
				'activo'     => new sfValidatorBoolean(array('required' => false)),
				'descuento'  => new sfValidatorNumber(array('required' => false)),
				'created_at' => new sfValidatorDateTime(array('required' => false)),
				'updated_at' => new sfValidatorDateTime(array('required' => false)),
		));

		$this->validatorSchema->setPostValidator(
				new sfValidatorPropelUnique(array('model' => 'Beca', 'column' => array('nombre')))
		);

		$this->widgetSchema->setNameFormat('beca[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Beca';
	}


}
