<?php

/**
 * SocioPagos form base class.
 *
 * @method SocioPagos getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseSocioPagosForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'               => new sfWidgetFormInputHidden(),
				'centro_id'        => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'socio_id'         => new sfWidgetFormPropelChoice(array('model' => 'Socio', 'add_empty' => true)),
				'tipo_cobro_id'    => new sfWidgetFormPropelChoice(array('model' => 'TipoCobro', 'add_empty' => true)),
				'producto_id'      => new sfWidgetFormPropelChoice(array('model' => 'Producto', 'add_empty' => true)),
				'sub_producto_de'  => new sfWidgetFormPropelChoice(array('model' => 'Producto', 'add_empty' => true)),
				'numero_pago'      => new sfWidgetFormInputText(),
				'detalle_orden_id' => new sfWidgetFormPropelChoice(array('model' => 'DetalleOrden', 'add_empty' => true)),
				'fecha_a_pagar'    => new sfWidgetFormDateTime(),
				'fecha_pagado'     => new sfWidgetFormDateTime(),
				'pagado'           => new sfWidgetFormInputCheckbox(),
				'acuerdo_pago'     => new sfWidgetFormInputCheckbox(),
				'observaciones'    => new sfWidgetFormTextarea(),
				'cancelado'        => new sfWidgetFormInputCheckbox(),
				'orden_base_id'    => new sfWidgetFormPropelChoice(array('model' => 'Orden', 'add_empty' => true)),
				'created_at'       => new sfWidgetFormDateTime(),
				'updated_at'       => new sfWidgetFormDateTime(),
				'precio_lista'     => new sfWidgetFormInputText(),
				'tiempo'           => new sfWidgetFormInputText(),
				'cantidad'         => new sfWidgetFormInputText(),
				'descuento'        => new sfWidgetFormInputText(),
				'subtotal'         => new sfWidgetFormInputText(),
				'promocion_id'     => new sfWidgetFormPropelChoice(array('model' => 'Promocion', 'add_empty' => true)),
		));

		$this->setValidators(array(
				'id'               => new sfValidatorPropelChoice(array('model' => 'SocioPagos', 'column' => 'id', 'required' => false)),
				'centro_id'        => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id', 'required' => false)),
				'socio_id'         => new sfValidatorPropelChoice(array('model' => 'Socio', 'column' => 'id', 'required' => false)),
				'tipo_cobro_id'    => new sfValidatorPropelChoice(array('model' => 'TipoCobro', 'column' => 'id', 'required' => false)),
				'producto_id'      => new sfValidatorPropelChoice(array('model' => 'Producto', 'column' => 'id', 'required' => false)),
				'sub_producto_de'  => new sfValidatorPropelChoice(array('model' => 'Producto', 'column' => 'id', 'required' => false)),
				'numero_pago'      => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
				'detalle_orden_id' => new sfValidatorPropelChoice(array('model' => 'DetalleOrden', 'column' => 'id', 'required' => false)),
				'fecha_a_pagar'    => new sfValidatorDateTime(array('required' => false)),
				'fecha_pagado'     => new sfValidatorDateTime(array('required' => false)),
				'pagado'           => new sfValidatorBoolean(array('required' => false)),
				'acuerdo_pago'     => new sfValidatorBoolean(array('required' => false)),
				'observaciones'    => new sfValidatorString(array('required' => false)),
				'cancelado'        => new sfValidatorBoolean(array('required' => false)),
				'orden_base_id'    => new sfValidatorPropelChoice(array('model' => 'Orden', 'column' => 'id', 'required' => false)),
				'created_at'       => new sfValidatorDateTime(array('required' => false)),
				'updated_at'       => new sfValidatorDateTime(array('required' => false)),
				'precio_lista'     => new sfValidatorNumber(array('required' => false)),
				'tiempo'           => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
				'cantidad'         => new sfValidatorNumber(array('required' => false)),
				'descuento'        => new sfValidatorNumber(array('required' => false)),
				'subtotal'         => new sfValidatorNumber(array('required' => false)),
				'promocion_id'     => new sfValidatorPropelChoice(array('model' => 'Promocion', 'column' => 'id', 'required' => false)),
		));

		$this->widgetSchema->setNameFormat('socio_pagos[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'SocioPagos';
	}


}
