<?php

/**
 * Cupon form base class.
 *
 * @method Cupon getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCuponForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'                 => new sfWidgetFormInputHidden(),
				'promocion_cupon_id' => new sfWidgetFormPropelChoice(array('model' => 'PromocionCupon', 'add_empty' => true)),
				'centro_id'          => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'codigo'             => new sfWidgetFormInputText(),
				'validador'          => new sfWidgetFormInputText(),
				'otorgado_a'         => new sfWidgetFormPropelChoice(array('model' => 'Socio', 'add_empty' => true)),
				'otorgado_por'       => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'fecha_otorgado'     => new sfWidgetFormDateTime(),
				'razon_otorga'       => new sfWidgetFormTextarea(),
				'utilizado'          => new sfWidgetFormInputCheckbox(),
				'fecha_utilizado'    => new sfWidgetFormDateTime(),
		));

		$this->setValidators(array(
				'id'                 => new sfValidatorPropelChoice(array('model' => 'Cupon', 'column' => 'id', 'required' => false)),
				'promocion_cupon_id' => new sfValidatorPropelChoice(array('model' => 'PromocionCupon', 'column' => 'id', 'required' => false)),
				'centro_id'          => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id', 'required' => false)),
				'codigo'             => new sfValidatorString(array('max_length' => 16, 'required' => false)),
				'validador'          => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
				'otorgado_a'         => new sfValidatorPropelChoice(array('model' => 'Socio', 'column' => 'id', 'required' => false)),
				'otorgado_por'       => new sfValidatorPropelChoice(array('model' => 'Usuario', 'column' => 'id', 'required' => false)),
				'fecha_otorgado'     => new sfValidatorDateTime(array('required' => false)),
				'razon_otorga'       => new sfValidatorString(array('required' => false)),
				'utilizado'          => new sfValidatorBoolean(array('required' => false)),
				'fecha_utilizado'    => new sfValidatorDateTime(array('required' => false)),
		));

		$this->validatorSchema->setPostValidator(
				new sfValidatorPropelUnique(array('model' => 'Cupon', 'column' => array('codigo')))
		);

		$this->widgetSchema->setNameFormat('cupon[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Cupon';
	}


}
