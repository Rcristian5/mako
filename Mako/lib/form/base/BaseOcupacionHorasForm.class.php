<?php

/**
 * OcupacionHoras form base class.
 *
 * @method OcupacionHoras getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseOcupacionHorasForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'             => new sfWidgetFormInputHidden(),
				'centro_id'      => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => false)),
				'fecha_hora'     => new sfWidgetFormDateTime(),
				'fecha'          => new sfWidgetFormDate(),
				'hora'           => new sfWidgetFormTime(),
				'consumo'        => new sfWidgetFormInputText(),
				'sesiones'       => new sfWidgetFormInputText(),
				'capacidad_hora' => new sfWidgetFormInputText(),
		));

		$this->setValidators(array(
				'id'             => new sfValidatorPropelChoice(array('model' => 'OcupacionHoras', 'column' => 'id', 'required' => false)),
				'centro_id'      => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id')),
				'fecha_hora'     => new sfValidatorDateTime(),
				'fecha'          => new sfValidatorDate(),
				'hora'           => new sfValidatorTime(),
				'consumo'        => new sfValidatorNumber(array('required' => false)),
				'sesiones'       => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
				'capacidad_hora' => new sfValidatorInteger(array('min' => -9.2233720368548E+18, 'max' => 9.2233720368548E+18, 'required' => false)),
		));

		$this->validatorSchema->setPostValidator(
				new sfValidatorPropelUnique(array('model' => 'OcupacionHoras', 'column' => array('centro_id', 'fecha_hora')))
		);

		$this->widgetSchema->setNameFormat('ocupacion_horas[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'OcupacionHoras';
	}


}
