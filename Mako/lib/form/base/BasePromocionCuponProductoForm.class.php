<?php

/**
 * PromocionCuponProducto form base class.
 *
 * @method PromocionCuponProducto getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePromocionCuponProductoForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'promocion_id' => new sfWidgetFormInputHidden(),
				'producto_id'  => new sfWidgetFormInputHidden(),
		));

		$this->setValidators(array(
				'promocion_id' => new sfValidatorPropelChoice(array('model' => 'PromocionCupon', 'column' => 'id', 'required' => false)),
				'producto_id'  => new sfValidatorPropelChoice(array('model' => 'Producto', 'column' => 'id', 'required' => false)),
		));

		$this->widgetSchema->setNameFormat('promocion_cupon_producto[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'PromocionCuponProducto';
	}


}
