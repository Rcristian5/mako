<?php

/**
 * RespuestasCuestionario form base class.
 *
 * @method RespuestasCuestionario getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseRespuestasCuestionarioForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'                 => new sfWidgetFormInputHidden(),
				'categoria_curso_id' => new sfWidgetFormPropelChoice(array('model' => 'CategoriaCurso', 'add_empty' => false)),
				'curso_id'           => new sfWidgetFormPropelChoice(array('model' => 'Curso', 'add_empty' => true)),
				'respuesta'          => new sfWidgetFormTextarea(),
				'valor'              => new sfWidgetFormInputText(),
				'activo'             => new sfWidgetFormInputCheckbox(),
				'operador_id'        => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'created_at'         => new sfWidgetFormDateTime(),
				'updated_at'         => new sfWidgetFormDateTime(),
		));

		$this->setValidators(array(
				'id'                 => new sfValidatorPropelChoice(array('model' => 'RespuestasCuestionario', 'column' => 'id', 'required' => false)),
				'categoria_curso_id' => new sfValidatorPropelChoice(array('model' => 'CategoriaCurso', 'column' => 'id')),
				'curso_id'           => new sfValidatorPropelChoice(array('model' => 'Curso', 'column' => 'id', 'required' => false)),
				'respuesta'          => new sfValidatorString(),
				'valor'              => new sfValidatorNumber(),
				'activo'             => new sfValidatorBoolean(array('required' => false)),
				'operador_id'        => new sfValidatorPropelChoice(array('model' => 'Usuario', 'column' => 'id', 'required' => false)),
				'created_at'         => new sfValidatorDateTime(array('required' => false)),
				'updated_at'         => new sfValidatorDateTime(array('required' => false)),
		));

		$this->widgetSchema->setNameFormat('respuestas_cuestionario[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'RespuestasCuestionario';
	}


}
