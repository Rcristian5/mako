<?php

/**
 * SubproductoCobranza form base class.
 *
 * @method SubproductoCobranza getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseSubproductoCobranzaForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'producto_id'     => new sfWidgetFormInputHidden(),
				'sub_producto_de' => new sfWidgetFormPropelChoice(array('model' => 'Producto', 'add_empty' => true)),
				'cobro_id'        => new sfWidgetFormInputHidden(),
				'numero_pago'     => new sfWidgetFormInputText(),
		));

		$this->setValidators(array(
				'producto_id'     => new sfValidatorPropelChoice(array('model' => 'Producto', 'column' => 'id', 'required' => false)),
				'sub_producto_de' => new sfValidatorPropelChoice(array('model' => 'Producto', 'column' => 'id', 'required' => false)),
				'cobro_id'        => new sfValidatorPropelChoice(array('model' => 'TipoCobro', 'column' => 'id', 'required' => false)),
				'numero_pago'     => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
		));

		$this->widgetSchema->setNameFormat('subproducto_cobranza[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'SubproductoCobranza';
	}


}
