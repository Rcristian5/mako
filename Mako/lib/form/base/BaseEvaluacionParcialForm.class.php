<?php

/**
 * EvaluacionParcial form base class.
 *
 * @method EvaluacionParcial getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseEvaluacionParcialForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'                  => new sfWidgetFormInputHidden(),
				'curso_id'            => new sfWidgetFormPropelChoice(array('model' => 'Curso', 'add_empty' => false)),
				'tema_id'             => new sfWidgetFormPropelChoice(array('model' => 'Tema', 'add_empty' => false)),
				'nombre'              => new sfWidgetFormInputText(),
				'descripcion'         => new sfWidgetFormTextarea(),
				'calificacion_minima' => new sfWidgetFormInputText(),
				'calificacion_maxima' => new sfWidgetFormInputText(),
				'porcentaje'          => new sfWidgetFormInputText(),
				'activo'              => new sfWidgetFormInputCheckbox(),
				'operador_id'         => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'created_at'          => new sfWidgetFormDateTime(),
				'updated_at'          => new sfWidgetFormDateTime(),
		));

		$this->setValidators(array(
				'id'                  => new sfValidatorPropelChoice(array('model' => 'EvaluacionParcial', 'column' => 'id', 'required' => false)),
				'curso_id'            => new sfValidatorPropelChoice(array('model' => 'Curso', 'column' => 'id')),
				'tema_id'             => new sfValidatorPropelChoice(array('model' => 'Tema', 'column' => 'id')),
				'nombre'              => new sfValidatorString(array('max_length' => 255)),
				'descripcion'         => new sfValidatorString(),
				'calificacion_minima' => new sfValidatorNumber(array('required' => false)),
				'calificacion_maxima' => new sfValidatorNumber(array('required' => false)),
				'porcentaje'          => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'activo'              => new sfValidatorBoolean(array('required' => false)),
				'operador_id'         => new sfValidatorPropelChoice(array('model' => 'Usuario', 'column' => 'id', 'required' => false)),
				'created_at'          => new sfValidatorDateTime(array('required' => false)),
				'updated_at'          => new sfValidatorDateTime(array('required' => false)),
		));

		$this->widgetSchema->setNameFormat('evaluacion_parcial[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'EvaluacionParcial';
	}


}
