<?php

/**
 * ReporteAsistencias form base class.
 *
 * @method ReporteAsistencias getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseReporteAsistenciasForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'                            => new sfWidgetFormInputHidden(),
				'centro_id'                     => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => false)),
				'curso_id'                      => new sfWidgetFormPropelChoice(array('model' => 'Curso', 'add_empty' => false)),
				'fecha'                         => new sfWidgetFormDate(),
				'socios_registrados'            => new sfWidgetFormInputText(),
				'socios_primera_sesion'         => new sfWidgetFormInputText(),
				'socios_ultima_sesion'          => new sfWidgetFormInputText(),
				'socios_penultima_sesion'       => new sfWidgetFormInputText(),
				'socios_imprmieron_certificado' => new sfWidgetFormInputText(),
				'socios_presentaron_examen'     => new sfWidgetFormInputText(),
		));

		$this->setValidators(array(
				'id'                            => new sfValidatorPropelChoice(array('model' => 'ReporteAsistencias', 'column' => 'id', 'required' => false)),
				'centro_id'                     => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id')),
				'curso_id'                      => new sfValidatorPropelChoice(array('model' => 'Curso', 'column' => 'id')),
				'fecha'                         => new sfValidatorDate(),
				'socios_registrados'            => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'socios_primera_sesion'         => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'socios_ultima_sesion'          => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'socios_penultima_sesion'       => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'socios_imprmieron_certificado' => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'socios_presentaron_examen'     => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
		));

		$this->validatorSchema->setPostValidator(
				new sfValidatorPropelUnique(array('model' => 'ReporteAsistencias', 'column' => array('centro_id', 'curso_id', 'fecha')))
		);

		$this->widgetSchema->setNameFormat('reporte_asistencias[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'ReporteAsistencias';
	}


}
