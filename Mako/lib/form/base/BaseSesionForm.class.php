<?php

/**
 * Sesion form base class.
 *
 * @method Sesion getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseSesionForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'             => new sfWidgetFormInputHidden(),
				'centro_id'      => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'computadora_id' => new sfWidgetFormPropelChoice(array('model' => 'Computadora', 'add_empty' => true)),
				'ip'             => new sfWidgetFormInputText(),
				'socio_id'       => new sfWidgetFormPropelChoice(array('model' => 'Socio', 'add_empty' => true)),
				'usuario'        => new sfWidgetFormInputText(),
				'fecha_login'    => new sfWidgetFormDateTime(),
				'tipo_id'        => new sfWidgetFormPropelChoice(array('model' => 'TipoSesion', 'add_empty' => true)),
				'saldo'          => new sfWidgetFormInputText(),
		));

		$this->setValidators(array(
				'id'             => new sfValidatorPropelChoice(array('model' => 'Sesion', 'column' => 'id', 'required' => false)),
				'centro_id'      => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id', 'required' => false)),
				'computadora_id' => new sfValidatorPropelChoice(array('model' => 'Computadora', 'column' => 'id', 'required' => false)),
				'ip'             => new sfValidatorString(array('max_length' => 15)),
				'socio_id'       => new sfValidatorPropelChoice(array('model' => 'Socio', 'column' => 'id', 'required' => false)),
				'usuario'        => new sfValidatorString(array('max_length' => 90)),
				'fecha_login'    => new sfValidatorDateTime(),
				'tipo_id'        => new sfValidatorPropelChoice(array('model' => 'TipoSesion', 'column' => 'id', 'required' => false)),
				'saldo'          => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
		));

		$this->validatorSchema->setPostValidator(
				new sfValidatorPropelUnique(array('model' => 'Sesion', 'column' => array('centro_id', 'computadora_id')))
		);

		$this->widgetSchema->setNameFormat('sesion[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Sesion';
	}


}
