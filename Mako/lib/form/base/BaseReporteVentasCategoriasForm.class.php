<?php

/**
 * ReporteVentasCategorias form base class.
 *
 * @method ReporteVentasCategorias getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseReporteVentasCategoriasForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'           => new sfWidgetFormInputHidden(),
				'centro_id'    => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => false)),
				'fecha'        => new sfWidgetFormDate(),
				'semana'       => new sfWidgetFormInputText(),
				'anio'         => new sfWidgetFormInputText(),
				'monto'        => new sfWidgetFormInputText(),
				'cantidad'     => new sfWidgetFormInputText(),
				'tiempo'       => new sfWidgetFormInputText(),
				'categoria_id' => new sfWidgetFormPropelChoice(array('model' => 'CategoriaProducto', 'add_empty' => false)),
				'categoria'    => new sfWidgetFormInputText(),
				'pcs'          => new sfWidgetFormInputText(),
				'cursos'       => new sfWidgetFormInputText(),
				'aulas'        => new sfWidgetFormInputText(),
		));

		$this->setValidators(array(
				'id'           => new sfValidatorPropelChoice(array('model' => 'ReporteVentasCategorias', 'column' => 'id', 'required' => false)),
				'centro_id'    => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id')),
				'fecha'        => new sfValidatorDate(),
				'semana'       => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'anio'         => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'monto'        => new sfValidatorNumber(),
				'cantidad'     => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'tiempo'       => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
				'categoria_id' => new sfValidatorPropelChoice(array('model' => 'CategoriaProducto', 'column' => 'id')),
				'categoria'    => new sfValidatorString(array('max_length' => 255)),
				'pcs'          => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'cursos'       => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'aulas'        => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
		));

		$this->validatorSchema->setPostValidator(
				new sfValidatorPropelUnique(array('model' => 'ReporteVentasCategorias', 'column' => array('centro_id', 'categoria_id', 'fecha', 'monto')))
		);

		$this->widgetSchema->setNameFormat('reporte_ventas_categorias[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'ReporteVentasCategorias';
	}


}
