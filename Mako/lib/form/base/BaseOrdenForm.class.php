<?php

/**
 * Orden form base class.
 *
 * @method Orden getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseOrdenForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'               => new sfWidgetFormInputHidden(),
				'centro_id'        => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'socio_id'         => new sfWidgetFormPropelChoice(array('model' => 'Socio', 'add_empty' => true)),
				'folio'            => new sfWidgetFormInputText(),
				'total'            => new sfWidgetFormInputText(),
				'forma_pago_id'    => new sfWidgetFormPropelChoice(array('model' => 'FormaPago', 'add_empty' => true)),
				'operador_id'      => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'tipo_id'          => new sfWidgetFormPropelChoice(array('model' => 'TipoOrden', 'add_empty' => true)),
				'ip_caja'          => new sfWidgetFormInputText(),
				'es_factura'       => new sfWidgetFormInputCheckbox(),
				'razon_social'     => new sfWidgetFormInputText(),
				'rfc'              => new sfWidgetFormInputText(),
				'calle_fiscal'     => new sfWidgetFormInputText(),
				'colonia_fiscal'   => new sfWidgetFormInputText(),
				'cp_fiscal'        => new sfWidgetFormInputText(),
				'estado_fiscal'    => new sfWidgetFormInputText(),
				'municipio_fiscal' => new sfWidgetFormInputText(),
				'num_ext_fiscal'   => new sfWidgetFormInputText(),
				'num_int_fiscal'   => new sfWidgetFormInputText(),
				'pais_fiscal'      => new sfWidgetFormInputText(),
				'estatus_id'       => new sfWidgetFormPropelChoice(array('model' => 'EstatusOrden', 'add_empty' => true)),
				'corte_id'         => new sfWidgetFormPropelChoice(array('model' => 'Corte', 'add_empty' => true)),
				'factura_id'       => new sfWidgetFormPropelChoice(array('model' => 'Factura', 'add_empty' => true)),
				'created_at'       => new sfWidgetFormDateTime(),
				'updated_at'       => new sfWidgetFormDateTime(),
		));

		$this->setValidators(array(
				'id'               => new sfValidatorPropelChoice(array('model' => 'Orden', 'column' => 'id', 'required' => false)),
				'centro_id'        => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id', 'required' => false)),
				'socio_id'         => new sfValidatorPropelChoice(array('model' => 'Socio', 'column' => 'id', 'required' => false)),
				'folio'            => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
				'total'            => new sfValidatorNumber(array('required' => false)),
				'forma_pago_id'    => new sfValidatorPropelChoice(array('model' => 'FormaPago', 'column' => 'id', 'required' => false)),
				'operador_id'      => new sfValidatorPropelChoice(array('model' => 'Usuario', 'column' => 'id', 'required' => false)),
				'tipo_id'          => new sfValidatorPropelChoice(array('model' => 'TipoOrden', 'column' => 'id', 'required' => false)),
				'ip_caja'          => new sfValidatorString(array('max_length' => 15, 'required' => false)),
				'es_factura'       => new sfValidatorBoolean(array('required' => false)),
				'razon_social'     => new sfValidatorString(array('max_length' => 256, 'required' => false)),
				'rfc'              => new sfValidatorString(array('max_length' => 13, 'required' => false)),
				'calle_fiscal'     => new sfValidatorString(array('max_length' => 256, 'required' => false)),
				'colonia_fiscal'   => new sfValidatorString(array('max_length' => 256, 'required' => false)),
				'cp_fiscal'        => new sfValidatorString(array('max_length' => 5, 'required' => false)),
				'estado_fiscal'    => new sfValidatorString(array('max_length' => 256, 'required' => false)),
				'municipio_fiscal' => new sfValidatorString(array('max_length' => 256, 'required' => false)),
				'num_ext_fiscal'   => new sfValidatorString(array('max_length' => 60, 'required' => false)),
				'num_int_fiscal'   => new sfValidatorString(array('max_length' => 60, 'required' => false)),
				'pais_fiscal'      => new sfValidatorString(array('max_length' => 60, 'required' => false)),
				'estatus_id'       => new sfValidatorPropelChoice(array('model' => 'EstatusOrden', 'column' => 'id', 'required' => false)),
				'corte_id'         => new sfValidatorPropelChoice(array('model' => 'Corte', 'column' => 'id', 'required' => false)),
				'factura_id'       => new sfValidatorPropelChoice(array('model' => 'Factura', 'column' => 'id', 'required' => false)),
				'created_at'       => new sfValidatorDateTime(array('required' => false)),
				'updated_at'       => new sfValidatorDateTime(array('required' => false)),
		));

		$this->widgetSchema->setNameFormat('orden[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Orden';
	}


}
