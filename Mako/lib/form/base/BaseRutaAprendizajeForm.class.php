<?php

/**
 * RutaAprendizaje form base class.
 *
 * @method RutaAprendizaje getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseRutaAprendizajeForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'curso_id' => new sfWidgetFormPropelChoice(array('model' => 'Curso', 'add_empty' => false)),
				'padre_id' => new sfWidgetFormPropelChoice(array('model' => 'Curso', 'add_empty' => false)),
				'id'       => new sfWidgetFormInputHidden(),
		));

		$this->setValidators(array(
				'curso_id' => new sfValidatorPropelChoice(array('model' => 'Curso', 'column' => 'id')),
				'padre_id' => new sfValidatorPropelChoice(array('model' => 'Curso', 'column' => 'id')),
				'id'       => new sfValidatorPropelChoice(array('model' => 'RutaAprendizaje', 'column' => 'id', 'required' => false)),
		));

		$this->widgetSchema->setNameFormat('ruta_aprendizaje[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'RutaAprendizaje';
	}


}
