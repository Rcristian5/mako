<?php

/**
 * AsignacionBeca form base class.
 *
 * @method AsignacionBeca getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseAsignacionBecaForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'                   => new sfWidgetFormInputHidden(),
				'beca_id'              => new sfWidgetFormPropelChoice(array('model' => 'Beca', 'add_empty' => true)),
				'centro_id'            => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'socio_id'             => new sfWidgetFormPropelChoice(array('model' => 'Socio', 'add_empty' => true)),
				'registro_operador_id' => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'fecha_solicitud'      => new sfWidgetFormDateTime(),
				'otorga_operador_id'   => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'fecha_otorgado'       => new sfWidgetFormDateTime(),
		));

		$this->setValidators(array(
				'id'                   => new sfValidatorPropelChoice(array('model' => 'AsignacionBeca', 'column' => 'id', 'required' => false)),
				'beca_id'              => new sfValidatorPropelChoice(array('model' => 'Beca', 'column' => 'id', 'required' => false)),
				'centro_id'            => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id', 'required' => false)),
				'socio_id'             => new sfValidatorPropelChoice(array('model' => 'Socio', 'column' => 'id', 'required' => false)),
				'registro_operador_id' => new sfValidatorPropelChoice(array('model' => 'Usuario', 'column' => 'id', 'required' => false)),
				'fecha_solicitud'      => new sfValidatorDateTime(array('required' => false)),
				'otorga_operador_id'   => new sfValidatorPropelChoice(array('model' => 'Usuario', 'column' => 'id', 'required' => false)),
				'fecha_otorgado'       => new sfValidatorDateTime(array('required' => false)),
		));

		$this->validatorSchema->setPostValidator(
				new sfValidatorPropelUnique(array('model' => 'AsignacionBeca', 'column' => array('socio_id', 'beca_id')))
		);

		$this->widgetSchema->setNameFormat('asignacion_beca[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'AsignacionBeca';
	}


}
