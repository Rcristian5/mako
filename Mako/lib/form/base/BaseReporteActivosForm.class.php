<?php

/**
 * ReporteActivos form base class.
 *
 * @method ReporteActivos getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseReporteActivosForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'centro_id'       => new sfWidgetFormInputHidden(),
				'fecha'           => new sfWidgetFormInputHidden(),
				'socio_id'        => new sfWidgetFormInputHidden(),
				'usuario'         => new sfWidgetFormInputText(),
				'folio'           => new sfWidgetFormInputText(),
				'nombre'          => new sfWidgetFormInputText(),
				'apepat'          => new sfWidgetFormInputText(),
				'apemat'          => new sfWidgetFormInputText(),
				'nombre_completo' => new sfWidgetFormInputText(),
				'fecha_nac'       => new sfWidgetFormDate(),
				'sexo'            => new sfWidgetFormInputText(),
				'email'           => new sfWidgetFormInputText(),
				'tel'             => new sfWidgetFormInputText(),
				'celular'         => new sfWidgetFormInputText(),
				'estado'          => new sfWidgetFormInputText(),
				'ciudad'          => new sfWidgetFormInputText(),
				'municipio'       => new sfWidgetFormInputText(),
				'colonia'         => new sfWidgetFormInputText(),
				'cp'              => new sfWidgetFormInputText(),
				'calle'           => new sfWidgetFormInputText(),
				'num_ext'         => new sfWidgetFormInputText(),
				'num_int'         => new sfWidgetFormInputText(),
				'lat'             => new sfWidgetFormInputText(),
				'long'            => new sfWidgetFormInputText(),
		));

		$this->setValidators(array(
				'centro_id'       => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id', 'required' => false)),
				'fecha'           => new sfValidatorPropelChoice(array('model' => 'ReporteActivos', 'column' => 'fecha', 'required' => false)),
				'socio_id'        => new sfValidatorPropelChoice(array('model' => 'ReporteActivos', 'column' => 'socio_id', 'required' => false)),
				'usuario'         => new sfValidatorString(array('max_length' => 90)),
				'folio'           => new sfValidatorString(array('max_length' => 50)),
				'nombre'          => new sfValidatorString(array('max_length' => 30)),
				'apepat'          => new sfValidatorString(array('max_length' => 60)),
				'apemat'          => new sfValidatorString(array('max_length' => 60, 'required' => false)),
				'nombre_completo' => new sfValidatorString(array('max_length' => 200, 'required' => false)),
				'fecha_nac'       => new sfValidatorDate(),
				'sexo'            => new sfValidatorString(array('max_length' => 1)),
				'email'           => new sfValidatorString(array('max_length' => 60, 'required' => false)),
				'tel'             => new sfValidatorString(array('max_length' => 60, 'required' => false)),
				'celular'         => new sfValidatorString(array('max_length' => 60, 'required' => false)),
				'estado'          => new sfValidatorString(array('max_length' => 30, 'required' => false)),
				'ciudad'          => new sfValidatorString(array('max_length' => 60, 'required' => false)),
				'municipio'       => new sfValidatorString(array('max_length' => 60, 'required' => false)),
				'colonia'         => new sfValidatorString(array('max_length' => 60, 'required' => false)),
				'cp'              => new sfValidatorString(array('max_length' => 5, 'required' => false)),
				'calle'           => new sfValidatorString(array('max_length' => 60, 'required' => false)),
				'num_ext'         => new sfValidatorString(array('max_length' => 30, 'required' => false)),
				'num_int'         => new sfValidatorString(array('max_length' => 30, 'required' => false)),
				'lat'             => new sfValidatorString(array('max_length' => 30, 'required' => false)),
				'long'            => new sfValidatorString(array('max_length' => 30, 'required' => false)),
		));

		$this->widgetSchema->setNameFormat('reporte_activos[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'ReporteActivos';
	}


}
