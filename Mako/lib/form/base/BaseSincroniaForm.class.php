<?php

/**
 * Sincronia form base class.
 *
 * @method Sincronia getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseSincroniaForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'         => new sfWidgetFormInputHidden(),
				'pk_async'   => new sfWidgetFormInputText(),
				'clase'      => new sfWidgetFormTextarea(),
				'operacion'  => new sfWidgetFormInputText(),
				'tipo'       => new sfWidgetFormInputText(),
				'detalle'    => new sfWidgetFormTextarea(),
				'completo'   => new sfWidgetFormInputCheckbox(),
				'created_at' => new sfWidgetFormDateTime(),
				'updated_at' => new sfWidgetFormDateTime(),
		));

		$this->setValidators(array(
				'id'         => new sfValidatorPropelChoice(array('model' => 'Sincronia', 'column' => 'id', 'required' => false)),
				'pk_async'   => new sfValidatorInteger(array('min' => -9.2233720368548E+18, 'max' => 9.2233720368548E+18, 'required' => false)),
				'clase'      => new sfValidatorString(),
				'operacion'  => new sfValidatorString(array('max_length' => 255)),
				'tipo'       => new sfValidatorString(array('max_length' => 255)),
				'detalle'    => new sfValidatorString(),
				'completo'   => new sfValidatorBoolean(array('required' => false)),
				'created_at' => new sfValidatorDateTime(array('required' => false)),
				'updated_at' => new sfValidatorDateTime(array('required' => false)),
		));

		$this->widgetSchema->setNameFormat('sincronia[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Sincronia';
	}


}
