<?php

/**
 * AlumnoPagos form base class.
 *
 * @method AlumnoPagos getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseAlumnoPagosForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'              => new sfWidgetFormInputHidden(),
				'centro_id'       => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'socio_id'        => new sfWidgetFormPropelChoice(array('model' => 'Socio', 'add_empty' => true)),
				'curso_id'        => new sfWidgetFormPropelChoice(array('model' => 'Curso', 'add_empty' => true)),
				'grupo_id'        => new sfWidgetFormPropelChoice(array('model' => 'Grupo', 'add_empty' => true)),
				'tipo_cobro_id'   => new sfWidgetFormPropelChoice(array('model' => 'TipoCobro', 'add_empty' => true)),
				'pagado'          => new sfWidgetFormInputCheckbox(),
				'numero_pago'     => new sfWidgetFormInputText(),
				'cancelado'       => new sfWidgetFormInputCheckbox(),
				'producto_id'     => new sfWidgetFormPropelChoice(array('model' => 'Producto', 'add_empty' => true)),
				'sub_producto_de' => new sfWidgetFormPropelChoice(array('model' => 'Producto', 'add_empty' => true)),
				'orden_base_id'   => new sfWidgetFormPropelChoice(array('model' => 'Orden', 'add_empty' => true)),
				'created_at'      => new sfWidgetFormDateTime(),
				'updated_at'      => new sfWidgetFormDateTime(),
		));

		$this->setValidators(array(
				'id'              => new sfValidatorPropelChoice(array('model' => 'AlumnoPagos', 'column' => 'id', 'required' => false)),
				'centro_id'       => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id', 'required' => false)),
				'socio_id'        => new sfValidatorPropelChoice(array('model' => 'Socio', 'column' => 'id', 'required' => false)),
				'curso_id'        => new sfValidatorPropelChoice(array('model' => 'Curso', 'column' => 'id', 'required' => false)),
				'grupo_id'        => new sfValidatorPropelChoice(array('model' => 'Grupo', 'column' => 'id', 'required' => false)),
				'tipo_cobro_id'   => new sfValidatorPropelChoice(array('model' => 'TipoCobro', 'column' => 'id', 'required' => false)),
				'pagado'          => new sfValidatorBoolean(array('required' => false)),
				'numero_pago'     => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
				'cancelado'       => new sfValidatorBoolean(array('required' => false)),
				'producto_id'     => new sfValidatorPropelChoice(array('model' => 'Producto', 'column' => 'id', 'required' => false)),
				'sub_producto_de' => new sfValidatorPropelChoice(array('model' => 'Producto', 'column' => 'id', 'required' => false)),
				'orden_base_id'   => new sfValidatorPropelChoice(array('model' => 'Orden', 'column' => 'id', 'required' => false)),
				'created_at'      => new sfValidatorDateTime(array('required' => false)),
				'updated_at'      => new sfValidatorDateTime(array('required' => false)),
		));

		$this->widgetSchema->setNameFormat('alumno_pagos[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'AlumnoPagos';
	}


}
