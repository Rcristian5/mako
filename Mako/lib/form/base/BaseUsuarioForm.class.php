<?php

/**
 * Usuario form base class.
 *
 * @method Usuario getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseUsuarioForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'                  => new sfWidgetFormInputHidden(),
				'rol_id'              => new sfWidgetFormPropelChoice(array('model' => 'Rol', 'add_empty' => true)),
				'usuario'             => new sfWidgetFormInputText(),
				'clave'               => new sfWidgetFormInputText(),
				'nombre'              => new sfWidgetFormInputText(),
				'apepat'              => new sfWidgetFormInputText(),
				'apemat'              => new sfWidgetFormInputText(),
				'nombre_completo'     => new sfWidgetFormInputText(),
				'email'               => new sfWidgetFormInputText(),
				'operador_id'         => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'activo'              => new sfWidgetFormInputCheckbox(),
				'created_at'          => new sfWidgetFormDateTime(),
				'updated_at'          => new sfWidgetFormDateTime(),
				'usuario_centro_list' => new sfWidgetFormPropelChoice(array('multiple' => true, 'model' => 'Centro')),
				'usuario_perfil_list' => new sfWidgetFormPropelChoice(array('multiple' => true, 'model' => 'PerfilFacilitador')),
		));

		$this->setValidators(array(
				'id'                  => new sfValidatorPropelChoice(array('model' => 'Usuario', 'column' => 'id', 'required' => false)),
				'rol_id'              => new sfValidatorPropelChoice(array('model' => 'Rol', 'column' => 'id', 'required' => false)),
				'usuario'             => new sfValidatorString(array('max_length' => 90)),
				'clave'               => new sfValidatorString(array('max_length' => 30)),
				'nombre'              => new sfValidatorString(array('max_length' => 30)),
				'apepat'              => new sfValidatorString(array('max_length' => 60)),
				'apemat'              => new sfValidatorString(array('max_length' => 60, 'required' => false)),
				'nombre_completo'     => new sfValidatorString(array('max_length' => 200)),
				'email'               => new sfValidatorString(array('max_length' => 60, 'required' => false)),
				'operador_id'         => new sfValidatorPropelChoice(array('model' => 'Usuario', 'column' => 'id', 'required' => false)),
				'activo'              => new sfValidatorBoolean(array('required' => false)),
				'created_at'          => new sfValidatorDateTime(array('required' => false)),
				'updated_at'          => new sfValidatorDateTime(array('required' => false)),
				'usuario_centro_list' => new sfValidatorPropelChoice(array('multiple' => true, 'model' => 'Centro', 'required' => false)),
				'usuario_perfil_list' => new sfValidatorPropelChoice(array('multiple' => true, 'model' => 'PerfilFacilitador', 'required' => false)),
		));

		$this->validatorSchema->setPostValidator(
				new sfValidatorPropelUnique(array('model' => 'Usuario', 'column' => array('usuario')))
		);

		$this->widgetSchema->setNameFormat('usuario[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Usuario';
	}


	public function updateDefaultsFromObject()
	{
		parent::updateDefaultsFromObject();

		if (isset($this->widgetSchema['usuario_centro_list']))
		{
			$values = array();
			foreach ($this->object->getUsuarioCentros() as $obj)
			{
				$values[] = $obj->getCentroId();
			}

			$this->setDefault('usuario_centro_list', $values);
		}

		if (isset($this->widgetSchema['usuario_perfil_list']))
		{
			$values = array();
			foreach ($this->object->getUsuarioPerfils() as $obj)
			{
				$values[] = $obj->getPerfilId();
			}

			$this->setDefault('usuario_perfil_list', $values);
		}

	}

	protected function doSave($con = null)
	{
		parent::doSave($con);

		$this->saveUsuarioCentroList($con);
		$this->saveUsuarioPerfilList($con);
	}

	public function saveUsuarioCentroList($con = null)
	{
		if (!$this->isValid())
		{
			throw $this->getErrorSchema();
		}

		if (!isset($this->widgetSchema['usuario_centro_list']))
		{
			// somebody has unset this widget
			return;
		}

		if (null === $con)
		{
			$con = $this->getConnection();
		}

		$c = new Criteria();
		$c->add(UsuarioCentroPeer::USUARIO_ID, $this->object->getPrimaryKey());
		UsuarioCentroPeer::doDelete($c, $con);

		$values = $this->getValue('usuario_centro_list');
		if (is_array($values))
		{
			foreach ($values as $value)
			{
				$obj = new UsuarioCentro();
				$obj->setUsuarioId($this->object->getPrimaryKey());
				$obj->setCentroId($value);
				$obj->save();
			}
		}
	}

	public function saveUsuarioPerfilList($con = null)
	{
		if (!$this->isValid())
		{
			throw $this->getErrorSchema();
		}

		if (!isset($this->widgetSchema['usuario_perfil_list']))
		{
			// somebody has unset this widget
			return;
		}

		if (null === $con)
		{
			$con = $this->getConnection();
		}

		$c = new Criteria();
		$c->add(UsuarioPerfilPeer::USUARIO_ID, $this->object->getPrimaryKey());
		UsuarioPerfilPeer::doDelete($c, $con);

		$values = $this->getValue('usuario_perfil_list');
		if (is_array($values))
		{
			foreach ($values as $value)
			{
				$obj = new UsuarioPerfil();
				$obj->setUsuarioId($this->object->getPrimaryKey());
				$obj->setPerfilId($value);
				$obj->save();
			}
		}
	}

}
