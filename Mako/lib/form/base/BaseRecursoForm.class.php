<?php

/**
 * Recurso form base class.
 *
 * @method Recurso getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseRecursoForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'                      => new sfWidgetFormInputHidden(),
				'categoria_recursos_id'   => new sfWidgetFormPropelChoice(array('model' => 'CategoriaRecursos', 'add_empty' => false)),
				'unidad_id'               => new sfWidgetFormPropelChoice(array('model' => 'UnidadProducto', 'add_empty' => false)),
				'codigo'                  => new sfWidgetFormInputText(),
				'nombre'                  => new sfWidgetFormInputText(),
				'activo'                  => new sfWidgetFormInputCheckbox(),
				'operador_id'             => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'created_at'              => new sfWidgetFormDateTime(),
				'updated_at'              => new sfWidgetFormDateTime(),
				'recursos_asignados_list' => new sfWidgetFormPropelChoice(array('multiple' => true, 'model' => 'Curso')),
		));

		$this->setValidators(array(
				'id'                      => new sfValidatorPropelChoice(array('model' => 'Recurso', 'column' => 'id', 'required' => false)),
				'categoria_recursos_id'   => new sfValidatorPropelChoice(array('model' => 'CategoriaRecursos', 'column' => 'id')),
				'unidad_id'               => new sfValidatorPropelChoice(array('model' => 'UnidadProducto', 'column' => 'id')),
				'codigo'                  => new sfValidatorString(array('max_length' => 250, 'required' => false)),
				'nombre'                  => new sfValidatorString(array('max_length' => 250, 'required' => false)),
				'activo'                  => new sfValidatorBoolean(array('required' => false)),
				'operador_id'             => new sfValidatorPropelChoice(array('model' => 'Usuario', 'column' => 'id', 'required' => false)),
				'created_at'              => new sfValidatorDateTime(array('required' => false)),
				'updated_at'              => new sfValidatorDateTime(array('required' => false)),
				'recursos_asignados_list' => new sfValidatorPropelChoice(array('multiple' => true, 'model' => 'Curso', 'required' => false)),
		));

		$this->widgetSchema->setNameFormat('recurso[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Recurso';
	}


	public function updateDefaultsFromObject()
	{
		parent::updateDefaultsFromObject();

		if (isset($this->widgetSchema['recursos_asignados_list']))
		{
			$values = array();
			foreach ($this->object->getRecursosAsignadoss() as $obj)
			{
				$values[] = $obj->getCursoId();
			}

			$this->setDefault('recursos_asignados_list', $values);
		}

	}

	protected function doSave($con = null)
	{
		parent::doSave($con);

		$this->saveRecursosAsignadosList($con);
	}

	public function saveRecursosAsignadosList($con = null)
	{
		if (!$this->isValid())
		{
			throw $this->getErrorSchema();
		}

		if (!isset($this->widgetSchema['recursos_asignados_list']))
		{
			// somebody has unset this widget
			return;
		}

		if (null === $con)
		{
			$con = $this->getConnection();
		}

		$c = new Criteria();
		$c->add(RecursosAsignadosPeer::RECURSO_ID, $this->object->getPrimaryKey());
		RecursosAsignadosPeer::doDelete($c, $con);

		$values = $this->getValue('recursos_asignados_list');
		if (is_array($values))
		{
			foreach ($values as $value)
			{
				$obj = new RecursosAsignados();
				$obj->setRecursoId($this->object->getPrimaryKey());
				$obj->setCursoId($value);
				$obj->save();
			}
		}
	}

}
