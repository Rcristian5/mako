<?php

/**
 * SesionOperador form base class.
 *
 * @method SesionOperador getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseSesionOperadorForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'          => new sfWidgetFormInputHidden(),
				'centro_id'   => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'ip'          => new sfWidgetFormInputText(),
				'usuario_id'  => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'usuario'     => new sfWidgetFormInputText(),
				'fecha_login' => new sfWidgetFormDateTime(),
		));

		$this->setValidators(array(
				'id'          => new sfValidatorPropelChoice(array('model' => 'SesionOperador', 'column' => 'id', 'required' => false)),
				'centro_id'   => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id', 'required' => false)),
				'ip'          => new sfValidatorString(array('max_length' => 15)),
				'usuario_id'  => new sfValidatorPropelChoice(array('model' => 'Usuario', 'column' => 'id', 'required' => false)),
				'usuario'     => new sfValidatorString(array('max_length' => 90)),
				'fecha_login' => new sfValidatorDateTime(),
		));

		$this->widgetSchema->setNameFormat('sesion_operador[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'SesionOperador';
	}


}
