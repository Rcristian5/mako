<?php

/**
 * ProductoCurso form base class.
 *
 * @method ProductoCurso getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseProductoCursoForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'producto_id' => new sfWidgetFormInputHidden(),
				'curso_id'    => new sfWidgetFormInputHidden(),
		));

		$this->setValidators(array(
				'producto_id' => new sfValidatorPropelChoice(array('model' => 'Producto', 'column' => 'id', 'required' => false)),
				'curso_id'    => new sfValidatorPropelChoice(array('model' => 'Curso', 'column' => 'id', 'required' => false)),
		));

		$this->widgetSchema->setNameFormat('producto_curso[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'ProductoCurso';
	}


}
