<?php

/**
 * PromocionReglaci form base class.
 *
 * @method PromocionReglaci getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePromocionReglaciForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'               => new sfWidgetFormInputHidden(),
				'promocion_id'     => new sfWidgetFormPropelChoice(array('model' => 'Promocion', 'add_empty' => true)),
				'sexo'             => new sfWidgetFormInputText(),
				'edad_de'          => new sfWidgetFormInputText(),
				'edad_a'           => new sfWidgetFormInputText(),
				'nivel_estudio'    => new sfWidgetFormPropelChoice(array('model' => 'NivelEstudio', 'add_empty' => true)),
				'recomendados'     => new sfWidgetFormInputText(),
				'centro_alta_id'   => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'centro_venta_id'  => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'municipio_centro' => new sfWidgetFormInputText(),
				'empresa_id'       => new sfWidgetFormPropelChoice(array('model' => 'Empresa', 'add_empty' => true)),
				'discapacidad_id'  => new sfWidgetFormPropelChoice(array('model' => 'Discapacidad', 'add_empty' => true)),
		));

		$this->setValidators(array(
				'id'               => new sfValidatorPropelChoice(array('model' => 'PromocionReglaci', 'column' => 'id', 'required' => false)),
				'promocion_id'     => new sfValidatorPropelChoice(array('model' => 'Promocion', 'column' => 'id', 'required' => false)),
				'sexo'             => new sfValidatorString(array('max_length' => 1, 'required' => false)),
				'edad_de'          => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
				'edad_a'           => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
				'nivel_estudio'    => new sfValidatorPropelChoice(array('model' => 'NivelEstudio', 'column' => 'id', 'required' => false)),
				'recomendados'     => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
				'centro_alta_id'   => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id', 'required' => false)),
				'centro_venta_id'  => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id', 'required' => false)),
				'municipio_centro' => new sfValidatorString(array('max_length' => 256, 'required' => false)),
				'empresa_id'       => new sfValidatorPropelChoice(array('model' => 'Empresa', 'column' => 'id', 'required' => false)),
				'discapacidad_id'  => new sfValidatorPropelChoice(array('model' => 'Discapacidad', 'column' => 'id', 'required' => false)),
		));

		$this->widgetSchema->setNameFormat('promocion_reglaci[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'PromocionReglaci';
	}


}
