<?php

/**
 * CategoriaCurso form base class.
 *
 * @method CategoriaCurso getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCategoriaCursoForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'          => new sfWidgetFormInputHidden(),
				'nombre'      => new sfWidgetFormInputText(),
				'siglas'      => new sfWidgetFormInputText(),
				'descripcion' => new sfWidgetFormTextarea(),
				'padre'       => new sfWidgetFormPropelChoice(array('model' => 'CategoriaCurso', 'add_empty' => true)),
				'profundidad' => new sfWidgetFormInputText(),
				'path'        => new sfWidgetFormInputText(),
				'operador_id' => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'activa'      => new sfWidgetFormInputCheckbox(),
				'created_at'  => new sfWidgetFormDateTime(),
				'updated_at'  => new sfWidgetFormDateTime(),
		));

		$this->setValidators(array(
				'id'          => new sfValidatorPropelChoice(array('model' => 'CategoriaCurso', 'column' => 'id', 'required' => false)),
				'nombre'      => new sfValidatorString(array('max_length' => 255)),
				'siglas'      => new sfValidatorString(array('max_length' => 2)),
				'descripcion' => new sfValidatorString(array('max_length' => 255)),
				'padre'       => new sfValidatorPropelChoice(array('model' => 'CategoriaCurso', 'column' => 'id', 'required' => false)),
				'profundidad' => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'path'        => new sfValidatorString(array('max_length' => 255, 'required' => false)),
				'operador_id' => new sfValidatorPropelChoice(array('model' => 'Usuario', 'column' => 'id', 'required' => false)),
				'activa'      => new sfValidatorBoolean(array('required' => false)),
				'created_at'  => new sfValidatorDateTime(array('required' => false)),
				'updated_at'  => new sfValidatorDateTime(array('required' => false)),
		));

		$this->validatorSchema->setPostValidator(
				new sfValidatorPropelUnique(array('model' => 'CategoriaCurso', 'column' => array('nombre')))
		);

		$this->widgetSchema->setNameFormat('categoria_curso[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'CategoriaCurso';
	}


}
