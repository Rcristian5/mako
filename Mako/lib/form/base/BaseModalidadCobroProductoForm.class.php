<?php

/**
 * ModalidadCobroProducto form base class.
 *
 * @method ModalidadCobroProducto getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseModalidadCobroProductoForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'producto_id'  => new sfWidgetFormInputHidden(),
				'cobro_id'     => new sfWidgetFormInputHidden(),
				'numero_pagos' => new sfWidgetFormInputText(),
		));

		$this->setValidators(array(
				'producto_id'  => new sfValidatorPropelChoice(array('model' => 'Producto', 'column' => 'id', 'required' => false)),
				'cobro_id'     => new sfValidatorPropelChoice(array('model' => 'TipoCobro', 'column' => 'id', 'required' => false)),
				'numero_pagos' => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
		));

		$this->widgetSchema->setNameFormat('modalidad_cobro_producto[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'ModalidadCobroProducto';
	}


}
