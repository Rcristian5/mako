<?php

/**
 * ReporteServidores form base class.
 *
 * @method ReporteServidores getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseReporteServidoresForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'centro_id'    => new sfWidgetFormInputHidden(),
				'ultima_fecha' => new sfWidgetFormDate(),
				'created_at'   => new sfWidgetFormDateTime(),
		));

		$this->setValidators(array(
				'centro_id'    => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id', 'required' => false)),
				'ultima_fecha' => new sfValidatorDate(),
				'created_at'   => new sfValidatorDateTime(array('required' => false)),
		));

		$this->widgetSchema->setNameFormat('reporte_servidores[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'ReporteServidores';
	}


}
