<?php

/**
 * RestriccionesSocio form base class.
 *
 * @method RestriccionesSocio getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseRestriccionesSocioForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'                       => new sfWidgetFormInputHidden(),
				'curso_id'                 => new sfWidgetFormPropelChoice(array('model' => 'Curso', 'add_empty' => false)),
				'edad_minima'              => new sfWidgetFormInputText(),
				'edad_maxima'              => new sfWidgetFormInputText(),
				'sexo'                     => new sfWidgetFormInputText(),
				'ocupacion_id'             => new sfWidgetFormPropelChoice(array('model' => 'Ocupacion', 'add_empty' => true)),
				'estudia'                  => new sfWidgetFormInputCheckbox(),
				'nivel_estudio_id'         => new sfWidgetFormPropelChoice(array('model' => 'NivelEstudio', 'add_empty' => true)),
				'habilidad_informatica_id' => new sfWidgetFormPropelChoice(array('model' => 'HabilidadInformatica', 'add_empty' => true)),
				'dominio_ingles_id'        => new sfWidgetFormPropelChoice(array('model' => 'DominioIngles', 'add_empty' => true)),
				'activo'                   => new sfWidgetFormInputCheckbox(),
				'created_at'               => new sfWidgetFormDateTime(),
				'updated_at'               => new sfWidgetFormDateTime(),
		));

		$this->setValidators(array(
				'id'                       => new sfValidatorPropelChoice(array('model' => 'RestriccionesSocio', 'column' => 'id', 'required' => false)),
				'curso_id'                 => new sfValidatorPropelChoice(array('model' => 'Curso', 'column' => 'id')),
				'edad_minima'              => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
				'edad_maxima'              => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
				'sexo'                     => new sfValidatorString(array('max_length' => 1, 'required' => false)),
				'ocupacion_id'             => new sfValidatorPropelChoice(array('model' => 'Ocupacion', 'column' => 'id', 'required' => false)),
				'estudia'                  => new sfValidatorBoolean(array('required' => false)),
				'nivel_estudio_id'         => new sfValidatorPropelChoice(array('model' => 'NivelEstudio', 'column' => 'id', 'required' => false)),
				'habilidad_informatica_id' => new sfValidatorPropelChoice(array('model' => 'HabilidadInformatica', 'column' => 'id', 'required' => false)),
				'dominio_ingles_id'        => new sfValidatorPropelChoice(array('model' => 'DominioIngles', 'column' => 'id', 'required' => false)),
				'activo'                   => new sfValidatorBoolean(array('required' => false)),
				'created_at'               => new sfValidatorDateTime(array('required' => false)),
				'updated_at'               => new sfValidatorDateTime(array('required' => false)),
		));

		$this->widgetSchema->setNameFormat('restricciones_socio[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'RestriccionesSocio';
	}


}
