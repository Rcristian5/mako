<?php

/**
 * Centro form base class.
 *
 * @method Centro getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCentroForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'                  => new sfWidgetFormInputHidden(),
				'ip'                  => new sfWidgetFormInputText(),
				'hostname'            => new sfWidgetFormTextarea(),
				'nombre'              => new sfWidgetFormTextarea(),
				'alias'               => new sfWidgetFormInputText(),
				'razon_social'        => new sfWidgetFormInputText(),
				'id_colonia'          => new sfWidgetFormPropelChoice(array('model' => 'NM01Colonia', 'add_empty' => true)),
				'estado'              => new sfWidgetFormInputHidden(),
				'municipio'           => new sfWidgetFormInputHidden(),
				'colonia'             => new sfWidgetFormInputHidden(),
				'cp'                  => new sfWidgetFormInputText(),
				'calle'               => new sfWidgetFormInputText(),
				'num_ext'             => new sfWidgetFormInputText(),
				'num_int'             => new sfWidgetFormInputText(),
				'lat'                 => new sfWidgetFormInputText(),
				'long'                => new sfWidgetFormInputText(),
				'dirgmaps'            => new sfWidgetFormTextarea(),
				'iva'                 => new sfWidgetFormInputText(),
				'rfc'                 => new sfWidgetFormInputText(),
				'desde'               => new sfWidgetFormDate(),
				'hora_apertura'       => new sfWidgetFormTime(),
				'hora_cierre'         => new sfWidgetFormTime(),
				'pcs_usuario'         => new sfWidgetFormInputText(),
				'pcs_operacion'       => new sfWidgetFormInputText(),
				'key_gmaps'           => new sfWidgetFormTextarea(),
				'activo'              => new sfWidgetFormInputCheckbox(),
				'created_at'          => new sfWidgetFormDateTime(),
				'updated_at'          => new sfWidgetFormDateTime(),
				'alias_reporte'       => new sfWidgetFormInputText(),

				'zona_id'             => new sfWidgetFormPropelChoice(array('model' => 'Zona', 'add_empty' => true)),
				'stock_list'          => new sfWidgetFormPropelChoice(array('multiple' => true, 'model' => 'Producto')),
				'usuario_centro_list' => new sfWidgetFormPropelChoice(array('multiple' => true, 'model' => 'Usuario')),
				'curso_centro_list'   => new sfWidgetFormPropelChoice(array('multiple' => true, 'model' => 'Curso')),
		));

		$this->setValidators(array(
				'id'                  => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id', 'required' => false)),
				'ip'                  => new sfValidatorString(array('max_length' => 15)),
				'hostname'            => new sfValidatorString(),
				'nombre'              => new sfValidatorString(array('required' => false)),
				'alias'               => new sfValidatorString(array('max_length' => 50, 'required' => false)),
				'razon_social'        => new sfValidatorString(array('max_length' => 254, 'required' => false)),
				'id_colonia'          => new sfValidatorPropelChoice(array('model' => 'NM01Colonia', 'column' => 'clna_id', 'required' => true)),
				'estado'              => new sfValidatorString(array('max_length' => 30, 'required' => false)),
				'municipio'           => new sfValidatorString(array('max_length' => 60, 'required' => false)),
				'colonia'             => new sfValidatorString(array('max_length' => 60, 'required' => false)),
				'cp'                  => new sfValidatorString(array('max_length' => 5, 'required' => false)),
				'calle'               => new sfValidatorString(array('max_length' => 60, 'required' => false)),
				'num_ext'             => new sfValidatorString(array('max_length' => 30, 'required' => false)),
				'num_int'             => new sfValidatorString(array('max_length' => 30, 'required' => false)),
				'lat'                 => new sfValidatorString(array('max_length' => 30, 'required' => false)),
				'long'                => new sfValidatorString(array('max_length' => 30, 'required' => false)),
				'dirgmaps'            => new sfValidatorString(array('required' => false)),
				'iva'                 => new sfValidatorNumber(array('required' => false)),
				'rfc'                 => new sfValidatorString(array('max_length' => 20, 'required' => false)),
				'desde'               => new sfValidatorDate(array('required' => false)),
				'hora_apertura'       => new sfValidatorTime(array('required' => false)),
				'hora_cierre'         => new sfValidatorTime(array('required' => false)),
				'pcs_usuario'         => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
				'pcs_operacion'       => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
				'key_gmaps'           => new sfValidatorString(array('required' => false)),
				'activo'              => new sfValidatorBoolean(array('required' => false)),
				'created_at'          => new sfValidatorDateTime(array('required' => false)),
				'updated_at'          => new sfValidatorDateTime(array('required' => false)),
				'alias_reporte'       => new sfValidatorString(array('max_length' => 50, 'required' => false)),

				'zona_id'             => new sfValidatorPropelChoice(array('model' => 'Zona', 'column' => 'id', 'required' => false)),
				'stock_list'          => new sfValidatorPropelChoice(array('multiple' => true, 'model' => 'Producto', 'required' => false)),
				'usuario_centro_list' => new sfValidatorPropelChoice(array('multiple' => true, 'model' => 'Usuario', 'required' => false)),
				'curso_centro_list'   => new sfValidatorPropelChoice(array('multiple' => true, 'model' => 'Curso', 'required' => false)),
		));

		$this->widgetSchema->setNameFormat('centro[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Centro';
	}


	public function updateDefaultsFromObject()
	{
		parent::updateDefaultsFromObject();

		if (isset($this->widgetSchema['stock_list']))
		{
			$values = array();
			foreach ($this->object->getStocks() as $obj)
			{
				$values[] = $obj->getProductoId();
			}

			$this->setDefault('stock_list', $values);
		}

		if (isset($this->widgetSchema['usuario_centro_list']))
		{
			$values = array();
			foreach ($this->object->getUsuarioCentros() as $obj)
			{
				$values[] = $obj->getUsuarioId();
			}

			$this->setDefault('usuario_centro_list', $values);
		}

		if (isset($this->widgetSchema['curso_centro_list']))
		{
			$values = array();
			foreach ($this->object->getCursoCentros() as $obj)
			{
				$values[] = $obj->getCursoId();
			}

			$this->setDefault('curso_centro_list', $values);
		}

	}

	protected function doSave($con = null)
	{
		parent::doSave($con);

		$this->saveStockList($con);
		$this->saveUsuarioCentroList($con);
		$this->saveCursoCentroList($con);
	}

	public function saveStockList($con = null)
	{
		if (!$this->isValid())
		{
			throw $this->getErrorSchema();
		}

		if (!isset($this->widgetSchema['stock_list']))
		{
			// somebody has unset this widget
			return;
		}

		if (null === $con)
		{
			$con = $this->getConnection();
		}

		$c = new Criteria();
		$c->add(StockPeer::CENTRO_ID, $this->object->getPrimaryKey());
		StockPeer::doDelete($c, $con);

		$values = $this->getValue('stock_list');
		if (is_array($values))
		{
			foreach ($values as $value)
			{
				$obj = new Stock();
				$obj->setCentroId($this->object->getPrimaryKey());
				$obj->setProductoId($value);
				$obj->save();
			}
		}
	}

	public function saveUsuarioCentroList($con = null)
	{
		if (!$this->isValid())
		{
			throw $this->getErrorSchema();
		}

		if (!isset($this->widgetSchema['usuario_centro_list']))
		{
			// somebody has unset this widget
			return;
		}

		if (null === $con)
		{
			$con = $this->getConnection();
		}

		$c = new Criteria();
		$c->add(UsuarioCentroPeer::CENTRO_ID, $this->object->getPrimaryKey());
		UsuarioCentroPeer::doDelete($c, $con);

		$values = $this->getValue('usuario_centro_list');
		if (is_array($values))
		{
			foreach ($values as $value)
			{
				$obj = new UsuarioCentro();
				$obj->setCentroId($this->object->getPrimaryKey());
				$obj->setUsuarioId($value);
				$obj->save();
			}
		}
	}

	public function saveCursoCentroList($con = null)
	{
		if (!$this->isValid())
		{
			throw $this->getErrorSchema();
		}

		if (!isset($this->widgetSchema['curso_centro_list']))
		{
			// somebody has unset this widget
			return;
		}

		if (null === $con)
		{
			$con = $this->getConnection();
		}

		$c = new Criteria();
		$c->add(CursoCentroPeer::CENTRO_ID, $this->object->getPrimaryKey());
		CursoCentroPeer::doDelete($c, $con);

		$values = $this->getValue('curso_centro_list');
		if (is_array($values))
		{
			foreach ($values as $value)
			{
				$obj = new CursoCentro();
				$obj->setCentroId($this->object->getPrimaryKey());
				$obj->setCursoId($value);
				$obj->save();
			}
		}
	}

}
