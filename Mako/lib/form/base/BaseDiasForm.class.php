<?php

/**
 * Dias form base class.
 *
 * @method Dias getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseDiasForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'          => new sfWidgetFormInputHidden(),
				'horario_id'  => new sfWidgetFormPropelChoice(array('model' => 'Horario', 'add_empty' => true)),
				'dia'         => new sfWidgetFormDateTime(),
				'hora_inicio' => new sfWidgetFormTime(),
				'hora_fin'    => new sfWidgetFormTime(),
		));

		$this->setValidators(array(
				'id'          => new sfValidatorPropelChoice(array('model' => 'Dias', 'column' => 'id', 'required' => false)),
				'horario_id'  => new sfValidatorPropelChoice(array('model' => 'Horario', 'column' => 'id', 'required' => false)),
				'dia'         => new sfValidatorDateTime(),
				'hora_inicio' => new sfValidatorTime(),
				'hora_fin'    => new sfValidatorTime(),
		));

		$this->widgetSchema->setNameFormat('dias[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Dias';
	}


}
