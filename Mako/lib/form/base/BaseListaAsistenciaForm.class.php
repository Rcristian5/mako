<?php

/**
 * ListaAsistencia form base class.
 *
 * @method ListaAsistencia getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseListaAsistenciaForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'           => new sfWidgetFormInputHidden(),
				'grupo_id'     => new sfWidgetFormPropelChoice(array('model' => 'Grupo', 'add_empty' => false)),
				'curso_id'     => new sfWidgetFormPropelChoice(array('model' => 'Curso', 'add_empty' => false)),
				'alumno_id'    => new sfWidgetFormPropelChoice(array('model' => 'Alumnos', 'add_empty' => false)),
				'fecha_id'     => new sfWidgetFormPropelChoice(array('model' => 'FechasHorario', 'add_empty' => false)),
				'asistencia'   => new sfWidgetFormInputCheckbox(),
				'operador_id'  => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'fecha_tomada' => new sfWidgetFormDateTime(),
				'created_at'   => new sfWidgetFormDateTime(),
				'updated_at'   => new sfWidgetFormDateTime(),
		));

		$this->setValidators(array(
				'id'           => new sfValidatorPropelChoice(array('model' => 'ListaAsistencia', 'column' => 'id', 'required' => false)),
				'grupo_id'     => new sfValidatorPropelChoice(array('model' => 'Grupo', 'column' => 'id')),
				'curso_id'     => new sfValidatorPropelChoice(array('model' => 'Curso', 'column' => 'id')),
				'alumno_id'    => new sfValidatorPropelChoice(array('model' => 'Alumnos', 'column' => 'matricula')),
				'fecha_id'     => new sfValidatorPropelChoice(array('model' => 'FechasHorario', 'column' => 'id')),
				'asistencia'   => new sfValidatorBoolean(),
				'operador_id'  => new sfValidatorPropelChoice(array('model' => 'Usuario', 'column' => 'id', 'required' => false)),
				'fecha_tomada' => new sfValidatorDateTime(array('required' => false)),
				'created_at'   => new sfValidatorDateTime(array('required' => false)),
				'updated_at'   => new sfValidatorDateTime(array('required' => false)),
		));

		$this->widgetSchema->setNameFormat('lista_asistencia[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'ListaAsistencia';
	}


}
