<?php

/**
 * Cancelacion form base class.
 *
 * @method Cancelacion getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCancelacionForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'                   => new sfWidgetFormInputHidden(),
				'centro_id'            => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'operador_id'          => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'orden_id'             => new sfWidgetFormPropelChoice(array('model' => 'Orden', 'add_empty' => true)),
				'orden_cancelacion_id' => new sfWidgetFormPropelChoice(array('model' => 'Orden', 'add_empty' => true)),
				'observaciones'        => new sfWidgetFormTextarea(),
				'created_at'           => new sfWidgetFormDateTime(),
		));

		$this->setValidators(array(
				'id'                   => new sfValidatorPropelChoice(array('model' => 'Cancelacion', 'column' => 'id', 'required' => false)),
				'centro_id'            => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id', 'required' => false)),
				'operador_id'          => new sfValidatorPropelChoice(array('model' => 'Usuario', 'column' => 'id', 'required' => false)),
				'orden_id'             => new sfValidatorPropelChoice(array('model' => 'Orden', 'column' => 'id', 'required' => false)),
				'orden_cancelacion_id' => new sfValidatorPropelChoice(array('model' => 'Orden', 'column' => 'id', 'required' => false)),
				'observaciones'        => new sfValidatorString(array('required' => false)),
				'created_at'           => new sfValidatorDateTime(array('required' => false)),
		));

		$this->widgetSchema->setNameFormat('cancelacion[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Cancelacion';
	}


}
