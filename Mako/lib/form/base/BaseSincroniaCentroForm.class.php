<?php

/**
 * SincroniaCentro form base class.
 *
 * @method SincroniaCentro getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseSincroniaCentroForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'             => new sfWidgetFormInputHidden(),
				'sincronia_id'   => new sfWidgetFormPropelChoice(array('model' => 'Sincronia', 'add_empty' => true)),
				'centro_id'      => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'sincronizado'   => new sfWidgetFormInputCheckbox(),
				'ultimo_mensaje' => new sfWidgetFormTextarea(),
				'intento'        => new sfWidgetFormInputText(),
				'created_at'     => new sfWidgetFormDateTime(),
				'updated_at'     => new sfWidgetFormDateTime(),
		));

		$this->setValidators(array(
				'id'             => new sfValidatorPropelChoice(array('model' => 'SincroniaCentro', 'column' => 'id', 'required' => false)),
				'sincronia_id'   => new sfValidatorPropelChoice(array('model' => 'Sincronia', 'column' => 'id', 'required' => false)),
				'centro_id'      => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id', 'required' => false)),
				'sincronizado'   => new sfValidatorBoolean(array('required' => false)),
				'ultimo_mensaje' => new sfValidatorString(array('required' => false)),
				'intento'        => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
				'created_at'     => new sfValidatorDateTime(array('required' => false)),
				'updated_at'     => new sfValidatorDateTime(array('required' => false)),
		));

		$this->widgetSchema->setNameFormat('sincronia_centro[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'SincroniaCentro';
	}


}
