<?php

/**
 * Relfixes form base class.
 *
 * @method Relfixes getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseRelfixesForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'socio_id'      => new sfWidgetFormInputHidden(),
				'centro_id'     => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'usuario'       => new sfWidgetFormInputText(),
				'fecha_alta'    => new sfWidgetFormDateTime(),
				'uidnumber'     => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'uidnumber_new' => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
		));

		$this->setValidators(array(
				'socio_id'      => new sfValidatorPropelChoice(array('model' => 'Relfixes', 'column' => 'socio_id', 'required' => false)),
				'centro_id'     => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id', 'required' => false)),
				'usuario'       => new sfValidatorString(array('max_length' => 255)),
				'fecha_alta'    => new sfValidatorDateTime(array('required' => false)),
				'uidnumber'     => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id', 'required' => false)),
				'uidnumber_new' => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id', 'required' => false)),
		));

		$this->validatorSchema->setPostValidator(
				new sfValidatorPropelUnique(array('model' => 'Relfixes', 'column' => array('usuario')))
		);

		$this->widgetSchema->setNameFormat('relfixes[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Relfixes';
	}


}
