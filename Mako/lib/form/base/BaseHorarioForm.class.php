<?php

/**
 * Horario form base class.
 *
 * @method Horario getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseHorarioForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'           => new sfWidgetFormInputHidden(),
				'centro_id'    => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => false)),
				'curso_id'     => new sfWidgetFormPropelChoice(array('model' => 'Curso', 'add_empty' => false)),
				'aula_id'      => new sfWidgetFormPropelChoice(array('model' => 'Aula', 'add_empty' => false)),
				'fecha_inicio' => new sfWidgetFormDateTime(),
				'fecha_fin'    => new sfWidgetFormDateTime(),
				'lunes'        => new sfWidgetFormInputCheckbox(),
				'hi_lu'        => new sfWidgetFormTime(),
				'hf_lu'        => new sfWidgetFormTime(),
				'martes'       => new sfWidgetFormInputCheckbox(),
				'hi_ma'        => new sfWidgetFormTime(),
				'hf_ma'        => new sfWidgetFormTime(),
				'miercoles'    => new sfWidgetFormInputCheckbox(),
				'hi_mi'        => new sfWidgetFormTime(),
				'hf_mi'        => new sfWidgetFormTime(),
				'jueves'       => new sfWidgetFormInputCheckbox(),
				'hi_ju'        => new sfWidgetFormTime(),
				'hf_ju'        => new sfWidgetFormTime(),
				'viernes'      => new sfWidgetFormInputCheckbox(),
				'hi_vi'        => new sfWidgetFormTime(),
				'hf_vi'        => new sfWidgetFormTime(),
				'sabado'       => new sfWidgetFormInputCheckbox(),
				'hi_sa'        => new sfWidgetFormTime(),
				'hf_sa'        => new sfWidgetFormTime(),
				'created_at'   => new sfWidgetFormDateTime(),
				'updated_at'   => new sfWidgetFormDateTime(),
		));

		$this->setValidators(array(
				'id'           => new sfValidatorPropelChoice(array('model' => 'Horario', 'column' => 'id', 'required' => false)),
				'centro_id'    => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id')),
				'curso_id'     => new sfValidatorPropelChoice(array('model' => 'Curso', 'column' => 'id')),
				'aula_id'      => new sfValidatorPropelChoice(array('model' => 'Aula', 'column' => 'id')),
				'fecha_inicio' => new sfValidatorDateTime(),
				'fecha_fin'    => new sfValidatorDateTime(),
				'lunes'        => new sfValidatorBoolean(),
				'hi_lu'        => new sfValidatorTime(array('required' => false)),
				'hf_lu'        => new sfValidatorTime(array('required' => false)),
				'martes'       => new sfValidatorBoolean(),
				'hi_ma'        => new sfValidatorTime(array('required' => false)),
				'hf_ma'        => new sfValidatorTime(array('required' => false)),
				'miercoles'    => new sfValidatorBoolean(),
				'hi_mi'        => new sfValidatorTime(array('required' => false)),
				'hf_mi'        => new sfValidatorTime(array('required' => false)),
				'jueves'       => new sfValidatorBoolean(),
				'hi_ju'        => new sfValidatorTime(array('required' => false)),
				'hf_ju'        => new sfValidatorTime(array('required' => false)),
				'viernes'      => new sfValidatorBoolean(),
				'hi_vi'        => new sfValidatorTime(array('required' => false)),
				'hf_vi'        => new sfValidatorTime(array('required' => false)),
				'sabado'       => new sfValidatorBoolean(),
				'hi_sa'        => new sfValidatorTime(array('required' => false)),
				'hf_sa'        => new sfValidatorTime(array('required' => false)),
				'created_at'   => new sfValidatorDateTime(array('required' => false)),
				'updated_at'   => new sfValidatorDateTime(array('required' => false)),
		));

		$this->widgetSchema->setNameFormat('horario[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Horario';
	}


}
