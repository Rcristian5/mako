<?php

/**
 * SesionMac form base class.
 *
 * @method SesionMac getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseSesionMacForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'ip'         => new sfWidgetFormInputHidden(),
				'ip_host'    => new sfWidgetFormInputText(),
				'ip_virtual' => new sfWidgetFormInputText(),
		));

		$this->setValidators(array(
				'ip'         => new sfValidatorPropelChoice(array('model' => 'SesionMac', 'column' => 'ip', 'required' => false)),
				'ip_host'    => new sfValidatorString(array('max_length' => 15, 'required' => false)),
				'ip_virtual' => new sfValidatorString(array('max_length' => 15)),
		));

		$this->validatorSchema->setPostValidator(
				new sfValidatorAnd(array(
						new sfValidatorPropelUnique(array('model' => 'SesionMac', 'column' => array('ip_host'))),
						new sfValidatorPropelUnique(array('model' => 'SesionMac', 'column' => array('ip_virtual'))),
				))
		);

		$this->widgetSchema->setNameFormat('sesion_mac[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'SesionMac';
	}


}
