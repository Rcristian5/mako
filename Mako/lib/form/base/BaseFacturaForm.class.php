<?php

/**
 * Factura form base class.
 *
 * @method Factura getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseFacturaForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'              => new sfWidgetFormInputHidden(),
				'centro_id'       => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'folio_fiscal_id' => new sfWidgetFormPropelChoice(array('model' => 'FolioFiscal', 'add_empty' => true)),
				'folio'           => new sfWidgetFormInputText(),
				'importe'         => new sfWidgetFormInputText(),
				'subtotal'        => new sfWidgetFormInputText(),
				'total'           => new sfWidgetFormInputText(),
				'retencion'       => new sfWidgetFormInputText(),
				'cadena_original' => new sfWidgetFormTextarea(),
				'sello'           => new sfWidgetFormTextarea(),
				'fecha_factura'   => new sfWidgetFormDateTime(),
				'updated_at'      => new sfWidgetFormDateTime(),
		));

		$this->setValidators(array(
				'id'              => new sfValidatorPropelChoice(array('model' => 'Factura', 'column' => 'id', 'required' => false)),
				'centro_id'       => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id', 'required' => false)),
				'folio_fiscal_id' => new sfValidatorPropelChoice(array('model' => 'FolioFiscal', 'column' => 'id', 'required' => false)),
				'folio'           => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
				'importe'         => new sfValidatorNumber(array('required' => false)),
				'subtotal'        => new sfValidatorNumber(array('required' => false)),
				'total'           => new sfValidatorNumber(array('required' => false)),
				'retencion'       => new sfValidatorNumber(array('required' => false)),
				'cadena_original' => new sfValidatorString(array('required' => false)),
				'sello'           => new sfValidatorString(array('required' => false)),
				'fecha_factura'   => new sfValidatorDateTime(array('required' => false)),
				'updated_at'      => new sfValidatorDateTime(array('required' => false)),
		));

		$this->widgetSchema->setNameFormat('factura[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Factura';
	}


}
