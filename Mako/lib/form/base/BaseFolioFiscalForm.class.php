<?php

/**
 * FolioFiscal form base class.
 *
 * @method FolioFiscal getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseFolioFiscalForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'                => new sfWidgetFormInputHidden(),
				'centro_id'         => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'serie'             => new sfWidgetFormInputText(),
				'no_aprobacion'     => new sfWidgetFormInputText(),
				'ano_aprobacion'    => new sfWidgetFormInputText(),
				'serie_certificado' => new sfWidgetFormInputText(),
				'folio_inicial'     => new sfWidgetFormInputText(),
				'folio_final'       => new sfWidgetFormInputText(),
				'folio_actual'      => new sfWidgetFormInputText(),
				'activo'            => new sfWidgetFormInputCheckbox(),
				'num_certificado'   => new sfWidgetFormInputText(),
				'created_at'        => new sfWidgetFormDateTime(),
				'updated_at'        => new sfWidgetFormDateTime(),
		));

		$this->setValidators(array(
				'id'                => new sfValidatorPropelChoice(array('model' => 'FolioFiscal', 'column' => 'id', 'required' => false)),
				'centro_id'         => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id', 'required' => false)),
				'serie'             => new sfValidatorString(array('max_length' => 30, 'required' => false)),
				'no_aprobacion'     => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
				'ano_aprobacion'    => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
				'serie_certificado' => new sfValidatorString(array('max_length' => 30, 'required' => false)),
				'folio_inicial'     => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
				'folio_final'       => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
				'folio_actual'      => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647, 'required' => false)),
				'activo'            => new sfValidatorBoolean(array('required' => false)),
				'num_certificado'   => new sfValidatorString(array('max_length' => 60, 'required' => false)),
				'created_at'        => new sfValidatorDateTime(array('required' => false)),
				'updated_at'        => new sfValidatorDateTime(array('required' => false)),
		));

		$this->widgetSchema->setNameFormat('folio_fiscal[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'FolioFiscal';
	}


}
