<?php

/**
 * CategoriaReporte form base class.
 *
 * @method CategoriaReporte getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCategoriaReporteForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'id'                              => new sfWidgetFormInputHidden(),
				'nombre'                          => new sfWidgetFormInputText(),
				'activo'                          => new sfWidgetFormInputCheckbox(),
				'created_at'                      => new sfWidgetFormDateTime(),
				'updated_at'                      => new sfWidgetFormDateTime(),
				'categoria_reporte_producto_list' => new sfWidgetFormPropelChoice(array('multiple' => true, 'model' => 'Producto')),
		));

		$this->setValidators(array(
				'id'                              => new sfValidatorPropelChoice(array('model' => 'CategoriaReporte', 'column' => 'id', 'required' => false)),
				'nombre'                          => new sfValidatorString(array('max_length' => 255)),
				'activo'                          => new sfValidatorBoolean(array('required' => false)),
				'created_at'                      => new sfValidatorDateTime(array('required' => false)),
				'updated_at'                      => new sfValidatorDateTime(array('required' => false)),
				'categoria_reporte_producto_list' => new sfValidatorPropelChoice(array('multiple' => true, 'model' => 'Producto', 'required' => false)),
		));

		$this->validatorSchema->setPostValidator(
				new sfValidatorPropelUnique(array('model' => 'CategoriaReporte', 'column' => array('nombre')))
		);

		$this->widgetSchema->setNameFormat('categoria_reporte[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'CategoriaReporte';
	}


	public function updateDefaultsFromObject()
	{
		parent::updateDefaultsFromObject();

		if (isset($this->widgetSchema['categoria_reporte_producto_list']))
		{
			$values = array();
			foreach ($this->object->getCategoriaReporteProductos() as $obj)
			{
				$values[] = $obj->getProductoId();
			}

			$this->setDefault('categoria_reporte_producto_list', $values);
		}

	}

	protected function doSave($con = null)
	{
		parent::doSave($con);

		$this->saveCategoriaReporteProductoList($con);
	}

	public function saveCategoriaReporteProductoList($con = null)
	{
		if (!$this->isValid())
		{
			throw $this->getErrorSchema();
		}

		if (!isset($this->widgetSchema['categoria_reporte_producto_list']))
		{
			// somebody has unset this widget
			return;
		}

		if (null === $con)
		{
			$con = $this->getConnection();
		}

		$c = new Criteria();
		$c->add(CategoriaReporteProductoPeer::CATEGORIA_ID, $this->object->getPrimaryKey());
		CategoriaReporteProductoPeer::doDelete($c, $con);

		$values = $this->getValue('categoria_reporte_producto_list');
		if (is_array($values))
		{
			foreach ($values as $value)
			{
				$obj = new CategoriaReporteProducto();
				$obj->setCategoriaId($this->object->getPrimaryKey());
				$obj->setProductoId($value);
				$obj->save();
			}
		}
	}

}
