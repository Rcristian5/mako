<?php

/**
 * ReporteVentaCursos form base class.
 *
 * @method ReporteVentaCursos getObject() Returns the current form's model object
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseReporteVentaCursosForm extends BaseFormPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'centro_id'        => new sfWidgetFormInputHidden(),
				'fecha'            => new sfWidgetFormInputHidden(),
				'detalle_orden_id' => new sfWidgetFormInputHidden(),
				'categoria_id'     => new sfWidgetFormInputText(),
				'curso_id'         => new sfWidgetFormInputText(),
				'socio_id'         => new sfWidgetFormInputText(),
				'monto'            => new sfWidgetFormInputText(),
				'cantidad'         => new sfWidgetFormInputText(),
				'siglas'           => new sfWidgetFormInputText(),
				'categoria_nombre' => new sfWidgetFormInputText(),
		));

		$this->setValidators(array(
				'centro_id'        => new sfValidatorPropelChoice(array('model' => 'Centro', 'column' => 'id', 'required' => false)),
				'fecha'            => new sfValidatorPropelChoice(array('model' => 'ReporteVentaCursos', 'column' => 'fecha', 'required' => false)),
				'detalle_orden_id' => new sfValidatorPropelChoice(array('model' => 'ReporteVentaCursos', 'column' => 'detalle_orden_id', 'required' => false)),
				'categoria_id'     => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'curso_id'         => new sfValidatorInteger(array('min' => -9.2233720368548E+18, 'max' => 9.2233720368548E+18, 'required' => false)),
				'socio_id'         => new sfValidatorInteger(array('min' => -9.2233720368548E+18, 'max' => 9.2233720368548E+18, 'required' => false)),
				'monto'            => new sfValidatorNumber(),
				'cantidad'         => new sfValidatorInteger(array('min' => -2147483648, 'max' => 2147483647)),
				'siglas'           => new sfValidatorString(array('max_length' => 60, 'required' => false)),
				'categoria_nombre' => new sfValidatorString(array('max_length' => 120, 'required' => false)),
		));

		$this->widgetSchema->setNameFormat('reporte_venta_cursos[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'ReporteVentaCursos';
	}


}
