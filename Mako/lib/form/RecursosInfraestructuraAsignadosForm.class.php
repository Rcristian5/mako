<?php

/**
 * RecursosInfraestructuraAsignados form.
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: RecursosInfraestructuraAsignadosForm.class.php,v 1.2 2010/03/17 20:08:01 david Exp $
 */
class RecursosInfraestructuraAsignadosForm extends BaseRecursosInfraestructuraAsignadosForm
{
	public function configure()
	{
		unset($this['id'], $this['activo'], $this['created_at'], $this['updated_at'], $this['operador_id']);
	}
}
