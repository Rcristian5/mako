<?php

/**
 * PromocionRegla form.
 *
 * @package    mako
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: PromocionReglaForm.class.php,v 1.3 2011/01/02 04:08:16 eorozco Exp $
 */
class PromocionReglaForm extends BasePromocionReglaForm
{
	public function configure()
	{
		 
		unset(
				$this['created_at'],
				$this['updated_at'],
				$this['promocion_list']
		);

		$this->setWidget('promocion_id',new sfWidgetFormInputHidden());

		//sexo
		$this->setWidget('sexo', new sfWidgetFormSelect($options = array('choices' => array(null=>'-- Indistinto','F' => '1. Femenino', 'M' => '2. Masculino'))));
		$this->setWidget('promocion_id',new sfWidgetFormInputHidden());

		$this->widgetSchema['edad_de']->setAttributes(array('value' => '1', 'filtro'=>'numerico', 'requerido'=>1, 'size'=>3,'maxlength'=>'3'));
		$this->widgetSchema['edad_a']->setAttributes(array('value' => '99', 'filtro'=>'numerico', 'requerido'=>1, 'size'=>3,'maxlength'=>'3'));
		$this->widgetSchema['edad_de']->setOption('label','Edad desde');
		$this->widgetSchema['edad_a']->setOption('label','Edad hasta');

		$this->widgetSchema['recomendados_de']->setOption('label','Reecomendados desde');
		$this->widgetSchema['recomendados_de']->setAttributes(array('filtro'=>'numerico', 'size'=>3,'maxlength'=>'6'));

		$this->widgetSchema['recomendados_a']->setOption('label','Reecomendados hasta');
		$this->widgetSchema['recomendados_a']->setAttributes(array('filtro'=>'numerico', 'size'=>3,'maxlength'=>'6'));

		$this->setWidget('estudia', new sfWidgetFormSelect($options = array('choices' => array(null=>'-- Indistinto', '1' => '1. Si', '0' => '2. No'))));
		$this->setWidget('trabaja', new sfWidgetFormSelect($options = array('choices' => array(null=>'-- Indistinto', '1' => '1. Si', '0' => '2. No'))));

		$this->widgetSchema['nivel_estudio']->setOption('add_empty','-- Indistinto');
		$this->widgetSchema['centro_alta_id']->setOption('add_empty','-- Indistinto');
		$this->widgetSchema['centro_venta_id']->setOption('add_empty','-- Indistinto');
		$this->widgetSchema['discapacidad_id']->setOption('add_empty','-- Indistinto');
		$this->widgetSchema['habilidad_informatica_id']->setOption('add_empty','-- Indistinto');
		$this->widgetSchema['dominio_ingles_id']->setOption('add_empty','-- Indistinto');
		$this->widgetSchema['profesion_id']->setOption('add_empty','-- Indistinto');
		$this->widgetSchema['ocupacion_id']->setOption('add_empty','-- Indistinto');
		$this->widgetSchema['posicion_id']->setOption('add_empty','-- Indistinto');
		$this->widgetSchema['beca_id']->setOption('add_empty','-- Indistinto');
		//$this->widgetSchema['nivel_estudio']->setOption('add_empty','-- Indistinto');

	}
}
