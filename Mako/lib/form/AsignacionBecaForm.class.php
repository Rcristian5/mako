<?php

/**
 * AsignacionBeca form.
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: AsignacionBecaForm.class.php,v 1.1 2010/11/04 00:19:10 eorozco Exp $
 */
class AsignacionBecaForm extends BaseAsignacionBecaForm
{
	public function configure()
	{
		unset(
				$this['centro_id'],
				$this['fecha_solicitud'],
				$this['fecha_otorgado'],
				$this['registro_operador_id'],
				$this['otorga_operador_id']
		);
		 
		$this->setWidget('socio_id',new sfWidgetFormInputHidden());
		$this->validatorSchema['beca_id']->setOption('required', true);
		$this->validatorSchema['beca_id']->setMessage('required', 'Debe seleccionar la beca solicitada.');
		 
		$this->validatorSchema['socio_id']->setOption('required', true);
		$this->validatorSchema['socio_id']->setMessage('required', 'Debe seleccionar el socio que solicita la beca.');

		$this->validatorSchema->setPostValidator(
				new sfValidatorPropelUnique(
						array('model' => 'AsignacionBeca', 'column' => array('socio_id', 'beca_id'))
						,array('invalid' => 'Ya existe una solicitud del socio a esta beca.')
				)

		);
		 
		 
	}

}
