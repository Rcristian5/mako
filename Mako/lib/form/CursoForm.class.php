<?php

/**
 * Curso form.
 *
 * @package    mako
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: CursoForm.class.php,v 1.12 2010/09/10 08:04:17 david Exp $
 */
class CursoForm extends BaseCursoForm
{
	public function configure()
	{
		$this->useFields(array('categoria_curso_id', 'clave', 'nombre', 'nombre_corto', 'descripcion', 'perfil_id', 'duracion',
				'asociado_moodle','asociado_rs','seriado', 'calificacion_minima'));
		$this->setWidget('curso_padre_list', new sfWidgetFormChoice(array( 'multiple'=>true,'choices' => CursoPeer::rutaAprendizaje($this->getObject()->getId()))) );
		$this->setValidator('curso_padre_list', new sfValidatorPropelChoice(array('multiple' => true, 'model' => 'Curso', 'required' => false)));
		$this->setValidator('nombre_corto', new sfValidatorString(array('max_length' => 100, 'required' => true )));
		$this->setWidget('categoria_curso_id', new sfWidgetFormSelect(array('choices' => CategoriaCursoPeer::listaCategoriasActivas() )));
		$this->widgetSchema['curso_padre_list']->setOption('renderer_class', 'sfWidgetFormSelectDoubleList');
		$this->widgetSchema['curso_padre_list']->setOption('renderer_options', array('label_associated' => 'Cursos seleecionados',
				'label_unassociated' => 'Lista de Cursos', 'class' => 'listaDoble'));
		 
		$restriccionesHorario = $this->getObject()->getRestriccionesHorarios();
		//Si no hay realcion creamos al menos un objeto de restriccionesHorario
		if (!$restriccionesHorario)
		{
			$horario = new RestriccionesHorario();
			$horario->setCurso($this->getObject());
			$restriccionesHorario = array($horario);
		}
		//Se recorre el arreglo que debe contener al menos un objeto restriccionesHorario
		foreach ($restriccionesHorario as $horario)
		{
			$horarioForm = new RestriccionesHorarioForm($horario);
			$this->embedForm('rh', $horarioForm);
		}

		$restriccionesSocio = $this->getObject()->getRestriccionesSocios();
		//Si no hay realcion creamos al menos un objeto de restriccionessocio
		if (!$restriccionesSocio)
		{
			$socio = new RestriccionesSocio();
			$socio->setCurso($this->getObject());
			$restriccionesSocio = array($socio);
		}
		//Se recorre el arreglo que debe contener al menos un objeto restriccionesSocio
		foreach ($restriccionesSocio as $socio)
		{
			//print_r($socio);
			$socioForm = new RestriccionesSocioForm($socio);
			$this->embedForm('rs', $socioForm);
		}

		$this->setValidator('duracion', new sfValidatorInteger(array('min' => 1, 'max' => 9999), array('max'=>'La duración mínima es de 1 hora', 'min'=>'La duración máxima es de 9999 horas')));
		$this->setValidator('calificacion_minima', new sfValidatorInteger(array('min' => 1, 'max' => 10, 'required' => false), array('max'=>'La calificación mínima es de 1', 'min'=>'La calificación máxima es de 10')));

		$this->validatorSchema->setPostValidator(
				new sfValidatorAnd(array(
						new sfValidatorPropelUnique(array('model' => 'Curso', 'column' => array('nombre')),array('invalid'=>'Este nombre ya ha sido asignado a otro curso')),
						new sfValidatorPropelUnique(array('model' => 'Curso', 'column' => array('clave')),array('invalid'=>'Esta clave ya ha sido asignada a otro curso')),
				))
		);
	}

	public function updateDefaultsFromObject()
	{
		parent::updateDefaultsFromObject();

		if (isset($this->widgetSchema['curso_padre_list']))
		{
			$values = array();
			foreach ($this->object->getRutaAprendizajesRelatedByCursoId() as $obj)
			{
				$values[] = $obj->getPadreId();
			}

			$this->setDefault('curso_padre_list', $values);
		}
	}


	public function saveCursoPadreList($con = null)
	{
		if (!$this->isValid())
		{
			throw $this->getErrorSchema();
		}

		if (!isset($this->widgetSchema['curso_padre_list']))
		{
			// somebody has unset this widget
			return;
		}

		if (null === $con)
		{
			$con = $this->getConnection();
		}
		$c = new Criteria();
		$c->add(RutaAprendizajePeer::CURSO_ID, $this->object->getPrimaryKey());
		RutaAprendizajePeer::doDelete($c, $con);
		//Si serido esta seleccionado de guarda la lista
		if($this->getValue('seriado'))
		{
			$values = $this->getValue('curso_padre_list');
			if (is_array($values))
			{
				foreach ($values as $value)
				{
					$obj = new RutaAprendizaje();
					$obj->setCursoId($this->object->getPrimaryKey());
					$obj->setPadreId($value);
					$obj->save();
				}
			}
		}

	}
}
