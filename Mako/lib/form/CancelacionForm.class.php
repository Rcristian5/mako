<?php

/**
 * Cancelacion form.
 *
 * @package    mako
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: CancelacionForm.class.php,v 1.5 2010/10/11 08:03:51 eorozco Exp $
 */
class CancelacionForm extends BaseCancelacionForm
{
	public function configure()
	{
		unset(
				$this['created_at'],
				$this['orden_id'],
				$this['orden_cancelacion_id'],
				$this['operador_id']
		);
		 
		if(sfConfig::get('app_centro_actual_id') != sfConfig::get('app_central_id'))
		{
			unset($this['centro_id']);
		}

		$this->setWidgets(array(
				'folio_a_cancelar'    => new sfWidgetFormInput(),
				'id_transaccion'    => new sfWidgetFormInput(),
				'observaciones' => new sfWidgetFormTextArea()
		));
		 
		$this->widgetSchema['folio_a_cancelar']->setAttributes(array('requerido' => 1,'filtro'=>'numerico','size'=>'9','maxlenght'=>'15'));
		$this->widgetSchema['folio_a_cancelar']->setOption('label','Folio de venta');
		$this->widgetSchema['id_transaccion']->setAttributes(array('requerido' => 1,'filtro'=>'numerico','size'=>'16','maxlenght'=>'20'));
		$this->widgetSchema['id_transaccion']->setOption('label','ID transacción');
		// Reingenieria Mako C. ID 12. Responsable:  FE. Fecha: 21-01-2014.  Descripción del cambio: Se agrega atributo campo obligatorio y se cambia el titulo de etiqueta.
		// Reingenieria Mako C. ID 54. Responsable:  FE. Fecha: 07-04-2014.  Descripción del cambio: Se agrega filtro del tipo texto
		$this->widgetSchema['observaciones']->setOption('label','Razón de la cancelación');
		$this->widgetSchema['observaciones']->setAttributes(array('requerido' => 1,'rows'=>'4', 'cols'=>'50','filtro' => 'texto','maxlength'=>'500'));
		// Fecha: 07-04-2014 Fin
		// Fecha: 21-01-2014. - Fin.
	}
}
