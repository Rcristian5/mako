<?php

/**
 * RecursoInfraestructura form.
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: RecursoInfraestructuraForm.class.php,v 1.6 2011/01/06 00:14:01 marcela Exp $
 */
class RecursoInfraestructuraForm extends BaseRecursoInfraestructuraForm
{
	public function configure()
	{
		unset($this['id'],$this['activo'],$this['operador_id'],$thi['created_at'],$this['updated_at'], $this['recursos_infraestructura_asignados_list']);
		$this->embedForm('recursoCompuesta', new RecursoCompuestaForm());
	}
	//Cambiamos el metodo que guarda los formularios embebidos para no guardarlos si son nulos
	public function saveEmbeddedForms($con = null, $forms = null)
	{
		$forms = $this->embeddedForms;

		if(($this->values['recursoCompuesta']['compuesta_id_1']=='') && ($this->values['recursoCompuesta']['compuesta_id_1']==''))
		{
			unset($forms['recursoCompuesta']);
		}
		 
		return parent::saveEmbeddedForms($con, $forms);
	}
}
