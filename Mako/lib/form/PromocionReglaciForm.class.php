<?php

/**
 * PromocionReglaci form.
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: PromocionReglaciForm.class.php,v 1.1 2010/03/19 01:53:52 eorozco Exp $
 */
class PromocionReglaciForm extends BasePromocionReglaciForm
{
	public function configure()
	{
		unset(
				$this['recomendados'],
				$this['centro_alta_id'],
				$this['centro_venta_id'],
				$this['municipio_centro']
		);

		$this->setWidget('promocion_id',new sfWidgetFormInputHidden());

		//sexo
		$this->setWidget('sexo', new sfWidgetFormSelect($options = array('choices' => array(''=>null,'F' => '1. Femenino', 'M' => '2. Masculino'))));
		$this->setWidget('promocion_id',new sfWidgetFormInputHidden());
	}
}
