<?php

/**
 * Salida form.
 *
 * @package    mako
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: SalidaForm.class.php,v 1.3 2010/03/02 07:25:39 eorozco Exp $
 */
class SalidaForm extends BaseSalidaForm
{
	public function configure()
	{
		unset(
				$this['created_at'],
				$this['folio'],
				$this['updated_at'],
				$this['orden_id'],
				$this['operador_id']
		);

		if(sfConfig::get('app_centro_actual_id') != sfConfig::get('app_central_id'))
		{
			unset($this['centro_id']);
			unset($this['ip_caja']);
		}


		$this->widgetSchema['monto']->setAttributes(array('requerido' => 1,'filtro'=>'numerico','size'=>'6','maxlenght'=>'6'));
		$this->widgetSchema['tipo_id']->setAttributes(array('requerido' => 1));
		$this->widgetSchema['tipo_id']->setOption('default',1);
		// Reingenieria Mako C. ID 45. Responsable: FE. Fecha: 10-04-2014. Descripción del cambio: Se oculta folio
		$this->widgetSchema['folio_docto_aval']->setOption('label',' ');
		$this->widgetSchema['folio_docto_aval']->setAttributes(array('hidden'=>'true'));
		// Fecha: 10-04-2014 Fin
		// Reingenieria Mako C. ID 57. Responsable:  FE. Fecha: 07-04-2014.  Descripción del cambio: Se agrega filtro del tipo texto
		$this->widgetSchema['observaciones']->setAttributes(array('rows'=>'4', 'cols'=>'50','filtro' => 'texto','maxlength'=>'500'));
		// Fecha: 07-04-2014 Fin


		// Reingenieria Mako C. ID 13. Responsable:  FE. Fecha: 26-02-2014.  Descripción del cambio: Se valida 'Salida de caja' en cero.
		$this->validatorSchema['monto'] = new sfValidatorAnd(array(
				$this->validatorSchema['monto'],
				new sfValidatorNumber(array('min' => '0'),array('min'=>'"%value%" no es el mínimo permitido para dar salida.'))),
				array('required'=>true)
		);
		// Fecha: 14-01-2014. Fin
	}
}
