<?php

/**
 * PromocionCupon form.
 *
 * @package    mako
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: PromocionCuponForm.class.php,v 1.9 2011/02/04 04:16:04 marcela Exp $
 */
class PromocionCuponForm extends BasePromocionCuponForm
{
	public function configure()
	{
		unset(
				$this['created_at'],
				$this['updated_at'],
				$this['creado_por']
		);
		 
		if(!$this->getObject()->isNew())
		{
			unset($this['cantidad_cupones']);
		}
		else
		{
			/*Reingeniería MAKO Administrar cupones  | Responsable:  FE. - Fecha: 15-01-2014. - ID: 31. - Campo: Pantalla - "Generar nueva promoción de cupones".
			 Descripción del cambio: El tamaño de campo indefinido, se coloco atributo de longitud máxima.*/
			$this->widgetSchema['cantidad_cupones']->setAttributes(array('requerido' => 1, 'filtro' => 'numerico','maxlength'=>'3'));
			/*Fecha: 15-01-2014. Fin.*/
		}

		//Si no es el central se muestra selector de centros
		if(sfConfig::get('app_centro_actual_id') != sfConfig::get('app_central_id'))
		{
			unset($this['centro_id']);
		}

		// Reingenieria Mako C. ID 27. Responsable:  FE. Fecha: 23-01-2014.  Descripción del cambio: Se valido campos de fecha inicial y final.
		$this->widgetSchema['vigente_de'] = new sfWidgetFormInputText();
		$this->widgetSchema['vigente_de']->setOption('label','Vigente desde');
		$this->widgetSchema['vigente_de']->setAttributes(array('requerido' => 1, 'readonly'=>'1'));

		$this->widgetSchema['vigente_a'] = new sfWidgetFormInputText();
		$this->widgetSchema['vigente_a']->setOption('label','Vigente hasta');
		$this->widgetSchema['vigente_a']->setAttributes(array('requerido' => 1, 'readonly'=>'1'));
		// Fecha: 23-01-2014. - FIn.
		//Reingenieria Mako C. ID 47,48,49. Responsable:  FE. Fecha: 07-04-2014.  Descripción del cambio: Se agrega filtro del tipo texto
		$this->widgetSchema['nombre']->setAttributes(array('requerido' => 1,'filtro' => 'texto','maxlength'=>'75'));
		$this->widgetSchema['descripcion']->setAttributes(array('requerido' => 1,'rows'=>'4', 'cols'=>'50','filtro' => 'texto','maxlength'=>'300'));
		// Fecha: 07-04-2014 Fin

		$this->widgetSchema['descuento']->setOption('label','Descuento (%)');
		$this->widgetSchema['descuento']->setAttributes(array('requerido' => 1, 'filtro' => 'numerico','maxlength'=>'3'));

		//$this->widgetSchema['promocion_cupon_producto_list']->setOption('label','Productos aplicables al cupón de descuento.');

		$this->widgetSchema['promocion_cupon_producto_list']->setOption('label','Productos aplicables al cupón de descuento.');
		$this->widgetSchema['promocion_cupon_producto_list']->setAttributes(array('requerido' => 1));




		$this->widgetSchema['promocion_cupon_producto_list']->setOption(
				'renderer_class',
				'sfWidgetFormSelectDoubleList'
		);
		$this->widgetSchema['promocion_cupon_producto_list']->setOption(
				'renderer_options',
				array(
						'label_associated' => 'Productos seleccionados',
						'label_unassociated' => 'Lista de Productos'
				)

		);
		$this->widgetSchema['promocion_cupon_producto_list']->setOption(
				'order_by',array('Nombre','asc')
		);

	 //Mostramos en el select, unicamente los productos activos.
		$c = new Criteria();
		//$c->add(ProductoPeer::ES_SUBPRODUCTO,false);
		$c->add(ProductoPeer::ACTIVO,true);
		$c->addAscendingOrderByColumn(ProductoPeer::NOMBRE);
		 
		//error_log($c->toString());    //$this->Productos = ProductoPeer::doSelect($c);

		$this->widgetSchema['promocion_cupon_producto_list']->setOption('criteria',$c);

	}
}
