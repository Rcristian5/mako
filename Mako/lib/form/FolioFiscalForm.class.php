<?php

/**
 * FolioFiscal form.
 *
 * @package    mako
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: FolioFiscalForm.class.php,v 1.4 2011/02/04 04:16:04 marcela Exp $
 */
class FolioFiscalForm extends BaseFolioFiscalForm
{
	public function configure()
	{
		unset(
				$this['created_at'],
				$this['updated_at']
		);
		 
		$this->widgetSchema['centro_id']->setAttributes(array('requerido' => 1));
		$this->widgetSchema['centro_id']->setOption(
				'order_by',array('Nombre','asc'),
				'method', 'getLabel'
		);
		$this->widgetSchema['serie']->setAttributes(array('requerido' => 1,'filtro'=>'alfanumerico'));
		$this->widgetSchema['no_aprobacion']->setAttributes(array('requerido' => 1,'filtro'=>'numerico'));
		$this->widgetSchema['ano_aprobacion']->setAttributes(array('requerido' => 1,'filtro'=>'numerico'));
		$this->widgetSchema['serie_certificado']->setAttributes(array('requerido' => 1,'filtro'=>'numerico'));
		$this->widgetSchema['folio_inicial']->setAttributes(array('requerido' => 1,'filtro'=>'numerico'));
		$this->widgetSchema['folio_final']->setAttributes(array('requerido' => 1,'filtro'=>'numerico'));

		$this->widgetSchema['num_certificado']->setAttributes(array('requerido' => 1,'filtro'=>'numerico'));

		$this->widgetSchema['ano_aprobacion']->setOption('label','Año aprobación');
		$this->widgetSchema['no_aprobacion']->setOption('label','Núm aprobación');
		$this->widgetSchema['num_certificado']->setOption('label','Núm certificado');

		 
	}
}
