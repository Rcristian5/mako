<?php

/**
 * Usuario form.
 *
 * @package    mako
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: UsuarioForm.class.php,v 1.8 2011/02/04 04:16:04 marcela Exp $
 */
class UsuarioForm extends BaseUsuarioForm
{

	/**
	 * Se usa para saber si se hace un cambio de nombre de usuario
	 * @var boolean
	 */
	private $cambioNombreUsuario = false;

	/**
	 * Aqui almacenamos el objeto Socio antes del cambio de nombre de usuario.
	 * @var Usuario
	 */
	private $UsuarioAnterior = null;


	public function configure()
	{
		unset(
				$this['created_at'],
				$this['updated_at'],
				$this['operador_id'],
				$this['nombre_completo'],
				$this['grupo_facilitador_list']
		);
		 

		$this->widgetSchema['usuario_centro_list']->setOption('label','Centros asignados');
		$this->widgetSchema['usuario_perfil_list']->setOption('label','Perfil del usuario');
		$this->widgetSchema['usuario_centro_list']->setAttributes(array('requerido' => 1));

		$this->widgetSchema['nombre']->setAttributes(array('requerido' => 1,'filtro'=>'alfabetico'));
		$this->widgetSchema['apepat']->setAttributes(array('requerido' => 1,'filtro'=>'alfabetico'));
		$this->widgetSchema['apemat']->setAttributes(array('filtro'=>'alfabetico'));

		$this->widgetSchema['rol_id']->setAttributes(array('requerido' => 1));

		$this->widgetSchema['clave'] = new sfWidgetFormInputPassword();
		$this->widgetSchema['clave']->setAttributes(array('value'=>$this->getObject()->getClave(),'requerido' => 1,'filtro'=>'alfanumerico','nouppercase'=>'1'));
		$this->widgetSchema['clave_confirmacion'] = new sfWidgetFormInputPassword();
		$this->widgetSchema['clave_confirmacion']->setAttributes(array('value'=>$this->getObject()->getClave(),'requerido' => 1,'filtro'=>'alfanumerico','nouppercase'=>'1'));
		$this->validatorSchema['clave_confirmacion'] = clone $this->validatorSchema['clave'];

		$this->widgetSchema['usuario']->setAttributes(array('requerido' => 1,'filtro'=>'alfanumerico','nouppercase'=>'1'));
		$this->widgetSchema['email']->setAttributes(array('requerido' => 1,'nouppercase'=>'1'));


		$this->widgetSchema['apepat']->setOption('label','Apellido paterno');
		$this->widgetSchema['apemat']->setOption('label','Apellido materno');

		$this->validatorSchema['email'] = new sfValidatorAnd(array(
				$this->validatorSchema['email'],
				new sfValidatorEmail(array(),array('invalid'=>'La direción de correo es invalida.'))),
				array('required'=>true)
		);


		$this->validatorSchema->setPostValidator(
				new sfValidatorCallback(array('callback' => array($this, 'validaUsuarioEnLdap')))
		);


		$this->mergePostValidator(new sfValidatorSchemaCompare('clave',
				sfValidatorSchemaCompare::EQUAL, 'clave_confirmacion',
				array(),
				array('invalid' => 'Las constraseñas deben coincidir.')
		)
		);

		$this->validatorSchema['clave'] = new sfValidatorString (array(
				'min_length' => 6,
				'max_length' => 30
		),
				array(
						'min_length' => 'La contraseña es demasiado corta (6 carácteres mínimo)'
				)
		);

		$this->widgetSchema->moveField('clave_confirmacion',sfWidgetFormSchema::AFTER,'clave');

		$this->widgetSchema['usuario_centro_list']->setOption(
				'order_by',array('Nombre','asc'),
				'method', 'getLabel'
		);

	}


	/**
	 * Regresa el ultimo id existente en la tabla para el objeto de la forma
	 */
	public function getLastId()
	{

		$p = $this->getObject()->getPeer();
		$pkNombre = $p->translateFieldName('id', BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_COLNAME);

		$c = new Criteria();
		$c->addSelectColumn('max('.$pkNombre.') as last');
		error_log($c->toString());
		$sth = $p->doSelectStmt($c);
		if($sth->rowCount()>0)
		{
			$res = $sth->fetch();
			return $res[0];
		}
		return 0;
	}

	/**
	 * Se hace override para insertar el id antes de guardar.
	 * Este metodo se ejecuta antes de asociar los valores del request de la forma al objeto a guardar.
	 * @see lib/vendor/symfony/lib/plugins/sfPropelPlugin/lib/form/sfFormPropel::doUpdateObject()
	 */
	public function doUpdateObject($values)
	{

		//Si no es nuevo el usuario hacemos copia del Usuario original
		//Es necesario porque si cambia el username necesitamos hacer cambios en el ldap y el username es pk en el ldap.
		if(!$this->isNew())
		{
			$this->UsuarioAnterior = $this->getObject()->copy();
		}
		 
		//Seteamos los valores que vienen de la forma
		parent::doUpdateObject($values);
		 
		//Si es un nuevo objeto entonces hacemos el autoncrement, debe ser despues de asociar los valores de la forma
		//para no sobreescribir el valor del id que viene en la forma, que es nulo.
		if($this->isNew() && !$this->getObject()->getId())
		{
			$this->getObject()->setId($this->getLastId() + 1);
		}
		else
		{
			//Si no es nuevo, se trata de una edición, comprobamos si hay un cambio en el nombre de usuario para setear la bandera
			if(in_array(UsuarioPeer::USUARIO,$this->getObject()->getModifiedColumns()))
			{
				$this->cambioNombreUsuario = true;
			}
			else
			{
				//Como no ha cambiado el nombre de usuario no necesitamos la copia del registro original
				$this->UsuarioAnterior = null;
			}
		}

		//Asociamos el id del operador que esta realizando cambios al registro.
		$operador = sfContext::getInstance()->getUser()->getAttribute('usuario')->getId();
		$this->getObject()->setOperadorId($operador);

	}

	/**
	 * Getter de la propiedad de cambio de nombre de usuario
	 * @return boolean true si cambio el campo de nombre de usuario
	 */
	public function cambioNombreUsuario()
	{
		return $this->cambioNombreUsuario;
	}

	/**
	 * Regresa el objeto Usuario anterior al cambio de nombre de usuario
	 * @return Usuario
	 */
	public function getUsuarioAnterior()
	{
		return $this->UsuarioAnterior;
	}


	/**
	 * Callback para el postvalidator.
	 * @param sfValidatorSchema $validator
	 * @param array $values
	 * @throws sfValidatorErrorSchema
	 */
	public function validaUsuarioEnLdap($validator, $values)
	{
		if (($values['usuario'] != $this->getObject()->getUsuario()) )
		{
			if(OperacionLDAP::existeUsuario($values['usuario']) === true)
			{
				$error = new sfValidatorError($validator, 'El nombre de usuario seleccionado ya existe.');
				throw new sfValidatorErrorSchema($validator, array('usuario' => $error));
			}

		}
		return $values;
	}

}