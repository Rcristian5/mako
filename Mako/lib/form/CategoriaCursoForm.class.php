<?php

/**
 * CategoriaCurso form.
 *
 * @package    mako
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: CategoriaCursoForm.class.php,v 1.4 2010/09/10 08:04:17 david Exp $
 */
class CategoriaCursoForm extends BaseCategoriaCursoForm
{
	public function configure()
	{
		$this->useFields(array('padre', 'nombre', 'descripcion', 'siglas','operador_id'));

		$this->setWidget('padre', new sfWidgetFormPropelChoice(array('model' => 'CategoriaCurso', 'add_empty' => 'PRINCIPAL')));
		$this->setWidget('operador_id', new sfWidgetFormInputHidden());

		$this->setDefault('operador_id', $this->getOption('operador_id'));
		$this->validatorSchema->setPostValidator(
				new sfValidatorPropelUnique(array('model' => 'CategoriaCurso', 'column' => array('nombre')), array('invalid'=>'Ya existe una categoría con este <b>nombre</b>'))
		);

	}
}
