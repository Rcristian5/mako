<?php

/**
 * Producto form.
 *
 * @package    mako
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: ProductoForm.class.php,v 1.7 2011/01/06 00:14:02 marcela Exp $
 */
class ProductoForm extends BaseProductoForm
{
	public function configure()
	{

		unset(
				$this['created_at'],
				$this['updated_at'],
				$this['stock_list'],
				$this['promocion_producto_list'],
				$this['modalidad_cobro_producto_list'],
				$this['subproducto_cobranza_list'],
				$this['promocion_cupon_producto_list'],
				$this['producto_curso_list'],
				$this['categoria_reporte_producto_list'],
				$this['es_subproducto']
		);
		 
		//$this->setWidget('producto_paquete_list', new sfWidgetFormPropelChoice(array('multiple' => true, 'model' => 'Producto')));
		$c = new Criteria();
		$c->add(CategoriaProductoPeer::ACTIVO,true);
		$c->addAscendingOrderByColumn(CategoriaProductoPeer::NOMBRE);
		$this->widgetSchema['categoria_id'] = new sfWidgetFormPropelChoice(array('model' => 'CategoriaProducto', 'add_empty' => '-- Seleccione', 'criteria' => $c));
		$this->widgetSchema['categoria_id']->setAttributes(array('requerido' => 1));

		$c = new Criteria();
		$c->add(TipoProductoPeer::ACTIVO,true);
		$c->addAscendingOrderByColumn(TipoProductoPeer::NOMBRE);
		$this->widgetSchema['tipo_id'] = new sfWidgetFormPropelChoice(array('model' => 'TipoProducto', 'add_empty' => '-- Seleccione', 'criteria' => $c));
		$this->widgetSchema['tipo_id']->setAttributes(array('requerido' => 1));

		$c = new Criteria();
		$c->add(UnidadProductoPeer::ACTIVO,true);
		$c->addAscendingOrderByColumn(UnidadProductoPeer::NOMBRE);
		$this->widgetSchema['unidad_id'] = new sfWidgetFormPropelChoice(array('model' => 'UnidadProducto', 'add_empty' => '-- Seleccione', 'criteria' => $c));
		$this->widgetSchema['unidad_id']->setAttributes(array('requerido' => 1));
		/* Reingeniería AP | Responsable: FE. - Fecha: 11-12-2013. - ID: 8,10,14,15,16. - Campo: Codigo, precio de lista, tiempo de uso en pc.
		 Descripción del cambio: Se validaron los campos correspondientes*/
		$this->widgetSchema['codigo']->setAttributes(array('requerido' => 1,'filtro'=>'codigo'));
		$this->widgetSchema['nombre']->setAttributes(array('requerido' => 1, 'nouppercase'=>'1','filtro'=>'alfanumeric'));
		// Reingenieria Mako C. ID 53. Responsable:  FE. Fecha: 07-04-2014.  Descripción del cambio: Se agrega filtro del tipo texto
		$this->widgetSchema['descripcion']->setAttributes(array('requerido' => 1,'rows'=>'4', 'cols'=>'50','filtro' => 'texto','maxlength'=>'500'));
		// Fecha: 07-04-2014 Fin
		$this->widgetSchema['precio_lista']->setAttributes(array('requerido' => 1,'filtro'=>'decimal','maxlength'=>'12'));
		$this->widgetSchema['precio_lista']->setOption('label','Precio de lista ($)');
		$this->widgetSchema['tiempo']->setOption('label','Tiempo de uso en PC (segundos)');
		$this->widgetSchema['tiempo']->setAttributes(array('filtro'=>'entero', "type"=>"hidden"));
		$this->widgetSchema['en_pv']->setOption('label','Visible en PV');
		/*Fecha: 11-12-2013. Fin.*/

		//$this->widgetSchema['producto_paquete_list']->setAttributes(array('style' => 'display:none;'));
		//$this->widgetSchema['producto_paquete_list']->setOption('label','Productos que componen el paquete:');

		//$this->widgetSchema['modalidad_cobro_producto_list']->setOption('label','Modalidades de cobranza');
		//    $this->widgetSchema->setHelp('codigo','Código con el que se reconoce el producto, puede ser el número del código de barras. ');
		//    $this->widgetSchema->setHelp('en_pv','Indica si el producto podrá ser seleccionado por el cajero, o será solo cobrado cuando se asigne en otro módulo.');
	}


	/**
	 * Se hace override para insertar el id antes de guardar.
	 * Este metodo se ejecuta antes de asociar los valores del request de la forma al objeto a guardar.
	 * @see lib/vendor/symfony/lib/plugins/sfPropelPlugin/lib/form/sfFormPropel::doUpdateObject()
	 */
	public function doUpdateObject($values)
	{

		//Seteamos los valores que vienen de la forma
		parent::doUpdateObject($values);
		 
		//Si es un nuevo objeto entonces hacemos el autoncrement, debe ser despues de asociar los valores de la forma
		//para no sobreescribir el valor del id que viene en la forma, que es nulo.
		if($this->isNew() && !$this->getObject()->getId())
		{
			$this->getObject()->setId(ProductoPeer::getLastId() + 1);
		}
	}


}
