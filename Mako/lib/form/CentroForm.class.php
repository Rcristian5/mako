<?php

/**
 * Centro form.
 *
 * @package    mako
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: CentroForm.class.php,v 1.3 2010/10/03 08:56:23 eorozco Exp $
 */
class CentroForm extends BaseCentroForm
{
	public function configure()
	{
		unset(
				$this['created_at'],
				$this['updated_at'],
				$this['dirgmaps'],
				$this['curso_centro_list'],
				$this['stock_list'],
				$this['usuario_centro_list']
		);
		/**
		 * Nuevos agregagos al formulario para manejar los combos en cascada para la direccion
		 *
		 */
		 
		 
		 
		 
		 
		$this->widgetSchema['ip']->setAttributes(array('requerido' => 1,'filtro'=>'alfanumerico','nouppercase'=>1));
		$this->widgetSchema['nombre']->setAttributes(array('requerido' => 1));
		$this->widgetSchema['alias']->setAttributes(array('requerido' => 1,'filtro'=>'alfanumerico','nouppercase'=>1));
		$this->widgetSchema['rfc']->setAttributes(array('requerido' => 1,'filtro'=>'alfanumerico'));
		$this->widgetSchema['razon_social']->setAttributes(array('requerido' => 1,'filtro'=>'alfanumerico'));
		$this->widgetSchema['estado']->setAttributes(array('requerido' => 1,'filtro'=>'alfanumerico'));
		$this->widgetSchema['municipio']->setAttributes(array('requerido' => 1,'filtro'=>'alfanumerico'));
		$this->widgetSchema['colonia']->setAttributes(array('requerido' => 1,'filtro'=>'alfanumerico'));
		$this->widgetSchema['cp']->setAttributes(array('requerido' => 1,'filtro'=>'numerico', 'readonly'=>'readonly'));
		$this->widgetSchema['calle']->setAttributes(array('requerido' => 1,'filtro'=>'alfanumerico'));
		$this->widgetSchema['num_ext']->setAttributes(array('requerido' => 1,'filtro'=>'alfanumerico'));
		$this->widgetSchema['num_int']->setAttributes(array('filtro'=>'alfanumerico'));
		$this->widgetSchema['lat']->setAttributes(array('requerido' => 1,'filtro'=>'numerico'));
		$this->widgetSchema['long']->setAttributes(array('requerido' => 1,'filtro'=>'numerico'));
		 
		$this->widgetSchema['iva']->setAttributes(array('requerido' => 1,'filtro'=>'numerico','maxlength'=>6,'size'=>7));
		$this->widgetSchema['hora_apertura']->setAttributes(array('requerido' => 1,'filtro'=>'numerico'));
		$this->widgetSchema['hora_cierre']->setAttributes(array('requerido' => 1,'filtro'=>'numerico'));
		$this->widgetSchema['pcs_usuario']->setAttributes(array('requerido' => 1,'filtro'=>'numerico'));
		$this->widgetSchema['pcs_operacion']->setAttributes(array('requerido' => 1,'filtro'=>'numerico'));
		 
		$this->widgetSchema['ip']->setOption('label','IP del servidor de centro');
		$this->widgetSchema['razon_social']->setOption('label','Razón social del centro');
		$this->widgetSchema['iva']->setOption('label','I.V.A (%)');
		$this->widgetSchema['rfc']->setOption('label','R.F.C.');
		$this->widgetSchema['desde']->setOption('label','Fecha inauguración');
		$this->widgetSchema['hora_apertura']->setOption('label','Horario de apertura');
		$this->widgetSchema['hora_cierre']->setOption('label','Horario de cierre');
		$this->widgetSchema['pcs_usuario']->setOption('label','Núm PCs para uso de socios');
		$this->widgetSchema['pcs_operacion']->setOption('label','Núm PCs de operación');
		$this->widgetSchema['key_gmaps']->setOption('label','Llave licencia Gmaps');
		 
		 
		/**
		 *
		 * Modificaciones realizadas para implementar el funcionamiento de registro de direccion a partir de combos de
		 * estado, ciudad y municipio.
		 * silvio.bravo@enova.mx
		 * Reingenieria Mako.
		 * 10-Marzo-2014
		 *
		 *
		 */
		 
		$this->setWidget('cmbEstado', new sfWidgetFormPropelChoice(array('model' => 'EntidadFederativa', 'add_empty' => true)));
		$this->widgetSchema['cmbEstado']->setOption('label', "Estado");
		 
		$this->widgetSchema['id_colonia']->setOption('label', "Colonia");
		$coloniaCriteria	= new Criteria();
		$coloniaCriteria->add(NM01ColoniaPeer::CLNA_ID,0);
		$this->widgetSchema['id_colonia']->setOption('criteria', $coloniaCriteria);
		 
		$this->setWidget('cmbMunicipio', new sfWidgetFormPropelChoice(array('model' => 'NM01Municipio', 'add_empty' => true)));
		$this->widgetSchema['cmbMunicipio']->setOption('label', "Municipio");
		$municipioCriteria	= new Criteria();
		$municipioCriteria->add(NM01MunicipioPeer::EDO_ID,0);
		$this->widgetSchema['cmbMunicipio']->setOption('criteria', $municipioCriteria);
		 
		//Agregamos estas 2 validaciones ya que aunque estos parametros no ingresan a la base de datos, al momento de hacer la validacion
		//con los datos que tiene el formulario vs los parametros que debe esperar faltan estos 2 parametros y no se guardaria el form debido a esto.
		 
		 
		 
		$this->validatorSchema['cmbMunicipio']=new sfValidatorInteger(array('min' => 1, 'max' => 2147483647, 'required' => true));
		$this->validatorSchema['cmbEstado']=new sfValidatorInteger(array('min' => 1, 'max' => 2147483647, 'required' => true));
		$this->widgetSchema['cmbMunicipio']->setAttributes(array('requerido' => 1,'filtro'=>'numerico'));
		$this->widgetSchema['cmbEstado']->setAttributes(array('requerido' => 1,'filtro'=>'numerico'));
		$this->widgetSchema['id_colonia']->setAttributes(array('requerido' => 1,'filtro'=>'numerico'));
		 
		//Agregamos useFields para definir el orden en que se mostraran los campos dentro del formulario.
		 
		$this->useFields(
				array(
						"ip",
						"hostname",
						"nombre",
						"alias",
						"razon_social",
						"cmbEstado",
						"cmbMunicipio",
						"id_colonia",
						"estado",
						"municipio",
						"colonia",
						"cp",
						"calle",
						"num_ext",
						"num_int",
						"lat",
						"long",
						"iva",
						"rfc",
						"desde",
						"hora_apertura",
						"hora_cierre",
						"pcs_usuario",
						"pcs_operacion",
						"key_gmaps",
						"activo",
						"alias_reporte",
						"zona_id"
				)
		);

		 
	}

	/**
	 * Se hace override para insertar el id antes de guardar.
	 * Este metodo se ejecuta antes de asociar los valores del request de la forma al objeto a guardar.
	 * @see lib/vendor/symfony/lib/plugins/sfPropelPlugin/lib/form/sfFormPropel::doUpdateObject()
	 */
	public function doUpdateObject($values)
	{

		//Seteamos los valores que vienen de la forma
		parent::doUpdateObject($values);
		 
		//Si es un nuevo objeto entonces hacemos el autoncrement, debe ser despues de asociar los valores de la forma
		//para no sobreescribir el valor del id que viene en la forma, que es nulo.
		if($this->isNew() && !$this->getObject()->getId())
		{
			$this->getObject()->setId($this->getLastId() + 1);
		}

	}

	/**
	 * Regresa el ultimo id existente en la tabla para el objeto de la forma
	 */
	public function getLastId()
	{

		$p = $this->getObject()->getPeer();
		$pkNombre = $p->translateFieldName('id', BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_COLNAME);

		$c = new Criteria();
		$c->add(CentroPeer::ID,sfConfig::get('app_central_id'),Criteria::NOT_EQUAL);
		$c->addSelectColumn('max('.$pkNombre.') as last');
		error_log($c->toString());
		$sth = $p->doSelectStmt($c);
		if($sth->rowCount()>0)
		{
			$res = $sth->fetch();
			return $res[0];
		}
		return 0;
	}
}
