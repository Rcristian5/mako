<?php

/**
 * DetalleOrden form.
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: DetalleOrdenForm.class.php,v 1.2 2010/02/25 07:51:34 eorozco Exp $
 */
class DetalleOrdenForm extends BaseDetalleOrdenForm
{
	public function configure()
	{

		unset(
				$this['orden_id'],
				$this['centro_id'],
				$this['socio_id'],
				$this['promocion_id'],
				$this['tiempo'],
				$this['cantidad'],
				$this['descuento'],
				$this['subtotal'],
				$this['precio_lista']
		);

	}
}
