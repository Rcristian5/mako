<?php

/**
 * SocioClave form.
 * Esta forma es la que se ve en el dash de socio cuando cambia su clave.
 *
 * @package    mako
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: SocioClaveForm.class.php,v 1.1 2010/09/12 05:02:24 eorozco Exp $
 */
class SocioClaveForm extends BaseSocioForm
{

	public function configure()
	{
		 
		$this->useFields(array('clave','id'));
	 $this->widgetSchema['clave'] = new sfWidgetFormInputPassword();
	 $this->widgetSchema['clave_confirmacion']  = new sfWidgetFormInputPassword();
	  
	 $this->validatorSchema['clave_confirmacion'] = clone $this->validatorSchema['clave'];
	 $this->validatorSchema['clave']->setOption('required', true);
	 	
	 $this->mergePostValidator(new sfValidatorSchemaCompare('clave',
	 		sfValidatorSchemaCompare::EQUAL, 'clave_confirmacion',
	 		array(),
	 		array('invalid' => 'Las constraseñas deben coincidir.')));

	 $this->validatorSchema['clave'] = new sfValidatorString (array(
	 		'min_length'  => 8,
	 		'max_length' => 30
	 ),
	 		array(
	 				'min_length' => 'La contraseña es demasiado corta (8 carácteres mínimo)'));


	}
}
