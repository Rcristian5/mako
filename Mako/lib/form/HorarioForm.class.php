<?php

/**
 * Horario form.
 *
 * @package    mako
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: HorarioForm.class.php,v 1.5 2010/10/15 20:37:22 david Exp $
 */
class HorarioForm extends BaseHorarioForm
{
	public function configure()
	{
		$this->useFields(array('centro_id','curso_id','aula_id','fecha_inicio','fecha_fin','lunes','martes','miercoles','jueves','viernes','sabado',
				'hi_lu', 'hf_lu', 'hi_ma', 'hf_ma', 'hi_mi', 'hf_mi', 'hi_ju', 'hf_ju', 'hi_vi', 'hf_vi', 'hi_sa', 'hf_sa' ));

		$centro = CentroPeer::retrieveByPK($this->getOption('centro'));
		$horasInicio = array();
		$horasFin = array();
		for($i = $centro->getHoraApertura('G')+0; $i < $centro->getHoraCierre()+0; $i++)
		{
			if($i < 10)
			{
				$horasInicio[$i] = '0'.$i;
			}
			else
			{
				$horasInicio[$i] = $i;
			}
		}

		for($i = $centro->getHoraApertura('G')+1; $i < $centro->getHoraCierre()+1; $i++)
		{
			if($i < 10)
			{
				$horasFin[$i] = '0'.$i;
			}
			else
			{
				$horasFin[$i] = $i;
			}
		}

		$minutos = array(0=>'00');

		/*
		 * Defaults
		*/
		$this->setDefault('centro_id', $this->getOption('centro'));
		$this->setDefault('curso_id', $this->getOption('curso'));
		$this->setDefault('aula_id', $this->getOption('aula'));

		/*
		 * Comprobamos que el curso no tenga restricciones de horario
		*/
		$curso = CursoPeer::retrieveByPK($this->getOption('curso'));
		if($curso->getRestriccionesHorario())
		{
			$restricciones = $curso->getRestriccionesHorarios();
			foreach ($restricciones as $restriccion)
			{
				/*
				 * Si hay restricciones de horario quitamos las horas a las que no se debe dar un curso
				*/
				if($restriccion->getHoraInicio())
				{
					foreach ($horasInicio as $key => $hora)
					{
						if($key < $restriccion->getHoraInicio('G'))
						{
							unset ($horasInicio[$key]);
						}
					}
					foreach ($horasFin as $key => $hora)
					{
						if($key <= $restriccion->getHoraInicio('G'))
						{
							unset ($horasFin[$key]);
						}
					}
				}
				if($restriccion->getHoraFin())
				{
					foreach ($horasFin as $key => $hora)
					{
						if($key > $restriccion->getHoraFin('G'))
						{
							unset ($horasFin[$key]);
						}
					}
					foreach ($horasInicio as $key => $hora)
					{
						if($key >= $restriccion->getHoraFin('G'))
						{
							unset ($horasInicio[$key]);
						}
					}
				}
			}
		}


		/*
		 * Widtgets
		*/
		$this->setWidget('hi_lu', new sfWidgetFormTime(array('format' => '%hour%','can_be_empty'=>false,'hours' => $horasInicio, 'minutes'=>$minutos)));
		$this->setWidget('hf_lu',  new sfWidgetFormTime(array('format' => '%hour%','can_be_empty'=>false,'hours' => $horasFin, 'minutes'=>$minutos)));
		$this->setWidget('hi_ma',  new sfWidgetFormTime(array('format' => '%hour%','can_be_empty'=>false,'hours' => $horasInicio, 'minutes'=>$minutos)));
		$this->setWidget('hf_ma',  new sfWidgetFormTime(array('format' => '%hour%','can_be_empty'=>false,'hours' => $horasFin, 'minutes'=>$minutos)));
		$this->setWidget('hi_mi',  new sfWidgetFormTime(array('format' => '%hour%','can_be_empty'=>false,'hours' => $horasInicio, 'minutes'=>$minutos)));
		$this->setWidget('hf_mi',  new sfWidgetFormTime(array('format' => '%hour%','can_be_empty'=>false,'hours' => $horasFin, 'minutes'=>$minutos)));
		$this->setWidget('hi_ju',  new sfWidgetFormTime(array('format' => '%hour%','can_be_empty'=>false,'hours' => $horasInicio, 'minutes'=>$minutos)));
		$this->setWidget('hf_ju',  new sfWidgetFormTime(array('format' => '%hour%','can_be_empty'=>false,'hours' => $horasFin, 'minutes'=>$minutos)));
		$this->setWidget('hi_vi',  new sfWidgetFormTime(array('format' => '%hour%','can_be_empty'=>false,'hours' => $horasInicio, 'minutes'=>$minutos)));
		$this->setWidget('hf_vi',  new sfWidgetFormTime(array('format' => '%hour%','can_be_empty'=>false,'hours' => $horasFin, 'minutes'=>$minutos)));
		$this->setWidget('hi_sa',  new sfWidgetFormTime(array('format' => '%hour%','can_be_empty'=>false,'hours' => $horasInicio, 'minutes'=>$minutos)));
		$this->setWidget('hf_sa',  new sfWidgetFormTime(array('format' => '%hour%','can_be_empty'=>false,'hours' => $horasFin, 'minutes'=>$minutos)));
		$this->setWidget('centro_id',  new sfWidgetFormInputHidden());
		$this->setWidget('curso_id',  new sfWidgetFormInputHidden());
		$this->setWidget('aula_id',  new sfWidgetFormInputHidden());
		$this->setWidget('fecha_inicio',  new sfWidgetFormInputHidden());
		$this->setWidget('fecha_fin',  new sfWidgetFormInputHidden());

		if($this->getObject()->isNew())
		{
			$this->setDefault('fecha_inicio', $this->getOption('fecha'));
			switch ($this->getOption('default'))
			{
				case 'Lun';
				$this->setDefault('lunes', true);
				break;
				case 'Mar';
				$this->setDefault('martes', true);
				break;
				case 'Mié';
				$this->setDefault('miercoles', true);
				break;
				case 'Jue';
				$this->setDefault('jueves', true);
				break;
				case 'Vie';
				$this->setDefault('viernes', true);
				break;
				case 'Sáb';
				$this->setDefault('sabado', true);
				break;
			}
		}
		//Habilitamos los campos extras

		$this->validatorSchema->setOption('allow_extra_fields', true);

	}
}
