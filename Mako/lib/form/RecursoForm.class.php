<?php

/**
 * Recurso form.
 *
 * @package    mako
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: RecursoForm.class.php,v 1.3 2010/03/15 22:35:19 david Exp $
 */
class RecursoForm extends BaseRecursoForm
{
	public function configure()
	{
		unset($this['created_at'],
				$this['updated_at'],
				$this['activo'],
				$this['recursos_asignados_list'],
				$this['operador_id']
		);

		$this->widgetSchema['cantidad'] = new sfWidgetFormInput();
		$this->validatorSchema['cantidad'] = new sfValidatorInteger(array('required' => false,'min' => 0, 'max' => 2147483647));
		$this->validatorSchema->setPostValidator(
				new sfValidatorAnd(array(
						new sfValidatorPropelUnique(array('model' => 'Recurso', 'column' => array('nombre')), array('invalid'=>'Este nombre ya ha sido asignado a otro recurso')),
						new sfValidatorPropelUnique(array('model' => 'Recurso', 'column' => array('codigo')), array('invalid'=>'Esta clave ya esta asociada a otro recurso')),
				))
		);
	}
}
