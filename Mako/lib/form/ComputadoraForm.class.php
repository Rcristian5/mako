<?php

/**
 * Computadora form.
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: ComputadoraForm.class.php,v 1.3 2011/01/13 03:55:06 eorozco Exp $
 */
class ComputadoraForm extends BaseComputadoraForm
{
	public function configure()
	{
		 
		$this->widgetSchema['ip']->setAttributes(array('requerido' => 1,'filtro'=>'numerico'));

		$this->widgetSchema['centro_id']->setAttributes(array('requerido' => 1));
		$this->widgetSchema['seccion_id']->setAttributes(array('requerido' => 1));

		$this->widgetSchema['hostname']->setAttributes(array('nouppercase' => 1,'requerido' => 1,'filtro'=>'alfanumerico'));
		$this->widgetSchema['mac_adress']->setAttributes(array('requerido' => 1));
		$this->widgetSchema['tipo_id']->setAttributes(array('requerido' => 1));
		$this->widgetSchema['alias']->setAttributes(array('requerido' => 1));

		$this->widgetSchema['seccion_id']->setOption('label','Sección');

		$c = new Criteria();
		$c2 = new Criteria();
		$centro_id = sfConfig::get('app_centro_actual_id');
		if($centro_id != sfConfig::get('app_central_id'))
		{
			$c->add(CentroPeer::ID,$centro_id);
			$c2->add(SeccionPeer::CENTRO_ID,$centro_id);
		}
		$this->widgetSchema['centro_id']->setOption('criteria',$c);
		$this->widgetSchema['centro_id']->setOption('add_empty',false);
		$this->widgetSchema['seccion_id']->setOption('add_empty',false);
		$this->widgetSchema['seccion_id']->setOption('criteria',$c2);

	}
}
