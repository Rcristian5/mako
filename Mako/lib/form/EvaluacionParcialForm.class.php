<?php

/**
 * EvaluacionParcial form.
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: EvaluacionParcialForm.class.php,v 1.1 2010/09/09 03:57:58 david Exp $
 */
class EvaluacionParcialForm extends BaseEvaluacionParcialForm
{

	public function configure()
	{
		$this->useFields(array('tema_id','curso_id', 'nombre','descripcion','calificacion_minima','calificacion_maxima','porcentaje'));
		$this->setWidget('tema_id',  new sfWidgetFormSelect(array('choices' => TemaPeer::listTemas($this->getOption('curso')))));
		$this->setWidget('curso_id',  new sfWidgetFormInputHidden());
		$this->setValidator('porcentaje', new sfValidatorInteger(array('min' => 1, 'max' => 100), array('max'=>'El porcentaje máximo es 1', 'min'=>'El porcentaje mínimo es 1') ));
		$this->setValidator('calificacion_minima', new sfValidatorNumber(array('min' => 0.01, 'max' => 10.00, 'required' => false), array('max'=>'La calificación mínima-máxima es 10.00', 'min'=>'La calificación mínima-mínima es 0.01') ));
		$this->setValidator('calificacion_maxima', new sfValidatorNumber(array('min' => 0.01, 'max' => 10.00, 'required' => false), array('max'=>'La calificación máxima-máxima es 10.00', 'min'=>'La calificación máxima-máxima es 0.01') ));
		//Se pasa el id del curso por default
		$this->setDefault('curso_id', $this->getOption('curso'));
	}
}
