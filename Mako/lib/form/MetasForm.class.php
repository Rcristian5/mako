<?php

/**
 * Metas form.
 *
 * @package    mako
 * @subpackage form
 * @author     Bixit SA de CV
 * @version    SVN: $Id: MetasForm.class.php,v 1.7 2011/02/04 04:16:04 marcela Exp $
 */
class MetasForm extends BaseMetasForm
{
	public function configure()
	{
		unset(
				$this['fecha_fin'],
				$this['created_at'],
				$this['updated_at'],
				$this['activo'],
				$this['usuario_id']
		);

		//1 day = seconds/hours in day/minutes in hours/seconds in minute)
		//Floor because it will be the lowest integer
		if ($this->isNew()){
			$dateA = time();
			$dateB = mktime(0,0,0,date('m'),date('d'),date("Y")+1);
		}else {
			$dateA = mktime(0,0,0,date('m',strtotime($this->getObject()->getFechaInicio())), date('d',strtotime($this->getObject()->getFechaInicio())), date('Y',strtotime($this->getObject()->getFechaInicio())));
			$dateB = mktime(0,0,0,date('m'),date('d'),date("Y")+1);
		}
		// $dateA = time();
		//$dateB = mktime(0,0,0,date('m'),date('d'),date("Y")+1);
		$startDay = floor($dateA/24/60/60);
		$endDay =  floor($dateB/24/60/60);
		$daysDiff = $endDay-$startDay;

		for($i=0;$i<$daysDiff;$i++)
		{
			//This is the start date plus $i days.
			$day = Date("d-m-Y",$startDay*24*60*60+$i*24*60*60);
			if(date('N',strtotime($day)) == 1){
				//error_log ($day);
				//$fechaFin = date("d-m-Y",strtotime(date("Y-m-d", strtotime($day)) . " +6 days"));
				$semana = date('W',strtotime($day));
				$anio = date('Y',strtotime($day));
				$label = $day.'|'.$semana.'|'.$anio;
				$semanas[$label] = 'Año '. $anio .' - Semana '.$semana . ' - Inicia: '. $day;
				$primerLunes[] =$day;
			}
		}

		//Periodo
		$this->setWidget('Periodo', new sfWidgetFormSelect($options = array('choices' => $semanas),
				$attributes = array('onChange' => 'asignaValores(this.value)')));
			

			


		$this->widgetSchema['centro_id']->setOption('add_empty' , false);
		$this->widgetSchema['centro_id']->setOption(
				'order_by',array('Nombre','asc'),
				'method', 'getLabel'
		);
		//Fecha Inicio hidden
		$this->setWidget('fecha_inicio', new sfWidgetFormInputHidden());
		$this->setWidget('semana', new sfWidgetFormInputHidden());
		$this->setWidget('anio', new sfWidgetFormInputHidden());

		if ($this->isNew()){
			$this->widgetSchema['fecha_inicio']->setAttributes(array('value' => $primerLunes[0]));
			$this->widgetSchema['semana']->setAttributes(array('value' =>  date('W',strtotime($primerLunes[0]))));
			$this->widgetSchema['anio']->setAttributes(array('value' => date('Y',strtotime($primerLunes[0]))));
		}else {
			//error_log (date('d-m-Y', strtotime($this->getObject()->getFechaInicio())).'|'.sprintf("%02s",$this->getObject()->getSemana()).'|'.$this->getObject()->getAnio());
			$this->widgetSchema['Periodo']->setOption('default', date('d-m-Y', strtotime($this->getObject()->getFechaInicio())).'|'.sprintf("%02s",$this->getObject()->getSemana()).'|'.$this->getObject()->getAnio());
		}
		//pinches acentos del pinche alfredo pa sus putos casos de uso
		//Selects categoria
		$this->widgetSchema['categoria_fija_id']->setOption('label','Categoría fija');
		$this->widgetSchema['categoria_fija_id']->setAttributes(array('onChange' => "habilitaSelects('fija')"));
		$this->widgetSchema['categoria_producto_id']->setOption('label','Categoría producto');
		$this->widgetSchema['categoria_producto_id']->setAttributes(array('onChange' => "habilitaSelects('producto')"));
		$this->widgetSchema['categoria_beca_id']->setOption('label','Categoría beca');
		$this->widgetSchema['categoria_beca_id']->setAttributes(array('onChange' => "habilitaSelects('beca')"));
		 
	}

	public function bind(array $taintedValues = null, array $taintedFiles = null) {
		unset($taintedValues['Periodo']) ;
		parent::bind($taintedValues, $taintedFiles);
	}

}
