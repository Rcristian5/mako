  protected function processForm(sfWebRequest $request, sfForm $form)
  {
    $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
    if ($form->isValid())
    {
      $<?php echo $this->getSingularName() ?> = $form->save();


	  $this->getUser()->setFlash('ok', sprintf('Se ha guardado correctamente el registro: %s',$form->getObject()));



<?php if (isset($this->params['route_prefix']) && $this->params['route_prefix']): ?>
      $this->redirect('@<?php echo $this->getUrlForAction('index') ?>?<?php echo $this->getPrimaryKeyUrlParams() ?>);
<?php else: ?>
      $this->redirect('<?php echo $this->getModuleName() ?>/index?<?php echo $this->getPrimaryKeyUrlParams() ?>);
<?php endif; ?>
    }
  }
