[?php use_stylesheets_for_form($form) ?]
[?php use_javascripts_for_form($form) ?]


<div class="ui-widget ui-corner-all sombra">
<div  class="ui-widget-header ui-corner-top tsss" style="padding:5px;">[TITULO FORMA]</div>
<div class="ui-widget-content ui-corner-bottom">

<div  style="margin:15px;">


<?php $form = $this->getFormObject() ?>
<?php if (isset($this->params['route_prefix']) && $this->params['route_prefix']): ?>
[?php echo form_tag_for($form, '@<?php echo $this->params['route_prefix'] ?>') ?]
<?php else: ?>
<form action="[?php echo url_for('<?php echo $this->getModuleName() ?>/'.($form->getObject()->isNew() ? 'create' : 'update').(!$form->getObject()->isNew() ? '?<?php echo $this->getPrimaryKeyUrlParams('$form->getObject()', true) ?> : '')) ?]" method="post" [?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?]>
[?php if (!$form->getObject()->isNew()): ?]
<input type="hidden" name="sf_method" value="put" />
[?php endif; ?]
<?php endif;?>
  <table>
    <tfoot>
      <tr>
        <td colspan="2">
<?php if (!isset($this->params['non_verbose_templates']) || !$this->params['non_verbose_templates']): ?>
          [?php echo $form->renderHiddenFields(false) ?]
<?php endif; ?>
<?php if (isset($this->params['route_prefix']) && $this->params['route_prefix']): ?>
          &nbsp;<a href="[?php echo url_for('<?php echo $this->getUrlForAction('list') ?>') ?]">Regresar al índice</a>
<?php else: ?>
          &nbsp;<a href="[?php echo url_for('<?php echo $this->getModuleName() ?>/index') ?]">Regresar al índice</a>
<?php endif; ?>
          [?php if (!$form->getObject()->isNew()): ?]
<?php if (isset($this->params['route_prefix']) && $this->params['route_prefix']): ?>
            &nbsp;[?php echo link_to('Borrar registro', '<?php echo $this->getUrlForAction('delete') ?>', $form->getObject(), array('method' => 'delete', 'confirm' => '¿Está seguro? Esta acción no se puede deshacer.')) ?]
<?php else: ?>
            &nbsp;[?php echo link_to('Borrar registro', '<?php echo $this->getModuleName() ?>/delete?<?php echo $this->getPrimaryKeyUrlParams('$form->getObject()', true) ?>, array('method' => 'delete', 'confirm' => '¿Está seguro? Esta acción no se puede deshacer.')) ?]
<?php endif; ?>
          [?php endif; ?]
          <input type="submit" value="Guardar registro" />
          <input type="button" value="Cancelar" onclick="location.href='[?php echo url_for('<?php echo $this->getModuleName() ?>/new') ?]'"/>
        </td>
      </tr>
    </tfoot>
    <tbody>
<?php if (isset($this->params['non_verbose_templates']) && $this->params['non_verbose_templates']): ?>
      [?php echo $form ?]
<?php else: ?>
      [?php echo $form->renderGlobalErrors() ?]
<?php foreach ($form as $name => $field): if ($field->isHidden()) continue ?>
      <tr>
        <th>[?php echo $form['<?php echo $name ?>']->renderLabel() ?]</th>
        <td>
          [?php echo $form['<?php echo $name ?>']->renderError() ?]
          [?php echo $form['<?php echo $name ?>'] ?]
        </td>
      </tr>
<?php endforeach; ?>
<?php endif; ?>
    </tbody>
  </table>
</form>


</div>
</div>
</div>