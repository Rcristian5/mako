[?php use_javascript('plugins/jquery.dataTables.js') ?]
[?php use_stylesheet('datatables.css') ?]

<script>

var lT<?php echo $this->getPluralName() ?>;
$(document).ready(function() {

   lT<?php echo $this->getPluralName() ?> = $('#lista-<?php echo $this->getPluralName() ?>').dataTable({
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": true,
        "bJQueryUI": true,
        "sPaginationType": "full_numbers",
        "aaSorting": [[1, 'asc']] 
     });
   
});

</script>

<div  class="ui-widget-header ui-corner-top tsss" style="padding:5px;">[TITULO]</div>
<div class="ui-widget ui-corner-bottom sombra">
<div class="ui-widget-content ui-corner-bottom" style="padding:5px;">

[?php if ($sf_user->hasFlash('ok')): ?]
  <div class="flash_ok ui-state-highlight ts sombra-delgada ui-corner-all" style="padding:5px; margin-top:10px; margin-bottom:10px;">
  	<span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-info"></span>
  	<strong>[?php echo $sf_user->getFlash('ok') ?]</strong>
  </div>
[?php endif; ?]

<br/>
<span style="float: left;" class="ui-icon ui-icon-circle-plus"></span>
<?php if (isset($this->params['route_prefix']) && $this->params['route_prefix']): ?>
  <a href="[?php echo url_for('<?php echo $this->getUrlForAction('new') ?>') ?]">
  Agregar registro
  </a>
<?php else: ?>
  <a href="[?php echo url_for('<?php echo $this->getModuleName() ?>/new') ?]">
  	Agregar registro
  </a>
<?php endif; ?>
<br/>
<br/>


<table id="lista-<?php echo $this->getPluralName() ?>" style="width:100%;">
  <thead>
    <tr>
<?php foreach ($this->getTableMap()->getColumns() as $column): ?>
      <th><?php echo sfInflector::humanize(sfInflector::underscore($column->getPhpName())) ?></th>
<?php endforeach; ?>
    </tr>
  </thead>
  <tbody>
    [?php foreach ($<?php echo $this->getPluralName() ?> as $<?php echo $this->getSingularName() ?>): ?]
    <tr>
<?php foreach ($this->getTableMap()->getColumns() as $column): ?>
<?php if ($column->isPrimaryKey()): ?>
<?php if (isset($this->params['route_prefix']) && $this->params['route_prefix']): ?>
      <td>
      	<a href="[?php echo url_for('<?php echo $this->getUrlForAction(isset($this->params['with_edit']) && $this->params['with_edit'] ? 'edit' : 'edit') ?>', $<?php echo $this->getSingularName() ?>) ?]">
      		<span class="ui-icon  ui-icon-pencil"></span>
      	</a>
      </td>
<?php else: ?>
      <td>
      	<a href="[?php echo url_for('<?php echo $this->getModuleName() ?>/<?php echo isset($this->params['with_show']) && $this->params['with_show'] ? 'show' : 'edit' ?>?<?php echo $this->getPrimaryKeyUrlParams() ?>) ?]">
      		<span class="ui-icon  ui-icon-pencil"></span>
      	</a>
      </td>
<?php endif; ?>
<?php else: ?>
      <td>[?php echo $<?php echo $this->getSingularName() ?>->get<?php echo $column->getPhpName() ?>() ?]</td>
<?php endif; ?>
<?php endforeach; ?>
    </tr>
    [?php endforeach; ?]
  </tbody>
</table>


</div>
</div>

