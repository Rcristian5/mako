<?php

/**
 * GrupoHorario filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseGrupoHorarioFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'grupo_id'   => new sfWidgetFormPropelChoice(array('model' => 'Grupo', 'add_empty' => true)),
				'horario_id' => new sfWidgetFormPropelChoice(array('model' => 'Horario', 'add_empty' => true)),
		));

		$this->setValidators(array(
				'grupo_id'   => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Grupo', 'column' => 'id')),
				'horario_id' => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Horario', 'column' => 'id')),
		));

		$this->widgetSchema->setNameFormat('grupo_horario_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'GrupoHorario';
	}

	public function getFields()
	{
		return array(
				'grupo_id'   => 'ForeignKey',
				'horario_id' => 'ForeignKey',
				'id'         => 'Number',
		);
	}
}
