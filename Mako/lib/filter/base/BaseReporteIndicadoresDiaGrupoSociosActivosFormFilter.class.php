<?php

/**
 * ReporteIndicadoresDiaGrupoSociosActivos filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseReporteIndicadoresDiaGrupoSociosActivosFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'reporte_indicador_dia_grupo_id' => new sfWidgetFormPropelChoice(array('model' => 'ReporteIndicadoresDiaGrupo', 'add_empty' => true)),
				'socio_id'                       => new sfWidgetFormFilterInput(array('with_empty' => false)),
		));

		$this->setValidators(array(
				'reporte_indicador_dia_grupo_id' => new sfValidatorPropelChoice(array('required' => false, 'model' => 'ReporteIndicadoresDiaGrupo', 'column' => 'id')),
				'socio_id'                       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
		));

		$this->widgetSchema->setNameFormat('reporte_indicadores_dia_grupo_socios_activos_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'ReporteIndicadoresDiaGrupoSociosActivos';
	}

	public function getFields()
	{
		return array(
				'id'                             => 'Number',
				'reporte_indicador_dia_grupo_id' => 'ForeignKey',
				'socio_id'                       => 'Number',
		);
	}
}
