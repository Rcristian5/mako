<?php

/**
 * Corte filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCorteFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'centro_id'            => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'folio_venta_inicial'  => new sfWidgetFormFilterInput(),
				'folio_venta_final'    => new sfWidgetFormFilterInput(),
				'fondo_inicial'        => new sfWidgetFormFilterInput(),
				'fondo_caja'           => new sfWidgetFormFilterInput(),
				'operador_id'          => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'total_ventas'         => new sfWidgetFormFilterInput(),
				'total_salidas'        => new sfWidgetFormFilterInput(),
				'total_cancelaciones'  => new sfWidgetFormFilterInput(),
				'total_vpg'            => new sfWidgetFormFilterInput(),
				'total_facturas'       => new sfWidgetFormFilterInput(),
				'total_conciliaciones' => new sfWidgetFormFilterInput(),
				'factura_id'           => new sfWidgetFormPropelChoice(array('model' => 'Factura', 'add_empty' => true)),
				'ip_caja'              => new sfWidgetFormFilterInput(),
				'created_at'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'updated_at'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
		));

		$this->setValidators(array(
				'centro_id'            => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Centro', 'column' => 'id')),
				'folio_venta_inicial'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'folio_venta_final'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'fondo_inicial'        => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
				'fondo_caja'           => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
				'operador_id'          => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Usuario', 'column' => 'id')),
				'total_ventas'         => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
				'total_salidas'        => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
				'total_cancelaciones'  => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
				'total_vpg'            => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
				'total_facturas'       => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
				'total_conciliaciones' => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
				'factura_id'           => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Factura', 'column' => 'id')),
				'ip_caja'              => new sfValidatorPass(array('required' => false)),
				'created_at'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'updated_at'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
		));

		$this->widgetSchema->setNameFormat('corte_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Corte';
	}

	public function getFields()
	{
		return array(
				'id'                   => 'Number',
				'centro_id'            => 'ForeignKey',
				'folio_venta_inicial'  => 'Number',
				'folio_venta_final'    => 'Number',
				'fondo_inicial'        => 'Number',
				'fondo_caja'           => 'Number',
				'operador_id'          => 'ForeignKey',
				'total_ventas'         => 'Number',
				'total_salidas'        => 'Number',
				'total_cancelaciones'  => 'Number',
				'total_vpg'            => 'Number',
				'total_facturas'       => 'Number',
				'total_conciliaciones' => 'Number',
				'factura_id'           => 'ForeignKey',
				'ip_caja'              => 'Text',
				'created_at'           => 'Date',
				'updated_at'           => 'Date',
		);
	}
}
