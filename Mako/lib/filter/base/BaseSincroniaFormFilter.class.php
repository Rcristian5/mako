<?php

/**
 * Sincronia filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseSincroniaFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'pk_async'   => new sfWidgetFormFilterInput(),
				'clase'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'operacion'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'tipo'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'detalle'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'completo'   => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'created_at' => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'updated_at' => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
		));

		$this->setValidators(array(
				'pk_async'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'clase'      => new sfValidatorPass(array('required' => false)),
				'operacion'  => new sfValidatorPass(array('required' => false)),
				'tipo'       => new sfValidatorPass(array('required' => false)),
				'detalle'    => new sfValidatorPass(array('required' => false)),
				'completo'   => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'created_at' => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'updated_at' => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
		));

		$this->widgetSchema->setNameFormat('sincronia_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Sincronia';
	}

	public function getFields()
	{
		return array(
				'id'         => 'Number',
				'pk_async'   => 'Number',
				'clase'      => 'Text',
				'operacion'  => 'Text',
				'tipo'       => 'Text',
				'detalle'    => 'Text',
				'completo'   => 'Boolean',
				'created_at' => 'Date',
				'updated_at' => 'Date',
		);
	}
}
