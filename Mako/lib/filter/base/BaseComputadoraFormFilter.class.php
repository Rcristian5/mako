<?php

/**
 * Computadora filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseComputadoraFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'centro_id'  => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'seccion_id' => new sfWidgetFormPropelChoice(array('model' => 'Seccion', 'add_empty' => true)),
				'ip'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'hostname'   => new sfWidgetFormFilterInput(),
				'mac_adress' => new sfWidgetFormFilterInput(),
				'alias'      => new sfWidgetFormFilterInput(),
				'tipo_id'    => new sfWidgetFormPropelChoice(array('model' => 'TipoPc', 'add_empty' => true)),
				'activo'     => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'created_at' => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'updated_at' => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
		));

		$this->setValidators(array(
				'centro_id'  => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Centro', 'column' => 'id')),
				'seccion_id' => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Seccion', 'column' => 'id')),
				'ip'         => new sfValidatorPass(array('required' => false)),
				'hostname'   => new sfValidatorPass(array('required' => false)),
				'mac_adress' => new sfValidatorPass(array('required' => false)),
				'alias'      => new sfValidatorPass(array('required' => false)),
				'tipo_id'    => new sfValidatorPropelChoice(array('required' => false, 'model' => 'TipoPc', 'column' => 'id')),
				'activo'     => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'created_at' => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'updated_at' => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
		));

		$this->widgetSchema->setNameFormat('computadora_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Computadora';
	}

	public function getFields()
	{
		return array(
				'id'         => 'Number',
				'centro_id'  => 'ForeignKey',
				'seccion_id' => 'ForeignKey',
				'ip'         => 'Text',
				'hostname'   => 'Text',
				'mac_adress' => 'Text',
				'alias'      => 'Text',
				'tipo_id'    => 'ForeignKey',
				'activo'     => 'Boolean',
				'created_at' => 'Date',
				'updated_at' => 'Date',
		);
	}
}
