<?php

/**
 * Paquete filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePaqueteFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'cantidad'       => new sfWidgetFormFilterInput(),
				'precio_paquete' => new sfWidgetFormFilterInput(),
		));

		$this->setValidators(array(
				'cantidad'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'precio_paquete' => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
		));

		$this->widgetSchema->setNameFormat('paquete_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Paquete';
	}

	public function getFields()
	{
		return array(
				'paquete_id'     => 'ForeignKey',
				'producto_id'    => 'ForeignKey',
				'cantidad'       => 'Number',
				'precio_paquete' => 'Number',
		);
	}
}
