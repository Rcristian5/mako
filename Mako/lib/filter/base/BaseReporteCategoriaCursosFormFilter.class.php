<?php

/**
 * ReporteCategoriaCursos filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseReporteCategoriaCursosFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'semana'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'anio'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'monto'            => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'cantidad'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'siglas'           => new sfWidgetFormFilterInput(),
				'categoria_nombre' => new sfWidgetFormFilterInput(),
		));

		$this->setValidators(array(
				'semana'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'anio'             => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'monto'            => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
				'cantidad'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'siglas'           => new sfValidatorPass(array('required' => false)),
				'categoria_nombre' => new sfValidatorPass(array('required' => false)),
		));

		$this->widgetSchema->setNameFormat('reporte_categoria_cursos_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'ReporteCategoriaCursos';
	}

	public function getFields()
	{
		return array(
				'centro_id'        => 'ForeignKey',
				'fecha'            => 'Date',
				'semana'           => 'Number',
				'anio'             => 'Number',
				'monto'            => 'Number',
				'cantidad'         => 'Number',
				'categoria_id'     => 'Number',
				'siglas'           => 'Text',
				'categoria_nombre' => 'Text',
		);
	}
}
