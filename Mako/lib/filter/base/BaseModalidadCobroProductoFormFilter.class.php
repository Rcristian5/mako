<?php

/**
 * ModalidadCobroProducto filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseModalidadCobroProductoFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'numero_pagos' => new sfWidgetFormFilterInput(),
		));

		$this->setValidators(array(
				'numero_pagos' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
		));

		$this->widgetSchema->setNameFormat('modalidad_cobro_producto_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'ModalidadCobroProducto';
	}

	public function getFields()
	{
		return array(
				'producto_id'  => 'ForeignKey',
				'cobro_id'     => 'ForeignKey',
				'numero_pagos' => 'Number',
		);
	}
}
