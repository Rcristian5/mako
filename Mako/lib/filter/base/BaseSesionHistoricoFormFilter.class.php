<?php

/**
 * SesionHistorico filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseSesionHistoricoFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'centro_id'      => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'computadora_id' => new sfWidgetFormPropelChoice(array('model' => 'Computadora', 'add_empty' => true)),
				'ip'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'socio_id'       => new sfWidgetFormPropelChoice(array('model' => 'Socio', 'add_empty' => true)),
				'usuario'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'fecha_login'    => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
				'fecha_logout'   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
				'saldo_inicial'  => new sfWidgetFormFilterInput(),
				'saldo_final'    => new sfWidgetFormFilterInput(),
				'tipo_id'        => new sfWidgetFormPropelChoice(array('model' => 'TipoSesion', 'add_empty' => true)),
				'ocupados'       => new sfWidgetFormFilterInput(),
		));

		$this->setValidators(array(
				'centro_id'      => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Centro', 'column' => 'id')),
				'computadora_id' => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Computadora', 'column' => 'id')),
				'ip'             => new sfValidatorPass(array('required' => false)),
				'socio_id'       => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Socio', 'column' => 'id')),
				'usuario'        => new sfValidatorPass(array('required' => false)),
				'fecha_login'    => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'fecha_logout'   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'saldo_inicial'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'saldo_final'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'tipo_id'        => new sfValidatorPropelChoice(array('required' => false, 'model' => 'TipoSesion', 'column' => 'id')),
				'ocupados'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
		));

		$this->widgetSchema->setNameFormat('sesion_historico_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'SesionHistorico';
	}

	public function getFields()
	{
		return array(
				'id'             => 'Number',
				'centro_id'      => 'ForeignKey',
				'computadora_id' => 'ForeignKey',
				'ip'             => 'Text',
				'socio_id'       => 'ForeignKey',
				'usuario'        => 'Text',
				'fecha_login'    => 'Date',
				'fecha_logout'   => 'Date',
				'saldo_inicial'  => 'Number',
				'saldo_final'    => 'Number',
				'tipo_id'        => 'ForeignKey',
				'ocupados'       => 'Number',
		);
	}
}
