<?php

/**
 * Producto filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseProductoFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'categoria_id'                    => new sfWidgetFormPropelChoice(array('model' => 'CategoriaProducto', 'add_empty' => true)),
				'tipo_id'                         => new sfWidgetFormPropelChoice(array('model' => 'TipoProducto', 'add_empty' => true)),
				'unidad_id'                       => new sfWidgetFormPropelChoice(array('model' => 'UnidadProducto', 'add_empty' => true)),
				'codigo'                          => new sfWidgetFormFilterInput(),
				'nombre'                          => new sfWidgetFormFilterInput(),
				'descripcion'                     => new sfWidgetFormFilterInput(),
				'precio_lista'                    => new sfWidgetFormFilterInput(),
				'tiempo'                          => new sfWidgetFormFilterInput(),
				'venta_unitaria'                  => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'en_pv'                           => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'es_subproducto'                  => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'activo'                          => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'created_at'                      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'updated_at'                      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'stock_list'                      => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'modalidad_cobro_producto_list'   => new sfWidgetFormPropelChoice(array('model' => 'TipoCobro', 'add_empty' => true)),
				'subproducto_cobranza_list'       => new sfWidgetFormPropelChoice(array('model' => 'TipoCobro', 'add_empty' => true)),
				'promocion_cupon_producto_list'   => new sfWidgetFormPropelChoice(array('model' => 'PromocionCupon', 'add_empty' => true)),
				'promocion_producto_list'         => new sfWidgetFormPropelChoice(array('model' => 'Promocion', 'add_empty' => true)),
				'categoria_reporte_producto_list' => new sfWidgetFormPropelChoice(array('model' => 'CategoriaReporte', 'add_empty' => true)),
				'producto_curso_list'             => new sfWidgetFormPropelChoice(array('model' => 'Curso', 'add_empty' => true)),
		));

		$this->setValidators(array(
				'categoria_id'                    => new sfValidatorPropelChoice(array('required' => false, 'model' => 'CategoriaProducto', 'column' => 'id')),
				'tipo_id'                         => new sfValidatorPropelChoice(array('required' => false, 'model' => 'TipoProducto', 'column' => 'id')),
				'unidad_id'                       => new sfValidatorPropelChoice(array('required' => false, 'model' => 'UnidadProducto', 'column' => 'id')),
				'codigo'                          => new sfValidatorPass(array('required' => false)),
				'nombre'                          => new sfValidatorPass(array('required' => false)),
				'descripcion'                     => new sfValidatorPass(array('required' => false)),
				'precio_lista'                    => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
				'tiempo'                          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'venta_unitaria'                  => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'en_pv'                           => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'es_subproducto'                  => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'activo'                          => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'created_at'                      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'updated_at'                      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'stock_list'                      => new sfValidatorPropelChoice(array('model' => 'Centro', 'required' => false)),
				'modalidad_cobro_producto_list'   => new sfValidatorPropelChoice(array('model' => 'TipoCobro', 'required' => false)),
				'subproducto_cobranza_list'       => new sfValidatorPropelChoice(array('model' => 'TipoCobro', 'required' => false)),
				'promocion_cupon_producto_list'   => new sfValidatorPropelChoice(array('model' => 'PromocionCupon', 'required' => false)),
				'promocion_producto_list'         => new sfValidatorPropelChoice(array('model' => 'Promocion', 'required' => false)),
				'categoria_reporte_producto_list' => new sfValidatorPropelChoice(array('model' => 'CategoriaReporte', 'required' => false)),
				'producto_curso_list'             => new sfValidatorPropelChoice(array('model' => 'Curso', 'required' => false)),
		));

		$this->widgetSchema->setNameFormat('producto_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function addStockListColumnCriteria(Criteria $criteria, $field, $values)
	{
		if (!is_array($values))
		{
			$values = array($values);
		}

		if (!count($values))
		{
			return;
		}

		$criteria->addJoin(StockPeer::PRODUCTO_ID, ProductoPeer::ID);

		$value = array_pop($values);
		$criterion = $criteria->getNewCriterion(StockPeer::CENTRO_ID, $value);

		foreach ($values as $value)
		{
			$criterion->addOr($criteria->getNewCriterion(StockPeer::CENTRO_ID, $value));
		}

		$criteria->add($criterion);
	}

	public function addModalidadCobroProductoListColumnCriteria(Criteria $criteria, $field, $values)
	{
		if (!is_array($values))
		{
			$values = array($values);
		}

		if (!count($values))
		{
			return;
		}

		$criteria->addJoin(ModalidadCobroProductoPeer::PRODUCTO_ID, ProductoPeer::ID);

		$value = array_pop($values);
		$criterion = $criteria->getNewCriterion(ModalidadCobroProductoPeer::COBRO_ID, $value);

		foreach ($values as $value)
		{
			$criterion->addOr($criteria->getNewCriterion(ModalidadCobroProductoPeer::COBRO_ID, $value));
		}

		$criteria->add($criterion);
	}

	public function addSubproductoCobranzaListColumnCriteria(Criteria $criteria, $field, $values)
	{
		if (!is_array($values))
		{
			$values = array($values);
		}

		if (!count($values))
		{
			return;
		}

		$criteria->addJoin(SubproductoCobranzaPeer::PRODUCTO_ID, ProductoPeer::ID);

		$value = array_pop($values);
		$criterion = $criteria->getNewCriterion(SubproductoCobranzaPeer::COBRO_ID, $value);

		foreach ($values as $value)
		{
			$criterion->addOr($criteria->getNewCriterion(SubproductoCobranzaPeer::COBRO_ID, $value));
		}

		$criteria->add($criterion);
	}

	public function addPromocionCuponProductoListColumnCriteria(Criteria $criteria, $field, $values)
	{
		if (!is_array($values))
		{
			$values = array($values);
		}

		if (!count($values))
		{
			return;
		}

		$criteria->addJoin(PromocionCuponProductoPeer::PRODUCTO_ID, ProductoPeer::ID);

		$value = array_pop($values);
		$criterion = $criteria->getNewCriterion(PromocionCuponProductoPeer::PROMOCION_ID, $value);

		foreach ($values as $value)
		{
			$criterion->addOr($criteria->getNewCriterion(PromocionCuponProductoPeer::PROMOCION_ID, $value));
		}

		$criteria->add($criterion);
	}

	public function addPromocionProductoListColumnCriteria(Criteria $criteria, $field, $values)
	{
		if (!is_array($values))
		{
			$values = array($values);
		}

		if (!count($values))
		{
			return;
		}

		$criteria->addJoin(PromocionProductoPeer::PRODUCTO_ID, ProductoPeer::ID);

		$value = array_pop($values);
		$criterion = $criteria->getNewCriterion(PromocionProductoPeer::PROMOCION_ID, $value);

		foreach ($values as $value)
		{
			$criterion->addOr($criteria->getNewCriterion(PromocionProductoPeer::PROMOCION_ID, $value));
		}

		$criteria->add($criterion);
	}

	public function addCategoriaReporteProductoListColumnCriteria(Criteria $criteria, $field, $values)
	{
		if (!is_array($values))
		{
			$values = array($values);
		}

		if (!count($values))
		{
			return;
		}

		$criteria->addJoin(CategoriaReporteProductoPeer::PRODUCTO_ID, ProductoPeer::ID);

		$value = array_pop($values);
		$criterion = $criteria->getNewCriterion(CategoriaReporteProductoPeer::CATEGORIA_ID, $value);

		foreach ($values as $value)
		{
			$criterion->addOr($criteria->getNewCriterion(CategoriaReporteProductoPeer::CATEGORIA_ID, $value));
		}

		$criteria->add($criterion);
	}

	public function addProductoCursoListColumnCriteria(Criteria $criteria, $field, $values)
	{
		if (!is_array($values))
		{
			$values = array($values);
		}

		if (!count($values))
		{
			return;
		}

		$criteria->addJoin(ProductoCursoPeer::PRODUCTO_ID, ProductoPeer::ID);

		$value = array_pop($values);
		$criterion = $criteria->getNewCriterion(ProductoCursoPeer::CURSO_ID, $value);

		foreach ($values as $value)
		{
			$criterion->addOr($criteria->getNewCriterion(ProductoCursoPeer::CURSO_ID, $value));
		}

		$criteria->add($criterion);
	}

	public function getModelName()
	{
		return 'Producto';
	}

	public function getFields()
	{
		return array(
				'id'                              => 'Number',
				'categoria_id'                    => 'ForeignKey',
				'tipo_id'                         => 'ForeignKey',
				'unidad_id'                       => 'ForeignKey',
				'codigo'                          => 'Text',
				'nombre'                          => 'Text',
				'descripcion'                     => 'Text',
				'precio_lista'                    => 'Number',
				'tiempo'                          => 'Number',
				'venta_unitaria'                  => 'Boolean',
				'en_pv'                           => 'Boolean',
				'es_subproducto'                  => 'Boolean',
				'activo'                          => 'Boolean',
				'created_at'                      => 'Date',
				'updated_at'                      => 'Date',
				'stock_list'                      => 'ManyKey',
				'modalidad_cobro_producto_list'   => 'ManyKey',
				'subproducto_cobranza_list'       => 'ManyKey',
				'promocion_cupon_producto_list'   => 'ManyKey',
				'promocion_producto_list'         => 'ManyKey',
				'categoria_reporte_producto_list' => 'ManyKey',
				'producto_curso_list'             => 'ManyKey',
		);
	}
}
