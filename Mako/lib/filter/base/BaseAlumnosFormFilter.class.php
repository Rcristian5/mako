<?php

/**
 * Alumnos filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseAlumnosFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'socio_id'  => new sfWidgetFormPropelChoice(array('model' => 'Socio', 'add_empty' => true)),
		));

		$this->setValidators(array(
				'socio_id'  => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Socio', 'column' => 'id')),
		));

		$this->widgetSchema->setNameFormat('alumnos_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Alumnos';
	}

	public function getFields()
	{
		return array(
				'matricula' => 'Number',
				'socio_id'  => 'ForeignKey',
		);
	}
}
