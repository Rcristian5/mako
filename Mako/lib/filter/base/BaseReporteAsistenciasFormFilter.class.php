<?php

/**
 * ReporteAsistencias filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseReporteAsistenciasFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'centro_id'                     => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'curso_id'                      => new sfWidgetFormPropelChoice(array('model' => 'Curso', 'add_empty' => true)),
				'fecha'                         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
				'socios_registrados'            => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'socios_primera_sesion'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'socios_ultima_sesion'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'socios_penultima_sesion'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'socios_imprmieron_certificado' => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'socios_presentaron_examen'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
		));

		$this->setValidators(array(
				'centro_id'                     => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Centro', 'column' => 'id')),
				'curso_id'                      => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Curso', 'column' => 'id')),
				'fecha'                         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'socios_registrados'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'socios_primera_sesion'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'socios_ultima_sesion'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'socios_penultima_sesion'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'socios_imprmieron_certificado' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'socios_presentaron_examen'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
		));

		$this->widgetSchema->setNameFormat('reporte_asistencias_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'ReporteAsistencias';
	}

	public function getFields()
	{
		return array(
				'id'                            => 'Number',
				'centro_id'                     => 'ForeignKey',
				'curso_id'                      => 'ForeignKey',
				'fecha'                         => 'Date',
				'socios_registrados'            => 'Number',
				'socios_primera_sesion'         => 'Number',
				'socios_ultima_sesion'          => 'Number',
				'socios_penultima_sesion'       => 'Number',
				'socios_imprmieron_certificado' => 'Number',
				'socios_presentaron_examen'     => 'Number',
		);
	}
}
