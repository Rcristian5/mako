<?php

/**
 * Evaluacion filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseEvaluacionFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'activo'             => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'curso_id'           => new sfWidgetFormPropelChoice(array('model' => 'Curso', 'add_empty' => true)),
				'capitulo_inicio'    => new sfWidgetFormPropelChoice(array('model' => 'Capitulo', 'add_empty' => true)),
				'capitulo_fin'       => new sfWidgetFormPropelChoice(array('model' => 'Capitulo', 'add_empty' => true)),
				'descripcion'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'tema_inicio'        => new sfWidgetFormPropelChoice(array('model' => 'Tema', 'add_empty' => true)),
				'tema_fin'           => new sfWidgetFormPropelChoice(array('model' => 'Tema', 'add_empty' => true)),
				'tipo'               => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'porcentaje'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'numero'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'tipo_evaluacion_id' => new sfWidgetFormPropelChoice(array('model' => 'TipoEvaluacion', 'add_empty' => true)),
				'operador_id'        => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'created_at'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'updated_at'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
		));

		$this->setValidators(array(
				'activo'             => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'curso_id'           => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Curso', 'column' => 'id')),
				'capitulo_inicio'    => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Capitulo', 'column' => 'id')),
				'capitulo_fin'       => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Capitulo', 'column' => 'id')),
				'descripcion'        => new sfValidatorPass(array('required' => false)),
				'tema_inicio'        => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Tema', 'column' => 'id')),
				'tema_fin'           => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Tema', 'column' => 'id')),
				'tipo'               => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'porcentaje'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'numero'             => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'tipo_evaluacion_id' => new sfValidatorPropelChoice(array('required' => false, 'model' => 'TipoEvaluacion', 'column' => 'id')),
				'operador_id'        => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Usuario', 'column' => 'id')),
				'created_at'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'updated_at'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
		));

		$this->widgetSchema->setNameFormat('evaluacion_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Evaluacion';
	}

	public function getFields()
	{
		return array(
				'id'                 => 'Number',
				'activo'             => 'Boolean',
				'curso_id'           => 'ForeignKey',
				'capitulo_inicio'    => 'ForeignKey',
				'capitulo_fin'       => 'ForeignKey',
				'descripcion'        => 'Text',
				'tema_inicio'        => 'ForeignKey',
				'tema_fin'           => 'ForeignKey',
				'tipo'               => 'Number',
				'porcentaje'         => 'Number',
				'numero'             => 'Number',
				'tipo_evaluacion_id' => 'ForeignKey',
				'operador_id'        => 'ForeignKey',
				'created_at'         => 'Date',
				'updated_at'         => 'Date',
		);
	}
}
