<?php

/**
 * RecursosInfraestructuraAsignados filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseRecursosInfraestructuraAsignadosFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'cantidad'   => new sfWidgetFormFilterInput(),
		));

		$this->setValidators(array(
				'cantidad'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
		));

		$this->widgetSchema->setNameFormat('recursos_infraestructura_asignados_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'RecursosInfraestructuraAsignados';
	}

	public function getFields()
	{
		return array(
				'curso_id'   => 'ForeignKey',
				'recurso_id' => 'ForeignKey',
				'cantidad'   => 'Number',
		);
	}
}
