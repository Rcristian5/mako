<?php

/**
 * CalificacionParcial filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCalificacionParcialFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'curso_id'      => new sfWidgetFormPropelChoice(array('model' => 'Curso', 'add_empty' => true)),
				'evaluacion_id' => new sfWidgetFormPropelChoice(array('model' => 'EvaluacionParcial', 'add_empty' => true)),
				'alumno_id'     => new sfWidgetFormPropelChoice(array('model' => 'Alumnos', 'add_empty' => true)),
				'calificacion'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'operador_id'   => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'created_at'    => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'updated_at'    => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
		));

		$this->setValidators(array(
				'curso_id'      => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Curso', 'column' => 'id')),
				'evaluacion_id' => new sfValidatorPropelChoice(array('required' => false, 'model' => 'EvaluacionParcial', 'column' => 'id')),
				'alumno_id'     => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Alumnos', 'column' => 'matricula')),
				'calificacion'  => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
				'operador_id'   => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Usuario', 'column' => 'id')),
				'created_at'    => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'updated_at'    => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
		));

		$this->widgetSchema->setNameFormat('calificacion_parcial_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'CalificacionParcial';
	}

	public function getFields()
	{
		return array(
				'id'            => 'Number',
				'curso_id'      => 'ForeignKey',
				'evaluacion_id' => 'ForeignKey',
				'alumno_id'     => 'ForeignKey',
				'calificacion'  => 'Number',
				'operador_id'   => 'ForeignKey',
				'created_at'    => 'Date',
				'updated_at'    => 'Date',
		);
	}
}
