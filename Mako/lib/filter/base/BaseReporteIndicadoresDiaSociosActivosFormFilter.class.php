<?php

/**
 * ReporteIndicadoresDiaSociosActivos filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseReporteIndicadoresDiaSociosActivosFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'socio_id'                 => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'usuario'                  => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'nombre_completo'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'edad'                     => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'ocupacion_id'             => new sfWidgetFormPropelChoice(array('model' => 'Ocupacion', 'add_empty' => true)),
				'sexo'                     => new sfWidgetFormFilterInput(array('with_empty' => false)),
		));

		$this->setValidators(array(
				'socio_id'                 => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'usuario'                  => new sfValidatorPass(array('required' => false)),
				'nombre_completo'          => new sfValidatorPass(array('required' => false)),
				'edad'                     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'ocupacion_id'             => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Ocupacion', 'column' => 'id')),
				'sexo'                     => new sfValidatorPass(array('required' => false)),
		));

		$this->widgetSchema->setNameFormat('reporte_indicadores_dia_socios_activos_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'ReporteIndicadoresDiaSociosActivos';
	}

	public function getFields()
	{
		return array(
				'id'                       => 'Number',
				'reporte_indicador_dia_id' => 'ForeignKey',
				'centro_id'                => 'ForeignKey',
				'socio_id'                 => 'Number',
				'usuario'                  => 'Text',
				'nombre_completo'          => 'Text',
				'edad'                     => 'Number',
				'ocupacion_id'             => 'ForeignKey',
				'sexo'                     => 'Text',
		);
	}
}
