<?php

/**
 * Factura filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseFacturaFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'centro_id'       => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'folio_fiscal_id' => new sfWidgetFormPropelChoice(array('model' => 'FolioFiscal', 'add_empty' => true)),
				'folio'           => new sfWidgetFormFilterInput(),
				'importe'         => new sfWidgetFormFilterInput(),
				'subtotal'        => new sfWidgetFormFilterInput(),
				'total'           => new sfWidgetFormFilterInput(),
				'retencion'       => new sfWidgetFormFilterInput(),
				'cadena_original' => new sfWidgetFormFilterInput(),
				'sello'           => new sfWidgetFormFilterInput(),
				'fecha_factura'   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'updated_at'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
		));

		$this->setValidators(array(
				'centro_id'       => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Centro', 'column' => 'id')),
				'folio_fiscal_id' => new sfValidatorPropelChoice(array('required' => false, 'model' => 'FolioFiscal', 'column' => 'id')),
				'folio'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'importe'         => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
				'subtotal'        => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
				'total'           => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
				'retencion'       => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
				'cadena_original' => new sfValidatorPass(array('required' => false)),
				'sello'           => new sfValidatorPass(array('required' => false)),
				'fecha_factura'   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'updated_at'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
		));

		$this->widgetSchema->setNameFormat('factura_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Factura';
	}

	public function getFields()
	{
		return array(
				'id'              => 'Number',
				'centro_id'       => 'ForeignKey',
				'folio_fiscal_id' => 'ForeignKey',
				'folio'           => 'Number',
				'importe'         => 'Number',
				'subtotal'        => 'Number',
				'total'           => 'Number',
				'retencion'       => 'Number',
				'cadena_original' => 'Text',
				'sello'           => 'Text',
				'fecha_factura'   => 'Date',
				'updated_at'      => 'Date',
		);
	}
}
