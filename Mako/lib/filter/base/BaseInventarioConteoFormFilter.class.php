<?php

/**
 * InventarioConteo filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseInventarioConteoFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'corte_id'       => new sfWidgetFormPropelChoice(array('model' => 'InventarioCorte', 'add_empty' => true)),
				'operacion_id'   => new sfWidgetFormPropelChoice(array('model' => 'InventarioOperacion', 'add_empty' => true)),
				'producto_id'    => new sfWidgetFormPropelChoice(array('model' => 'Producto', 'add_empty' => true)),
				'cantidad_final' => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'conteo1'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'conteo2'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'centro_id'      => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'created_at'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
		));

		$this->setValidators(array(
				'corte_id'       => new sfValidatorPropelChoice(array('required' => false, 'model' => 'InventarioCorte', 'column' => 'id')),
				'operacion_id'   => new sfValidatorPropelChoice(array('required' => false, 'model' => 'InventarioOperacion', 'column' => 'id')),
				'producto_id'    => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Producto', 'column' => 'id')),
				'cantidad_final' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'conteo1'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'conteo2'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'centro_id'      => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Centro', 'column' => 'id')),
				'created_at'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
		));

		$this->widgetSchema->setNameFormat('inventario_conteo_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'InventarioConteo';
	}

	public function getFields()
	{
		return array(
				'id'             => 'Number',
				'corte_id'       => 'ForeignKey',
				'operacion_id'   => 'ForeignKey',
				'producto_id'    => 'ForeignKey',
				'cantidad_final' => 'Number',
				'conteo1'        => 'Number',
				'conteo2'        => 'Number',
				'centro_id'      => 'ForeignKey',
				'created_at'     => 'Date',
		);
	}
}
