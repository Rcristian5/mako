<?php

/**
 * PromocionReglaci filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePromocionReglaciFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'promocion_id'     => new sfWidgetFormPropelChoice(array('model' => 'Promocion', 'add_empty' => true)),
				'sexo'             => new sfWidgetFormFilterInput(),
				'edad_de'          => new sfWidgetFormFilterInput(),
				'edad_a'           => new sfWidgetFormFilterInput(),
				'nivel_estudio'    => new sfWidgetFormPropelChoice(array('model' => 'NivelEstudio', 'add_empty' => true)),
				'recomendados'     => new sfWidgetFormFilterInput(),
				'centro_alta_id'   => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'centro_venta_id'  => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'municipio_centro' => new sfWidgetFormFilterInput(),
				'empresa_id'       => new sfWidgetFormPropelChoice(array('model' => 'Empresa', 'add_empty' => true)),
				'discapacidad_id'  => new sfWidgetFormPropelChoice(array('model' => 'Discapacidad', 'add_empty' => true)),
		));

		$this->setValidators(array(
				'promocion_id'     => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Promocion', 'column' => 'id')),
				'sexo'             => new sfValidatorPass(array('required' => false)),
				'edad_de'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'edad_a'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'nivel_estudio'    => new sfValidatorPropelChoice(array('required' => false, 'model' => 'NivelEstudio', 'column' => 'id')),
				'recomendados'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'centro_alta_id'   => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Centro', 'column' => 'id')),
				'centro_venta_id'  => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Centro', 'column' => 'id')),
				'municipio_centro' => new sfValidatorPass(array('required' => false)),
				'empresa_id'       => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Empresa', 'column' => 'id')),
				'discapacidad_id'  => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Discapacidad', 'column' => 'id')),
		));

		$this->widgetSchema->setNameFormat('promocion_reglaci_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'PromocionReglaci';
	}

	public function getFields()
	{
		return array(
				'id'               => 'Number',
				'promocion_id'     => 'ForeignKey',
				'sexo'             => 'Text',
				'edad_de'          => 'Number',
				'edad_a'           => 'Number',
				'nivel_estudio'    => 'ForeignKey',
				'recomendados'     => 'Number',
				'centro_alta_id'   => 'ForeignKey',
				'centro_venta_id'  => 'ForeignKey',
				'municipio_centro' => 'Text',
				'empresa_id'       => 'ForeignKey',
				'discapacidad_id'  => 'ForeignKey',
		);
	}
}
