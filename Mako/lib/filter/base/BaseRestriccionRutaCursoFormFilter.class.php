<?php

/**
 * RestriccionRutaCurso filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseRestriccionRutaCursoFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
		));

		$this->setValidators(array(
		));

		$this->widgetSchema->setNameFormat('restriccion_ruta_curso_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'RestriccionRutaCurso';
	}

	public function getFields()
	{
		return array(
				'restriccion_ruta_id' => 'ForeignKey',
				'curso_id'            => 'ForeignKey',
		);
	}
}
