<?php

/**
 * ReporteActivos filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseReporteActivosFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'usuario'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'folio'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'nombre'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'apepat'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'apemat'          => new sfWidgetFormFilterInput(),
				'nombre_completo' => new sfWidgetFormFilterInput(),
				'fecha_nac'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
				'sexo'            => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'email'           => new sfWidgetFormFilterInput(),
				'tel'             => new sfWidgetFormFilterInput(),
				'celular'         => new sfWidgetFormFilterInput(),
				'estado'          => new sfWidgetFormFilterInput(),
				'ciudad'          => new sfWidgetFormFilterInput(),
				'municipio'       => new sfWidgetFormFilterInput(),
				'colonia'         => new sfWidgetFormFilterInput(),
				'cp'              => new sfWidgetFormFilterInput(),
				'calle'           => new sfWidgetFormFilterInput(),
				'num_ext'         => new sfWidgetFormFilterInput(),
				'num_int'         => new sfWidgetFormFilterInput(),
				'lat'             => new sfWidgetFormFilterInput(),
				'long'            => new sfWidgetFormFilterInput(),
		));

		$this->setValidators(array(
				'usuario'         => new sfValidatorPass(array('required' => false)),
				'folio'           => new sfValidatorPass(array('required' => false)),
				'nombre'          => new sfValidatorPass(array('required' => false)),
				'apepat'          => new sfValidatorPass(array('required' => false)),
				'apemat'          => new sfValidatorPass(array('required' => false)),
				'nombre_completo' => new sfValidatorPass(array('required' => false)),
				'fecha_nac'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'sexo'            => new sfValidatorPass(array('required' => false)),
				'email'           => new sfValidatorPass(array('required' => false)),
				'tel'             => new sfValidatorPass(array('required' => false)),
				'celular'         => new sfValidatorPass(array('required' => false)),
				'estado'          => new sfValidatorPass(array('required' => false)),
				'ciudad'          => new sfValidatorPass(array('required' => false)),
				'municipio'       => new sfValidatorPass(array('required' => false)),
				'colonia'         => new sfValidatorPass(array('required' => false)),
				'cp'              => new sfValidatorPass(array('required' => false)),
				'calle'           => new sfValidatorPass(array('required' => false)),
				'num_ext'         => new sfValidatorPass(array('required' => false)),
				'num_int'         => new sfValidatorPass(array('required' => false)),
				'lat'             => new sfValidatorPass(array('required' => false)),
				'long'            => new sfValidatorPass(array('required' => false)),
		));

		$this->widgetSchema->setNameFormat('reporte_activos_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'ReporteActivos';
	}

	public function getFields()
	{
		return array(
				'centro_id'       => 'ForeignKey',
				'fecha'           => 'Date',
				'socio_id'        => 'Number',
				'usuario'         => 'Text',
				'folio'           => 'Text',
				'nombre'          => 'Text',
				'apepat'          => 'Text',
				'apemat'          => 'Text',
				'nombre_completo' => 'Text',
				'fecha_nac'       => 'Date',
				'sexo'            => 'Text',
				'email'           => 'Text',
				'tel'             => 'Text',
				'celular'         => 'Text',
				'estado'          => 'Text',
				'ciudad'          => 'Text',
				'municipio'       => 'Text',
				'colonia'         => 'Text',
				'cp'              => 'Text',
				'calle'           => 'Text',
				'num_ext'         => 'Text',
				'num_int'         => 'Text',
				'lat'             => 'Text',
				'long'            => 'Text',
		);
	}
}
