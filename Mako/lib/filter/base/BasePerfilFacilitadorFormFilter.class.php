<?php

/**
 * PerfilFacilitador filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePerfilFacilitadorFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'nombre'              => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'operador_id'         => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'activo'              => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'created_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'updated_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'usuario_perfil_list' => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
		));

		$this->setValidators(array(
				'nombre'              => new sfValidatorPass(array('required' => false)),
				'operador_id'         => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Usuario', 'column' => 'id')),
				'activo'              => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'created_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'updated_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'usuario_perfil_list' => new sfValidatorPropelChoice(array('model' => 'Usuario', 'required' => false)),
		));

		$this->widgetSchema->setNameFormat('perfil_facilitador_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function addUsuarioPerfilListColumnCriteria(Criteria $criteria, $field, $values)
	{
		if (!is_array($values))
		{
			$values = array($values);
		}

		if (!count($values))
		{
			return;
		}

		$criteria->addJoin(UsuarioPerfilPeer::PERFIL_ID, PerfilFacilitadorPeer::ID);

		$value = array_pop($values);
		$criterion = $criteria->getNewCriterion(UsuarioPerfilPeer::USUARIO_ID, $value);

		foreach ($values as $value)
		{
			$criterion->addOr($criteria->getNewCriterion(UsuarioPerfilPeer::USUARIO_ID, $value));
		}

		$criteria->add($criterion);
	}

	public function getModelName()
	{
		return 'PerfilFacilitador';
	}

	public function getFields()
	{
		return array(
				'id'                  => 'Number',
				'nombre'              => 'Text',
				'operador_id'         => 'ForeignKey',
				'activo'              => 'Boolean',
				'created_at'          => 'Date',
				'updated_at'          => 'Date',
				'usuario_perfil_list' => 'ManyKey',
		);
	}
}
