<?php

/**
 * InventarioOperacion filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseInventarioOperacionFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'operacion' => new sfWidgetFormFilterInput(),
		));

		$this->setValidators(array(
				'operacion' => new sfValidatorPass(array('required' => false)),
		));

		$this->widgetSchema->setNameFormat('inventario_operacion_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'InventarioOperacion';
	}

	public function getFields()
	{
		return array(
				'id'        => 'Number',
				'operacion' => 'Text',
		);
	}
}
