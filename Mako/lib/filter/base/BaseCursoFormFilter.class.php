<?php

/**
 * Curso filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCursoFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'categoria_curso_id'    => new sfWidgetFormPropelChoice(array('model' => 'CategoriaCurso', 'add_empty' => true)),
				'clave'                 => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'nombre'                => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'descripcion'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'perfil_id'             => new sfWidgetFormPropelChoice(array('model' => 'PerfilFacilitador', 'add_empty' => true)),
				'duracion'              => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'seriado'               => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'ultima_verificacion'   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'asociado_moodle'       => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'registrado_moodle'     => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'asociado_rs'           => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'registrado_rs'         => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'restricciones_socio'   => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'restricciones_horario' => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'calificacion_minima'   => new sfWidgetFormFilterInput(),
				'publicado'             => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'operador_id'           => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'activo'                => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'created_at'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'updated_at'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'nombre_corto'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'curso_centro_list'     => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'producto_curso_list'   => new sfWidgetFormPropelChoice(array('model' => 'Producto', 'add_empty' => true)),
		));

		$this->setValidators(array(
				'categoria_curso_id'    => new sfValidatorPropelChoice(array('required' => false, 'model' => 'CategoriaCurso', 'column' => 'id')),
				'clave'                 => new sfValidatorPass(array('required' => false)),
				'nombre'                => new sfValidatorPass(array('required' => false)),
				'descripcion'           => new sfValidatorPass(array('required' => false)),
				'perfil_id'             => new sfValidatorPropelChoice(array('required' => false, 'model' => 'PerfilFacilitador', 'column' => 'id')),
				'duracion'              => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'seriado'               => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'ultima_verificacion'   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'asociado_moodle'       => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'registrado_moodle'     => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'asociado_rs'           => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'registrado_rs'         => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'restricciones_socio'   => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'restricciones_horario' => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'calificacion_minima'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'publicado'             => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'operador_id'           => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Usuario', 'column' => 'id')),
				'activo'                => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'created_at'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'updated_at'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'nombre_corto'          => new sfValidatorPass(array('required' => false)),
				'curso_centro_list'     => new sfValidatorPropelChoice(array('model' => 'Centro', 'required' => false)),
				'producto_curso_list'   => new sfValidatorPropelChoice(array('model' => 'Producto', 'required' => false)),
		));

		$this->widgetSchema->setNameFormat('curso_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function addCursoCentroListColumnCriteria(Criteria $criteria, $field, $values)
	{
		if (!is_array($values))
		{
			$values = array($values);
		}

		if (!count($values))
		{
			return;
		}

		$criteria->addJoin(CursoCentroPeer::CURSO_ID, CursoPeer::ID);

		$value = array_pop($values);
		$criterion = $criteria->getNewCriterion(CursoCentroPeer::CENTRO_ID, $value);

		foreach ($values as $value)
		{
			$criterion->addOr($criteria->getNewCriterion(CursoCentroPeer::CENTRO_ID, $value));
		}

		$criteria->add($criterion);
	}

	public function addProductoCursoListColumnCriteria(Criteria $criteria, $field, $values)
	{
		if (!is_array($values))
		{
			$values = array($values);
		}

		if (!count($values))
		{
			return;
		}

		$criteria->addJoin(ProductoCursoPeer::CURSO_ID, CursoPeer::ID);

		$value = array_pop($values);
		$criterion = $criteria->getNewCriterion(ProductoCursoPeer::PRODUCTO_ID, $value);

		foreach ($values as $value)
		{
			$criterion->addOr($criteria->getNewCriterion(ProductoCursoPeer::PRODUCTO_ID, $value));
		}

		$criteria->add($criterion);
	}

	public function getModelName()
	{
		return 'Curso';
	}

	public function getFields()
	{
		return array(
				'id'                    => 'Number',
				'categoria_curso_id'    => 'ForeignKey',
				'clave'                 => 'Text',
				'nombre'                => 'Text',
				'descripcion'           => 'Text',
				'perfil_id'             => 'ForeignKey',
				'duracion'              => 'Number',
				'seriado'               => 'Boolean',
				'ultima_verificacion'   => 'Date',
				'asociado_moodle'       => 'Boolean',
				'registrado_moodle'     => 'Boolean',
				'asociado_rs'           => 'Boolean',
				'registrado_rs'         => 'Boolean',
				'restricciones_socio'   => 'Boolean',
				'restricciones_horario' => 'Boolean',
				'calificacion_minima'   => 'Number',
				'publicado'             => 'Boolean',
				'operador_id'           => 'ForeignKey',
				'activo'                => 'Boolean',
				'created_at'            => 'Date',
				'updated_at'            => 'Date',
				'nombre_corto'          => 'Text',
				'curso_centro_list'     => 'ManyKey',
				'producto_curso_list'   => 'ManyKey',
		);
	}
}
