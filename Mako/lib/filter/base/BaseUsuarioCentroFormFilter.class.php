<?php

/**
 * UsuarioCentro filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseUsuarioCentroFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
		));

		$this->setValidators(array(
		));

		$this->widgetSchema->setNameFormat('usuario_centro_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'UsuarioCentro';
	}

	public function getFields()
	{
		return array(
				'usuario_id' => 'ForeignKey',
				'centro_id'  => 'ForeignKey',
		);
	}
}
