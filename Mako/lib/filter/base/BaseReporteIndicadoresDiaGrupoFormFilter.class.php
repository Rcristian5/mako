<?php

/**
 * ReporteIndicadoresDiaGrupo filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseReporteIndicadoresDiaGrupoFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'grupo_id'                     => new sfWidgetFormPropelChoice(array('model' => 'Grupo', 'add_empty' => true)),
				'curso_id'                     => new sfWidgetFormPropelChoice(array('model' => 'Curso', 'add_empty' => true)),
				'facilitador_id'               => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'categoria_curso_id'           => new sfWidgetFormPropelChoice(array('model' => 'CategoriaCurso', 'add_empty' => true)),
				'aula_id'                      => new sfWidgetFormPropelChoice(array('model' => 'Aula', 'add_empty' => true)),
				'fecha'                        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
				'semana'                       => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'anio'                         => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'socios_preinscritos'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'socios_inscritos'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'socios_inscritos_nuevos'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'socios_inscritos_recurrentes' => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'alumnos_activos'              => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'alumnos_actividad_programada' => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'alumnos_graduados'            => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'alumnos_reprobados'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'graduacion_esperada'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'alumnos_desertores'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'pagos_programados'            => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'pagos_cobrados'               => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'cobranza_programada'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'cobranza_cobrada'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'created_at'                   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'updated_at'                   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
		));

		$this->setValidators(array(
				'grupo_id'                     => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Grupo', 'column' => 'id')),
				'curso_id'                     => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Curso', 'column' => 'id')),
				'facilitador_id'               => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Usuario', 'column' => 'id')),
				'categoria_curso_id'           => new sfValidatorPropelChoice(array('required' => false, 'model' => 'CategoriaCurso', 'column' => 'id')),
				'aula_id'                      => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Aula', 'column' => 'id')),
				'fecha'                        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'semana'                       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'anio'                         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'socios_preinscritos'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'socios_inscritos'             => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'socios_inscritos_nuevos'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'socios_inscritos_recurrentes' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'alumnos_activos'              => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'alumnos_actividad_programada' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'alumnos_graduados'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'alumnos_reprobados'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'graduacion_esperada'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'alumnos_desertores'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'pagos_programados'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'pagos_cobrados'               => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'cobranza_programada'          => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
				'cobranza_cobrada'             => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
				'created_at'                   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'updated_at'                   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
		));

		$this->widgetSchema->setNameFormat('reporte_indicadores_dia_grupo_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'ReporteIndicadoresDiaGrupo';
	}

	public function getFields()
	{
		return array(
				'id'                           => 'Number',
				'centro_id'                    => 'ForeignKey',
				'grupo_id'                     => 'ForeignKey',
				'curso_id'                     => 'ForeignKey',
				'facilitador_id'               => 'ForeignKey',
				'categoria_curso_id'           => 'ForeignKey',
				'aula_id'                      => 'ForeignKey',
				'fecha'                        => 'Date',
				'semana'                       => 'Number',
				'anio'                         => 'Number',
				'socios_preinscritos'          => 'Number',
				'socios_inscritos'             => 'Number',
				'socios_inscritos_nuevos'      => 'Number',
				'socios_inscritos_recurrentes' => 'Number',
				'alumnos_activos'              => 'Number',
				'alumnos_actividad_programada' => 'Number',
				'alumnos_graduados'            => 'Number',
				'alumnos_reprobados'           => 'Number',
				'graduacion_esperada'          => 'Number',
				'alumnos_desertores'           => 'Number',
				'pagos_programados'            => 'Number',
				'pagos_cobrados'               => 'Number',
				'cobranza_programada'          => 'Number',
				'cobranza_cobrada'             => 'Number',
				'created_at'                   => 'Date',
				'updated_at'                   => 'Date',
		);
	}
}
