<?php

/**
 * ListaAsistencia filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseListaAsistenciaFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'grupo_id'     => new sfWidgetFormPropelChoice(array('model' => 'Grupo', 'add_empty' => true)),
				'curso_id'     => new sfWidgetFormPropelChoice(array('model' => 'Curso', 'add_empty' => true)),
				'alumno_id'    => new sfWidgetFormPropelChoice(array('model' => 'Alumnos', 'add_empty' => true)),
				'fecha_id'     => new sfWidgetFormPropelChoice(array('model' => 'FechasHorario', 'add_empty' => true)),
				'asistencia'   => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'operador_id'  => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'fecha_tomada' => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'created_at'   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'updated_at'   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
		));

		$this->setValidators(array(
				'grupo_id'     => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Grupo', 'column' => 'id')),
				'curso_id'     => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Curso', 'column' => 'id')),
				'alumno_id'    => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Alumnos', 'column' => 'matricula')),
				'fecha_id'     => new sfValidatorPropelChoice(array('required' => false, 'model' => 'FechasHorario', 'column' => 'id')),
				'asistencia'   => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'operador_id'  => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Usuario', 'column' => 'id')),
				'fecha_tomada' => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'created_at'   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'updated_at'   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
		));

		$this->widgetSchema->setNameFormat('lista_asistencia_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'ListaAsistencia';
	}

	public function getFields()
	{
		return array(
				'id'           => 'Number',
				'grupo_id'     => 'ForeignKey',
				'curso_id'     => 'ForeignKey',
				'alumno_id'    => 'ForeignKey',
				'fecha_id'     => 'ForeignKey',
				'asistencia'   => 'Boolean',
				'operador_id'  => 'ForeignKey',
				'fecha_tomada' => 'Date',
				'created_at'   => 'Date',
				'updated_at'   => 'Date',
		);
	}
}
