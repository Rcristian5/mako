<?php

/**
 * CategoriaReporteProducto filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCategoriaReporteProductoFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
		));

		$this->setValidators(array(
		));

		$this->widgetSchema->setNameFormat('categoria_reporte_producto_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'CategoriaReporteProducto';
	}

	public function getFields()
	{
		return array(
				'categoria_id' => 'ForeignKey',
				'producto_id'  => 'ForeignKey',
		);
	}
}
