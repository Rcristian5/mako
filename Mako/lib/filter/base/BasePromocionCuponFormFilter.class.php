<?php

/**
 * PromocionCupon filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePromocionCuponFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'centro_id'                     => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'nombre'                        => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'descripcion'                   => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'vigente_de'                    => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'vigente_a'                     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'descuento'                     => new sfWidgetFormFilterInput(),
				'cantidad_cupones'              => new sfWidgetFormFilterInput(),
				'creado_por'                    => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'activo'                        => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'created_at'                    => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'updated_at'                    => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'promocion_cupon_producto_list' => new sfWidgetFormPropelChoice(array('model' => 'Producto', 'add_empty' => true)),
		));

		$this->setValidators(array(
				'centro_id'                     => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Centro', 'column' => 'id')),
				'nombre'                        => new sfValidatorPass(array('required' => false)),
				'descripcion'                   => new sfValidatorPass(array('required' => false)),
				'vigente_de'                    => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'vigente_a'                     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'descuento'                     => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
				'cantidad_cupones'              => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'creado_por'                    => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Usuario', 'column' => 'id')),
				'activo'                        => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'created_at'                    => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'updated_at'                    => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'promocion_cupon_producto_list' => new sfValidatorPropelChoice(array('model' => 'Producto', 'required' => false)),
		));

		$this->widgetSchema->setNameFormat('promocion_cupon_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function addPromocionCuponProductoListColumnCriteria(Criteria $criteria, $field, $values)
	{
		if (!is_array($values))
		{
			$values = array($values);
		}

		if (!count($values))
		{
			return;
		}

		$criteria->addJoin(PromocionCuponProductoPeer::PROMOCION_ID, PromocionCuponPeer::ID);

		$value = array_pop($values);
		$criterion = $criteria->getNewCriterion(PromocionCuponProductoPeer::PRODUCTO_ID, $value);

		foreach ($values as $value)
		{
			$criterion->addOr($criteria->getNewCriterion(PromocionCuponProductoPeer::PRODUCTO_ID, $value));
		}

		$criteria->add($criterion);
	}

	public function getModelName()
	{
		return 'PromocionCupon';
	}

	public function getFields()
	{
		return array(
				'id'                            => 'Number',
				'centro_id'                     => 'ForeignKey',
				'nombre'                        => 'Text',
				'descripcion'                   => 'Text',
				'vigente_de'                    => 'Date',
				'vigente_a'                     => 'Date',
				'descuento'                     => 'Number',
				'cantidad_cupones'              => 'Number',
				'creado_por'                    => 'ForeignKey',
				'activo'                        => 'Boolean',
				'created_at'                    => 'Date',
				'updated_at'                    => 'Date',
				'promocion_cupon_producto_list' => 'ManyKey',
		);
	}
}
