<?php

/**
 * ReporteVentasCategorias filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseReporteVentasCategoriasFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'centro_id'    => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'fecha'        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
				'semana'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'anio'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'monto'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'cantidad'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'tiempo'       => new sfWidgetFormFilterInput(),
				'categoria_id' => new sfWidgetFormPropelChoice(array('model' => 'CategoriaProducto', 'add_empty' => true)),
				'categoria'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'pcs'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'cursos'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'aulas'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
		));

		$this->setValidators(array(
				'centro_id'    => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Centro', 'column' => 'id')),
				'fecha'        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'semana'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'anio'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'monto'        => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
				'cantidad'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'tiempo'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'categoria_id' => new sfValidatorPropelChoice(array('required' => false, 'model' => 'CategoriaProducto', 'column' => 'id')),
				'categoria'    => new sfValidatorPass(array('required' => false)),
				'pcs'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'cursos'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'aulas'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
		));

		$this->widgetSchema->setNameFormat('reporte_ventas_categorias_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'ReporteVentasCategorias';
	}

	public function getFields()
	{
		return array(
				'id'           => 'Number',
				'centro_id'    => 'ForeignKey',
				'fecha'        => 'Date',
				'semana'       => 'Number',
				'anio'         => 'Number',
				'monto'        => 'Number',
				'cantidad'     => 'Number',
				'tiempo'       => 'Number',
				'categoria_id' => 'ForeignKey',
				'categoria'    => 'Text',
				'pcs'          => 'Number',
				'cursos'       => 'Number',
				'aulas'        => 'Number',
		);
	}
}
