<?php

/**
 * EvaluacionParcial filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseEvaluacionParcialFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'curso_id'            => new sfWidgetFormPropelChoice(array('model' => 'Curso', 'add_empty' => true)),
				'tema_id'             => new sfWidgetFormPropelChoice(array('model' => 'Tema', 'add_empty' => true)),
				'nombre'              => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'descripcion'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'calificacion_minima' => new sfWidgetFormFilterInput(),
				'calificacion_maxima' => new sfWidgetFormFilterInput(),
				'porcentaje'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'activo'              => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'operador_id'         => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'created_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'updated_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
		));

		$this->setValidators(array(
				'curso_id'            => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Curso', 'column' => 'id')),
				'tema_id'             => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Tema', 'column' => 'id')),
				'nombre'              => new sfValidatorPass(array('required' => false)),
				'descripcion'         => new sfValidatorPass(array('required' => false)),
				'calificacion_minima' => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
				'calificacion_maxima' => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
				'porcentaje'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'activo'              => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'operador_id'         => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Usuario', 'column' => 'id')),
				'created_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'updated_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
		));

		$this->widgetSchema->setNameFormat('evaluacion_parcial_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'EvaluacionParcial';
	}

	public function getFields()
	{
		return array(
				'id'                  => 'Number',
				'curso_id'            => 'ForeignKey',
				'tema_id'             => 'ForeignKey',
				'nombre'              => 'Text',
				'descripcion'         => 'Text',
				'calificacion_minima' => 'Number',
				'calificacion_maxima' => 'Number',
				'porcentaje'          => 'Number',
				'activo'              => 'Boolean',
				'operador_id'         => 'ForeignKey',
				'created_at'          => 'Date',
				'updated_at'          => 'Date',
		);
	}
}
