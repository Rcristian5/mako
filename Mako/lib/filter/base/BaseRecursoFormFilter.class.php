<?php

/**
 * Recurso filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseRecursoFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'categoria_recursos_id'   => new sfWidgetFormPropelChoice(array('model' => 'CategoriaRecursos', 'add_empty' => true)),
				'unidad_id'               => new sfWidgetFormPropelChoice(array('model' => 'UnidadProducto', 'add_empty' => true)),
				'codigo'                  => new sfWidgetFormFilterInput(),
				'nombre'                  => new sfWidgetFormFilterInput(),
				'activo'                  => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'operador_id'             => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'created_at'              => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'updated_at'              => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'recursos_asignados_list' => new sfWidgetFormPropelChoice(array('model' => 'Curso', 'add_empty' => true)),
		));

		$this->setValidators(array(
				'categoria_recursos_id'   => new sfValidatorPropelChoice(array('required' => false, 'model' => 'CategoriaRecursos', 'column' => 'id')),
				'unidad_id'               => new sfValidatorPropelChoice(array('required' => false, 'model' => 'UnidadProducto', 'column' => 'id')),
				'codigo'                  => new sfValidatorPass(array('required' => false)),
				'nombre'                  => new sfValidatorPass(array('required' => false)),
				'activo'                  => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'operador_id'             => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Usuario', 'column' => 'id')),
				'created_at'              => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'updated_at'              => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'recursos_asignados_list' => new sfValidatorPropelChoice(array('model' => 'Curso', 'required' => false)),
		));

		$this->widgetSchema->setNameFormat('recurso_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function addRecursosAsignadosListColumnCriteria(Criteria $criteria, $field, $values)
	{
		if (!is_array($values))
		{
			$values = array($values);
		}

		if (!count($values))
		{
			return;
		}

		$criteria->addJoin(RecursosAsignadosPeer::RECURSO_ID, RecursoPeer::ID);

		$value = array_pop($values);
		$criterion = $criteria->getNewCriterion(RecursosAsignadosPeer::CURSO_ID, $value);

		foreach ($values as $value)
		{
			$criterion->addOr($criteria->getNewCriterion(RecursosAsignadosPeer::CURSO_ID, $value));
		}

		$criteria->add($criterion);
	}

	public function getModelName()
	{
		return 'Recurso';
	}

	public function getFields()
	{
		return array(
				'id'                      => 'Number',
				'categoria_recursos_id'   => 'ForeignKey',
				'unidad_id'               => 'ForeignKey',
				'codigo'                  => 'Text',
				'nombre'                  => 'Text',
				'activo'                  => 'Boolean',
				'operador_id'             => 'ForeignKey',
				'created_at'              => 'Date',
				'updated_at'              => 'Date',
				'recursos_asignados_list' => 'ManyKey',
		);
	}
}
