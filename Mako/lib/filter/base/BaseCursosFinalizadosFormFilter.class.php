<?php

/**
 * CursosFinalizados filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCursosFinalizadosFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'curso_id'                    => new sfWidgetFormPropelChoice(array('model' => 'Curso', 'add_empty' => true)),
				'alumno_id'                   => new sfWidgetFormPropelChoice(array('model' => 'Alumnos', 'add_empty' => true)),
				'grupo_id'                    => new sfWidgetFormPropelChoice(array('model' => 'Grupo', 'add_empty' => true)),
				'calificacion_id'             => new sfWidgetFormPropelChoice(array('model' => 'CalificacionFinal', 'add_empty' => true)),
				'promedio_final'              => new sfWidgetFormFilterInput(),
				'recursado'                   => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'created_at'                  => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'updated_at'                  => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'estatus_curso_finalizado_id' => new sfWidgetFormPropelChoice(array('model' => 'EstatusCursoFinalizado', 'add_empty' => true)),
		));

		$this->setValidators(array(
				'curso_id'                    => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Curso', 'column' => 'id')),
				'alumno_id'                   => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Alumnos', 'column' => 'matricula')),
				'grupo_id'                    => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Grupo', 'column' => 'id')),
				'calificacion_id'             => new sfValidatorPropelChoice(array('required' => false, 'model' => 'CalificacionFinal', 'column' => 'id')),
				'promedio_final'              => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
				'recursado'                   => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'created_at'                  => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'updated_at'                  => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'estatus_curso_finalizado_id' => new sfValidatorPropelChoice(array('required' => false, 'model' => 'EstatusCursoFinalizado', 'column' => 'id')),
		));

		$this->widgetSchema->setNameFormat('cursos_finalizados_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'CursosFinalizados';
	}

	public function getFields()
	{
		return array(
				'id'                          => 'Number',
				'curso_id'                    => 'ForeignKey',
				'alumno_id'                   => 'ForeignKey',
				'grupo_id'                    => 'ForeignKey',
				'calificacion_id'             => 'ForeignKey',
				'promedio_final'              => 'Number',
				'recursado'                   => 'Boolean',
				'created_at'                  => 'Date',
				'updated_at'                  => 'Date',
				'estatus_curso_finalizado_id' => 'ForeignKey',
		);
	}
}
