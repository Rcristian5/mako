<?php

/**
 * Centro filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCentroFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'ip'                  => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'hostname'            => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'nombre'              => new sfWidgetFormFilterInput(),
				'alias'               => new sfWidgetFormFilterInput(),
				'razon_social'        => new sfWidgetFormFilterInput(),
				'estado'              => new sfWidgetFormFilterInput(),
				'municipio'           => new sfWidgetFormFilterInput(),
				'colonia'             => new sfWidgetFormFilterInput(),
				'cp'                  => new sfWidgetFormFilterInput(),
				'calle'               => new sfWidgetFormFilterInput(),
				'num_ext'             => new sfWidgetFormFilterInput(),
				'num_int'             => new sfWidgetFormFilterInput(),
				'lat'                 => new sfWidgetFormFilterInput(),
				'long'                => new sfWidgetFormFilterInput(),
				'dirgmaps'            => new sfWidgetFormFilterInput(),
				'iva'                 => new sfWidgetFormFilterInput(),
				'rfc'                 => new sfWidgetFormFilterInput(),
				'desde'               => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'hora_apertura'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'hora_cierre'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'pcs_usuario'         => new sfWidgetFormFilterInput(),
				'pcs_operacion'       => new sfWidgetFormFilterInput(),
				'key_gmaps'           => new sfWidgetFormFilterInput(),
				'activo'              => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'created_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'updated_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'alias_reporte'       => new sfWidgetFormFilterInput(),
				'zona_id'             => new sfWidgetFormPropelChoice(array('model' => 'Zona', 'add_empty' => true)),
				'stock_list'          => new sfWidgetFormPropelChoice(array('model' => 'Producto', 'add_empty' => true)),
				'usuario_centro_list' => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'curso_centro_list'   => new sfWidgetFormPropelChoice(array('model' => 'Curso', 'add_empty' => true)),
		));

		$this->setValidators(array(
				'ip'                  => new sfValidatorPass(array('required' => false)),
				'hostname'            => new sfValidatorPass(array('required' => false)),
				'nombre'              => new sfValidatorPass(array('required' => false)),
				'alias'               => new sfValidatorPass(array('required' => false)),
				'razon_social'        => new sfValidatorPass(array('required' => false)),
				'estado'              => new sfValidatorPass(array('required' => false)),
				'municipio'           => new sfValidatorPass(array('required' => false)),
				'colonia'             => new sfValidatorPass(array('required' => false)),
				'cp'                  => new sfValidatorPass(array('required' => false)),
				'calle'               => new sfValidatorPass(array('required' => false)),
				'num_ext'             => new sfValidatorPass(array('required' => false)),
				'num_int'             => new sfValidatorPass(array('required' => false)),
				'lat'                 => new sfValidatorPass(array('required' => false)),
				'long'                => new sfValidatorPass(array('required' => false)),
				'dirgmaps'            => new sfValidatorPass(array('required' => false)),
				'iva'                 => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
				'rfc'                 => new sfValidatorPass(array('required' => false)),
				'desde'               => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'hora_apertura'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'hora_cierre'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'pcs_usuario'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'pcs_operacion'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'key_gmaps'           => new sfValidatorPass(array('required' => false)),
				'activo'              => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'created_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'updated_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'alias_reporte'       => new sfValidatorPass(array('required' => false)),
				'zona_id'             => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Zona', 'column' => 'id')),
				'stock_list'          => new sfValidatorPropelChoice(array('model' => 'Producto', 'required' => false)),
				'usuario_centro_list' => new sfValidatorPropelChoice(array('model' => 'Usuario', 'required' => false)),
				'curso_centro_list'   => new sfValidatorPropelChoice(array('model' => 'Curso', 'required' => false)),
		));

		$this->widgetSchema->setNameFormat('centro_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function addStockListColumnCriteria(Criteria $criteria, $field, $values)
	{
		if (!is_array($values))
		{
			$values = array($values);
		}

		if (!count($values))
		{
			return;
		}

		$criteria->addJoin(StockPeer::CENTRO_ID, CentroPeer::ID);

		$value = array_pop($values);
		$criterion = $criteria->getNewCriterion(StockPeer::PRODUCTO_ID, $value);

		foreach ($values as $value)
		{
			$criterion->addOr($criteria->getNewCriterion(StockPeer::PRODUCTO_ID, $value));
		}

		$criteria->add($criterion);
	}

	public function addUsuarioCentroListColumnCriteria(Criteria $criteria, $field, $values)
	{
		if (!is_array($values))
		{
			$values = array($values);
		}

		if (!count($values))
		{
			return;
		}

		$criteria->addJoin(UsuarioCentroPeer::CENTRO_ID, CentroPeer::ID);

		$value = array_pop($values);
		$criterion = $criteria->getNewCriterion(UsuarioCentroPeer::USUARIO_ID, $value);

		foreach ($values as $value)
		{
			$criterion->addOr($criteria->getNewCriterion(UsuarioCentroPeer::USUARIO_ID, $value));
		}

		$criteria->add($criterion);
	}

	public function addCursoCentroListColumnCriteria(Criteria $criteria, $field, $values)
	{
		if (!is_array($values))
		{
			$values = array($values);
		}

		if (!count($values))
		{
			return;
		}

		$criteria->addJoin(CursoCentroPeer::CENTRO_ID, CentroPeer::ID);

		$value = array_pop($values);
		$criterion = $criteria->getNewCriterion(CursoCentroPeer::CURSO_ID, $value);

		foreach ($values as $value)
		{
			$criterion->addOr($criteria->getNewCriterion(CursoCentroPeer::CURSO_ID, $value));
		}

		$criteria->add($criterion);
	}

	public function getModelName()
	{
		return 'Centro';
	}

	public function getFields()
	{
		return array(
				'id'                  => 'Number',
				'ip'                  => 'Text',
				'hostname'            => 'Text',
				'nombre'              => 'Text',
				'alias'               => 'Text',
				'razon_social'        => 'Text',
				'estado'              => 'Text',
				'municipio'           => 'Text',
				'colonia'             => 'Text',
				'cp'                  => 'Text',
				'calle'               => 'Text',
				'num_ext'             => 'Text',
				'num_int'             => 'Text',
				'lat'                 => 'Text',
				'long'                => 'Text',
				'dirgmaps'            => 'Text',
				'iva'                 => 'Number',
				'rfc'                 => 'Text',
				'desde'               => 'Date',
				'hora_apertura'       => 'Date',
				'hora_cierre'         => 'Date',
				'pcs_usuario'         => 'Number',
				'pcs_operacion'       => 'Number',
				'key_gmaps'           => 'Text',
				'activo'              => 'Boolean',
				'created_at'          => 'Date',
				'updated_at'          => 'Date',
				'alias_reporte'       => 'Text',
				'zona_id'             => 'ForeignKey',
				'stock_list'          => 'ManyKey',
				'usuario_centro_list' => 'ManyKey',
				'curso_centro_list'   => 'ManyKey',
		);
	}
}
