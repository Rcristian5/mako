<?php

/**
 * Empresa filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseEmpresaFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'nombre'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'razon_social'    => new sfWidgetFormFilterInput(),
				'tipo_id'         => new sfWidgetFormPropelChoice(array('model' => 'TipoEmpresa', 'add_empty' => true)),
				'id_estado'       => new sfWidgetFormPropelChoice(array('model' => 'EntidadFederativa', 'add_empty' => true)),
				'estado'          => new sfWidgetFormFilterInput(),
				'ciudad'          => new sfWidgetFormFilterInput(),
				'id_del'          => new sfWidgetFormFilterInput(),
				'municipio'       => new sfWidgetFormFilterInput(),
				'id_municipio'    => new sfWidgetFormFilterInput(),
				'colonia'         => new sfWidgetFormFilterInput(),
				'id_asentamiento' => new sfWidgetFormFilterInput(),
				'cp'              => new sfWidgetFormFilterInput(),
				'calle'           => new sfWidgetFormFilterInput(),
				'num_ext'         => new sfWidgetFormFilterInput(),
				'num_int'         => new sfWidgetFormFilterInput(),
				'activo'          => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'created_at'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'updated_at'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
		));

		$this->setValidators(array(
				'nombre'          => new sfValidatorPass(array('required' => false)),
				'razon_social'    => new sfValidatorPass(array('required' => false)),
				'tipo_id'         => new sfValidatorPropelChoice(array('required' => false, 'model' => 'TipoEmpresa', 'column' => 'id')),
				'id_estado'       => new sfValidatorPropelChoice(array('required' => false, 'model' => 'EntidadFederativa', 'column' => 'id')),
				'estado'          => new sfValidatorPass(array('required' => false)),
				'ciudad'          => new sfValidatorPass(array('required' => false)),
				'id_del'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'municipio'       => new sfValidatorPass(array('required' => false)),
				'id_municipio'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'colonia'         => new sfValidatorPass(array('required' => false)),
				'id_asentamiento' => new sfValidatorPass(array('required' => false)),
				'cp'              => new sfValidatorPass(array('required' => false)),
				'calle'           => new sfValidatorPass(array('required' => false)),
				'num_ext'         => new sfValidatorPass(array('required' => false)),
				'num_int'         => new sfValidatorPass(array('required' => false)),
				'activo'          => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'created_at'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'updated_at'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
		));

		$this->widgetSchema->setNameFormat('empresa_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Empresa';
	}

	public function getFields()
	{
		return array(
				'id'              => 'Number',
				'nombre'          => 'Text',
				'razon_social'    => 'Text',
				'tipo_id'         => 'ForeignKey',
				'id_estado'       => 'ForeignKey',
				'estado'          => 'Text',
				'ciudad'          => 'Text',
				'id_del'          => 'Number',
				'municipio'       => 'Text',
				'id_municipio'    => 'Number',
				'colonia'         => 'Text',
				'id_asentamiento' => 'Text',
				'cp'              => 'Text',
				'calle'           => 'Text',
				'num_ext'         => 'Text',
				'num_int'         => 'Text',
				'activo'          => 'Boolean',
				'created_at'      => 'Date',
				'updated_at'      => 'Date',
		);
	}
}
