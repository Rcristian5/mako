<?php

/**
 * Salida filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseSalidaFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'centro_id'        => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'operador_id'      => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'monto'            => new sfWidgetFormFilterInput(),
				'tipo_id'          => new sfWidgetFormPropelChoice(array('model' => 'TipoSalida', 'add_empty' => true)),
				'folio_docto_aval' => new sfWidgetFormFilterInput(),
				'folio'            => new sfWidgetFormFilterInput(),
				'ip_caja'          => new sfWidgetFormFilterInput(),
				'observaciones'    => new sfWidgetFormFilterInput(),
				'orden_id'         => new sfWidgetFormPropelChoice(array('model' => 'Orden', 'add_empty' => true)),
				'created_at'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
		));

		$this->setValidators(array(
				'centro_id'        => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Centro', 'column' => 'id')),
				'operador_id'      => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Usuario', 'column' => 'id')),
				'monto'            => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
				'tipo_id'          => new sfValidatorPropelChoice(array('required' => false, 'model' => 'TipoSalida', 'column' => 'id')),
				'folio_docto_aval' => new sfValidatorPass(array('required' => false)),
				'folio'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'ip_caja'          => new sfValidatorPass(array('required' => false)),
				'observaciones'    => new sfValidatorPass(array('required' => false)),
				'orden_id'         => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Orden', 'column' => 'id')),
				'created_at'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
		));

		$this->widgetSchema->setNameFormat('salida_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Salida';
	}

	public function getFields()
	{
		return array(
				'id'               => 'Number',
				'centro_id'        => 'ForeignKey',
				'operador_id'      => 'ForeignKey',
				'monto'            => 'Number',
				'tipo_id'          => 'ForeignKey',
				'folio_docto_aval' => 'Text',
				'folio'            => 'Number',
				'ip_caja'          => 'Text',
				'observaciones'    => 'Text',
				'orden_id'         => 'ForeignKey',
				'created_at'       => 'Date',
		);
	}
}
