<?php

/**
 * Aula filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseAulaFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'centro_id'   => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'nombre'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'descripcion' => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'capacidad'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'disponible'  => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'activo'      => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'operador_id' => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'created_at'  => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'updated_at'  => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
		));

		$this->setValidators(array(
				'centro_id'   => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Centro', 'column' => 'id')),
				'nombre'      => new sfValidatorPass(array('required' => false)),
				'descripcion' => new sfValidatorPass(array('required' => false)),
				'capacidad'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'disponible'  => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'activo'      => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'operador_id' => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Usuario', 'column' => 'id')),
				'created_at'  => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'updated_at'  => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
		));

		$this->widgetSchema->setNameFormat('aula_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Aula';
	}

	public function getFields()
	{
		return array(
				'id'          => 'Number',
				'centro_id'   => 'ForeignKey',
				'nombre'      => 'Text',
				'descripcion' => 'Text',
				'capacidad'   => 'Number',
				'disponible'  => 'Boolean',
				'activo'      => 'Boolean',
				'operador_id' => 'ForeignKey',
				'created_at'  => 'Date',
				'updated_at'  => 'Date',
		);
	}
}
