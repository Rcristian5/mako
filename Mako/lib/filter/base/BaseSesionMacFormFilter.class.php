<?php

/**
 * SesionMac filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseSesionMacFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'ip_host'    => new sfWidgetFormFilterInput(),
				'ip_virtual' => new sfWidgetFormFilterInput(array('with_empty' => false)),
		));

		$this->setValidators(array(
				'ip_host'    => new sfValidatorPass(array('required' => false)),
				'ip_virtual' => new sfValidatorPass(array('required' => false)),
		));

		$this->widgetSchema->setNameFormat('sesion_mac_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'SesionMac';
	}

	public function getFields()
	{
		return array(
				'ip'         => 'Text',
				'ip_host'    => 'Text',
				'ip_virtual' => 'Text',
		);
	}
}
