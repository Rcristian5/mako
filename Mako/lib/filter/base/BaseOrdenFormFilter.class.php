<?php

/**
 * Orden filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseOrdenFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'centro_id'        => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'socio_id'         => new sfWidgetFormPropelChoice(array('model' => 'Socio', 'add_empty' => true)),
				'folio'            => new sfWidgetFormFilterInput(),
				'total'            => new sfWidgetFormFilterInput(),
				'forma_pago_id'    => new sfWidgetFormPropelChoice(array('model' => 'FormaPago', 'add_empty' => true)),
				'operador_id'      => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'tipo_id'          => new sfWidgetFormPropelChoice(array('model' => 'TipoOrden', 'add_empty' => true)),
				'ip_caja'          => new sfWidgetFormFilterInput(),
				'es_factura'       => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'razon_social'     => new sfWidgetFormFilterInput(),
				'rfc'              => new sfWidgetFormFilterInput(),
				'calle_fiscal'     => new sfWidgetFormFilterInput(),
				'colonia_fiscal'   => new sfWidgetFormFilterInput(),
				'cp_fiscal'        => new sfWidgetFormFilterInput(),
				'estado_fiscal'    => new sfWidgetFormFilterInput(),
				'municipio_fiscal' => new sfWidgetFormFilterInput(),
				'num_ext_fiscal'   => new sfWidgetFormFilterInput(),
				'num_int_fiscal'   => new sfWidgetFormFilterInput(),
				'pais_fiscal'      => new sfWidgetFormFilterInput(),
				'estatus_id'       => new sfWidgetFormPropelChoice(array('model' => 'EstatusOrden', 'add_empty' => true)),
				'corte_id'         => new sfWidgetFormPropelChoice(array('model' => 'Corte', 'add_empty' => true)),
				'factura_id'       => new sfWidgetFormPropelChoice(array('model' => 'Factura', 'add_empty' => true)),
				'created_at'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'updated_at'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
		));

		$this->setValidators(array(
				'centro_id'        => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Centro', 'column' => 'id')),
				'socio_id'         => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Socio', 'column' => 'id')),
				'folio'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'total'            => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
				'forma_pago_id'    => new sfValidatorPropelChoice(array('required' => false, 'model' => 'FormaPago', 'column' => 'id')),
				'operador_id'      => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Usuario', 'column' => 'id')),
				'tipo_id'          => new sfValidatorPropelChoice(array('required' => false, 'model' => 'TipoOrden', 'column' => 'id')),
				'ip_caja'          => new sfValidatorPass(array('required' => false)),
				'es_factura'       => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'razon_social'     => new sfValidatorPass(array('required' => false)),
				'rfc'              => new sfValidatorPass(array('required' => false)),
				'calle_fiscal'     => new sfValidatorPass(array('required' => false)),
				'colonia_fiscal'   => new sfValidatorPass(array('required' => false)),
				'cp_fiscal'        => new sfValidatorPass(array('required' => false)),
				'estado_fiscal'    => new sfValidatorPass(array('required' => false)),
				'municipio_fiscal' => new sfValidatorPass(array('required' => false)),
				'num_ext_fiscal'   => new sfValidatorPass(array('required' => false)),
				'num_int_fiscal'   => new sfValidatorPass(array('required' => false)),
				'pais_fiscal'      => new sfValidatorPass(array('required' => false)),
				'estatus_id'       => new sfValidatorPropelChoice(array('required' => false, 'model' => 'EstatusOrden', 'column' => 'id')),
				'corte_id'         => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Corte', 'column' => 'id')),
				'factura_id'       => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Factura', 'column' => 'id')),
				'created_at'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'updated_at'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
		));

		$this->widgetSchema->setNameFormat('orden_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Orden';
	}

	public function getFields()
	{
		return array(
				'id'               => 'Number',
				'centro_id'        => 'ForeignKey',
				'socio_id'         => 'ForeignKey',
				'folio'            => 'Number',
				'total'            => 'Number',
				'forma_pago_id'    => 'ForeignKey',
				'operador_id'      => 'ForeignKey',
				'tipo_id'          => 'ForeignKey',
				'ip_caja'          => 'Text',
				'es_factura'       => 'Boolean',
				'razon_social'     => 'Text',
				'rfc'              => 'Text',
				'calle_fiscal'     => 'Text',
				'colonia_fiscal'   => 'Text',
				'cp_fiscal'        => 'Text',
				'estado_fiscal'    => 'Text',
				'municipio_fiscal' => 'Text',
				'num_ext_fiscal'   => 'Text',
				'num_int_fiscal'   => 'Text',
				'pais_fiscal'      => 'Text',
				'estatus_id'       => 'ForeignKey',
				'corte_id'         => 'ForeignKey',
				'factura_id'       => 'ForeignKey',
				'created_at'       => 'Date',
				'updated_at'       => 'Date',
		);
	}
}
