<?php

/**
 * EstatusSocios filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseEstatusSociosFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'nuevos'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'activos'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
		));

		$this->setValidators(array(
				'nuevos'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'activos'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
		));

		$this->widgetSchema->setNameFormat('estatus_socios_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'EstatusSocios';
	}

	public function getFields()
	{
		return array(
				'centro_id' => 'ForeignKey',
				'fecha'     => 'Date',
				'nuevos'    => 'Number',
				'activos'   => 'Number',
		);
	}
}
