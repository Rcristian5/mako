<?php

/**
 * AlumnoPagos filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseAlumnoPagosFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'centro_id'       => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'socio_id'        => new sfWidgetFormPropelChoice(array('model' => 'Socio', 'add_empty' => true)),
				'curso_id'        => new sfWidgetFormPropelChoice(array('model' => 'Curso', 'add_empty' => true)),
				'grupo_id'        => new sfWidgetFormPropelChoice(array('model' => 'Grupo', 'add_empty' => true)),
				'tipo_cobro_id'   => new sfWidgetFormPropelChoice(array('model' => 'TipoCobro', 'add_empty' => true)),
				'pagado'          => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'numero_pago'     => new sfWidgetFormFilterInput(),
				'cancelado'       => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'producto_id'     => new sfWidgetFormPropelChoice(array('model' => 'Producto', 'add_empty' => true)),
				'sub_producto_de' => new sfWidgetFormPropelChoice(array('model' => 'Producto', 'add_empty' => true)),
				'orden_base_id'   => new sfWidgetFormPropelChoice(array('model' => 'Orden', 'add_empty' => true)),
				'created_at'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'updated_at'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
		));

		$this->setValidators(array(
				'centro_id'       => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Centro', 'column' => 'id')),
				'socio_id'        => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Socio', 'column' => 'id')),
				'curso_id'        => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Curso', 'column' => 'id')),
				'grupo_id'        => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Grupo', 'column' => 'id')),
				'tipo_cobro_id'   => new sfValidatorPropelChoice(array('required' => false, 'model' => 'TipoCobro', 'column' => 'id')),
				'pagado'          => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'numero_pago'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'cancelado'       => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'producto_id'     => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Producto', 'column' => 'id')),
				'sub_producto_de' => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Producto', 'column' => 'id')),
				'orden_base_id'   => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Orden', 'column' => 'id')),
				'created_at'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'updated_at'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
		));

		$this->widgetSchema->setNameFormat('alumno_pagos_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'AlumnoPagos';
	}

	public function getFields()
	{
		return array(
				'id'              => 'Number',
				'centro_id'       => 'ForeignKey',
				'socio_id'        => 'ForeignKey',
				'curso_id'        => 'ForeignKey',
				'grupo_id'        => 'ForeignKey',
				'tipo_cobro_id'   => 'ForeignKey',
				'pagado'          => 'Boolean',
				'numero_pago'     => 'Number',
				'cancelado'       => 'Boolean',
				'producto_id'     => 'ForeignKey',
				'sub_producto_de' => 'ForeignKey',
				'orden_base_id'   => 'ForeignKey',
				'created_at'      => 'Date',
				'updated_at'      => 'Date',
		);
	}
}
