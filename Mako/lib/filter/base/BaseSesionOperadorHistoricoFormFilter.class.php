<?php

/**
 * SesionOperadorHistorico filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseSesionOperadorHistoricoFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'centro_id'     => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'ip'            => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'usuario_id'    => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'usuario'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'fecha_login'   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
				'fecha_logout'  => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
				'transcurridos' => new sfWidgetFormFilterInput(),
		));

		$this->setValidators(array(
				'centro_id'     => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Centro', 'column' => 'id')),
				'ip'            => new sfValidatorPass(array('required' => false)),
				'usuario_id'    => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Usuario', 'column' => 'id')),
				'usuario'       => new sfValidatorPass(array('required' => false)),
				'fecha_login'   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'fecha_logout'  => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'transcurridos' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
		));

		$this->widgetSchema->setNameFormat('sesion_operador_historico_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'SesionOperadorHistorico';
	}

	public function getFields()
	{
		return array(
				'id'            => 'Number',
				'centro_id'     => 'ForeignKey',
				'ip'            => 'Text',
				'usuario_id'    => 'ForeignKey',
				'usuario'       => 'Text',
				'fecha_login'   => 'Date',
				'fecha_logout'  => 'Date',
				'transcurridos' => 'Number',
		);
	}
}
