<?php

/**
 * Sesion filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseSesionFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'centro_id'      => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'computadora_id' => new sfWidgetFormPropelChoice(array('model' => 'Computadora', 'add_empty' => true)),
				'ip'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'socio_id'       => new sfWidgetFormPropelChoice(array('model' => 'Socio', 'add_empty' => true)),
				'usuario'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'fecha_login'    => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
				'tipo_id'        => new sfWidgetFormPropelChoice(array('model' => 'TipoSesion', 'add_empty' => true)),
				'saldo'          => new sfWidgetFormFilterInput(),
		));

		$this->setValidators(array(
				'centro_id'      => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Centro', 'column' => 'id')),
				'computadora_id' => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Computadora', 'column' => 'id')),
				'ip'             => new sfValidatorPass(array('required' => false)),
				'socio_id'       => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Socio', 'column' => 'id')),
				'usuario'        => new sfValidatorPass(array('required' => false)),
				'fecha_login'    => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'tipo_id'        => new sfValidatorPropelChoice(array('required' => false, 'model' => 'TipoSesion', 'column' => 'id')),
				'saldo'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
		));

		$this->widgetSchema->setNameFormat('sesion_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Sesion';
	}

	public function getFields()
	{
		return array(
				'id'             => 'Number',
				'centro_id'      => 'ForeignKey',
				'computadora_id' => 'ForeignKey',
				'ip'             => 'Text',
				'socio_id'       => 'ForeignKey',
				'usuario'        => 'Text',
				'fecha_login'    => 'Date',
				'tipo_id'        => 'ForeignKey',
				'saldo'          => 'Number',
		);
	}
}
