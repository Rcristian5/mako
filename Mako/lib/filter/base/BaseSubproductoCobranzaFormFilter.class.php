<?php

/**
 * SubproductoCobranza filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseSubproductoCobranzaFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'sub_producto_de' => new sfWidgetFormPropelChoice(array('model' => 'Producto', 'add_empty' => true)),
				'numero_pago'     => new sfWidgetFormFilterInput(),
		));

		$this->setValidators(array(
				'sub_producto_de' => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Producto', 'column' => 'id')),
				'numero_pago'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
		));

		$this->widgetSchema->setNameFormat('subproducto_cobranza_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'SubproductoCobranza';
	}

	public function getFields()
	{
		return array(
				'producto_id'     => 'ForeignKey',
				'sub_producto_de' => 'ForeignKey',
				'cobro_id'        => 'ForeignKey',
				'numero_pago'     => 'Number',
		);
	}
}
