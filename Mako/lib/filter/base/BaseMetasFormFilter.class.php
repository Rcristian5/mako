<?php

/**
 * Metas filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseMetasFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'centro_id'             => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'categoria_fija_id'     => new sfWidgetFormPropelChoice(array('model' => 'CategoriaMetasFijas', 'add_empty' => true)),
				'categoria_producto_id' => new sfWidgetFormPropelChoice(array('model' => 'CategoriaReporte', 'add_empty' => true)),
				'categoria_beca_id'     => new sfWidgetFormPropelChoice(array('model' => 'Beca', 'add_empty' => true)),
				'fecha_inicio'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
				'fecha_fin'             => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'semana'                => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'anio'                  => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'meta'                  => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'entero'                => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'usuario_id'            => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'activo'                => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'created_at'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'updated_at'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
		));

		$this->setValidators(array(
				'centro_id'             => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Centro', 'column' => 'id')),
				'categoria_fija_id'     => new sfValidatorPropelChoice(array('required' => false, 'model' => 'CategoriaMetasFijas', 'column' => 'id')),
				'categoria_producto_id' => new sfValidatorPropelChoice(array('required' => false, 'model' => 'CategoriaReporte', 'column' => 'id')),
				'categoria_beca_id'     => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Beca', 'column' => 'id')),
				'fecha_inicio'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'fecha_fin'             => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'semana'                => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'anio'                  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'meta'                  => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
				'entero'                => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'usuario_id'            => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Usuario', 'column' => 'id')),
				'activo'                => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'created_at'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'updated_at'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
		));

		$this->widgetSchema->setNameFormat('metas_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Metas';
	}

	public function getFields()
	{
		return array(
				'id'                    => 'Number',
				'centro_id'             => 'ForeignKey',
				'categoria_fija_id'     => 'ForeignKey',
				'categoria_producto_id' => 'ForeignKey',
				'categoria_beca_id'     => 'ForeignKey',
				'fecha_inicio'          => 'Date',
				'fecha_fin'             => 'Date',
				'semana'                => 'Number',
				'anio'                  => 'Number',
				'meta'                  => 'Number',
				'entero'                => 'Boolean',
				'usuario_id'            => 'ForeignKey',
				'activo'                => 'Boolean',
				'created_at'            => 'Date',
				'updated_at'            => 'Date',
		);
	}
}
