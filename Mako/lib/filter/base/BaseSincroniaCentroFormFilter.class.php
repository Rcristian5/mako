<?php

/**
 * SincroniaCentro filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseSincroniaCentroFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'sincronia_id'   => new sfWidgetFormPropelChoice(array('model' => 'Sincronia', 'add_empty' => true)),
				'centro_id'      => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'sincronizado'   => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'ultimo_mensaje' => new sfWidgetFormFilterInput(),
				'intento'        => new sfWidgetFormFilterInput(),
				'created_at'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'updated_at'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
		));

		$this->setValidators(array(
				'sincronia_id'   => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Sincronia', 'column' => 'id')),
				'centro_id'      => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Centro', 'column' => 'id')),
				'sincronizado'   => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'ultimo_mensaje' => new sfValidatorPass(array('required' => false)),
				'intento'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'created_at'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'updated_at'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
		));

		$this->widgetSchema->setNameFormat('sincronia_centro_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'SincroniaCentro';
	}

	public function getFields()
	{
		return array(
				'id'             => 'Number',
				'sincronia_id'   => 'ForeignKey',
				'centro_id'      => 'ForeignKey',
				'sincronizado'   => 'Boolean',
				'ultimo_mensaje' => 'Text',
				'intento'        => 'Number',
				'created_at'     => 'Date',
				'updated_at'     => 'Date',
		);
	}
}
