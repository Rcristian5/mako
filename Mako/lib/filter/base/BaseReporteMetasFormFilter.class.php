<?php

/**
 * ReporteMetas filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseReporteMetasFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'centro_id'             => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'categoria_fija_id'     => new sfWidgetFormPropelChoice(array('model' => 'CategoriaMetasFijas', 'add_empty' => true)),
				'categoria_producto_id' => new sfWidgetFormPropelChoice(array('model' => 'CategoriaReporte', 'add_empty' => true)),
				'categoria_beca_id'     => new sfWidgetFormPropelChoice(array('model' => 'Beca', 'add_empty' => true)),
				'cantidad'              => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'fecha'                 => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
				'semana'                => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'anio'                  => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'entero'                => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'meta_diaria'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'activo'                => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'sync'                  => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'created_at'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'updated_at'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
		));

		$this->setValidators(array(
				'centro_id'             => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Centro', 'column' => 'id')),
				'categoria_fija_id'     => new sfValidatorPropelChoice(array('required' => false, 'model' => 'CategoriaMetasFijas', 'column' => 'id')),
				'categoria_producto_id' => new sfValidatorPropelChoice(array('required' => false, 'model' => 'CategoriaReporte', 'column' => 'id')),
				'categoria_beca_id'     => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Beca', 'column' => 'id')),
				'cantidad'              => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
				'fecha'                 => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'semana'                => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'anio'                  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'entero'                => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'meta_diaria'           => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
				'activo'                => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'sync'                  => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'created_at'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'updated_at'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
		));

		$this->widgetSchema->setNameFormat('reporte_metas_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'ReporteMetas';
	}

	public function getFields()
	{
		return array(
				'id'                    => 'Number',
				'centro_id'             => 'ForeignKey',
				'categoria_fija_id'     => 'ForeignKey',
				'categoria_producto_id' => 'ForeignKey',
				'categoria_beca_id'     => 'ForeignKey',
				'cantidad'              => 'Number',
				'fecha'                 => 'Date',
				'semana'                => 'Number',
				'anio'                  => 'Number',
				'entero'                => 'Boolean',
				'meta_diaria'           => 'Number',
				'activo'                => 'Boolean',
				'sync'                  => 'Boolean',
				'created_at'            => 'Date',
				'updated_at'            => 'Date',
		);
	}
}
