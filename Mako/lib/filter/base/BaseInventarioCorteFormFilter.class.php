<?php

/**
 * InventarioCorte filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseInventarioCorteFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'operacion_id' => new sfWidgetFormPropelChoice(array('model' => 'InventarioOperacion', 'add_empty' => true)),
				'estado'       => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'centro_id'    => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'created_at'   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'updated_at'   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
		));

		$this->setValidators(array(
				'operacion_id' => new sfValidatorPropelChoice(array('required' => false, 'model' => 'InventarioOperacion', 'column' => 'id')),
				'estado'       => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'centro_id'    => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Centro', 'column' => 'id')),
				'created_at'   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'updated_at'   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
		));

		$this->widgetSchema->setNameFormat('inventario_corte_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'InventarioCorte';
	}

	public function getFields()
	{
		return array(
				'id'           => 'Number',
				'operacion_id' => 'ForeignKey',
				'estado'       => 'Boolean',
				'centro_id'    => 'ForeignKey',
				'created_at'   => 'Date',
				'updated_at'   => 'Date',
		);
	}
}
