<?php

/**
 * GrupoAlumnos filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseGrupoAlumnosFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'grupo_id'          => new sfWidgetFormPropelChoice(array('model' => 'Grupo', 'add_empty' => true)),
				'alumno_id'         => new sfWidgetFormPropelChoice(array('model' => 'Alumnos', 'add_empty' => true)),
				'preinscrito'       => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'fecha_preinscrito' => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
				'created_at'        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'updated_at'        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
		));

		$this->setValidators(array(
				'grupo_id'          => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Grupo', 'column' => 'id')),
				'alumno_id'         => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Alumnos', 'column' => 'matricula')),
				'preinscrito'       => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'fecha_preinscrito' => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'created_at'        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'updated_at'        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
		));

		$this->widgetSchema->setNameFormat('grupo_alumnos_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'GrupoAlumnos';
	}

	public function getFields()
	{
		return array(
				'id'                => 'Number',
				'grupo_id'          => 'ForeignKey',
				'alumno_id'         => 'ForeignKey',
				'preinscrito'       => 'Boolean',
				'fecha_preinscrito' => 'Date',
				'created_at'        => 'Date',
				'updated_at'        => 'Date',
		);
	}
}
