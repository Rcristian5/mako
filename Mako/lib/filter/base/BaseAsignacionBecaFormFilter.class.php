<?php

/**
 * AsignacionBeca filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseAsignacionBecaFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'beca_id'              => new sfWidgetFormPropelChoice(array('model' => 'Beca', 'add_empty' => true)),
				'centro_id'            => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'socio_id'             => new sfWidgetFormPropelChoice(array('model' => 'Socio', 'add_empty' => true)),
				'registro_operador_id' => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'fecha_solicitud'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'otorga_operador_id'   => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'fecha_otorgado'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
		));

		$this->setValidators(array(
				'beca_id'              => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Beca', 'column' => 'id')),
				'centro_id'            => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Centro', 'column' => 'id')),
				'socio_id'             => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Socio', 'column' => 'id')),
				'registro_operador_id' => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Usuario', 'column' => 'id')),
				'fecha_solicitud'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'otorga_operador_id'   => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Usuario', 'column' => 'id')),
				'fecha_otorgado'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
		));

		$this->widgetSchema->setNameFormat('asignacion_beca_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'AsignacionBeca';
	}

	public function getFields()
	{
		return array(
				'id'                   => 'Number',
				'beca_id'              => 'ForeignKey',
				'centro_id'            => 'ForeignKey',
				'socio_id'             => 'ForeignKey',
				'registro_operador_id' => 'ForeignKey',
				'fecha_solicitud'      => 'Date',
				'otorga_operador_id'   => 'ForeignKey',
				'fecha_otorgado'       => 'Date',
		);
	}
}
