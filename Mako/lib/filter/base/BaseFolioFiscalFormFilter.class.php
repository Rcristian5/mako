<?php

/**
 * FolioFiscal filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseFolioFiscalFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'centro_id'         => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'serie'             => new sfWidgetFormFilterInput(),
				'no_aprobacion'     => new sfWidgetFormFilterInput(),
				'ano_aprobacion'    => new sfWidgetFormFilterInput(),
				'serie_certificado' => new sfWidgetFormFilterInput(),
				'folio_inicial'     => new sfWidgetFormFilterInput(),
				'folio_final'       => new sfWidgetFormFilterInput(),
				'folio_actual'      => new sfWidgetFormFilterInput(),
				'activo'            => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'num_certificado'   => new sfWidgetFormFilterInput(),
				'created_at'        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'updated_at'        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
		));

		$this->setValidators(array(
				'centro_id'         => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Centro', 'column' => 'id')),
				'serie'             => new sfValidatorPass(array('required' => false)),
				'no_aprobacion'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'ano_aprobacion'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'serie_certificado' => new sfValidatorPass(array('required' => false)),
				'folio_inicial'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'folio_final'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'folio_actual'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'activo'            => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'num_certificado'   => new sfValidatorPass(array('required' => false)),
				'created_at'        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'updated_at'        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
		));

		$this->widgetSchema->setNameFormat('folio_fiscal_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'FolioFiscal';
	}

	public function getFields()
	{
		return array(
				'id'                => 'Number',
				'centro_id'         => 'ForeignKey',
				'serie'             => 'Text',
				'no_aprobacion'     => 'Number',
				'ano_aprobacion'    => 'Number',
				'serie_certificado' => 'Text',
				'folio_inicial'     => 'Number',
				'folio_final'       => 'Number',
				'folio_actual'      => 'Number',
				'activo'            => 'Boolean',
				'num_certificado'   => 'Text',
				'created_at'        => 'Date',
				'updated_at'        => 'Date',
		);
	}
}
