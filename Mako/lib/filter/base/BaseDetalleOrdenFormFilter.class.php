<?php

/**
 * DetalleOrden filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseDetalleOrdenFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'orden_id'     => new sfWidgetFormPropelChoice(array('model' => 'Orden', 'add_empty' => true)),
				'centro_id'    => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'socio_id'     => new sfWidgetFormPropelChoice(array('model' => 'Socio', 'add_empty' => true)),
				'producto_id'  => new sfWidgetFormPropelChoice(array('model' => 'Producto', 'add_empty' => true)),
				'precio_lista' => new sfWidgetFormFilterInput(),
				'tiempo'       => new sfWidgetFormFilterInput(),
				'cantidad'     => new sfWidgetFormFilterInput(),
				'descuento'    => new sfWidgetFormFilterInput(),
				'subtotal'     => new sfWidgetFormFilterInput(),
				'cupon_id'     => new sfWidgetFormPropelChoice(array('model' => 'Cupon', 'add_empty' => true)),
				'promocion_id' => new sfWidgetFormPropelChoice(array('model' => 'Promocion', 'add_empty' => true)),
				'created_at'   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'updated_at'   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
		));

		$this->setValidators(array(
				'orden_id'     => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Orden', 'column' => 'id')),
				'centro_id'    => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Centro', 'column' => 'id')),
				'socio_id'     => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Socio', 'column' => 'id')),
				'producto_id'  => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Producto', 'column' => 'id')),
				'precio_lista' => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
				'tiempo'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'cantidad'     => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
				'descuento'    => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
				'subtotal'     => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
				'cupon_id'     => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Cupon', 'column' => 'id')),
				'promocion_id' => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Promocion', 'column' => 'id')),
				'created_at'   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'updated_at'   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
		));

		$this->widgetSchema->setNameFormat('detalle_orden_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'DetalleOrden';
	}

	public function getFields()
	{
		return array(
				'id'           => 'Number',
				'orden_id'     => 'ForeignKey',
				'centro_id'    => 'ForeignKey',
				'socio_id'     => 'ForeignKey',
				'producto_id'  => 'ForeignKey',
				'precio_lista' => 'Number',
				'tiempo'       => 'Number',
				'cantidad'     => 'Number',
				'descuento'    => 'Number',
				'subtotal'     => 'Number',
				'cupon_id'     => 'ForeignKey',
				'promocion_id' => 'ForeignKey',
				'created_at'   => 'Date',
				'updated_at'   => 'Date',
		);
	}
}
