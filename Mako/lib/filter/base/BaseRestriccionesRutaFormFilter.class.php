<?php

/**
 * RestriccionesRuta filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseRestriccionesRutaFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'tipo_restriccion_id'         => new sfWidgetFormPropelChoice(array('model' => 'TipoRestriccion', 'add_empty' => true)),
				'curso_id'                    => new sfWidgetFormPropelChoice(array('model' => 'Curso', 'add_empty' => true)),
				'restriccion_ruta_curso_list' => new sfWidgetFormPropelChoice(array('model' => 'Curso', 'add_empty' => true)),
		));

		$this->setValidators(array(
				'tipo_restriccion_id'         => new sfValidatorPropelChoice(array('required' => false, 'model' => 'TipoRestriccion', 'column' => 'id')),
				'curso_id'                    => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Curso', 'column' => 'id')),
				'restriccion_ruta_curso_list' => new sfValidatorPropelChoice(array('model' => 'Curso', 'required' => false)),
		));

		$this->widgetSchema->setNameFormat('restricciones_ruta_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function addRestriccionRutaCursoListColumnCriteria(Criteria $criteria, $field, $values)
	{
		if (!is_array($values))
		{
			$values = array($values);
		}

		if (!count($values))
		{
			return;
		}

		$criteria->addJoin(RestriccionRutaCursoPeer::RESTRICCION_RUTA_ID, RestriccionesRutaPeer::ID);

		$value = array_pop($values);
		$criterion = $criteria->getNewCriterion(RestriccionRutaCursoPeer::CURSO_ID, $value);

		foreach ($values as $value)
		{
			$criterion->addOr($criteria->getNewCriterion(RestriccionRutaCursoPeer::CURSO_ID, $value));
		}

		$criteria->add($criterion);
	}

	public function getModelName()
	{
		return 'RestriccionesRuta';
	}

	public function getFields()
	{
		return array(
				'id'                          => 'Number',
				'tipo_restriccion_id'         => 'ForeignKey',
				'curso_id'                    => 'ForeignKey',
				'restriccion_ruta_curso_list' => 'ManyKey',
		);
	}
}
