<?php

/**
 * Bitacora filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseBitacoraFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'curso_id'    => new sfWidgetFormPropelChoice(array('model' => 'Curso', 'add_empty' => true)),
				'grupo_id'    => new sfWidgetFormPropelChoice(array('model' => 'Grupo', 'add_empty' => true)),
				'comentario'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'operador_id' => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'created_at'  => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'updated_at'  => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
		));

		$this->setValidators(array(
				'curso_id'    => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Curso', 'column' => 'id')),
				'grupo_id'    => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Grupo', 'column' => 'id')),
				'comentario'  => new sfValidatorPass(array('required' => false)),
				'operador_id' => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Usuario', 'column' => 'id')),
				'created_at'  => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'updated_at'  => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
		));

		$this->widgetSchema->setNameFormat('bitacora_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Bitacora';
	}

	public function getFields()
	{
		return array(
				'id'          => 'Number',
				'alumno_id'   => 'ForeignKey',
				'curso_id'    => 'ForeignKey',
				'grupo_id'    => 'ForeignKey',
				'comentario'  => 'Text',
				'operador_id' => 'ForeignKey',
				'created_at'  => 'Date',
				'updated_at'  => 'Date',
		);
	}
}
