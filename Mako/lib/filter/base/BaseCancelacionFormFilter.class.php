<?php

/**
 * Cancelacion filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCancelacionFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'centro_id'            => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'operador_id'          => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'orden_id'             => new sfWidgetFormPropelChoice(array('model' => 'Orden', 'add_empty' => true)),
				'orden_cancelacion_id' => new sfWidgetFormPropelChoice(array('model' => 'Orden', 'add_empty' => true)),
				'observaciones'        => new sfWidgetFormFilterInput(),
				'created_at'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
		));

		$this->setValidators(array(
				'centro_id'            => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Centro', 'column' => 'id')),
				'operador_id'          => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Usuario', 'column' => 'id')),
				'orden_id'             => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Orden', 'column' => 'id')),
				'orden_cancelacion_id' => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Orden', 'column' => 'id')),
				'observaciones'        => new sfValidatorPass(array('required' => false)),
				'created_at'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
		));

		$this->widgetSchema->setNameFormat('cancelacion_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Cancelacion';
	}

	public function getFields()
	{
		return array(
				'id'                   => 'Number',
				'centro_id'            => 'ForeignKey',
				'operador_id'          => 'ForeignKey',
				'orden_id'             => 'ForeignKey',
				'orden_cancelacion_id' => 'ForeignKey',
				'observaciones'        => 'Text',
				'created_at'           => 'Date',
		);
	}
}
