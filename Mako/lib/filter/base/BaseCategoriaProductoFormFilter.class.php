<?php

/**
 * CategoriaProducto filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCategoriaProductoFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'nombre'                   => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'activo'                   => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'created_at'               => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'updated_at'               => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'promocion_categoria_list' => new sfWidgetFormPropelChoice(array('model' => 'Promocion', 'add_empty' => true)),
		));

		$this->setValidators(array(
				'nombre'                   => new sfValidatorPass(array('required' => false)),
				'activo'                   => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'created_at'               => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'updated_at'               => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'promocion_categoria_list' => new sfValidatorPropelChoice(array('model' => 'Promocion', 'required' => false)),
		));

		$this->widgetSchema->setNameFormat('categoria_producto_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function addPromocionCategoriaListColumnCriteria(Criteria $criteria, $field, $values)
	{
		if (!is_array($values))
		{
			$values = array($values);
		}

		if (!count($values))
		{
			return;
		}

		$criteria->addJoin(PromocionCategoriaPeer::CATEGORIA_ID, CategoriaProductoPeer::ID);

		$value = array_pop($values);
		$criterion = $criteria->getNewCriterion(PromocionCategoriaPeer::PROMOCION_ID, $value);

		foreach ($values as $value)
		{
			$criterion->addOr($criteria->getNewCriterion(PromocionCategoriaPeer::PROMOCION_ID, $value));
		}

		$criteria->add($criterion);
	}

	public function getModelName()
	{
		return 'CategoriaProducto';
	}

	public function getFields()
	{
		return array(
				'id'                       => 'Number',
				'nombre'                   => 'Text',
				'activo'                   => 'Boolean',
				'created_at'               => 'Date',
				'updated_at'               => 'Date',
				'promocion_categoria_list' => 'ManyKey',
		);
	}
}
