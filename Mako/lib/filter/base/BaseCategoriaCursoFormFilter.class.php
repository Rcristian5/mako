<?php

/**
 * CategoriaCurso filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCategoriaCursoFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'nombre'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'siglas'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'descripcion' => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'padre'       => new sfWidgetFormPropelChoice(array('model' => 'CategoriaCurso', 'add_empty' => true)),
				'profundidad' => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'path'        => new sfWidgetFormFilterInput(),
				'operador_id' => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'activa'      => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'created_at'  => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'updated_at'  => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
		));

		$this->setValidators(array(
				'nombre'      => new sfValidatorPass(array('required' => false)),
				'siglas'      => new sfValidatorPass(array('required' => false)),
				'descripcion' => new sfValidatorPass(array('required' => false)),
				'padre'       => new sfValidatorPropelChoice(array('required' => false, 'model' => 'CategoriaCurso', 'column' => 'id')),
				'profundidad' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'path'        => new sfValidatorPass(array('required' => false)),
				'operador_id' => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Usuario', 'column' => 'id')),
				'activa'      => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'created_at'  => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'updated_at'  => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
		));

		$this->widgetSchema->setNameFormat('categoria_curso_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'CategoriaCurso';
	}

	public function getFields()
	{
		return array(
				'id'          => 'Number',
				'nombre'      => 'Text',
				'siglas'      => 'Text',
				'descripcion' => 'Text',
				'padre'       => 'ForeignKey',
				'profundidad' => 'Number',
				'path'        => 'Text',
				'operador_id' => 'ForeignKey',
				'activa'      => 'Boolean',
				'created_at'  => 'Date',
				'updated_at'  => 'Date',
		);
	}
}
