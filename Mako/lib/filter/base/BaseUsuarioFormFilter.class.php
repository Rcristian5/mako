<?php

/**
 * Usuario filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseUsuarioFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'rol_id'              => new sfWidgetFormPropelChoice(array('model' => 'Rol', 'add_empty' => true)),
				'usuario'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'clave'               => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'nombre'              => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'apepat'              => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'apemat'              => new sfWidgetFormFilterInput(),
				'nombre_completo'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'email'               => new sfWidgetFormFilterInput(),
				'operador_id'         => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'activo'              => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'created_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'updated_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'usuario_centro_list' => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'usuario_perfil_list' => new sfWidgetFormPropelChoice(array('model' => 'PerfilFacilitador', 'add_empty' => true)),
		));

		$this->setValidators(array(
				'rol_id'              => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Rol', 'column' => 'id')),
				'usuario'             => new sfValidatorPass(array('required' => false)),
				'clave'               => new sfValidatorPass(array('required' => false)),
				'nombre'              => new sfValidatorPass(array('required' => false)),
				'apepat'              => new sfValidatorPass(array('required' => false)),
				'apemat'              => new sfValidatorPass(array('required' => false)),
				'nombre_completo'     => new sfValidatorPass(array('required' => false)),
				'email'               => new sfValidatorPass(array('required' => false)),
				'operador_id'         => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Usuario', 'column' => 'id')),
				'activo'              => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'created_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'updated_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'usuario_centro_list' => new sfValidatorPropelChoice(array('model' => 'Centro', 'required' => false)),
				'usuario_perfil_list' => new sfValidatorPropelChoice(array('model' => 'PerfilFacilitador', 'required' => false)),
		));

		$this->widgetSchema->setNameFormat('usuario_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function addUsuarioCentroListColumnCriteria(Criteria $criteria, $field, $values)
	{
		if (!is_array($values))
		{
			$values = array($values);
		}

		if (!count($values))
		{
			return;
		}

		$criteria->addJoin(UsuarioCentroPeer::USUARIO_ID, UsuarioPeer::ID);

		$value = array_pop($values);
		$criterion = $criteria->getNewCriterion(UsuarioCentroPeer::CENTRO_ID, $value);

		foreach ($values as $value)
		{
			$criterion->addOr($criteria->getNewCriterion(UsuarioCentroPeer::CENTRO_ID, $value));
		}

		$criteria->add($criterion);
	}

	public function addUsuarioPerfilListColumnCriteria(Criteria $criteria, $field, $values)
	{
		if (!is_array($values))
		{
			$values = array($values);
		}

		if (!count($values))
		{
			return;
		}

		$criteria->addJoin(UsuarioPerfilPeer::USUARIO_ID, UsuarioPeer::ID);

		$value = array_pop($values);
		$criterion = $criteria->getNewCriterion(UsuarioPerfilPeer::PERFIL_ID, $value);

		foreach ($values as $value)
		{
			$criterion->addOr($criteria->getNewCriterion(UsuarioPerfilPeer::PERFIL_ID, $value));
		}

		$criteria->add($criterion);
	}

	public function getModelName()
	{
		return 'Usuario';
	}

	public function getFields()
	{
		return array(
				'id'                  => 'Number',
				'rol_id'              => 'ForeignKey',
				'usuario'             => 'Text',
				'clave'               => 'Text',
				'nombre'              => 'Text',
				'apepat'              => 'Text',
				'apemat'              => 'Text',
				'nombre_completo'     => 'Text',
				'email'               => 'Text',
				'operador_id'         => 'ForeignKey',
				'activo'              => 'Boolean',
				'created_at'          => 'Date',
				'updated_at'          => 'Date',
				'usuario_centro_list' => 'ManyKey',
				'usuario_perfil_list' => 'ManyKey',
		);
	}
}
