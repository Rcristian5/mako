<?php

/**
 * RutaAprendizaje filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseRutaAprendizajeFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'curso_id' => new sfWidgetFormPropelChoice(array('model' => 'Curso', 'add_empty' => true)),
				'padre_id' => new sfWidgetFormPropelChoice(array('model' => 'Curso', 'add_empty' => true)),
		));

		$this->setValidators(array(
				'curso_id' => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Curso', 'column' => 'id')),
				'padre_id' => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Curso', 'column' => 'id')),
		));

		$this->widgetSchema->setNameFormat('ruta_aprendizaje_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'RutaAprendizaje';
	}

	public function getFields()
	{
		return array(
				'curso_id' => 'ForeignKey',
				'padre_id' => 'ForeignKey',
				'id'       => 'Number',
		);
	}
}
