<?php

/**
 * RecursoCompuesta filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseRecursoCompuestaFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'compuesta_id_1' => new sfWidgetFormPropelChoice(array('model' => 'RecursoInfraestructura', 'add_empty' => true)),
				'compuesta_id_2' => new sfWidgetFormPropelChoice(array('model' => 'RecursoInfraestructura', 'add_empty' => true)),
		));

		$this->setValidators(array(
				'compuesta_id_1' => new sfValidatorPropelChoice(array('required' => false, 'model' => 'RecursoInfraestructura', 'column' => 'id')),
				'compuesta_id_2' => new sfValidatorPropelChoice(array('required' => false, 'model' => 'RecursoInfraestructura', 'column' => 'id')),
		));

		$this->widgetSchema->setNameFormat('recurso_compuesta_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'RecursoCompuesta';
	}

	public function getFields()
	{
		return array(
				'compuesta_id_1' => 'ForeignKey',
				'compuesta_id_2' => 'ForeignKey',
				'id'             => 'Number',
		);
	}
}
