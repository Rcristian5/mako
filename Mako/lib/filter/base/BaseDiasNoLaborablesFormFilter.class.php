<?php

/**
 * DiasNoLaborables filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseDiasNoLaborablesFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'dia'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
				'descripcion' => new sfWidgetFormFilterInput(),
				'operador_id' => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'created_at'  => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'updated_at'  => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
		));

		$this->setValidators(array(
				'dia'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'descripcion' => new sfValidatorPass(array('required' => false)),
				'operador_id' => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Usuario', 'column' => 'id')),
				'created_at'  => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'updated_at'  => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
		));

		$this->widgetSchema->setNameFormat('dias_no_laborables_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'DiasNoLaborables';
	}

	public function getFields()
	{
		return array(
				'id'          => 'Number',
				'dia'         => 'Date',
				'descripcion' => 'Text',
				'operador_id' => 'ForeignKey',
				'created_at'  => 'Date',
				'updated_at'  => 'Date',
		);
	}
}
