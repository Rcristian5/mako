<?php

/**
 * CategoriaReporte filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCategoriaReporteFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'nombre'                          => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'activo'                          => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'created_at'                      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'updated_at'                      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'categoria_reporte_producto_list' => new sfWidgetFormPropelChoice(array('model' => 'Producto', 'add_empty' => true)),
		));

		$this->setValidators(array(
				'nombre'                          => new sfValidatorPass(array('required' => false)),
				'activo'                          => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'created_at'                      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'updated_at'                      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'categoria_reporte_producto_list' => new sfValidatorPropelChoice(array('model' => 'Producto', 'required' => false)),
		));

		$this->widgetSchema->setNameFormat('categoria_reporte_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function addCategoriaReporteProductoListColumnCriteria(Criteria $criteria, $field, $values)
	{
		if (!is_array($values))
		{
			$values = array($values);
		}

		if (!count($values))
		{
			return;
		}

		$criteria->addJoin(CategoriaReporteProductoPeer::CATEGORIA_ID, CategoriaReportePeer::ID);

		$value = array_pop($values);
		$criterion = $criteria->getNewCriterion(CategoriaReporteProductoPeer::PRODUCTO_ID, $value);

		foreach ($values as $value)
		{
			$criterion->addOr($criteria->getNewCriterion(CategoriaReporteProductoPeer::PRODUCTO_ID, $value));
		}

		$criteria->add($criterion);
	}

	public function getModelName()
	{
		return 'CategoriaReporte';
	}

	public function getFields()
	{
		return array(
				'id'                              => 'Number',
				'nombre'                          => 'Text',
				'activo'                          => 'Boolean',
				'created_at'                      => 'Date',
				'updated_at'                      => 'Date',
				'categoria_reporte_producto_list' => 'ManyKey',
		);
	}
}
