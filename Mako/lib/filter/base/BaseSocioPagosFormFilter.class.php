<?php

/**
 * SocioPagos filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseSocioPagosFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'centro_id'        => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'socio_id'         => new sfWidgetFormPropelChoice(array('model' => 'Socio', 'add_empty' => true)),
				'tipo_cobro_id'    => new sfWidgetFormPropelChoice(array('model' => 'TipoCobro', 'add_empty' => true)),
				'producto_id'      => new sfWidgetFormPropelChoice(array('model' => 'Producto', 'add_empty' => true)),
				'sub_producto_de'  => new sfWidgetFormPropelChoice(array('model' => 'Producto', 'add_empty' => true)),
				'numero_pago'      => new sfWidgetFormFilterInput(),
				'detalle_orden_id' => new sfWidgetFormPropelChoice(array('model' => 'DetalleOrden', 'add_empty' => true)),
				'fecha_a_pagar'    => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'fecha_pagado'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'pagado'           => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'acuerdo_pago'     => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'observaciones'    => new sfWidgetFormFilterInput(),
				'cancelado'        => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'orden_base_id'    => new sfWidgetFormPropelChoice(array('model' => 'Orden', 'add_empty' => true)),
				'created_at'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'updated_at'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'precio_lista'     => new sfWidgetFormFilterInput(),
				'tiempo'           => new sfWidgetFormFilterInput(),
				'cantidad'         => new sfWidgetFormFilterInput(),
				'descuento'        => new sfWidgetFormFilterInput(),
				'subtotal'         => new sfWidgetFormFilterInput(),
				'promocion_id'     => new sfWidgetFormPropelChoice(array('model' => 'Promocion', 'add_empty' => true)),
		));

		$this->setValidators(array(
				'centro_id'        => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Centro', 'column' => 'id')),
				'socio_id'         => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Socio', 'column' => 'id')),
				'tipo_cobro_id'    => new sfValidatorPropelChoice(array('required' => false, 'model' => 'TipoCobro', 'column' => 'id')),
				'producto_id'      => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Producto', 'column' => 'id')),
				'sub_producto_de'  => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Producto', 'column' => 'id')),
				'numero_pago'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'detalle_orden_id' => new sfValidatorPropelChoice(array('required' => false, 'model' => 'DetalleOrden', 'column' => 'id')),
				'fecha_a_pagar'    => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'fecha_pagado'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'pagado'           => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'acuerdo_pago'     => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'observaciones'    => new sfValidatorPass(array('required' => false)),
				'cancelado'        => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'orden_base_id'    => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Orden', 'column' => 'id')),
				'created_at'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'updated_at'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'precio_lista'     => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
				'tiempo'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'cantidad'         => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
				'descuento'        => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
				'subtotal'         => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
				'promocion_id'     => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Promocion', 'column' => 'id')),
		));

		$this->widgetSchema->setNameFormat('socio_pagos_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'SocioPagos';
	}

	public function getFields()
	{
		return array(
				'id'               => 'Number',
				'centro_id'        => 'ForeignKey',
				'socio_id'         => 'ForeignKey',
				'tipo_cobro_id'    => 'ForeignKey',
				'producto_id'      => 'ForeignKey',
				'sub_producto_de'  => 'ForeignKey',
				'numero_pago'      => 'Number',
				'detalle_orden_id' => 'ForeignKey',
				'fecha_a_pagar'    => 'Date',
				'fecha_pagado'     => 'Date',
				'pagado'           => 'Boolean',
				'acuerdo_pago'     => 'Boolean',
				'observaciones'    => 'Text',
				'cancelado'        => 'Boolean',
				'orden_base_id'    => 'ForeignKey',
				'created_at'       => 'Date',
				'updated_at'       => 'Date',
				'precio_lista'     => 'Number',
				'tiempo'           => 'Number',
				'cantidad'         => 'Number',
				'descuento'        => 'Number',
				'subtotal'         => 'Number',
				'promocion_id'     => 'ForeignKey',
		);
	}
}
