<?php

/**
 * ReporteIndicadoresDia filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseReporteIndicadoresDiaFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'fecha'                 => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
				'semana'                => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'anio'                  => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'registros_nuevos'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'socios_registrados'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'socios_activos'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'socios_activos_nuevos' => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'cobranza_educacion'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'cobranza_otros'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'cobranza_internet'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'cobranza_total'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'created_at'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'updated_at'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
		));

		$this->setValidators(array(
				'fecha'                 => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'semana'                => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'anio'                  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'registros_nuevos'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'socios_registrados'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'socios_activos'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'socios_activos_nuevos' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'cobranza_educacion'    => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
				'cobranza_otros'        => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
				'cobranza_internet'     => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
				'cobranza_total'        => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
				'created_at'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'updated_at'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
		));

		$this->widgetSchema->setNameFormat('reporte_indicadores_dia_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'ReporteIndicadoresDia';
	}

	public function getFields()
	{
		return array(
				'id'                    => 'Number',
				'centro_id'             => 'ForeignKey',
				'fecha'                 => 'Date',
				'semana'                => 'Number',
				'anio'                  => 'Number',
				'registros_nuevos'      => 'Number',
				'socios_registrados'    => 'Number',
				'socios_activos'        => 'Number',
				'socios_activos_nuevos' => 'Number',
				'cobranza_educacion'    => 'Number',
				'cobranza_otros'        => 'Number',
				'cobranza_internet'     => 'Number',
				'cobranza_total'        => 'Number',
				'created_at'            => 'Date',
				'updated_at'            => 'Date',
		);
	}
}
