<?php

/**
 * Relfixes filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseRelfixesFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'centro_id'     => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'usuario'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'fecha_alta'    => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'uidnumber'     => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'uidnumber_new' => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
		));

		$this->setValidators(array(
				'centro_id'     => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Centro', 'column' => 'id')),
				'usuario'       => new sfValidatorPass(array('required' => false)),
				'fecha_alta'    => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'uidnumber'     => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Centro', 'column' => 'id')),
				'uidnumber_new' => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Centro', 'column' => 'id')),
		));

		$this->widgetSchema->setNameFormat('relfixes_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Relfixes';
	}

	public function getFields()
	{
		return array(
				'socio_id'      => 'Number',
				'centro_id'     => 'ForeignKey',
				'usuario'       => 'Text',
				'fecha_alta'    => 'Date',
				'uidnumber'     => 'ForeignKey',
				'uidnumber_new' => 'ForeignKey',
		);
	}
}
