<?php

/**
 * EstudioCualitativo filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseEstudioCualitativoFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'curso_id'     => new sfWidgetFormPropelChoice(array('model' => 'Curso', 'add_empty' => true)),
				'grupo_id'     => new sfWidgetFormPropelChoice(array('model' => 'Grupo', 'add_empty' => true)),
				'alumno_id'    => new sfWidgetFormPropelChoice(array('model' => 'Alumnos', 'add_empty' => true)),
				'pregunta_id'  => new sfWidgetFormPropelChoice(array('model' => 'Cuestionario', 'add_empty' => true)),
				'respuesta_id' => new sfWidgetFormPropelChoice(array('model' => 'RespuestasCuestionario', 'add_empty' => true)),
				'operador_id'  => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'created_at'   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'updated_at'   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
		));

		$this->setValidators(array(
				'curso_id'     => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Curso', 'column' => 'id')),
				'grupo_id'     => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Grupo', 'column' => 'id')),
				'alumno_id'    => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Alumnos', 'column' => 'socio_id')),
				'pregunta_id'  => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Cuestionario', 'column' => 'id')),
				'respuesta_id' => new sfValidatorPropelChoice(array('required' => false, 'model' => 'RespuestasCuestionario', 'column' => 'id')),
				'operador_id'  => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Usuario', 'column' => 'id')),
				'created_at'   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'updated_at'   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
		));

		$this->widgetSchema->setNameFormat('estudio_cualitativo_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'EstudioCualitativo';
	}

	public function getFields()
	{
		return array(
				'id'           => 'Number',
				'curso_id'     => 'ForeignKey',
				'grupo_id'     => 'ForeignKey',
				'alumno_id'    => 'ForeignKey',
				'pregunta_id'  => 'ForeignKey',
				'respuesta_id' => 'ForeignKey',
				'operador_id'  => 'ForeignKey',
				'created_at'   => 'Date',
				'updated_at'   => 'Date',
		);
	}
}
