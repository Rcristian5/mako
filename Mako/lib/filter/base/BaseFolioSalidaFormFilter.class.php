<?php

/**
 * FolioSalida filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseFolioSalidaFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'folio'     => new sfWidgetFormFilterInput(),
		));

		$this->setValidators(array(
				'folio'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
		));

		$this->widgetSchema->setNameFormat('folio_salida_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'FolioSalida';
	}

	public function getFields()
	{
		return array(
				'centro_id' => 'ForeignKey',
				'folio'     => 'Number',
		);
	}
}
