<?php

/**
 * Inventario filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseInventarioFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'operacion_id' => new sfWidgetFormPropelChoice(array('model' => 'InventarioOperacion', 'add_empty' => true)),
				'producto_id'  => new sfWidgetFormPropelChoice(array('model' => 'Producto', 'add_empty' => true)),
				'cantidad'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'centro_id'    => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'motivo'       => new sfWidgetFormFilterInput(),
				'created_at'   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
		));

		$this->setValidators(array(
				'operacion_id' => new sfValidatorPropelChoice(array('required' => false, 'model' => 'InventarioOperacion', 'column' => 'id')),
				'producto_id'  => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Producto', 'column' => 'id')),
				'cantidad'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'centro_id'    => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Centro', 'column' => 'id')),
				'motivo'       => new sfValidatorPass(array('required' => false)),
				'created_at'   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
		));

		$this->widgetSchema->setNameFormat('inventario_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Inventario';
	}

	public function getFields()
	{
		return array(
				'id'           => 'Number',
				'operacion_id' => 'ForeignKey',
				'producto_id'  => 'ForeignKey',
				'cantidad'     => 'Number',
				'centro_id'    => 'ForeignKey',
				'motivo'       => 'Text',
				'created_at'   => 'Date',
		);
	}
}
