<?php

/**
 * Expediente filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseExpedienteFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'alumno_id'             => new sfWidgetFormPropelChoice(array('model' => 'Alumnos', 'add_empty' => true, 'key_method' => 'getSocioId')),
				'acta_nacimiento'       => new sfWidgetFormFilterInput(),
				'id_fotografia'         => new sfWidgetFormFilterInput(),
				'comprobante_domicilio' => new sfWidgetFormFilterInput(),
				'curp'                  => new sfWidgetFormFilterInput(),
				'activo'                => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'created_at'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'updated_at'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
		));

		$this->setValidators(array(
				'alumno_id'             => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Alumnos', 'column' => 'socio_id')),
				'acta_nacimiento'       => new sfValidatorPass(array('required' => false)),
				'id_fotografia'         => new sfValidatorPass(array('required' => false)),
				'comprobante_domicilio' => new sfValidatorPass(array('required' => false)),
				'curp'                  => new sfValidatorPass(array('required' => false)),
				'activo'                => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'created_at'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'updated_at'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
		));

		$this->widgetSchema->setNameFormat('expediente_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Expediente';
	}

	public function getFields()
	{
		return array(
				'id'                    => 'Number',
				'alumno_id'             => 'ForeignKey',
				'acta_nacimiento'       => 'Text',
				'id_fotografia'         => 'Text',
				'comprobante_domicilio' => 'Text',
				'curp'                  => 'Text',
				'activo'                => 'Boolean',
				'created_at'            => 'Date',
				'updated_at'            => 'Date',
		);
	}
}
