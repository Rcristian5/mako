<?php

/**
 * OcupacionHoras filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseOcupacionHorasFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'centro_id'      => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'fecha_hora'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
				'fecha'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
				'hora'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
				'consumo'        => new sfWidgetFormFilterInput(),
				'sesiones'       => new sfWidgetFormFilterInput(),
				'capacidad_hora' => new sfWidgetFormFilterInput(),
		));

		$this->setValidators(array(
				'centro_id'      => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Centro', 'column' => 'id')),
				'fecha_hora'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'fecha'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'hora'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'consumo'        => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
				'sesiones'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'capacidad_hora' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
		));

		$this->widgetSchema->setNameFormat('ocupacion_horas_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'OcupacionHoras';
	}

	public function getFields()
	{
		return array(
				'id'             => 'Number',
				'centro_id'      => 'ForeignKey',
				'fecha_hora'     => 'Date',
				'fecha'          => 'Date',
				'hora'           => 'Date',
				'consumo'        => 'Number',
				'sesiones'       => 'Number',
				'capacidad_hora' => 'Number',
		);
	}
}
