<?php

/**
 * RestriccionesSocio filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseRestriccionesSocioFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'curso_id'                 => new sfWidgetFormPropelChoice(array('model' => 'Curso', 'add_empty' => true)),
				'edad_minima'              => new sfWidgetFormFilterInput(),
				'edad_maxima'              => new sfWidgetFormFilterInput(),
				'sexo'                     => new sfWidgetFormFilterInput(),
				'ocupacion_id'             => new sfWidgetFormPropelChoice(array('model' => 'Ocupacion', 'add_empty' => true)),
				'estudia'                  => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'nivel_estudio_id'         => new sfWidgetFormPropelChoice(array('model' => 'NivelEstudio', 'add_empty' => true)),
				'habilidad_informatica_id' => new sfWidgetFormPropelChoice(array('model' => 'HabilidadInformatica', 'add_empty' => true)),
				'dominio_ingles_id'        => new sfWidgetFormPropelChoice(array('model' => 'DominioIngles', 'add_empty' => true)),
				'activo'                   => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'created_at'               => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'updated_at'               => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
		));

		$this->setValidators(array(
				'curso_id'                 => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Curso', 'column' => 'id')),
				'edad_minima'              => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'edad_maxima'              => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'sexo'                     => new sfValidatorPass(array('required' => false)),
				'ocupacion_id'             => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Ocupacion', 'column' => 'id')),
				'estudia'                  => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'nivel_estudio_id'         => new sfValidatorPropelChoice(array('required' => false, 'model' => 'NivelEstudio', 'column' => 'id')),
				'habilidad_informatica_id' => new sfValidatorPropelChoice(array('required' => false, 'model' => 'HabilidadInformatica', 'column' => 'id')),
				'dominio_ingles_id'        => new sfValidatorPropelChoice(array('required' => false, 'model' => 'DominioIngles', 'column' => 'id')),
				'activo'                   => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'created_at'               => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'updated_at'               => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
		));

		$this->widgetSchema->setNameFormat('restricciones_socio_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'RestriccionesSocio';
	}

	public function getFields()
	{
		return array(
				'id'                       => 'Number',
				'curso_id'                 => 'ForeignKey',
				'edad_minima'              => 'Number',
				'edad_maxima'              => 'Number',
				'sexo'                     => 'Text',
				'ocupacion_id'             => 'ForeignKey',
				'estudia'                  => 'Boolean',
				'nivel_estudio_id'         => 'ForeignKey',
				'habilidad_informatica_id' => 'ForeignKey',
				'dominio_ingles_id'        => 'ForeignKey',
				'activo'                   => 'Boolean',
				'created_at'               => 'Date',
				'updated_at'               => 'Date',
		);
	}
}
