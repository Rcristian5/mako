<?php

/**
 * GrupoFacilitador filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseGrupoFacilitadorFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'grupo_id'   => new sfWidgetFormPropelChoice(array('model' => 'Grupo', 'add_empty' => true)),
				'usuario_id' => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
		));

		$this->setValidators(array(
				'grupo_id'   => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Grupo', 'column' => 'id')),
				'usuario_id' => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Usuario', 'column' => 'id')),
		));

		$this->widgetSchema->setNameFormat('grupo_facilitador_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'GrupoFacilitador';
	}

	public function getFields()
	{
		return array(
				'id'         => 'Number',
				'grupo_id'   => 'ForeignKey',
				'usuario_id' => 'ForeignKey',
		);
	}
}
