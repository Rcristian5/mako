<?php

/**
 * RespuestasCuestionario filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseRespuestasCuestionarioFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'categoria_curso_id' => new sfWidgetFormPropelChoice(array('model' => 'CategoriaCurso', 'add_empty' => true)),
				'curso_id'           => new sfWidgetFormPropelChoice(array('model' => 'Curso', 'add_empty' => true)),
				'respuesta'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'valor'              => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'activo'             => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'operador_id'        => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'created_at'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'updated_at'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
		));

		$this->setValidators(array(
				'categoria_curso_id' => new sfValidatorPropelChoice(array('required' => false, 'model' => 'CategoriaCurso', 'column' => 'id')),
				'curso_id'           => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Curso', 'column' => 'id')),
				'respuesta'          => new sfValidatorPass(array('required' => false)),
				'valor'              => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
				'activo'             => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'operador_id'        => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Usuario', 'column' => 'id')),
				'created_at'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'updated_at'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
		));

		$this->widgetSchema->setNameFormat('respuestas_cuestionario_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'RespuestasCuestionario';
	}

	public function getFields()
	{
		return array(
				'id'                 => 'Number',
				'categoria_curso_id' => 'ForeignKey',
				'curso_id'           => 'ForeignKey',
				'respuesta'          => 'Text',
				'valor'              => 'Number',
				'activo'             => 'Boolean',
				'operador_id'        => 'ForeignKey',
				'created_at'         => 'Date',
				'updated_at'         => 'Date',
		);
	}
}
