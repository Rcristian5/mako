<?php

/**
 * PromocionRegla filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePromocionReglaFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'promocion_id'             => new sfWidgetFormPropelChoice(array('model' => 'Promocion', 'add_empty' => true)),
				'sexo'                     => new sfWidgetFormFilterInput(),
				'edad_de'                  => new sfWidgetFormFilterInput(),
				'edad_a'                   => new sfWidgetFormFilterInput(),
				'nivel_estudio'            => new sfWidgetFormPropelChoice(array('model' => 'NivelEstudio', 'add_empty' => true)),
				'recomendados_de'          => new sfWidgetFormFilterInput(),
				'recomendados_a'           => new sfWidgetFormFilterInput(),
				'centro_alta_id'           => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'centro_venta_id'          => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'municipio'                => new sfWidgetFormFilterInput(),
				'colonia'                  => new sfWidgetFormFilterInput(),
				'cp'                       => new sfWidgetFormFilterInput(),
				'discapacidad_id'          => new sfWidgetFormPropelChoice(array('model' => 'Discapacidad', 'add_empty' => true)),
				'habilidad_informatica_id' => new sfWidgetFormPropelChoice(array('model' => 'HabilidadInformatica', 'add_empty' => true)),
				'dominio_ingles_id'        => new sfWidgetFormPropelChoice(array('model' => 'DominioIngles', 'add_empty' => true)),
				'profesion_id'             => new sfWidgetFormPropelChoice(array('model' => 'Profesion', 'add_empty' => true)),
				'ocupacion_id'             => new sfWidgetFormPropelChoice(array('model' => 'Ocupacion', 'add_empty' => true)),
				'posicion_id'              => new sfWidgetFormPropelChoice(array('model' => 'Posicion', 'add_empty' => true)),
				'estudia'                  => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'trabaja'                  => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'created_at'               => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'updated_at'               => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'beca_id'                  => new sfWidgetFormPropelChoice(array('model' => 'Beca', 'add_empty' => true)),
		));

		$this->setValidators(array(
				'promocion_id'             => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Promocion', 'column' => 'id')),
				'sexo'                     => new sfValidatorPass(array('required' => false)),
				'edad_de'                  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'edad_a'                   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'nivel_estudio'            => new sfValidatorPropelChoice(array('required' => false, 'model' => 'NivelEstudio', 'column' => 'id')),
				'recomendados_de'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'recomendados_a'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'centro_alta_id'           => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Centro', 'column' => 'id')),
				'centro_venta_id'          => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Centro', 'column' => 'id')),
				'municipio'                => new sfValidatorPass(array('required' => false)),
				'colonia'                  => new sfValidatorPass(array('required' => false)),
				'cp'                       => new sfValidatorPass(array('required' => false)),
				'discapacidad_id'          => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Discapacidad', 'column' => 'id')),
				'habilidad_informatica_id' => new sfValidatorPropelChoice(array('required' => false, 'model' => 'HabilidadInformatica', 'column' => 'id')),
				'dominio_ingles_id'        => new sfValidatorPropelChoice(array('required' => false, 'model' => 'DominioIngles', 'column' => 'id')),
				'profesion_id'             => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Profesion', 'column' => 'id')),
				'ocupacion_id'             => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Ocupacion', 'column' => 'id')),
				'posicion_id'              => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Posicion', 'column' => 'id')),
				'estudia'                  => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'trabaja'                  => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'created_at'               => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'updated_at'               => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'beca_id'                  => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Beca', 'column' => 'id')),
		));

		$this->widgetSchema->setNameFormat('promocion_regla_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'PromocionRegla';
	}

	public function getFields()
	{
		return array(
				'id'                       => 'Number',
				'promocion_id'             => 'ForeignKey',
				'sexo'                     => 'Text',
				'edad_de'                  => 'Number',
				'edad_a'                   => 'Number',
				'nivel_estudio'            => 'ForeignKey',
				'recomendados_de'          => 'Number',
				'recomendados_a'           => 'Number',
				'centro_alta_id'           => 'ForeignKey',
				'centro_venta_id'          => 'ForeignKey',
				'municipio'                => 'Text',
				'colonia'                  => 'Text',
				'cp'                       => 'Text',
				'discapacidad_id'          => 'ForeignKey',
				'habilidad_informatica_id' => 'ForeignKey',
				'dominio_ingles_id'        => 'ForeignKey',
				'profesion_id'             => 'ForeignKey',
				'ocupacion_id'             => 'ForeignKey',
				'posicion_id'              => 'ForeignKey',
				'estudia'                  => 'Boolean',
				'trabaja'                  => 'Boolean',
				'created_at'               => 'Date',
				'updated_at'               => 'Date',
				'beca_id'                  => 'ForeignKey',
		);
	}
}
