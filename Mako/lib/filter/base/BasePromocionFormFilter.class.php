<?php

/**
 * Promocion filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePromocionFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'nombre'                   => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'descripcion'              => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'vigente_de'               => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'vigente_a'                => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'activo'                   => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'descuento'                => new sfWidgetFormFilterInput(),
				'created_at'               => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'updated_at'               => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'promocion_categoria_list' => new sfWidgetFormPropelChoice(array('model' => 'CategoriaProducto', 'add_empty' => true)),
				'promocion_producto_list'  => new sfWidgetFormPropelChoice(array('model' => 'Producto', 'add_empty' => true)),
		));

		$this->setValidators(array(
				'nombre'                   => new sfValidatorPass(array('required' => false)),
				'descripcion'              => new sfValidatorPass(array('required' => false)),
				'vigente_de'               => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'vigente_a'                => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'activo'                   => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'descuento'                => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
				'created_at'               => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'updated_at'               => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'promocion_categoria_list' => new sfValidatorPropelChoice(array('model' => 'CategoriaProducto', 'required' => false)),
				'promocion_producto_list'  => new sfValidatorPropelChoice(array('model' => 'Producto', 'required' => false)),
		));

		$this->widgetSchema->setNameFormat('promocion_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function addPromocionCategoriaListColumnCriteria(Criteria $criteria, $field, $values)
	{
		if (!is_array($values))
		{
			$values = array($values);
		}

		if (!count($values))
		{
			return;
		}

		$criteria->addJoin(PromocionCategoriaPeer::PROMOCION_ID, PromocionPeer::ID);

		$value = array_pop($values);
		$criterion = $criteria->getNewCriterion(PromocionCategoriaPeer::CATEGORIA_ID, $value);

		foreach ($values as $value)
		{
			$criterion->addOr($criteria->getNewCriterion(PromocionCategoriaPeer::CATEGORIA_ID, $value));
		}

		$criteria->add($criterion);
	}

	public function addPromocionProductoListColumnCriteria(Criteria $criteria, $field, $values)
	{
		if (!is_array($values))
		{
			$values = array($values);
		}

		if (!count($values))
		{
			return;
		}

		$criteria->addJoin(PromocionProductoPeer::PROMOCION_ID, PromocionPeer::ID);

		$value = array_pop($values);
		$criterion = $criteria->getNewCriterion(PromocionProductoPeer::PRODUCTO_ID, $value);

		foreach ($values as $value)
		{
			$criterion->addOr($criteria->getNewCriterion(PromocionProductoPeer::PRODUCTO_ID, $value));
		}

		$criteria->add($criterion);
	}

	public function getModelName()
	{
		return 'Promocion';
	}

	public function getFields()
	{
		return array(
				'id'                       => 'Number',
				'nombre'                   => 'Text',
				'descripcion'              => 'Text',
				'vigente_de'               => 'Date',
				'vigente_a'                => 'Date',
				'activo'                   => 'Boolean',
				'descuento'                => 'Number',
				'created_at'               => 'Date',
				'updated_at'               => 'Date',
				'promocion_categoria_list' => 'ManyKey',
				'promocion_producto_list'  => 'ManyKey',
		);
	}
}
