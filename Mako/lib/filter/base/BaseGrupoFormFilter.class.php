<?php

/**
 * Grupo filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseGrupoFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'centro_id'         => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'curso_id'          => new sfWidgetFormPropelChoice(array('model' => 'Curso', 'add_empty' => true)),
				'aula_id'           => new sfWidgetFormPropelChoice(array('model' => 'Aula', 'add_empty' => true)),
				'horario_id'        => new sfWidgetFormPropelChoice(array('model' => 'Horario', 'add_empty' => true)),
				'facilitador_id'    => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'clave'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'cupo_total'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'cupo_actual'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'cupo_alcanzado'    => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'registrado_moodle' => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'registrado_rs'     => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'duracion'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'duracion_perdida'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'operador_id'       => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'activo'            => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'created_at'        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'updated_at'        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
		));

		$this->setValidators(array(
				'centro_id'         => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Centro', 'column' => 'id')),
				'curso_id'          => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Curso', 'column' => 'id')),
				'aula_id'           => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Aula', 'column' => 'id')),
				'horario_id'        => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Horario', 'column' => 'id')),
				'facilitador_id'    => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Usuario', 'column' => 'id')),
				'clave'             => new sfValidatorPass(array('required' => false)),
				'cupo_total'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'cupo_actual'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'cupo_alcanzado'    => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'registrado_moodle' => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'registrado_rs'     => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'duracion'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'duracion_perdida'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'operador_id'       => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Usuario', 'column' => 'id')),
				'activo'            => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'created_at'        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'updated_at'        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
		));

		$this->widgetSchema->setNameFormat('grupo_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Grupo';
	}

	public function getFields()
	{
		return array(
				'id'                => 'Number',
				'centro_id'         => 'ForeignKey',
				'curso_id'          => 'ForeignKey',
				'aula_id'           => 'ForeignKey',
				'horario_id'        => 'ForeignKey',
				'facilitador_id'    => 'ForeignKey',
				'clave'             => 'Text',
				'cupo_total'        => 'Number',
				'cupo_actual'       => 'Number',
				'cupo_alcanzado'    => 'Boolean',
				'registrado_moodle' => 'Boolean',
				'registrado_rs'     => 'Boolean',
				'duracion'          => 'Number',
				'duracion_perdida'  => 'Number',
				'operador_id'       => 'ForeignKey',
				'activo'            => 'Boolean',
				'created_at'        => 'Date',
				'updated_at'        => 'Date',
		);
	}
}
