<?php

/**
 * DiasOperacion filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseDiasOperacionFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'centro_id' => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'fecha'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
				'opera'     => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
		));

		$this->setValidators(array(
				'centro_id' => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Centro', 'column' => 'id')),
				'fecha'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'opera'     => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
		));

		$this->widgetSchema->setNameFormat('dias_operacion_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'DiasOperacion';
	}

	public function getFields()
	{
		return array(
				'id'        => 'Number',
				'centro_id' => 'ForeignKey',
				'fecha'     => 'Date',
				'opera'     => 'Boolean',
		);
	}
}
