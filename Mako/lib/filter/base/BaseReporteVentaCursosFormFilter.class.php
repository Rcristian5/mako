<?php

/**
 * ReporteVentaCursos filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseReporteVentaCursosFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'categoria_id'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'curso_id'         => new sfWidgetFormFilterInput(),
				'socio_id'         => new sfWidgetFormFilterInput(),
				'monto'            => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'cantidad'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
				'siglas'           => new sfWidgetFormFilterInput(),
				'categoria_nombre' => new sfWidgetFormFilterInput(),
		));

		$this->setValidators(array(
				'categoria_id'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'curso_id'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'socio_id'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'monto'            => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
				'cantidad'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'siglas'           => new sfValidatorPass(array('required' => false)),
				'categoria_nombre' => new sfValidatorPass(array('required' => false)),
		));

		$this->widgetSchema->setNameFormat('reporte_venta_cursos_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'ReporteVentaCursos';
	}

	public function getFields()
	{
		return array(
				'centro_id'        => 'ForeignKey',
				'fecha'            => 'Date',
				'detalle_orden_id' => 'Number',
				'categoria_id'     => 'Number',
				'curso_id'         => 'Number',
				'socio_id'         => 'Number',
				'monto'            => 'Number',
				'cantidad'         => 'Number',
				'siglas'           => 'Text',
				'categoria_nombre' => 'Text',
		);
	}
}
