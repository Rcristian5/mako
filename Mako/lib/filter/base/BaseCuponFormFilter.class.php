<?php

/**
 * Cupon filter form base class.
 *
 * @package    mako
 * @subpackage filter
 * @author     Bixit SA de CV
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCuponFormFilter extends BaseFormFilterPropel
{
	public function setup()
	{
		$this->setWidgets(array(
				'promocion_cupon_id' => new sfWidgetFormPropelChoice(array('model' => 'PromocionCupon', 'add_empty' => true)),
				'centro_id'          => new sfWidgetFormPropelChoice(array('model' => 'Centro', 'add_empty' => true)),
				'codigo'             => new sfWidgetFormFilterInput(),
				'validador'          => new sfWidgetFormFilterInput(),
				'otorgado_a'         => new sfWidgetFormPropelChoice(array('model' => 'Socio', 'add_empty' => true)),
				'otorgado_por'       => new sfWidgetFormPropelChoice(array('model' => 'Usuario', 'add_empty' => true)),
				'fecha_otorgado'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
				'razon_otorga'       => new sfWidgetFormFilterInput(),
				'utilizado'          => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
				'fecha_utilizado'    => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
		));

		$this->setValidators(array(
				'promocion_cupon_id' => new sfValidatorPropelChoice(array('required' => false, 'model' => 'PromocionCupon', 'column' => 'id')),
				'centro_id'          => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Centro', 'column' => 'id')),
				'codigo'             => new sfValidatorPass(array('required' => false)),
				'validador'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
				'otorgado_a'         => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Socio', 'column' => 'id')),
				'otorgado_por'       => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Usuario', 'column' => 'id')),
				'fecha_otorgado'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
				'razon_otorga'       => new sfValidatorPass(array('required' => false)),
				'utilizado'          => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
				'fecha_utilizado'    => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
		));

		$this->widgetSchema->setNameFormat('cupon_filters[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

		parent::setup();
	}

	public function getModelName()
	{
		return 'Cupon';
	}

	public function getFields()
	{
		return array(
				'id'                 => 'Number',
				'promocion_cupon_id' => 'ForeignKey',
				'centro_id'          => 'ForeignKey',
				'codigo'             => 'Text',
				'validador'          => 'Number',
				'otorgado_a'         => 'ForeignKey',
				'otorgado_por'       => 'ForeignKey',
				'fecha_otorgado'     => 'Date',
				'razon_otorga'       => 'Text',
				'utilizado'          => 'Boolean',
				'fecha_utilizado'    => 'Date',
		);
	}
}
