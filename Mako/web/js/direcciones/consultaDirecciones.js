/**
 * Consulta AJAX para parsear el XML del componente de direcciones.
 * Autor: eorozco
 * Version: $Id: consultaDirecciones.js,v 1.1 2010/01/20 06:22:15 marcela Exp $
 */
	var campo = "";
    function revisa(wg,objeto,filtro,tipo_cont,id_estado,id_municipio,id_asentamiento) {
    	campo = wg;
		
		//campo.className='direccionBuscando';

	    var req = doRequest("POST", "../comunes/direcciones/comDirecciones.php", {
	         accion: 'dinamico',
			 objeto: objeto,
			 filtro: filtro,
			 tipo_cont: tipo_cont,
			 id_estado: id_estado,
			 id_municipio: id_municipio,
			 id_asentamiento: id_asentamiento
			 
	     }, function(status, statusText, responseText, responseXML){
	         if (status != 200) 
			 {
	             //error;
		     	//dbg("ERRRROR:"+estatus);
				campo.className='direccion';				
	         }
	         else {

				campo.className='direccion';
				parsea(responseXML);

			}
	         
	     });		
	}

	function parsea(xml){

		var raiz = xml.childNodes[0];
		var elems = raiz.childNodes;
		var nombre = raiz.tagName;
		
		if(nombre == 'municipios')
		{
			arMunicipios.length=0;
			arAsentamientos.length=0;			
			arVialidades.length=0;
	    	obMunXArMun.length=0;
    		obAsenXArAsen.length=0;
    		obVialXArVial.length=0;
			var a = 0;
			for(var i=0; i<elems.length; i++){
				if(elems[i].nodeType == 1){
					arMunicipios[a]=elems[i].getAttribute('value');
					obMunicipios[elems[i].getAttribute('id')] = elems[i].getAttribute('value');
					obMunXArMun[a]=elems[i].getAttribute('id');
					a++;
					//dbg('monecip '+elems[i].getAttribute('value'));
				}		
			}
		}
		else if(nombre == 'asentamientos')
		{
			arAsentamientos.length=0;
			arVialidades.length=0;			
	    	obAsenXArAsen.length=0;
    		obVialXArVial.length=0;
			var b = 0;
			for(var i=0; i<elems.length; i++)
			{
				if(elems[i].nodeType == 1)
				{
					arAsentamientos[b]=elems[i].getAttribute('value');
					obAsentamientos[elems[i].getAttribute('id')] = elems[i].getAttribute('value');
					obAsenXArAsen[b]=elems[i].getAttribute('id');
					obCP[elems[i].getAttribute('id')]=elems[i].getAttribute('cp');
					obTAsen[elems[i].getAttribute('id')]=elems[i].getAttribute('tipo');
					b++;
				}
			}
		}
		else if(nombre == 'vialidades')
		{
			arVialidades.length=0;
    		obVialXArVial.length=0;
    		obTVialidad= Object();
			var z = 0;
			for(var i=0; i<elems.length; i++)
			{
				if(elems[i].nodeType == 1)
				{
					arVialidades[z]=elems[i].getAttribute('value');
					obTVialidad[z]=elems[i].getAttribute('tipo');
					z++;
				}
			}
		}
	}
	
	function dbg(tx){
		document.getElementById('debug').innerHTML += tx+"<br>";
	}
	