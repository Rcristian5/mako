<?php
//ini_set('error_reporting', E_ALL);

//include_once('/var/www/vhosts/bixit.com.mx/subdomains/ceit/httpdocs/ceit/comunes/constantes.php');
include_once('/var/www/html/crmceit/comunes/constantes.php');


/**
 * @uses 'clase_muestra.php' Se incluye la clase de sustitucion de plantillas.
 */
include_once(PATH_PROYECTO.'/comunes/clase_muestra.php');




/**
 * @uses 'conectaDirecciones.php' regresa la conexión a la base de datos. es aqui donde se debe
 * configurar todo lo concerniente a los datos de la conexion a la bd.
 */
include_once(PATH_PROYECTO.'/comunes/direcciones/conectaDirecciones.php');


/**
 * Clase del componente Direccion.
 */
class Direccion extends captura {

	/**
	 * Arreglo que contiene los nombres de los campos que aparecerán en la forma de direcciones.
	 */
	private $conf = array(
			'contenedor' => 'dir',
			'objeto' => 'direccion',
			'wgPais' => 'pais',
			'wgIdEstado' => 'id_estado',
			'wgEstado' => 'estado',
			'wgCiudad' => 'ciudad',
			'wgIdMunicipio' => 'id_municipio',
			'wgMunicipio' => 'municipio',
			'wgIdDelegacion' => 'id_del',
			'wgDelegacion' => 'del',
			'wgIdAsentamiento' => 'id_asentamiento',
			'wgAsentamiento' => 'colonia',
			'wgTAsentamiento' => 'tipo_asentamiento',
			'wgVialidad' => 'calle',
			'wgTVialidad' => 'tipo_vialidad',
			'wgCP' => 'cp',
			'wgMz' => 'mz',
			'wgLt' => 'lt',
			'wgNumInt' => 'num_int',
			'wgNumExt' => 'num_ext',
			'wgReferencia' => 'referencia'
	);

	/**
	 * Arreglo que contiene los valores de los widgets
	 */
	private $vals = array(
			'valwgPais' => 'MEXICO',
			'valwgIdEstado' => 15,
			'valwgEstado' => 'ESTADO DE MEXICO',
			'valwgCiudad' => '',
			'valwgIdMunicipio' => 0,
			'valwgMunicipio' => '',
			'valwgIdDelegacion' => 0,
			'valwgDelegacion' => '',
			'valwgIdAsentamiento' => 0,
			'valwgAsentamiento' => '',
			'valwgTAsentamiento' => '',
			'valwgVialidad' => '',
			'valwgTVialidad' => '',
			'valwgCP' => '',
			'valwgMz' => '',
			'valwgLt' => '',
			'valwgNumInt' => '',
			'valwgNumExt' => '',
			'valwgReferencia' => ''
	);

	/**
	 * Arreglo de campos requeridos.
	 */
	private $req = array(
			'wgIdEstado' => true,
			'wgIdMunicipio' => true,
			'wgIdAsentamiento' => true,
			'wgAsentamiento' => true,
			'wgVialidad' => true,
			'wgNumInt' => false,
			'wgNumExt' => false
	);

	/**
	 * Para convertir de ids de delegaciones sin hacer mas cueris.
	 * se asume que no van a cambiar los ids de los municipios, si ese fuera el caso habria que actualizar aqui.
	 */
	private $c2asen=array(
			'09002' => 187,	//AZCAPOTZALCO	9
			'09003' => 449,	//COYOACAN	9
			'09004' => 458,	//CUAJIMALPA DE MORELOS	9
			'09005' => 602,	//GUSTAVO A. MADERO	9
			'09006' => 728,	//IZTACALCO	9
			'09007' => 729,	//IZTAPALAPA	9
			'09008' => 828,	//LA MAGDALENA CONTRERAS	9
			'09009' => 908,	//MILPA ALTA	9
			'09010' => 73,	//ALVARO OBREGON	9
			'09011' => 2006,	//TLAHUAC	9
			'09012' => 2019,	//TLALPAN	9
			'09013' => 2192,	//XOCHIMILCO	9
			'09014' => 209,	//BENITO JUAREZ	9
			'09015' => 464,	//CUAUHTEMOC	9
			'09016' => 907,	//MIGUEL HIDALGO	9
			'09017' => 2129	//VENUSTIANO CARRANZA	9
	);

	/**
	 * Constructor del objeto Direccion
	 * @param array $conf Es un array del tipo conf[wgNombre]="nombre_widget";
	 * @param array $vals Es un array del tipo vals[wgNombre]=valor
	 */
	function Direccion($conf=null,$vals=null,$req=null){
		$this->setConf($conf);
		$this->setVals($vals);
	}

	/**
	 * Fija los nombres de los campos pasados en el arreglo $conf
	 */
	function setConf($conf=null){
		if(count($conf)){
			foreach($conf as $wg => $nombre){
				$this->conf[$wg] = $nombre;
			}
		}
	}

	/**
	 * regresa el arreglo de nombres de los campos.
	 */
	function getConf(){
		return $this->conf;
	}

	/**
	 * Fija los valores de la configuración de
	 */
	function setVals($vals=null){
		//Invertimos llaves por nombres para saber que widget debe contener el valor
		$noms = array_flip($this->conf);
		if(count($vals)){
			foreach($vals as $wg => $valor){
				//echo "$wg => $valor => {$noms[$wg]} => val{$noms[$wg]}<br>";
				$this->vals["val".$noms[$wg]] = $valor;
			}
		}
	}

	/**
	 * Regresa el arreglo de valores de los campos.
	 */
	function getVals(){
		return $this->vals;
	}

	/**
	 * Muestra la pantalla de captura de direcciones.
	 */
	function gui(){
		global $connSepomex;

		foreach($this->conf as $wg => $nombre){
			$dat['__global__'][$wg] = $nombre;
		}

		//Invertimos llaves por nombres para saber que widget debe contener el valor
		$noms = array_flip($this->conf);
		foreach($this->vals as $wg => $valor){
			$dat['__global__'][$wg] = $valor;
		}

		return $this->muestra($dat,'','../comunes/direcciones/plantillas/direcciones.html');
	}

	/**
	 * Función para hacer búsquedas
	 */
	function buscar($c){
		global $connSepomex;
		//Elementos que se buscan por igualdad directa, no por regexp.
		$directo[id_asentamiento]=1;
		$directo[id_municipio]=1;
		if($_POST[dir][v_direcciones][id_estado]=='9'){
			$_REQUEST[dir][v_direcciones][municipio]="";
			$_REQUEST[dir][v_direcciones][ciudad]="";
			$sql="select * from sepo_asen ".$c->prepara_where($_REQUEST[dir],$directo) . " order by municipio";
		}else{
			$_REQUEST[dir][v_direcciones][id_municipio]="";
			$_REQUEST[dir][v_direcciones][vialidad]="";
			$sql="select * from v_asentamientos ".$c->prepara_where($_REQUEST[dir][v_direcciones],$directo)  . " order by vialidad";
		}
		$res=pg_query($connSepomex,$sql);
		while($d=pg_fetch_array($res)){
			$datTB['calles'][]=$d;
		}
		if($_POST[dir][v_direcciones][id_estado]=='9'){
			if(count($datTB['calles'])){
				return $c->muestra($datTB,'','tabla_busqueda.html');
			}else{
				return "No se encontró ninguna coincidencia";
			}
		}else{
			if(count($datTB['calles'])){
				return $c->muestra($datTB,'','tabla_busqueda_municipios.html');
			}else{
				return "No se encontró ninguna coincidencia";
			}
		}
	}

	/**
	 * Pantalla del buscador
	 */
	function guiBuscador(){
		foreach($this->conf as $wg => $nombre){
			$dat['__global__'][$wg] = $nombre;
		}

		//Invertimos llaves por nombres para saber que widget debe contener el valor
		$noms = array_flip($this->conf);
		foreach($this->vals as $wg => $valor){
			$dat['__global__'][$wg] = $valor;
		}

		return $this->muestra($dat,'','buscador.html');
	}

	/**
	 * Sirve para rellenar los widgets con datos de las direcciones
	 */
	function rellenar($objeto,$filtro=null,$tipoCont){
		global $connSepomex;

		switch($objeto){
			case 'municipios':
				if($_REQUEST['id_estado'])
				{
					$sql="select id_municipio,municipio from estados_municipios where id_estado=".intval($_REQUEST['id_estado'])  . " order by municipio";
				}
				else
				{
					return;
				}
				$res=pg_query($connSepomex,$sql);
				$wg="";
				if($tipoCont == 'listBox')
				{
					$wg = "<label for=\"id_del\" id=\"labelIdDel\">Municipio: </label><select name=\"f_delegacion_municipio\" id=\"id_del\" tabindex=\"1\" onchange=\"carga(this.value,'asentamientos'); cargaDatos(this,this.value,'d');\">\n<option value=\"\">--Seleccione</option>\n";
				}
				elseif($tipoCont == 'ajax')
				{
					$wg = "<municipios>\n";
				}

				while($d=pg_fetch_array($res))
				{
					//error_log($d['id_municipio']. " -- ".  $d['municipio']);
					if($tipoCont == 'listBox')
					{
						$wg .= "<option " . "value=\"". $d['id_municipio']. "\">" .$d['municipio']. "</option>\n";
					}
					elseif($tipoCont == 'dropDown')
					{
						$wg .= $d['municipio']. "|" .$d['id_municipio'].",";
					}
					elseif($tipoCont == 'ajax')
					{
						$wg .= "\t<municipio id=\"".$d['id_municipio']."\" value=\"".$d['municipio']."\"/>\n";
					}
				}
				if($tipoCont == 'listBox')
				{
					$wg .= "</select>";
				}
				elseif($tipoCont == 'ajax')
				{
					$wg .= "</municipios>";
				}
				//echo $wg;

				break;
			case 'asentamientos':
					
				if($_REQUEST['id_municipio'])
				{
					$sql="select asentamiento,id_asentamiento,tipo_asentamiento,cp from v_asentamientos where id_municipio=".$_REQUEST['id_municipio'] ." and id_estado = ".intval($_REQUEST['id_estado'])." and id_tipo_asentamiento not in (14,22,29,34) order by asentamiento";
				}
				else
				{
					return;
				}
				$res=pg_query($connSepomex,$sql);
				$wg="";
				if($tipoCont == 'listBox')
				{
					$wg = "<select name=\"f_asentamiento\" id=\"f_asentamiento\" tabindex=\"1\">\n<option value=\"\">--Seleccione</option>\n";
				}
				elseif($tipoCont == 'ajax')
				{
					$wg = "<asentamientos>\n";
				}
				while($d=pg_fetch_array($res))
				{
						
					if($tipoCont == 'listBox')
					{
						$wg .= "<option " . "value=\"". $d['id_asentamiento']. "\">" .$d['asentamiento']. "</option>\n";
						//error_log("lb ".$wg);
					}
					elseif($tipoCont == 'dropDown')
					{
						$d['asentamiento']=str_replace(',',' ',$d['asentamiento']);
						$d['asentamiento']=str_replace('"','',$d['asentamiento']);
						$wg .= "{$d['asentamiento']}|{$d['id_asentamiento']}|{$d['tipo_asentamiento']}|{$d['cp']},";
						//error_log("dd ".$wg);
					}
					elseif($tipoCont == 'ajax')
					{
						$d['asentamiento']=str_replace(',',' ',$d['asentamiento']);
						$d['asentamiento']=str_replace('"','',$d['asentamiento']);
						$d['tipo_asentamiento']= $d['tipo_asentamiento'] ? $d['tipo_asentamiento'] : false;
						$d['cp']= $d['cp'] ? $d['cp'] : false;
						$wg .= "\t<asentamiento id=\"{$d['id_asentamiento']}\" value=\"{$d['asentamiento']}\" cp=\"{$d['cp']}\" tipo =\"{$d['tipo_asentamiento']}\"/>\n";
						//error_log("ajax ".$wg);
					}
				}

				if($tipoCont == 'listBox')
				{
					$wg .= "</select>";
				}
				elseif($tipoCont == 'ajax')
				{
					$wg .= "</asentamientos>\n";
				}
				break;
			case 'vialidades':

				if($_REQUEST['id_asentamiento'])
				{
					$sql="select * from sepo_asen where id_asentamiento =".intval($_REQUEST['id_asentamiento'])  . " order by vialidad";
				}
				else
				{
					return;
				}
				$res=pg_query($connSepomex,$sql);
					
				$wg = "<vialidades>\n";
				while($d=pg_fetch_array($res))
				{
					$d['tipo_vialidad'] = $d['tipo_vialidad'] ? $d['tipo_vialidad'] : ' ';
					$wg .= "\t<vialidad value=\"{$d['vialidad']}\" tipo =\"{$d['tipo_vialidad']}\"/>\n";
				}
				$wg .= "</vialidades>\n";
				break;
		}
		/* XMLHttRequest por default envía y espera utf8, asi que convertimos utf-8 a utf8 */
		//return utf8_encode($wg);

		return $wg;
	}
}

/**
 * Se usa este if si viene la llamada desde el autocompletador Ajax.
 */
if($_REQUEST['accion'] == "dinamico"){
	$d = new Direccion();
	echo $d->rellenar($_REQUEST['objeto'],$_REQUEST['filtro'],$_REQUEST['tipo_cont']);
}
?>