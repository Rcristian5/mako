/**
 * Clase que crea las graficas de los indicadores de operacion
 */
var DatosGraficos = function(){
	var mig = this;
	var arParams = {};
	var url = null;
	var graficas = [];
	this.nivel = 0;
	var renderTo = '';
	
	var type = 'spline';
	var fecha_ini = '';
	var fecha_fin = '';
	var opcionesTitulo=''; //'<?php echo $opciones_titulo ?>'
	var opcionesSubTitulo=''; //'<?php echo $opciones_subtitulo ?>'	
	var opcionesYAxis = ''; //'<?php echo $opciones_yaxis ?>'

	this.chb = null; //Chb target para ponerlo deshabilitado cuando hay una carga ajax
	this.params = {};
	this.arSeries = {};
	this.dTablas = {};
	this.dTablas.nombre='Global';
	this.dTablas.id='global';
	this.dTablas.series = [];
	this.modulo = null;
	this.claseSerie = 'global';
	
	this.idPuntoSerie = null;
	/**
	 * Definicion del grafico
	 */
	this.grafica = function()
	{
		var chart = new Highcharts.Chart({
			chart: {
				renderTo: this.renderTo,
				shadow: true,
		        events: {
		            selection: function(event) {
		                if (event.xAxis) {
		                	event.preventDefault();
		                	console.log(Highcharts.dateFormat('%Y-%m-%d', event.xAxis[0].min), Highcharts.dateFormat('%Y-%m-%d', event.xAxis[0].max));
		                	var ffin = event.xAxis[0].max;
		                	if(mig.nivel == 0) ffin = ffin+30*24*3600*1000;
		                	if(mig.nivel == 1) ffin = ffin+15*24*3600*1000;
		                	if(mig.nivel == 2) ffin = ffin+5*24*3600*1000;
		                	console.log('Alterado:'+Highcharts.dateFormat('%Y-%m-%d', event.xAxis[0].min), Highcharts.dateFormat('%Y-%m-%d', ffin));
		                	$(document).trigger('zoom', [Highcharts.dateFormat('%Y-%m-%d', event.xAxis[0].min), Highcharts.dateFormat('%Y-%m-%d', ffin)]);
		                }
		            }
		        },
		        defaultSeriesType: 'bar',
				animation: false,
				zoomType:'x'
			},
			loading: {
				labelStyle: {
			    	top: '45%'
			    }
			},
			legend: {
				margin: 25
			},
			credits: {
				enabled: false
			},
			title: {
				text: this.opcionesTitulo
			},
			subtitle: {
				text: this.opcionesSubTitulo
			},
			xAxis: {
				type: 'datetime',
				maxZoom: 86400000,
				dateTimeLabelFormats: {
					month: '%b %Y',
					week: 'S%W - %b %Y',
					day: '%a %d %b %Y',
					hour: '%a %d %b %Y',
					second: '%a %d %b %Y',
					minute: '%a %d %b %Y',
					year: '%Y'
				},
				labels: {
		            rotation: -90,
		            align: 'right'
		         }				
			},
			yAxis: {
				title: {
					text: this.opcionesYAxis
				},
			    labels: {
			    	formatter: function() {
			        	return $.fn.addCommas(this.value);
					}
			    },
				min: 0
			},
			tooltip: {
				formatter: function() {
		            var total = 0;
					var s ='<b>';
					if(mig.nivel == 0) {   
						s += Highcharts.dateFormat('%b %Y', this.x);
		            }
		            else if(mig.nivel == 1) {
						s += Highcharts.dateFormat('S%W - %b %Y', this.x);
		            }
		            else if(mig.nivel == 2) {
						s += Highcharts.dateFormat('%a %d %b %Y', this.x);
		            }
		            s += '</b>';
	            	s += '<br/><span style="color:'+ this.point.series.color +';">'+ this.point.series.name +': </span>'+
	                Highcharts.numberFormat(this.point.y,2,'.',',') ;
		            return s;
				},
				textAlign: 'right'
			},
		    plotOptions: {
		         column: {
		            stacking: 'normal'
		          },
		          series: {
		              point: {
		                  events: {
		                      mouseOver: function(event) {
		                    	  //Para marcar en las tablas los datos que corresponden a los puntos en la grafica.
		                    	  var ntabla = typeof event.target.series.name != "undefined"? event.target.series.name : 'nada';
		                          ntabla = ntabla.replace(/\W/g,'-');
		                          ntabla.replace(/\//g,'-');
		                          $('#tablas-'+mig.nivel).find('td').removeClass('marcar');
		                          $('#tablas-'+mig.nivel).find('table').removeClass('marcarTabla');
		                          
		                          $('#'+ntabla+'-'+this.x+'-'+mig.nivel).addClass('marcar');
		                          $('#'+ntabla+'-'+this.x+'-'+mig.nivel).closest('table').addClass('marcarTabla');
		                      }
		                  }
		              },
		              events: {
		                  mouseOut: function(event) {                        
	                          $('#tablas-'+mig.nivel).find('td').removeClass('marcar');
	                          $('#tablas-'+mig.nivel).find('table').removeClass('marcarTabla');
		                  }
		              }
		          }		          
		    }
		});
		return chart;
	};

	/**
	 * Parser de los datos que vienen del servidor
	 */
	this.parser = function(datos,tipo,categoria,idSerie)
	{
		var m = 0;
		jQuery.each(datos, function(i, val) 
		{
			var chart = mig.graficas[0];
			var sp = chart.addSeries({
				id:i,
				name:i,
				data:val,
				stack: (typeof categoria == "undefined"? 'global': categoria)+mig.idGrupo(m,idSerie,mig.modulo),
				shadow:false,
				type:tipo == null ? mig.type : tipo,
				visible: mig.visibilidad(i),
				indiceGrupo: m,
				categoria: categoria,
				events: {
	                	click:  function(e){
	                		//console.log(this);
	                		$(document).trigger('sub.serie',[{
	                			categoria: categoria, 
	                			idSerie:idSerie, 
	                			nombre: i, 
	                			fecha: e.point.x, 
	                			valor: e.point.y, 
	                			idx:this.options.indiceGrupo,
	                			fechaInicio: mig.params.fecha_ini,
	                			fechaFin: mig.params.fecha_fin,
	                			modulo: mig.modulo
	                			}]);
	                	}
	            },
	            cursor: mig.cursor(mig.modulo,m)			
			});
			mig.dTablas.series[m] = {'nombre':i,valores:val, id:idSerie};
			m++;
			mig.arSeries[i]={nombre: i, serie: sp, categoria:categoria, id:idSerie};
		});
	};

	/**
	 * Inicializa el grafico
	 */
	this.init = function()
	{
		mig.graficas.push(mig.grafica());
	};

	/**
	 * Cursor de serie
	 */
	this.cursor = function(nombre,idx)
	{
		return (nombre =='CobranzaTotales' && idx==0) ? 'pointer' : null;
	};

	/**
	 * Obtiene los datos via ajax del servidor
	 */
	this.get = function(tipo,opcTablas)
	{
		var chart = mig.graficas[0];
		chart.showLoading();
		var pp = mig.params;
		var categoria = typeof pp.zona == "undefined" ? (typeof pp.centro_id == "undefined" ? 'global' : 'centro') : 'zona';
		var idSerie = typeof pp.zona == "undefined" ? (typeof pp.centro_id == "undefined" ? null : pp.centro_id) : pp.zona;

		if(mig.chb != null) mig.chb.attr('disabled','disabled'); //Si existe un chb lo deshabilitamos para impedir doble click mientras se cargan datos
		
		jQuery.get(mig.url, mig.params, function(datos, state, xhr) {
			//Parseamos datos
			mig.parser(datos,tipo,categoria,idSerie);
			//Ocultamos msg de cargando
			chart.hideLoading();
			//Habilitamos chb
			if(mig.chb != null) mig.chb.removeAttr('disabled');
			//Seteamos opciones de tablas
			var opt = typeof opcTablas == "undefined" ? {}: opcTablas;
			mig.dTablas.nombre = typeof opt.nombre == "undefined" ? 'Global': opt.nombre;
			mig.dTablas.id = typeof opt.id == "undefined" ? 'global': opt.id;
			//Pintamos tablas
			mig.setTabla(mig.dTablas); //Pintamos tablas
		});
	};

	/**
	 * Setea la visibilidad inicial de las series.
	 */ 
	this.visibilidad = function(nombre)
	{
		var reMeta = /Meta/gi;
		var reAcum = /Acum/gi;
		var visacum = $('#visacum').attr('checked');
		var vismeta = $('#vismetas').attr('checked');
		var ret = true;
		if(nombre.match(reMeta) && nombre.match(reAcum) && visacum)
		{
			ret = true;
		}
		else if(nombre.match(reMeta) && nombre.match(reAcum) && !visacum)
		{
			ret = false;
		} 
		else if(nombre.match(reMeta) && !nombre.match(reAcum) && vismeta)
		{
			ret = true;
		} 
		else if(nombre.match(reMeta) && !nombre.match(reAcum) && !vismeta)
		{
			ret = false;
		} 
		else if(!nombre.match(reMeta) && nombre.match(reAcum) && visacum)
		{
			ret = true;
		} 
		else if(!nombre.match(reMeta) && nombre.match(reAcum) && !visacum)
		{
			ret = false;
		} 
		else
		{
			ret = true;
		}
		return ret;
	};

	/**
	 * Indice de grupo, indica como se agrupan las series en la grafica de barras.
	 * Default: Agrupa por "m" el indice numerico de las series que trae una unica consulta.
	 */
	this.idGrupo = function(m,idx,modulo)
	{
		return m;
	};

	/**
	 * Genera las tablas de datos asociadas a una serie
	 */
	this.setTabla = function (oDatos)
	{
		var nombreTabla = oDatos.nombre;
		var id = oDatos.id;
		var t = {};
		var html = '<table id="tabla_'+id+'" class="datos" nombre="'+nombreTabla+'"><caption class="ui-state-default">'+nombreTabla+'</caption>';
		var aFilas = {};
		var dt = [];
		var headCols = ['Periodo'];

		for(nval in oDatos.series){			
			var s = oDatos.series[nval];
			var nSer = s.nombre;
			headCols.push(nSer);
			var val = s.valores;
			//Convertimos de filas a columnas				
			jQuery.each(val,function(j,a)
			{
				if(typeof dt[a[0]] == 'undefined')
				{
					dt[a[0]] = 0; //Indice del segundo nivel, la llave es el num de columna
					aFilas[ a[0] ] = [];
					aFilas[ a[0] ][ dt[ a[0] ] ] = Highcharts.numberFormat(a[1],'2','.',',');
				}
				else if (dt[a[0]] >= 0)
				{
					dt[a[0]]++;
					aFilas[a[0]][dt[a[0]]] = Highcharts.numberFormat(a[1],'2','.',',');
				}
				else
				{
					//console.log('No deberia entrar aqui nunca: dt[a[0]]:'+dt[a[0]]);
				}
			});
		}

		//Construimos las cabezas de la tabla
		html += '<thead><tr>';
		jQuery.each(headCols,function(j,a)
		{
			html += '<th class="ui-state-default">'+a+'</th>';
		});
		html += '</tr></thead>';
		html += '<tbody>';

		//Construimos las filas de la tabla
		jQuery.each(aFilas,function(j,a)
		{
			html += '<tr id="'+j+'">';
			if(mig.nivel == 0) html += '<td>'+Highcharts.dateFormat('%b %Y',j)+'</td>';
			else if(mig.nivel == 1) html += '<td>'+Highcharts.dateFormat('S%W - %b %Y',j)+'</td>';
			else if(mig.nivel == 2) html += '<td>'+Highcharts.dateFormat('%a %d %b %Y',j)+'</td>';
			else if(mig.nivel == 3) html += '<td>'+Highcharts.dateFormat('%a %d %b %Y',j)+'</td>';

			jQuery.each(a,function(fe,b)
			{
				var idCol = headCols[fe+1]+'-'+j+'-'+mig.nivel;
				idCol = idCol.replace(/\W/g,'-');
				idCol = idCol.replace(/\//g,'-');
				html += '<td align="right" id="'+idCol+'">'+b+'</td>';
			});
			html += '</tr>';
		});
		html += '</tbody></table>';
		
		t = $(html);
		//Si ya existe el id de la tabla, eliminamos la tabla.
		if(typeof $('#tabla_'+id).attr('id') != "undefined"){ $('#tabla_'+id+'_wrapper').remove();}
		//Las insertamos
		$('#tablas-'+mig.nivel).append(t);
		$('#tabla_'+id).dataTable(tXopc);
		$('#tabla_'+id).css('border','thin solid silver');
		$('#tabla_'+id+'_wrapper').css("float","left");
	};
};