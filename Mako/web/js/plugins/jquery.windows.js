/*
 * File:        jquery.windows.js
 * Description: Genera columnas redimensionables y minimizables
 * Author:      David Israel Galindo García
 */

(function($){

    /*
     * Variable: Cdefault
     * Purpose:  Opciones default del plugin
     * Scope:    global
     */
    var Wdefault = {
            //Opciones generales
            resizable    : true,
            minimizable  : true,
            minwidth     : 150,
            minheight    : 0,
            maxwidth     : 0,
            display      : {position : 'horizontal', direction : 't'},
            onMax        : '',
            onMin        : '',
            //Opciones de estilo
            Wstyle  :  {
                stylecolumn  : "window-column ui-widget-content ui-corner-all sombra",
                stylecontent : "window-content",
                styleresize  : "window-resizer",
                stylewinhed  : "window-header ui-widget-header ui-corner-top",
                stylewintit  : "window-title",
                stylewinico  : "ui-icon ",
                stylewinbut  : "window-button",
                styleminwra  : "minimize-wrapper",
                styleminhor  : "minimize-wrapper-horizontal",
                styleminver  : "minimize-wrapper-vertical",
                styleminmin  : "minimize-header ui-widget-header ui-corner-all",
                stylemintit  : "minimize-title",
                styleminbut  : "ui-icon minimize-button "
            },
            //Opciones para iconos (clases de UI ThemeRoller)
            Wicons  :  {
                icnmin       : "ui-icon-minus",
                icnmax       : "ui-icon-extlink",
                icnwin       : "ui-icon-newwin"
            }
        };
    /*
     * Variable: Wopt
     * Purpose:  Opciones generales
     * Scope:    global
     */
    var Wopt = {};

    /*
     * Variable: Wchilds
     * Purpose:  Contiene a los chiilds del div principal
     * Scope:    global
     */
    var Wchilds = {};

    var Wwidth = 0;
    var Wmethods = {
        buttonInfo : function()
                    {
                        $('.window-button').each(function(){
                            var state = $(this).parent().parent().attr('window-state');
                            var title = $(this).parent().parent().attr('window-title');
                            if(state == 'min')
                            {
                                this.className += Wopt.Wicons.icnmin;

                                //Si solo se minimiza se agrega el atributo w-state min
                                $(this).attr('w-state', 'min');
                                $(this).attr('title', 'Minimizar -'+title+'-');
                            }
                            else
                            {
                                this.className += Wopt.Wicons.icnmax;

                                //Si solo se maximiza se agrega el atributo w-state max
                                $(this).attr('w-state', 'max');
                                $(this).attr('title', 'Maximizar -'+title+'-');
                            }
                        });

                        $('.window-button').click(function(){
                            Wmethods.minMax($(this));
                        });
                    },
                    
        minMax     : function(target)
                    {
                        var wrapper = $('.'+Wopt.Wstyle.styleminwra);
                        var bar = document.createElement('div');
                        var title = document.createElement('span');
                        var button = document.createElement('span');
                        var state = target.attr('w-state');
                        var column = target.parent().parent().parent();
                        var resizerNext = column.next();
                        var resizerPrev = column.prev();
                        var columnNext = resizerNext.next();
                        var columnPrev = resizerPrev.prev();
                        var columnWidth = column.width();
                        var text = target.parent().parent().attr('window-title');

                        if(text.length>=16)
                        {
                            text = text.substr(0, 13)+'…';
                        }
                        //Asignacion de clases para el menu minimizado
                        $(bar).addClass(Wopt.Wstyle.styleminmin);
                        $(title).addClass(Wopt.Wstyle.stylemintit);
                        $(button).addClass(Wopt.Wstyle.styleminbut);
                        $(button).addClass(Wopt.Wicons.icnwin);
                        //Texto para la ventana minimizada y el title del boton
                        $(title).text(text);
                        $(button).attr('title','Restaurar "'+target.parent().parent().attr('window-title')+'"');
                        //atributo min-patent para saber cual es el padre
                        $(button).attr('min-parent',target.parent().parent().attr('id'));
                        $(bar).append(title);
                        $(bar).append(button);
                        if(state == 'min')
                        {
                            var newWidth;
                            var visible
                            if((columnNext.length>0) &&(columnNext.is(':visible')))
                            {
                                newWidth = parseFloat(columnNext.width())+parseFloat(columnWidth) + 37;
                                columnNext.width(newWidth);
                                resizerNext.hide();
                            }
                            else
                            {
                                newWidth = parseFloat(columnPrev.width())+parseFloat(columnWidth) + 37;
                                columnPrev.width(newWidth);
                                resizerPrev.hide();
                            }
                            column.hide();
                            $(wrapper).append(bar);
                            $(button).click(function(){
                                Wmethods.restore($(this));
                            });
                            visible = $('.'+Wopt.Wstyle.stylewinbut+':visible').length;
                            if(visible==1)
                            {
                                $('.'+Wopt.Wstyle.stylewinbut+':visible').removeClass(Wopt.Wicons.icnmin);
                                $('.'+Wopt.Wstyle.stylewinbut+':visible').removeClass(Wopt.Wicons.icnmax);
                                $('.'+Wopt.Wstyle.stylewinbut+':visible').addClass(Wopt.Wicons.icnwin);
                                $('.'+Wopt.Wstyle.stylewinbut+':visible').attr('title','Restaurar -'+$('.'+Wopt.Wstyle.stylewinbut+':visible').parent().parent().attr('window-title')+'-') ;
                                $('.'+Wopt.Wstyle.stylewinbut+':visible').unbind('click');
                                $('.'+Wopt.Wstyle.stylewinbut+':visible').click(function(){
                                    var parent = $(this).parent().parent().parent();
                                    Wmethods.restoreAll(parent);
                                });
                                //alert('maximizado has algo');
                            }
                            if(typeof(Wopt.onMax) =='function')
                            {
                                Wopt.onMax();
                            }
                        }
                        if(state == 'max')
                        {
                           Wmethods.maxfull(column);
                        }
                    },

        maxfull    : function(target)
                    {
                        var allNext = target.nextAll();
                        var allPrev = target.prevAll();
                        if(allNext.length > 0)
                        {
                            allNext.each(function(){
                                if($(this).is (':visible'))
                                {
                                    $(this).hide();
                                    if($(this).attr('class') != Wopt.Wstyle.styleresize)
                                    {
                                        Wmethods.minAll($(this));
                                    }
                                }
                            });
                        }
                        if(allPrev.length > 0)
                        {
                            allPrev.each(function(){
                                if($(this).is (':visible'))
                                {
                                    $(this).hide();
                                    if($(this).attr('class') != Wopt.Wstyle.styleresize)
                                    {
                                        Wmethods.minAll($(this));
                                    }
                                }
                            });
                        }
                        target.width('98%');
                        var button = target.children().children().children('.'+Wopt.Wstyle.stylewinbut);
                        button.removeClass(Wopt.Wicons.icnmax);
                        button.addClass(Wopt.Wicons.icnwin);
                        button.attr('title','Restaurar -'+target.children().attr('window-title')+'-');
                        button.unbind('click');
                        button.click(function(){
                            Wmethods.restoreAll(target);
                        });
                        if(typeof(Wopt.onMax) =='function')
                        {
                            Wopt.onMax();
                        }
                    },

        minAll     : function(target)
                    {
                        var wrapper = $('.'+Wopt.Wstyle.styleminwra);
                        var bar = document.createElement('div');
                        var title = document.createElement('span');
                        var button = document.createElement('span');
                        var text = target.children().attr('window-title');
                        if(text.length>=16)
                        {
                            text = text.substr(0, 13)+'…';
                        }
                        //Asignacion de clases para el menu minimizado
                        $(bar).addClass(Wopt.Wstyle.styleminmin);
                        $(title).addClass(Wopt.Wstyle.stylemintit);
                        $(button).addClass(Wopt.Wstyle.styleminbut);
                        $(button).addClass(Wopt.Wicons.icnwin);
                        //Texto para la ventana minimizada y el title del boton
                        $(title).text(text);
                        $(button).attr('title','Restaurar -'+target.children().attr('window-title')+'-');
                        //atributo min-patent para saber cual es el padre
                        $(button).attr('min-parent',target.children().attr('id'));
                        $(bar).append(title);
                        $(bar).append(button);
                        $(wrapper).append(bar);
                        $(button).click(function(){
                                Wmethods.restore($(this));
                            });
                    },

        resize     : function()
                   {
                       $('.'+Wopt.Wstyle.styleresize).css('cursor','col-resize');
                       $('.'+Wopt.Wstyle.styleresize).mousedown(function(e){
                            var pos = e.pageX;
                            var resizer = $(this);
                            var Wprev = resizer.prev();
                            var Wnext = resizer.next();
                            var Nprev = 0;
                            var Nnext = 0;
                            var px;
                            var Fprev = 0;
                            var Fnex = 0;
                            $('#wpos').html('pos = '+pos);
                            $('#wprev').html('Wprev = '+Wprev.width());
                            $('#wnext').html('Wnext = '+Wnext.width());
                            $('#windows-wrapper').mousemove(function(e){
                               var init = Wopt.close;
                               if((Wnext.length >0) && (Wprev.length >0) && (Wnext.is (':visible')) && (Wprev.is (':visible'))  )
                               {
                                   Wchilds.css('cursor', 'col-resize')
                                   px = parseFloat(pos) - parseFloat(e.pageX);
                                   if((px > 0)&&(Fnext = 1))
                                       {
                                         Wnext = resizer.next();
                                         Fnex = 0;
                                       }
                                       if((px < 0)&&(Fprev = 1))
                                       {
                                         Wprev = resizer.prev();
                                         Fprev = 0;
                                       }
                                   Nprev = parseFloat(Wprev.width()) - parseFloat(px);
                                   Nnext = parseFloat(Wnext.width()) + parseFloat(px);
                                   pos = e.pageX;

                                   if((Nprev > Wopt.minwidth) && (Nnext > Wopt.minwidth))
                                   {
                                       Wprev.width(Nprev);
                                       Wnext.width(Nnext);
                                   }
                                   else if(Nnext < Wopt.minwidth)
                                   {
                                       Fnext = 1;
                                       Wnext = Wnext.next().next();
                                   }
                                   else if(Nprev < Wopt.minwidth)
                                   {
                                       Fprev = 1;
                                       Wprev = Wprev.prev().prev();
                                   }
                               }
                               else if(Wnext.length <= 0)
                               {
                                   Wnext = resizer.next();
                               }
                               else if(Wprev.length <= 0)
                               {
                                   Wprev =resizer.prev();
                               }

                           }).mouseleave(function(e){
                                Wmethods.resizeEnd($(this))
                           }).mouseup(function(e){
                                Wmethods.resizeEnd($(this))
                        });
                           return false;
                    });

                   },

        restore    : function(target)
                    {
                        var column = $('#'+target.attr('min-parent')).parent();
                        var nextAll = column.nextAll();
                        var prevAll = column.prevAll();
                        var width = column.width();
                        var newWidth;
                        var window = $('.'+Wopt.Wstyle.stylecontent+':visible');
                        if(window.length == 1)
                        {
                            var button = $('.'+Wopt.Wstyle.stylewinbut+':visible');
                            var state = button.parent().parent().attr('window-state');
                            button.removeClass(Wopt.Wicons.icnwin);
                            if(state == 'min')
                            {
                                button.addClass(Wopt.Wicons.icnmin);
                                button.attr('title','Minimizar -'+target.children().attr('window-title')+'-');
                            }
                            else
                            {
                                button.addClass(Wopt.Wicons.icnmax);
                                button.attr('title','Maximizar -'+target.children().attr('window-title')+'-');
                            }
                            button.unbind('click');
                            button.click(function(){
                            Wmethods.minMax($(this));
                        });
                        }
                        if(nextAll.length > 0)
                        {
                            nextAll.each(function(){
                                var type = $(this).attr('class');
                                if(type != Wopt.Wstyle.styleresize)
                                {
                                    if($(this).is(':visible'))
                                    {
                                        newWidth = parseFloat($(this).width()) - parseFloat(width)-37;
                                        $(this).width(newWidth);
                                        column.show();
                                        $(this).show();
                                        $(this).prev().show();
                                    }
                                }
                            });
                        }
                        else
                        {
                            prevAll.each(function(){
                                var type = $(this).attr('class');
                                if(type != Wopt.Wstyle.styleresize)
                                {
                                    if($(this).is(':visible'))
                                    {
                                        newWidth = parseFloat($(this).width()) - parseFloat(width)-37;
                                        $(this).width(newWidth);
                                        column.show();
                                        $(this).show();
                                        $(this).next().show();
                                    }
                                }
                            });
                        }
                        target.parent().remove();
                    },

        restoreAll : function(target)
                    {
                        var allNext = target.nextAll();
                        var allPrev = target.prevAll();
                        var wrapper = $('.'+Wopt.Wstyle.styleminwra);
                        if(allNext.length > 0)
                        {
                            allNext.each(function(){
                                $(this).show();
                                if($(this).attr('class') != Wopt.Wstyle.styleresize)
                                {
                                    $(this).width(Wwidth);
                                }
                            });
                        }
                        if(allPrev.length > 0)
                        {
                            allPrev.each(function(){
                                $(this).show();
                                if($(this).attr('class') != Wopt.Wstyle.styleresize)
                                {
                                    $(this).width(Wwidth);
                                }
                            });
                        }
                        target.width(Wwidth);
                        var button = target.children().children().children('.'+Wopt.Wstyle.stylewinbut);
                        var state = target.children().attr('window-state');
                        button.removeClass(Wopt.Wicons.icnwin);
                        if(state == 'min')
                        {
                            button.addClass(Wopt.Wicons.icnmin);
                            button.attr('title','Minimizar -'+target.children().attr('window-title')+'-');
                        }
                        else
                        {
                            button.addClass(Wopt.Wicons.icnmax);
                            button.attr('title','Maximizar -'+target.children().attr('window-title')+'-');
                        }
                        wrapper.html('');
                        button.unbind('click');
                        button.click(function(){
                            Wmethods.minMax($(this));
                        });
                        if(typeof(Wopt.onMin) =='function')
                        {
                            Wopt.onMin();
                        }
                    },

        resizeEnd  : function(target)
                    {                        
                        target.unbind('mousemove');
                        Wchilds.css('cursor', 'auto');
                        
                        return false;
                    },
       
        sameHeight : function (wrapper)
                   {
                        var h = Wopt.minheight;
                        wrapper.each(function()
                        {
                            var height = $(this).height();
                            if(height > h)
                            {
                                h = height;
                            }
                        });

                        return h+25;
                    },

        sameWidth  : function(wrapper)
                   {
                       var l = wrapper.children().length;
                       var w =(wrapper.width() - ( (l-1) * 15) - (l * 23))/wrapper.children().length;

                       return w;
                   },

        srcoll     : function()
                    {
                        $('.window-content').change(function(){
                           var child= $(this).height();
                           if(child!=724)
                           {
                               //$(this).css('overflow-y','scroll');
                           }
                        });
                    },

        title      : function()
                    {
                         $('.window-title').each(function(){
                            $(this).html($(this).parent().parent().attr('window-title'))
                         });
                    },

        wrapperMin : function(target, wrapper )
                    {
                        if(Wopt.display.position == 'horizontal')
                        {
                            wrapper.className = Wopt.Wstyle.styleminwra+' '+Wopt.Wstyle.styleminhor;
                            if(Wopt.display.direction == 'b')
                            {
                                target.after().append($(wrapper));
                            }
                            else
                            {
                                target.before($(wrapper));
                            }
                        }
                        else
                        {
                            wrapper.className = Wopt.Wstyle.styleminver;
                            if(Wopt.display.direction == 'r')
                            {
                                $(wrapper).css('right',0)
                            }
                            else
                            {
                                $(wrapper).css('left',0)
                            }
                            $(wrapper).css('top',target.position().top);
                            target.parent().prepend($(wrapper));
                        }
                    }
    };
    $.fn.windows = function(settings)
     {
        Wopt = $.extend(Wdefault, settings );
        
        return this.each(function(){
            var sId = this.getAttribute( 'id' );
            var wrapper = document.createElement('div');
            var content = document.createElement('div');
            var header = document.createElement('div');
            var resizer = document.createElement('div');
            var title = document.createElement('span');
            var button = document.createElement('span');
            var icnbutton = Wopt.Wstyle.stylewinbut+' '+Wopt.Wstyle.stylewinico;
            Wchilds = $(this).children();
            //Se calcula el el ancho y el alto de las columnas
            Wwidth = Wmethods.sameWidth($(this));
            var Wheigt = Wmethods.sameHeight(Wchilds);
            $(content).width(Wwidth); //= Wwidth;
            $(content).height(Wheigt);
            //Clase para mostar columnas
            Wchilds.addClass(Wopt.Wstyle.stylecontent);
            //Inserta un div que es el punto de partida para redimenzionar las
            //columnas
            resizer.className = Wopt.Wstyle.styleresize;
            Wchilds.after(resizer);
            $('.'+Wopt.Wstyle.styleresize).height(Wheigt);

            //Se quita el ultimo div
            $('#'+sId+' > div:last-child').remove();

            //Ingreso de estilos
            content.className = Wopt.Wstyle.stylecolumn;
            header.className = Wopt.Wstyle.stylewinhed;
            title.className = Wopt.Wstyle.stylewintit;
            button.className = icnbutton;

            //Ingreso de componentes
            header.appendChild(title);
            
            //Si la opcion resizable es true se llama a la funcion resize
            if(Wopt.resizable==true)
            {
              Wmethods.resize();
              Wmethods.wrapperMin($(this), wrapper)
            }

            //Si la opcion minizable es true se insertan los botones
            if(Wopt.minimizable == true)
            {
                header.appendChild(button);
            }
            Wchilds.wrap(content);
            Wchilds.prepend(header);

            //Función que ingresa los tiutlos a los encabezados
            Wmethods.title();

            //Función que ingresa los titulos y los iconos para minimizar
            Wmethods.buttonInfo();

            Wmethods.srcoll();
        });
   };

  $.fn.windows.format = function() {
   alert('funcion');
};

})(jQuery);

