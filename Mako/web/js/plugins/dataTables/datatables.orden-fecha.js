/**
 * Permite ordenar las columnas que contengan fechas en formato d-m-a. usa el
 * plugin date.js
 */

jQuery.fn.dataTableExt.oSort['fecha_dma-asc'] = function(a, b) {
	var a1 = (a == '') ? '01-01-1970' : a;
	var b1 = (b == '') ? '01-01-1970' : b;
	a1 = a1.replace(/\s/g, "");
	b1 = b1.replace(/\s/g, "");
	var x = Date.parseExact(a1, "dd-MM-yyyy");
	var y = Date.parseExact(b1, "dd-MM-yyyy");
	return ((x.compareTo(y) < 0) ? -1 : (x.compareTo(y) > 0 ? 1 : 0));
};

jQuery.fn.dataTableExt.oSort['fecha_dma-desc'] = function(a, b) {
	var a1 = (a == '') ? '01-01-1970' : a;
	var b1 = (b == '') ? '01-01-1970' : b;
	a1 = a1.replace(/\s/g, "");
	b1 = b1.replace(/\s/g, "");
	var x = Date.parseExact(a1, "dd-MM-yyyy");
	var y = Date.parseExact(b1, "dd-MM-yyyy");
	return ((x.compareTo(y) < 0) ? 1 : ((x.compareTo(y) > 0) ? -1 : 0));
};

/**
 * Permite ordenar las columnas que contengan fechas en formato d-m-a. usa el
 * plugin date.js
 */

jQuery.fn.dataTableExt.oSort['fecha_amd-asc'] = function(a, b) {
	var a1 = (a == '') ? '1970-01-01' : a;
	var b1 = (b == '') ? '1970-01-01' : b;
	a1 = a1.replace(/\s/g, "");
	b1 = b1.replace(/\s/g, "");
	var x = Date.parseExact(a1, "yyyy-MM-dd");
	var y = Date.parseExact(b1, "yyyy-MM-dd");
	return ((x.compareTo(y) < 0) ? -1 : (x.compareTo(y) > 0 ? 1 : 0));
};

jQuery.fn.dataTableExt.oSort['fecha_amd-desc'] = function(a, b) {
	var a1 = (a == '') ? '1970-01-01' : a;
	var b1 = (b == '') ? '1970-01-01' : b;
	a1 = a1.replace(/\s/g, "");
	b1 = b1.replace(/\s/g, "");
	var x = Date.parseExact(a, "yyyy-MM-dd");
	var y = Date.parseExact(b, "yyyy-MM-dd");
	return ((x.compareTo(y) < 0) ? 1 : ((x.compareTo(y) > 0) ? -1 : 0));
};

/**
 * Permite ordenar las columnas que contengan fechas en formato d-m-a h:m:s. usa
 * el plugin date.js
 */

jQuery.fn.dataTableExt.oSort['timestamp_dma-asc'] = function(a, b) {
	var a1 = (a == '') ? '01-01-1970 00:00:00' : a;
	var b1 = (b == '') ? '01-01-1970 00:00:00' : b;
	a1 = a1.replace(/\s/g, "");
	b1 = b1.replace(/\s/g, "");
	var x = Date.parseExact(a1, "dd-MM-yyyyHH:mm:ss");
	var y = Date.parseExact(b1, "dd-MM-yyyyHH:mm:ss");
	return ((x.compareTo(y) < 0) ? -1 : (x.compareTo(y) > 0 ? 1 : 0));
};

jQuery.fn.dataTableExt.oSort['timestamp_dma-desc'] = function(a, b) {
	var a1 = (a == '') ? '01-01-1970 00:00:00' : a;
	var b1 = (b == '') ? '01-01-1970 00:00:00' : b;
	a1 = a1.replace(/\s/g, "");
	b1 = b1.replace(/\s/g, "");
	var x = Date.parseExact(a1, "dd-MM-yyyyHH:mm:ss");
	var y = Date.parseExact(b1, "dd-MM-yyyyHH:mm:ss");
	return ((x.compareTo(y) < 0) ? 1 : ((x.compareTo(y) > 0) ? -1 : 0));
};

/**
 * Permite ordenar las columnas que contengan fechas en formato d-m-a h:m:s. usa
 * el plugin date.js
 */

jQuery.fn.dataTableExt.oSort['timestamp_amd-asc'] = function(a, b) {
	var a1 = (a == '') ? '1970-01-01 00:00:00' : a;
	var b1 = (b == '') ? '1970-01-01 00:00:00' : b;
	a1 = a1.replace(/\s/g, "");
	b1 = b1.replace(/\s/g, "");
	var x = Date.parseExact(a1, "yyyy-MM-ddHH:mm:ss");
	var y = Date.parseExact(b1, "yyyy-MM-ddHH:mm:ss");
	return ((x.compareTo(y) < 0) ? -1 : (x.compareTo(y) > 0 ? 1 : 0));
};

jQuery.fn.dataTableExt.oSort['timestamp_amd-desc'] = function(a, b) {
	var a1 = (a == '') ? '1970-01-01 00:00:00' : a;
	var b1 = (b == '') ? '1970-01-01 00:00:00' : b;
	a1 = a1.replace(/\s/g, "");
	b1 = b1.replace(/\s/g, "");
	var x = Date.parseExact(a1, "yyyy-MM-ddHH:mm:ss");
	var y = Date.parseExact(b1, "yyyy-MM-ddHH:mm:ss");
	return ((x.compareTo(y) < 0) ? 1 : ((x.compareTo(y) > 0) ? -1 : 0));
};
