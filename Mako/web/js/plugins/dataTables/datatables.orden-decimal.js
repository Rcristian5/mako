/**
 * Permite ordenar las columnas que contengan html, numeros negativos y comas separadoras de miles.
 */

    jQuery.fn.dataTableExt.oSort['decimal-asc']  = function(a,b) {
			var x = a.replace( /,/g, "" );
			var y = b.replace( /,/g, "" );
			x = x.replace( /\$/g, "" );
			y = y.replace( /\$/g, "" );
			x = x.replace( /<.*?>/g, "" );
			y = y.replace( /<.*?>/g, "" );
			x = x.replace( /\s/g, "" );
			y = y.replace( /\s/g, "" );
			x = parseFloat( x );
			y = parseFloat( y );
			return ((x < y) ? -1 : ((x > y) ?  1 : 0));
		};

		jQuery.fn.dataTableExt.oSort['decimal-desc'] = function(a,b) {
			var x = a.replace( /,/g, "" );
			var y = b.replace( /,/g, "" );
			x = x.replace( /\$/g, "" );
			y = y.replace( /\$/g, "" );
			x = x.replace( /<.*?>/g, "" );
			y = y.replace( /<.*?>/g, "" );
			x = x.replace( /\s/g, "" );
			y = y.replace( /\s/g, "" );
			x = parseFloat( x );
			y = parseFloat( y );
			return ((x < y) ?  1 : ((x > y) ? -1 : 0));
		};
