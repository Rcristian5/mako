/**
 * ******************************************************************************************************************************
 * 																																*
 * Generamos el plugin para hacer el cascade entre combos.																		*
 * basicamente hacemos que cuando se de el change de un combo																	*
 * ejecute un ajax y llene otro combo, y ejecutamos un callback de dicho combo para que 										*
 * se pueda cachar y realizar otra operacion																					*
 * @author silvio.bravo@enova.mx																								*
 * @title Reingenieria Mako.																									*
 * @date 10-Marzo-2014.																											*
 * ******************************************************************************************************************************
 */


/*
 * El plugin funciona basicamente generando una clase que contendra metodos y todo el funcionamiento del plugin,, y porteriormente 
 * al momento de instanciar el componente este generara una intancia de la clase realizada.
 * 
 */

;(function($) {  
  
/**
 * 
 * Definimos la clase comboMakoCascade y cada uno de sus metodos publicos y privados.
 * 
 */
	
  function comboMakoCascade(element, options) {
    // Referencia al objeto contendor.
    var el 	= element;
    var $el = $(element);
    options = $.extend({}, $.fn.comboMakoCascade.defaults, options);
 
    
    function init() {
     
      $el.change(function(){
    	  loadCascadaDatos($(this), $(options.comboCascade));
    	  //llamamos al metodo onChange
    	  $.isFunction( options.onChange ) && options.onChange.call( $(this) );
    	  
      });
      
    }
 
   
    /**
     * Opcion que permite setear propiedades aun despues de instanciado el componente.
     * @param key nombre de la propiedad.
     * @param val valor de la propiedad a setear.
     * @returns val.
     * 
     */
    
   
    function option (key, val) {
    
      if (val) {
        options[key] = val;
      } else {
        return options[key];
      }
    }
    
    /**
     * 
     * Obtenemos el valor de determinada propiedad.
     * @param key opcion de la cual se requiere obtener el valor
     * @return valor de la propiedad.
     * 
     */
    
    
    function getOption(key){
    	return(options[key]);
    }
    
    
    /**
     * Opcion para definir el valor por default que tendra el combo.
     * @param defaultvalue valor por default que debe tener el opcion que se requiere tener como seleccionado.
     * 
     */
    
    function setDefaultOption(defaultvalue){
    	
		options.DefaultValue=defaultvalue;
		var componente=$el;
		componente.find("option:selected").each(function(){
			$(this).removeAttr("selected");
		});
		componente.find('option[value='+ defaultvalue +  ']').attr('selected','selected');
		
		
	}
   
    function onComplete(){
    	
    	
    }
    function onChange(){
    	
    	
    }
    
    
    /**************************FUNCIONALIDAD INTERNA *************************************/
    
    
    
    /**
     *  Consume servicio por ajax para a partir de el resultado devuelto se llene un combo destino.
     *  @param origenComponente Componente origen
     *  @param destinoComponente COmponente donde seran cargado los datos que retorne el servicio consumido por ajax.
     *  @return void
     */
    
    
    
    function loadCascadaDatos(origenComponente, destinoComponente){
		
		destinoComponente.html('<option selected="selected">Buscando Informaci&oacute;n ...</option>');
		var id				= origenComponente.attr("value");
		var params			= options.params;
		params["valorId"]	= id;
		
		
		//COnsumimos el servicio que se configuro previamente al instanciar el componente
		
		$.ajax({  
			
			  url		: options.urlService,
			  type		: options.method,
			  dataType	: "json",
			  data		: params,
			  success	: function(datos){
				  
			       if(datos.success	===	true){
			    	   llenaCombo(destinoComponente,datos.data,options.comboCascadeDefaultValue);
			    	   //LLamamos el metodo onComplete para que desde la instanci pueda cachar dicho evento
						$.isFunction( options.onComplete ) && options.onComplete.call( destinoComponente );
				   } 
		      }
		});
	}
	
	
	/**
	 * LLenamos un combo a partir de un array de objetos 
	 * @param destino  Combo destino
	 * @param datos array de objetos
	 * @param selectedValue define la opcion que estara como selected dentro del combo.
	 * @return void 
	 */
	
	function llenaCombo(destino, datos, selectedValue){
		
		 
		var nombreId	=options.optionValue;
		var nombreValor	=options.optionLabel;
		var nombreExtra	=options.optionExtraValue;
		var cadena		='<option value="null"></option>';
		var encontrado	=false;
		var extracad	="";

		if(datos.length ==1){
			selectedValue=datos[0][nombreId];
		}
		
		for(var n=0; n<datos.length; n++){
			extracad="";
			if(selectedValue==datos[n][nombreId]){
				encontrado	=true;
				extracad	='selected="selected"';
			}
			cadena+='<option '  + extracad;  
			cadena+='  extra="' + datos[n][nombreExtra]  + '" value="'+ datos[n][nombreId]  +'">' + datos[n][nombreValor].toUpperCase() + '</option>';
		}
		
		destino.html(cadena);
		options.comboCascadeDefaultValue=(encontrado==true)?selectedValue:0;
    $.isFunction( options.onComplete ) && options.onComplete.call( destino );
		
	}
	
	
    
	
	/**************************** FIN FUNCIONALIDAD INTERNA ************************/
	
    
    
    //Iniciamos el plugin
    init();
 
    // Metodos que son publicos.
    return {
      option			: option,
      onComplete		: onComplete,
      setDefaultOption	: setDefaultOption,
      getOption			: getOption,
      onChange			: onChange,
      llenaCombo		:llenaCombo
    };
    
  }
 
  
  
  
  
  $.fn.comboMakoCascade = function(options) {
    
    if (typeof arguments[0] === 'string') {
      var methodName 	= arguments[0];
      var args 			= Array.prototype.slice.call(arguments, 1);
      var returnVal;
      this.each(function() {

        if ($.data(this, 'plugin_comboMakoCascade') && typeof $.data(this, 'plugin_comboMakoCascade')[methodName] === 'function') {
          
          returnVal = $.data(this, 'plugin_comboMakoCascade')[methodName].apply(this, args);
        } else {
          throw new Error('Method ' +  methodName + ' does not exist on jQuery comboMakoCascade');
        }
      });
      if (returnVal !== undefined){
        return returnVal;
      } else {
        return this;
      }
      
    } else if (typeof options === "object" || !options) {
      return this.each(function() {
        if (!$.data(this, 'plugin_comboMakoCascade')) {
          $.data(this, 'plugin_comboMakoCascade', new comboMakoCascade(this, options));
        }
      });
    }
  };
 
  
  /**
   * Opciones de configuracion por default.
   * 
   */
  
  $.fn.comboMakoCascade.defaults = {
    defaultValue		:0,
    params				:{},
	eventHandler		:"change",
	urlService			:"",
	method				:"POST",
	onComplete			:null,
	onChange			:null,
	optionValue 		:"",
	optionLabel			:"",
	optionExtraValue	:"",
	comboCascade		:"",

  };
 
})(jQuery);
