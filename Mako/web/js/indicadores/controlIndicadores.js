/**
 * Control de los indicadores principales: Registro, Inscripciones, Cobranza
 */

var stack=[];
		
$(document).ready(function() {

		//Utilizamos estas variables para simplificar ifs
		var ifArr ={};
		var ifArrCap ={};
		var ifArrOcupa ={};
		ifArrCap['Ocupacion'] = true;
		ifArrCap['TasaOcupacion'] = true;
		ifArr['OcupacionPorHora'] = true;
		ifArr['TasaOcupacionPorHora'] = true;
		ifArr['OcupacionPorDia'] = true;
		ifArr['TasaOcupacionPorDia'] = true;
		
		ifArrOcupa['TasaOcupacion'] = true;
		ifArrOcupa['TasaOcupacion60'] = true;
		ifArrOcupa['TasaOcupacion40'] = true;
		
		var nivel = 0;
		var nivelAnterior = 0;
		
		var ng = new DatosGraficos();
		//Estas variables vienen del programa principal que llama a este JS
		ng.url = urlModulo;
		ng.modulo = modulo;
		ng.opcionesTitulo = opcionesTitulo;
		ng.opcionesSubTitulo = opcionesSubTitulo;	
		ng.opcionesYAxis = opcionesYAxis;

		//Seteamos la visibilidad default para las series de indicadores de capacidad:
		if(ifArrCap[modulo] || ifArr[modulo]) ng.visibilidad = visibilidadOcupacion;
		if(modulo == 'EstatusSocios') ng.visibilidad = visibilidadEstatusSocios;
		
		ng.idGrupo = function(m,idx,modulo)
		{
			return (ifArrOcupa[modulo] || ifArr[modulo]) ? idx:m;
		};
		
		ng.renderTo = 'grafica-detalle-'+nivel;
		ng.graficas = [];
		ng.arParams = {};
		ng.params = {};
		ng.arParams['global'] = {};
		ng.nivel = 0;
		ng.type = (ifArrOcupa[modulo] || ifArr[ng.modulo]) ? 'column':'spline';
		ng.fecha_ini = '';
		ng.fecha_fin = '';
		ng.init();
		
		
		//Opciones de tablas
		var opt = {}; opt.id = 'global-'+nivel; opt.nombre = 'Global'; 
		ng.get();

		stack[0] = ng;
		
///BIND POR ZONAS
		$('#zonas').bind('zona',function(e){
			$(e.target).attr('disabled','disabled'); //Deshabilitamos el boton para impedir dobles clicks
			
			var idx = $(e.target).val();
			//var modulo = modulo; //Esta variable esta definida en el programa principal que llama a este prog.
			
			//Creamos las series para la zona si el chb de zona esta hablitado
			var gr = stack[nivel];
			if($(e.target).attr('checked')) 
			{
				var nombreTabla = $(e.target).next('label').text(); 
				var params = {zona:idx, nivel:nivel, fecha_ini: gr.fecha_ini, fecha_fin: gr.fecha_fin, categoria:'zona', nombreTabla:nombreTabla };
				//Pasamos como parametro los valores del control de horas en caso de ser uno de los modulos que lo usan.
				if(ifArrOcupa[modulo])
				{
						params.hora_ini = $('#control-horas').slider("values",0); 
						params.hora_fin = $('#control-horas').slider("values",1);
				}

				gr.arParams['zona_id_'+idx] = params;
				gr.params = params;
				gr.type = 'column';
				gr.chb = $(e.target); //Le pasamos el chb clickeado para impedir dobles clicks.
				
				var opt = {};
				opt.id='zona_id_'+idx+'-'+nivel; //Construimos lo necesario para las tablas de datos
				opt.nombre = nombreTabla;
				gr.dTablas.series = [];
				
				gr.get(null,opt); //Ejecutamos el get de datos.
				
			}
			else
			{
				for(var ar in gr.arSeries)
				{
					if(gr.arSeries[ar].categoria=='zona' && gr.arSeries[ar].id == idx){
						 gr.arSeries[ar].serie.remove();
						 delete gr.arSeries[ar];
						 delete gr.arParams['zona_id_'+idx];
					}
				}
				$('#tabla_zona_id_'+idx+'-'+nivel).remove();
				$('#tabla_zona_id_'+idx+'-'+nivel+'_wrapper').remove();
				$(e.target).removeAttr('disabled');
			}
		}); 

//BIND POR CENTROS
		$('#centros').bind('centro',function(e){
			var idx = $(e.target).val();
			$(e.target).attr('disabled','disabled');
			//var modulo = modulo //Esta var esta definida en el prog principal que llama a este modulo
			
			var labelTabla = $(e.target).closest('tr').find('label');
			var nombreTabla = labelTabla.length > 0 ? $(labelTabla[0]).text() + ' | ' + $(labelTabla[1]).text() : '';  
			//Creamos las series para el centro si el chb de centro esta hablitado
			var gr = stack[nivel];
			if($(e.target).attr('checked')) 
			{
				var params = {centro_id:idx, nivel:nivel, fecha_ini: gr.fecha_ini, fecha_fin: gr.fecha_fin, categoria:'centro', nombreTabla:nombreTabla};
				//Pasamos como parametro los valores del control de horas en caso de ser uno de los modulos que lo usan.
				if(ifArrOcupa[modulo])
				{
						params.hora_ini = $('#control-horas').slider("values",0); 
						params.hora_fin = $('#control-horas').slider("values",1);
				}
				gr.arParams['centro_id_'+idx] = params;
				gr.params = params;
				gr.type = 'column';
				gr.chb = $(e.target); //Le pasamos el chb clickeado para impedir dobles clicks.

				var opt = {};
				opt.id='centro_id_'+idx+'-'+nivel; //Construimos lo necesario para las tablas de datos
				opt.nombre = nombreTabla;
				gr.dTablas.series = [];
				gr.get(null,opt);
			}
			else //Si no esta checado el chb eliminams la serie
			{
				for(var ar in gr.arSeries)
				{
					if(gr.arSeries[ar].categoria=='centro' && gr.arSeries[ar].id == idx){
						 gr.arSeries[ar].serie.remove();
						 delete gr.arSeries[ar];
						 delete gr.arParams['centro_id_'+idx];
					}
				}
				$('#tabla_centro_id_'+idx+'-'+nivel).remove();
				$('#tabla_centro_id_'+idx+'-'+nivel+'_wrapper').remove();
				$(e.target).removeAttr('disabled');
			}
		});
		 
//BIND DE ZOOM		
		$(document).bind('zoom',function(e,f1,f2){

			//Si el nivel actual es menor que 2 aumentamos el nivel y escondemos el nivel actual
			if(nivel < 2) { 
				nivelAnterior = nivel;
				nivel++;
				$('#grafica-detalle-'+nivelAnterior).hide();
				$('#zonas-'+nivelAnterior).hide();
				
				$('#tablas-'+nivelAnterior).hide();
				$('#tablas-'+nivelAnterior+'_wrapper').hide();

				//Reseteamos los checkboxes de la tabla antes de setear series
				$('#centros-'+nivel).find(":checked").attr('checked',false);
				
				//Ponemos el default del chb de las series globales a checked
				if(ifArr[modulo]) $('#visacum').attr('checked','checked');
					
				//Seteamos los chb de las tablas de centros.
				setTablasCentros(nivel,nivelAnterior);
				
				//Clonamos los controles
				var nz = $('#zonas-'+nivelAnterior).clone();
				nz.attr('id','zonas-'+nivel);
				nz.children('input').each(function(){$(this).attr('id',$(this).attr('id')+'-'+nivel);});
				nz.children('label').each(function(){$(this).attr('for',$(this).attr('for')+'-'+nivel);});
				nz.appendTo('#zonas');
				nz.show();

				$('#centros-'+nivelAnterior).hide();
				$('#centros-'+nivelAnterior+'_wrapper').hide();
				$('#centros-'+nivel).show();
				$('#centros-'+nivel+'_wrapper').show();

				//Mostramos el control para regresar de nivel
				$('#cont_regresar').show();
			} 
			//Mostramos el nuevo nivel
			$('#grafica-detalle-'+nivel).show();
			$('#tablas-'+nivel).show();

			//obtenemos los parametros del nivel anterior para regenerar las series que se hayan creado en el anterior
			var arParams = stack[nivelAnterior].arParams;
			
			//Construimos el nuevo nivel
			var ng = new DatosGraficos();
			//var urlModulo = '<?php echo url_for('reporte/'.$modulo)?>';
			ng.url = urlModulo;
			ng.modulo = modulo;
			ng.opcionesTitulo = stack[nivelAnterior].opcionesTitulo;
			ng.opcionesSubTitulo = stack[nivelAnterior].opcionesSubTitulo;	
			ng.opcionesYAxis = stack[nivelAnterior].opcionesYAxis;
			ng.idGrupo = function(m,idx,modulo)
			{
				return (ifArrOcupa[modulo] || ifArr[modulo])  ? idx:m;
			};
			//Seteamos la visibilidad default para las series de indicadores de capacidad:
			if(ifArrCap[ng.modulo] ||  ifArr[ng.modulo]) ng.visibilidad = visibilidadOcupacion;
			if(ng.modulo == 'EstatusSocios') ng.visibilidad = visibilidadEstatusSocios;
			
			ng.renderTo = 'grafica-detalle-'+nivel;
			ng.graficas = [];
			ng.nivel = nivel;
			ng.params = {'fecha_ini':f1,'fecha_fin':f2, 'nivel':nivel, nombreTabla:'Global'};
			
			//Pasamos como parametro los valores del control de horas en caso de ser uno de los modulos que lo usan.
			if(ifArrOcupa[ng.modulo])
			{
					ng.params.hora_ini = $('#control-horas').slider("values",0); 
					ng.params.hora_fin = $('#control-horas').slider("values",1);
			}
			ng.fecha_ini = f1;
			ng.fecha_fin = f2;
			ng.arParams={};
			ng.arParams['global'] = ng.params;
			ng.type = (ifArrOcupa[ng.modulo]  || ifArr[ng.modulo]) ? 'column':'spline';

			ng.init();
			//Opciones de tablas
			var opt = {}; opt.id ='global-'+nivel; opt.nombre = 'Global';  
			ng.get(null,opt);

			stack[nivel] = ng;
			fecha_ini = f1;
			fecha_fin = f2;

			//Todas las series que se han solicitado en el nivel anterior las volvemos a solicitar al servidor con el nuevo nivel de zoom
			for(var pars in arParams)
			{
				if(pars != 'global')
				{
					var narParams = arParams[pars]
					narParams.nivel= nivel;
					narParams.fecha_ini = f1;
					narParams.fecha_fin = f2;
					ng.params = narParams;
					ng.arParams[pars] = narParams;
					var opcTablas = {};
					opcTablas.nombre = narParams['nombreTabla'];
					opcTablas.id = pars+'-'+nivel;
					var categoria = narParams['categoria'];
					var nombreTabla = narParams['nombreTabla'];
					ng.get('column',opcTablas);
				}
			}
			
		}); 


//BIND REGRESAR
		$('#regresar').click(function(e){
			//console.log('nivel antes regreso:'+nivelAnterior);
			fecha_ini = typeof stack[nivelAnterior].params.fecha_ini == "undefined" ? '': stack[nivelAnterior].params.fecha_ini;
			fecha_fin = typeof stack[nivelAnterior].params.fecha_fin == "undefined" ? '': stack[nivelAnterior].params.fecha_fin;
			if(nivel > 0) {
				$('#grafica-detalle-'+nivel).hide();
				$('#zonas-'+nivel).remove();
				$('#centros-'+nivel).hide();
				$('#centros-'+nivel+'_wrapper').hide();
				$('#tablas-'+nivel).html(null);
				nivel--;
				if(nivel == 0) $('#cont_regresar').hide(); 
			}
			else
			{
				$('#cont_regresar').hide();
			}
			//console.log('Nivel actual:'+nivel+' F1:'+fecha_ini+' F2: '+fecha_fin);
			$('#grafica-detalle-'+nivel).show();
			$('#zonas-'+nivel).show();
			$('#centros-'+nivel).show();
			$('#centros-'+nivel+'_wrapper').show();
			$('#tablas-'+nivel).show();
			
		});

//BIND DE LOS CHB DE VISIBILIDAD DE METAS
		$('#vismetas').click(function(e){

			var gr = stack[nivel];
			var reMeta = /Meta/gi;
			var reAcum = /Acum/gi;
		    
			if($(e.target).attr('checked')) 
				{
					for(var ids in gr.arSeries)
					{
						var nombre = gr.arSeries[ids].nombre;
						if(nombre.match(reMeta) && !nombre.match(reAcum)) gr.arSeries[ids].serie.show();
					}
				}
				else
				{
					for(var ids in gr.arSeries)
					{
						var nombre = gr.arSeries[ids].nombre;
						if(nombre.match(reMeta) && !nombre.match(reAcum)) gr.arSeries[ids].serie.hide();
					}
				}
			});
		
//BIND DE LOS CHB DE VISIBILIDAD DE ACUMULADOS
		$('#visacum').click(function(e){
			var gr = stack[nivel];
			var reMeta = /Meta/gi;
			var reAcum = /Acum/gi;
		    var reCapa = /Capacidad/gi;
		    
			if($(e.target).attr('checked')) 
			{
				for(var ids in gr.arSeries)
				{
					var nombre = gr.arSeries[ids].nombre;
					if(nombre.match(reMeta) && nombre.match(reAcum)) gr.arSeries[ids].serie.show();
					if(!nombre.match(reMeta) && nombre.match(reAcum)) gr.arSeries[ids].serie.show();
					if(nombre.match(reCapa)) gr.arSeries[ids].serie.show();				
					if(ifArr[modulo] && gr.arSeries[ids].serie.options.categoria=='global') gr.arSeries[ids].serie.show();
				}
				
			}
			else
			{
				for(var ids in gr.arSeries)
				{
					var nombre = gr.arSeries[ids].nombre;
					if(nombre.match(reMeta) && nombre.match(reAcum)) gr.arSeries[ids].serie.hide();
					if(!nombre.match(reMeta) && nombre.match(reAcum)) gr.arSeries[ids].serie.hide();
					if(nombre.match(reCapa)) gr.arSeries[ids].serie.hide();
					if(ifArr[modulo] && gr.arSeries[ids].serie.options.categoria=='global') gr.arSeries[ids].serie.hide();
				}
			}
		});

		//BIND DE LOS CHB DE VISIBILIDAD DE ACTIVOS (indicadores de estatus de socio modulo EstatusSocio)
		$('#visactivos').click(function(e){
			var gr = stack[nivel];
			var reActivo = /Activo/gi;
			if($(e.target).attr('checked')) 
			{
				for(var ids in gr.arSeries)
				{
					var nombre = gr.arSeries[ids].nombre;
					if(nombre.match(reActivo)) gr.arSeries[ids].serie.show();
				}
				
			}
			else
			{
				for(var ids in gr.arSeries)
				{
					var nombre = gr.arSeries[ids].nombre;
					if(nombre.match(reActivo)) gr.arSeries[ids].serie.hide();
				}
			}
		});

		//BIND DE LOS CHB DE VISIBILIDAD DE NUEVOS (indicadores de estatus de socio modulo EstatusSocio)
		$('#visnuevos').click(function(e){
			var gr = stack[nivel];
			var reNuevo = /Nuevo/gi;
			if($(e.target).attr('checked')) 
			{
				for(var ids in gr.arSeries)
				{
					var nombre = gr.arSeries[ids].nombre;
					if(nombre.match(reNuevo)) gr.arSeries[ids].serie.show();
				}
				
			}
			else
			{
				for(var ids in gr.arSeries)
				{
					var nombre = gr.arSeries[ids].nombre;
					if(nombre.match(reNuevo)) gr.arSeries[ids].serie.hide();
				}
			}
		});

//Tablas de catalogos
var tCentros = [];
tCentros[0] = $('#centros-0').dataTable(tOpc);
tCentros[1] = $('#centros-1').dataTable(tOpc);
tCentros[2] = $('#centros-2').dataTable(tOpc);
tCentros[3] = $('#centros-3').dataTable(tOpc);

$('#centros-1_wrapper').hide();
$('#centros-2_wrapper').hide();
$('#centros-3_wrapper').hide();

tablasCentrosConfigInicial();

function setTablasCentros(n,na)
{
	var rows = tCentros[na].fnGetNodes();
	var rChb = {};
	for(var row in rows)
	{
		$(rows[row]).find(":checked").each(function(){
			if($(this).attr('checked')) rChb[row] = $(this).attr('value'); 
		});
	}

	for(var nrow in rChb)
	{
		var row = tCentros[n].fnGetNodes(nrow);
		$(row).find("input").each(function(){
			$(this).attr('checked','checked');
		});
	}
}

/**
 * Configura los ids de las tablas de centros inicialmente
 */
function tablasCentrosConfigInicial()
{
	for(var n=1; n<=3; n++)
	{
		var rows = tCentros[n].fnGetNodes();
		var rChb = {};
		for(var row in rows)
		{
			$(rows[row]).find(":checkbox").each(function(){
				$(this).attr('id',$(this).attr('id')+'-'+n);
			});
			$(rows[row]).find("label").each(function(){
				$(this).attr('for',$(this).attr('for')+'-'+n);
			});
		}
	}
}

//BIND DE LAS ACCIONES AJAX
$(document).ajaxSuccess(function(e, xhr, settings){
	$(e.target).removeAttr('disabled');
});
			
 //Bind de las subseries en cobranza
$(document).bind('sub.serie', function(event, p) {
	if(p.idx == 0 && p.modulo == 'CobranzaTotales')
	{
		subDetalle(p.categoria, p.idSerie, p.fecha, p.fechaInicio, p.fechaFin, p.valor, p.nombre, nivel);
	}
});

	$('#sub_detalle_ventana').dialog({ 
		autoOpen: false,
		modal:true
	});

	$('#sub_sub_detalle_ventana').dialog({ 
		autoOpen: false,
		modal:true 
	});

    //Modificamos el texto del label del checkb
	if(modulo == 'Ocupacion')
	{
		$('[for="visacum"]').text('Mostrar Capacidad');
	}
	if(ifArr[modulo])
	{
		$('[for="visacum"]').text('Mostrar Global');
		$('#visacum').attr('checked','checked');
	}
	
	function visibilidadOcupacion(nombre)
	{
		var reCapa = /Capacidad/gi;
		var visacum = $('#visacum').attr('checked');
		var ret = true;
		if(nombre.match(reCapa) && visacum)
		{
			ret = true;
		}
		else if(nombre.match(reCapa) && !visacum)
		{
			ret = false;
		} 
		else
		{
			ret = true;
		}
		return ret;
	};
	//function

	
	function visibilidadEstatusSocios(nombre)
	{
		var reActivo = /Activo/gi;
		var reNuevo = /Nuevo/gi;
		var reAcum = /Acum/gi;
		
		var visacum = $('#visacum').attr('checked');
		var visactivo = $('#visactivos').attr('checked');
		var visnuevo = $('#visnuevos').attr('checked');
		
		var ret = true;
		if(nombre.match(reAcum) && visacum || nombre.match(reNuevo) && visnuevo || nombre.match(reActivo) && visactivo  )
		{
			ret = true;
		}
		else if(nombre.match(reAcum) && !visacum ||  nombre.match(reNuevo) && !visnuevo || nombre.match(reActivo) && !visactivo  )
		{
			ret = false;
		} 
		else
		{
			ret = true;
		}
		return ret;
	};
	//function
	
	//Slider de horas
	$( "#control-horas" ).slider({
			range: true,
			min: 8,
			max: 20,
			step: 1,
			   minRangeSize: 1,
			    maxRangeSize: 20,
			    lowMax: 19,
			    topMin: 9,
			    autoShift: false,
			values: [ 8,20],
			slide: function( event, ui ) {$('#rango-horas').text( 'Entre las '+ui.values[ 0 ]+':00 y las '+(ui.values[ 1 ]-1)+ ':59 Hrs.' );},
			stop: function( event, ui ) {
				
				//Todas las series que se han solicitado en el nivel anterior las volvemos a solicitar al servidor con el nuevo nivel de zoom
				var arParams = stack[nivel].arParams;
				
				var ng = new DatosGraficos();
				ng.url = urlModulo;
				ng.modulo = modulo;
				ng.opcionesTitulo = stack[nivel].opcionesTitulo;
				ng.opcionesSubTitulo = stack[nivel].opcionesSubTitulo;	
				ng.opcionesYAxis = stack[nivel].opcionesYAxis;
				ng.idGrupo = function(m,idx,modulo)
				{
					return (modulo=='TasaOcupacion'  || modulo=='TasaOcupacion60' || modulo=='TasaOcupacion40' || ifArr[modulo])  ? idx:m;
				};
				//Seteamos la visibilidad default para las series de indicadores de capacidad:
				if(ifArrCap[ng.modulo] ||  ifArr[ng.modulo]) ng.visibilidad = visibilidadOcupacion;
				if(modulo == 'EstatusSocios') ng.visibilidad = visibilidadEstatusSocios;
				
				ng.renderTo = 'grafica-detalle-'+nivel;
				ng.graficas = [];
				ng.nivel = nivel;
				ng.params = {'fecha_ini':stack[nivel].fecha_ini,'fecha_fin':stack[nivel].fecha_fin, 'nivel':nivel, nombreTabla:'Global', hora_ini:ui.values[ 0 ],hora_fin:(ui.values[ 1 ]-1)};
				ng.fecha_ini = stack[nivel].fecha_ini;
				ng.fecha_fin = stack[nivel].fecha_fin;
				ng.arParams={};
				ng.arParams['global'] = ng.params;
				ng.type = (ng.modulo=='TasaOcupacion'  || ng.modulo=='TasaOcupacion60' || ng.modulo=='TasaOcupacion40'  || ifArr[ng.modulo]) ? 'column':'spline';

				ng.init();
				//Opciones de tablas
				var opt = {}; opt.id = (nivel == 0)? 'global':'global-'+nivel;
				opt.nombre = 'Global';  
				ng.get(null,opt);

				//console.log(arParams);
				for(var pars in arParams)
				{
					//console.log(pars);
					if(pars != 'global')
					{
						var narParams = arParams[pars]
						narParams.nivel= nivel;
						narParams.fecha_ini = stack[nivel].fecha_ini;
						narParams.fecha_fin = stack[nivel].fecha_fin;
						narParams.hora_ini = ui.values[ 0 ];
						narParams.hora_fin = (ui.values[ 1 ]-1);
						ng.params = narParams;
						ng.arParams[pars] = narParams;
						var opcTablas = {};
						opcTablas.nombre = narParams['nombreTabla'];
						opcTablas.id = pars+'-'+nivel;
						var categoria = narParams['categoria'];
						var nombreTabla = narParams['nombreTabla'];
						ng.get('column',opcTablas);
					}
				}
				
				stack[nivel] = ng;
				
				//$('#rango-horas').text( 'Entre las '+ui.values[ 0 ]+' y las '+ui.values[ 1 ]+ ':59 Hrs.' );
				
			}
		});
	
});

//Slider extension
(function ($) {
    if ($.ui.slider)
    {
        // add minimum range length option
        $.extend($.ui.slider.prototype.options, {
            minRangeSize: 0,
            maxRangeSize: 100,
            autoShift: false,
            lowMax: 100,
            topMin: 0
        });

        $.extend($.ui.slider.prototype, {
            _slide: function (event, index, newVal) {
                var otherVal,
                newValues,
                allowed,
                factor;

                if (this.options.values && this.options.values.length)
                {
                    otherVal = this.values(index ? 0 : 1);
                    factor = index === 0 ? 1 : -1;

                    if (this.options.values.length === 2 && this.options.range === true)
                    {
                        // lower bound max
                        if (index === 0 && newVal > this.options.lowMax)
                        {
                            newVal = this.options.lowMax;
                        }
                        // upper bound min
                        if (index === 1 && newVal < this.options.topMin)
                        {
                            newVal = this.options.topMin;
                        }
                        // minimum range requirements
                        if ((otherVal - newVal) * factor < this.options.minRangeSize)
                        {
                            newVal = otherVal - this.options.minRangeSize * factor;
                        }
                        // maximum range requirements
                        if ((otherVal - newVal) * factor > this.options.maxRangeSize)
                        {
                            if (this.options.autoShift === true)
                            {
                                otherVal = newVal + this.options.maxRangeSize * factor;
                            }
                            else
                            {
                                newVal = otherVal - this.options.maxRangeSize * factor;
                            }
                        }
                    }

                    if (newVal !== this.values(index))
                    {
                        newValues = this.values();
                        newValues[index] = newVal;
                        newValues[index ? 0 : 1] = otherVal;
                        // A slide can be canceled by returning false from the slide callback
                        allowed = this._trigger("slide", event, {
                            handle: this.handles[index],
                            value: newVal,
                            values: newValues
                        });
                        // otherVal = this.values(index ? 0 : 1);
                        if (allowed !== false)
                        {
                            this.values(index, newVal, true);
                            this.values((index + 1) % 2, otherVal, true);
                        }
                    }
                } else
                {
                    if (newVal !== this.value())
                    {
                        // A slide can be canceled by returning false from the slide callback
                        allowed = this._trigger("slide", event, {
                            handle: this.handles[index],
                            value: newVal
                        });
                        if (allowed !== false)
                        {
                            this.value(newVal);
                        }
                    }
                }
            }
        });
    }
})(jQuery);