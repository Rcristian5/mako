/**
 * Operaciones del control de captura de huella
 */


var cerrarControl = false;
var ch = ''; //Calidad de huella

/**
 * Comunica los mensajes de debug del applet java a javascript
 * @param {String} cad
 */
function debuguear(cad){
    //alert(cad);
    //var log = document.getElementById('logdiv');
    //log.innerHTML = cad +'<br/>'+log.innerHTML;
}

/**
 * Recibe la cadena b64 correspondiente a la matriz de vectores de la huella
 * @param {String} cad
 */
function tplB64(cad){
    document.getElementById('tplb64').value = '';
    if (cad == '') {
        return;
    }
	
    document.getElementById('tplb64').value = cad;
    if (cerrarControl) {
    	cerrarControl = false;
    	$(document).trigger('huella.cierramodal');
    }
}

/**
 * Indica la calidad de la huella tomada, esta funcion la usa directamente el jar de huella
 * @param {String} ch
 */
function qHuella(ch){

    if (ch == '') 
	{
        return;
    }
    else
	{
        if (ch == 'HQ') 
		{
        	$('#img_huella').attr({'src':'/imgs/iconos/huella_verde.jpg', 'calidad': 'HQ'});
			cerrarControl = true;
        }
        else {
            if (ch == 'MQ') 
			{
            	$('#img_huella').attr({'src':'/imgs/iconos/huella_rojo.jpg', 'calidad': 'MQ'});
            }
            else{
                if (ch == 'LQ') 
				{
                	$('#img_huella').attr({'src':'/imgs/iconos/huella_rojo.jpg', 'calidad': 'LQ'});
                }
            }
        }
    }
}

$(document).ready(function() 
{
    	//Verificamos si en la edición ya traemos una huella capturada, entonces cambiamos el icono de
		//huella de gris a verde.
		if($('#socio_huella').val() != '')
		{
			$('#img_huella').attr('src','/imgs/iconos/huella_verde.jpg');
		}
    	var $wcm = $("#dialogo-control-huella").dialog(
		{
    		modal: true,
    		autoOpen: false,
    		closeOnEscape: false,
    		position: ['center','top'],
    		title:	'Capturar Huella'
			});

    	$('#liga-control-huella').click(function() 
    	{
	    	$wcm.dialog('open');
    	});
    	

    	$("#dialogo-control-huella").bind('dialogclose',function(event,ui)
    	{
    		$('#control_huella').hide();
    		if($('#tplb64').val()=='')
    		{
    			$('#img_huella').attr('src',"/imgs/iconos/huella_gray.jpg");
    		}
    		else
    		{
    			
    			if($('#img_huella').attr('calidad')=='HQ')
    			{
    				$('#img_huella').attr('src',"/imgs/iconos/huella_verde.jpg");
    				//TODO: Se debe desasociar el campo de la plantilla para poder hacer un componente independiente
    				$('#socio_huella').val($('#tplb64').val());
    			}
    			else
    			{
    				$('#img_huella').attr('src',"/imgs/iconos/huella_gray.jpg");
    				$('#socio_huella').val("");
    			}
    		}
    	});
    	
    	$("#dialogo-control-huella").bind('dialogopen',function(event,ui)
    	{
    		$('#img_huella').attr('src',"/imgs/iconos/huella_rojo.jpg");
    		$('#control_huella').show();
    	});
    	
    	$(document).bind('huella.cierramodal',function(){
    		$wcm.dialog('close');
    	});
    	
});