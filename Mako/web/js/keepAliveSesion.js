/**
 * Mantiene viva la sesión del lado del servidor
 * Esta solución es debido a q ningunas de las opciones ofrecidas por symfony ni por config
 * del ini de php han funcionado.
 */
$(function() {
	
	var intervalo = 900000;
	var contexto = '';
	var urlSes = contexto+'/sesiones/keepAlive';
	console.log('url:'+urlSes);
	setInterval(function(){
		
		$.ajax({  
			  type: 'GET',
			  dataType: 'json', //Esperamos que regrese JSON del servidor  
			  url: urlSes,  
			  success: function(resJson, txtStatus, req) {  
				//Respuesta json
				console.log(resJson.msg);
			  },
			  error: function(res) 
			  {  
					console.error(res);
			  }
		});
		
	},intervalo);

});