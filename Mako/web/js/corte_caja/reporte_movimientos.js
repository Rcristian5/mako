$(document).ready(function(){
	
	
	var facturaDetailWindow=$("#verDetalleFactura").dialog(
			{
	    		modal: true,
				autoOpen: false,	    		
	    		position: ['center','center'],
	    		title:	'Detalle de Factura',
	    		//setter
	    		height: 550,
	    		width: 800,
	    		buttons: {
					'Cerrar': function() 
		    		{
						$(this).dialog('close');
					}
				}
	});
	
	
	
	$("#generarReporte").click(function(){
		var tipo=	$("#movimiento").is(":checked")==false?1:2;
		var desde	=$("#desde").attr("value");
		var hasta	=$("#hasta").attr("value");
		$("#contentReporte").html('<div style="width: 100%; text-align: center; "><img src="/imgs/ajax-circle.gif" /> &nbsp;&nbsp; Buscando datos, espere un momento....</div>');
		$.ajax({  
			
			  url		: "/corte_caja/ResumenValija",
			  type		: "POST",
			  data		: {tipo: tipo, fechainicio:desde, fechafinal: hasta},
			  success	: function(datos){
				  $("#contentReporte").html(datos); 
		      }
		});
	});
	
	
	$(".detalleFactura").live("click",function(){
		var folio=$(this).attr("alt");

		$("#verDetalleFactura").html("");
		$("#verDetalleFactura").load(								 
				"/corte_caja/detalleFactura",
				 {
					facturaid:folio
				 }			
				 
		); 
		
		facturaDetailWindow.dialog("open");
		
	});
	
	
});