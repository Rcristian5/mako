﻿/**
 * Librería de filtros y validaciones. 
 *TODO: Implementar paso de ñs y acentos para ciertos campos. donde es forzoso.
 */
 
 // deshabilita back
window.history.forward(1);


this.onload=pegaHandlers;
//this.form[0].onsubmit=validaEnvio;

function validaExtensiones(f){
	var reg=/1/;
	try{
		var soportados=f.getAttribute('extensiones');
		var exts=soportados.split(',');
		var partes=f.value.split('.');
		var extAr=partes[partes.length-1];
		var ban="";
		for(var a=0; a < exts.length; a++){
			if(exts[a] != extAr){
				ban += true;
			}
		}
		if(!reg.test(ban)){
			alert('El archivo no es del ninguno de los siguientes tipos: ' + soportados);
			return false;
		}
	}catch(e){
		//alert(e);
	}
}

function validaFecha(){
	var fecha=this.value;
	if(fecha == '') return false;
	if(!isDate(fecha,'dd-MM-yyyy') && fecha != ''){
		alert('No es una fecha válida');
		this.focus;
		this.select;
		this.setAttribute('class','error');				
		return false;
	}	
}
//funcion de marce para las fechas de vencimiento de las tarjetas
function validaVencimiento(){
	var fecha=this.value;
	if(!isDate(fecha,'MM-yy') && fecha != ''){
		alert('No es una fecha válida');
		this.focus;
		this.select;
		this.setAttribute('class','error');				
		return false;
	}	
}

function requeridos(esteDiv){
	
	if (esteDiv == undefined) {
		elems = document.getElementsByTagName('input');
		elemsS = document.getElementsByTagName('select');
		elemsT = document.getElementsByTagName('textarea');
	}else{
		elems = esteDiv.getElementsByTagName('input');
		elemsS = esteDiv.getElementsByTagName('select');
		elemsT = esteDiv.getElementsByTagName('textarea');
	}
	
	cont=document.getElementById('codigos');

	// Para los Inputs :  Text, radio, etc
	for(i=0;i<elems.length;i++){
		//checa para inputs de texto
		if(elems[i].type=='text' &&
		elems[i].getAttribute('requerido') =="1" &&
		elems[i].style.display != 'none' &&
		elems[i].getAttribute('disabled') != false
		){
		//alert('evaluando > '+elems[i].id + ': display -> '+elems[i].getAttribute('style')+ ': requerido -> '+elems[i].getAttribute('requerido'));
			if(elems[i].value==''){
				elems[i].focus();
				abreError(elems[i]);
				alert('Este dato es requerido');
				return false;
			}else
				cierraError(elems[i]);
		}
		//checa para inputs de archivo
		if(elems[i].type=='file' && elems[i].getAttribute('requerido') =="1"){
			validaExtensiones(elems[i]);
			if(elems[i].value==''){
				elems[i].focus();
				abreError(elems[i]);				
				alert('Este campo es requerido');
				return false;
			}else
				cierraError(elems[i]);
		}
		
		//Checa para radios
		if((elems[i].type=='radio' && elems[i].getAttribute('requerido') == "1") || (elems[i].type=='checkbox' && elems[i].getAttribute('requerido') == "1")){
			rer=/true/;
			var radios=document.getElementsByName(elems[i].name);
			var r="";
			for(j=0;j<radios.length;j++){
				r += radios[j].checked;
			}
			if(!rer.test(r)){
				elems[i].focus();
				elems[i].select;
				abreError(elems[i]);				
				alert('Se requiere que conteste almenos una opción.');
				return false;
			}else
				cierraError(elems[i]);
		}
	}


















	//Para elementos select
	for(i=0;i<elemsS.length;i++){
		console.log('');
		console.log('');
		console.log('******************************************************************');
		console.log(i);
		console.log(elemsS[i]);
		if(elemsS[i].getAttribute('requerido') == "1"){
			if(elemsS[i].value=='' &&
			elemsS[i].style.display != 'none' &&
			elemsS[i].getAttribute('disabled') != false
			){
				elemsS[i].focus();
				elemsS[i].select;
				abreError(elemsS[i]);
				alert('Este campo es requerido');
				return false;
			}else{
				cierraError(elemsS[i]);
				console.log('Elemento');
				console.log(elemsS[i]);
			}
		}
	}
	//Para textarea
	for(i=0;i<elemsT.length;i++){
		if(elemsT[i].getAttribute('requerido') == "1"){
			if(elemsT[i].value==''){
				elemsT[i].focus();
				abreError(elemsT[i]);
				alert('Este campo es requerido');
				return false;
			}else
				cierraError(elems[i]);
		}
	}
	return true;
}

//Funci?n que desv?a todos los eventos de keypress dentro de un campo a una funci?n.
function pegaHandlers(){
	//colecci?n de campos text y textarea
	var elems=document.getElementsByTagName('input');
	var elemsT=document.getElementsByTagName('textarea');
	for(i=0;i<elems.length;i++){
		if((elems[i].type == 'text' || elems[i].type == 'textarea') && !elems[i].getAttribute('nouppercase')=='1'){
			elems[i].onchange=aMayusculas;
		}
		
		if(elems[i].type == 'text' || elems[i].type == 'textarea' || elems[i].type == 'file'){
			if(elems[i].getAttribute('filtro')){
				elems[i].onkeypress=manEvento;
			}
			if(elems[i].getAttribute('filtro')=='fecha'){
				//OJO cargar librerias de validacion fecha
				elems[i].onblur=validaFecha;								
			}
			if(elems[i].getAttribute('filtro')=='fechaVencimiento'){
				//OJO cargar librerias de validacion fecha
				elems[i].onblur=validaVencimiento;								
			}
		}
	}
                // Reingenieria Mako C. ID 48,51,53,54,57. Responsable:  FE. Fecha: 09-04-2014.  Descripción del cambio: Se agrega funcionalidad para campos tipo textarea
                for(j=0;j<elemsT.length;j++){
		if(elemsT[j].type == 'text' || elemsT[j].type == 'textarea' || elemsT[j].type == 'file'){
			if(elemsT[j].getAttribute('filtro')){
				elemsT[j].onkeypress=manEventoTA;
			}
		}
	}
                // Fecha: 09-04-2014 Fin
}

//Funci?n filtro para los eventos de keypress
function manEvento(ev){

	var tipoFiltro = this.getAttribute('filtro'); //Obtenemos el tipo de filtro para el campo
	ev=ev||event||null;  //Obtenemos el evento (cada q se presiona una tecla)

	// Si la tecla presionada es no caracter (enter, suprimir, backsp, tab, etc)
	noc=false; //ambito global
	
   	if (ev) {
		//alert(ev.modifiers);
		var cc=ev.charCode||ev.keyCode||ev.which; // ?Cu?l tecla se presiono?
		cc=Number(cc);
		if(ev.charCode===0 && ev.keyCode != 0 && ev.which==0){
			noc = true;
		}
		
		switch(tipoFiltro){ 
			case 'alfabetico': //a-z A-Z 
				if(esLetra(cc) || esEspacio(cc)){
					return true;
				}
			break;
			case 'numerico': // 0-9
				if(esNumero(cc)){
					return true;
				}
			break;
			case 'alfanumerico': //a-z A-Z : , . - ' # @ 
				if(esNumero(cc) || esLetra(cc) || esPuntuacion(cc) || esCalificativo(cc) || esGuion(cc) || esEspacio(cc)){return true;
				}
			break;
			/*
			Fecha: 20-06-2013. Inicio.
			Sugerencia/Acciones: Según matriz 4M RegSocios (Validar campos del domicilio, Validar campos del dom
			icilio).
			ID Seguimiento: 42,66,67.  
			Responsable: FE. Se valido que el campos solo acepte caracteres alfabéticos.
			*/
			case 'alfanumeric': //a-z A-Z : , . - ' # @ 
				if(esNumero(cc) || esLetra(cc) || esEspacio(cc) || esGuion(cc)){return true;
				}
			break;
                                                // Reingenieria Mako C. ID 47,49,50,52. Responsable:  FE. Fecha: 07-04-2014.  Descripción del cambio: Se crea el filtro texto, para validar letras, numero, puntuacion y guin bajo
			case 'texto': //a-z A-Z : , . - ' # @ 
				if(esNumero(cc) || esLetra(cc) || esEspacio(cc) || esPuntuacion(cc) || esGuion(cc)){return true;
				}
			break;
                                                // Fecha: 09-04-2014 Fin
			case 'correo': //a-z A-Z : , . - @ 
				if(esNumero(cc) || esLetra(cc) || esArroba(cc) || esGuion(cc) || esSuma(cc)){return true;
				}
			break;
			/*Fecha: 20-06-2013. Fin.*/
			/*
			Reingeniería PV | Responsable: FE. - Fecha: 23-10-2013. - ID: 2. - Campo: Nombre usuario.
			Descripción del cambio: Se crea el filtro usuario, para validar letras, numero y punto.
			*/
			case 'usuario': //a-z A-Z : . - 
				if(esNumero(cc) || esLetra(cc)){return true;
				}
			break;
			/*Fecha: 23-10-2013. - Fin*/
			case 'fecha': //0-9 - / 
				if(esEntero(cc) && filtraFecha(this,cc)){
					return true;
				}
			break;
			case 'fechaVencimiento': //0-9 - / 
				if(esNumero(cc) && filtraFecha(this,cc)){
					return true;
			}
			break;
			case 'hora': //0-9 : / 
				if(esEntero(cc) && filtraHora(this,cc)){
					return true;
				}
			break;
			case 'curp': //Clave "Unica" del Registro de Poblaci?n : / 
				if((esEntero(cc) || esLetra(cc)) && filtraCurp(this,cc)){
					return true;
				}
			break;
			case 'rfc': //Registro federal de contribuyentes 
				if((esEntero(cc) || esLetra(cc)) && filtraRFC(this,cc)){
					return true;
				}
			break;
			case 'tarjeta': //tarjeta de credito : / 
				if(esNumero(cc) && filtraTC(this,cc)){
					return true;
				}
			break;
			case 'expira_tc': // expiracion de tarjeta de credito : / 
				if(esNumero(cc) && filtraExpiraTC(this,cc)){
					return true;
				}
			break;
			case 'entero':
				if(esEntero(cc)){
					return true;
				}
			break;
			case 'razon_social':
				if(esLetra(cc) || esNumero(cc) || esEspacio(cc) || esGuion(cc) || esPuntuacion(cc) || esAmpersand(cc)){
					return true;
				}
			break;
			/* Reingeniería AP | Responsable: FE. - Fecha: 11-12-2013. - ID: 8,14,15,16. - Campo: Codigo, precio de lista, tiempo de uso en pc.
			Descripción del cambio: Se crearon los filtros para admin productos.*/  	
			case 'codigo':
				if(esNumero(cc) || esLetra(cc) || esGuion(cc) ){
					return true;
				}
			break;
                        case 'decimal':
                                if(esEntero(cc) || cc==46 ){
                                        return true;
                                }
                        break;
			/*Fecha: 11-12-2013. Fin.*/
			default:
			 return true;
			break;
		}
		if(cc==9 || cc==8 ){return true;}
		if((cc==35 || cc==36 || cc==37 || cc==38 || cc==39 || cc==40) && noc==true){return true;}
		return false;
	}
}

//Funci?n filtro para los eventos de keypress en TextAreas
function manEventoTA(ev){
	this.setAttribute('class','campo');
	var tipoFiltro = this.getAttribute('filtro'); //Obtenemos el tipo de filtro para el campo
	ev=ev||event||null;  //Obtenemos el evento (cada q se presiona una tecla)
	noc=false; //ambito global
	
   	if (ev) {
		//alert(ev.modifiers);
		var cc=ev.charCode||ev.keyCode||ev.which; // ?Cu?l tecla se presiono?
		cc=Number(cc);
		if(ev.charCode===0 && ev.keyCode != 0 && ev.which==0){
			noc = true;
		}
		switch(tipoFiltro){ 
			case 'alfabetico': //a-z A-Z 
				if(esLetra(cc) || esEspacio(cc)){
					return true;
				}
			break;
			case 'numerico': // 0-9
				if(esNumero(cc)){
					return true;
				}
			break;
			case 'alfanumerico': //a-z A-Z : , . - ' # @ 
				if(esNumero(cc) || esLetra(cc) || esPuntuacion(cc) || esCalificativo(cc) || esGuion(cc) || esEspacio(cc)){return true;
				}
			break;
			// Reingenieria Mako C. ID 48,51,53,54,57. Responsable:  FE. Fecha: 07-04-2014.  Descripción del cambio: Se crea el filtro texto, para validar letras, numero, puntuacion y guin bajo
			case 'texto': //a-z A-Z : , . - ' # @ 
				if(esNumero(cc) || esLetra(cc) || esEspacio(cc) || esPuntuacion(cc) || esGuion(cc)){return true;
				}
			// Fecha: 07-04-2014 Fin
			break;
			case 'fecha': //0-9 - / 
				if(esNumero(cc) && filtraFecha(this,cc)){
					return true;
				}
			break;
			case 'fechaVencimiento': //0-9 - / 
				if(esNumero(cc) && filtraFecha(this,cc)){
					return true;
			}
			break;
			case 'hora': //0-9 : / 
				if(esNumero(cc) && filtraHora(this,cc)){
					return true;
				}
			break;
			case 'curp': //Clave "Unica" del Registro de Poblaci?n : / 
				if((esNumero(cc) || esLetra(cc)) && filtraCurp(this,cc)){
					return true;
				}
			break;
			case 'tarjeta': //tarjeta de credito : / 
				if(esNumero(cc) && filtraTC(this,cc)){
					return true;
				}
			break;
			case 'razon_social':
				if(esLetra(cc) || esNumero(cc) || esEspacio(cc) || esGuion(cc) || esPuntuacion(cc) || esAmpersand(cc)){
					return true;
				}
			break;
			default:
			 return true;
			break;
		}
		if(cc==9 || cc==8 ){return true;}
		if((cc==35 || cc==36 || cc==37 || cc==38 || cc==39 || cc==40) && noc==true){return true;}
		return false;
                /*
		if(cc==9 || cc==8 || cc==46){return true;}
		if(cc==35 || cc==36){return true;}		
		if(cc==37 || cc==38 || cc==39 || cc==40){return true;}
		return false;
                */
	}
}
/* Filtra Formato de fecha */
function filtraFecha(f,cc){
	var x=String.fromCharCode(cc);
	var tam=f.value.length;
	var rd1=/[0-3]/;				
	var rd2=/[0-9]/;
	var rm1=/[0-1]/;
	var rm2=/[0-9]/;
	var ra1=/[1-2]/;
	var ra2=/[0-9]/;
	var ra3=/[0-9]/;
	var ra4=/[0-9]/;
	if(tam == 0){
		if(rd1.test(x)){
			return true;
		}else{return false;}
	}else if(tam==1){
		if(rd2.test(x)){
			return true;
		}else{return false;}
	}else if(tam == 2 || tam == 3){
		if(rm1.test(x)){
			if(f.value.charAt(2) != '-') {f.value=f.value + '-';}
			return true;
		}else{return false;}
	}else if(tam == 4){
		if(rm2.test(x)){
			return true;
		}else{return false;}
	}else if(tam == 5 || tam == 6){
		if(ra1.test(x)){
			if(f.value.charAt(5) != '-') {f.value=f.value + '-';}		
			return true;
		}else{return false;}
	}else if(tam==7){
		if(ra2.test(x)){
			return true;
		}else{return false;}
	}else if(tam == 8){
		if(ra3.test(x)){
			return true;
		}else{return false;}
	}else if(tam == 9){
		if(ra4.test(x)){
			return true;
		}else{return false;}
	}
}

/*Filtra horas v?lidas. solo en formato de 24 Hrs 00:00-23:59 */
function filtraHora(f,cc){
	var x=String.fromCharCode(cc);
	var tam=f.value.length;
	var rh1=/[0-2]/; //Horas decenas s?lo puede llegar el primer digito hasta 2 (23hrs)			
	var rh2=/[0-9]/; //Horas unidades puede ser de 0-9
	var rm1=/[0-5]/; //Minutos decenas de 0 - 5
	var rm2=/[0-9]/; //Minutos unidades de 0 - 9
	if(tam == 0){
		if(rh1.test(x)){
			return true;
		}else{return false;}
	}else if(tam==1){
		if((rh2.test(x) && f.value.charAt(0)==2 && x < 4) || (rh2.test(x) && f.value.charAt(0)< 2)) {
			return true;
		}else{return false;}
	}else if(tam == 2 || tam == 3){
		if(rm1.test(x)){
			if(f.value.charAt(2) != ':') {f.value=f.value + ':';}
			return true;
		}else{return false;}
	}else if(tam == 4){
		if(rm2.test(x)){
			return true;
		}else{return false;}
	}
}

/* Filtra CURP */
function filtraCurp(f,cc){
	var x=String.fromCharCode(cc);
	var tam=f.value.length;
	var rp1=/[a-zA-ZñÑ]/; //Letra inicial del primer apellido
	var rp2=/[aeiouAEIOU]/; //Primera vocal interna del primer apellido
	var rp3=/[a-zA-ZñÑ]/; //Primer caracter del segundo apellido, X en caso de no tener segundo ap
	var rp4=/[a-zA-ZñÑ]/; //Letra inicial del primer nombre de pila, en caso de ser compuesto con maria o jose, se toma el car, del 2? nombre

	var rp5=/[0-9]/; //Primer digito del a?o de nacimiento de 0 - 9
	var rp6=/[0-9]/; //Segundo digito del a?o de nacimiento
	var rp7=/[0-1]/; //primer d?gito del Mes de nacimiento 
	var rp8=/[0-9]/; //Segundo digito del mes de nacimiento
	var rp9=/[0-3]/; //Primer digito del dia de nacimiento
	var rp10=/[0-9]/; //Segundo digito del dia de nacimiento

	var rp11=/[hmHM]/; //Sexo H o M hombre o mujer

	var rp12=/[abcdghjmnopqstvyzABCDGHJMNOPQSTVYZ]/; //Primer caracter de la Ent. Fed. de nacimiento
	var rp13=/[sclmhfgtrnpzSCLMHFGTRNPZ]/; //Segundo caracter de la Ent. Fed. de nacimiento	
	
	var rp14=/[bcdfghjklmnñpqrstvwxyzBCDFGHJKLMNÑPQRSTVWXYZ]/; //Primera consonante interna del primer apellido
	var rp15=/[bcdfghjklmnñpqrstvwxyzBCDFGHJKLMNÑPQRSTVWXYZ]/; //Primera consonante interna del segundo apellido
	var rp16=/[bcdfghjklmnñpqrstvwxyzBCDFGHJKLMNÑPQRSTVWXYZ]/; //Primera consonante interna del primer nombre de pila		
	
	var rp17=/[a-zA-Z0-9]/; //Hasta el 31 de dic 1999 es numerico, despues es alfabetico, sirve para diferenciar homonimias
	
	var rp18=/[0-9]/; //Digito verificador.
	
	if(tam == 0){
		if(rp1.test(x)){
			return true;
		}else{return false;}
	}else if(tam==1){
		if(rp2.test(x)){
			return true;
		}else{return false;}
	}else if(tam == 2){
		if(rp3.test(x)){
			return true;
		}else{return false;}
	}else if(tam == 3){
		if(rp4.test(x)){
			return true;
		}else{return false;}
	}else if(tam == 4){
		if(rp5.test(x)){
			return true;
		}else{return false;}
	}else if(tam == 5){
		if(rp6.test(x)){
			return true;
		}else{return false;}
	}else if(tam == 6){
		if(rp7.test(x)){
			return true;
		}else{return false;}
	}else if(tam == 7){
		if((rp8.test(x) && f.value.charAt(6)==0) || (rp8.test(x) && f.value.charAt(6)==1 && x <= 2)){
			return true;
		}else{return false;}
	}else if(tam == 8){
		if(rp9.test(x)){
			return true;
		}else{return false;}
	}else if(tam == 9){
		if((rp10.test(x) && f.value.charAt(8) <= 2) || (rp10.test(x) && f.value.charAt(8) == 3 && x <=1)){
			return true;
		}else{return false;}
	}else if(tam == 10){
		if(rp11.test(x)){
			return true;
		}else{return false;}
	}else if(tam == 11){
		if(rp12.test(x)){
			return true;
		}else{return false;}
	}else if(tam == 12){
		if(rp13.test(x)){
			return true;
		}else{return false;}
	}else if(tam == 13){
		if(rp14.test(x)){
			return true;
		}else{return false;}
	}else if(tam == 14){
		if(rp15.test(x)){
			return true;
		}else{return false;}
	}else if(tam == 15){
		if(rp16.test(x)){
			return true;
		}else{return false;}
	}else if(tam == 16){
		if(rp17.test(x)){
			return true;
		}else{return false;}
	}else if(tam == 17){
		if(rp18.test(x)){
			return true;
		}else{return false;}
	}
}


/* Filtra RFC */
function filtraRFC(f,cc){
	var x=String.fromCharCode(cc);
	var tam=f.value.length;
	var rp1=/[a-zA-Z]/; //Letra inicial del primer apellido
	var rp2=/[a-zA-Z]/; //Primera vocal interna del primer apellido para personas fisicas, para morales puede ser cualquier letra del nombre de la empresa
	var rp3=/[a-zA-Z]/; //Primer caracter del segundo apellido, X en caso de no tener segundo ap
	var rp4=/[a-zA-Z0-9]/; //Letra inicial del primer nombre de pila, en caso de ser compuesto con maria o jose, se toma el car, del 2? nombre
	//En caso de personas morales el primer digito del año de constitución

	var rp5=/[0-9]/; //Primer digito del año de nacimiento de 0 - 9 o segundo del año de constitucion persona moral
	var rp6=/[0-9]/; //Segundo digito del año de nacimiento o primero del mes de constitucion persona moral
	var rp7=/[0-9]/; //primer digito del Mes de nacimiento o segundo del mes de constitucion persona moral
	var rp8=/[0-9]/; //Segundo digito del mes de nacimiento o primero del dia de constitucion persona moral
	var rp9=/[0-9]/; //Primer digito del dia de nacimiento o segundo del dia de constitucion persona moral
	var rp10=/[a-zA-Z0-9]/; //Segundo digito del dia de nacimiento o primer caracter de homoclave persona moral
	var rp11=/[a-zA-Z0-9]/; //Primer caracter de homoclave o segundo caracter de homoclave persona moral
	var rp12=/[a-zA-Z0-9]/; //Segundo caracter de homoclave o tercer caracter de homoclave persona moral
	var rp13=/[a-zA-Z0-9]/; //Tercer caracter de homoclave	

	if(tam == 0){
		if(rp1.test(x)){
			return true;
		}else{return false;}
	}else if(tam==1){
		if(rp2.test(x)){
			return true;
		}else{return false;}
	}else if(tam == 2){
		if(rp3.test(x)){
			return true;
		}else{return false;}
	}else if(tam == 3){
		if(rp4.test(x)){
			return true;
		}else{return false;}
	}else if(tam == 4){
		if(rp5.test(x)){
			return true;
		}else{return false;}
	}else if(tam == 5){
		if(rp6.test(x)){
			return true;
		}else{return false;}
	}else if(tam == 6){
		if(rp7.test(x)){
			return true;
		}else{return false;}
	}else if(tam == 7){
		if(rp8.test(x)){
			return true;
		}else{return false;}
	}else if(tam == 8){
		if(rp9.test(x)){
			return true;
		}else{return false;}
	}else if(tam == 9){
		if(rp10.test(x)){
			return true;
		}else{return false;}
	}else if(tam == 10){
		if(rp11.test(x)){
			return true;
		}else{return false;}
	}else if(tam == 11){
		if(rp12.test(x)){
			return true;
		}else{return false;}
	}else if(tam == 12){
		if(rp13.test(x)){
			return true;
		}else{return false;}
	}
}


/*Filtra numeros de tarjeta */
function filtraTC(f,cc){
	var x=String.fromCharCode(cc);
	var tam=f.value.length;
	var rh1=/[0-9]/; //Solo digitos del 0 al 9
	if(tam < 4){
		if(rh1.test(x)){
			return true;
		}else{return false;}
	}else if(tam==4){
		if(rh1.test(x)){
			if(f.value.charAt(4) != '-') {f.value=f.value + '-';}
			return true;
		}else{return false;}
	}else if(tam > 4 && tam < 9){
		if(rh1.test(x)){
			return true;
		}else{return false;}
	}else if(tam==9){
		if(rh1.test(x)){
			if(f.value.charAt(9) != '-') {f.value=f.value + '-';}
			return true;
		}else{return false;}
	}else if(tam > 9 && tam < 14){
		if(rh1.test(x)){
			return true;
		}else{return false;}
	}else if(tam==14){
		if(rh1.test(x)){
			if(f.value.charAt(14) != '-') {f.value=f.value + '-';}
			return true;
		}else{return false;}
	}else if(tam > 14 && tam < 19){
		if(rh1.test(x)){
			return true;
		}else{return false;}
	}
	
}
/* Filtro de fecha de expiracion de una tarjeta */
function filtraExpiraTC(f,cc){
	var x=String.fromCharCode(cc);
	var tam=f.value.length;
	var rh1=/[0-1]/; //unidades del mes
	var rh2=/[0-9]/; //decenas del mes (puede ser hasta nueve si las unidades es cero)
	var rm1=/[0-1]/; //a?o, 0-9
	var rm2=/[0-9]/; //a?o, 0-9
	if(tam == 0){
		if(rh1.test(x)){
			return true;
		}else{return false;}
	}else if(tam==1){
		if((rh2.test(x) && f.value.charAt(0)==0) || (f.value.charAt(0)== 1 && x < 3)) {
			return true;
		}else{return false;}
	}else if(tam == 2 || tam == 3){
		if(rm1.test(x)){
			if(f.value.charAt(2) != '-') {f.value=f.value + '-';}
			return true;
		}else{return false;}
	}else if(tam == 4){
		if(rm2.test(x)){
			return true;
		}else{return false;}
	}
}


function esLetra(cc){
	if(cc >= 65 && cc <=90){ // mayusculas
		return(1);
	}
	if(cc >= 97 && cc <=122){ //minusculas
		return(1);
	}
	if(cc == 225 || cc == 233 || cc == 237 || cc == 243 || cc == 250){ // min acento
		return(1);
	}
	if(cc == 193 || cc == 201 || cc == 205 || cc == 211 || cc == 218){ // may acento
		return(1);
	}
	if(cc == 252 || cc == 220){ // u dieresis
		return(1);
	}
	if(cc == 209 || cc == 241){ // ??
		return(1);
	}
	return(0);
}

function esNumero(cc){
	if(cc >=48 && cc <= 57 || cc == 46 || cc==45){
		return(1);
	}else{
		return(0);
	}
}

/**
 * Determina si el valor de la tecla oprimida es un numero entero.
 * @param {Object} cc
 */
function esEntero(cc){
	if((cc >=48 && cc <= 57) || noc==true){
		return true;
	}else{
		return false;
	}
}


function esPuntuacion(cc){
	if(cc == 44 || cc == 46 || cc == 58 || cc == 59){ // ,;:.
		return(1);
	}else{
		return(0);
	}
}

function esCalificativo(cc){ // !? ??
	if(cc == 63 || cc == 191 || cc == 33 || cc == 161){
		return(1);
	}else{
		return(0);
	}
}

function esGuion(cc){ // -_
	if(cc == 45 || cc == 95){
		return(1);
	}else{
		return(0);
	}
}

function esEspacio(cc){ // -_
	if(cc == 32){
		return(1);
	}else{
		return(0);
	}
}

function esSuprimir(cc){ // -_
	if(cc == 32 || cc == 46){
		return(1);
	}else{
		return(0);
	}
}

function esArroba(cc){
	if(cc == 64){
		return(1);
	}else{
		return(0);
	}
}

function esAmpersand(cc){
	if(cc == 38){
		return(1);
	}else{
		return(0);
	}
}

/*
Fecha: 10-07-2013. Inicio.
Sugerencia/Acciones: Según matriz 4M RegSocios (Validar campo E-mail para que no acepte caracteres iguales 
continuos (p.e xxxx@dominio.com)).
ID Seguimiento: F2_16.  
Responsable: FE. Se valido correo para que solo acepte + - . 0-9 _ a-z A-Z.
*/
function esSuma(cc){
	if(cc == 43){
		return(1);
	}else{
		return(0);
	}
}
/*
Fecha: 10-07-2013. Fin
*/
function manDown(ev){
	ev=ev||event||null;  //Obtenemos el evento (cada q se presiona una tecla)
   	if (ev) {
		var cc=ev.charCode||ev.keyCode||ev.which; // ?Cu?l tecla se presiono?
		cc=Number(cc);
		imprime(cc);
	}
}

function manUp(ev){
	var cad=this.value;
	cad.replace(/./gi, ""); 
	this.value=cad;
}

function imprime(cc,noc){
	t=document.getElementById('msg');
	t.innerHTML += cc + ' -> ' + String.fromCharCode(cc) + 'NOC: '+noc+'</br>';
	//ch.value="";
	s="";
}

function aMayusculas(){

	var cad =this.value;
	cad = cad.replace(/[áàÁÀ]/gi, "A");
	cad = cad.replace(/[éèÉÈ]/gi, "E");
	cad = cad.replace(/[íìÍÌ]/gi, "I");
	cad = cad.replace(/[óòÓÒ]/gi, "O");
	cad = cad.replace(/[úùüÚÙÜ]/gi, "U");
	cad = cad.replace(/[Ññ]/gi, "Ñ");	
	this.value=cad.toUpperCase();
	//TODO: Implementar trim
	//this.value=cad.TrimLeft();
	//this.value=cad.TrimRight();
}

function abreError(elemento) {
	console.log('abreError');
	console.log(elemento);
	node = $(elemento);
	console.log(node);
	console.log(node.get(0).tagName);
	console.log(node.attr('type'));
	
	
	if(node.get(0).tagName == 'INPUT' || node.get(0).tagName == 'input' || node.get(0).tagName == 'SELECT' || node.get(0).tagName == 'select'){
		if(node.attr('type') == 'TEXT' || node.attr('type') == 'text' || node.get(0).tagName == 'SELECT' || node.get(0).tagName == 'select')
			node.css('border-color','red').css('border-width','2px');
		if(node.attr('type') == 'RADIO' || node.attr('type') == 'radio' )
			node.parent().parent().css('border','2px solid red');
	}else{
			node.css('border','2px solid red');
	}
	//node.css('border-color','red');
/*
//	elemento.style.background='#fff url(./estilo/default/warning.gif) no-repeat bottom right;
	pos = posicion(elemento);
	var d = document.createElement("div");
	document.getElementsByTagName("body")[0].appendChild(d);	
	//d.style.background='url(./estilo/default/warning.gif) no-repeat right';
	d.innerHTML="<h3>&nbsp;&nbsp;&nbsp;&nbsp;</h3>";
	d.style.position="absolute";
	d.style.left=pos.left+"px";
	d.style.top=pos.top+"px";
*/
}
function cierraError(elemento) {
	console.log('Cerramos 1');
	console.log(elemento);
	node = $(elemento);
	if(node.get(0).tagName == 'INPUT' || node.get(0).tagName == 'input' || node.get(0).tagName == 'SELECT' || node.get(0).tagName == 'select'){
		console.log('Cerramos 2');
		if(node.attr('type') == 'TEXT' || node.attr('type') == 'text' || node.get(0).tagName == 'SELECT' || node.get(0).tagName == 'select')
			console.log('Cerramos 3');
			node.css('border-color','#bdc7d8');
		if(node.attr('type') == 'RADIO' || node.attr('type') == 'radio' )
			node.parent().parent().css('border','');
	}else{
			node.css('border','');
	}
	console.log('Cerramos 1');
}

function posicion(element){
	var left=0,top=0;
	while (element!=null){
		left+=element.offsetLeft-element.scrollLeft;
		top+=element.offsetTop-element.scrollTop;
		
		element=element.offsetParent;
	}
	return {top:top,left:left};
}

/* Verificando la validez del CURP mediante el algoritmo de LUHN con caracteres sustituidos. */
function luhn(curp){
	var esNumero=/\d/;
	document.getElementById('res').innerHTML='Evaluando '+curp+'<br>';
	var map = new Array();
	map['A']=10;map['B']=11;map['C']=12;map['D']=13;map['E']=14;map['F']=15;map['G']=16;
	map['H']=17;map['I']=18;map['J']=19;map['K']=20;map['L']=21;map['M']=22;map['N']=23;
	/*map['?']=24;*/ map['O']=24;map['P']=25;map['Q']=26;map['R']=27;map['S']=28;map['T']=29;
	map['U']=30;map['V']=31;map['W']=32;map['X']=33;map['Y']=34;map['Z']=35;
	/*map['A']=1; map['B']=2; map['C']=3; map['D']=4; map['E']=5; map['F']=6; map['G']=7;
	map['H']=8; map['I']=9; map['J']=1; map['K']=2; map['L']=3; map['M']=4; map['N']=5;
	map['?']=6; map['O']=7; map['P']=8; map['Q']=9;	map['R']=1; map['S']=2; map['T']=3;
	map['U']=4; map['V']=5; map['W']=6; map['X']=7;	map['Y']=8; map['Z']=9;*/
	
	var flip = true;
	document.getElementById('res').innerHTML='long '+curp.length+'<br>';
	var suma = 0;
	for(i=curp.length;i>=1;i--){
		flip = !flip;
		
		if(flip){
			if(!esNumero.test(curp.charAt(i))){evaluando = map[curp.charAt(i)];}else{evaluando = curp.charAt(i);}
			digito = evaluando*2;
			dig = digito;
			sp=0;
			while (digito) {
				sp += digito % 10;
      			suma += digito % 10;
      			digito = parseInt(digito / 10);
			}
			
			document.getElementById('res').innerHTML=document.getElementById('res').innerHTML + 'Evaluando '+curp.charAt(i) + ' como '+ evaluando + ' En '+i+' -> '+ dig+' parcial '+sp+' -> suma '+suma+'<br>' ;
		}
	}
	var valido = (suma % 10==0);
	document.getElementById('res').innerHTML=document.getElementById('res').innerHTML + "Es Curp? "+ valido;
}


//melinka
function comparaCampos(campo1, campo2){
	
	if (campo1 != campo2 ){
		alert('Las contraseñas no coinciden, por favor capturelas nuevamente');
		return false;
	}else {
		return true;
	}
}
