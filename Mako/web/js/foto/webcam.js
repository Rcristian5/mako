/* Libreria para tomar foto y subirla al srcript del servidor. */

/* Usage:
	<script language="JavaScript">
		document.write( webcam.get_html(320, 240) );
		webcam.set_api_url( 'test.php' );
		webcam.set_hook( 'onComplete', 'my_callback_function' );
		function my_callback_function(response) {
			alert("Success! PHP returned: " + response);
		}
	</script>
	<a href="javascript:void(webcam.snap())">Take Snapshot</a>
*/
$(document).ready(function() 
{

// Everything is under a 'webcam' Namespace
window.webcam = {
	// globals
	ie: !!navigator.userAgent.match(/MSIE/),
	protocol: location.protocol.match(/https/i) ? 'https' : 'http',
	callback: null, // user callback for completed uploads
	swf_url: '/js/foto/webcam.swf', // URI to webcam.swf movie (defaults to cwd)
	shutter_url: '/imgs/shutter.mp3', // URI to shutter.mp3 sound
	api_url: '../comunes/foto/foto.php', // URL to upload script
	loaded: false, // true when webcam movie finishes loading
	quality: 100, // JPEG quality (1 - 100)
	shutter_sound: false, // shutter sound effect on/off
	hooks: {
		onLoad: null,
		onComplete: null,
		onError: null
	}, // callback
	
	set_hook: function(name, callback) {
		if (typeof(this.hooks[name]) == 'undefined')
			return alert("Hook no soportado: " + name);
		
		this.hooks[name] = callback;
	},
	
	fire_hook: function(name, value) {
		// Dispara gancho
		if (this.hooks[name]) {
			if (typeof(this.hooks[name]) == 'function') 
			{

				this.hooks[name](value);
			}
			else if (typeof(this.hooks[name]) == 'array') 
			{
				this.hooks[name][0][this.hooks[name][1]](value);
			}
			else if (window[this.hooks[name]]) {
				
				window[ this.hooks[name] ](value);
			}
			return true;
		}
		return false; 
	},
	
	set_api_url: function(url) 
	{

		this.api_url = url;
	},
	
	set_swf_url: function(url) 
	{
		this.swf_url = url;
	},
	
	get_html: function(width, height) {
		// Return HTML for embedding webcam capture movie
		// Specify pixel width and height (640x480, 320x240, etc.)
		var html = '';
		var flashvars = 'shutter_enabled=' + (this.shutter_sound ? 1 : 0) + 
			'&shutter_url=' + escape(this.shutter_url) + 
			'&width=' + width + 
			'&height=' + height;
		
		if (this.ie) {
			html += '<div id="cont_foto" style="display:none;"><object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="'+this.protocol+'://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,0,0" width="'+width+'" height="'+height+'" id="webcam_movie" align="middle"><param name="allowScriptAccess" value="sameDomain" /><param name="allowFullScreen" value="false" /><param name="movie" value="'+this.swf_url+'" /><param name="loop" value="false" /><param name="menu" value="false" /><param name="quality" value="best" /><param name="bgcolor" value="#ffffff" /><param name="flashvars" value="'+flashvars+'"/></object></div>';
		}
		else {
			html += '<div id="cont_foto" style="display:none;"><embed id="webcam_movie" src="'+this.swf_url+'" loop="false" menu="false" quality="best" bgcolor="#ffffff" width="'+width+'" height="'+height+'" name="webcam_movie" align="middle" allowScriptAccess="sameDomain" allowFullScreen="false" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" flashvars="'+flashvars+'" /></div>';
		}
		
		this.loaded = false;
		return html;
	},
	
	get_movie: function() {
		// get reference to movie object/embed in DOM
		if (!this.loaded) return alert("ERROR: Movie is not loaded yet");
		var movie = document.getElementById('webcam_movie');
		if (!movie) alert("ERROR: no se puede localizar el 'webcam_movie' en el DOM");
		return movie;
	},
	
	snap: function(url, callback) {
		// take snapshot and send to server
		// specify fully-qualified URL to server API script
		// and callback function (string or function object)
		if (callback) this.set_hook('onComplete', callback);
		if (url) this.set_api_url(url);
		
		this.get_movie()._snap( this.api_url, this.quality, this.shutter_sound ? 1 : 0 );
	},
	
	configure: function(panel) {
		// open flash configuration panel -- specify tab name:
		// "camera", "privacy", "default", "localStorage", "microphone", "settingsManager"
		if (!panel) panel = "camera";
		this.get_movie()._configure(panel);
	},
	
	set_quality: function(new_quality) {
		// set the JPEG quality (1 - 100)
		// default is 90
		this.quality = new_quality;
	},
	
	set_shutter_sound: function(enabled, url) {
		// enable or disable the shutter sound effect
		// defaults to enabled
		this.shutter_sound = enabled;
		this.shutter_url = url ? url : 'shutter.mp3';
	},
	
	flash_notify: function(type, msg) {
		// receive notification from flash about event
		
		switch (type) {
			case 'flashLoadComplete':
				// movie loaded successfully
				this.loaded = true;
				this.fire_hook('onLoad');
				break;

			case 'error':
				// HTTP POST error most likely
				if (!this.fire_hook('onError', msg)) {
					alert("JPEGCam Flash Error: " + msg);
				}
				break;

			case 'success':
				// upload complete, execute user callback function
				// and pass raw API script results to function
				//this.fire_hook('onComplete', msg.toString());
				$(document).trigger('foto.tomada', [msg.toString()]);
				break;

			default:
				// catch-all, just in case
				alert("jpegcam flash_notify: " + type + ": " + msg);
				break;
		}
	}
};

});