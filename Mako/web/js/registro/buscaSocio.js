		/*Reingeniería PV | Responsable: FE. - Fecha: 23-10-2013. - ID: 4,6. - Campo: Nombre usuario,NombreCompleto.
		Descripción del cambio: Se crea archivo ./web/js/buscaSocio.js para hacer validaciones de búsqueda de socio.*/

		function espacio_izquierdo(campo){
			while (campo.charAt(0) == " ")
			campo = campo.substr(1, campo.length - 1);
			return campo;
		}
		function espacio_derecho(campo){
			while (campo.charAt(campo.length - 1) == " ")
			campo = campo.substr(0, campo.length - 1);
			return campo;
		}
		function quitar_espacios(campo){
			return espacio_derecho(espacio_izquierdo(campo));
		}
		function valida_espacios(campo){
			campo.value=quitar_espacios(campo.value);
		}
		function validaCampo(campo,e){
			var cc=e.charCode||e.keyCode||e.which;
			//var cc = (e.keyCode ? e.keyCode : e.which);
			cc=Number(cc);
	        	tamanioCampo=String(campo.value).length;
			if(tamanioCampo>0){
                                if(String(campo.name)!='qFolio')
                                        $( "input[name='qFolio']" ).attr("disabled","disabled");
                                if(String(campo.name)!='qUsuario')
                                        $( "input[name='qUsuario']" ).attr("disabled","disabled");
                                if(String(campo.name)!='qNombre')
                                        $( "input[name='qNombre']" ).attr("disabled","disabled");

                                if(String(campo.name)=='qFolio')
                                        if(!cc==37 || !cc==39)valida_espacios(campo);
                                if(String(campo.name)=='qUsuario')
                                        if(!cc==37 || !cc==39)valida_espacios(campo);
                                if(String(campo.name)=='qNombre'){
                                        RegExPattern=/^[\w]+(\s[\w]+)*\s$/i;
                                        if (!campo.value.match(RegExPattern)) {
                                                        while (campo.value.charAt(campo.value.length - 1) == " ")
                                                        campo.value = (espacio_derecho(campo.value));
                                        }
                                }
                        }
                        else{
                                $( "input[name='qFolio']" ).removeAttr("disabled");
                                $( "input[name='qUsuario']" ).removeAttr("disabled");
                                $( "input[name='qNombre']" ).removeAttr("disabled");
                        }
                        return false;
		}

		/*Fecha: 23-10-2013. - Fin*/
