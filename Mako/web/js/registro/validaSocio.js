$(function(){
    /**
     * Para cuando no hay colonia en Edit Socio 
     * Si no hay colonia seleccionada, se preseleciona la
     * opcion de Pendiente, debido a que no existe la 
     * colonia en nuestra base de Datos
     */
    console.log('Colonias******');

    socio_clave  = $("#socio_clave").val();
    socio_id_estado  = $('#socio_id_estado').val();
    id_municipio  = $('#id_municipio').val();
    socio_municipio  = $('#socio_municipio').val();
    id_colonia  = $('#id_colonia').val();
    socio_colonia  = $('#socio_colonia').val();

    console.log('socio_clave '+ socio_clave);
    console.log('socio_id_estado ' + socio_id_estado);
    console.log('id_municipio' + id_municipio);
    console.log('socio_municipio' + socio_municipio);
    console.log('id_colonia' + id_colonia);
    console.log('socio_colonia' + socio_colonia);

    if(typeof socio_clave != 'undefined' ){
         if( (socio_id_estado.length != 0  ||  socio_id_estado.length != 0 ) 
          && (id_municipio.length != 0 ||  socio_municipio.length != 0 )
           && (socio_colonia.length == 0  )
         ){
            console.log('colonia vacia');
            $("#id_colonia").attr('disabled','disabled').removeAttr('requerido');
            $('#socio_tr_clna'). attr( "checked", true );
        }
    }

    $('#socio_fecha_nac_day, #socio_fecha_nac_month, #socio_fecha_nac_year').change(function(){
        var day = $('#socio_fecha_nac_day'),
            month = $('#socio_fecha_nac_month'),
            year  = $('#socio_fecha_nac_year');

        if(day.val() != '' && month.val() != '' && year.val() != '') {
            var d   = new Date(year.val(), parseInt(month.val()) - 1, day.val()),
                age = (~~((Date.now() - d) / (31557600000)));

            if(age < 18) {
                $('#socio_tutor_responsable').removeAttr('disabled');
            } else {
                $('#socio_tutor_responsable').attr('disabled', true).val('');
            }
        }
    });

    $('#socio_fecha_nac_day, #socio_fecha_nac_month, #socio_fecha_nac_year').change();

        function replaceRequiredLabel(){
            $('.RequiredLabel').each(function(){
              $( this ).append(' <img src="/css/obligatorio.png"/>');
               console.log(this);
            });
        }
        replaceRequiredLabel();
});
        var process = true;

        /**
         * Hace las validaciones  de requeridos y tipos de datos.
         * Si todo está bien manda a verificar el folio con una llamada asincrona al servidor. Si todo sale bien esta función hace submit de la forma.
         */
        function comparaRequeridos(){
            $("#boton-guardar").hide();
            $("#boton-cancelar").hide();


            //if (requeridos() && validaTels() && validaFolio()) {
            //ToDo: checar con Edgar la validacion del folio
            if (requeridos() && validaTels() && validaFolio2() &&  validaCURP() &&  validaaccesotecnología() && validaOcupacion() && validaDiscapacidad() && verificaFoto()) {
               /* if (comparaCampos(document.getElementById('clave').value, document.getElementById('claveCompara').value)) {
                    //verificaFolio();
                    return false;
                }
                else 
                    return true;
                */
				return true;

            }
            else
            {
                $("#boton-guardar").show();
                $("#boton-cancelar").show();
                return false;
            }

            $("#boton-guardar").show();
            $("#boton-cancelar").show();
            return false;
        }

        function validaOcupacion(){
            socio_socio_ocupacion_1       = $("#socio_socio_ocupacion_1").is(':checked');
            socio_socio_ocupacion_3       = $("#socio_socio_ocupacion_3").is(':checked');
            socio_socio_ocupacion_4       = $("#socio_socio_ocupacion_4").is(':checked');
            socio_socio_ocupacion_5       = $("#socio_socio_ocupacion_5").is(':checked');
            socio_socio_ocupacion_6       = $("#socio_socio_ocupacion_6").is(':checked');
            socio_socio_ocupacion_7       = $("#socio_socio_ocupacion_7").is(':checked');
            socio_socio_ocupacion_8       = $("#socio_socio_ocupacion_8").is(':checked');
            socio_socio_ocupacion_N       = $("#socio_socio_ocupacion_N").is(':checked');

            socio_socio_ocupacion_1 || socio_socio_ocupacion_3 || socio_socio_ocupacion_4 || socio_socio_ocupacion_5 || socio_socio_ocupacion_6 || socio_socio_ocupacion_7 || socio_socio_ocupacion_8 || socio_socio_ocupacion_N

            if(socio_socio_ocupacion_1 || socio_socio_ocupacion_3 || socio_socio_ocupacion_4 || socio_socio_ocupacion_5 || socio_socio_ocupacion_6 || socio_socio_ocupacion_7 || socio_socio_ocupacion_8 || socio_socio_ocupacion_N){
                cierraError(document.getElementById('OcupacionTD'));
                return true;
            }
            else{
                abreError(document.getElementById('OcupacionTD'));
                alert('Debe seleccionar al menos una opción en ¿A qué se dedica?.');
            }
            return false;
        }

        function validaDiscapacidad(){
            socio_socio_discapacidad_2       = $("#socio_socio_discapacidad_2").is(':checked');
	        socio_socio_discapacidad_4       = $("#socio_socio_discapacidad_4").is(':checked');
            socio_socio_discapacidad_5       = $("#socio_socio_discapacidad_5").is(':checked');
            socio_socio_discapacidad_3       = $("#socio_socio_discapacidad_3").is(':checked');
            socio_socio_discapacidad_N       = $("#socio_socio_discapacidad_N").is(':checked');

            if(socio_socio_discapacidad_2 || socio_socio_discapacidad_4 ||  socio_socio_discapacidad_5 || socio_socio_discapacidad_3 || socio_socio_discapacidad_N ){
                cierraError(document.getElementById('DiscapacidadTD'));
                return true;
            }
            else{
                abreError(document.getElementById('DiscapacidadTD'));
                alert('Debe seleccionar al menos una opción en Discapacidad.');
            }
            return false;
        }

        /** ES-Termina función nueva**/

        /**
         * Despliega el control de toma de fotografia
         */
        var foto = false; //foto del presentado
        function muestraControlFoto(){

            if (foto == true) {
                if (!confirm('Ya se ha capturado el indice de la mano ' + dedo + '. ¿Desea capturarlo nuevamente?')) {
                    return false;
                }
            }

            var cont = document.getElementById('cont_foto');
            //alert(document.getElementById('img_capturar_foto').src);
            cont.style.display = '';
        }

    /*
        Fecha: 24-06-2013. Inicio.
        Sugerencia/Acciones: Según matriz 4M RegSocios Fase 2.
        ID Seguimiento: 19
        Responsable: ES. Se crea la función para validar el folio de la credencial, debe de iniciar con 99 y long de 13.
        */
    function validaFolio2(){
        var folio = document.getElementById('socio_folio');
        var prefijo_folio = folio.value.substring(0, 2);
        if (folio.value.length<13){
            alert ('El folio de la credencial que está capturando no es válido por favor ingrese un folio correcto.');
            folio.focus();
            return false;
        }
        return true;

    }
        /**
         * Valida numeros telefonicos
         */ 
            /*
            * Reingenieria  Mako Registro Socios. ID #7
            * Responsable:  (BP) Bet Gader Porcayo Juárez
            * Fecha:        29-08-2014 
            * Descripción:  Valida que el usuario al menos escriba un número telefónico
            */
        function validaTels(){
            var tel = document.getElementById('socio_tel');
            var cel = document.getElementById('socio_celular');
            console.log('validaTels');
            if(tel.value == '' && cel.value == ''){
                $('#socio_tel').css('border-color', 'red').css('border-width','2px');
                $('#socio_celular').css('border-color', 'red').css('border-width','2px');
                tel.focus();
                alert('Debe ingresar al menos un número telefónico');
                return false;
            }
            console.log('Ha ingresado al menos un número');
            if((tel.value == cel.value)&& (tel.value != '' && cel.value != '')){
                $('#socio_tel').css('border-color', 'red').css('border-width','2px');
                $('#socio_celular').css('border-color', 'red').css('border-width','2px');
                tel.focus();
                alert('Los números telefónicos que ingresó son iguales, por favor solo ingrese uno.');
                return false;
            }
            console.log('Numeros diferentes');
            if(tel.value != ''){
                if(tel.value.length < 10){
                    $('#socio_tel').css('border-color', 'red').css('border-width','2px');
                    $('#socio_celular').css('border-color', '').css('border-width','1px');
                    tel.focus();
                    alert('El número telefonico que ingresó no es válido. Porfavor si no existe el número deje el campo vacío, no intente capturar nada.');
                    return false;
                }
                console.log('Telefono válido');
            }
            if(cel.value != ''){
                if(cel.value.length < 10){
                    $('#socio_tel').css('border-color', '').css('border-width','1px');
                    $('#socio_celular').css('border-color', 'red').css('border-width','2px');
                    cel.focus();
                    alert('El número de teléfono celular que ingresó no es válido. Porfavor si no existe el número deje el campo vacío, no intente capturar nada.');
                    return false;
                }
                console.log('Celular válido');
            }
            $('#socio_tel').css('border-color', '').css('border-width','1px');
            $('#socio_celular').css('border-color', '').css('border-width','1px');
            return true;
        }
        /*
        Fecha: 24-06-2013. Inicio.
        Sugerencia/Acciones: Según matriz 4M RegSocios (Revisar librerías para la validación de correo electróni
        co).
        ID Seguimiento: 38.
        Responsable: FE. Se crea la función para validar una dirección email con un formato valido.
        */
        /**
         * Valida correo electrónico
         */
        function validaEmail(){
            var email = document.getElementById('socio_email');
            var exr   = /^[0-9a-z_\-\.]+@[0-9a-z\-\.]+\.[a-z]{2,4}$/i;

            email.value = email.value.replace(/ /g, '');

            if(email.value != ''){
                if (!exr.test(email.value)){
                    alert('El correo electrónico que ingresó no es válido.');
                    abreError(document.getElementById('socio_email'));
                    email.focus();
                    return false;
                }
            }
            cierraError(document.getElementById('socio_email'));
            return true;
        }

        /*
        Fecha: 24-06-2013. Fin.
        */
        /*
        Fecha: 25-06-2013. Inicio.
        Sugerencia/Acciones: Según matriz 4M RegSocios (-Verificar el número de dígitos en los números telefónic
        os y su formato en general. -Definir el tamaño del campo (con o sin 044). Investigar la forma como se ma
        neja el número celular en cada estado, y países colindantes.)
        ID Seguimiento: 36,37.
        Responsable: FE. Se crean las funciones para validar el teléfono que tenga un formato valido.
        */
        /**
        * Valida Lada
        */
        function validaLada(numtelefono,cadena,lada){
            /*
                De acuerdo a lo solicitado para Registro de socios, se quita la validacion de lada,
                por lo la validacion para el telefono solo constará de numeros sin espacios ni signos,
                deberá ser mayor a 10 numeros
                Responsable: BP

            */
            telefono = $(numtelefono);

            /*
                Primero verificamos si es vacio
            */
            if(telefono.val() == ''){
                return true;
            }


            /*
               Verificamos si son puros numeros
            */
            var isnum = /^\d+$/.test(telefono.val());
  
              if(!isnum){
                alert(cadena+'Favor de capturar solo números.');
                return false;
              }

            /*
               Ahora validamos si es diferente de 10
            */
            if(telefono.val().length != 10){
                alert(cadena+'Favor de capturarlo a 10 dígitos.');
                //telefono.focus();
                return false;
            }
            return true;
        }

        function ValidaSoloNumeros() {
         if ((event.keyCode < 48) || (event.keyCode > 57)) 
          event.returnValue = false;
        }

        /*
        * Valida Teléfono
        */
        function validaTelefono(){
                var tel = document.getElementById('socio_tel');
                validaLada(tel,'El número telefonico que ingresó no es válido. ','01 ,')
        }
        /**
        * Valida Celular
        */
        function validaCelular(){
                var celular = document.getElementById('socio_celular');
                validaLada(celular,'El número de teléfono celular que ingresó no es válido. ','045 ,044 ');
        }
        /*
        Fecha: 25-06-2013. Fin.
        /**
        * Valida longitud de folio de credencial
         */
        function validaFolio(){
            var folio = document.getElementById('socio_folio');
            if(folio.value.length < 13)
            {
                alert('El folio de la credencial que está capturando no es válido porfavor ingrese un folio correcto.');
                folio.focus();
                return false;
            }
            return true;
        }

        /*
        Fecha: 01/10/2014
        /**
        * Valida cupr si se ha 
         */
        function validaCURP(){
            curp_nombre = $('#curp_nombre').val(); 
            curp_fecha = $('#curp_fecha').val(); 
            curp_genero = $('#curp_genero').val(); 
            curp_estado = $('#curp_estado').val(); 
            curp_cnombre = $('#curp_cnombre').val(); 
            curp_codigo = $('#curp_codigo').val();

            console.log('Los datos del cupr son');
            console.log(curp_nombre);
            console.log(curp_fecha);
            console.log(curp_genero);
            console.log(curp_estado);
            console.log(curp_cnombre);
            console.log(curp_codigo);
            if( curp_nombre != '' || curp_fecha != '' || curp_genero != '' || curp_estado != '' || curp_cnombre != '' || curp_codigo ){
                if( curp_nombre != '' && curp_fecha != '' && curp_genero != '' && curp_estado != '' && curp_cnombre != '' &&  curp_codigo ){
                    curpCompleto = (curp_nombre + curp_fecha + curp_genero + curp_estado + curp_cnombre + curp_codigo );
                    $('#socio_curp').val(curpCompleto);
                    return true;
                }else{
                    alert('Hay campos en el curp sin llenar.');
                    return false;
                }
            }
            return true;
        }
        /*
        * Medio de acercamiento
        */
        /*
        * Fecha: 24-06-2013. Inicio.
        * Sugerencia/Acciones: Según matriz 4M RegSocios (Medio de acercamiento – Recomendación – Socio – Buscador de socio. Limpiar campos después de realizar cada búsqueda.).
        * ID Seguimiento: 86.
        * Responsable: YD. Se agrega metodos de acceso a los elementos para deshabilitar o habilitar función de busqueda de socios.
        */
        // Habilita selects dependiendo del valor del canal de contacto
        function acercamiento(canalContacto, socioRecomiendaId){
            //Deshabilita todos los campos
            toggleDisabled(document.getElementById("acercamientoCampos"));

            if(canalContacto != 8) {
                document.getElementById('socio_otros_contacto').disabled = true;
                document.getElementById('socio_otros_contacto').value    = '';
            }

            //Promotor
            if(canalContacto  == 5){
                document.getElementById('autocomplete_socio_promotor_ref_id').removeAttribute('disabled');
                document.getElementById('socio_promotor_ref_id').removeAttribute('disabled');
            }
            //Sitio Web
            else if(canalContacto  == 6){
                document.getElementById('socio_liga').removeAttribute('disabled');
            }
            //Recomendacion
            else if(canalContacto  == 7){
                document.getElementById('socio_recomienda_id').removeAttribute('disabled');

                //amigo default
                if(socioRecomiendaId == null) {
                    document.getElementById('socio_recomienda_id').options[1].selected = true;
                }
                else{
                    document.getElementById('socio_recomienda_id').options[socioRecomiendaId].selected = true;
                }
            }

            if (canalContacto  != 7){
                document.getElementById('recomendo_socio').style.display='none';
                document.getElementById('socio_ref_nombre').style.display='none';
            }

            //Fecha: 24-06-2013. Inicio.
                //Acciones: Según matriz 4M RegSocios (Medio de acercamiento – Recomendación – Habilitar opción "Otros".
            //ID Seguimiento: F2_18.
                //Responsable: ES.

            // Otro
            if (canalContacto == 8){
                document.getElementById('socio_otros_contacto').removeAttribute('disabled');
            }
            //Fin F2_18

            // Asegurarse que el motivo de capacitación y acercamiento SIEMPRE estan habilitados
            document.getElementById('socio_id_motivo_acercamiento').removeAttribute('disabled');
            document.getElementById('socio_id_motivo_capacitacion').removeAttribute('disabled');

        }

        // Habilita selects dependiendo del valor del canal de contacto select Recomendó
        function acercamientoRecomienda(recomendo){
            //deshabilita todos los campos
            toggleDisabled(document.getElementById("acercamientoCamposRecomendo"));

            // Habilita los campos de motivos de capacitacion y acercamiento
            document.getElementById('socio_id_motivo_capacitacion').removeAttribute('disabled');
            document.getElementById('socio_id_motivo_acercamiento').removeAttribute('disabled');

            //Otro contacto
            if(recomendo == 7){
                document.getElementById('socio_otros_contacto').removeAttribute('disabled');
            }
            //Socio
            else if(recomendo  == 6){
                document.getElementById('socio_socio_ref_id').removeAttribute('disabled');
                document.getElementById('recomendo_socio').style.display='block';
                document.getElementById('socio_ref_nombre').style.display='block';

            }

            if(recomendo  != 6) {
                document.getElementById('recomendo_socio').style.display='none';
                document.getElementById('socio_ref_nombre').style.display='none';
            }
        }
        /**/

        /**
         * Estudios
         */
         /*
        // Habilita selects dependiendo del valor del select Estudia
        // Reingenieria Mako AP. ID 5. Responsable: FE. Fecha: 01-04-2014. Descripción del cambio: Se modifoca funcionalidad
        function estudiosActual(o){
            console.log('estudiosActual');
            estudia = $('input[name="socio[estudia]"]:checked').val();
            console.log(estudia);

            if(window.location.href.indexOf('edit') == -1){
                //document.getElementById('socio_grado_id').value = '';
            }

            //document.getElementById('socio_grado_id').setAttribute('requerido', '1');

            console.log(estudia);
            //Nivel de estudios en curso
            if(estudia == 'true'){
                //$('#socio_nivel_id').attr('requerido','1');

                //document.getElementById('socio_nivel_id').removeAttribute('disabled');

                //Cambios matriz 4M - F2
                //ID:F2_26,F2_27 Responsable: ES

                document.getElementById('socio_tipo_escuela_id')    .disabled = false;
                document.getElementById('socio_tipo_estudio_id')    .disabled = false;
                //document.getElementById('socio_profesion_ultimo_id').disabled = true;
                //document.getElementById('socio_grado_id')           .disabled = true;

                //document.getElementById('socio_grado_id').value = '';
                //document.getElementById('socio_grado_id').removeAttribute('requerido');

            } else { //deshabilita todo
                //$('#socio_nivel_id').removeAttr('requerido','1');

                //deshabilita todos los campos
                toggleDisabled(document.getElementById("estudiaCamposActual"));

                //document.getElementById('socio_nivel_id')       .value = '';
                document.getElementById('socio_tipo_escuela_id').value = '';
                document.getElementById('socio_tipo_estudio_id').value = '';

                //document.getElementById('socio_grado_id')           .disabled = false;
                //document.getElementById('socio_nivel_id')           .disabled = true;
                document.getElementById('socio_tipo_escuela_id')    .disabled = true;
                document.getElementById('socio_tipo_estudio_id')    .disabled = true;
                //document.getElementById('socio_profesion_ultimo_id').disabled = true;

                //document.getElementById('socio_grado_id').setAttribute('requerido', '1');
            }
        }
        */

        function tipoVivienda(tipovivienda){            

            if(tipovivienda == 5){ 

                document.getElementById('socio_vivienda_es_id').value = '';
                document.getElementById('socio_vivienda_es_id').disabled = true;
                document.getElementById('socio_material_techo_id').value = '';
                document.getElementById('socio_material_techo_id').disabled = true;
                document.getElementById('socio_material_piso_id').value = '';
                document.getElementById('socio_material_piso_id').disabled = true;
                document.getElementById('socio_energia_electrica').checked = false;
                document.getElementById('socio_energia_electrica').disabled = true; 
                document.getElementById('socio_agua_entubada').checked = false;
                document.getElementById('socio_agua_entubada').disabled = true;                
                document.getElementById('socio_drenaje').checked = false;
                document.getElementById('socio_drenaje').disabled = true;                
                document.getElementById('socio_linea_telefonica').checked = false;
                document.getElementById('socio_linea_telefonica').disabled = true;                
                document.getElementById('socio_television_paga').checked = false;
                document.getElementById('socio_television_paga').disabled = true;                
                document.getElementById('socio_internet').checked = false;
                document.getElementById('socio_internet').disabled = true;                
                document.getElementById('socio_tableta').checked = false;
                document.getElementById('socio_tableta').disabled = true;                
                document.getElementById('socio_lavadora').checked = false;
                document.getElementById('socio_lavadora').disabled = true;                
                document.getElementById('socio_secadora').checked = false;
                document.getElementById('socio_secadora').disabled = true;                
                document.getElementById('socio_television').checked = false;
                document.getElementById('socio_television').disabled = true;                
                document.getElementById('socio_calentador_agua').checked = false;
                document.getElementById('socio_calentador_agua').disabled = true;                
                document.getElementById('socio_horno_microondas').checked = false;
                document.getElementById('socio_horno_microondas').disabled = true;                
                document.getElementById('socio_tostador').checked = false;
                document.getElementById('socio_tostador').disabled = true;                
                document.getElementById('socio_reproductor_video').checked = false;
                document.getElementById('socio_reproductor_video').disabled = true;                
                document.getElementById('socio_computadora').checked = false;
                document.getElementById('socio_computadora').disabled = true;                            
                document.getElementById('socio_telefono_celular').checked = false;
                document.getElementById('socio_telefono_celular').disabled = true;                
                document.getElementById('socio_reproductor_audio').checked = false;
                document.getElementById('socio_reproductor_audio').disabled = true;                
                document.getElementById('socio_fregadero').checked = false;
                document.getElementById('socio_fregadero').disabled = true;                            
                document.getElementById('socio_estufa').checked = false;
                document.getElementById('socio_estufa').disabled = true;                            
                document.getElementById('socio_refrigerador').checked = false;
                document.getElementById('socio_refrigerador').disabled = true;                            
                document.getElementById('socio_licuadora').checked = false;
                document.getElementById('socio_licuadora').disabled = true;

            }else{

                document.getElementById('socio_vivienda_es_id').disabled = false;
                document.getElementById('socio_vivienda_es_id').disabled = false;
                document.getElementById('socio_material_techo_id').disabled = false;
                document.getElementById('socio_material_piso_id').disabled = false;
                document.getElementById('socio_energia_electrica').disabled = false;                
                document.getElementById('socio_agua_entubada').disabled = false;                
                document.getElementById('socio_drenaje').disabled = false;                
                document.getElementById('socio_linea_telefonica').disabled = false;                
                document.getElementById('socio_television_paga').disabled = false;                
                document.getElementById('socio_internet').disabled = false;                
                document.getElementById('socio_tableta').disabled = false;                
                document.getElementById('socio_lavadora').disabled = false;                
                document.getElementById('socio_secadora').disabled = false;                
                document.getElementById('socio_television').disabled = false;                
                document.getElementById('socio_calentador_agua').disabled = false;                
                document.getElementById('socio_horno_microondas').disabled = false;                
                document.getElementById('socio_tostador').disabled = false;                
                document.getElementById('socio_reproductor_video').disabled = false;                
                document.getElementById('socio_computadora').disabled = false;                            
                document.getElementById('socio_telefono_celular').disabled = false;                
                document.getElementById('socio_reproductor_audio').disabled = false;                
                document.getElementById('socio_fregadero').disabled = false;                            
                document.getElementById('socio_estufa').disabled = false;                            
                document.getElementById('socio_refrigerador').disabled = false;                            
                document.getElementById('socio_licuadora').disabled = false;

               }       
            
        }
/*
        function estudiosNivelActual(nivel){
            if(nivel == 7 || nivel == 20 || nivel == 17){
                document.getElementById('socio_profesion_actual_id').removeAttribute('disabled');
                document.getElementById('socio_tipo_escuela_id')    .removeAttribute('disabled');
                document.getElementById('socio_tipo_estudio_id')    .removeAttribute('disabled');

            }else{
                //deshabilita todos los campos
                toggleDisabled(document.getElementById("otrosEstudiosActual"));
                document.getElementById('socio_tipo_escuela_id').disabled = true;
                document.getElementById('socio_tipo_estudio_id').disabled = true;

                if(window.location.href.indexOf('edit') == -1){
                    //document.getElementById('socio_grado_id').value = 1;
                }

                //document.getElementById('socio_grado_id').setAttribute('requerido', '1');
            }

            //Revisar el nivel para escoger el ultimo grado de estudios de manera correcta.
            var ultimo_grado_de_estudios = document.getElementById('socio_grado_id');
            switch(parseInt(nivel)) {
                case 3: //Primaria
                    ultimo_grado_de_estudios.value = 18;
                    document.getElementById('socio_tipo_escuela_id').removeAttribute('disabled');
                    document.getElementById('socio_tipo_estudio_id').removeAttribute('disabled');
                    break;

                case 4: //Secundaria
                    ultimo_grado_de_estudios.value = 3;
                    document.getElementById('socio_tipo_escuela_id').removeAttribute('disabled');
                    document.getElementById('socio_tipo_estudio_id').removeAttribute('disabled');
                    break;

                case 7: //Carrera técnica
                    ultimo_grado_de_estudios.value = 4;
                    break;

                case 19: //Preparatoria
                    ultimo_grado_de_estudios.value = 4;
                    document.getElementById('socio_tipo_escuela_id').removeAttribute('disabled');
                    document.getElementById('socio_tipo_estudio_id').removeAttribute('disabled');
                    break;

                case 20: //Licenciatura
                    ultimo_grado_de_estudios.value = 19;
                    break;

                case 17: //Posgrado
                    ultimo_grado_de_estudios.value = 20;
                    break;

                case 18: //Sin estudios formales
                    ultimo_grado_de_estudios.value = 18;
                    break;
            }

            estudiosNivelUltimo(ultimo_grado_de_estudios.value);

            estudia = $('input[name="socio[estudia]"]:checked').val();

            if(estudia == 'true') {
                ultimo_grado_de_estudios.disabled = true;

            } else {
                ultimo_grado_de_estudios.removeAttribute('disabled');
            }

            //Agregamos un campo oculto para mandar el valor del ultimo grado de estudios
            $('form[name="registro"]').bind('submit', function(){
                ultimo_grado_de_estudios.removeAttribute('disabled');
            });
        }*/

        function estudiosOtrosActual(otro){
            //otros estudios
            if(otro == 5){
                //document.getElementById('socio_otro_estudio_actual').removeAttribute('disabled');
            }
            //deshabilita otro
            else{
                //document.getElementById('socio_otro_estudio_actual').disabled = true;
                //document.getElementById('socio_otro_estudio_actual').value = '';
            }

        }

        // Habilita selects dependiendo del valor del select ultimo grado de estudios
        function estudiosNivelUltimo(nivel){
            if(nivel == 7 || nivel == 20 || nivel == 17){
                //document.getElementById('socio_profesion_ultimo_id').removeAttribute('disabled');
            }else{
                //document.getElementById('socio_profesion_ultimo_id').disabled = true;
            }
        }

        function estudiosOtrosUltimo(otro){
            //otros estudios
            if(otro == 5){
                // document.getElementById('socio_otro_estudio_ultimo').removeAttribute('disabled');
            }
            //deshabilita otro
            else{
                // document.getElementById('socio_otro_estudio_ultimo').disabled = true;
                // document.getElementById('socio_otro_estudio_ultimo').value = '';
            }

        }



        /*
         * Ocupacion
         */

            /*
            * Reingenieria  Mako Registro Socios. ID #
            * Responsable:  (BP) Bet Gader Porcayo Juárez
            * Fecha:        29-08-2014 
            * Descripción:  Validación de que si eligue ¿Trabaja?, se habilite el campo de sector,
            * Nota:         Esto tambien implicó pásar la busqueda de los inputs a jquery
            */
        // Habilita selects dependiendo del valor del select trabaja
        function trabaja(){
            return true;
            var socio_trabaja = $("#socio_trabaja");
            var socio_organizacion_id = $("#socio_organizacion_id");
            var socio_sector_id = $("#socio_sector_id");
            var socio_id_problema_empresa = $("#socio_id_problema_empresa");
            var socio_dependientes_economicos = $("#socio_dependientes_economicos");
            var socio_id_tiempo_actividad = $("#socio_id_tiempo_actividad");
            //var socio_ocupacion_id = $("#socio_ocupacion_id");

            var valor = $('input[name=socio[trabaja]]:checked', '#registro').val();
            console.log('----------Trabaja-----------');
            console.log(valor);
            //Si trabaja
            if( valor == true || valor == "true"){
                socio_organizacion_id.removeAttr("disabled");
                socio_sector_id.removeAttr("disabled");
                // document.getElementById('socio_posicion_id')            .removeAttribute('disabled');
                socio_id_problema_empresa.removeAttr("disabled");
                socio_dependientes_economicos.removeAttr("disabled");
                socio_id_tiempo_actividad.removeAttr("disabled");

                //socio_ocupacion_id.attr("disabled","disabled"); //Fecha: 24-06-2013. Inicio. ID Seguimiento: F2_36 Responsable: ES.
                //socio_ocupacion_id.val(""); // Fecha: 01-04-2014. Fin


//               Reingenieria  #6.-MAKO-ARFS-002
//               Responsable:  (BP) Bet Gader Porcayo Juárez
//               Fecha:        06-10-2014
//               Descripción: Solo mostramos opciones de Empleado (5), Negocio Propio (7) y Otro (9)
                clearOptionsOcupacion(); //Borramos todo lo que aya
                OptionsOcupacion.each(function(){
                    OptionIn = $(this);
                    console.log(OptionIn.html());
                    switch (OptionIn.val()){
                        case 5:
                        case '5':
                        case 7:
                        case '7':
                        case 9:
                        case '9':
                            $("#socio_ocupacion_id").append(this);
                            break;
                    }
                })

            }else{
                //deshabilita todos los campos
                socio_organizacion_id.attr("disabled","disabled");
                socio_sector_id.attr("disabled","disabled");
                socio_id_problema_empresa.attr("disabled","disabled");
                socio_dependientes_economicos.attr("disabled","disabled");
                socio_id_tiempo_actividad.attr("disabled","disabled");

                //socio_ocupacion_id.removeAttr("disabled");


                clearOptionsOcupacion(); //Borramos todo lo que aya
                OptionsOcupacion.each(function(){
                    OptionIn = $(this);
                    console.log(OptionIn.html());
                    switch (OptionIn.val()){
                        case 1:
                        case '1':
                        case 2:
                        case '2':
                        case 3:
                        case '3':
                        case 4:
                        case '4':
                        case 6:
                        case '6':
                        case 9:
                        case '9':
                            $("#socio_ocupacion_id").append(this);
                            break;
                    }
                })
            }

        }
        /*
            * Reingenieria  Mako Registro Socios. ID #
            * Responsable:  (BP) Bet Gader Porcayo Juárez
            * Fecha:        29-08-2014 
            * Descripción:  Validación de que si eligue ¿Trabaja?, se habilite el campo de sector,
            * Nota:         Esta funcion se ejecuta al inicio para Modificiar un Socio, y la derivada es 
            *               Si seleccion anteriormente una opcion de Si trabaja, Seleccionar Si trabaja, por consecuente, en caso contrario, se seleccionará No trabaja
            */
        // Habilita selects dependiendo del valor del select trabaja
        function trabajaInicio(){
           
            // Ahora verificamos en que categoría entra el seleccionado
            OptionsOcupacion.each(function(){
                OptionIn = $(this);
                console.log(OptionIn.context.defaultSelected);
                console.log(OptionIn.html());
                if(OptionIn.context.defaultSelected){
                    switch (OptionIn.val()){
                        case 5:
                        case '5':
                        case 7:
                        case '7':
                            $("#socio_trabaja_true").attr("checked", true);
                            break;
                        case 1:
                        case '1':
                        case 2:
                        case '2':
                        case 3:
                        case '3':
                        case 4:
                        case '4':
                        case 6:
                        case '6':
                            $("#socio_trabaja_false").attr("checked", true);
                            break;
                    }
                }
            });
            //trabaja();
        }

        //NSE
        //Uso automovil
        function nseUsoAutomovil(uso){
            var automovil = document.getElementById('socio_automovil_propio');
            //otros estudios
            if(automovil.checked == true){
                document.getElementById('socio_uso_automovil_id').removeAttribute('disabled');
            }
            //deshabilita uso
            else{
                document.getElementById('socio_uso_automovil_id').disabled = true;
                document.getElementById('socio_uso_automovil_id').value = '';
            }

        }


        function toggleDisabled(el) {
            allSelects = el.getElementsByTagName('select');
            for (i = 0; i < allSelects.length; i++) {
                allSelects[i].disabled = true;

                if(window.location.href.indexOf('edit') == -1){
                    allSelects[i].options[0].selected = true;

                } else { // edicion
                    if(allSelects[i].value != '') {
                        allSelects[i].removeAttribute('disabled');
                    }
                }
            }

            allInputs = el.getElementsByTagName('input');
            for (i = 0; i < allInputs.length; i++) {
                allInputs[i].disabled = true;

                if(window.location.href.indexOf('edit') == -1){
                    allInputs[i].value = '';

                } else { // edicion
                    if(allInputs[i].value != '') {
                        allInputs[i].removeAttribute('disabled');
                    }
                }
            }
        }


        /**
         * Envia los datos del folio de la credencia al servidor para verificar que no existan registros previos de la credencial
         * Si todo esta bien o el usuario selecciona confirmar la accion esta misma funcion es la que realiza el sumbit de la forma.
         */
        function verificaFolio(){
            var fv = document.getElementById('folio');
            var id_usuario = document.getElementById('id_usuario');
            if(fv.value==''){return true;}
            var respuesta;

            var req = doRequest("POST", "./registro.php",
                {accion:'verifica_folio',folio:fv.value,id_usuario:id_usuario.value},
                function(status, statusText, responseText) {

                    if (status != 200) {
                        document.getElementById('error').innerHTML="No es posible conectar con el sevidor. La conexi&oacute;n a la red se ha perdido. Intente nuevamente.";
                        return false;
                    }
                    else if(responseText=='OK' && status == 200)
                    {
                        document.getElementById('id_usuario').form.submit();
                        return true;
                    }
                    else if(responseText !='OK' && status == 200)
                    {
                        var resp = responseText.split("|");

                        var nombre = resp[0]+ " " +resp[1]+ " "+resp[2];
                        var fecha_alta =   resp[3];
                        var centro =  resp[4];
                        var id_usuario_registrado =  resp[5];
                        var id_usuario = document.getElementById('id_usuario');
                        if ((id_usuario.value != id_usuario_registrado.value)) {
                            var msgConfirmacion = "La credencial con folio: " + fv.value + " ya se tiene registrada al socio:\nNombre: " +
                            nombre +
                            "\nDado de alta la fecha: " +
                            fecha_alta +
                            "\nEn el centro: " +
                            centro +
                            "\nSi continúa se dará de baja el registro anterior del socio: " +
                            nombre +
                            "\n\n¿Desea continuar con el registro?";

                            if (confirm(msgConfirmacion))
                            {
                                document.getElementById('baja_registro_duplicado').value = id_usuario_registrado;
                                id_usuario.form.submit();
                            }
                            else {
                                return false;
                            }
                        }
                    }

                });
            return false;
        }
        /**
         * Envia los datos del folio de la credencia al servidor para verificar que no existan registros previos de la credencial
         * Si todo esta bien o el usuario selecciona confirmar la accion esta misma funcion es la que realiza el sumbit de la forma.
         */

            /*
            * Reingenieria  Mako Registro Socios. ID #18, 20 y 17
            * Responsable:  (BP) Bet Gader Porcayo Juárez
            * Fecha:        29-08-2014 
            * Descripción:  Valida que el usuario al menos seleccione una opcion de acceso a la tecnología, también, permite tambien las 2 funciones ()
            *               seleccionar o deselecionar la opcion de ninguno  
            */
        function validaaccesotecnología(){
            socio_computadora           = $("#socio_computadora").is(':checked');
            socio_linea_telefonica      = $("#socio_linea_telefonica").is(':checked');
            socio_telefono_celular      = $("#socio_telefono_celular").is(':checked');
            socio_internet              = $("#socio_internet").is(':checked');
            socio_tableta               = $("#socio_tableta").is(':checked');
            socio_television            = $("#socio_television").is(':checked');
            socio_noacceso              = $("#socio_noacceso").is(':checked');

            console.log('validaaccesotecnología');
            console.log("socio_computadora");
            console.log(socio_computadora);
            console.log("socio_linea_telefonica");
            console.log(socio_linea_telefonica);
            console.log("socio_telefono_celular");
            console.log(socio_telefono_celular);
            console.log("socio_internet");
            console.log(socio_internet);
            console.log("socio_tableta");
            console.log(socio_tableta);
            console.log("socio_television");
            console.log(socio_television);
            console.log("socio_noacceso");
            console.log(socio_noacceso);

            if(socio_computadora || socio_linea_telefonica || socio_telefono_celular || socio_internet ||  socio_tableta || socio_television || socio_noacceso){
                cierraError(document.getElementById('AccesoTecnologia'));
                return true;
            }
            else{
                abreError(document.getElementById('AccesoTecnologia'));
                alert('Debe seleccionar al menos una opción en Acceso a la tenología.');
            }
            return false;
        }



        /*
            * Reingenieria  MAKO-ARFS-002 PUNTO 10
            * Responsable:  (BP) Bet Gader Porcayo Juárez
            * Fecha:        02/oct/2014
            * Descripción:  ¿Desea tomar fotografía al socio? 
            */
        function verificaFoto(){
            /*
            primero verificamos si ya hay una foto, si hay se continua con el proceso, si no, se manda mensaje de siguiente
            */
            Foto = $("#img_capturar_foto").attr('src');
            console.log(Foto);
            if(Foto == '/imgs/sin_foto_sh.jpg'){
                $('body').append('<div id="modal_dialog"><div class="title" id="TitleModal"></div></div>');
                var warning = '¿Desea tomar fotografía al socio?';
                $('#TitleModal').html(warning);
                var dialog = $('#modal_dialog').dialog({
                    resizable: false,
                    height:150,
                    width:400,
                    buttons: {
                        "Si": function() {
                            console.log('Desea tomar foto');
                            dialogCamera.dialog('open');
                            $("#cont_foto").show();
                            $( this ).dialog( "close" );
                            $("#modal_dialog").remove();
                        },
                        "No": function() {
                            console.log('No desea tomar la foto');
                            $( this ).dialog( "close" );
                            $("#modal_dialog").remove();
                            $("#registro").submit();
                        }
                      }
                });
                return false;
            } else {
                console.log('Ya hay una foto');
                return true;
console.log('P+');
            }
        }

        function clearOptionsOcupacion(){
            $("#socio_ocupacion_id option").each(function (){ $(this).remove();});
        }







        /*
           Reingenieria  #11.-MAKO-ARFS-002
           Responsable:  (BP) Bet Gader Porcayo Juárez
           Fecha:        06-10-2014
           Descripción: Validamos el Curp OJO son varias funciones validar_cur*
        */
        function valida_curp_nombre(tecla){
            teclazo = tecla.charCode || tecla.keyCode; 
            console.log('valida_curp_nombre');
            console.log(tecla);
            console.log(teclazo);
            // ----if FireFox
            if(tecla.keyCode)
                if(tecla.keyCode == 8 || tecla.keyCode == 46)
                    return true;
            if(tecla)
                if((teclazo < 97 || teclazo > 122) && (teclazo < 65 || teclazo > 90) || (teclazo == 0))
                    return false;

            valor = $("#curp_nombre").val();
            expr   = /^[A-Z]{1}[AEIOU]{1}[A-Z]{2}/i;

            console.log(valor);
            console.log(valor.length);
            switch(valor.length){
                case 1:
                    if(valor.match(/^[A-Z]{1}/i))
                        $("#curp_nombre").css('border-color', '#FFDB70').css('border-width','2px');
                    else
                        $("#curp_nombre").css('border-color', 'red').css('border-width','2px');
                    break;
                case 2:
                    if(valor.match(/^[A-Z]{1}[AEIOU]{1}/i))
                        $("#curp_nombre").css('border-color', '#FFDB70').css('border-width','2px');
                    else
                        $("#curp_nombre").css('border-color', 'red').css('border-width','2px');
                    break;
                case 3:
                    if(valor.match(/^[A-Z]{1}[AEIOU]{1}[A-Z]{1}/i))
                        $("#curp_nombre").css('border-color', '#FFDB70').css('border-width','2px');
                    else
                        $("#curp_nombre").css('border-color', 'red').css('border-width','2px');
                    break;
                case 4:
                    if(valor.match(/^[A-Z]{1}[AEIOU]{1}[A-Z]{2}/i))
                        $("#curp_nombre").css('border-color', '').css('border-width','1px');
                    else
                        $("#curp_nombre").css('border-color', 'red').css('border-width','2px');
                    break;

            }
        }
        function valida_curp_fecha(tecla){
            console.log('valida_curp_fecha');
            console.log(tecla);
            teclazo = tecla.charCode || tecla.keyCode; 
            // ----if FireFox
            if(tecla.keyCode)
                if(tecla.keyCode == 8 || tecla.keyCode == 46)
                    return true;
            if(tecla)
                if(teclazo < 48 || teclazo > 57 || (teclazo == 0))
                    return false;

            valor = $("#curp_fecha").val();
            expr   = /^[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])/i;

            console.log(valor);
            console.log(valor.length);
            switch(valor.length){
                case 1:
                    $("#curp_fecha").css('border-color', '#FFDB70').css('border-width','2px');
                    break;
                case 2:
                    if(valor.match(/^[0-9]{2}/i)){
                        $("#curp_fecha").css('border-color', '#FFDB70').css('border-width','2px');
                    }else
                        $("#curp_fecha").css('border-color', 'red').css('border-width','2px');
                    break;
                case 3:
                    $("#curp_fecha").css('border-color', '#FFDB70').css('border-width','2px');
                    break;
                case 4:
                    if(valor.match(/^[0-9]{2}(0[1-9]|1[0-2])/i)){
                        // Obtenemos el año y mes seleccionados
                        anioCurp = valor.substring(0,2);
                        mesCurp = valor.substring(2,4);

                        console.log('El año ingresado es '+ anioCurp);
                        console.log('El mes ingresado es '+ mesCurp);

                        /* Ahora verificamos que el mes este entre 1 y 12 */
                        if( (mesCurp > 0) && (mesCurp < 13) )
                            $("#curp_fecha").css('border-color', '#FFDB70').css('border-width','2px');
                        else 
                            $("#curp_fecha").css('border-color', 'red').css('border-width','2px');
                    }else
                        $("#curp_fecha").css('border-color', 'red').css('border-width','2px');
                    break;
                case 5:
                    $("#curp_fecha").css('border-color', '#FFDB70').css('border-width','2px');
                    break;
                case 6:
                    console.log('case 6')
                    if(valor.match(/^[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])/i)){
                        // Obtenemos el año y mes seleccionados
                        anioCurp = valor.substring(0,2);
                        mesCurp = valor.substring(2,4);
                        diaCurp = valor.substring(4,6);

                        console.log('El año ingresado es '+ anioCurp);
                        console.log('El mes ingresado es '+ mesCurp);
                        console.log('El día ingresado es '+ mesCurp);

                        /* Ahora verificamos que el mes este entre 1 y 12 */
                        if( (mesCurp > 0) && (mesCurp < 13) )
                            $("#curp_fecha").css('border-color', '#FFDB70').css('border-width','2px');
                        else {
                            $("#curp_fecha").css('border-color', 'red').css('border-width','2px');
                            break;
                        }

                        FechaMinima = 0;
                        if(anioCurp < 14)
                            FechaMinima = anioCurp*1 + 2000;
                        else
                            FechaMinima = anioCurp*1 + 1900;
                        console.log('El año completo del cupr es '+ FechaMinima);


                        /* Ahora validamos el numero de días en un mes */
                        if(diaCurp <= daysInMonth(mesCurp, FechaMinima) )
                            $("#curp_fecha").css('border-color', '').css('border-width','1px');
                        else
                            $("#curp_fecha").css('border-color', 'red').css('border-width','2px');
                    }else
                        $("#curp_fecha").css('border-color', 'red').css('border-width','2px');
                    break;
                

            }
        }
        function daysInMonth(humanMonth, year) {
             return new Date(year || new Date().getFullYear(), humanMonth, 0).getDate();
        }
        function valida_curp_genero(tecla){
            teclazo = tecla.charCode || tecla.keyCode; 
            console.log('valida_curp_genero');
            console.log(teclazo);
            console.log(tecla);
            // ----if FireFox
            if(tecla.keyCode)
                if(tecla.keyCode == 8 || tecla.keyCode == 46)
                    return true;
            if(tecla)
                if(!(teclazo == 104 || teclazo == 109 || teclazo == 72 || teclazo == 77) || (teclazo == 0) ) return false;

            valor = $("#curp_genero").val();
            expr   = /^[HM]{1}/i;

            console.log(valor);
            console.log(valor.length);
            switch(valor.length){
                case 1:
                    if(valor.match(/^[HM]{1}/i))
                        $("#curp_genero").css('border-color', '').css('border-width','1px');
                    else
                        $("#curp_genero").css('border-color', 'red').css('border-width','2px');
                    break;
                
            }
        }
        function valida_curp_estado(tecla){
            teclazo = tecla.charCode || tecla.keyCode; 
            console.log('valida_curp_genero');
            console.log(teclazo);
            console.log(tecla);
            // ----if FireFox
            if(tecla.keyCode)
                if(tecla.keyCode == 8 || tecla.keyCode == 46)
                    return true;
            if(tecla)
                if((teclazo < 97 || teclazo > 122) && (teclazo < 65 || teclazo > 90 ) || (teclazo == 0))
                    return false;

            valor = $("#curp_estado").val();
            expr   = /^(AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE)/i;

            console.log(valor);
            console.log(valor.length);
            switch(valor.length){
                case 1:
                    $("#curp_estado").css('border-color', '#FFDB70').css('border-width','2px');
                    break;
                case 2:
                    if(valor.match(/^(AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE)/i))
                        $("#curp_estado").css('border-color', '').css('border-width','1px');
                    else
                        $("#curp_estado").css('border-color', 'red').css('border-width','2px');
                    break;
                
            }
        }
        function valida_curp_cnombre(tecla){
            teclazo = tecla.charCode || tecla.keyCode; 
            console.log('valida_curp_cnombre');
            console.log(teclazo);
            console.log(tecla);
            // ----if FireFox
            if(tecla.keyCode)
                if(tecla.keyCode == 8 || tecla.keyCode == 46)
                    return true;
            if(tecla)
                if((teclazo < 97 || teclazo > 122) && (teclazo < 65 || teclazo > 90) || (teclazo == 0))
                    return false;

            valor = $("#curp_cnombre").val();

            console.log(valor);
            console.log(valor.length);
            switch(valor.length){
                case 1:
                    $("#curp_cnombre").css('border-color', '#FFDB70').css('border-width','2px');
                    break;
                case 2:
                    $("#curp_cnombre").css('border-color', '').css('border-width','1px');
                    break;
            }
        }
        function valida_curp_codigo(tecla){
            teclazo = tecla.charCode || tecla.keyCode; 
            console.log('valida_curp_codigo');
            console.log(teclazo);
            console.log(tecla);
            // ----if FireFox
            if(tecla.keyCode)
                if(tecla.keyCode == 8 || tecla.keyCode == 46)
                    return true;
            if(tecla)
                if((teclazo < 48 || teclazo > 57)  &&  (teclazo < 97 || teclazo > 122) && (teclazo < 65 || teclazo > 90) || (teclazo == 0))
                    return false;

            valor = $("#curp_codigo").val();

            console.log(valor);
            console.log(valor.length);
            switch(valor.length){
                case 1:
                    $("#curp_codigo").css('border-color', '#FFDB70').css('border-width','2px');
                    break;
                case 2:
                    $("#curp_codigo").css('border-color', '').css('border-width','1px');
                    break;
                
            }
        }


$(document).ready(function() {

        //OptionsOcupacion = $("#socio_ocupacion_id option");
        //clearOptionsOcupacion();
        /* Si viene de editar lo seleccionamos*/
        if($("#socio_clave").length != 0) {
            //trabajaInicio();
        }
        
        $('#curp_nombre').change(function(){
            valida_curp_nombre(false);
        });
        $('#curp_nombre').keypress(function(tecla){
            return valida_curp_nombre(tecla);
        });

        $('#curp_fecha').change(function(){
            valida_curp_fecha(false);
        });
        $('#curp_fecha').keypress(function(tecla){
            return valida_curp_fecha(tecla);
        });

        $('#curp_genero').change(function(){
            valida_curp_genero(false);
        });
        $('#curp_genero').keypress(function(tecla){
            return valida_curp_genero(tecla);
        });

        $('#curp_estado').change(function(){
            valida_curp_estado(false);
        });
        $('#curp_estado').keypress(function(tecla){
            return valida_curp_estado(tecla);
        });

        $('#curp_cnombre').change(function(){
            valida_curp_cnombre(false);
        });
        $('#curp_cnombre').keypress(function(tecla){
            return valida_curp_cnombre(tecla);
        });

        $('#curp_codigo').change(function(){
            valida_curp_codigo(false);
        });
        $('#curp_codigo').keypress(function(tecla){
            return valida_curp_codigo(tecla);
        });


/*        
        $('#curp_nombre').focus(function(){
            console.log('Focus Text ');
            $('#message_curp').append("Primer letra del primer apellido. <br/> Primer vocal no inicial del primer apellido <br/> Primer letra del segundo apellido <br/> Primer letra del primer nombre.");
        });

        $('#curp_fecha').focus(function(){
            console.log('Focus Text ');
            $('#message_curp').append("Dos últimos dígitos de la fecha de nacimiento <br/> Dos dígitos del mes de nacimiento <br/> Dos dígitos del día de nacimiento. ");
        });

        $('#curp_genero').focus(function(){
            console.log('Focus Text ');
            $('#message_curp').append("H o M para indecar el género Hombre o Mujer. ");
        });

        $('#curp_estado').focus(function(){
            console.log('Focus Text ');
            $('#message_curp').append("Código en dos carateres correspondientes a la entidad federativa");
        });

        $('#curp_cnombre').focus(function(){
            console.log('Focus Text ');
            $('#message_curp').append("Primer consonante no inicial del primer apellido <br/> Primer consonante no inicial del segundo apellido <br/> Primer consonante no inicial del nombre.");
        });

        $('#curp_codigo').focus(function(){
            console.log('Focus Text ');
            $('#message_curp').append("Código asignado por RENAPO.");
        });


        $('#curp_nombre, #curp_fecha, #curp_genero, #curp_estado, #curp_cnombre, #curp_codigo').blur(function(){
            console.log('Focus Out');
            $('#message_curp').hide('fast').empty();
        });
        $('#curp_nombre, #curp_fecha, #curp_genero, #curp_estado, #curp_cnombre, #curp_codigo').focus(function(){
            console.log('Focus IN');
            $('#message_curp').show('fast');
        });
*/


        /*
            Buscamos si no hay ningun campo de accceso a la tecnología ponemos el de 
        */
        socio_computadora           = $("#socio_computadora").is(':checked');
        socio_linea_telefonica      = $("#socio_linea_telefonica").is(':checked');
        socio_telefono_celular      = $("#socio_telefono_celular").is(':checked');
        socio_internet              = $("#socio_internet").is(':checked');
        socio_tableta               = $("#socio_tableta").is(':checked');
        socio_noacceso              = $("#socio_noacceso");

         if(!socio_computadora && !socio_linea_telefonica && !socio_telefono_celular && !socio_internet && !socio_tableta ){
            /* RegSocios viene de editar lo seleccionamos*/
            if($("#socio_clave").length != 0) {
                socio_noacceso.attr( "checked", true );
            }
         }


         $('#socio_noacceso').click(function(){
            socio_computadora           = $("#socio_computadora");
            socio_linea_telefonica      = $("#socio_linea_telefonica");
            socio_telefono_celular      = $("#socio_telefono_celular");
            socio_internet              = $("#socio_internet");
            socio_tableta               = $("#socio_tableta");
            socio_television            = $("#socio_television");
            socio_noacceso              = $("#socio_noacceso");
            if(socio_noacceso.is(':checked')){
                socio_computadora.attr( "checked", false );
                socio_linea_telefonica.attr( "checked", false);
                socio_telefono_celular.attr( "checked", false );
                socio_internet.attr( "checked", false );
                socio_tableta.attr( "checked", false );
                socio_television.attr( "checked", false );
            }
        });

        $('#socio_computadora, #socio_telefono_celular, #socio_internet, #socio_tableta, #socio_television, #socio_linea_telefonica').click(function(){
            socio_noacceso              = $("#socio_noacceso");
            socio_noacceso.attr( "checked", false );
        });








        //Validamos si fue selecionado la opcion de estudia para habilitar tipo de estudio y tipo de escuenla
      /*  socio_estudia_true            = $("#socio_estudia_true").is(':checked');
        if(socio_estudia_true){
            document.getElementById('socio_tipo_escuela_id')    .disabled = false;
            document.getElementById('socio_tipo_estudio_id')    .disabled = false;
        }
*/
        //Validamos si fue selecionado la opcion de trabaja para habilitar sector}
        /*socio_trabaja_true            = $("#socio_trabaja_true").is(':checked');
        if(socio_trabaja_true){
            document.getElementById('socio_sector_id')    .disabled = false;
        }*/
        



        


//               Reingenieria  #8.-MAKO-ARFS-002
//               Responsable:  (BP) Bet Gader Porcayo Juárez
//               Fecha:        06-10-2014
//               Descripción: Agregar la opcion Sin discapacidad

        /*
            Primero insertamos el de No discapacidad y despues buscamos si no hay ningun campo de discapacidad para seleccionar el por defecto
        */
        $('#DiscapacidadTD .checkbox_list').append('<li><input type="checkbox" id="socio_socio_discapacidad_N">&nbsp;<label for="socio_socio_discapacidad_N">Sin discapacidad</label></li>');

        socio_socio_discapacidad_2       = $("#socio_socio_discapacidad_2").is(':checked');
        socio_socio_discapacidad_4       = $("#socio_socio_discapacidad_4").is(':checked');
        socio_socio_discapacidad_5       = $("#socio_socio_discapacidad_5").is(':checked');
        socio_socio_discapacidad_3       = $("#socio_socio_discapacidad_3").is(':checked');
        socio_socio_discapacidad_N       = $("#socio_socio_discapacidad_N");

        if(!socio_socio_discapacidad_2 && !socio_socio_discapacidad_4 && !socio_socio_discapacidad_5 && !socio_socio_discapacidad_3 ){
            /* Si viene de editar lo seleccionamos*/
            if($("#socio_clave").length != 0) {
                console.log('Estamos en EDIt');
                socio_socio_discapacidad_N.attr( "checked", true );
            }
        }

        /*
            Si se ha seleccionado alguna discapacidad, el campo de "Sin discapacidad" se deshabilita
        */
        $('#socio_socio_discapacidad_2, #socio_socio_discapacidad_4, #socio_socio_discapacidad_5, #socio_socio_discapacidad_3').click(function(){
            socio_socio_discapacidad_N       = $("#socio_socio_discapacidad_N");
            socio_socio_discapacidad_N.attr( "checked", false );
        });

        /*
            Si se ha seleccionado el campo de "Sin discapacidad" se deshabilitan los demas
        */
        $('#socio_socio_discapacidad_N').click(function(){
            socio_socio_discapacidad_2     = $("#socio_socio_discapacidad_2");
            socio_socio_discapacidad_4     = $("#socio_socio_discapacidad_4");
            socio_socio_discapacidad_5     = $("#socio_socio_discapacidad_5");
            socio_socio_discapacidad_3     = $("#socio_socio_discapacidad_3");
            socio_socio_discapacidad_N     = $("#socio_socio_discapacidad_N");
            if(socio_socio_discapacidad_N.is(':checked')){
                socio_socio_discapacidad_2.attr( "checked", false );
                socio_socio_discapacidad_3.attr( "checked", false);
                socio_socio_discapacidad_4.attr( "checked", false );
                socio_socio_discapacidad_5.attr( "checked", false );
            }
        });

        /*
            Para ocuupacion
        */
        $('#OcupacionTD ul').append('<li><label for="socio_socio_ocupacion_N">Sin ocupación</label><input type="checkbox" name="noOcupacion" value="" id="socio_socio_ocupacion_N"></li>');

        $('#socio_socio_ocupacion_1, #socio_socio_ocupacion_3, #socio_socio_ocupacion_4, #socio_socio_ocupacion_5, #socio_socio_ocupacion_6, #socio_socio_ocupacion_7, #socio_socio_ocupacion_8').click(function(){
            console.log('Ocup1');
            socio_socio_ocupacion_N       = $("#socio_socio_ocupacion_N");
            socio_socio_ocupacion_N.attr( "checked", false );
        });

            socio_socio_ocupacion_1       = $("#socio_socio_ocupacion_1").is(':checked');
            socio_socio_ocupacion_3       = $("#socio_socio_ocupacion_3").is(':checked');
            socio_socio_ocupacion_4       = $("#socio_socio_ocupacion_4").is(':checked');
            socio_socio_ocupacion_5       = $("#socio_socio_ocupacion_5").is(':checked');
            socio_socio_ocupacion_6       = $("#socio_socio_ocupacion_6").is(':checked');
            socio_socio_ocupacion_7       = $("#socio_socio_ocupacion_7").is(':checked');
            socio_socio_ocupacion_8       = $("#socio_socio_ocupacion_8").is(':checked');

        if(!socio_socio_ocupacion_1 && !socio_socio_ocupacion_3 && !socio_socio_ocupacion_4 && !socio_socio_ocupacion_5 && !socio_socio_ocupacion_6 && !socio_socio_ocupacion_7 && !socio_socio_ocupacion_8){
            /* Si viene de editar lo seleccionamos*/
            if($("#socio_clave").length != 0) {
                console.log('Estamos en EDIt y en ocupacion')
                socio_socio_ocupacion_N       = $("#socio_socio_ocupacion_N");;
                socio_socio_ocupacion_N.attr( "checked", true );
            }
        }

        /*
            Si se ha seleccionado el campo de "Sin discapacidad" se deshabilitan los demas
        */
        $('#socio_socio_ocupacion_N').click(function(){
            socio_socio_ocupacion_1       = $("#socio_socio_ocupacion_1");
            socio_socio_ocupacion_3       = $("#socio_socio_ocupacion_3");
            socio_socio_ocupacion_4       = $("#socio_socio_ocupacion_4");
            socio_socio_ocupacion_5       = $("#socio_socio_ocupacion_5");
            socio_socio_ocupacion_6       = $("#socio_socio_ocupacion_6");
            socio_socio_ocupacion_7       = $("#socio_socio_ocupacion_7");
            socio_socio_ocupacion_8       = $("#socio_socio_ocupacion_8");
            socio_socio_ocupacion_N       = $("#socio_socio_ocupacion_N");
            console.log('Click en socio_socio_ocupacion_N');

            if(socio_socio_ocupacion_N.is(':checked')){
                console.log('Checado');
                socio_socio_ocupacion_1.attr( "checked", false );
                socio_socio_ocupacion_3.attr( "checked", false );
                socio_socio_ocupacion_4.attr( "checked", false );
                socio_socio_ocupacion_5.attr( "checked", false );
                socio_socio_ocupacion_6.attr( "checked", false );
                socio_socio_ocupacion_7.attr( "checked", false );
                socio_socio_ocupacion_8.attr( "checked", false );
            }
        });




        /*
        Para llenar los campos del curp
        */
        if($("#socio_clave").length != 0) {
            $('#curp_nombre').val( $('#socio_curp').val().substring(0,4)); 
            $('#curp_fecha').val(  $('#socio_curp').val().substring(4,10)); 
            $('#curp_genero').val( $('#socio_curp').val().substring(10,11)); 
            $('#curp_estado').val( $('#socio_curp').val().substring(11,13)); 
            $('#curp_cnombre').val($('#socio_curp').val().substring(13,16)); 
            $('#curp_codigo').val( $('#socio_curp').val().substring(16,18));
        }





    });

//c0nt4cTo
