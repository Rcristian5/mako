var regs_mensajes = {
	'socio_fecha_nac_day':'La edad mínima de un socio es 6 años.',
	'socio_fecha_nac_month':'La edad mínima de un socio es 6 años.',
	'socio_fecha_nac_year':'La edad mínima de un socio es 6 años.',
	'socio_celular':'No capture 044 o 045 al inicio.',
	'socio_email':'Si el socio no tiene correo deje el campo vacío.',
	'autocomplete_socio_promotor_ref_id':'Capture parte del nombre y el sistema le ayudará a autocompletar.',
	'socio_otros_contacto':'Ejemplo: vecino, vivo cerca, pasaba por aquí.',
	'socio_recomienda_id':'Si elige "Socio", haga clic en la liga "Buscar socio" que habilita la búsqueda.',
	'socio_nivel_id':'<p><strong>Básico</strong> Primaria y Secundaria</p><p><strong>Media superior</strong> ' +
		'Preparatoria o bachillerato técnico</p><p><strong>Superior</strong> Universidad</p><p><strong>Posgrado' +
		'</strong> Diplomado, Especialidad, Maestría o Doctorado</p>',
	'socio_profesion_actual_id':'Seleccione la categoría relacionada a la carrera profesional que estudia el socio.',
	'socio_estudio_actual_id':'Seleccione: Arte/Oficio/Otros para referirse a una carrera corta.',
	'socio_otro_estudio_actual':'Escriba la carrera corta o el dato que indique el socio.',
	'socio_habilidad_informatica_id':'<strong>Ninguna</strong><p>Nunca ha usado una computadora.</p><strong>Básico' +
		'</strong><p>Puede manejar el teclado y el ratón, crear documentos y consultar información en Internet.</p>' +
		'<strong>Intermedio</strong><p>Puede elaborar presentaciones con diapositivas, consultar y producir ' +
		'información en Internet.</p><strong>Avanzado</strong><p>Puede trabajar hojas de calculo con facilidad, '+
		'utiliza Internet para realizar tramites y compras.</p>',
	'socio_dominio_ingles_id':'<strong>Ninguno</strong><p>Nunca ha tenido acercamiento al idioma.</p><strong>Básico.' +
		'</strong><p>Entiende y elabora frases cortas sobre temas cotidianos.</p><strong>Intermedio.</strong><p>' +
		'Entiende y puede decir oraciones completas sobre diversos temas pero no sostener una conversación larga.' +
		'</p><strong>Avanzado.</strong><p>Puede tener una conversación larga sobre cualquier tema.</p>'
}

function mensaje_muestra(control){
	var id = control.id + '_mensaje';

	if (regs_mensajes[control.id] != undefined)	document.getElementById(id).innerHTML = regs_mensajes[control.id];
	else document.getElementById(id).innerHTML = '[ERROR]';

	control.focus()
}

function mensaje_oculta(control){document.getElementById(control.id + '_mensaje').innerHTML = '';}
