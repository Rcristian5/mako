/**
 * Funciones para manejo de mapas.
 */

var map;
var geocoder;
var address;
var localsearch;
var searchControl;
var latIni;
var longIni;
var marker;
var is_init = true;


function initialize() 
{
	var latlng = new google.maps.LatLng(latIni, longIni);
    var myOptions = {
      zoom: 15,
      center: latlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("map_canvas"),
        myOptions);
    marker = new google.maps.Marker({
        position: latlng, 
        map: map, 
        animation: google.maps.Animation.DROP,
        draggable:true,

    });      
    geocoder = new google.maps.Geocoder();
    google.maps.event.addListener(marker, 'dragend', setCoordinates);
    google.maps.event.addListener(map, 'click', function(event){
  	  getClickMarker(event.latLng);
    });

}

function toggleBounce() 
{

	if (marker.getAnimation() != null) 
	{
	    marker.setAnimation(null);
	} 
	else 
	{
	    marker.setAnimation(google.maps.Animation.BOUNCE);
	}
}
function getClickMarker(location){
	marker.setPosition(location);
	setCoordinates();
}
function setCoordinates()
{
	geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
	      if (status == google.maps.GeocoderStatus.OK) 
	      {
	        if (results[1]) 
	        {
	        	$(document).trigger('georeferencia.modificada',[marker.getPosition().lat(),marker.getPosition().lng(),results[1].formatted_address]);
	        }
	      } 
	      else 
	      {
	    	  console.log("Geocoder falló: " + status);
	      }
	 });
}
function setAddress(address) {
	console.log('Gmap: '+ address);
	geocoder.geocode( { 'address': address}, function(results, status) {
	      if (status == google.maps.GeocoderStatus.OK) {
	        map.setCenter(results[0].geometry.location);
	        marker.setPosition(results[0].geometry.location);
	        $(document).trigger('georeferencia.modificada',[marker.getPosition().lat(),marker.getPosition().lng(),address]);
	      } else {
	    	  console.log(address + " Direccion no encontrada");
	      }
	    });
}
var qaDir='';
var qaDirAnt='';
function dirAMapa()
{

	
	if(parseInt($("#appUseMap").attr("value"))==0){
		return false;
	}
	
	/*
	 * **********************************************************************************************************
	 * 
	 *  Agregamos funcionalidad de reemplazar las cadenas que contengan texto entre parentesis para
	 *  mejorar la busqueda con google.
	 *  asi por ejemplo si esta la colonia:  vista nueva (la vista)
	 *  quedara solo: vista nueva
	 *  @author: silvio.bravo@enova.mx
	 *  @date 27 feb 2014
	 *  @title: Reingenieria Mako.
	 * **********************************************************************************************************  
	 *  
	 * */
	//var edo 	= $($("#socio_id_estado > option")[$("#socio_id_estado").val()]).text();

	var edo;
	var mun;
	var calle;
	var numext;
	var cp;

	if(is_init){
		console.log('Inicio');
		is_init = false;
		edo 	= $("#socio_id_estado  option:selected").text();
		mun 	= $('#socio_municipio').val();
		col 	= $('#socio_colonia').val(); col = col.replace(/\((.)+\)/g, "");
	}else{
		edo 	= $("#socio_id_estado  option:selected").text();
		mun 	= $("#id_municipio  option:selected").text();
		mun 	= (mun != "Buscando municipios") ? mun : '';
		col 	= $("#id_colonia  option:selected").text();
		col 	= (col != "Buscando colonias") ? col : '';
	}
	

	calle 	= $('#socio_calle').val();
	numext 	= $('#socio_num_ext').val();
	cp 		= $('#socio_cp').val();
	




	console.log(' --------------------------------------------------------------- ');
	console.log(' --------------------------------------------------------------- ');
	console.log(' --------------------------------------------------------------- ');
	console.log(' --------------------------------------------------------------- ');
	console.log(' --------------------------------------------------------------- ');
	console.log(' - edo - ' + edo );
	console.log(' - mun - ' + mun );
	console.log(' - col - ' + col );
	console.log(' - calle - ' + calle );
	console.log(' - numext - ' + numext );
	
	//console.log(' - cp  - ' +$('#socio_cp').val() );
	console.log(' --------------------------------------------------------------- ');
	console.log(' --------------------------------------------------------------- ');
	console.log(' --------------------------------------------------------------- ');
	console.log(' --------------------------------------------------------------- ');
	console.log(' --------------------------------------------------------------- ');


	qaDir = "";
	qaDir2 = "";
	if(edo){
		qaDir = edo +', MEXICO';
		if(mun){
			qaDir = mun + ', ' + qaDir;
			if(col){
				qaDir = col + ', ' + qaDir;
			}
		}
	}
	if(cp)
		qaDir= cp + ', ' + qaDir;

	if(calle){
		qaDir2 = calle;
		if(numext){
			qaDir2 = numext + ' ' + qaDir2;
		}
	}
	console.log(qaDir);
	console.log(qaDir2);
	//if(cp)
	//	qaDir2 = qaDir2 + ', ' + cp;
	qaDir = qaDir2 + ', ' + qaDir;
	console.log(qaDir);
//	qaDir = calle+' '+numext+','+col+','+cp+','+mun+','+edo+', MEXICO';
	
	if(qaDir!=qaDirAnt)
	{
		qaDirAnt = qaDir;
		setAddress(qaDir);
	}
}
$(function() {
	
	latIni = ($("#socio_lat").val()=='')?$("#centro_lat").val():$("#socio_lat").val();
	longIni = ($("#socio_long").val()=='')?$("#centro_long").val():$("#socio_long").val();
	initialize();

	$(document).bind('georeferencia.modificada',function(ev,lat,long,dir){
		$("#socio_lat").val(lat);
		$("#socio_long").val(long);
		$("#socio_dirgmaps").val(dir);
	});		
});
