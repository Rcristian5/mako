$(document).ready(function(){
	
	
	$("#centro_cmbEstado").comboMakoCascade(
			{
				
				eventHandler				:"change",
				urlService					:"/registro/buscaDatosDireccion",
				method						:"POST",
				params						:{ "llave": "municipio" },
				optionValue			 		:"McpioId",
				optionLabel					:"McpioNombre",
				optionExtraValue			:"McpioNombre",
				comboCascade				:"#centro_cmbMunicipio",
				comboCascadeDefaultValue	:$("#idMunicipioVar").attr("value"),
				onComplete					: function(){
					
					if($("#centro_cmbEstado").comboMakoCascade('getOption',"comboCascadeDefaultValue")>0){
						$("#centro_cmbMunicipio").trigger("change");
					}
				},
				onChange					: function(element){
					
					$("#centro_id_colonia").html("");
					$("#centro_cp").attr("value","");
					setComboTextToInputText($(this), $("#centro_estado"));
				}
			}
		);
		
	
		$("#centro_cmbMunicipio").comboMakoCascade(
				
			{
				params						:{ "llave": "colonia" },
				eventHandler				:"change",
				urlService					:"/registro/buscaDatosDireccion",
				method						:"POST",
				optionValue			 		:"ClnaId",
				optionLabel					:"ClnaNombre",
				optionExtraValue			:"ClnaCp",
				comboCascade				:"#centro_id_colonia",
				comboCascadeDefaultValue	:$("#idColoniaVar").attr("value"),
				onComplete					: function(){
			
					
					if($("#centro_cmbMunicipio").comboMakoCascade('getOption',"comboCascadeDefaultValue")>0){
						$("#centro_id_colonia").trigger("change");
					}
				},
				onChange					: function(element){
					setComboTextToInputText($("#centro_cmbMunicipio"), $("#centro_municipio"));
					$("#centro_cp").attr("value","");
					
				}	
			}
		);
		
		
		$("#centro_id_colonia").change(function(){
			var cp=$(this).find('option:selected').attr("extra");
			setComboTextToInputText($(this), $("#centro_colonia"));
			$("#centro_cp").attr("value",cp);
			
		});
		
		$("#centro_cmbEstado").comboMakoCascade('setDefaultOption', $("#idEstadoVar").attr("value"));
		setComboTextToInputText($("#centro_cmbEstado"), $("#centro_estado"));
		$("#centro_cmbEstado").trigger("change");
	
		
		function setComboTextToInputText(comboComponent, inputComponent){
			var etiqueta=comboComponent.find("option:selected").text();
			inputComponent.attr("value", etiqueta);
		}
		
});