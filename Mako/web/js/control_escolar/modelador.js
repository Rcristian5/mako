/*
 * Script para control escolar
 */
var detail;
$(document).ready(function()
{
   //Llamada el plugin dataTables

   detail = $('#detalle').dataTable({
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
                    "bJQueryUI": true,
                    "sPaginationType": "full_numbers",
                    "aoColumns": [
                                    {"bSortable": false},
                                    {"bVisible" : false},
                                    null,
                                    null,
                                    null,
                                    {"bVisible" : false},
                                    {"bVisible" : false},
                                    {"bVisible" : false},
                                    {"bSortable": false}
                                 ],
                    "aaSorting": [[2, 'asc']]
                });
   topic = $('#detalle-temas').dataTable({
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
                    "bJQueryUI": true,
                    "sPaginationType": "full_numbers",
                    "aoColumns": [
                                    {"bSortable": false},
                                    null,
                                    null,
                                    null,
                                    {"bSortable": false},
                                    {"bSortable": false},
                                 ],
                    "aaSorting": [[2, 'asc']]
                });
   eval = $('#detalle-evaluaciones').dataTable({
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
                    "bJQueryUI": true,
                    "sPaginationType": "full_numbers",
                    "aoColumns": [
                                    {"bSortable": false},
                                    null,
                                    null,
                                    null,
                                    null,
                                    null,
                                    null,
                                    {"bSortable": false},
                                    {"bSortable": false},
                                 ],
                    "aaSorting": [[1, 'asc']]
                });
  $('#tab').tabs({
            cookie : {expires: 1}
  });

  //Llamada al plugin de columnas
  $('#windows-wrapper').windows({
                onMax : function()
                        {
                            detail.fnSetColumnVis( 1, true );
                            detail.fnSetColumnVis( 5, true );
                            detail.fnSetColumnVis( 6, true );
                            detail.fnSetColumnVis( 7, true );
                        },
                onMin : function()
                        {
                            detail.fnSetColumnVis( 1, false );
                            detail.fnSetColumnVis( 5, false );
                            detail.fnSetColumnVis( 6, false );
                            detail.fnSetColumnVis( 7, false );
                            detail.fnSort( [ [2,'asc']] );
                        }
                });
 });

/*
 * Declaración de funciones
 */

