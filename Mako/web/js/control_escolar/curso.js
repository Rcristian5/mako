/* 
 * Script para control escolar
 */
var oTable;
$(document).ready(function()
{
   //Llamada el plugin dataTables

   oTable = $('#detalle_curso').dataTable({
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
                    "bJQueryUI": true,
                    "sPaginationType": "full_numbers",
                    "aoColumns": [
                                    {"bSortable": false},
                                    {"bVisible" : false},
                                    null,
                                    null,
                                    {"bVisible" : false},
				    {"bVisible" : false},
                                    {"bVisible" : false},
                                    {"bVisible" : false},
                                    {"bVisible" : false},
                                    {"bVisible" : false},
                                    {"bVisible" : false},
                                    {"bSortable": false},
                                    {"bSortable": false}

                                 ],
                    "aaSorting": [[2, 'asc']]
                });
  //Llamada al plugin de columnas
  $('#windows-wrapper').windows({
                onMax : function()
                        {
                            oTable.fnSetColumnVis( 1, true );
                            oTable.fnSetColumnVis( 4, true );
                            oTable.fnSetColumnVis( 5, true );
                            oTable.fnSetColumnVis( 6, true );
                            oTable.fnSetColumnVis( 7, true );
                            oTable.fnSetColumnVis( 8, true );
                            oTable.fnSetColumnVis( 9, true );
                        },
                onMin : function()
                        {
                            oTable.fnSetColumnVis( 1, false );
                            oTable.fnSetColumnVis( 4, false );
                            oTable.fnSetColumnVis( 5, false );
                            oTable.fnSetColumnVis( 6, false );
                            oTable.fnSetColumnVis( 7, false );
                            oTable.fnSetColumnVis( 8, false );
                            oTable.fnSetColumnVis( 9, false );
                            oTable.fnSort( [ [2,'asc']] );
                        }
                });

   //Evento change para el checkbox "Curso seriado"
   $('#curso_seriado').change(toogleRuta);
   if($('#curso_seriado').attr('checked'))
   {
       toogleRuta();
   }

 });

/*
 * Declaración de funciones
 */

/*
 * toogleRuta
 * Oculta o muestra el tr del select que muestra el catalogo de cursos
 */
function toogleRuta()
{
    $('.ruta-aprendizaje').slideToggle(0);
}
