var disabledDays;
var disabledMesage;
var target = '';
$(document).ready(function() {
   
  var dias = ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];
  var meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio','Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
  //Plugin de calendario
  $('#calendar').fullCalendar({
        //Layout y vistas disponibles, deshabilitamos la vista por dia
        header          : {
                           left: 'today',
                           center: 'title',
                           right: 'prev,next '
                          },
        height          : 700,
        weekMode        :  'liquid',
        //Tema y traducción de textos
        theme           : true,
        dayNamesShort   : dias,
        monthNames      : meses,
        monthNamesShort : meses,
        buttonText      : {
                              prev:     '&nbsp;&#9668;&nbsp;',
                              next:     '&nbsp;&#9658;&nbsp;',
                              prevYear: '&nbsp;&lt;&lt;&nbsp;',
                              nextYear: '&nbsp;&gt;&gt;&nbsp;',
                              today:    '&nbsp;hoy&nbsp;',
                              month:    '&nbsp;&nbsp;&nbsp;mes&nbsp;&nbsp;&nbsp;',
                              week:     'semana',
                              day:      'día'
                          },
        allDayText      : 'día entero',
        //Día dee incio cambio Lunes
        firstDay        : 1,
        //Sias no habiles
        events          : disabledDays,
        dayClick: function(date, allDay, jsEvent, view)
            {
               $('tbody td').each(function(){
                    $(this).removeClass("fc-selected-day");
               });
               $('.fc-tip').each(function(){
                   $(this).remove();
               })
               if($(this).is(":not([class~=fc-other-month][class~=fc-sun])"))
               {
                   if(target!=$.fullCalendar.formatDate( date, "d" ))
                   {
                       var clase = "panel-select-day";
                       var imagen = '<img src="/imgs/tilde.png" />';
                       if($(this).is('[class~=fc-mon]'))
                       {
                          clase = "panel-select-day-monday";
                          imagen = '<img src="/imgs/tilde-l.png" />';
                       }
                       if($(this).is('[class~=fc-sat]'))
                       {
                          clase = "panel-select-day-saturday";
                          imagen = '<img src="/imgs/tilde-r.png" />';
                       }
                       if($(this).parent().is('[class~=fc-week0]') || $(this).parent().is('[class~=fc-week1]'))
                       {
                          clase = "panel-select-day-firts-week";
                          imagen = '<img src="/imgs/tilde-b.png" />';
                          if($(this).is('[class~=fc-mon]'))
                          {
                            clase = "panel-select-day-firts-week-monday";
                            imagen = '<img src="/imgs/tilde-l-b.png" />';
                          }
                          if($(this).is('[class~=fc-sat]'))
                          {
                            clase = "panel-select-day-firts-week-saturday";
                            imagen = '<img src="/imgs/tilde-r-b.png" />';
                          }
                       }
                       $(this).addClass("fc-selected-day");
                       $(this).append('<div class="fc-tip"><div class="ui-widget-content ui-corner-all fc-box-day sombra-delgada '+clase+'" style="z-index:110;"></div></div>');
                       $('.fc-box-day').append('<div class="ui-widget ui-state-default  ui-corner-all fc-close-box-day"><span class="ui-icon ui-icon-circle-close"></span></div>');
                       $('.fc-box-day').append('<span class="fc-load">&nbsp;</span>');
                       $('.fc-box-day').append(imagen);
                       $('.fc-box-day').append(loadForm(null, $.fullCalendar.formatDate( date, "dddd, dd ") +'de'+$.fullCalendar.formatDate( date, " MMMM ")+'de'+$.fullCalendar.formatDate( date, " yyyy"), $.fullCalendar.formatDate( date, "yyyy-MM-dd") ));
                       eventsBoxDay();
                       target = $.fullCalendar.formatDate( date, "d" );
                   }
                   else
                   {
                        target = "";
                   }
                   
               }
               jsEvent.stopImmediatePropagation();

            },
         eventClick : function( event, jsEvent, view ) {
               $('tbody td').each(function(){
                    $(this).removeClass("fc-selected-day");
               });
               $('.fc-tip').each(function(){
                   $(this).remove();
               });
               console.log($(this));
               if(target!=$.fullCalendar.formatDate( event.start, "d" ))
               {
                   var top = $(this).css('top').substring(0, $(this).css('top').length-2);
                   var left = $(this).css('left').substring(0, $(this).css('left').length-2);
                   var clase = "tpanel-select-day";
                   var imagen = '<img src="/imgs/tilde.png" />';
                   if(left < 100)
                   {
                      clase = "tpanel-select-day-monday";
                      imagen = '<img src="/imgs/tilde-l.png" />';
                   }
                   if(left > 700)
                   {
                      clase = "tpanel-select-day-saturday";
                      imagen = '<img src="/imgs/tilde-r.png" />';
                   }
                   if(top < 200)
                   {
                       clase = "tpanel-select-day-firts-week";
                       imagen = '<img src="/imgs/tilde-b.png" />';
                   }
                   if(top < 200 && left < 100)
                   {
                       clase = "tpanel-select-day-firts-week-monday";
                       imagen = '<img src="/imgs/tilde-l-b.png" />';
                   }
                   if(top < 200 && left > 700)
                   {
                       clase = "tpanel-select-day-firts-week-saturday";
                       imagen = '<img src="/imgs/tilde-r-b.png" />';
                   }
                   $(this).append('<div class="fc-tip"><div class="ui-widget-content ui-corner-all fc-box-day sombra-delgada '+clase+'"></div></div>');
                   $('.fc-box-day').append('<div class="ui-widget ui-state-default  ui-corner-all fc-close-box-day"><span class="ui-icon ui-icon-circle-close"></span></div>');
                   $('.fc-box-day').append('<span class="fc-load">&nbsp;</span>');
                   $('.fc-box-day').append(imagen);
                   $('.fc-box-day').append(loadForm(event.ident, $.fullCalendar.formatDate( event.start, "dddd, dd ") +'de'+$.fullCalendar.formatDate( event.start, " MMMM ")+'de'+$.fullCalendar.formatDate( event.start, " yyyy"), $.fullCalendar.formatDate( event.start, "yyyy-MM-dd") ));
                   eventsBoxDay();
                   target = $.fullCalendar.formatDate( event.start, "d" );
              }
              else
              {
                target = "";
              }
              jsEvent.stopImmediatePropagation();
         }
  });

$('#windows-wrapper').windows({
                        resizable : true,
                        minimizable: false,
                        minheight : 700
                    });

  $('.fc-button-prev, .fc-button-next, .fc-button-today').mousedown(function(){
      $('tbody td').each(function(){
        $(this).removeClass("fc-selected-day");
       });
      $('.fc-tip').each(function(){
            $(this).remove();
      });
  });

});

function eventsBoxDay()
{
    $(".fc-box-day").click(function(e){
        e.stopImmediatePropagation();
    });
    $(".fc-close-box-day, .fc-box-day img").click(function(){
         $('tbody td').each(function(){
                    $(this).removeClass("fc-selected-day");
                    $('.fc-box-day').remove();
         });
    });
}
function loadForm(evento, dia, fecha)
{
    $.ajax({
          url : 'dias_laborables/form',
          data : {evento : evento, fecha: fecha, dia : dia},
          beforeSend : function(){
            $('.fc-load').show();
          },
          success: function(data) {
            $('.fc-load').hide();
            $('.fc-box-day').append(data);
            pegaHandlers();
          },
          error: function(data){
            console.error(data);
          }
        });
}

/* utility functions */
function noLaborables(date) {
	var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();
	
	for (i = 0; i < disabledDays.length; i++) {
		if($.inArray((m+1) + '-' + d + '-' + y,disabledDays) != -1 ) {
			return [false,'', disabledMesage[i]];
		}
	}
	return [true];

}
function comparaRequeridos()
{
    if (requeridos() && validaTels())
    {
        return true;
    }
    else
    {
        return false;
    }

}
function showNoLaborales(date)
{
    //var lastYearEnd = new Date(date.getFullYear() - 1, 12 - 1, 31, 0, 0, 0, 0);
    //var doy = Math.floor(((date.getTime() - lastYearEnd.getTime()) / 86400000) + 0.25);
    return [true, '', 'Dia no laboral'];
}