$(function() {

  $("#accordion-anotaciones").accordion({
      collapsible: true
    });
    $('.help').each(function(){
        $(this).qtip({
        content: $(this).attr('q-title'),
        style:  {
              background: '#fcf0ba url(/css/southstreet/images/ui-bg_glass_55_fcf0ba_1x400.png) 50% 50% repeat-x',
              color: '#363636',
              textAlign: 'left',
              width: 400,
              border: {
                width: 3,
                radius: 3,
                color : '#e8e1b5'
                },
              title: {
                width  : '100%',
                height : 20,
                background: '#e8e1b5',
                padding: 0,
                color: '#363636'
              },
              padding: 10,
              tip: true,
              name: 'green'
          },
          show: {
                delay: 0,
                when: {
                    event: 'click'
                  }
              },
          hide: {
                delay: 0,
                when: {
                  event: 'unfocus'
                }
              },
          position: {
              corner: {
              target: 'bottomMiddle',
              tooltip: 'topMiddle'
                  }
           }
    });
  });

});

function maximizar()
{
  $('.radio_list li label').css('width', '110px');
  $('.radio_list li').css('margin', '0 35px');
}

function minimizar()
{
  $('.radio_list li label').css('width', '60px');
  $('.radio_list li').css('margin', '5px');
}

function comparaRequeridos()
{
    if (requeridos() && validaTels())
    {
        return true;
    }
    else
    {
        return false;
    }

}