$(function() {
  
  //Calendario en linea
  $.datepicker.setDefaults($.extend({showMonthAfterYear: false}, $.datepicker.regional['']));
  $("#inline-calendar").datepicker($.datepicker.regional['es']);
  var fecha =$("#inline-calendar").datepicker( "getDate" )
  var dias = ['Domingo','Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado']
  $('#day-number span').text(fecha.getDate());
  $('#day-name').text(dias[fecha.getDay()]);
  $('#datos-grupo a').click(function(){

    return false;
  });
  //Qtip para los enlaces
  $('#datos-grupo a').qtip(
   {
        content: {
                  text: '',
                     title: {
                        text: '',
                        button: '<span class="qtip-icon ui-icon-close"> </span>'
                  }},
        style:  {
            background: '#fcf0ba url(/css/southstreet/images/ui-bg_glass_55_fcf0ba_1x400.png) 50% 50% repeat-x',
            color: '#363636',
            textAlign: 'left',
            width: 650,
            border: {
              width: 3,
              radius: 3,
              color : '#e8e1b5'
              },
            title: {
              width  : '100%',
              height : 20,
              background: '#e8e1b5',
              padding: 0,
              color: '#363636'
            },
            padding: 10,
            tip: true,
            name: 'green'
        },
        show: {
              delay: 0,
              when: {
                  event: 'click'
                }
            },
        hide: {
              delay: 0,
              when: {
                event: 'unfocus'
              }
            },
        position: {
            corner: {
            target: 'bottomMiddle',
            tooltip: 'topMiddle'
                }
         },
         api: {
               onRender: function()
             {
                var id = $(this).attr('id');
                var self = this;
                var title =  '<span class="qtip-title-text">'+cursos[id].curso+'</span>';
                var contenido = '<div class="qtip-info">';
                contenido += '<p><b>Aula: </b>'+cursos[id].aula+'</p>';
                contenido += '<p class="center"><b>Fecha de inicio: </b>'+cursos[id].inicio+'</p>';
                contenido += '<p class="right"><b>Cupo: </b>'+cursos[id].cupo+'</p>';
                contenido += '<p class="clear">&nbsp;</p>';
                contenido += '</div>';
                contenido += '<div class="qtip-botones">';
                contenido += '<input type="button" value="Registro de asistencia" onclick="location.href=\''+cursos[id].asistencia+'\'">';
                contenido += '<input type="button" value="Registro de evaluaciones" onclick="location.href=\''+cursos[id].evaluaciones+'\'">';
                contenido += '<input type="button" value="Bitácora de socio" onclick="location.href=\''+cursos[id].bitacora+'\'">';
                contenido += '<input type="button" value="Estudio cualitativo" onclick="location.href=\''+cursos[id].cualitativo+'\'">';
                contenido += '</div>';
                self.updateContent(contenido);
                self.updateTitle(title);
             }

         }
   });


});