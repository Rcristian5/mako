var oTable;
$(document).ready(function()
{

  //Llamada al plugin de columnas
  $('#windows-wrapper').windows({
                minheight : 700,
                onMax : function()
                        {
                            oTable.fnSetColumnVis( 4, true );
                            oTable.fnSetColumnVis( 5, true );
                        },
                onMin : function()
                        {
                            oTable.fnSetColumnVis( 4, false );
                            oTable.fnSetColumnVis( 5, false );
                            oTable.fnSort( [ [1,'asc']] );
                        }
                });
   /*
       * Imagenes para mostrar datos adicionales
       */
      var nCloneTh = document.createElement( 'th' );
      var nCloneTd = document.createElement( 'td' );
      nCloneTd.innerHTML = '<span class="ui-icon ui-icon-plus"></span>';

      $('#detalle_recurso thead tr').each( function () {
                this.insertBefore( nCloneTh, this.childNodes[0] );
      } );

      $('#detalle_recurso tbody tr').each( function () {
                this.insertBefore(  nCloneTd.cloneNode( true ), this.childNodes[0] );
      } );


   //Llamada el plugin dataTables

   oTable = $('#detalle_recurso').dataTable({
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
                    "bJQueryUI": true,
                    "sPaginationType": "full_numbers",
                    "aoColumns": [
                                    {"bSortable": false},
                                    null,
                                    null,
                                    null,
                                    {"bVisible":    false},
                                    {"bVisible":    false}
                                 ],
                    "aaSorting": [[1, 'asc']]
                });
   /* Add event listener for opening and closing details
       * Note that the indicator for showing which row is open is not controlled by DataTables,
       * rather it is done here
       */
      $('td span.ui-icon', oTable.fnGetNodes() ).each( function () {
                $(this).click( function () {

                          var nTr = this.parentNode.parentNode;
                          var id = $(this).parent().parent().attr('id');
                          if ( $(this).hasClass('ui-icon-minus') )
                          {
                                    $(this).removeClass('ui-icon-minus');
                                    $(this).addClass('ui-icon-plus');
                                    oTable.fnClose( nTr );
                          }
                          else
                          {
                                    $(this).removeClass('ui-icon-plus');
                                    $(this).addClass('ui-icon-minus');
                                    oTable.fnOpen(nTr, fnFormatDetails(nTr,id), 'details' );
                          }
                } );
        } );

        comparaSeleccion();
        //If para daber si esta marcado como aula compuesta
        $('#recurso_infraestructura_compuesta').change(function (){
          comparaSeleccion();
        });


});

/*
 * Declaración de funciones
 */

/*Funcion para mostrar mas detalles en la tabla
 *
 */
function fnFormatDetails(nTr, id)
{
	var aData = oTable.fnGetData(nTr );
          var sOut = '<div class="tabla-opciones ui-state-highlight ui-corner-all">';
          sOut += '<h4>Opciones para el recurso: <b>'+aData[4]+'</b></h4>';
          sOut += '<p>';
          sOut += '<input type="button" value="Editar datos del recurso" onclick="location.href=\'/recursosinf/edit?id='+id+'\'" />';
          //sOut += '<input type="button" value="Dar de baja este recurso" onclick="location.href=\'/temario?id='+id+'\'" />';
	sOut += '</p';
	sOut += '</div>';

	return sOut;
}
function comparaSeleccion()
{
  if($('#recurso_infraestructura_compuesta').is(':checked'))
          {
            $('.compuesta').show();
            $('.compuesta-sel').attr('requerido', 1);
          }
          else
          {
            $('.compuesta').hide();
            $('.compuesta-sel').attr('requerido', 0);
          }
}
/*
 * comparaRequeridos
 * Comprueba que todos los datos requeridos esten capturados
 */
function comparaRequeridos()
{
    if (requeridos() && validaTels())
    {
        return true;
    }
    else
    {
        return false;
    }

}