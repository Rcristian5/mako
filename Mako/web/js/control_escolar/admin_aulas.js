/*
 * Script para control escolar
 */
var oTable;
$(document).ready(function()
{
   //Llamada el plugin dataTables

   oTable = $('#detalle-aulas').dataTable({
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
                    "bJQueryUI": true,
                    "sPaginationType": "full_numbers",
                    "aoColumns": [
                                    {"bSortable": false},
                                    null,
                                    null,
                                    {"bVisible": false},
                                    {"bVisible": false},
                                    {"bVisible": false},
                                    {"bSortable": false},
                                    {"bSortable": false}

                                 ],
                    "aaSorting": [[1, 'asc']]
                });
                
  //Llamada al plugin de columnas
  $('#windows-wrapper').windows({
                onMax : function()
                        {
                            oTable.fnSetColumnVis( 3, true );
                            oTable.fnSetColumnVis( 4, true );
                            oTable.fnSetColumnVis( 5, true );
                        },
                onMin : function()
                        {
                            oTable.fnSetColumnVis( 3, false );
                            oTable.fnSetColumnVis( 4, false );
                            oTable.fnSetColumnVis( 5, false );
                            oTable.fnSort( [ [1,'asc']] );
                        }
                });
 });

