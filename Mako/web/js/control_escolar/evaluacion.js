$(document).ready(function()
{
  comprobarTipo();
  $("#slider-porcent").slider({
            range: "min",
            value: 1,
            min: 1,
            max: 100,
            slide: function(event, ui) {
                      $("#evaluacion_porcentaje").val(ui.value);
            }
  });
  $("#evaluacion_porcentaje").keyup (function(e){
    if($(this).val()<101)
    {
      $("#slider-porcent").slider("value",$(this).val());
    }
  });
  $('#evaluacion_tipo').change(function(){
    comprobarTipo()
  });
});

function comprobarTipo()
{
  if($('#evaluacion_tipo').val()==1)
  {
    $('.form-capitulo').show();
    $('.form-tema').hide();

  }
  else
  {
    $('.form-capitulo').hide();
    $('.form-tema').show();

  }
}