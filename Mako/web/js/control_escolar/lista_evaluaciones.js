function maximizar()
{
  eTable.fnSetColumnVis( 1, true );
  eTable.fnSetColumnVis( 2, true );
  eTable.fnSetColumnVis( 3, true );
}

function minimizar()
{
  eTable.fnSetColumnVis( 1, false );
  eTable.fnSetColumnVis( 2, false );
  eTable.fnSetColumnVis( 3, false );
  eTable.fnSort( [ [0,'asc']] );
}
