
$(document).ready(function()
{

  //Llamada al plugin de columnas
  $('#windows-wrapper').windows({
                            resizable : true,
                            minheight : 700
                        });
                        
  $("#expediente_alumno_id").val( $("#expediente_matricula").val());
  $('#resultados-socio .forma-buscador-socios').remove();
  $('#resultados-socio hr').remove();
  $("#liga-sel-"+$("#expediente_alumno_id").val()).text();
  $("#liga-sel-"+$("#expediente_alumno_id").val()).text('Seleccionado').css('color','red');
  var $wBuscador = $("#ventana-buscador-socios").dialog({
			modal: true,
			minWidth: 250,
			width: 320,
			maxWidth: 400,
			minHeight: 350,
			height: 600,
			maxHeight: 710,
			autoOpen: ($("#expediente_alumno_id").val()!='')?false:true,
			position: ['center','top'],
			closeOnEscape: false,
			title:	'Seleccione socio'
		});

   $('#buscar-nuevo').click(function(){
     $wBuscador.dialog('open');
   });
      //Bindings de eventos a escuchar de la ventana
      $("#ventana-buscador-socios").bind('dialogclose',function(){
                if($("#expediente_alumno_id").val() == '')
                {
                          alert('Necesitas seleccionar un socio para obtener su expediente');
                }
      });

      //Escuchamos evento del buscador, socio.seleccionado
      $(document).bind('socio.seleccionado',function(ev, socio_id, socio_nombre){

                $("#expediente_alumno_id").val( socio_id);
                //Deshabilitamos la liga de seleccionar y ponemos un msg para indicar que ese socio se ha seleccionado
                $("#liga-sel-"+socio_id).text();
                $("#liga-sel-"+socio_id).text('Seleccionado').css('color','red');

                if($wBuscador.dialog('isOpen'))
                {
                          $wBuscador.dialog('close');
                          //$("#autocomp_productos").focus();
                }
                 location.href="/control/registro?id="+socio_id;

      });

      //Escuchamos cuando se ejecuta el evento de búsqueda
      $(document).bind('socios.buscador.buscando',function(){
                console.debug('Buscando...');
                $("#socio_id").val("");
                $("#nombre").html("");
                $("#vp-socio-seleccionado").hide('slow');
      });

      //Escuchamos cuando se termina
      $(document).bind('socios.buscador.encontrados',function(){

                console.debug('Terminando...');
      });

});