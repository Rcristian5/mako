var disabledDays;
var disabledMesage;
var afterday;
var selectday='';
$(document).ready(function() {

   
  var meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio','Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
  var dias = ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];

  //Plugin de calendario
  $('#calendar').fullCalendar({
        //Layout y vistas disponibles, deshabilitamos la vista por dia
        header          : {
                           left: 'prev,next today',
                           center: 'title',
                           right: 'month,agendaWeek'
                          },
        height          : 700,
        weekMode        :  'liquid',
        
        //Tema y traducción de textos
        theme           : true,
        monthNames      : meses,
        monthNamesShort : meses,
        dayNames        : dias,
        dayNamesShort   : dias,
        buttonText      : {
                              prev:     '&nbsp;&#9668;&nbsp;',
                              next:     '&nbsp;&#9658;&nbsp;',
                              prevYear: '&nbsp;&lt;&lt;&nbsp;',
                              nextYear: '&nbsp;&gt;&gt;&nbsp;',
                              today:    '&nbsp;hoy&nbsp;',
                              month:    '&nbsp;&nbsp;&nbsp;mes&nbsp;&nbsp;&nbsp;',
                              week:     'semana',
                              day:      'día'
                          },
        allDayText      : 'día entero',
        //Tiempio mínimo y máximo
        minTime         : 8,
        maxTime         : 22,
        //Día dee incio cambio Lunes
        firstDay        : 1,
        events          : disabledDays,
        dayClick: function(date, allDay, jsEvent, view) {
            var fecha = $.fullCalendar.formatDate( date, 'yyyy-MM-dd');
            var isIn = $.inArray(fecha, disabled);
            if (isIn<0)
            {
                  console.log(date);
                  console.log($(this).attr('class'));
                  console.log(fecha);
                  if(afterday!='')
                  {
                    $(afterday).removeClass('fc-selected-day');
                  }
                  afterday = $(this);
                  $(this).addClass('fc-selected-day');
                  selectday = fecha;
            }
            
            // change the day's background color just for fun
            //$(this).css('background-color', 'red');

        },

        //Comprobamos que el dia al que se calendariza un nuevo cursos no este deshabilitado
        eventDrop      : function(event, dayDelta, minuteDelta, allDay, revertFunc) {
               var fecha = $.fullCalendar.formatDate( event.start, 'yyyy-MM-dd');
               var today = event.start;
               var isIn = $.inArray(fecha, disabled);
               if (isIn>=0)
               {
                  revertFunc();
               }
               var curso = $('#calendar').fullCalendar( 'clientEvents', function(calEvent){
                                                                           return calEvent.start == ""+today+"";
                                                                      });
               if(curso.length>1)
               {
                  var nueva_fecha =  $.fullCalendar.formatDate( event.start, 'yyyy-MM-dd');
                  var nueva_hora = parseInt($.fullCalendar.formatDate( event.start, 'H'),10)+1;
                  if(nueva_hora<10)
                  {
                     nueva_hora = '0'+nueva_hora;
                  }
                  
                  var fecha_search = $.fullCalendar.parseDate(nueva_fecha+' '+nueva_hora+':00');
                  var search;
                  var flag = 1;
                 while(flag>0){
                      search = $('#calendar').fullCalendar( 'clientEvents', function(calEvent){
                                                                           return calEvent.start == fecha_search.toString();
                                                                      });
                      flag = search.length;
                      if(flag>0)
                      {
                        nueva_fecha =  $.fullCalendar.formatDate( fecha_search, 'yyyy-MM-dd');
                        nueva_hora = parseInt($.fullCalendar.formatDate( fecha_search, 'H'),10)+1;
                        if(nueva_hora<10)
                        {
                          nueva_hora = '0'+nueva_hora;
                          
                        }
                        fecha_search = $.fullCalendar.parseDate(nueva_fecha+' '+nueva_hora+':00');
                      }
                    }

                 var hora_fin = parseInt(nueva_hora,10)+1;
                 if(hora_fin<10)
                        {
                          hora_fin = '0'+hora_fin;
                        }

                 event.start = nueva_fecha+' '+nueva_hora+':00';
                 event.end =   nueva_fecha+' '+hora_fin+':00';
                 
                 $('#calendar').fullCalendar('updateEvent', event);
               }
               //Se destruye el tooltip, de no hacerlo este se clona a la nueva posición
              

            },
        eventDragStop : function ( event, jsEvent, ui, view ) {
           $(this).qtip("destroy");
        },
       
        // disableResizing: true,
         eventResizeStart : function(event, jsEvent, ui, view) {
                if(view.name == 'month')
                {
                  ui.resizable( "disable" );
                }
            },
        eventRender: function(event, element) {
          var nombre = 'green';
          var className = event.className;
          var cupo = event.cupo;
          var contenido = {
                     text: event.description,
                     title: {
                        text: '',
                        button: '<span class="qtip-icon ui-icon-close"> </span>'
                     }
                    };
          var estilo = {
                      background: '#f5f3e5',
                      color: '#3e3e3e',
                      textAlign: 'left',
                      width: 400,
                      border: {
                        width: 3,
                        radius: 3,
                        color : '#847400'
                        },
                      title: {
                        width: '100%',
                        background: '#847400',
                        padding: 0
                      },
                      padding: 10,
                      tip: true,
                      name: nombre
                      };
          if(className == 'fc-disable')
          {
            contenido = event.description;
            nombre = 'red'
            estilo = {
                      background: '#CCC',
                      color: '#3e3e3e',
                      textAlign: 'left',
                      border: {
                        width: 3,
                        radius: 3,
                        color : '#6f6f6f'
                        },
                      padding: 10,
                      tip: true,
                      name: nombre
                      };
          }
          if(className == 'fc-nuevo-curso')
          {

            nombre = 'green'
            estilo = {
                      background: '#459e00',
                      color: '#FFF',
                      textAlign: 'left',
                      border: {
                        width: 3,
                        radius: 3,
                        color : '#397f03'
                        },
                      title: {
                        width: '100%',
                        background: '#397f03',
                        padding: 0
                      },
                      padding: 10,
                      tip: true,
                      name: nombre
                      };
          }
          element.qtip({
            content: contenido,
            style: estilo,
            show: {
                    delay: 0,
                    when: {
                        event: 'click'
                      }
                  },
            hide: {
                    delay: 0,
                    when: {
                      event: 'unfocus'
                    }
                  },
            position: {
              corner: {
                target: 'bottomMiddle',
                tooltip: 'topMiddle'
                      }
                   },
                 api: {
                  onShow: function(event) {
                    if(className[0] == 'fc-calendarizados')
                    {
                       $(".fc-calendarizados").animate({
                          opacity: 0.3
                        }, 0 );
                       $("."+className[1]).animate({
                          opacity: 1
                        }, 0 );
                        if(cupo==0)
                        {
                          $('.qtip-cancel').show();
                        }
                        else
                        {
                          $('.qtip-change').show();
                        }
                        $('.edit-facilitador').click(function(){
                            $(this).hide();
                            editarFacilitador($(this).attr('grupo'), $(this).prev());
                          });
                    }
                  },
                  onHide: function(event) {
                       $('.qtip-cancel').hide();
                       $('.qtip-change').hide();
                       $(".fc-calendarizados").animate({
                        opacity: 1
                    }, 0 );
                  }
            }
         });
        
      }

    });
    
    if(actualDay.length >0)
    {
     $('#calendar').fullCalendar( 'addEventSource', actualDay);
     $('#calendar').fullCalendar( 'gotoDate',  selyear,selmoth,selday);

    }
    if(cursosCalendarizados.length >0)
    {
     $('#calendar').fullCalendar( 'addEventSource', cursosCalendarizados);
    }
     //Plugin de ventanas
   $('#windows-wrapper').windows({
                        resizable : true,
                        minimizable: false
                        //minheight : $('#calendar').height()
                    });
//     /*
//      * Ventana de seleccion de curso
//      */
      $("#ventana-curso").dialog({
                        modal: true,
                        minWidth: 250,
                        width: 700,
                        maxWidth: 900,
                        minHeight: 150,
                        height: 300,
                        maxHeight: 500,
                        autoOpen: false,
                        position: ['center','top'],
                        closeOnEscape: false,
                        title: 'Seleccione un curso'
	});
      $('#abrir-ventana').click(function(){
        $("#ventana-curso").dialog('open');
      });

      $('#comprobar').click(function(){
        //Funcion para comproar si lodias estan disponibles
        var boxes = $('.checkbox_list li input:checked').length;
        if(boxes>0)
        {
          comprobarDias();
        }
        else
        {
          alert('Debes seleccionar al menos un día');
        }
      });

      //Evento click para el boton  que limpia los dias seleccionados
      $('#limpiar-seleccion').click(function(){
        $('#calendar').fullCalendar( 'removeEvents','preview-curso' );
      });

      //Evento click para el boton  guardar
      $('#guardar').click(function(){
        guardarDias();
      });
      $('#categorias-curso').change(function(){
        if($(this).val()==0)
        {
          $(".fc-calendarizados").show();
        }
        else
        {
          $(".fc-calendarizados").hide();
          $(".fc-categoria-"+$(this).val()).show();
        }
      
    });
});

/* utility functions */
function noLaborables(date) {
	var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();

	for (i = 0; i < disabledDays.length; i++) {
		if($.inArray((m+1) + '-' + d + '-' + y,disabledDays) != -1 ) {
			return [false,'', disabledMesage[i]];
		}
	}
	return [true];

}

/*
 * moveDias
 * Selecciona o elimina todos los dias, segun el valor del checbox
 *
 */
function comprobarDias() {
	//Se eliminan los eventos preview, si los existiera para evitar traslapar dias
        $('#calendar').fullCalendar( 'removeEvents','preview-curso' );
        //Se crea un array con los dias seleccionados
        var dias = [];
        var i=1;
        var fecha;
        var curso = $('#calendar').fullCalendar( 'clientEvents', 'nuevo-curso')[0];
        var start = curso.start;
        var end = curso.end;
        var m_inicio = $.fullCalendar.formatDate( start, 'm');
        var h_inicio = $.fullCalendar.formatDate( start, 'H');
        var m_fin = $.fullCalendar.formatDate( end, 'm');
        var h_fin = $.fullCalendar.formatDate( end, 'H');
        var min = parseInt(m_fin,10) - parseInt(m_inicio,10);
        if(min==30)
        {
          min = 0.5;
        }
        var hour = parseInt(h_fin,10) - parseInt(h_inicio,10);
        hour = hour + min;
        $('.checkbox_list li input:checkbox').each(function(){
            if($(this).is(':checked'))
            {
              dias.push(""+$(this).val()+"");
            }
        });
        var duracion = duracionTotal;
        var sig;
        var sigh;
        var prev = [];
        var inicio = new Date($.fullCalendar.formatDate( start, 'yyyy'),$.fullCalendar.formatDate( start, 'MM')-1,$.fullCalendar.formatDate( start, 'dd'), $.fullCalendar.formatDate( start, 'HH'), $.fullCalendar.formatDate( start, 'mm') );
        var fin = new Date($.fullCalendar.formatDate( end, 'yyyy'),$.fullCalendar.formatDate( end, 'MM')-1,$.fullCalendar.formatDate( end, 'dd'), $.fullCalendar.formatDate( end, 'HH'), $.fullCalendar.formatDate( end, 'mm') );
        duracion = duracion-hour;
        while(duracion>0)
        {
           sig= new Date(inicio.getFullYear(),inicio.getMonth(),(inicio.getDate()+i),inicio.getHours(),inicio.getMinutes());
           sigh= new Date(fin.getFullYear(),fin.getMonth(),(fin.getDate()+i),fin.getHours(),fin.getMinutes());
           fecha = $.fullCalendar.formatDate( sig, 'yyyy-MM-dd');
           //Se comprueba que el dia no sea un dia no laborable
           var isIn = $.inArray(fecha, disabled);
           if (isIn<0)
           {
                //Se comprueba que el dia sea un dia seleccionado
                var s = $.inArray(sig.getDay().toString(), dias);
                if(s>=0)
                {
                  prev.push({
                      id:         'preview-curso',
                      title       : curso.title,
                      start       : $.fullCalendar.formatDate(sig,'yyyy-MM-dd HH:mm'),
                      end         : $.fullCalendar.formatDate(sigh,'yyyy-MM-dd HH:mm'),
                      allDay      : false,
                      className   : 'fc-nuevo-curso',
                      editable    : false
                 });
                duracion = duracion - hour;
               }
           }
          i++;
        }
        //Se muestran los botones de guardar y limpiar
        facilitadores();
        $('#limpiar-seleccion').show();
        $('#guardar').show();
        $('#select-facilitador').show();

        $('#calendar').fullCalendar( 'addEventSource', prev);
}

function facilitadores()
{
  var calendario = $('#calendar').fullCalendar( 'clientEvents', function(calEvent){
                                                                 return calEvent.className == "fc-nuevo-curso";
                                                            });
  var fechas = new Array();
  var horas_inicio = new Array();
  var horas_fin = new Array();
  for (var i in calendario)
  {
     fechas[i] = $.fullCalendar.formatDate(calendario[i].start, 'yyyy-MM-dd');
     horas_inicio[i] = $.fullCalendar.formatDate(calendario[i].start, 'HH:mm'),
     horas_fin[i] =  $.fullCalendar.formatDate(calendario[i].end, 'HH:mm');
  }
  $.ajax({
        url:"/calendarizacion/facilitadores",
        dataType: "json",
        data   :{dates  : implode(fechas),
                 inicio : implode(horas_inicio),
                 fin    : implode(horas_fin)
               },
        success: function(data){
                  var i;
                  $('#facilitador').html('');
                  var option = '<option value="">--Selecciona un facilitador--</option>'
                  $('#facilitador').append(option);
                  for(i=0;i<=data.length;i++)
                  {
                    option = '<option value="'+data[i].id+'">'+data[i].nombre+'</option>'
                    $('#facilitador').append(option);
                  }
                },
        error  : function(e){
                console.log(e);
              }
    });
}
function editarFacilitador(grupo, elemento)
{
  var opciones
  elemento.prev().hide();
  $.ajax({
        type       : "POST",
        url        :"/calendarizacion/cambiarFacilitadores",
        data       :{grupo : grupo},
        dataType   : "json",
        success    : function(data){
                      var select = '<select id="cambio-facilitador">'
                      var i
                      for(i=0;i<=data.length-1;i++)
                      {
                        opciones += '<option value="'+data[i].id+'">'+data[i].nombre+'</option>';
                      }
                      select += opciones;
                      select +='</select>';
                      select +='<br> <input type="button" id="facilitador-update" value="actualizar" style="margin-left:87px;">';
                      select +='<input type="button" id="facilitador-cancel" value="canclear">';
                      elemento.html(select);
                      $('#facilitador-update').click(function(){
                          location.href='editFacilitador?id='+$('#cambio-facilitador').val();
                      });
                      $('#facilitador-cancel').click(function(){
                        elemento.html('');
                        elemento.prev().show();
                        elemento.next().show();
                      });
                   },
        error      : function(e){
                    console.log(e);
                   }
          });
}

/*
 * moveDias
 * Selecciona o elimina todos los dias, segun el valor del checbox
 */
function moveDias()
{
    var dia = $(this).attr('checked');
    if(dia == false)
    {
        moveList('horario_dias_list', 'unassociated_horario_dias_list');
    }
    else
    {
        moveList('unassociated_horario_dias_list', 'horario_dias_list');
    }
}

/*
 * moveList
 * Mueve todos los dias a la caja "Dias seleccionados" o "Lista de dias".
 * Fragmento tomado del archivo double_list.js y fixeado para cumplir esta funcion
 */
function moveList(srcId, destId)
  {
    var src = document.getElementById(srcId);
    var dest = document.getElementById(destId);
    for (var i = 0; i < src.options.length; i++)
    {
        dest.options[dest.length] = new Option(src.options[i].text, src.options[i].value);
        src.options[i] = null;
        --i;
    }
  }

/*
 * comporbarTodos
 * Comprueba que todos los centros esten seleccionados, si lo estan marca como
 * checked el checkbox #curso_centros_todos
 */
function comprobarTodos()
{
    var noAsociados = document.getElementById('unassociated_horario_dias_list');
    if(noAsociados.options.length==0)
    {
        $('#horario_todos_dias').attr('checked', true);
    }
    else
    {
        $('#horario_todos_dias').attr('checked', false)
    }
}

/*
 * confirmDelete
 * Confirma si se quiere o no eliminar el curso calendarizado
 * id el id del grupo
 */
function confirmDelete(id)
{
    if(confirm('¿Estás seguro de querer borrar este curso calendarizado?'))
    {
      location.href='delete?id='+id;
    }
}

/*
 * confirmCahnge
 * Redireccioana a transferir
 * id el id del grupo
 */
function confirmChange(id)
{
   location.href='transferir?id='+id;
}
/*
 * guardarDias
 * Guarda los dias elegidos para dar un nuevo curso
 */
function guardarDias()
{
  if($('#facilitador').val()=='')
  {
    alert('Selecciona un facilitdor');
  }
  else
  {
    var calendario = $('#calendar').fullCalendar( 'clientEvents', function(calEvent){
                                                                 return calEvent.className == "fc-nuevo-curso";
                                                            });
  var fechas = new Array();
  var horas_inicio = new Array();
  var horas_fin = new Array();
  for (var i in calendario)
  {
     fechas[i] = $.fullCalendar.formatDate(calendario[i].start, 'yyyy-MM-dd');
     horas_inicio[i] = $.fullCalendar.formatDate(calendario[i].start, 'HH:mm'),
     horas_fin[i] =  $.fullCalendar.formatDate(calendario[i].end, 'HH:mm');
  }
  $.ajax({
      type       : "POST",
      url        :"/calendarizacion/ajaxFechas",
      data       :{dates  : implode(fechas),
                  inicio : implode(horas_inicio),
                  fin    : implode(horas_fin),
                  facilitador : $('#facilitador').val()
                  },
      dataType   : "json",
      beforeSend : function(){
                  $('.botones_finales').hide();
                 },
      success    : function(){
                  location.href='new';
                 },
      error      : function(e){
                  location.href='new';
                  console.log(e);
                 }
        });
  }
}

/*
 * comparaRequeridos
 * Compara los calores requeridos
 */
function comparaRequeridos()
{
    if (requeridos() && validaTels())
    {
        return true;
    }
    else
    {
        return false;
    }

}

function implode(array)
{
  var i;
  var imploded=array[0];
  for (i=1; i<array.length; i++)imploded += '|' + array[i];

  return imploded
}
/* utility functions */

