$(document).ready(function()
{

  //Llamada al plugin de columnas
  $('#windows-wrapper').windows({
                            resizable : false,
                            minimizable: false,
                            minheight : 900
                        });
  $("#accordion-grupos").accordion({autoHeight: false});
  // let the gallery items be draggable
  var $alumnos = $('#alumnos-curso');
  var $cursos = $('.drop-here');
  $('div',$alumnos).draggable({
            cancel: 'a.ui-icon',
            revert: 'invalid', 
            containment: $('#demo-frame').length ? '#demo-frame' : 'document', // stick to demo-frame if present
            helper: 'clone',
            cursor: 'move'
  });
  // let the trash be droppable, accepting the gallery items
  $cursos.droppable({
            accept: '#alumnos-curso > div',
            activeClass: 'ui-state-highlight',
            drop: function(ev, ui) {
                      cambiarCurso(ui.draggable,$(this));
            }
  });

  $alumnos.droppable({
            accept: '.drop-here  div',
            activeClass: 'custom-state-active',
            drop: function(ev, ui) {
                      deshacer(ui.draggable,$(this));
            }
  });
$('.uiicon').click(function(ev) {
          var $item = $(this);
          deshacer($item, '#alumnos-curso');
          return false;
});


});

function cambiarCurso($item,$target) {
  var back = '<a href="#" title="Deshacer mover de grupo" onclick="deshacer($(this).parent(), \'#alumnos-curso\');"class="ui-icon ui-icon-arrowreturnthick-1-w undo">Recycle image</a>';
  console.log('Alumno = '+$item.attr('id'));
  console.log('Curso nuevo = '+$target.attr('id'));
  updateData($item, $item.attr('id'), $('#grupo-actual').val(), $target.attr('id'));
  $item.fadeOut(function() {
            $item.append(back);
            $item.appendTo($target).fadeIn();
  });
}

function deshacer($item,$target) {
  console.log('Alumno = '+$item.attr('id'));
  console.log('Curso nuevo = '+$item.parent().attr('id'));
  updateData($item, $item.attr('id'), $item.parent().attr('id'), $('#grupo-actual').val());
  $item.children('.ui-icon').fadeOut();
  $item.fadeOut(function() {
            $item.appendTo($target).fadeIn();
  });
}

function updateData($target, $alumno, $grupo, $nuevo)
{
  $.ajax({
        type       : "POST",
        url        :"/calendarizacion/updateData",
        data       :{alumno : $alumno,
                     grupo : $grupo,
                     nuevo : $nuevo},
        dataType   :'json',
        beforeSend : function(){
                      $target.css('opacity', '.5');
                    },
        success    : function(){
                      $target.css('opacity', '1');
                   },
        error      : function(e){
                    console.log(e);
                   }
          });
}