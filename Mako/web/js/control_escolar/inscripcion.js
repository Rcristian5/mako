$(function() {

    var meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio','Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
    var dias = ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];

    $('#ventana-calendar').dialog({
			modal  : true,
			width  : 1000,
			height : 800,
                              resizable: false,
			autoOpen: false,
			position: ['center','top'],
			closeOnEscape: false,
			title:	'Selecciona una fecha y hora'
		});

    //Plugin de calendario
    $('#calendar').fullCalendar({
        //Layout y vistas disponibles, deshabilitamos la vista por dia
        header          : {
                           left: 'prev,next today',
                           center: 'title',
                           right: 'month,agendaWeek'
                          },
        height          : 500,
        //Tema y traducción de textos
        theme           : true,
        monthNames      : meses,
        monthNamesShort : meses,
        dayNames        : dias,
        dayNamesShort   : dias,
        buttonText      : {
                              prev:     '&nbsp;&#9668;&nbsp;',
                              next:     '&nbsp;&#9658;&nbsp;',
                              prevYear: '&nbsp;&lt;&lt;&nbsp;',
                              nextYear: '&nbsp;&gt;&gt;&nbsp;',
                              today:    '&nbsp;hoy&nbsp;',
                              month:    '&nbsp;&nbsp;&nbsp;mes&nbsp;&nbsp;&nbsp;',
                              week:     'semana',
                              day:      'día'
                          },
        allDayText      : 'día entero',
        //Tiempio mínimo y máximo
        minTime         : 8,
        maxTime         : 22,
        //Día dee incio cambio Lunes
        firstDay        : 1,
        eventClick: function(calEvent, jsEvent, view)
                            {
                              if(view.name != 'agendaWeek')
                              {
                                $('#calendar').fullCalendar( 'gotoDate',  calEvent.start);
                                $('#calendar').fullCalendar( 'changeView', 'agendaWeek' );
                              }
                                var fecha_fin = calEvent.className.toString();
                                var datos = '<p style="text-align:center; font-weight: bold; clear: both; with:100%;">'+calEvent.title +'</p>';
                                datos += '<p class="fechas"><span>Fecha inico:</span> '+$.fullCalendar.formatDate(calEvent.start, 'yyyy-MM-dd') +'</p>';
                                datos += '<p class="fechas"><span>Fecha termino:</span> '+fecha_fin.substring(15, fecha_fin.length)+'</p>';
                                datos += '<p class="fechas"><span>Hora inicio:</span> '+$.fullCalendar.formatDate(calEvent.start, 'HH:mm') +'</p>';
                                datos += '<p class="fechas"><span>Hora fin:</span> '+$.fullCalendar.formatDate(calEvent.end, 'HH:mm') +'</p>';
                                $('#curso-pre-inscribir').val(calEvent.id);
                                $('#datos-finales').html(datos);
                                $('#datos-pre-inscribir').show();
                            }

    });

    $('#end').click(function(){
        location.href='/control/inscribir?id='+$('#curso-pre-inscribir').val();
       }
    );
    
});
