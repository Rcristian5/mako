var oTable;
var l = 0;
var r = 110;
var g = 165;
var b = 45;
$(document).ready(function()
{

  //Llamada al plugin de columnas
  $('#windows-wrapper').windows({
                            resizable : true,
                            minimizable: false,
                            minheight : 700
                        });
  //fixHeigth();
  /*
   * Plugin de tooltip para los titles de los links a capitulos, temas y evaluaciones
   */
  $('.temario-title, .evaluacion a').tooltip({
    track: true,
    delay: 0,
    showURL: false,
    showBody: " - ",
    fade: 250
  });
  
  $('.link-tema').tooltip({
    track: true,
    delay: 0,
    showURL: false,
    fade: 250,
    extraClass: "add-tema"
  });

  $('#duracion').tooltip({
    track: true,
    delay: 0,
    showURL: false,
    fade: 250,
    extraClass: "add-tema"
  });
  
  getPos();
  $('.evaluacion').each(function(){
      var tipoC =$(this).attr('c-inicio');
      var tipoT =$(this).attr('t-inicio');
      var inicio;
      var fin;
      var posT;
      var h;
      if(tipoC!= undefined)
      {
        inicio = tipoC;
        fin = $(this).attr('c-fin');
        posT = $('li[capitulo="'+inicio+'"]').position().top;
        if(inicio == fin)
        {
          h = $('li[capitulo="'+inicio+'"]').height()- 20;
        }
        else
        {
          h = $('li[capitulo="'+fin+'"]').height() + $('li[capitulo="'+inicio+'"]').height();
        }
        $(this).mouseover(function(){
          $('li[capitulo="'+inicio+'"] a:first').addClass('ui-state-active');
          $('li[capitulo="'+fin+'"] a:first').addClass('ui-state-active');
        }).mouseleave(function(){
          $('li[capitulo="'+inicio+'"] a:first').removeClass('ui-state-active');
          $('li[capitulo="'+fin+'"] a:first').removeClass('ui-state-active');
        });

      }
      if(tipoT!= undefined)
      {
        inicio = tipoT;
        fin = $(this).attr('t-fin');
        posT = $('a[tema="'+inicio+'"]').position().top;
        h =   $('a[tema="'+fin+'"]').position().top - posT;
        $(this).mouseover(function(){
          $('a[tema="'+inicio+'"]').addClass('ui-state-active');
          $('a[tema="'+fin+'"]').addClass('ui-state-active');
        }).mouseleave(function(){
          $('a[tema="'+inicio+'"]').removeClass('ui-state-active');
          $('a[tema="'+fin+'"]').removeClass('ui-state-active');
        });
      }
      l= l+35;
      barEvaluacion($(this), posT, l, h)
  });

});


/*
 * comparaRequeridos
 * Comprueba que todos los datos requeridos esten capturados
 */
function comparaRequeridos()
{
    if (requeridos() && validaTels())
    {
        return true;
    }
    else
    {
        return false;
    }

}

/*
 * fixHeigth
 * Compruba que el alto del div que muestra el detalle del temario no
 * exceda el 90%, si lo hace fija como alto 90%
 */
function fixHeigth()
{
    var heigth = $('#lista_resultados').height();
    var parentHeigth = $('#lista_resultados').parent().parent().height();
    var x = (heigth *100)/parentHeigth;
    if(x >= 65)
    {
      var h = (65 * parentHeigth)/100;
      $('#lista_resultados').height(h);
    }
}

/*
 * barEvalaucion
 * Crea la barra indicadora de la evaluacion
 */
function barEvaluacion(target,posT,posL,h)
{
    $(target).css({
      'top'    : posT,
      'left'   : posL,
      'background-color' : color()
    });
    $(target).height(h+10);
    
}
/*
 * getPos
 * Obtiene la posicion Left de los links para asignarla a las barras de evaluación
 */
function getPos()
{
    $('.caps a').each(function(){
       var left = $(this).position().left + $(this).width()+30;
       if(left>l)
       {
         l = left;
       }
    })
}
/*
 * color
 * Obtiene un color diferenta para cada barra de evaluación
 */
function color()
{
  if(g==255)
  {
    r += 15;
    g = 195;
    b = 120;
  }
  var color = 'rgb('+r+','+g+','+b+')';
  g += 15
  b += 15
  
  return color;
}