
function maximizar()
{
  aTable.fnSetColumnVis( 1, true );
  aTable.fnSetColumnVis( 2, true );
  aTable.fnSetColumnVis( 3, true );
  aTable.fnSetColumnVis( 4, true );
  $('.no-visibles').show();  

}

function minimizar()
{
  aTable.fnSetColumnVis( 1, false );
  aTable.fnSetColumnVis( 2, false );
  aTable.fnSetColumnVis( 3, false );
  aTable.fnSetColumnVis( 4, false );
  aTable.fnSort( [ [0,'asc']] );
  $('.no-visibles').hide();
}
