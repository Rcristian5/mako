var disDays = [];
var oTable;
$(document).ready(function() {
  var dias = ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];
  var meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio','Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
  //Plugin de calendario
  $('#calendar').fullCalendar({
        //Layout y vistas disponibles, deshabilitamos la vista por dia
        header          : {
                           left: 'today',
                           center: 'title',
                           right: 'prev,next '
                          },
        height          : 600,
        weekMode        :  'liquid',
        //Tema y traducción de textos
        theme           : true,
        monthNames      : meses,
        monthNamesShort : meses,
        dayNamesShort   : dias,
        buttonText      : {
                              prev:     '&nbsp;&#9668;&nbsp;',
                              next:     '&nbsp;&#9658;&nbsp;',
                              prevYear: '&nbsp;&lt;&lt;&nbsp;',
                              nextYear: '&nbsp;&gt;&gt;&nbsp;',
                              today:    '&nbsp;hoy&nbsp;',
                              month:    '&nbsp;&nbsp;&nbsp;mes&nbsp;&nbsp;&nbsp;',
                              week:     'semana',
                              day:      'día'
                          },
        allDayText      : 'día entero',
        //Día dee incio cambio Lunes
        firstDay        : 1,
        //Sias no habiles
        events          : disabledDays,
        eventClick : function( event, jsEvent, view ) {
            if(event.id == 'vacaciones-actuales')
            {
                location.href= $edit+"?id="+event.ident;
            }
            if(event.id == 'vacaciones-pasadas')
            {
                alert('No puedes editar un periodo de vacaciones que ya termino o esta en curso');
            }
         }
  });
  $('#calendar').fullCalendar( 'addEventSource', vacacionesActuales);
  $('#calendar').fullCalendar( 'addEventSource', periodoActual);

  $('#windows-wrapper').windows({
                        resizable : true,
                        minimizable: false
                    });

  oTable = $('#table').dataTable({
		"bPaginate": false,
		"bLengthChange": false,
		"bFilter": false,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
                "bJQueryUI": true
                });
  

$("#autocomplete_vacaciones_usuario_id").bind('result',function(){
    $.ajax({
          url : '/admin_vacaciones/vacacionesUsuario',
          data : {id : $('#vacaciones_usuario_id').val()},
          dataType : 'json',
          beforeSend : function(){            
            $('.load').show();
          },
          success: function(data) {
            $('.load').hide();
            disDays = data.dias;
            var i;
            var vacacionesFacilitador = [];
            $('#calendar').fullCalendar( 'removeEvents', 'vacaciones-pasadas');
            $('#calendar').fullCalendar( 'removeEvents', 'vacaciones-actuales');
            $('#calendar').fullCalendar( 'removeEvents', 'perido-actual');
            $('#periodos-pasados').show();
            oTable.fnClearTable();
            for(i = 0; i <= data.periodo.length-1; i++)
            {
                vacacionesFacilitador = [
                      {
                        id  : 'vacaciones-facilitador',
                        title  : $("#autocomplete_vacaciones_usuario_id").val(),
                        start  : data.periodo[i].inicio,
                        end    : data.periodo[i].fin,
                        allDay : true,
                        className  : 'vaciones-pasadas',
                        editable: false
                    }
                    ];
                $('#calendar').fullCalendar( 'addEventSource', vacacionesFacilitador);
                oTable.fnAddData( [
                    data.periodo[i].inicio,
                    data.periodo[i].fin ] );

                }
          },
          error: function(data){
            console.error(data);
          }
        });
});

  $.datepicker.setDefaults($.datepicker.regional['es']);
   
  $("#vacaciones_fecha_fin").datepicker({
      changeMonth: true,
      changeYear: true,
      minDate: 0,
      firstDay: 1,
      beforeShowDay: function(date){
          return disablePeriodo(date);
      }
  });
  

  $("#vacaciones_fecha_inicio").datepicker({
      changeMonth: true,
      changeYear: true,
      minDate: 0,
      firstDay: 1,
      beforeShowDay: function(date){
         return disablePeriodo(date);
      }

  });
        
  $('#mostrar-todos').change(function(){
     comprobarMostrar();
  });

  comprobarMostrar();
});

/**
*Funcion para desactivar dias
*/
function disablePeriodo(date)
{
    var i;
    for (i = 0; i < disDays.length; i++)
    {
         if (date.getMonth() == parseInt(disDays[i][0])-1 && date.getDate() == parseInt(disDays[i][1]) && date.getFullYear() == parseInt(disDays[i][2]))
         {

                    return false;
         }
    }
    return [(date.getDay()>0),""]
}


//Funcion para comprobar si se muestran o no las vacaciones pasadas
function comprobarMostrar()
{
      if($('#mostrar-todos').is(':checked'))
      {
          $('#calendar').fullCalendar( 'addEventSource', vacacionesPasadas);
      }
      else
      {
          $('#calendar').fullCalendar( 'removeEvents', 'vacaciones-pasadas');
      }
}
function comprobarCampos()
{
    if($("#vacaciones_fecha_inicio").val() > $("#vacaciones_fecha_fin").val())
    {
        alert('La fecha de inicio no puede ser menor a la fecha de fin');
        return false;
    }
    else
    {
        return requeridos();
    }
}