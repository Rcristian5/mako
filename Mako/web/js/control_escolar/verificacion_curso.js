/*
 * Script para control escolar
 */
$(document).ready(function()
{
   //Llamada el plugin dataTables

   $('#detalle').dataTable({
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
                    "bJQueryUI": true,
                    "sPaginationType": "full_numbers",
                    "aoColumns": [
                                    null,
                                    null,
                                    null,
                                    null,
                                    null,
                                    null,
                                    null,
                                    {"bSortable": false}

                                 ],
                    "aaSorting": [[0, 'asc']]
                });
   $('#conflicto').dataTable({
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bSort": true,
		"bInfo": true,
		"bAutoWidth": false,
                    "bJQueryUI": true,
                    "sPaginationType": "full_numbers",
                    "aaSorting": [[0, 'asc']]
                });

  //Llamada al plugin de columnas
  $('#windows-wrapper').windows();
 });

