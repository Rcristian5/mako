
	
	/**
	 * Realiza la operación de reposicion de credencial
	 */
	function reposicionCredencial(id_usuario, costoRepCred){
		
		if(!confirm('La reposición de credencial tiene un costo de $'+ costoRepCred +' pesos. ¿Desea continuar con la reposición?')){
			return false;			
		}
		document.getElementById('reposicionCred'+id_usuario).style.display ='block';
		return true;		
	}
	
	/**
	 * Filtra que la cadena de busqueda debe traer al menos 10 caracteres.
	 */
	function caracteresBusqueda(campo){
		
		nombre = document.getElementById('qNombre').value;
		usuario = document.getElementById('qUsuario').value;
		folio = document.getElementById('qFolio').value;
		
		if(folio.length > 0 && folio.length < 10){
			alert("La búsqueda por folio debe tener al menos 10 caracteres.");
			return false;	
		}if(usuario.length > 0 && usuario.length < 10){
			alert("La búsqueda por usuario debe tener al menos 10 caracteres.");
			return false;	
		}if(nombre.length > 0 && nombre.length < 10){
				alert("La búsqueda por nombre debe tener al menos 10 caracteres.");
				return false;	
		}else{
			return true;	
		}

	}
