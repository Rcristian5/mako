/**
 * Funciones para manejo de mapas.
 */

var map;
var geocoder;
var address;
var localsearch;
var searchControl;
var latIni;
var longIni;



function initialize() 
{
	geocoder = new GClientGeocoder();
	map = new GMap2(document.getElementById("map_canvas"));
	var point = new GLatLng(latIni, longIni);
	map.addControl(new GLargeMapControl);
	map.setCenter(point, 13);
	marker = new GMarker(point, {
		'draggable' : true,
		'bouncy' : true
	});
	map.addOverlay(marker);
	GEvent.addListener(marker, "dragend", palArrastre);
	map.setZoom(15);
}

function palArrastre(latlng)
{
	geocoder.getLocations(latlng, showAddress);
}

function showAddress(response)
{
	map.clearOverlays();

	if (!response || response.Status.code != 200)
	{
		alert("Status Code:" + response.Status.code);
	} 
	else 
	{
		place = response.Placemark[0];
		point = new GLatLng(place.Point.coordinates[1],
				place.Point.coordinates[0]);
		marker = new GMarker(point, {
			'draggable' : true,
			'bouncy' : true
		});
		map.addOverlay(marker);
		GEvent.addListener(marker, "dragend", palArrastre);
		$(document).trigger('georeferencia.modificada',[place.Point.coordinates[1],place.Point.coordinates[0],place.address]);
	}
}

function setAddress(address) {
	map.clearOverlays();
	geocoder.getLatLng(address, function(point) 
	{
		if (!point) 
		{
			alert(address + " Direccion no encontrada");
		} else {
			map.setCenter(point, 13);
			marker = new GMarker(point, {
				'draggable' : true,
				'bouncy' : true
			});
			map.addOverlay(marker);
			GEvent.addListener(marker, "dragend", palArrastre);
			map.setZoom(15);
			$(document).trigger('georeferencia.modificada',[point.lat(),point.lng(),address]);
		}
	});
}
var qaDir='';
var qaDirAnt='';
function dirAMapa()
{
	console.log('entra en maapa');
	var edo = $($("#socio_id_estado > option")[$("#socio_id_estado").val()]).text();
	var calle = $('#socio_calle').val();
	var numext = $('#socio_num_ext').val();
	var mun = $('#socio_municipio').val();
	var col = $('#socio_colonia').val();
	var cp = $('#socio_cp').val();
	qaDir = calle+' '+numext+','+col+','+cp+','+mun+','+edo+', MEXICO';
	console.log(qaDir);
	if(qaDir!=qaDirAnt)
	{
		qaDirAnt = qaDir;
		setAddress(qaDir);
	}
}


$(function() {
	google.load("search", "1");
	
	latIni = ($("#socio_lat").val()=='')?$("#centro_lat").val():$("#socio_lat").val();
	longIni = ($("#socio_long").val()=='')?$("#centro_long").val():$("#socio_long").val();
	initialize();

	GSearch.setOnLoadCallback(initialize);

	$(document).bind('georeferencia.modificada',function(ev,lat,long,dir){
		console.debug('latitud:'+lat);
		console.debug('longitud:'+long);
		console.debug('dir:'+dir);
		$("#socio_lat").val(lat);
		$("#socio_long").val(long);
		$("#socio_dirgmaps").val(dir);
	});	
});