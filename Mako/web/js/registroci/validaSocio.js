        
		/**
		 * Hace las validaciones  de requeridos y tipos de datos. 
		 * Si todo está bien manda a verificar el folio con una llamada asincrona al servidor. Si todo sale bien esta función hace submit de la forma. 
		 */
        function comparaRequeridos(){
        
            //if (requeridos() && validaTels() && validaFolio()) {
        	//ToDo: checar con Edgar la validacion del folio
            if (requeridos(document.getElementById('tabs')) && validaTels()) {
               /* if (comparaCampos(document.getElementById('clave').value, document.getElementById('claveCompara').value)) {
					//verificaFolio();
					return false;
				}
				else */
					return true;
				
            }
            else 
                return false;
			
			return false;
        }
        
        /**
         * Despliega el control de toma de fotografia
         */
        var foto = false; //foto del presentado
        function muestraControlFoto(){
        
            if (foto == true) {
                if (!confirm('Ya se ha capturado el indice de la mano ' + dedo + '. ¿Desea capturarlo nuevamente?')) {
                    return false;
                }
            }
            
            var cont = document.getElementById('cont_foto');
            //alert(document.getElementById('img_capturar_foto').src);
            cont.style.display = '';
        }
		
		/**
		 * Valida numeros telefonicos
		 */
		function validaTels(){
			var tel = document.getElementById('socio_tel');
			var cel = document.getElementById('socio_celular');
			if((tel.value == cel.value)&& (tel.value != '' && cel.value != '')){
				alert('Los números telefónicos que ingresó son iguales, por favor solo ingrese uno.');
				tel.focus();
				return false;
			}
			if(tel.value.length < 8 && tel.value != ''){
				alert('El número telefonico que ingresó no es válido. Porfavor si no existe el número deje el campo vacío, no intente capturar nada.');
				tel.focus();
				return false;
			}
			if(cel.value.length < 10 && cel.value != ''){
				alert('El número de teléfono celular que ingresó no es válido. Porfavor si no existe el número deje el campo vacío, no intente capturar nada.');
				cel.focus();
				return false;
			}
			return true;
		}
		
		/**
		 * Valida longitud de folio de credencial
		 */
		function validaFolio(){
			var folio = document.getElementById('socio_folio');
			if(folio.value.length < 13)
			{
				alert('El folio de la credencial que está capturando no es válido porfavor ingrese un folio correcto.');
				folio.focus();
				return false;
			}
			return true;
		}
		
		
		
		function toggleDisabled(el) {
		    allSelects = el.getElementsByTagName('select');
		   // i;
			for (i = 0; i < allSelects.length; i++) {
				allSelects[i].disabled = true;
				allSelects[i].options[0].selected = true;
			}
			allInputs = el.getElementsByTagName('input');
		    //i;
			for (i = 0; i < allInputs.length; i++) {
				allInputs[i].disabled = true;
				allInputs[i].value = '';
			}
		}
		
		
		/**
		 * Envia los datos del folio de la credencia al servidor para verificar que no existan registros previos de la credencial
		 * Si todo esta bien o el usuario selecciona confirmar la accion esta misma funcion es la que realiza el sumbit de la forma.
		 */
		function verificaFolio(){
			var fv = document.getElementById('folio');
			var id_usuario = document.getElementById('id_usuario');
			if(fv.value==''){return true;}
			var respuesta;
			
			var req = doRequest("POST", "./registro.php",
			    {accion:'verifica_folio',folio:fv.value,id_usuario:id_usuario.value},
			    function(status, statusText, responseText) {

					if (status != 200) {
						document.getElementById('error').innerHTML="No es posible conectar con el sevidor. La conexi&oacute;n a la red se ha perdido. Intente nuevamente.";
						return false;			
					}
					else if(responseText=='OK' && status == 200)
					{
						document.getElementById('id_usuario').form.submit();
						return true;
					}
					else if(responseText !='OK' && status == 200)
					{
						var resp = responseText.split("|");
						
						var nombre = resp[0]+ " " +resp[1]+ " "+resp[2];
						var fecha_alta =   resp[3];
						var centro =  resp[4];
						var id_usuario_registrado =  resp[5];
						var id_usuario = document.getElementById('id_usuario');
						if ((id_usuario.value != id_usuario_registrado.value)) {
							var msgConfirmacion = "La credencial con folio: " + fv.value + " ya se tiene registrada al socio:\nNombre: " +
							nombre +
							"\nDado de alta la fecha: " +
							fecha_alta +
							"\nEn el centro: " +
							centro +
							"\nSi continúa se dará de baja el registro anterior del socio: " +
							nombre +
							"\n\n¿Desea continuar con el registro?";
							
							if (confirm(msgConfirmacion)) 
							{
								document.getElementById('baja_registro_duplicado').value = id_usuario_registrado; 
								id_usuario.form.submit();
							}
							else {
								return false;
							}
						}
					}
						
				});
			return false;
		}	
