// Reingenieria Mako C. ID 35. Responsable:  FE. Fecha: 23-01-2014.  Descripción del cambio: Se valido campos de fecha inicial y final.
	$(function() {
		var fecha = new Date();
		var ano = fecha.getFullYear()+1;
		$( "#promocion_vigente_de" ).datepicker({
			firstDay: 1,
			changeYear: true,
			changeMonth: true,
			dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
			dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
			monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
			constrainInput: true,

			yearRange: '2009:' + ano,
			showWeek: true,
			showOtherMonths: true,
			selectOtherMonths: true,
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy-mm-dd',
			onClose: function( selectedDate ) {
				var aDate= selectedDate.split("-");
				var dia=aDate[2];
				var mes=aDate[1];
				var anio=aDate[0];
				$( "#promocion_vigente_a" ).datepicker( "option", "minDate", new Date(anio, mes - 1, dia));
			}
		});
		$( "#promocion_vigente_a" ).datepicker({
			firstDay: 1,
			changeYear: true,
			changeMonth: true,
			dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
			dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
			monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
			constrainInput: true,

			yearRange: '2009:' + ano,
			showWeek: true,
			maxDate: '+30Y',	
			showOtherMonths: true,
			selectOtherMonths: true,
			changeMonth: true,
			numberOfMonths: 1,
			dateFormat: 'yy-mm-dd',
			onClose: function( selectedDate ) {
				var dDate= selectedDate.split("-");
				var dia=dDate[2];
				var mes=dDate[1];
				var anio=dDate[0];
				$( "#promocion_cupon_vigente_de" ).datepicker( "option", "maxDate", new Date(anio, mes - 1, dia));
			}
		});
	});
// FE. Fecha: 23-01-2014. - Fin.
