/**
 * Hace override de la consola de firebug. 
 * Esto es para hacer portable el código en producción, desarrollo y pruebas.
 */

if (typeof console == 'undefined') {
    var console = {};
    console.log = function(msg) {
        return;
    };
    console.debug = function(msg) {
        return;
    };
    console.info = function(msg) {
        return;
    };
    console.warn = function(msg) {
        return;
    };
    console.error = function(msg) {
        return;
    };
}
