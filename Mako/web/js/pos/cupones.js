$(document).ready(function(){

	//Convertimos en botones abrir-cerrar los divs con los titulos
	$("#handler-cupones").click(function(){$("#lista-cupones").toggle('blind','fast');});

	//Escuchamos evento curso.panel-abierto cuando se abre el panel de cursos.
	$(document).bind('cursos.panel-abierto',function(ev){
		$("#lista-cupones").hide('fast');
	});
	
	
	//Escuchamos evento socio.seleccionado cuando se selecciona un socio revisamos si tiene cupones de descuento
	$(document).bind('socio.seleccionado',function(ev, socio_id, socio_nombre){

		//Borramos la tabla por si hay productos ya seleccionados y con cupones probablemete
		tCar.fnClearTable();
		$(document).trigger('actualiza_carrito');
		
		tieneCupones(socio_id);
	});
	
	
	
	//Cuando se agrega un nuevo producto al carrito pegamos el handler de dropable
	$(document).bind('actualiza_carrito',function(){

		$(".dropable").droppable({
			activeClass: 'ui-state-hover',
			hoverClass: 'ui-state-active tss',
			accept: '.cupon-detalle',
			drop: function(event, ui) {
				console.log('tirado: '+ui.draggable.attr('cupon_id'));
				console.log('descuento: '+ui.draggable.attr('descuento'));
				var descto = $.sprintf("%.2f %%",ui.draggable.attr('descuento'));

				//Obtenemos el id_producto donde se ha dejado caer el cupón
				var aPos = tCar.fnGetPosition(this);
				var producto_id = $(this).attr('producto_id');
				
				console.log('Solo a productos:'+ui.draggable.attr('productos_permitidos'));
				
				var sPermitidos = ui.draggable.attr('productos_permitidos');
				var aPermitidos = sPermitidos.split(',');
				var procede = false;
				for(var i in aPermitidos )
				{
					//console.log('Permitido: '+producto_id+' ? '+aPermitidos[i]);
					if(producto_id == aPermitidos[i])
					{
						//agregamos el id del div del cupon para hacer referencia en caso de borrar el producto
						var tr = $(this).closest("tr");
						tr.attr('cupon-div',ui.draggable.attr('id'));

						procede = true;
						ui.draggable.hide('fast');
						$(this).droppable('disable');
						$(this).removeClass('ui-state-disabled');
						$(this).addClass('ui-state-highlight');

						//Seteamos el valor del cupon_id en su columna la clumna 8.
						tCar.fnUpdate( ui.draggable.attr('cupon_id'), aPos[0], 8 );
						actualizaDescuento(ui.draggable.attr('descuento'),aPos[0],aPos[1]);
						//Seteamos la cantidad a 1, la columna de cantidad es la 3
						actualizaCantidad(1,aPos[0],3);
						break;
					}
				}
				
				//No procede el descuento con este cupon
				if(!procede)
				{
					console.log('no permitido');
				}
				
			}
		});
	});	

	//Cuando se elimina un producto del carrito y tiene un cupón lo regresamos al contenedor de cupones
	$(document).bind('carrito.producto-eliminado',function(ev,tr){
		console.log('div cupon revertido: '+tr.attr('cupon-div'));
		if(tr.attr('cupon-div') != undefined)
		{
			$('#'+tr.attr('cupon-div')).appendTo("#lista-cupones");
			$('#'+tr.attr('cupon-div')).show('fast');
		}
	});
	
	
	//Redimir cupones impresos
	
	/**
	 * Ventana para otortgar cupones impresos
	 */
	var $wValCupon= $("#otorgar-cupon").dialog({
		modal: true,
		autoOpen: false,
		closeOnEscape: false,
		title:	'Validar cupón',
		buttons: {
			'Validar': function() {
				if($('#codigo-cupon').val()=='' || $('#validador').val()=='')
				{
					alert('Debe ingresar el código del cupón y el validador.');
					return false;
				}
				
				$.getJSON(
						$("#urlGetCuponesManual").val(),
						{
							'socio_id':$('#socio_id').val(),
							'codigo_cupon':$('#codigo-cupon').val(),
							'validador':$('#validador').val()
						}, 
						function(dat) {
							
							if(dat.resultado != 'ok' && dat.resultado != 'otorgado')
							{
								alert(dat.msg);
							}
							else if(dat.resultado=='otorgado')
							{
								alert(dat.msg+'\nOtorgado a: '+dat.otorgado_a+'\nEn '+dat.otorgado_el);
							}
							else
							{
								tieneCupones($('#socio_id').val());
								$('#codigo-cupon').val('');
								$('#validador').val('');
								$wValCupon.dialog('close');
							}
						}
				);
				
			},
			'Cancelar':function() {
				$(this).dialog('close');
			}
		}
	});
	
	$('#liga-otorgar-cupon').click(function(){
					
		$wValCupon.dialog('open');
	});
	
	
	/**
	 * Revisa si tiene cupones o no el socio y pinta la persiana de cupon
	 */
	function tieneCupones(socio_id){

		//escondemos el panel si esta abierto
		$("#panel-cupones").hide();
		
		//Borramos los cupones que hayan podido estar presentes
		$("#lista-cupones").html('');
		
		//Traemos del servidor los cupones
		$.getJSON($("#urlGetCuponesPorRedimir").val(),{'socio_id':socio_id}, function(dat) {

			var c = dat.length;
			
			for(var i = 0; i < dat.length; i++)
			{
				var productosPermitidos = dat[i].productos_asociados;
				var cupon = $("#cupon-plantilla").clone(true);
				
				cupon.attr('id','cupon-'+dat[i].cupon_id);
				cupon.attr('cupon_id',dat[i].cupon_id);
				cupon.attr('productos_permitidos',productosPermitidos);
				cupon.attr('promocion_id',dat[i].promocion_id);
				cupon.attr('descuento',dat[i].descuento);
				cupon.find('.nombre-promocion').text(dat[i].promocion);
				cupon.find('.cupon-codigo').text(dat[i].codigo);
				cupon.find('.cupon-descuento').text($.sprintf( "%.2f %%",dat[i].descuento));
				cupon.find('.cupon-vigencia-de').text(dat[i].vigencia_de);
				cupon.find('.cupon-vigencia-a').text(dat[i].vigencia_a);
				cupon.draggable({ 
					revert: 'invalid', 
					opacity: 0.4, 
					helper: 'clone'
				});
				cupon.appendTo('#lista-cupones');
				cupon.show();
				cupon.mousedown(function(event, ui) { 
					$(this).draggable('option','cursorAt',{left: cupon.width()/2, top: cupon.height()/2 });
				});
			}
			if(c > 0) {
				$("#lista-cupones").show();
				$("#panel-cupones").show('bounce','fast');
			}
			else
			{
				$("#lista-cupones").hide();
				$("#panel-cupones").hide();
			}
		})
	}
	
	
	
});