/**
 * Buscador de socios del punto de venta
 */
	$(document).ready(function(){
		loadInitialData();
		
		//Ventana del buscador de socios
		var $wBuscador = $("#ventana-buscador-socios").dialog({
			modal: true,
			minWidth: 250,
			width: 320,
			maxWidth: 400,
			minHeight: 350,
			height: 600,
			maxHeight: 710,
			autoOpen: ($("#socio_id").val()!='')?false:true,
			position: ['center','top'],
			closeOnEscape: false,
			title:	'Seleccione socio',
                        //Id: 10 , Responsable: ES, Fecha: 22/10/2013
                        open: function(event, ui) { $(".ui-dialog-titlebar-close").hide(); }        
		});

		//Bindings de eventos a escuchar de la ventana
		$("#ventana-buscador-socios").bind('dialogclose',function(){
			if($("#socio_id").val() == '')
			{
				alert('Para realizar una venta, antes debe seleccionar a un socio');
			}
			$("#buscador").hide().clone(true).appendTo("#contenedor-usuario").fadeIn('slow');
		});

		//Escuchamos evento del buscador, socio.seleccionado
		$(document).bind('socio.seleccionado',function(ev, socio_id, socio_nombre){

			//Seteamos el hidden de socio_id con el valor seleccionado
			$("#socio_id").val(socio_id);
			$("#vp-socio-seleccionado").show('slow');
			$("#nombre").html(socio_nombre);
			
			
			//Deshabilitamos la liga de seleccionar y ponemos un msg para indicar que ese socio se ha seleccionado
			//$("#liga-sel-"+socio_id).hide();
			$("#liga-sel-"+socio_id).text('Seleccionado').css('color','red');
			
			if($wBuscador.dialog('isOpen'))
			{
				$wBuscador.dialog('close');
				$("#autocomp_productos").focus();
			}

			//Preguntamos si podemos realizar la venta.
			puedeRealizarVenta();

			//Cuando seleccionamos un usuario escondemos todos los demas resultados
			$(".resultados-buscador-socios").find("fieldset").each(function(){

				if($(this).attr('id') != 'fs_'+socio_id)
				{
					$(this).hide('fast');
				}
			});

			//Para finalizar ponemos el foco en el buscador de productos
			$("#autocomp_productos").focus();
		});

		//Escuchamos cuando se ejecuta el evento de búsqueda
		$(document).bind('socios.buscador.buscando',function(){
			console.debug('Buscando...');
			$("#socio_id").val("");	
			$("#nombre").html("");
			$("#vp-socio-seleccionado").hide('slow');
		});

		//Escuchamos cuando se termina 
		$(document).bind('socios.buscador.encontrados',function(){
			
			console.debug('Terminando...');	
		});

		//Ventana de detalle del socio
		var $wcm= $("#verDetalleSocio").dialog({
			modal: true,
			autoOpen: false,	    		
			position: ['center','center'],
			title:	'Ver detalle',
			height: 550,
			width: 1100,
			buttons: {
				'Cerrar': function() 
				{
					$(this).dialog('close');
				}							
			}
		});
		
		//Funcion que despliega la ventana de detalles del socio
		function verDetalle(idSocio){
			var UrlVerDetalle = $("#urlVerDetalle").val();
			$("#verDetalleSocio").load(								 
				UrlVerDetalle,
				{
					id: idSocio
				}			
			); 
			$wcm.dialog('open');					
		}

		//Escuchamos evento del buscador, socio.seleccionado
		$(document).bind('socios.ver-detalle',function(ev, socio_id){
			if(socio_id != undefined){
				console.log("Escuchando evento de ver detalle: "+socio_id);
				verDetalle(socio_id);
			}
		});
		
		
		
		
		/**
		 * 
		 * **************************************************************************************
		 * Ejecutamos funcionalidad en caso de que la pagina se cargue con datos de socio 		*
		 * y producto que se debe cargar por default.											*
		 * @author silvio.bravo@enova.mx 24 Marzo 2014											*
		 * @copyright ENOVA																		*
		 * @category Reingeniria Mako.															*
		 * **************************************************************************************
		 * 
		 */
		
		function loadInitialData(){
			var extraSocioId	=$("#extraSocioId").attr("value");
			
			if(typeof(extraSocioId)!=="undefined" && extraSocioId){
				

				
				var extraSocioFolio		= $("#extraSocioFolio").attr("value");
				var extraProductoId		= $("#extraProductoId").attr("value");
				var url					= $("#makoBuscaSocioComponentForm").attr("action");
				
				$.ajax({  
					
					  url		: url,
					  type		: "POST",
					  data		: {qFolio:extraSocioFolio, qNombre:"", qUsuario:""},
					  success	: function(datos){
						  
					  	//Cargamos lo datos que vienen de ajax con el resultado de la busqueda.
					    $(".resultados-buscador-socios:first").html(datos);
					    //Tomamos la liga que dice selecciona(de los datos cargados) y le damos click
					    $("#liga-sel-"+ extraSocioId +":first").trigger("click");

						  /* formamos un array con los datos del producto que sera cargado por default. */
						if(extraProductoId!=""){

							  var extraDatosProducto	= JSON.decode($("#extraProductoData").attr("value"));
							  var aux					=extraDatosProducto[extraProductoId];
							  var vtproducto			=[];
							  vtproducto.push(extraProductoId);
							  vtproducto.push(aux.nombre);
							  vtproducto.push(aux.descripcion);
							  vtproducto.push(aux.precio_lista);
							  vtproducto.push(aux.categoria);
							  vtproducto.push(aux.existencia);
							  vtproducto.push(aux.codigo);
							  vtproducto.push(aux.descuento);
							  vtproducto.push(aux.promocion_id);
							  vtproducto.push(aux.modalidades_pago);
							  vtproducto.push(aux.venta_unitaria);
							  //cargamos el producto
					       	  seleccionado(null,vtproducto,null);
						}
						  
					       
				      }
				});
			}
		} //loadInitialData
		
	});

