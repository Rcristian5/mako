
$(document).ready(function(){
	
	es_inicio = false;	
	 $("#cmbEntidadFederativa").comboMakoCascade(
		{
			params						:{ "llave": "municipio" },
			eventHandler				:"change",
			urlService					:url_Datos,
			method						:"POST",
			optionValue			 		:"McpioId",
			optionLabel					:"McpioNombre",
			optionExtraValue			:"McpioNombre",
			comboCascade				:"#cmbMunicipio",
			comboCascadeDefaultValue	:0,
			onComplete					: function(){
				$("#id_colonia_fiscal").html();
				if($("#cmbEntidadFederativa").comboMakoCascade('getOption',"comboCascadeDefaultValue")>0){
					$("#cmbMunicipio").trigger("change");
//-*					$('#cp_fiscal').attr("valueinicial", "");
				}
			},
			onChange					: function(element){
				setComboTextToInputText($("#cmbEntidadFederativa"), $("#estado_fiscal"));
//-*				$("#cp_fiscal").attr("value","");
//-*				$("#id_colonia_fiscal").html("");
//-*				$('#cp_fiscal').attr("valueinicial", "");
			}
		}
	);
	
	$("#cmbMunicipio").comboMakoCascade(
			
		{
			params						:{ "llave": "colonia" },
			eventHandler				:"change",
			urlService					:url_Datos,
			method						:"POST",
			optionValue			 		:"ClnaId",
			optionLabel					:"ClnaNombre",
			optionExtraValue			:"ClnaCp",
			comboCascade				:"#id_colonia_fiscal",
			comboCascadeDefaultValue	:0,
			onComplete					: function(){
				
				if($("#cmbMunicipio").comboMakoCascade('getOption','comboCascadeDefaultValue') >0 ){
					$("#id_colonia_fiscal").trigger("change");
					
				}
			},
			onChange					: function(element){
				setComboTextToInputText($(this), $("#municipio_fiscal"));
//-*				$("#cp_fiscal").attr("value","");
//-*				$('#cp_fiscal').attr("valueinicial", "");
			}	
		}
	);
	
	
	$("#id_colonia_fiscal").live("change",function(){
		setComboTextToInputText($(this), $("#colonia_fiscal"));
		var cp=$(this).find('option:selected').attr("extra");
//-*		$("#cp_fiscal").attr("value",cp);
//-*		$('#cp_fiscal').attr("valueinicial", cp);
		
	});
	
	
	function setComboTextToInputText(comboComponent, inputComponent){
		var etiqueta=comboComponent.find("option:selected").text();
		inputComponent.attr("value", etiqueta);
	}
	
//-*
/*	
	$("#cp_fiscal").keyup(function(){
		
		if($(this).attr("value").length ==5){							//llenaron un codigo de 5 digitos
			var id	=$(this).attr("value");
			if($(this).attr("valueinicial")!= $(this).attr("value")){	//Lo que habia antes(lo que cargamos en el foco) es diferente a lo..
				$(this).attr("valueinicial",$(this).attr("value"));     //..que hay ahorita
				$.ajax({
					  url		: url_Datos,
					  type		: "POST",
					  dataType	: "json",
					  data		: {"llave": "cp", "valorId": id },
					  success	: function(datos){			//Funcionamiento especifico para Codigo postal 
						
						$("#cmbEntidadFederativa").comboMakoCascade("setDefaultOption",datos.data.estadoId);
						$("#cmbMunicipio").comboMakoCascade("setDefaultOption",datos.data.municipioId);
						$("#cmbEntidadFederativa").comboMakoCascade("llenaCombo",$("#cmbMunicipio"),datos.data.municipios, datos.data.municipioId );
						$("#cmbMunicipio").comboMakoCascade("llenaCombo",$("#id_colonia_fiscal"),datos.data.colonias, datos.data.coloniaId );
						//$("#cp_fiscal").attr("value","");
							
					      //datos.data.municipioId 
					}
					    

				});
				
			}
		}
		//else if($(this).attr("value").length ==0){
		//	$("#id_municipio").trigger("change");
		//}
		
	});*/
	

	$("#cmbEntidadFederativa, #cmbMunicipio").change(function(){
		if(!es_inicio)
			{
				console.log('limpiaDatosDir');
				$('#colonia_fiscal').val('');
				$('#calle_fiscal').val('');
				$('#num_ext_fiscal').val('');
				$('#num_int_fiscal').val('');
				$('#cp_fiscal').val('');
			}
		es_inicio = false;
	});




});

function setInitCascade(datos){
	
	$("#cmbEntidadFederativa").comboMakoCascade("setDefaultOption",datos.idEstado_fiscal);
	$("#cmbEntidadFederativa").comboMakoCascade("option","comboCascadeDefaultValue",datos.idMunicipio_fiscal );
	$("#cmbMunicipio").comboMakoCascade("option","comboCascadeDefaultValue",datos.idColonia_fiscal);
	$("#cmbEntidadFederativa").trigger("change");
	
	
	
}