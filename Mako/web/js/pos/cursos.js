$(document).ready(function(){
	

	//Escuchamos evento 
	$(document).bind('preinscripcion.cancelada',function(ev){
		cargaCursos();
	});
	
	//Convertimos en botones abrir-cerrar los divs con los titulos
	$("#handler-cursos").toggle(
			function(){

				if($("#socio_id").val()=='') return false;
				cargaCursos();
			},
			function()
			{
	     		$("#lista-cursos").hide('fast');
	    		$("#overlay-cursos").hide('fast');
			$("#lista-alumnos").remove();
                        $("#abandono-auth").remove();
	    		$(document).trigger('cursos.panel-cerrado');
			}
	);

	function cargaCursos()
	{
		$.ajax({
	        url : $("#urlCursosSocio").val(),
			
	        data : {id : $("#socio_id").val()},
	        success: function(data) {
	        	 $("#lista-cursos").html(data);
	         },
	         beforeSend: function()
	         {
	        	 $("#lista-cursos").html('');
	        	 $("#ajax-loader").fadeIn();
	         },
	         complete: function()
	         {
	        	 $(document).trigger('cursos.panel-abierto');
	        	 
	        	 $("#ajax-loader").hide();
	        	 $("#lista-cursos").height($(window).height() - $('#handler-cursos').height() - 100);
	        	 $("#lista-cursos").css('overflow-y','auto');
	        	 $("#lista-cursos").css('background-color','white');
	        	 $("#lista-cursos").show();

	        	 $("#overlay-cursos").height($(document).height());
	        	 $("#overlay-cursos").width($(document).width());
	        	 $("#overlay-cursos").show();
	        	 
	         }
	        
	       });
		
	}
});
