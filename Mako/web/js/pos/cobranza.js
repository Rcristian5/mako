$(document).ready(function(){

	//Ayuda a controlar el orden en que se realizan los pagos, no puede pagarse un pago posterior si no se ha pagado el anterior 
	var prodNumPago = new Object();
	var numPagoProd = new Object();
	
	//Convertimos en botones abrir-cerrar los divs con los titulos
	$("#handler-cobranza").click(function(){$("#lista-cobros").toggle('blind','fast');});

	//Escuchamos evento curso.panel-abierto cuando se abre el panel de cursos.
	$(document).bind('cursos.panel-abierto',function(ev){
		$("#lista-cobros").hide('fast');
	});
	
	//Escuchamos evento socio.seleccionado cuando se selecciona un socio revisamos si tiene cupones de descuento
	$(document).bind('socio.seleccionado',function(ev, socio_id, socio_nombre){
		
		//escondemos el panel si esta abierto
		$("#panel-cobranza").hide();
		
		//Borramos registros de cobranza q hayan podido estar presentes
		$("#lista-cobros").html('');
		
		//Traemos del servidor los cupones
		$.getJSON($("#urlGetPagosPendientes").val(),{'socio_id':socio_id}, function(dat) {
			var c=0;
			for(var producto_id in dat)
			{
				var nums_pago = dat[producto_id].length;
				//console.log('Productos '+ dat[producto_id].length);
				for(pago in dat[producto_id])
				{
					//console.log('padre_id:'+producto_id+' numpago:'+dat[producto_id][pago].numero_pago+' producto_id:'+dat[producto_id][pago].producto_id);
					numPagoProd[dat[producto_id][pago].numero_pago] = dat[producto_id][pago].producto_id;
					prodNumPago[dat[producto_id][pago].producto_id] = dat[producto_id][pago].numero_pago;
					c++;
					
					//console.log(dat[producto_id][pago].pago_id+' '+dat[producto_id][pago].nombre);
					var cobro = $("#cobranza-plantilla").clone(true);
					
					//Todos los pagos de cobranza son de venta unitaria
					ventas_unitarias[dat[producto_id][pago].producto_id] = true;
					
					cobro.attr('id','cobro-'+dat[producto_id][pago].producto_id);
					cobro.attr('producto_id',dat[producto_id][pago].producto_id);
					cobro.attr('padre_id',producto_id);
					cobro.attr('promocion_id',dat[producto_id][pago].promocion_id);
					cobro.attr('descuento',dat[producto_id][pago].descuento);
					cobro.attr('codigo',dat[producto_id][pago].codigo);
					cobro.attr('nombre',dat[producto_id][pago].nombre);
					cobro.attr('numero_pago',dat[producto_id][pago].numero_pago);
					cobro.attr('descripcion',dat[producto_id][pago].descripcion);
					cobro.attr('precio_lista',dat[producto_id][pago].precio_lista);
					cobro.attr('categoria',dat[producto_id][pago].categoria);
					cobro.attr('modalidades_pago',dat[producto_id][pago].modalidades_pago);
					cobro.attr('existencia',dat[producto_id][pago].existencia);
					cobro.attr('fecha-pagar',dat[producto_id][pago].fecha_a_pagar);
					cobro.attr('adeudo',dat[producto_id][pago].adeudo);
					cobro.attr('acuerdo_pago',dat[producto_id][pago].acuerdo_pago);
					cobro.attr('pago_id',dat[producto_id][pago].pago_id);
					
					cobro.find('.nombre-producto').text(dat[producto_id][pago].nombre);
					cobro.find('.fecha-pagar').text(dat[producto_id][pago].fecha_a_pagar);
					//Si tiene adeudo agregamos la clase
					if(dat[producto_id][pago].adeudo==true)
					{
						cobro.find('.fecha-pagar').addClass('ui-state-error');
						var acuerdo = $("#acuerdo-plantilla").clone(true);
						acuerdo.attr('id','acuerdo-'+dat[producto_id][pago].pago_id);
						acuerdo.attr('acuerdo_pago',dat[producto_id][pago].acuerdo_pago);
						acuerdo.attr('pago_id',dat[producto_id][pago].pago_id);
						acuerdo.find('.check-acuerdo').attr('id','chk-ap-'+dat[producto_id][pago].pago_id);
						//console.log('AP: '+dat[producto_id][pago].acuerdo_pago);
						if(dat[producto_id][pago].acuerdo_pago==true)
						{
							
							acuerdo.find('.check-acuerdo').removeClass('ui-icon-circle-close');
							acuerdo.find('.check-acuerdo').addClass('ui-icon-circle-check');
						}
						acuerdo.find('.check-acuerdo').attr
						acuerdo.show();

						acuerdo.click(function(){
							//console.log('click '+$(this).attr('id'));
							if($(this).attr('acuerdo_pago') == 'false')
							{
								$(this).attr('acuerdo_pago','true');
								$(this).find('.check-acuerdo').removeClass('ui-icon-circle-close');
								$(this).find('.check-acuerdo').addClass('ui-icon-circle-check');
								$.getJSON($("#urlGetAcuerdoPago").val(),{'pago_id':$(this).attr('pago_id'),'acuerdo_pago':'true'}, function(dat) {
									console.log(dat);
								});
								
								
							}
							else
							{
								$(this).attr('acuerdo_pago','false');
								$(this).find('.check-acuerdo').removeClass('ui-icon-circle-check');
								$(this).find('.check-acuerdo').addClass('ui-icon-circle-close');
								$.getJSON($("#urlGetAcuerdoPago").val(),{'pago_id':$(this).attr('pago_id'),'acuerdo_pago':'false'}, function(dat) {
									console.log(dat);
								});
								
							}
							return false;
						});
						
						acuerdo.mouseover(function(){
							$(this).addClass('ui-state-highlight');
						});
						acuerdo.mouseout(function(){
							$(this).removeClass('ui-state-highlight');
						});
						
						cobro.find('.fecha-pagar').after(acuerdo);
						
					}
					//cobro.find('.adeudo').text(dat[producto_id][pago].adeudo);
					//cobro.find('.acuerdo_pago').text(dat[producto_id][pago].acuerdo_pago);
					cobro.appendTo('#lista-cobros');
					cobro.show();
					
					cobro.click(function(){
						
						var numpago = $(this).attr('num_pago');
						var ppadre = $(this).attr('padre_id');
						var div_anterior = $(this).prev("div[class=cobranza-detalle]"); 
						if(div_anterior.attr('id'))
						{
							if(div_anterior.attr('padre_id') == ppadre && !div_anterior.is(':hidden'))
							{
								alert('Para realizar este pago, debe pagar antes el precedente.');
								return false;
							}
						}
						
						//console.log($(this).prev("div[class=cobranza-detalle]").arr);
						//return false;
						
						var subtotal = $.sprintf("%.2f",subTotal($(this).attr('precio_lista'),1,$(this).attr('descuento')));
						//console.log($(this).attr('nombre'));
						
						tCar.fnAddData([
						                $(this).attr('codigo'),
						                $(this).attr('categoria'),
						                $(this).attr('nombre'),
						                1,
						                $(this).attr('precio_lista'),
						                $(this).attr('descuento'),
						                subtotal,
						                $(this).attr('producto_id'),
						                '',
						                $(this).attr('promocion_id'),
						                ''
					                   ]);
						
						$(this).hide();
						$(document).trigger('actualiza_carrito');
					});
				}
			}
			if(c > 0) {
				$("#lista-cobros").show();
				$("#panel-cobranza").show('bounce','fast');
			}
			else
			{
				$("#lista-cobros").hide();
				$("#panel-cobranza").hide();
			}
		});
	});
	
	//Cuando se elimina un producto de cobranza del carrito debemos regresarlo a la persiana 
	$(document).bind('carrito.producto-eliminado',function(ev,tr){
		
		var producto_id = 0;
		$(tr).find('td').each(function()
		{
			if($(this).attr('producto_id'))
			{
				producto_id = $(this).attr('producto_id');
				$('#cobro-'+producto_id).show('fast');
				return false;
			}
		});

		//Buscamos en el carrito de compra los pagos q pudieran estar por arriba de este pago
		//y los eliminamos.
		
		//console.log('buscando pagos por arriba: '+$(tr).next('tr').find('td'));
		$(tr).next('tr').find('td').each(function()
		{
			//console.log('buscando pagos por arriba: '+$(this).text());
			/*if($(this).attr('producto_id'))
			{
				if(prodNumPago[$(this).attr('producto_id')])
				{
					eliminar(this);
					return false;
				}
			}
			*/
		});
	
	});
	
});