/**
 * Operaciones de la tabla carrito de compras
 */
//TODO: Si el producto sólo se puede comprar una unidad a la vez se debe validar que no sea editable
//la candidad y q no se pueda agregar dos veces al carrito.


/**
 * Elimina producto de la tabla
 * @param tr
 * @return boolean
 */
function eliminar(td)
{
	var tr = $(td).closest("tr").get(0);
	var fila = $(td).closest("tr");
	tCar.fnDeleteRow(tCar.fnGetPosition(tr),null,true);
	tCar.fnDraw(true);
	$(document).trigger('carrito.producto-eliminado',[fila]);
	$(document).trigger('actualiza_carrito');
	if($('#lista-opciones-pago').children().length)
	{ 
		$('#producto-cobranza').show();
	}
        $("#vista_previa_prod #categoria").html('');
        $("#vista_previa_prod #precio_lista").html('');
        $("#vista_previa_prod #descuento").html('');
        $("#vista_previa_prod #descripcion").html('');
}

/**
 * Realiza la operación de subtotales de producto
 * @param precio
 * @param cantidad
 * @param descuento
 * @return decimal
 */
function subTotal(precio,cantidad,descuento)
{
	return cantidad * precio * (1-descuento/100);
}

/**
 * hace la suma del total a pagar
 */
function sumaTotal()
{
	var nods = tCar.fnGetData();
	var total = 0;
	for(var idx in nods)
	{
		if(nods[idx] != null)
		{
			total += Number( $.sprintf( "%.2f", nods[idx][6] ) );
		}
	}
	$("#carrito #total").html($.sprintf( "%.2f",total));
	$("#a_pagar").html($.sprintf( "%.2f",total));
}



$(document).ready(function() 
{
	/**
	 * Tabla del carrito de compras
	 */
	tCar = $('#carrito').dataTable({

		"bPaginate": false,
		"bLengthChange": false,
		"bFilter": false,
		"bSort": false,
		"bInfo": false,
		"bAutoWidth": false,
        "aoColumns": [
                      null, //codigo
                      null, //categoria
                      null, //producto
                      null, //cantidad
                      null, //precio lista
                      null, //descuento
                      null, //Sub total
                      null, //producto_id
                      {"bVisible":    false}, //cupon_id
                      {"bVisible":    false}, //promocion_id
                      {"bVisible":    false} //tipo cobro_id
                   ],
		"fnRowCallback": function(nRow, aData, iDisplayIndex)
		{
                        /*Reingeniería PV | Responsable: YD => FE. - Fecha: 23-11-2013. - ID: 17. - Campo: Cuadro del detalle.
                        Descripción del cambio: Se habilita botón "limpiar carrito" si es mas de un producto.*/
			if(Number(iDisplayIndex)>0)
			{
				//$('#puede_limpiar_venta').fadeIn('slow');
				$('#puede_limpiar_venta').show();
			}
			else
			{
				$('#puede_limpiar_venta').hide();
			}
                        /*Fecha: 23-11-2013. Fin.*/
			$('td:eq(3)', nRow).addClass('sumable');	//La cantidad
			//Comprobamos si el producto_id existe en el arreglo global de ventas_unitarias
			if(ventas_unitarias[aData[7]] === true)
				{
					$('td:eq(3)', nRow).html(+aData[3]);
				}
			else
				{
					/*Reingeniería PV | Responsable: ES => FE. - Fecha: 05-11-2013. - ID: 14. - Campo: Cuadro del detalle.
					Descripción del cambio: Se agregó la funcionalidad editable del atributo cantidad (n) veces.*/
					if(isNaN(aData[3]))
						sData=aData[3];
					else
						sData='<a id="CantId' + iDisplayIndex + '" href="#" tabindex="1" class="editable">'+aData[3]+'</a>';
					$('td:eq(3)', nRow).html(sData);
					/*Fecha: 05-11-2013. Fin.*/
				}

			//$('td:eq(3)',nRow).addClass('editable');

			$('td:eq(4)', nRow).addClass('sumable');	//El precio de lista
			$('td:eq(4)', nRow).html('$ '+aData[4]);
			
			$('td:eq(5)', nRow).addClass('sumable');	//El descuento
			$('td:eq(5)', nRow).addClass('dropable');	//La clase para dropable
			$('td:eq(5)', nRow).html($.sprintf("%.2f",aData[5])+' %');
			$('td:eq(5)', nRow).attr('producto_id',aData[7]); //Ponemos este atrubuto en la celda descuento para facilitar operaciones con los dropables
			
			$('td:eq(6)', nRow).addClass('sumable');	//El subtotal
			$('td:eq(6)', nRow).html('$ '+aData[6]);
			$('td:eq(7)', nRow).addClass('centrable');
			$('td:eq(7)', nRow).attr('productos_ids',true);
			$('td:eq(7)', nRow).attr('producto_id',aData[7]);
			$('td:eq(7)', nRow).html('<a href="javascript:void(0);" onclick="eliminar(this);"><img src="/css/borrar.png" width="18" height="18" border="0"/></a>');		
			return nRow;
		},
		"oLanguage": {
			"sZeroRecords": "No se han agregado productos"
		}
	});

	
	/**
	 * Hace editables las columnas de cantidad
	 */
	function editableCantidad()
	{
		/*
		Reingeniería PV | Responsable: FE. - Fecha: 22-10-2013. - ID: 15. - Campo: Cuadro del detalle.
		Descripción del cambio: Se agrega función para que acepte solo números.
		*/
		$(".editable").bind('keypress', function(e){
			var cc = (e.keyCode ? e.keyCode : e.which);
			noc=false; //ambito global
			if(cc==0){
				noc = true;
			}
			if (cc<48 || cc>57){
				return false;
			}
		});
		/*Fecha: 22-10-2013. Fin.*/
		$(".editable").editable(function(value, settings) 
			{
				if(value == '') return 1;
				if(value < 1) return 1;
				else if (value > 0) return parseInt(value);
				else if(isNaN(value)) return 1;
				else return 1;
				
			},
			{
				event : "click",
				select: true,
				width:	'60px',
				onblur : "submit",
				tooltip : "Ingresar cantidad",
				/*
				Reingeniería PV | Responsable: FE. - Fecha: 22-10-2013. - ID: 15. - Campo: Cuadro del detalle.
				Descripción del cambio: Se modifica el archivo ./Mako/web/js/plugins/jquery.jeditable.js para que soporte el atributo maxlength.
				*/
				maxlength: 4,
				/*Fecha: 22-10-2013. Fin.*/
				callback: function( sValue, y ) 
				{
					var aPos = tCar.fnGetPosition( this.parentNode );
					actualizaCantidad(sValue,aPos[0],aPos[1]);
				}
			}
		);
	}
	
	
	// Bindings
	
	$(document).bind('actualiza_carrito',editableCantidad);
	$(document).bind('actualiza_carrito',sumaTotal);
	$(document).bind('actualiza_carrito',cambio);
	$(document).bind('actualiza_carrito',puedeRealizarVenta);
	
	$(document).trigger('actualiza_carrito');
	
	//Escuchamos cuando se ejecuta el evento de búsqueda de socios, para borrar la tabla
	//No se deben sumar productos de diferentes socios en una venta.
	$(document).bind('socios.buscador.buscando',function(){
		console.debug('Borrando tabla...');
		tCar.fnClearTable();
		$(document).trigger('actualiza_carrito');
	});
        /*Reingeniería PV | Responsable: YD => FE. - Fecha: 23-11-2013. - ID: 17. - Campo: Cuadro del detalle.
        Descripción del cambio: Se crea función "limpiar carrito y punto de venta".*/
	$("#limpiar-venta").click(function() {

		/* Rutina para llenar la persiana de nuevo cuando se borra toda la tabla
		responsable: IMB 27/10/2014 */

		var producto_id = 0;

		$('#carrito tr').each(function(){

    		$(this).find('td').each(function(){				

    			if($(this).attr('producto_id'))
				{
					producto_id = $(this).attr('producto_id');
					$('#cobro-'+producto_id).show('fast');
					console.debug('llenando cobro' + producto_id);
					
				}
        	
    		})
		})
        
        console.debug('Borrando tabla ISAED14 carrito...');
		tCar.fnClearTable();

		$(document).trigger('actualiza_carrito');
		$('#puede_limpiar_venta').hide("");
		$("#vista_previa_prod").hide("slow");

	

	});
        /*Fecha: 23-11-2013. Fin.*/	
});
