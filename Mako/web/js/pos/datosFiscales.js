/*
 * Componentes Bixit
 *
 * Copyright (c) 2009-2011 Bixit SA de CV
 * Bajo Licencia dual MIT (http://www.bixit.mx/legal/MIT-LICENSE.txt)
 * y GPL (http://www.bixit.mx/legal/GPL-LICENSE.txt).
 *
 */
/**
 * Operaciones de la forma de datos fiscales.
 */
//TODO: hacer validación de RFC conforme al estandar.

/**
* Variable que tendrá los datos fiscales originales o los que guarde el/la cajer@. BGPJ
*/
DatosFiscales=new Array();
$(document).ready(function() 
{
	/*Reingeniería PV | Responsable: FE. - Fecha: 25-10-2013. - ID: 63 - Campo: Codigo postal.
	Descripción del cambio: Se agrego función para validar código postal.*/
	$( "#cp_fiscal" ).keyup(function( event ) {
		var tam=this.value.length;
		var expreg1=/^[1-9]|[0-9]|[1-9]$/;
		var expreg2=/^[1-9]{2}|[0-9][1-9]|[1-9][0-9]$/;
		var expreg3=/^([1-9]{2}|[0-9][1-9]|[1-9][0-9])[0-9]$/;
		var expreg4=/^([1-9]{2}|[0-9][1-9]|[1-9][0-9])[0-9]{2}$/;
		var expreg5=/^([1-9]{2}|[0-9][1-9]|[1-9][0-9])[0-9]{3}$/;
		RegExPattern='';
		if(tam==1)
			RegExPattern=expreg1;
		if(tam==2)
			RegExPattern=expreg2;
		if(tam==3)
			RegExPattern=expreg3;
		if(tam==4)
			RegExPattern=expreg4;
		if(tam==5)
			RegExPattern=expreg5;
		
		if (!this.value.match(RegExPattern)) {
			this.value = this.value.substr(0, this.value.length - 1);
		}
	});
	/*Fecha: 25-10-2013. - Fin.*/
	//Datos fiscales
	$('#req_factura').click(function() 
	{
		if($(this).attr('checked'))
		{
			
			secondFiscales(); // Antes de abrir el panel, llenamos los datos de municipio
			$wFiscales.dialog('open');
			
			$("#fiscales > input, #fiscales > select").each(function(){
				
				$(this).attr('requerido','1');
				if($(this).attr('id') == 'num_int_fiscal')
				{
					$(this).attr('requerido','0');
				}
			});
			
		}
	});

	var $wFiscales= $("#fiscales").dialog({
		modal: true,
		width: 500,
		autoOpen: false,
		closeOnEscape: false,
		title:	'Datos Fiscales',
		buttons: {
			'Guardar': function() {
				if(requeridos($("#fiscales").get(0) ))
				{

					//Validacion para municipio y colonia
					console.log($('#cmbMunicipio').val());
					if( $('#cmbMunicipio').val() == '' || $('#cmbMunicipio').val() == "Buscando Información ..."){
						abreError($('#cmbMunicipio').get(0));
						return false;
					}else
						cierraError($('#cmbMunicipio').get(0));

					console.log($('#id_colonia_fiscal').val());
					if( $('#id_colonia_fiscal').val() == '' || $('#id_colonia_fiscal').val() == "Buscando Información ..."){
						abreError($('#id_colonia_fiscal').get(0));
						return false;
					}else
						
					console.log($('#cp_fiscal').val());
					if( $('#cp_fiscal').val().length < 5){
						abreError($('#cp_fiscal').get(0));
						return false;
					}else
						cierraError($('#cp_fiscal').get(0));


					var dat = {};
					$("#fiscales > input, #fiscales > select").each(function(){
//						console.log($(this).val());
						dat[$(this).attr('id')] = $(this).val(); 
					});
					prevFiscales(dat);
					$(this).dialog('close');
				}
				return false;
			},
			'Cancelar':function() {
				$('#req_factura').attr('checked',false);
				ResetFiscales();
				$(this).dialog('close');
			}
		}
	});
	
	//Cuando se cierra la ventana de datos fiscales observamos si los campos requeridos estan vacios.
	//Si es así borramos el checkbox de datos fiscales
	$(document).bind('dialogclose',function(){ 

		$("#fiscales > input, #fiscales > select").each(function(){
			
			if($(this).val() == '' && $(this).attr('requerido')==1)
			{
				$('#req_factura').attr('checked',false);
				return false;
			}
		});
	});
	
	//Observamos el evento socio.seleccionado para consultar sus datos fiscales en caso de existir
	$(document).bind('socio.seleccionado',function(ev, socio_id, socio_nombre){

		console.log('Socio: '+socio_id);
		
		$.getJSON($("#urlGetFiscales").val(),{'socio_id':socio_id}, function(dat) {

			console.log('Socio: '+socio_id);

			/*
			 * Mandamos a llamar la funcionalidad para que se haga lo de la cascada de los combos de direccion.
			 * Silvio Bravo
			 * 06-Marzo_2014
			 * Reingeniria Mako
			 * */
			
			prevFiscales(dat);
			setInitCascade(DatosFiscales);
		});
		
	});
	
	//Observamos el evento de nueva busqueda, en nueva busqueda cleareamos los valores de los campos
	$(document).bind('socios.buscador.buscando',function(){
				$('#req_factura').attr('checked',false);
				$("#razon_social").val("");
				$("#rfc").val();
				$("#calle_fiscal").val("");
				$("#num_ext_fiscal").val("");
				$("#num_int_fiscal").val("");
				$("#colonia_fiscal").val("");
				$("#municipio_fiscal").val("");
				$("#cp_fiscal").val("");
				$("#estado_fiscal").val("");
	
				$("#vp-razon_social").html('');
				$("#vp-rfc").html('');
				$("#vp-calle_fiscal").html('');
				$("#vp-num_ext_fiscal").html('');
				$("#vp-num_int_fiscal").html('');
				$("#vp-colonia_fiscal").html('');
				$("#vp-municipio_fiscal").html('');
				$("#vp-cp_fiscal").html('');
				$("#vp-estado_fiscal").html('');
				
				$("#vp-socio-fiscales").hide('slow');
	});
});

/**
 * Llena el preview de datos fiscales
 * @param Object datos fiscales
 * @return
 */
function prevFiscales(dat)
{

	if(dat.rfc != undefined & dat.rfc != ''){
		console.log('--- Datos Fiscales ---');
		for(var d in dat)
		{
				insertarDato(d, dat[d]);
		}
		//Ahora incluimos el idEstado_fiscal, por lo que lo tenemos que buscar en la lista de estados
		estado_seleccionado = $('#estado_fiscal').val();
		id_Estado_Fiscal = 0;
		estados = $('#cmbEntidadFederativa option');
		console.log(estados);
		estados.each(function(index){
			if(estado_seleccionado==$(this).text()){
				insertarDato('idEstado_fiscal',$(this).val())
				$(this).attr("selected","selected");
			}
			else
				$(this).removeAttr("selected");
			
		});

		console.log('--- Datos Fiscales ---');
		$("#vp-socio-fiscales").show('slow');
	}
	else
	{
		console.debug('No trae fiscales');	
		$("#vp-socio-fiscales").hide('slow');

		// Datos Fiscales vacios 
		DatosFiscales['razon_social']='';
		DatosFiscales['rfc']='';
		DatosFiscales['calle_fiscal']='';
		DatosFiscales['colonia_fiscal']='';
		DatosFiscales['cp_fiscal']='';
		DatosFiscales['ciudad_fiscal']='';
		DatosFiscales['estado_fiscal']='';
		DatosFiscales['municipio_fiscal']='';
		DatosFiscales['num_ext_fiscal']='';
		DatosFiscales['num_int_fiscal']='';
		DatosFiscales['pais_fiscal']='';
		DatosFiscales['idEstado_fiscal']='';
		DatosFiscales['idMunicipio_fiscal']='';
		DatosFiscales['idColonia_fiscal ']='';

	}
	
}
/**
 * inserta los valores
 */
function insertarDato(key, val)
{
	console.log(key + ': ' + val);
	DatosFiscales[key]=val;
	$("#"+key).val(val); //introducimos los datos 

	$("#vp-"+key).html(val);
	
}
/**
 * Llena los valoes de datos fiscales, excepto estado y municipio
 */
function secondFiscales()
{
	prevFiscales(DatosFiscales);
	/*
	Ahora seleccionamos el municipio, debido a que no almacena el ID
	*/
	console.log('Los municipios que hay son:');
	municipios = $('#cmbMunicipio option');
	console.log(municipios);
	municipios.each(function(index){
		if(DatosFiscales['municipio_fiscal']==$(this).text())
			    $(this).attr("selected","selected");
		else
			$(this).removeAttr("selected");
		
	});
}
/**
 * Reestablece los valores de Datos fiscales
 */
function ResetFiscales()
{
	console.log('Se reestablecen los valores');
	DatosFiscales2 = DatosFiscales;
	prevFiscales(DatosFiscales2);
	setInitCascade(DatosFiscales2);
}

