/**
 * Arreglo global que contiene los productos que son venta unitaria
 */
var ventas_unitarias = [];

/**
 * Formatea la persiana del campo autocomplete
 * @param row Son los datos en forma de array, este array se define en la función parse
 * @return string
 */
//TODO: Si no está seleccionado el socio, no debe realizar búsqueda de productos.

function formatItem(row) 
{
	return row[1] + " <span >(<strong>Categoría: " + row[4]	+ "</strong>)</span>";
}

/**
 * Formatea el resultado de coincidencias en la persiana, indicando las subcadenas coincidentes 
 * @param row Son los datos en forma de array, este array se define en la función parse
 * @return string
 */
function formatResult(row) 
{
	return row[1].replace(/(<.+?>)/gi, '');
}

/**
 * Construye el arreglo de datos del autocompletador
 * @param data
 * @return
 */
function parser(data) {

	var parsed = [];
	for (key in data) {
		parsed[parsed.length] = {
				data: [ key, 
				        data[key].nombre,
				        data[key].descripcion,
				        data[key].precio_lista,
				        data[key].categoria,
				        data[key].existencia,
				        data[key].codigo,
				        data[key].descuento,
				        data[key].promocion_id,
				        data[key].modalidades_pago,
				        data[key].venta_unitaria
				        ],
				        value: data[key].nombre,
				        result: data[key].nombre
		};
	}
	return parsed;
}


/**
 * Procesa el registro seleccionado 
 * @param event
 * @param data
 * @param formatted
 * @return
 */
function seleccionado(event, data, formatted) 
{

	var socio_id = $("#socio_id").val();
	var socio_nombre = $("#socio_nombre").val();
	var prod_id = data[0];
	var nombre = data[1];
	var descrip = data[2];
	var precio = data[3];
	var catego = data[4];
	var cantidad = 1;
	var codigo = data[6];
	var descto = data[7];
	var promocion_id = data[8];
	var opciones_pago = data[9];
	var venta_unitaria = data[10];

	//Si es un producto de venta unitaria lo metemos en el array global ventas_unitarias
	if(venta_unitaria == '1')
	{
		ventas_unitarias[prod_id] = true;
	}
	
	
	$("#vista_previa_prod #categoria").html(catego);
	$("#vista_previa_prod #categoria").attr('categoria',catego);
	$("#vista_previa_prod #precio_lista").html(precio);
	$("#vista_previa_prod #descuento").html(descto);
	$("#vista_previa_prod #descripcion").html(descrip);
	$("#vista_previa_prod #socio").html(socio_nombre);
	
	$("#vista_previa_prod").show("slow");
	
	subtotal = $.sprintf("%.2f",subTotal(precio,cantidad,descto));
	
	//El producto Tiene modalidades de pago.
	//Cuando haya opciones de pago
	$('#lista-opciones-pago').html('');
	$('#producto-cobranza').hide();
	if(opciones_pago.length > 0)
	{

		
		var opcon = $('#plantilla-opcion-pago').clone(true);
		opcon.attr('id','opcion-pago-contado');
		opcon.find('.ligas-opciones-pago').attr('producto_id',prod_id);
		opcon.find('.ligas-opciones-pago').text('Contado');
		opcon.find('.numero-pagos').text('Un solo pago');
		opcon.show();
		
		//Cuando dan click a opción de pago de contado
		opcon.click(function()
		{
			console.log('click en: '+$(this).attr('cobro_id')+' prod id '+$(this).attr('producto_id'));			//Estos son productos unitarios, por fuerza debe verificarse si ya existen en la tabla de productos, si existen entonces no se vuelve  a agregar al carrito.
			if($('td[productos_ids][producto_id='+prod_id+']').attr('producto_id') != undefined) return;
			if($('td[productos_ids][sub_producto_de='+prod_id+']').attr('producto_id') != undefined) return;
			//Pago de contado
			tCar.fnAddData([codigo,catego,nombre,cantidad,precio,descto,subtotal,prod_id,'','','']);
			$(document).trigger('actualiza_carrito');
			$("#autocomp_productos").val("");
			$('#producto-cobranza').hide();
		});
		
		$('#lista-opciones-pago').append(opcon);
		
		for(var opp in opciones_pago)
		{

			var opc = $('#plantilla-opcion-pago').clone(true);
			opc.attr('id','opcion-pago-'+opciones_pago[opp].cobro_id);
			opc.attr('cobro_id',opciones_pago[opp].cobro_id);
			opc.attr('producto_id',prod_id);
			opc.find('.ligas-opciones-pago').text(opciones_pago[opp].modalidad);
			opc.find('.numero-pagos').text(opciones_pago[opp].num_pagos+' pagos');
			opc.show();
			opc.click(function(){
				//Consulta subproducto del tipo de cobranza actual
				console.log('click en: '+$(this).attr('cobro_id')+' prod id '+$(this).attr('producto_id'));
				var producto_id = $(this).attr('producto_id');
				var cobro_id = $(this).attr('cobro_id');
				
				//Estos son productos unitarios, por fuerza debe verificarse si ya existen en la tabla de productos, si existen entonces no se vuelve  a agregar al carrito.
				if($('td[productos_ids][producto_id='+prod_id+']').attr('producto_id') != undefined) return;
				if($('td[productos_ids][sub_producto_de='+prod_id+']').attr('producto_id') != undefined) return;
				
				//Traemos del servidor el primer subproducto de la opcion de pago
				$.getJSON($("#urlGetSubProducto").val(),
						{
							'subproducto_de': producto_id,
							'cobro_id': cobro_id,
							'socio_id': socio_id,
							'limit': 1
						}, function(dat) {
							console.log(dat);
							//Estos son productos unitarios, por fuerza debe verificarse si ya existen en la tabla de productos, si existen entonces no se vuelve  a agregar al carrito.
							if($('td[productos_ids][producto_id='+prod_id+']').attr('producto_id') != undefined) return;
							if($('td[productos_ids][sub_producto_de='+prod_id+']').attr('producto_id') != undefined) return;
							
							//Metemos el producto en ventas unitarias
							ventas_unitarias[dat.producto_id] = true;
							var subtotal = $.sprintf("%.2f",subTotal(dat.precio_lista,1,dat.descuento));
							tCar.fnAddData([dat.codigo,dat.categoria,dat.nombre,1,dat.precio_lista,dat.descuento,subtotal,dat.producto_id,'',dat.promocion_id,cobro_id]);
							
							$(document).trigger('actualiza_carrito');
							$("#autocomp_productos").val("");
							$('#producto-cobranza').hide();
							$('td[productos_ids][producto_id='+dat.producto_id+']').attr('sub_producto_de',producto_id);
						});

			});
			
			$('#lista-opciones-pago').append(opc);
		}
		
		$('#producto-cobranza').show('slow');
		return;
	}
	else
	{
		if(venta_unitaria == '1')
		{
			if($('td[productos_ids][producto_id='+prod_id+']').attr('producto_id') != undefined) return;
		}
	}
	
	tCar.fnAddData([
	                codigo,
	                catego,
	                nombre,
	                cantidad,
	                precio,
	                descto,
	                subtotal,
	                prod_id,
	                '',
	                promocion_id,
	                ''
                   ]);
	
	$(document).trigger('actualiza_carrito');
	
	$("#autocomp_productos").val("");
}


$(document).ready(function() 
{

	var url_accion = $("#url_accion").val();
	
	/**
	 * Auto complete buscador de productos
	 */
	$("#autocomp_productos").autocomplete(
			url_accion,
			{
				extraParams: {'limit': 20},
				dataType: 'json',
				parse: parser,
				autoFill: true,
				mustMatch: true,
				formatItem: formatItem,
				formatResult: formatResult,
				width: '600px'
	}).result(seleccionado);
	
	
	$("#autocomp_productos").blur(function(ev,val){
		console.debug('Saliendo de buscaventa por blur..');
		return false;
	});

	$("#autocomp_productos").change(function(ev,val){
		console.debug('Saliendo de buscaventa por change..');
		return false;
	});

	//Escuchamos cuando se ejecuta el evento de búsqueda de socios, 
	//No se deben sumar productos de diferentes socios en una venta.
	$(document).bind('socios.buscador.buscando',function(){
		console.debug('Cerrando preview de prods');
		$("#vista_previa_prod").hide("slow");
		$("#autocomp_productos").flushCache();
	});

	//Escuchamos cuando se selecciona un nuevo socio para setear el socio_id de los parametros enviados por 
	//el autocompletador de prods
	$(document).bind('socio.seleccionado',function(ev,socio_id,socio_nombre){
		console.debug('Reseteando valor de socio_id en autocomplete del buscador %s',socio_id);
		$("#autocomp_productos").setOptions({
			'extraParams':{'limit':20,'socio_id': socio_id}
		});
	});
	
});