/*
 * Componentes Bixit
 *
 * Copyright (c) 2009-2011 Bixit SA de CV
 * Bajo Licencia dual MIT (http://www.bixit.mx/legal/MIT-LICENSE.txt)
 * y GPL (http://www.bixit.mx/legal/GPL-LICENSE.txt).
 *
 */
/**
* Operaciones con los botones de controles de pago y cambio
 */


/**
 * Guarda la venta
 */


/**
 * 17 Feb 2014
 * Se agrego funcionalidad para aceptar el pago por tarjeta de credito,
 * Se captura el campo de numero de baucher siempre y cuando se haya seleccionado como forma de pago T.Credito
 * silvio.bravo@enova.mx
 *
 */

$(document).ready(function(){
	// Cambiar el mensaje de la ventana pAra capturar el numero de aprovacion o referencia
	$('#forma_pago_id').change(function(){
		switch($(this).val()) {
			case '2': //tarjeta bancaria
				$("#catch-baucher-window #msg-no-aprob").html('Registre el No. de Autorización');
				break;
			case '3': //deposito bancario
				$("#catch-baucher-window #msg-no-aprob").html('Registre el No. de Referencia');
				break;
		}
	});

	 //Ventana pára capturar el numero de baucher :)

	 $("#catch-baucher-window").dialog({

		modal			: true,
		minWidth		: 450,
		width			: 450,
		maxWidth		: 450,
		minHeight		: 250,
		height			: 250,
		maxHeight		: 250,
		autoOpen		: false,
		position		: ['center','top'],
		closeOnEscape	: true,
		title			:	'Aprobaci&oacute;n de Pago',
		buttons			: {


			"Cancelar"	: function(){
				$( this ).dialog( "close" );
			},

			"Registrar"	: function(){

					//Evaluamos si la caja de texto viene vacia
					if($.trim($("#baucherNumber").attr("value")).length >0 ){
						guardarVenta(); //Realizamos la venta
						$('#realizar-venta').hide();
						$("#catch-baucher-window").dialog("close");
					}
					else{
						var forma_pago_id = $('#forma_pago_id').val()
						var msg           = 'Registre el No. de Aprobación';

						switch($('#forma_pago_id').val()) {
							case '2': //tarjeta bancaria
								alert('Registre el No. de Autorización');
								break;
							case '3': //deposito bancario
								alert('Registre el No. de Referencia');
								break;
						}

						$("#baucherNumber").focus();
					}


			}
		}
	 });



	 $("#baucherNumber").keypress(function(e){
		 if(e.keyCode==13){

			 guardarVenta(); //Realizamos la venta
			 $('#realizar-venta').hide();
			 $("#catch-baucher-window").dialog("close");

		 }
	 });



	/**
	 * Evaluamos cuando seleccionaron la forma de pago con tarjeta para actualizar los valores del monto recibido
	 * y activar la validacion del boton de guardar la venta
	 *
	 * */
	$("#forma_pago_id").change(function(){

		if(parseInt($(this).attr("value")) >= 2 ){
			$("#pago").attr("value", $("#a_pagar").html());
			$("#pago").attr("readOnly","readOnly");
			$("#pago").trigger("keyup");
			//puedeRealizarVenta();
		}
		else{
			$("#pago").removeAttr("readOnly");

		}
	});

	$('#a_pagar').change(function(){


	});
});




function guardarVenta()
{
	var nods = tCar.fnGetData();

	//Cuando se borran productos en el carrito de venta quedan los espacios en el arreglo,
	//Este es un problema en el codigo del datatables. Por eso se debe limpiar el arreglo de la venta
	var nodsLimpio = [];
        /*Reingeniería PV | Responsable: ES => FE & LG. - Fecha: 05-11-2013. - ID: 14. - Campo: Cuadro del detalle.
        Descripción del cambio: Se quita el atributo enlace y se agrega solo el valor numérico.*/
	var CantLimpia = 0;
        /*Fecha: 05-11-2013. - FIN*/
	var j=0;
	for(var i=0; i< nods.length; i++)
	{
		console.debug('Hay producto? %s',nods[i]);
		if(nods[i])
		{
		        /*Reingeniería PV | Responsable: ES => FE & LG. - Fecha: 05-11-2013. - ID: 14. - Campo: Cuadro del detalle.
		        Descripción del cambio: Se quita el atributo enlace y se agrega solo el valor numérico.*/
			CantLimpia=Number($("#CantId" + i).html());
			nods[i][3] = CantLimpia;
			/*Fecha: 05-11-2013. - Fin*/
			nodsLimpio[j] = nods[i];
			j++;
		}
	}

	var total = Number($("#a_pagar").html());
	var fiscales = {};

	$("#fiscales > input, #fiscales > select").each(function(){
		//Evaluamos para registrar los datos fiscales y no se vallan entre ellos los combos, mas bien solo el id_colonia_fiscal

		if($(this).attr("id")!= "cmbEntidadFederativa" && $(this).attr("id")!= "cmbMunicipio"){
			fiscales[$(this).attr('id')] = $(this).val();
		}
	});

	/*var datosVenta = {
			socio_id: $("#socio_id").val(),
			venta: nodsLimpio,
			total: total,
			fiscales: fiscales,
			es_factura: $("#req_factura").attr('checked')
	};*/

	/**
	 * 	Agregamos nuevas variables de envio para realizar la venta.
	 * el codigo arriba comentado es la estructura anterior.
	 * silvio.bravo@enova.mx
	 * 18 Feb 2014
	 * **/


	var datosVenta = {
			socio_id: $("#socio_id").val(),
			venta: nodsLimpio,
			total: total,
			fiscales: fiscales,
			es_factura: $("#req_factura").attr('checked'),
			forma_pago_id: parseInt($("#forma_pago_id").attr("value")),
			num_aprobacion_tarjeta: $("#baucherNumber").attr("value")
	};

	console.warn(JSON.encode(datosVenta));

	//Enviamos los datos de la venta al servidor y obtenemos la respuesta JSON
	$.post($("#urlRealizarVenta").val(),
		{
			'venta':JSON.encode(datosVenta)
		},
		function(dat) {

			console.warn('Resultado: %s',dat.resultado);
			if(dat.resultado == "error")
			{
				alert(dat.msg);
				$('#puede_realizar_venta').show();
				$('#realizar-venta').show();
				return;
			}
			else{
				//Disparamos el evento de venta realizada y propagamos la respuesta
				$(document).trigger('venta.realizada',[dat]);
			}
		},
		'json'
	);
}

/**
 * Calcula el cambio.
 * @return
 */
function cambio()
{

	/**
	 *
	 * 	Agregamos esta opcion para evaluar cuando esta seleccionada la forma de pago en t. credito pero siguen agregando productos
	 * a la lista
	 * silvio.bravo@enova.mx
	 * 18 Feb 2014
	 *
	 **/

	if(parseInt($("#forma_pago_id").attr("value"))>1){
		$("#pago").attr("value", $("#a_pagar").html());
		$("#pago").attr("readOnly","readOnly");
	}
	else{
		$("#pago").removeAttr("readOnly");

	}

	/************  fin de validacion *****************************/


	var a_pagar = Number($("#a_pagar").html());
	var pago = $.sprintf( "%.2f",($("#pago").val()));
	var cambio = pago-a_pagar;

	if($("#pago").val() == '' && a_pagar >= 0){cambio = 0;}
	if(pago >= 0 && a_pagar == 0) {cambio=0; $("#pago").val(0);}
	if(isNaN(cambio)){cambio=0; $("#pago").val(0); $("#pago").select();}

	if(cambio < 0 && pago > 0)
	{
		$("#label_cambio").html('Por pagar:');
		$("#cambio").html($.sprintf( "%.2f",Math.abs(cambio))).css('color',cambio < 0 ? 'red':'');
	}
	else if(cambio < 0 && pago == 0)
	{
		$("#label_cambio").html('Cambio:');
		$("#cambio").html($.sprintf( "%.2f",0));
	}
	else
	{
		$("#label_cambio").html('Cambio:');
		$("#cambio").html($.sprintf( "%.2f",cambio)).css('color',cambio < 0 ? 'red':'');
	}
}

function puedeRealizarVenta()
{
	var nods = tCar.fnGetData();
	var total = 0;
	var nProds = 0;
	for(var idx in nods)
	{
		if(nods[idx] != null)
		{
			nProds++;
		}
	}
	var a_pagar = Number($("#a_pagar").html());
	var pago = $.sprintf( "%.2f",($("#pago").val()));
	var socio_id = $("#socio_id").val();

	if(a_pagar <= pago && nProds > 0 && socio_id != '')
	{

		$('#puede_realizar_venta').fadeIn('slow');
	}
	else
	{
		$('#puede_realizar_venta').hide();
	}
}

/**
 * Actualiza la cantidad de un producto, dados
 * @param valor
 * @param fila
 * @param columna
 * @return
 */
function actualizaCantidad(valor,fila,columna)
{
	var dat = tCar.fnGetData( fila );
	//Recalculamos el subtotal de la fila
	var precio = dat[4];
	var descto = dat[5];
	var cantidad = isNaN(valor)?1:Number(valor);

	var subtotal = cantidad * $.sprintf("%.2f",subTotal(precio,1,descto));

        /*Reingeniería PV | Responsable: ES => FE. - Fecha: 05-11-2013. - ID: 14. - Campo: Cuadro del detalle.
        Descripción del cambio: Se agregó la funcionalidad editable del atributo cantidad (n) veces.*/
	//Actualizamos la celda de cantidad y su arreglo de datos asociado con fnUpdate
        tCar.fnUpdate( '<a id="CantId' + fila + '" href="#" tabindex="1" class="editable">'+valor+'</a>', fila, columna );
        /*Fecha: 05-11-2013. Fin.*/

	//Actualizamos la celda de cantidad y su arreglo de datos asociado con fnUpdate
	//Es la columna 7 por lo tanto su indice es 6, habrá q modificar aquí si cambia el orden
	// o sea gregan columnas
	tCar.fnUpdate( $.sprintf("%.2f",subtotal), fila, 6 );

	//Se ha actualizado el carrito por lo tanto lanzamos el evento
	$(document).trigger('actualiza_carrito');
}

/**
 * Actualiza el descuento de un producto, dados
 * @param descuento
 * @param fila
 * @param columna
 * @return
 */
function actualizaDescuento(descto,fila,columna)
{
	console.log('fila %s columna %s descto %s',fila,columna,descto);
	var dat = tCar.fnGetData( fila );
	//Recalculamos el subtotal de la fila
	var cantidad = dat[3];
	var precio = dat[4];

	var subtotal = cantidad * $.sprintf("%.2f",subTotal(precio,1,descto));

	//Actualizamos la celda de descuento y su arreglo de datos asociado con fnUpdate
	tCar.fnUpdate( $.sprintf("%.2f",descto), fila, columna );

	//Actualizamos la celda de subtotal y su arreglo de datos asociado con fnUpdate
	//Es la columna 7 por lo tanto su indice es 6, habrá q modificar aquí si cambia el orden
	// o sea gregan columnas
	tCar.fnUpdate( $.sprintf("%.2f",subtotal), fila, 6 );

	//Se ha actualizado el carrito por lo tanto lanzamos el evento
	$(document).trigger('actualiza_carrito');
}

$(document).ready(function()
{
	/**
	 * Ejecuta función de guardado y realiza el hover del botón.
	 */
	$('#realizar-venta').click(function()
	{



		/*
		 * Evaluamos si la forma de pago es en efectivo pasa a registrarse la venta directamentee
		 * en caso contrario enviamos a capturar el numero de baucher
		 * silvio.bravo@enova.mx
		 * 18 Feb 2014
		 *
		 * */

		switch(parseInt($("#forma_pago_id").attr("value"))){
			case 1:
				guardarVenta();
			break;

			default:
				$("#catch-baucher-window").dialog("open");
				$("#baucherNumber").attr("value","");
				return 1;
				//Retornamos algo para no entrar en el hide del boton
			break;


		}

		$(this).hide();

		/******   Finaliza evaluacion de forma de pago   **********/

		}).hover(function() {
			$(this).addClass("ui-state-hover");
		}, function() {
			$(this).removeClass("ui-state-hover");
		}).mousedown(function() {
			$(this).addClass("ui-state-active");
		}).mouseup(function() {
			$(this).removeClass("ui-state-active");
	});

	//Bindings y eventos
	$('#pago').keyup(function() { puedeRealizarVenta();});
	$("#pago").blur(function(){$(this).val($.sprintf("%.2f",$(this).val()));});
	$("#pago").focus(function(){$(this).select();});

	//Se dispara cuando hay un evento de venta exitosa
	$(document).bind('venta.realizada',function(){

		$("#dialogo").dialog({
			modal: true,
			beforeclose:function() {
				window.location.href=getmylocalUrl()
				return false;
			},
			buttons: {
				Ok: function() {
					$(this).dialog('close');
				}
			}
		});

	});


	function getmylocalUrl(){
		return (window.location.href);

	}

});
