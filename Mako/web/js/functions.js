/*
* Reingenieria  Registro de Socios
* Responsable:  (BP) Bet Gader Porcayo Juárez
* Fecha:        22/oct/2014
* Fecha mod:    27/ene/2014
* Version:      1.1
* Descripción:  funcion de alert
*/
function alert(msg){
    process = true;
    mensaje = msg;

    //Variables de apoyo para acomodar el tamaño (alto) de la PoPuP
    alto_base = 120;
    alto_fuente=16;
    ancho_popup = 450;
    alto_popup = 0;
    caracteres_maximos_por_linea = 60;
    regex = /(<([^>]+)>)/ig;
    numero_lineas = 0;

    console.log('Numero de cacteres Totales' + mensaje.length);

    /*
     *      Parseo de la cadena
     */


    /*
     * Paso 1: Reemplazo de por <br>
     *  \n      => <br>
     * </br>    => <br>
     * <br/>    => <br>
     *  \t      => 4 espacio (&nbsp;)
     *  
     */
    console.log(mensaje);
    mensaje = mensaje.replace('</br>','<br>');
    mensaje = mensaje.replace('<br/>','<br>');
    mensaje = mensaje.replace(/\n/g,'<br>');
    mensaje = mensaje.replace(/\t/g,'&nbsp;&nbsp;&nbsp;&nbsp;');
    console.log('Texto parseado');
    console.log(mensaje);


     /*
     * Paso 2 Split
     *  Para saber cuantas en cuantas lineas de texto se mostrarán,
     *  se hace un split de <br>, y por cada elemento obtenido como 
     *  texto, se cuenta el número de caracteres que haya
     */
    mensaje_array = mensaje.split('<br>');
    console.log('Numero de elemento en el mensaje ' + mensaje_array.length);
    $.each(mensaje_array, function(index, value){
        console.log();
        console.log();
        console.log();
        mensaje_interno = value;

        //Reemplazamos todos los espacios colocados como caracter html &nbsp; y lo colocamos con un solo espacio
        mensaje_interno = mensaje_interno.replace(/&nbsp;/g, '_');
        console.log('mensaje_interno');
        console.log(mensaje_interno);
        //ahora eliminamos todos los tags de html de nuestra cadena para limpiarla
        mensaje_interno = mensaje_interno.replace(regex, '');
        //ahora contamos el número de caracteres
        numero_caracteres_interno = mensaje_interno.length; console.log('Numero de caracteres en el renglon ' + index + ': ' + numero_caracteres_interno);
        //Ahora dividimos la cadena en el número de caracteres por linea permitidos
        num_lineas_interno_float = numero_caracteres_interno/caracteres_maximos_por_linea;
        num_lineas_interno = parseInt(num_lineas_interno_float) + ((num_lineas_interno_float%1 > 0)?1:0); console.log('Numero de lineas en el renglon ' + index + ': ' + num_lineas_interno);
        numero_lineas += num_lineas_interno;
    });
    console.log('Numero de lineas en totales: ' + numero_lineas);


     /*
     * Paso 2 Calculo alto de la ventana
     *  Habiendo calculado el nímero de líneas totales, o renglones, o saltos de líneas, 
     *  ahora hacemos el calculo del alto de la PoPuP
     */

    if(numero_lineas <= 1){
        alto_popup = alto_base;
    }else{
        aumento = numero_lineas * alto_fuente;
        alto_popup = aumento + alto_base;
    }
    console.log('El alto de la PoPuP es de ' + alto_popup + 'px');


    $('body').append('<div id="modal_dialog"><div class="title" id="TitleModal"></div></div>');
    $('#TitleModal').html(mensaje);
    var dialog = $('#modal_dialog').dialog({
        modal: true,
        resizable: false,
        height: alto_popup,
        width: ancho_popup,
        buttons: {
            "Aceptar": function() {
                process = false;;
                $( this ).dialog( "close" );
                $("#modal_dialog").remove();
            }
          }
    });
    $('.ui-dialog .ui-dialog-buttonpane').css('margin', '0');
    $('.ui-dialog .ui-dialog-buttonpane').css('-webkit-box-shadow', 'none');
    $('.ui-dialog .ui-dialog-buttonpane').css('-moz-box-shadow', 'none');
    $('.ui-dialog .ui-dialog-buttonpane').css('box-shadow', 'none');
    $('#modal_dialog').css('-webkit-box-shadow', 'none');
    $('#modal_dialog').css('-moz-box-shadow', 'none');
    $('#modal_dialog').css('box-shadow', 'none');

    console.log('************************************');
}



