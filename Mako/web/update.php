<?php
/**
 * @author Arturo Bruno <arturo.bruno@enova.mx>
 *
 * Este script recibe los datos de gitlab (10.250.0.6) y realiza un pull para
 * obtener los ultimos cambios del branch de produccion del proyecto.
 *
 */

error_log('[code_update] Peticion para actualizar recibida.');

$log       = date('Y-m-d H:i:s ['.$_SERVER['REMOTE_ADDR'].']')."\n\n";
$project   = 'local_ria';
$gitlab    = '10.250.0.6';
$branch    = $project.'_PROD';
$repo      = 'origin';
$git_min_v = 1.7;
$notify    = array('arturo.bruno@enova.mx',
                   'silvio.bravo@enova.mx',
                   'luis.gonzalez@enova.mx',
                   'gerardo.delarosa@enova.mx');

/**
 * Asegurarse que la peticion viene de donde debe de venir...
 */
if($_SERVER['REMOTE_ADDR'] != $gitlab) {
    logg('No tienes permisos para actualizar el codigo...', true);
}

/**
 * Tenemos git??? con la version aceptada?
 */
$git_version = doubleval(str_replace('git version', '', exec('git --version')));

if($git_version < $git_min_v ) {
    logg('El servidor no cuenta con git, o con la version minima ('.$git_min_v.')', true);
}

/**
 * Revisamos que los datos recibidos sean correctos y matcheen con lo pedido
 */
$data = json_decode(trim(isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : null));
file_put_contents('/tmp/gitlab_data', print_r($data, true)."\n\n", FILE_APPEND);

if(is_null($data)) {
    logg('No se recibieron datos que procesar... :(', true);
}

if(str_replace('refs/heads/', '', $data->ref) !== $branch) {
    logg('El push no fue al repositorio que se esta revisando. ['.$data->ref.']['.$branch.']', true);
    die();
}

chdir('../../');
$current_branch = str_replace('* ', '', trim(shell_exec('git branch | grep \*')));

if($current_branch != $branch) {
    logg('Los branches no cooinciden, no se hara nada. ['.$current_branch.']', true);
}

$pull = trim(shell_exec('sudo -u root git pull '.$repo.' '.$branch));

//Colocar permisos correctos:
$perm = shell_exec('sudo chown -R www-data:www-data *');

logg($pull);


/*********************************
 **  @@ LGL 
 **  2015-01-08  Cada vez que se actualice el código en cada servidor se debe limpiar el CACHE de SYMFONY
 **
 *********************************/

$cleancache = shell_exec('sudo php /var/www/Mako/symfony cc');
logg("\n\tLimpiando CACHE de SYMFONY...\n");
logg($cleancache);

if($pull != 'Already up-to-date.'){
    notify_by_email();
}

function logg($m, $die = false) {
    global $log;

    $log .= '['.date('Y-m-d H:i:s').'] '.$m."\n";

    error_log('[code_update]: '.$m);

    if($die === true) {
        die($m);
    }
}

function notify_by_email() {
    global $log, $notify, $project, $gitlab, $branch, $repo, $git_version, $git_min_v, $data;

    $subject = '[UPDATE] ['.$_SERVER['HTTP_HOST'].'] ['.$project.'] ['.$repo.'] ['.$branch.']';
    $message = 'Se ejecuto la actualizacion del codigo de <b>'.$project.'</b> en el servidor '.$_SERVER['HTTP_HOST'].
               ' a peticion de: <b>['.$data->user_name.']</b>.<hr>'.
               'Gitlab Addr: '.$gitlab.'<br>'.
               'Repositorio: '.$data->repository->name.'<br>'.
               'Branch: '     .$branch.'<br>'.
               '<hr>'.
               '<pre>'.$log.'</pre>'.
               '<hr>'.
               '<pre>'.print_r($data, true).'</pre>';
    $headers = array('MIME-Version: 1.0',
                     'Content-type: text/html; charset=utf-8',
                     'From: '.$branch.' <'.$_SERVER['HTTP_HOST'].'@enova.mx>');

    foreach ($notify as $email) {
        if(!mail($email, $subject, $message, implode("\r\n", $headers))) {
            error_log('[code_update]: Mail not sent "'.$email.'"');
            file_put_contents('/tmp/'.$project.'_update_log', $log."\n\n\n\n", FILE_APPEND);

        } else {
            error_log('[code_update]: Mail sent "'.$email.'"');
        }
    }
}