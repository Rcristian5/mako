<?php
include ('./conexion.php');
include ('./funciones.php');

$ocupados=0; //variable para no descontar saldo al socio
$tipo_id=1; //indica el tipo de sesión utilizada por el usuario, 1 - Default, 2 - Sin consumo de tiempo
$id=generaId();
$ip=getRealIpAddr();

error_log("\n[INSERT.PHP] PARAMS[datos] => " . $_POST['datos'],3,"/tmp/ctrl_asistencia.log");
error_log("\n[INSERT.PHP] PARAMS[motivo] => " . $_POST['motivo'],3,"/tmp/ctrl_asistencia.log");

$data=$_POST['datos']; //usuario,fecha_login,fecha_logout,grupo_id,socio_id,curso,numDias
$motivo=$_POST['motivo'];

// Obtenemos el motivo, si viene motivo es por que viene de la pantalla Extemporanes
// en caso de que no venga motivo, viene de Pase de Lista
$TipoAsistencia='Extemporanea';
if ($motivo == '0' || $motivo == 0){
    $motivo = 'NULL';
    $tipo_asistencia_id = '2';
    $TipoAsistencia='Registro';
}else
    $tipo_asistencia_id = '1';

$oprid=$_REQUEST['oprid'];
$explodeData=explode('|',$data);
$explodeFechaIni=explode('@',$explodeData[1]);
$explodeFechaFin=explode('@',$explodeData[2]);
$fi=$explodeFechaIni[0].' '.$explodeFechaIni[1];
$ff=$explodeFechaFin[0].' '.$explodeFechaFin[1];

$ms=getTimestamp();
$fi=$fi.'.'.$ms;

//Query para obtener el último saldo de un socio, este saldo se pasara como dato para los campos saldo_inicial, sald_final
$getUltimoSaldo="select saldo_final from sesion_historico where socio_id='$explodeData[4]' order by fecha_logout desc limit 1";
error_log("\n[INSERT.PHP] QRY_ULT_SALDO => " . $getUltimoSaldo,3,"/tmp/ctrl_asistencia.log");
//echo $getUltimoSaldo;

//Query para obtener el id_computadora y el centro_id dependiendo de la ip
$getCompCentro="select id, centro_id from computadora where ip='$ip'";
error_log("\n[INSERT.PHP] QRY_GETPC => " . $getCompCentro,3,"/tmp/ctrl_asistencia.log");
//echo $getCompCentro;

$UltimoSaldo = pg_query ($con, $getUltimoSaldo ) or die ("Cannot execute query: $getUltimoSaldo\n");
$CompCentro = pg_query ($con, $getCompCentro ) or die ("Cannot execute query: $getCompCentro\n");

$Saldo=pg_fetch_row($UltimoSaldo);
$CompCentroData=pg_fetch_row($CompCentro);

if ($Saldo==''){
    $Saldo[0]=0;
}

//Modificaciones verificación 20% Verification - 23092013 ***
// @@ LGL  2016-05-05  Se ajusta porcentaje a 25% por petición de operación   REDMINE Ref# 2109

$verificaAsis="select * from list_asist where id_curso='$explodeData[5]' AND id_grupo='$explodeData[3]' AND id_socio='$explodeData[4]'";
error_log("\n[INSERT.PHP] QRY_VERF_ASIST => " . $verificaAsis,3,"/tmp/ctrl_asistencia.log");
$numJust = pg_query ($con, $verificaAsis) or die ("Cannot execute query: $verificaAsis\n");

$num = pg_num_rows($numJust);

// ROMPEMOS REGLAS PARA LOS SIGUIENTES CURSOS
if (in_array($explodeData[5], array('142075731670579', '142075737959757', '142075746698020', '142179544242130', '142075735340710', '141928694982054') ) ){
   $num=0;
}
$AsistenciaMax=0;
switch ($TipoAsistencia) {
    case 'Registro':
        $AsistenciaMax=1.5;
        break;
    case 'Extemporanea':
    default:
        $AsistenciaMax=0.25;
        break;
}

error_log("\n[INSERT.PHP] ===  ASISTENCIAS_OBTENIDAS_PARA_SOCIO => " . $num,3,"/tmp/ctrl_asistencia.log");
error_log("\n[INSERT.PHP] ===  ASISTENCIA_MAX_POSIBLE => " . $AsistenciaMax,3,"/tmp/ctrl_asistencia.log");
error_log("\n[INSERT.PHP] ===  explodeData[6] => " . $explodeData[6],3,"/tmp/ctrl_asistencia.log");
error_log("\n[INSERT.PHP] ===  FACTOR (ASISTENCIAS_OBTENIDAS_PARA_SOCIO/ASISTENCIA_MAX_POSIBLE)  => " . ($num/$explodeData[6]) / $AsistenciaMax,3,"/tmp/ctrl_asistencia.log");

if ($num/$explodeData[6]>=$AsistenciaMax){
    error_log("\n[INSERT.PHP] >>>>  NO_ES_VALIDO_PASAR_ASISTENCIA ",3,"/tmp/ctrl_asistencia.log");
    echo "0";
    return false;
}
else{
    $queryInsert="insert into sesion_historico (id,centro_id,computadora_id,ip,socio_id,usuario,fecha_login,fecha_logout,saldo_inicial,saldo_final,tipo_id,ocupados) values ('$id','$CompCentroData[1]','$CompCentroData[0]','$ip','$explodeData[4]','$explodeData[0]','$fi','$ff','$Saldo[0]','$Saldo[0]','$tipo_id','$ocupados')";

    error_log("\n[INSERT.PHP] QRY_INSERCIÓN => " . $queryInsert,3,"/tmp/ctrl_asistencia.log");
    error_log("Este es el query de insersion :::::::::::::::::::::::::::::::::::::::::::::::::");
    error_log($queryInsert);
    $insert= pg_query ($con, $queryInsert ) or die ("Cannot execute query: $queryInsert\n");
    $createdat=date("Y-m-d H:i:s");
    $list_asist="insert into list_asist (id_curso,  id_grupo,  id_socio,  fecha_asistencia,  created_at,  updated_at,  motivo_id,  oprid,  tipo_asistencia) values ('$explodeData[5]',  '$explodeData[3]',  '$explodeData[4]',  '$fi',  '$createdat',  '$createdat',  $motivo,  '$oprid',  '$tipo_asistencia_id ')";
    error_log("\n[INSERT.PHP] QRY_LIST_ASIST => " . $list_asist,3,"/tmp/ctrl_asistencia.log");
        $insert_list_asist=pg_query ($con, $list_asist ) or die ("Cannot execute query: $list_asist\n");
    if ($insert){
        error_log("\n[INSERT.PHP] >>>>  RESULT_INSERT => OK",3,"/tmp/ctrl_asistencia.log");
        echo "1";
    }
    pg_free_result($insert);
    pg_close($con);
}
?>
