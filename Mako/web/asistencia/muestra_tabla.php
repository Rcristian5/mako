<?php
include './conexion.php';

//Se reciben los Id's del grupo y del curso
$gpo=$_POST["grupo"];
$curso=$_POST["curso"];
$gpotxt=$_POST["grupotxt"];
$curtxt=$_POST["cursotxt"];
//Query que regresa la lista de asistencias para un determinado grupo

$getAsistencias="SELECT
    s.nombre_completo,
    s.usuario,
    fh.fecha, fh.fecha + fh.hora_inicio fecha_hr_inicio,
    fh.fecha + fh.hora_fin fecha_hr_fin,
    min(sh.fecha_login), max(sh.fecha_logout),
    case when min(sh.fecha_login) is null then false else (fh.fecha + fh.hora_inicio, fh.fecha + fh.hora_fin - interval '10 mins') OVERLAPS (min(sh.fecha_login),max(sh.fecha_logout)) end asistencia,
    case when min(sh.fecha_login)::timestamp - (fh.fecha + fh.hora_inicio )::timestamp > '00:10:00' then true else false end as retardo,
    g.id as gpo_id, s.id socio_id
FROM grupo_alumnos ga
JOIN socio s ON (s.id = ga.alumno_id)
JOIN grupo g ON (g.id = ga.grupo_id)
JOIN horario hr ON (hr.id = g.horario_id)
INNER JOIN fechas_horario fh ON (fh.horario_id = hr.id)
LEFT JOIN sesion_historico sh ON ((fh.fecha + fh.hora_inicio + interval '10 mins', fh.fecha + fh.hora_fin - interval '10 mins') OVERLAPS (sh.fecha_login,sh.fecha_logout) and sh.socio_id = s.id)
LEFT JOIN computadora comp ON (comp.centro_id = g.centro_id and sh.ip = comp.ip)
WHERE
    g.id = ".$gpo." AND
    ga.preinscrito=FALSE
GROUP BY s.nombre_completo, fh.fecha, fh.hora_inicio, fh.hora_fin, s.usuario, g.id, s.id, ga.preinscrito
ORDER BY nombre_completo, fh.fecha";

//echo $getAsistencias;

//Se realiza un query del query anterior para obtener las fechas en las que se imparte un curso
$getDias="SELECT DISTINCT x.fecha,count(*) registros FROM(".$getAsistencias.") x group by x.usuario,x.fecha,asistencia order by fecha";

$dias = pg_query ($con, $getDias ) or die ("Cannot execute query: $getDias\n");
$asistencias = pg_query ($con, $getAsistencias ) or die ("Cannot execute query: $getAsistencias\n");

//Si no se ejecuta algun query por alguna razón, se manda el mensaje de error
if(!$asistencias || !$getDias){
	echo "Error al ejecutar la consulta.\n";
	exit;
}

//Se verifica que el query regrese resultados para poder empezar a generar la tabla
$rows = pg_num_rows($asistencias);
$rowsDias = pg_num_rows($dias);

////////////////////////////////////////////////////////////////////////////////////////
if ($rows>0){
	$tabla2="";
        $tabla2.="<table class='dataTables_wrapper' border='0' id='control-asistencias' name='control-asistencias' width='100%'>";
        $tabla2.="<thead><tr><th class='ui-state-default'>NOMBRE DEL SOCIO</th>";

        //while para pintar los dias en que se dio el curso como encabezado de la tabla
        while ($rowDias=pg_fetch_array($dias)){
                $tabla2.="<th class='ui-state-default' colspan=".$rowDias[1].">".date('d-m-Y',strtotime($rowDias[0]))."</th>";

		if ($rowDias[1]>1){
                	$rowsDias++;
        	}
        }
	//Cambio solicitado por operaciones
	$tabla2.="<th class='ui-state-default'>Asistió</th>";
        $tabla2.="<th class='ui-state-default'>No Asistió</th>";
        $tabla2.="<th class='ui-state-default'>No. de Sesiones</th>";
        $tabla2.="<th class='ui-state-default'>% Asistencias</th>";
        $tabla2.="</tr></thead><tbody>";

	$socio_actual2='';
	$i=$a=$b=$c=$d=0;
	$at=$bt=$ct=$dt=0;
	$tmp=$tmp2=0;
	while ($rowData = pg_fetch_array($asistencias)){
		$i++;
		if ($socio_actual2!=$rowData[0]){
			$tabla2.="<tr class='odd'><td>".$rowData[0]."</td>";
                }
		$fecha_hr_inicio=str_replace(" ","@",$rowData[3]);
                $fecha_hr_fin=str_replace(" ","@",$rowData[4]);
                $ids_concat = $rowData[1] . "|" . $fecha_hr_inicio . "|" . $fecha_hr_fin . "|" . $rowData[9] . "|" . $rowData[10] . "|" .$curso . "|" . $rowsDias;
                
                $fecha_conc=explode(' ',$rowData[3]); //20%
                $fecha_conc2=str_replace("-","",$fecha_conc[0]); //20%
                $id_new=$rowData[10].$fecha_conc2; //20%
                
                //Verificamos si el alumno tiene asistencia, si la tiene se regresa un checkbox checked deshabilitado
                if ($rowData[7]=='t'){
                	$a=$a+1;
			$at=$at+1;
			$tmp2++;
                	$tabla2.="<td><input type='checkbox' id='$rowData[10]$fecha_conc2' name='$rowData[10]$fecha_conc2' value='$rowData[9]' disabled='disabled' checked onclick=\"eliminaAsistencia('$ids_concat','$id_new');\"><p style=\"display:none\">X</p> </td>";
                }
                else{
                        //Validación de fechas futuras
                        $fa=date("Y-m-d 23:00:00");
                        if ($rowData[3]>=$fa){
                            $b=$b+1;
                            $bt=$bt+1;
                            $tabla2.="<td><input type='checkbox' id='$rowData[10]$fecha_conc2' name='$rowData[10]$fecha_conc2' value='$rowData[9]' class='ClaseFija' disabled='disabled' onclick=\"insertaAsistencia('$ids_concat','$id_new');\"></td>";
                        }
                        else{
                	    $b=$b+1;
			    $bt=$bt+1;
                            //$tabla2.="<td><input type='checkbox' id='$rowData[9]$fecha_conc2' name='$rowData[9]$fecha_conc2' value='$rowData[9]' class='ClaseFija' onclick=\"insertaAsistencia('$ids_concat','$id_new');this.disabled=true;\"></td>";
                            $tabla2.="<td><input type='checkbox' id='$rowData[10]$fecha_conc2' name='$rowData[10]$fecha_conc2' value='$rowData[9]' class='ClaseFija' onclick=\"insertaAsistencia('$ids_concat','$id_new');\"></td>";
                        }
                }
		
		$tmp++;
		

		if($rowsDias==$i){
			$tabla2.="<td>".$a."</td>";
                	$tabla2.="<td>".$b."</td>";
			$c=$a+$b;
			$tabla2.="<td>".$c."</td>";
			if($c!=0)
                        {
                                $d=round(($a/$c)*100,2);
                        }
			$tabla2.="<td>".$d."%</td>";
			$tabla2.="</tr>";
			//Se reinicializan las variables
			$i=$a=$b=$c=$d=0;	
		}
		$ct=$at+$bt;
		$socio_actual2=$rowData[0];
		
	}
	$promedio_gpo=round(($tmp2/$tmp)*100,2);
	$contd="<td><b>PROMEDIO GRUPO</b></td>";
	for ($k=0;$k<$rowsDias;$k++){
		$contd.="<td></td>";
	}
	$prom_asistio="<td><b>".round(($at/$ct)*100,2)." %</b></td>";
	$prom_noasistio="<td><b>".round(($bt/$ct)*100,2)." %</b></td>";
	$prom_gpo="<td></td><td><b>".$promedio_gpo." %</b></td>";
 	$trini="<tr class='odd'>";
	$trfin="</tr></tbody>";	
	$tabla2.=$trini.$contd.$prom_asistio.$prom_noasistio.$prom_gpo.$trfin;
}
else{
        echo "No se encontraron resultados";
}
	
////////////////////////////////////////////////////////////////////////////////////////
echo $tabla2;
$script='<script>$("#control-asistencias").dataTable({
                "sDom": \'T<"clear">R<"H"lfr>t<"F"ip>\',
                "sPaginationType":"full_numbers",
		"aaSorting":[[0, "desc"]],
                "oTableTools": {
                        "sSwfPath": "./DataTables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf",
			"aButtons": [
				{
					"sExtends": "xls",
					"sButtonText": "Exportar Excel"
				},
				{
					"sExtends": "pdf",
					"sButtonText": "Exportar PDF"
				}
			]
                },
                "oLanguage": {
                "sUrl": "./DataTables/extras/TableTools/lenguaje/dataTables.spanish.txt"
        },
                "bJQueryUI":true
});</script>';
echo $script;
?>
