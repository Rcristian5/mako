<?php

include './conexion.php';

//Se recibe por POST el curso seleccionado del combobox
$CursoSeleccionado = $_POST["curso"];
$OperadorId = $_POST["oprid"];
$Config_Site_TodosLosCursos = ($_POST["todos"] == '1') ? true : false;
$Config_Site_CursosNoAsociadoMoodle = ($_POST["nomoodle"] == '1') ? true : false;

$QueryCursosNoAsociadosMoodle = "";
if ($Config_Site_CursosNoAsociadoMoodle) {
    $QueryCursosNoAsociadosMoodle = " AND cur.asociado_moodle=FALSE ";
}


/**************************************
**
**  @@ LGL  2016-05-05
** Se modifica Query para obtención de cursos a desplegar en base
** a nuevas reglas de negocio regidas por OPERACIONES
**
**************************************/

// Regla No 1.
//      Se deben mostrar los cursos que tengan grupos programados con fecha_inicio igual a la fecha de consulta.
// Regla No 2.
//      Se deben mostrar los cursos que hayan iniciado, y que su fecha_fin sea mayor a la fecha consultada
// Regla No 3.
//      Se deben mostrar los cursos que tengan grupos programados con fecha_fin hasta 9 días antes de la fecha de consulta.
//      Los días domingo no deben considerarse para este efecto.


// Obtenemos cual sería la fecha con la que vamos a comparar 

$sql_fecha_limite_inferior = "SELECT i FROM (
                                                        SELECT
                                                            int4( ROW_NUMBER() OVER (ORDER BY i DESC)) as no_dia,
                                                            i::date
                                                            FROM generate_series(CURRENT_DATE - interval '1 day', CURRENT_DATE - interval '15 days', '-1 day'::interval) i
                                                            WHERE EXTRACT (DOW FROM i) <> 0
                                                            ORDER BY i DESC
                            ) fechas 
                            WHERE no_dia = 9
";
$rs = pg_query($con, $sql_fecha_limite_inferior) or die("Error de ejecución de consulta: $sql_fecha_limite_inferior\n");
$row = pg_fetch_row($rs);

$fecha_limite_inferior = $row[0];


$filtro_regla_negocio_1 = " ( hr.fecha_inicio::date = CURRENT_DATE::date) ";
$filtro_regla_negocio_2 = " ( current_date::date BETWEEN hr.fecha_inicio::date AND hr.fecha_fin::date )";
$filtro_regla_negocio_3 = " ( hr.fecha_fin::date >= '$fecha_limite_inferior' AND hr.fecha_inicio::date <= CURRENT_DATE)";

//Cambiar a este query que contiene la validacion de los 30 dias posteriores de finalizado el curso
$query='';
if ($Config_Site_TodosLosCursos)
    $query = "
		SELECT 
			c.id, c.ip, c.nombre, cur.id, cur.nombre, g.id, g.clave, a.id, a.nombre,hr.id, hr.fecha_inicio, hr.fecha_fin, 
			hr.lunes, hr.hi_lu, hr.hf_lu, hr.martes, hr.hi_ma, hr.hf_ma, hr.miercoles, hr.hi_mi, hr.hf_mi, hr.jueves, hr.hi_ju, hr.hf_ju, 
			hr.viernes, hr.hi_vi, hr.hf_vi, hr.sabado, hr.hi_sa, hr.hf_sa 
		FROM 
			grupo g 
		JOIN curso cur ON ( cur.id = g.curso_id AND cur.id= $CursoSeleccionado AND cur.activo=TRUE AND substring(cur.clave from 1 for 6) <> 'CMCANA' )
        JOIN centro c ON ( c.id = g.centro_id)
		JOIN aula a on (a.id = g.aula_id and a.centro_id = c.id)
		JOIN horario hr on (hr.id = g.horario_id AND ( $filtro_regla_negocio_1 OR $filtro_regla_negocio_2 OR $filtro_regla_negocio_3))
		WHERE
            g.activo=TRUE
			$QueryCursosNoAsociadosMoodle 
		ORDER BY c.nombre, cur.nombre, g.clave;";
else {
    $query = "
    	SELECT 
    		c.id, c.ip, c.nombre, cur.id, cur.nombre, g.id, g.clave, a.id, a.nombre,hr.id, 
    		hr.fecha_inicio,  hr.fecha_fin, hr.lunes, hr.hi_lu, hr.hf_lu, hr.martes, hr.hi_ma, hr.hf_ma, 
    		hr.miercoles, hr.hi_mi,  hr.hf_mi, hr.jueves, hr.hi_ju, hr.hf_ju, hr.viernes, hr.hi_vi, hr.hf_vi, 
    		hr.sabado, hr.hi_sa, hr.hf_sa  
    	FROM 
    		grupo g  
    	JOIN curso cur  ON ( cur.id = g.curso_id AND cur.id= $CursoSeleccionado)
    	JOIN centro c ON ( c.id = g.centro_id)  
    	JOIN aula a  ON (a.id = g.aula_id and a.centro_id = c.id)  
    	JOIN horario hr  on (hr.id = g.horario_id  AND ( $filtro_regla_negocio_1 OR $filtro_regla_negocio_2 OR $filtro_regla_negocio_3))" ;
    $query .= " AND cur.activo=TRUE AND g.activo=TRUE AND g.facilitador_id=$OperadorId $QueryCursosNoAsociadosMoodle ORDER BY c.nombre, cur.nombre, g.clave;";
}

error_log("\nQRY_GPOS => " . $query,3,"/tmp/grupos_asistencia.log");

$lg = pg_query($con, $query) or die("Cannot execute query: $query\n");

if (!$lg) {
    echo "An error occured.\n";
    exit;
}

//Se poblan todos los grupos pertenecientes al grupo seleccionado
echo "<option value='0'>Seleccione</option>";
while ($row = pg_fetch_row($lg)) {
    echo "<option value='$row[5]'>" . $row[6] . "</option>";
}
?>
