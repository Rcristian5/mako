<?php
include './conexion.php';
$OperadorId = isset($_REQUEST['oprid']) ? $_REQUEST['oprid'] : NULL;
if ($OperadorId == '' || $OperadorId == null)
    $OperadorId = 1;

$TipoAsistencia = isset($_REQUEST['tpas']) ? $_REQUEST['tpas'] : NULL;
if ($TipoAsistencia == '' || $TipoAsistencia == null)
    $TipoAsistencia = 'Extemporanea'; // Solo hay dos casos actualmente, Extemporanea y Registro (Normal)

// Obtenemos configuración del CENTRO
$PathYmlApp = $PathMako . '/apps/frontend/config/config_centro.yml';
$YamlApplication = sfYaml::load($PathYmlApp);

#$CENTRO_ACTUAL_ID = $YamlApplication[getEnv('MAKO_RIA_R_ENV')]['centro_actual_id'];
$CENTRO_ACTUAL_ID = 2;
?>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<title>Control de Asistencia Extemporánea</title>
<link href="./../favicon.ico" rel="shortcut icon">
<link href="./../css/general.css" media="screen" type="text/css" rel="stylesheet">
<link href="./../css/formas.css" media="screen" type="text/css" rel="stylesheet">
<link href="./../css/start/jquery.ui.custom.css" media="screen" type="text/css" rel="stylesheet">
<link href="./../css/datatables.css" media="screen" type="text/css" rel="stylesheet">
<script type="text/javascript" src="./js/jquery-1.9.0.min.js"></script>

<script type="text/javascript" src="./DataTables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="./DataTables/extras/TableTools/media/js/TableTools.min.js"></script>

<style type="text/css" media="screen">
@import "./DataTables/extras/TableTools/media/css/TableTools.css";
@import "./DataTables/media/css/demo_table_jui.css";
div.dataTables_wrapper { font-size: 12px; }
table.display thead th, table.display td { font-size: 12px; }
</style>

</head>
<script type="text/javascript" >
<!-- Se utiliza el Jquery para realizar la dependencia de los combos-->

//Funcion para realizar la dependecia del combo de Cursos con Grupos
$(document).ready(function(){
   $("#cursos").change(function () {
            $("#cursos option:selected").each(function () {
            curso=$(this).val();
            console.log(curso);
            $.post("grupos_historico.php", { curso: curso }, function(data){
            $("#grupos").html(data);
            });
        });
   })
});

//Funcion que realiza la tabla dinamica de asistencias
$(document).ready(function(){
   $("#grupos").change(function () {
            $("#grupos option:selected").each(function () {
            grupo=$(this).val();
            $.post("muestra_tabla_read.php", { grupo: grupo, curso: curso}, function(data){
            $("#control-asistencias_wrapper").html(data);
	    //document.getElementById("excel").style.display = 'block';
            });
        });
   })
});

//Funcion que realiza el insertado de los datos a la tabla de sesion_historico
function insertaAsistencia(x){
	$.post("insert.php",{datos:x},function(data){
	$("#inserta").html(data);
	alert (data);
	});
}

//Funcion que elimina una asistencia
function eliminaAsistencia(y){
	$.post("delete.php",{datos:y},function(data){
	$("#elimina").html(data);
	alert (data);
	});
}


</script>

<body>
<div class="header"></div>
<div class="clearfix menubar">
    <div class="menu" style="float:left;"> </div>
    <div style="float:right;">
        Facilitador |
        <a href="/">Home</a>
        |
        <a onclick="return confirm('¿Desea salir del sistema?');" href="/sesiones/logoutExt">Salir</a>
    </div>
</div>
<div style="padding:5px;" class="ui-widget-header ui-corner-top tsss">
<b>HISTÓRICO</b>
</div>
<div style="padding:5px;" class="ui-widget-content ui-corner-bottom">

	<form id="listaAsistencia" name="listaAsistencia" method="POST" action="./updateAsistencias.php">
		Curso: <select name="cursos" id="cursos">
		<option value="0">Seleccione</option>

		<?php

		$curso="
		    SELECT DISTINCT
        		cur.id
        		, cur.nombre
    		FROM
        		curso cur
    		INNER JOIN horario hr ON (hr.curso_id = cur.id)
    		INNER JOIN grupo g ON (g.horario_id = hr.id AND g.curso_id = hr.curso_id AND g.activo AND g.centro_id = $CENTRO_ACTUAL_ID)
    		WHERE
        		cur.activo
        		AND cur.clave NOT ILIKE '%ANA%'
    		ORDER BY cur.nombre
		";
		$lc = pg_query ($con, $curso ) or die ("Cannot execute query: $curso\n");

		if (!$lc){
			echo "An error occured.\n";
			exit;
		}

		while ($row = pg_fetch_row($lc)){
			echo "<option value='$row[0]'>".$row[1]."</option>";
		}
		?>
		</select>

		Grupo: <select id="grupos" name="grupos">
		<option value="0">Seleccione</option>
		</select>
		<br>
		<br>
	</form>
	<div class="dataTables_wrapper" id="control-asistencias_wrapper">
	</div>

	</div>
</div>
</body>
</html>
