CREATE TABLE list_asist
(
      id_curso bigint NOT NULL,
      id_grupo bigint NOT NULL,
      id_socio bigint NOT NULL,
      fecha_asistencia timestamp without time zone,
      created_at timestamp without time zone,
      updated_at timestamp without time zone,
      motivo_id serial NOT NULL,
      oprid integer NOT NULL,
      CONSTRAINT list_asist_motivo_id_fkey FOREIGN KEY (motivo_id)
      REFERENCES motivo_asistencia (id_motivo) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);
