
<?php
include './conexion.php';

$Config_Site_Motivo = FALSE;
$Config_Site_CursosNoAsociadoMoodle = FALSE;



$OperadorId = isset($_REQUEST['oprid']) ? $_REQUEST['oprid'] : NULL;
if ($OperadorId == '' || $OperadorId == null)
    $OperadorId = 1;

$TipoAsistencia = isset($_REQUEST['tpas']) ? $_REQUEST['tpas'] : NULL;
if ($TipoAsistencia == '' || $TipoAsistencia == null)
    $TipoAsistencia = 'Extemporanea'; // Solo hay dos casos actualmente, Extemporanea y Registro (Normal)

$TituloPagina = "";
switch ($TipoAsistencia) {
    case 'Registro':
        $TituloPagina = "Registro de Asistencia";
        $Config_Site_Motivo = FALSE;
        $Config_Site_CursosNoAsociadoMoodle = TRUE;
        break;
    case 'Extemporanea':
    default:
        $TituloPagina = "Asistencia Extemporánea";
        $Config_Site_Motivo = TRUE;
        $Config_Site_CursosNoAsociadoMoodle = FALSE;
        break;
}

// Obtenemos las configuraciones o los roles
$PathYmlApp = $PathMako . '/apps/frontend/config/config_centro.yml';
$YamlApplication = sfYaml::load($PathYmlApp);

#$CENTRO_ACTUAL_ID = $YamlApplication[getEnv('MAKO_RIA_ENV')]['centro_actual_id'];
$CENTRO_ACTUAL_ID = 2;

$PathYmlApp = $PathMako . '/apps/frontend/config/app.yml';
$YamlApplication = sfYaml::load($PathYmlApp);

$RolUsuarioSql = "SELECT rol_id FROM usuario WHERE id=$OperadorId";
$RolUsuarioQuery = pg_query($con, $RolUsuarioSql) or die("Cannot execute query: $RolUsuarioSql\n");
$RolUsuario = pg_fetch_row($RolUsuarioQuery);


$filtro_todos_los_cursos = "";
$select_todos_los_cursos = "";

switch ($RolUsuario[0]) {
    case "{$YamlApplication['all']['rol_facilitador_id']}":
    case "{$YamlApplication['all']['rol_soporte_tecnico_id']}":
        $Config_Site_TodosLosCursos = false;
        $filtro_todos_los_cursos = " AND g.facilitador_id = " . $OperadorId;
        $select_todos_los_cursos = " , g.facilitador_id ";
        break;
    case "{$YamlApplication['all']['rol_coordinador_academico_id']}":
    case "{$YamlApplication['all']['rol_facilitador_encargado_id']}":
    case "{$YamlApplication['all']['rol_supervisor_id']}":
    case "{$YamlApplication['all']['rol_super_usuario_id']}":
        $Config_Site_TodosLosCursos = true;
        break;

    default:
        header("http://$_SERVER[REQUEST_URI]");
        break;
}
$QueryCursosNoAsociadosMoodle = "";
if ($Config_Site_CursosNoAsociadoMoodle) {
    $QueryCursosNoAsociadosMoodle = " AND NOT cur.asociado_moodle";
}else{
    $QueryCursosNoAsociadosMoodle = " AND cur.asociado_moodle";
}

/**************************************
**
**  @@ LGL  2016-05-05
** Se modifica Query para obtención de cursos a desplegar en base
** a nuevas reglas de negocio regidas por OPERACIONES
**
**************************************/

// Regla No 1.
//      Se deben mostrar los cursos que tengan grupos programados con fecha_inicio igual a la fecha de consulta.
// Regla No 2.
//      Se deben mostrar los cursos que hayan iniciado, y que su fecha_fin sea mayor a la fecha consultada
// Regla No 3.
//      Se deben mostrar los cursos que tengan grupos programados con fecha_fin hasta 9 días antes de la fecha de consulta.
//      Los días domingo no deben considerarse para este efecto.


// Obtenemos cual sería la fecha con la que vamos a comparar 

$sql_fecha_limite_inferior = "SELECT i FROM (
                                                        SELECT
                                                            int4( ROW_NUMBER() OVER (ORDER BY i DESC)) as no_dia,
                                                            i::date
                                                            FROM generate_series(CURRENT_DATE - interval '1 day', CURRENT_DATE - interval '15 days', '-1 day'::interval) i
                                                            WHERE EXTRACT (DOW FROM i) <> 0
                                                            ORDER BY i DESC
                            ) fechas 
                            WHERE no_dia = 9
";
$rs = pg_query($con, $sql_fecha_limite_inferior) or die("Error de ejecución de consulta: $sql_fecha_limite_inferior\n");
$row = pg_fetch_row($rs);

$fecha_limite_inferior = $row[0];


$filtro_regla_negocio_1 = " ( hr.fecha_inicio::date = CURRENT_DATE::date) ";
$filtro_regla_negocio_2 = " ( current_date::date BETWEEN hr.fecha_inicio::date AND hr.fecha_fin::date )";
$filtro_regla_negocio_3 = " ( hr.fecha_fin::date >= '$fecha_limite_inferior' AND hr.fecha_inicio::date <= CURRENT_DATE)";

$SQL_Cursos = "
    SELECT DISTINCT
        hr.curso_id
        , cur.nombre
        , cur.clave
        $select_todos_los_cursos
    FROM
        curso cur
    INNER JOIN horario hr ON (hr.curso_id = cur.id)
    INNER JOIN grupo g ON (g.horario_id = hr.id AND g.curso_id = hr.curso_id AND g.activo AND g.centro_id = $CENTRO_ACTUAL_ID)
    WHERE
        cur.activo
        $QueryCursosNoAsociadosMoodle
        $filtro_todos_los_cursos
        AND ( $filtro_regla_negocio_1 OR $filtro_regla_negocio_2 OR $filtro_regla_negocio_3)
        -- Eliminamos ANAS
        AND cur.clave NOT ILIKE '%ANA%'
    ORDER BY hr.curso_id";
//var_dump($SQL_Cursos);
//var_dump($Config_Site_TodosLosCursos);
$ListaCursos = pg_query($con, $SQL_Cursos) or die("Cannot execute query: $SQL_Cursos\n");

if (!$ListaCursos) {
    echo "An error occured.\n";
    exit;
}
?>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
            <title><?php echo $TituloPagina ?></title>
            <link href="./favicon.ico" rel="shortcut icon">
            <link href="./../css/general.css" media="screen" type="text/css" rel="stylesheet">
            <link href="./../css/formas.css" media="screen" type="text/css" rel="stylesheet">
            <link href="./../css/start/jquery.ui.custom.css" media="screen" type="text/css" rel="stylesheet">
            <link href="./../css/datatables.css" media="screen" type="text/css" rel="stylesheet">
            <script type="text/javascript" src="./js/jquery-1.9.0.min.js"></script>
            <script type="text/javascript" src="./DataTables/media/js/jquery.dataTables.js"></script>
            <script type="text/javascript" src="./DataTables/extras/TableTools/media/js/TableTools.min.js"></script>
            <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
            <style type="text/css" media="screen">
                @import "./DataTables/extras/TableTools/media/css/TableTools.css";
                @import "./DataTables/media/css/demo_table_jui.css";
                div.dataTables_wrapper { font-size: 12px; }
                table.display thead th, table.display td { font-size: 12px; }
            </style>
    </head>

    <script type="text/javascript" >
        var mot = '';
        var x;
        var y;
        $(function () {
            var motivo = $("#motivo");
            $("#dialog-form").dialog({
                autoOpen: false,
                height: 150,
                width: 350,
                modal: true,
                buttons: {
                    "Aceptar": function () {
                        $(this).dialog("close");
                        callback(x, y, motivo.val());
                    },
                },
            });
            $("#create-user")
                    .button()
                    .click(function () {
                        $("#dialog-form").dialog("open");
                    });
        });

        //Funcion para realizar la dependecia del combo de Cursos con Grupos
        $(document).ready(function () {
            $("#cursos").change(function () {
                document.getElementById('control-asistencias_wrapper').style.display = 'none';
                $("#cursos option:selected").each(function () {
                    curso = $(this).val();
                    cursotxt = $(this).text();
                    $.post("grupos.php", {curso: curso, todos:<?php echo ($Config_Site_TodosLosCursos) ? '1' : '0'; ?>, nomoodle:<?php echo ($Config_Site_CursosNoAsociadoMoodle) ? '1' : '0'; ?>, oprid:<?php echo $OperadorId; ?>}, function (data) {
                        $("#grupos").html(data);
                    });
                });
            })
        });
        //Funcion que realiza la tabla dinamica de asistencias
        $(document).ready(function () {
            $("#grupos").change(function () {
                $("#grupos option:selected").each(function () {
                    grupo = $(this).val();
                    grupotxt = $(this).text();
                    $.post("muestra_tabla.php", {grupo: grupo, curso: curso, grupotxt: grupotxt, cursotxt: cursotxt}, function (data) {
                        document.getElementById("control-asistencias_wrapper").style.display = 'block';
                        $("#control-asistencias_wrapper").html(data);
                    });
                });
            })
        });
        function callback(x, y, value) {
            $.post("insert.php", {datos: x, motivo: value, oprid:<?php echo $OperadorId; ?>}, function (data) {
                $("#inserta").html(data);
                if (data == 1) {
                    alert("Asistencia Tomada");
                    $('input[name=' + y + ']').attr('checked', true);
                    $('input[name=' + y + ']').attr('disabled', true);
                }
                if (data == 0) {
                    alert("No se puede pasar asistencia a más del 25% de sesiones");
                    $('input[name=' + y + ']').attr('checked', false);
                    $('input[name=' + y + ']').attr('disabled', false);
                }
            });
        }


        //Funcion que realiza el insertado de los datos a la tabla de sesion_historico
        function insertaAsistencia(a, b) {
            x = a;
            y = b;
<?php if ($Config_Site_Motivo) { ?>
                $("#dialog-form").dialog("open");
<?php } else { ?>
                callback(x, y, 0);
<?php } ?>

        }
    </script>

    <body>
        <div class="header"></div>
        <div class="clearfix menubar">
            <div class="menu" style="float:left;"></div>
            <div style="float:right;">
                |
                <a href="/">Home</a>
                |
                <a onclick="return confirm('¿Desea salir del sistema?');" href="/sesiones/logoutExt">Salir</a>
            </div>
        </div>
        <div style="padding:5px;" class="ui-widget-header ui-corner-top tsss">
            <b><?php echo $TituloPagina ?></b>
        </div>
        <div style="padding:5px;" class="ui-widget-content ui-corner-bottom">
            <div align="right"><a href="historico.php?oprid=<?php echo $OperadorId; ?>&tpas=<?php echo $TipoAsistencia ?>">Consulta Histórico</a></div>
            <form id="listaAsistencia" name="listaAsistencia" method="POST" action="./updateAsistencias.php">
                Curso: 
                <select name="cursos" id="cursos">
                    <option value="0">Seleccione</option>
                    <?php
                    while ($row = pg_fetch_row($ListaCursos))
                        echo "<option value='$row[0]'>" . $row[1] . "</option>";
                    ?>
                </select>

                Grupo: <select id="grupos" name="grupos">
                    <option value="0">Seleccione</option>
                </select>
                <br/>
                <br/>
            </form>

            <div class="dataTables_wrapper" id="control-asistencias_wrapper">
                <!--Div que muestra la informacion de la tabla dinámica-->
            </div>

        </div>
        <?php if ($Config_Site_Motivo) { ?>
            <div id="dialog-form" name="dialog-form" title="CONTROL DE ASISTENCIAS">
                <label for="motivo">Selecciona el motivo:</label>
                <select id="motivo" name="motivo">
                    <?php
                    $motivo = "SELECT id_motivo, descripcion FROM motivo_asistencia WHERE activo=TRUE";
                    $lista_motivos = pg_query($con, $motivo) or die("Cannot execute query: $motivo\n");
                    while ($row = pg_fetch_row($lista_motivos)) {
                        echo "<option value='$row[0]'>" . $row[1] . "</option>";
                    }
                    ?>
                </select>
            </div>
        <?php } ?>

        <div id="pp" name="pp"></div>

    </body>
</html>
