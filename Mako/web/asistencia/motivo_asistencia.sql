CREATE TABLE motivo_asistencia
(
      id_motivo serial NOT NULL,
      descripcion character varying(50),
      activo boolean DEFAULT true,
      created_at timestamp without time zone,
      updated_at timestamp without time zone,
      CONSTRAINT motivo_asistencia_pkey PRIMARY KEY (id_motivo)
);


INSERT INTO motivo_asistencia (id_motivo,descripcion,activo,created_at,updated_at) VALUES (1,'Falla técnica',TRUE,now(),now());
INSERT INTO motivo_asistencia (id_motivo,descripcion,activo,created_at,updated_at) VALUES (2,'Justificación laboral',TRUE,now(),now());
INSERT INTO motivo_asistencia (id_motivo,descripcion,activo,created_at,updated_at) VALUES (3,'Justificación médica',TRUE,now(),now());
