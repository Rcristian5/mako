    <?php
    $PathActual = getcwd();
    $PathMako = $PathActual . '/../..';

    require_once $PathMako . '/lib/vendor/symfony/lib/yaml/sfYaml.php';

    $PathYmlConfig = $PathMako . '/config/databases.yml';
    $YamlConfiguration = sfYaml::load($PathYmlConfig);

    $Ambiente = (isset($YamlConfiguration[getEnv('MAKO_RIA_ENV')])) ? getEnv('MAKO_RIA_ENV') : 'all';

    $ConfigBD = $YamlConfiguration[$Ambiente]['propel']['param']['dsn'];
    $ConfigBDArray0 = explode(':', $ConfigBD);
    $ConfigBDArray1 = explode(';', $ConfigBDArray0[1]);
    $ConfigBD_Config1 = explode('=', $ConfigBDArray1[0]);
    $ConfigBD_Config2 = explode('=', $ConfigBDArray1[1]);
    if ($ConfigBD_Config1[0] == 'dbname')
        $Config_BD_BDName = $ConfigBD_Config1[1]; //Nombre de la BD
    elseif ($ConfigBD_Config1[0] == 'host')
        $Config_BD_Host = $ConfigBD_Config1[1]; //Host donde se encuentra la BD
    if ($ConfigBD_Config2[0] == 'dbname')
        $Config_BD_BDName = $ConfigBD_Config2[1]; //Nombre de la BD
    elseif ($ConfigBD_Config2[0] == 'host')
        $Config_BD_Host = $ConfigBD_Config2[1]; //Host donde se encuentra la BD

    //Usuario de la BD 
    $Config_BD_User = (isset($YamlConfiguration[$Ambiente]['propel']['param']['username'])) ? $YamlConfiguration[$Ambiente]['propel']['param']['username'] : NULL;
    //Password del usuario de la BD
    $Config_BD_Pass = (isset($YamlConfiguration[$Ambiente]['propel']['param']['password'])) ? $YamlConfiguration[$Ambiente]['propel']['param']['password'] : NULL;

//Se realiza la conexión
    $con = pg_connect("host=$Config_BD_Host dbname=$Config_BD_BDName user=$Config_BD_User password=$Config_BD_Pass") or die("Could not connect to server\n");

    if (!$con) {
        echo "An error occured.\n";
        exit;
    }
    ?>
