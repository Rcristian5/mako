<?php
include './conexion.php';

//Se recibe por POST el curso seleccionado del combobox
$curso=$_POST["curso"];

//Cambiar a este query que contiene la validacion de los 30 dias posteriores de finalizado el curso

$query="
	SELECT c.id, c.ip, c.nombre, cur.id, cur.nombre, g.id, g.clave, a.id, a.nombre,hr.id,
			hr.fecha_inicio, hr.fecha_fin, hr.lunes, hr.hi_lu, hr.hf_lu, hr.martes, hr.hi_ma, hr.hf_ma,
			hr.miercoles, hr.hi_mi, hr.hf_mi, hr.jueves, hr.hi_ju, hr.hf_ju, hr.viernes, hr.hi_vi, hr.hf_vi,
			hr.sabado, hr.hi_sa, hr.hf_sa
	FROM grupo g
	JOIN curso cur ON ( cur.id = g.curso_id  AND substring(cur.clave from 1 for 6) <> 'CMCANA' )
	JOIN centro c ON ( c.id = g.centro_id)
	JOIN aula a on (a.id = g.aula_id and a.centro_id = c.id)
	JOIN horario hr on (hr.id = g.horario_id)
	WHERE cur.id=".$curso." AND cur.activo=TRUE AND g.activo=TRUE ORDER BY c.nombre, cur.nombre, g.clave;";

$lg = pg_query ($con, $query ) or die ("Cannot execute query: $query\n");

if (!$lg){
	echo "An error occured.\n";
	exit;
}

//Se poblan todos los grupos pertenecientes al grupo seleccionado
echo "<option value='0'>Seleccione</option>";
while ($row = pg_fetch_row($lg)){
	echo "<option value='$row[5]'>".$row[6]."</option>";
}
?>
