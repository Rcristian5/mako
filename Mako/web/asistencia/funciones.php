<?php

//Verifica que sea exactamente la ip real
function getRealIpAddr()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
      $ip=$_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
      $ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

//Genera el id para el bigint de la tabla sesion_historico, esta funcion se utiliza en MAKO en var/www/mko/lib/Comun.php
function generaId(){
        usleep(1000);
        list($useg, $seg) = explode(" ", microtime());
        $museg = sprintf("%05s",substr($useg,2,5));
        $cseg = $seg.$museg;
        return $cseg;
        }

//Funcion que regresa el numero de milisegundos para evitar conflictos con el constraint de la tabla sesion_historico
function getTimestamp(){
    $seconds = microtime(true); // true = float, false = weirdo "0.2342 123456" format 
    $a=explode('.',$seconds);
    return $a[1];
}


?>
